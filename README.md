# www.quaternum.net : carnet web d'Antoine
Sources du carnet web d'Antoine, [www.quaternum.net](https://www.quaternum.net).

## Moteur
Le carnet est un ensemble de fichiers Markdown transformés en fichiers HTML et CSS organisés grâce au générateur de site statique [Hugo](https://gohugo.io/).
Le _thème_ est une création personnelle, et pour les références bibliographiques j'utilise le formidable [Hugo Cite](https://labs.loupbrun.ca/hugo-cite/) de [Louis-Olivier](https://www.loupbrun.ca/).

## Propositions de modifications
N'hésitez pas à proposer des modifications via un patch avec `git-send-email` ([ce n'est pas si compliqué](https://git-send-email.io/)) sur le dépôt principal, ou une [issue](https://gitlab.com/antoinentl/www.quaternum.net/issues/new) sur [le dépôt miroir](https://gitlab.com/antoinentl/www.quaternum.net/).
