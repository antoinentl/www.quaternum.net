---
layout: post
title: "La qualité web : l'amélioration continue"
date: "2015-11-26T22:00:00"
comments: true
published: true
categories: 
- flux
slug: la-qualite-web-l-amelioration-continue
---
>La qualité Web est à la fois subjective puisque chacun en a sa perception et partiellement mesurable, car il existe des outils qui permettent de la rendre un peu plus objective. Je préfère souvent parler d’amélioration continue ou de professionnalisme. En contexte professionnel, plutôt que parler de qualité Web, il vaut mieux se poser la question du management de la qualité Web qui est une approche nettement moins subjective, qui rapporte, qui améliore l’image, qui renforce le positionnement sur le numérique… Bref, il faut y aller, et oui, ça n’a pas de fin et c’est pour ça que c’est bien.  
[Rashel Réguigne, Elie Sloïm, l’engagement pour la qualité web](http://blog-de-geekette.com/blog/numerique/elie-sloim-qualite-web/)

Un long entretien avec Elie Sloïm de [Temesis](http://temesis.com/), autour de la qualité web et de l'accessibilité.