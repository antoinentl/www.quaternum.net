---
layout: post
title: "Performance Web : 1 Méga, c'est beaucoup"
date: "2015-06-15T22:00:00"
comments: true
published: true
categories: 
- flux
tags:
- performance
slug: performance-web
---
>En résumé, les sites Web ne doivent pas seulement être configurés pour être délivrés rapidement, ils doivent être légers, tout simplement.  
[Performance Web : 1 Méga, c'est beaucoup, Élie Sloïm](http://blog.temesis.com/post/2015/06/15/Performance-Web-1-mega-c-est-beaucoup)

Est-ce que la question du poids et du chargement des pages Web doit se limiter à l'expérience utilisateur ? Il s'agit d'un problème plus global :

- *consommations* côtés serveur et client ;
- temps de chargement ;
- temps d'affichage ;
- expérience utilisateur ;
- alternatives en cas de problème de chargement ;
- etc.