---
layout: post
title: "Mise en chantier d’Opquast V3"
date: "2015-07-01T22:00:00"
comments: true
published: true
categories: 
- flux
tags:
- standards
slug: mise-en-chantier-d-opquast-v3
---
>Si vous ne connaissez pas Opquast (pour OPen QUAlity STandards), ce sont des référentiels de bonnes pratiques créées collaborativement, le tout sous l’égide de la société Temesis.  
[Mise en chantier d’Opquast V3, Le collectif Openweb](http://openweb.eu.org/blog/mise-en-chantier-d-opquast-v3)

Un projet colossal mais ouvert et très documenté, à tel point que l'on peut suivre les discussions à propos de bonnes pratiques axées structuration (HTML), mise en forme (CSS), UX, navigation, etc. Des débats souvent passionnants, [à suivre en ligne](https://checklists.opquast.com/oqs-v3/workshops/).