---
layout: post
title: "Manifeste pour une expérience utilisateur accessible"
date: "2015-07-09T22:00:00"
comments: true
published: true
categories: 
- flux
tags:
- accessibilité
slug: accessibleux
---
>When we examine accessibility through the lens of user experience, we see that accessibility is:  
- A core value, not an item on a checklist  
- A shared concern, not a delegated task  
- A creative challenge, not a challenge to creativity  
- An intrinsic quality, not a bolted-on fix  
- About people, not technology  
[AccessibleUX.org, Manifesto for Accessible User Experience](http://accessibleux.org/manifesto-for-accessible-user-experience/)

Un manifeste en quatre parties :

- observations ;
- postulats ;
- principes ;
- constats ;
- objectifs.
