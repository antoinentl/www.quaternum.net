---
layout: post
title: "Bonnes pratiques pour l'utilisation des web fonts"
date: "2015-10-12T22:00:00"
comments: true
published: true
categories: 
- flux
slug: bonnes-pratiques-pour-l-utilisation-des-web-fonts
---
>Over the past months there have been a few articles taking care of different font loading optimization techniques. Reading all of them, I ran into a few other issues that are not covered there. Finally, I wanted to have one resource combining the information of the others. Some code snippets are borrowed or adapted from the articles I linked here.  
[Anselm Hannemann, Using Web Fonts The Best Way (in 2015).](https://helloanselm.com/2015/using-webfonts-in-2015/)

Liste des bonnes pratiques pour l'utilisation des *web fonts* : chargement asynchrone, perfectionnement du temps de chargement des fontes, gestion du cache, etc.