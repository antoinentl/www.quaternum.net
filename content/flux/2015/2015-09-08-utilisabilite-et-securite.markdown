---
layout: post
title: "Utilisabilité et sécurité"
date: "2015-09-08T22:00:00"
comments: true
published: true
categories: 
- flux
---
>En sécu­rité, un aspect inté­res­sant est que les mesures prises ont pour objec­tifs de rendre accep­table le niveau de risque — et pas plus. Pour chaque risque iden­ti­fié, on éva­lue sa vrai­sem­blance et sa gra­vité, avant de prendre une mesure pour dimi­nuer son impact. A la fin, il reste des vul­né­ra­bi­li­tés rési­duelles, qu'il suf­fit d'expliciter et de jus­ti­fier : certes, quelqu'un avec un accès phy­sique au sys­tème, une porte déro­bée déjà en place et un super­cal­cu­la­teur de poche pour­rait opé­rer une brèche. Mais c'est un risque accep­table.  
Ce n'est pas très dif­fé­rent d'une démarche ergo, dans laquelle on iden­ti­fie cer­tains déter­mi­nants de l'activité (par exemple, l'utilisateur est forcé d'utiliser sa tablette avec des moufles), aux­quels on répond par des solu­tions (dou­bler la taille des bou­tons) ou des recom­man­da­tions (ne pas uti­li­ser la tablette dans un contexte néces­si­tant ces moufles).  
[Baptiste, De quelques similitudes entre utilisabilité et sécurité](http://toutcequibouge.net/2015/09/de-quelques-similitudes-entre-utilisabilite-et-securite/)

Ce rapprochement entre sécurité et utilisabilité est un moyen simple de casser le mythe de l'interface ou du système parfaitement ergonomique ou sécurisé. Un article très éclairant.

>Il suf­fit d'en reve­nir à l'utilisateur.