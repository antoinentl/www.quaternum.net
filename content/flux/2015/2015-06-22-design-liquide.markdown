---
layout: post
title: "Design liquide et l’unité CSS em"
date: "2015-06-22T22:00:00"
comments: true
published: true
categories: 
- flux
tags:
- design
slug: design-liquide
---
>La volonté très — trop — courante de maîtriser complètement la façon dont les contenus s’affichent sur les écrans, à tel point qu'on parle de « Pixel Perfect », va complètement à l'encontre de cette diversité de supports et de préférences des utilisateurs.  
[Lâchez prise sans perdre le contrôle grâce à l’unité CSS em, Nicolas Hoizey](http://www.24joursdeweb.fr/2013/lachez-prise-sans-perdre-le-controle-grace-a-l-unite-css-em/)

Retour sur cet article publié en décembre 2013 à l'occasion des [24 jours de web](http://www.24joursdeweb.fr/), dont le fond est de promouvoir une certaine façon de concevoir des interfaces web : il faut penser *adaptatif* et non *responsif*. Même s'il est intéressant d'imaginer des points de ruptures dans le design de pages web (selon la taille de l'écran ou le *device*), il faut plutôt designer des interfaces réellement liquides.