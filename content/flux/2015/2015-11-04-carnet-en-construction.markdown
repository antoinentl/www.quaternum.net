---
layout: post
title: "Carnet en construction"
date: "2015-11-04T23:00:00"
comments: true
published: true
categories: 
- flux
---
>Il y a tout à (re)construire, et c’est exitant, vivifiant. Écrire et lancer les fichiers par FTP, comme des petits avions en papier. C’est tellement simple au fond ! Et la métaphore change. Je ne rubrique plus rien, je ne content manage plus rien. Une idée, un fichier, ou comme je veux, je casse et je recommence.  
[Emmanuel, Tout à construire](http://emmanuel.clement.free.fr/2015/11/01/a-construire)

Emmanuel souhaite changer de *CMS* -- quitter Dotclear pour un système sans base de données -- et explique le plaisir d'utiliser uniquement des fichiers HTML et CSS sans dépendre de bases de données ou de dépendances.