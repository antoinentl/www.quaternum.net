---
layout: post
title: "Vietnamese Typography"
date: "2015-11-19T22:00:00"
comments: true
published: true
categories: 
- flux
---
>This book will help type designers understand Vietnamese’s unique typographic features so they can design their typefaces to support the Vietnamese language. It will also guide web and graphic designers in using correct Vietnamese typography in a project.  
[Donny Truong, Vietnamese Typography](https://vietnamesetypography.com/)

Après [Professional Web Typography](https://prowebtype.com/) -- j'en parlais [ici](https://www.quaternum.net/2015/05/12/un-design-de-livre-web/) --, Donny Truong a écrit un livre complet sur la typographie vietnamienne. Encore un *livre web* à la forme très intéressante, et aux contenus précis.