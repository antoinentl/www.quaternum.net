---
layout: post
title: "Projet de loi pour une République numérique"
date: "2015-09-29T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Pour la première fois, après déjà une première phase consultative réalisée l'an dernier par le Conseil national du numérique (CNNum), le texte est publié sur une plateforme participative qui permet à tous les citoyens de donner leur avis sur chacun des articles, d'en proposer des modifications soumises aux avis, ou même de proposer de nouveaux articles dans les différents chapitres du projet de loi.  
[Guillaume Champeau, Loi sur la République Numérique : un bon texte, et une méthode innovante](http://www.numerama.com/magazine/34311-loi-sur-la-republique-numerique-un-bon-texte-et-une-methode-innovante.html)

Un projet de loi qui inclue les remarques, critiques et propositions des citoyens. Une nouveauté, qui le sera réellement lorsque ce mode de fonctionnement concernera aussi des lois qui n'ont rien à voir avec le numérique.  

Pour un résumé très rapide de la loi c'est [par ici](http://www.gouvernement.fr/la-loinumerique-en-9-dessins-dont-un-burger-et-une-fusee-2916).  

Un projet qu'il faut donc commenter et augmenter.