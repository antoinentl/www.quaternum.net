---
layout: post
title: "Brief.me"
date: 2015-06-05
comments: true
published: true
categories: 
- flux
tags:
- média
slug: brief-me
---
>brief.me explique ce qui est complexe, résume ce qui est long, analyse ce qui est important.  
[brief.me](https://www.brief.me/)

Recontextualiser l'information, *designer* un flux qui n'est pas celui des réseaux, remettre le travail de sélection au centre du service. En plus brief.me est graphiquement surperbement réalisé.  
Une démarche à rapprocher de [Fortuitement Trivial](/2015/05/05/fortuitement-trivial)&nbsp;?