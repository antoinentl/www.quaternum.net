---
layout: post
title: "Publicité et Web"
date: "2015-09-14T22:00:00"
comments: true
published: true
categories: 
- flux
---
>J’encourage pour ma part tou·te·s les internautes à bloquer par tous les moyens les publicités qui envahissent certains sites.  
[Clochix, Faut-il bloquer les publicités ?](http://esquisses.clochix.net/2015/09/21/pub/)

Clochix prend le temps de présenter des contre arguments en faveur d'un modèle majoritairement publicitaire des sites Web. Cela fait du bien de lire une liste détaillée qui remet en cause beaucoup de principes perdus depuis les *débuts* du Web.  

**Edit** : Clochix a publié le lendemain un billet de mise à jour.

>Débloquer la publicité sur certains sites, c’est accepter d’être traqué, c’est abaisser la sécurité de son ordinateur, c’est entretenir l’illusion que la publicité est un modèle économique acceptable…