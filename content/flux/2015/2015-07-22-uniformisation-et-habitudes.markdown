---
layout: post
title: "Uniformisation et habitudes"
date: "2015-07-22T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Mais il n'y a rien de mieux qu'un large groupe de geeks dans une conférence, une réunion d'entreprises pour noter combien nous cultivons notre propre uniformité avec exactement les mêmes codes de valeur d'identification.  
[Karl, Uniformité](http://www.la-grange.net/2015/06/24/whistler)

Je profite encore d'un statut *à part* dans le milieu professionnel dans lequel j'évolue actuellement -- le monde du livre ou l'univers du numérique dans le domaine culturel. Cela me permet d'avoir la possibilité d'être dans l'alternative : utiliser d'autres outils que ceux dont disposent habituellement les personnes que je fréquente. Quelques exemples :

- utiliser un ordinateur portable plutôt qu'une tablette ou un *iPhone* ;
- ordinateurs portables qui n'est ni un Mac ni un *PC* sous OS propriétaire ;
- utiliser le papier dans les environnements les plus *geeks* et/ou connectés ;
- etc.

Mais le plus important n'est pas tant dans les outils que dans les pratiques qui y sont liées.