---
layout: post
title: "Raspberry Pi Zero : un ordinateur à 5$"
date: "2015-12-03T22:00:00"
comments: true
published: true
categories: 
- flux
slug: raspberry-pi-zero
---
>Today, I’m pleased to be able to announce the immediate availability of Raspberry Pi Zero, made in Wales and priced at just $5. Zero is a full-fledged member of the Raspberry Pi family [...].  
[Eben Upton, Raspberry Pi Zero: the $5 computer](https://www.raspberrypi.org/blog/raspberry-pi-zero/)

Un ordinateur à moins de 5 euros ? Le nouveau modèle de Raspberry Pi, le Raspberry Pi Zero, permet d'imaginer des usages encore plus massifs. Il reste encore les questions de l'apprentissage, de la *fracture numérique*, de la volonté et du temps disponibles pour utiliser un matériel peu coûteux en argent mais pas en temps. Un mouvement à accompagner.