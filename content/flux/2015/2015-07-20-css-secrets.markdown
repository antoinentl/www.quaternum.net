---
layout: post
title: "CSS Secrets"
date: "2015-07-20T22:00:00"
comments: true
published: true
categories: 
- flux
---
>In this brilliant guide, Lea Verou unveils the rare power of CSS that lets designers create stunning visual effects beyond the popular elements. With her extensive knowledge of CSS and her clarity in technical explanation, Verou provides practical solutions that are easy to achieve with just a few lines of CSS.  
[Donny Truong, Lea Verou: CSS Secrets](https://www.visualgui.com/2015/07/07/lea-verou-css-secrets/)

Il semble que [cet ouvrage](http://shop.oreilly.com/product/0636920031123.do) soit de plus en plus recommandé. L'enthousiasme de Donny Truong -- et de [Willy](http://insitu-collective.com/) en dehors des réseaux -- suscite en moi un intérêt qui dépasse la curiosité.