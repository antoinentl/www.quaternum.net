---
layout: post
title: "Des sites web plus légers et plus rapides"
date: "2015-12-23T22:00:00"
comments: true
published: true
categories: 
- flux
---
>So, let’s talk about websites—but not the way I’d talk about furniture, if you gave me half a chance. This is about the user that just wants to keep their coffee off the floor. They don’t care about **the website**. They don’t care about frameworks; they don’t care about browsers. They want their information and they want to get out. A website, to them, is its purpose.  
[Mat "Wilto" Marquis, Smaller, Faster Websites](https://bocoup.com/weblog/smaller-faster-websites)

Après une introduction à base de généralités percutantes et de chiffres précis -- notamment sur les usages *desktop* et mobiles, ou sur les qualités des connexions--, Mat "Wilto" Marquis propose beaucoup de méthodes -- très concrètes -- pour gagner en légèreté et en rapidité.

>We build a connection between every single person in the world and all the information in the world. We do more than just build websites—I don’t *care* about websites. I care about *purpose*.