---
layout: post
title: "websites are in permanent beta"
date: "2015-06-29T22:00:00"
comments: true
published: true
categories: 
- flux
tags:
- design
---
>websites are in permanent beta.  
[ILT 2015, John Boardley citant Edo van Dijk](http://ilovetypography.com/2015/06/29/ilt-2015/)

I Love Typography vient de *refondre* son site, John Boardley explique ses choix et surtout le changement de paradigme : ne plus faire de grands boulversements en terme de structure et de design, mais modifier continuellement le site.  
À noter que [I Love Typography](http://ilovetypography.com/) est un bon exemple de site thématique géré (articles, éditorialisation, développement, correspondances, etc) par une seule personne (du boulot, donc).