---
layout: post
title: "Prospective sur le développement web"
date: "2015-08-04T22:00:00"
comments: true
published: true
categories: 
- flux
slug: prospective-web-developpement
---
>You can’t make a decision on how to simplify your approach to web development—how to make it saner—without understanding the compromises, which by definition means that you already have to be a web development expert. Beginners are screwed.  
[Baldur Bjarnason, Iterating the web away: losing the next generation](https://www.baldurbjarnason.com/notes/iterating-the-web-away/)

Les articles qui pointent la complexification du développement web sont nombreux, mais Baldur Bjarnason aborde des questions intéressantes, notamment celle de l'intérêt de s'amuser et de créer.

>Web development will (or even has) become the boring thing you learn because your work needs you to.