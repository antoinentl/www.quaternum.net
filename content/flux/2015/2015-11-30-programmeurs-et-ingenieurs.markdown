---
layout: post
title: "Programmeurs et ingénieurs"
date: "2015-11-30T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Dans un stimulant billet publié sur le The Atlantic, le développeur de jeux, Ian Bogost s’est encore fait des amis. Pour lui, les programmeurs ne sont pas des ingénieurs. Pourquoi ? Parce que les ingénieurs ont, eux, une longue tradition de conception et de construction d’infrastructures d’intérêt public ! Les ingénieurs ont une responsabilité de sécurité publique et de fiabilité (même si elle fait parfois défaut). Ce n’est pas le cas de ceux qui font des logiciels explique-t-il en pointant le piratage massif de données personnelles, les logiciels tricheurs…  
[À lire ailleurs, Programmeurs : vous n’êtes pas des ingénieurs !](http://alireailleurs.tumblr.com/post/132925379481/programmeurs-vous-n%C3%AAtes-pas-des-ing%C3%A9nieurs)