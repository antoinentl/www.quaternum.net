---
layout: post
title: "Compte rendu de BlendWebMix : UX design"
date: "2015-11-09T22:00:00"
comments: true
published: true
categories: 
- flux
slug: compte-rendu-de-blend-web-mix-ux-design
---
>Si la diversité des conférences du Blend et l’étendue des locaux ont pu me faire tourner la tête, je me suis finalement concocté un programme mêlant web design, mobilité et expérience utilisateur (UX).  
[Marie Guillaumet, BlendWebMix 2015 : l’expérience utilisateur à l’honneur (1)](http://residence-mixte.com/blendwebmix-2015-l-experience-utilisateur-a-l-honneur-1/)

Ayant suivi deux des trois conférences présentées dans ce compte rendu, je ne peux que noter les qualités de synthèse de Marie Guillaumet ! À lire !