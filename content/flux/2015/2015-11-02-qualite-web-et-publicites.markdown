---
layout: post
title: "Qualité web et publicités"
date: "2015-11-02T22:00:00"
comments: true
published: true
categories: 
- flux
---
>La semaine dernière, je donnais une conférence intitulée : alerte rouge sur la qualité du Web chez BlendWebMix, à Lyon. Bien entendu, dans le tour d'horizon que j'ai fait sur les nombreuses dégradations actuelles de l'expérience utilisateur, je ne pouvais pas passer sous silence les problèmes liés à la publicité en ligne. Et ils sont énormes et graves, ces problèmes.  
[Élie Sloïm, Publicité Web : y'a des limites ?](http://blog.temesis.com/post/2015/11/02/Publicite-Web-y-a-des-limites)

Les problèmes liés à la qualité web posés par les publicités en ligne sont nombreux et complexes (Élie Sloïm en a parlé [avec panache](http://www.blendwebmix.com/programme/alerte-rouge-sur-la-qualite-du-web.html) à Blend Web Mix), les réponses sont encore timides, et peu convaincantes pour ceux qui utilisent la publicité. Mais il y a des pistes et il faudra bien réagir -- modification ou suppression partielle ou totale de la publicité sur le Web -- face à l'utilisation massive des bloqueurs de publicité.