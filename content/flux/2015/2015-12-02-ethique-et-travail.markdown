---
layout: post
title: "Travail et éthique"
date: "2015-12-02T22:00:00"
comments: true
published: true
categories: 
- flux
slug: ethique-et-travail
---
>Chacun fixera lui-même la limite entre compromis et compromission, et je ne me risquerai certainement pas à juger la position du curseur des uns et des autres. Le tout étant de le faire avec honnêteté, et surtout avec logique.  
[STPo, L’éthique dans le travail](http://www.stpo.fr/blog/lethique-dans-le-travail/)

Entre la recherche d'une certaine ligne éthique, et la stratégie économique inhérente à toute activité professionnelle, STPo décrit avec beaucoup de justesse le difficile parcours dans les choix des projets -- autant en indépendant qu'en tant que salarié.

>[...] si j’ai conscience de mes contradictions, je tente de tenir un discours cohérent et de ne pas changer d’avis avec le sens du vent ou de faire tout et son contraire.