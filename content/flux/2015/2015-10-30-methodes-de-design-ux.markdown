---
layout: post
title: "Méthodes de design UX"
date: "2015-10-30T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Articulant théorie et pratique, cet ouvrage présente 30 fiches méthodologiques couvrant l'essentiel du design UX et de l'ergonomie des interactions homme-machine (IHM).  
[Carine Lallemand et Guillaume Gronier, Méthodes de design UX](http://www.editions-eyrolles.com/Livre/9782212141436/methodes-de-design-ux)

Le livre de Carine Lallemand et Guillaume Gronier vient de sortir. Après avoir écouté [la formidable conférence de Carine Lallemand à Blend Web Mix](http://www.blendwebmix.com/programme/ux-design-et-si-la-cle-du-succes-se-trouvait-dans-les-theories-sur-l-ux.html), je pense que cet ouvrage vaut vraiment la peine d'être lu !