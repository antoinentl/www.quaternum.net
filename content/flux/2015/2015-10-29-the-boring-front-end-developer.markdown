---
layout: post
title: "The boring front-end developer"
date: "2015-10-29T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Cool front-end developers are always pushing the envelope, jumping out of their seat to use the latest and greatest and shiniest of UI frameworks and libraries. They are often found bridging the gap between native apps and web and so will strive to make the UI look and behave like an app. Which app? you may ask. iPhone? Android? What version? All good questions, alas another topic altogether. However, there is another kind of front-end developer, the boring front-end developer. Here is an ode to the boring front-end developer, BFED if you will.  
[Adam Silver, The boring front-end developer](http://thebfed.com/)

Je découvre ce *manifeste* via [Vincent Valentin](http://vincent-valentin.name/). [Adam Silver](http://adamsilver.io/) y défend la simplicité, l'accessibilité, la résilience ou encore la robustesse, face aux gadgets, aux dépendances techniques et aux interfaces pas ou peu accessibles.