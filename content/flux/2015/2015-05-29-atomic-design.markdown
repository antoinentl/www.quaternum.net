---
layout: post
title: "Atomic Design"
date: 2015-05-29
comments: true
published: true
categories: 
- flux
tags:
- livre
---
>Atomic design is methodology for creating design systems. There are five distinct levels in atomic design:  
1. Atoms  
2. Molecules  
3. Organisms  
4. Templates  
5. Pages  
[Atomic Design, Brad Frost](http://bradfrost.com/blog/post/atomic-web-design/)

Cet article a été publié en octobre 2013, depuis plusieurs mois Brad Frost écrit [un livre sur le *design atomique*](http://atomicdesign.bradfrost.com/) -- encore un [livre web](https://www.quaternum.net/2015/05/12/un-design-de-livre-web/). Il s'agit plutôt d'une approche métier du design web, très intéressante en terme de conception.  

À lire également, l'article de Thibault Mahé, [Design atomique et design de styleguides](http://thibault.mahe.io/journal/article25/design-atomique-et-design-de-styleguides).