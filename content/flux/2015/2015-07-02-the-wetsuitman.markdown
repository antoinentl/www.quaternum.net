---
layout: post
title: "The Wetsuitman"
date: "2015-07-02T22:00:00"
comments: true
published: true
categories: 
- flux
tags:
- design
---
>Without a Trace  
[The Wetsuitman, Anders Fjellberg](http://www.dagbladet.no/spesial/vatdraktmysteriet/eng/)

Outre le sujet de l'article, ce qui est intéressant c'est le format/forme : simple, sur une seule page, très lisible, avec une navigation très identifiable -- découpage de l'article en trois parties et non trois pages. Tout cela correspond bien au format/fond de l'enquête ou du récit documenté.