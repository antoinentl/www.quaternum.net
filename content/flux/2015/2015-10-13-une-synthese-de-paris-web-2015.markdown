---
layout: post
title: "Une synthèse de Paris Web 2015"
date: "2015-10-13T22:00:00"
comments: true
published: true
categories: 
- flux
---
>C’est tous les ans un moment attendu avec une grande impatience : Paris Web. Cette conférence francophone des gens qui font le Web abordent des sujets qui nous tiennent à coeur : l’accessibilité du Web, le design numérique et les standards ouverts.  
[Olivier Keul, Paris Web 2015 : 10 ans déjà !](http://blog.clever-age.com/fr/2015/10/08/paris-web-2015-10-ans-deja/)

Olivier Keul propose le résumé de quatre conférences de Paris Web 2015.