---
layout: post
title: "Guide CSS"
date: "2015-09-21T08:00:00"
comments: true
published: true
categories: 
- flux
---
>Peu importe le document, il faut toujours essayer de garder un formatage commun. Cela signifie une cohérence des commentaires, de la syntaxe et des règles de nommage.  
[Harry Roberts, traduit par David Leuliette, Guide CSS)](http://guidecss.fr/)

Je découvre, via [David](https://larlet.fr/david/), ce *Guide CSS*, conçu essentiellement pour développer à plusieurs, en 18 points clairs et concis.