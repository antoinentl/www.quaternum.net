---
layout: post
title: "Résilience du Web"
date: "2015-10-08T22:00:00"
comments: true
published: true
categories: 
- flux
---
>je pense que nous devons pousser pour le développement du Web mais vers une démarche d'amélioration sur la résilience, résistance, stabilité. Le Web est fragile, la complexification est un concept bien différent que celui du progrès. [...] Comment développer le futur du Web afin qu'il soit plus performant dans sa résistance au temps, dans sa facilité d'utilisation ?  
[Karl, Arrêtez le Web](http://www.la-grange.net/2015/08/04/stop)