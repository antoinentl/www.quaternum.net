---
layout: post
title: "Interface Writing: Code for Humans"
date: "2015-06-18T22:00:00"
comments: true
published: true
categories: 
- flux
tags:
- design
slug: interface-writing
---
>I signed up for their trial and they sent me a link to their mobile app. But instead of talking about themselves or explaining the features, they’re helping me get right to it. They say, “Why are you still reading this email? Download the app and dive into your first book!” I love that.  
[Interface Writing: Code for Humans, Nicole Fenton](http://nicolefenton.com/interface-writing/)

Il s'agit du contenu d'une présentation de Nicole Fenton, à propos des *instructions* et de leurs différentes formes sur le web. "The web and the world are full of instructions. Everywhere you go, signs and people are telling you what to do." En lisant ce que Nicole Fenton explique très clairement, on comprend pourquoi des services comme [Capitaine Train](https://www.capitainetrain.com/) ont réussi leur design de service. Un bon rappel des mauvais exemples et des bonnes pratiques.

Source : [le numéro 94](https://wdrl.info/archive/94/) de l'excellente veille *Web Development Reading List*.