---
layout: post
title: "L'efficience du Web"
date: "2015-07-10T22:00:00"
comments: true
published: true
categories: 
- flux
tags:
- design
slug: l-efficience-du-web
---
>Robustness and adaptability are a direct consequence of simplicity and loose coupling. Lose those two features and things start breaking.  
[Baldur Bjarnason](https://twitter.com/fakebaldur/status/619525386947461121)

Baldur Bjarnason rappelle en quelques *tweets* ce que nous devrions envisager lorsque nous fabriquons des sites web, et que nous souhaitons les fabriquer pour qu'ils soient *robustes*.

>The web we are creating is just as impermanent as old apps or docs. Robustness wasn't a feature of HTML but of what we were doing with HTML.