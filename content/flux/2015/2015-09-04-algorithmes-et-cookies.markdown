---
layout: post
title: "Algorithmes et cookies"
date: "2015-09-04T22:01:00"
comments: true
published: true
categories: 
- flux
---
>Sur les cinq sites d’Arte, revendique sa présidente, "la recommandation n’est pas automatique mais éditoriale et réfléchie".  
[Alexis Delcambre, "Quand Arte veut 'résister' aux géants du Web"](http://www.lemonde.fr/actualite-medias/article/2015/08/24/quand-arte-veut-resister-aux-geants-du-web_4735407_3236.html)

Une démarche, sur le web, à contre courant des autres chaînes. [La page de présentation des cookies](http://www.arte.tv/sites/fr/services/arte-et-les-cookies/) utilisés par Arte démontre cette volonté de transparence. Reste qu'Arte joue sur le même terrain que les "géants du Web", et qu'il semble donc difficile de gagner à moins de changer les règles.