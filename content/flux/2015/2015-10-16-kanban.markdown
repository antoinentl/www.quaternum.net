---
layout: post
title: "Kanban"
date: "2015-10-16T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Yet this is largely unknown outside of software development. All sorts of industries would benefit from this approach, from farming to law.  
[Daniel Pope, Get a Kanban! (or Scrum board)](http://mauveweb.co.uk/posts/2015/08/get-a-kanban.html)

Une méthode assez simple à mettre en place, dans tous les domaines qui peuvent intégrer un travail par itérations.  
À noter que le service [Framaboard](https://framaboard.org/) de Framasoft est basé sur Kanban.