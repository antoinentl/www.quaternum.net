---
layout: post
title: "Webkit et Edge"
date: "2015-11-07T23:00:00"
comments: true
published: true
categories: 
- flux
---
>Mais Edge considère désormais les préfixes `-webkit-` comme des *aliases*, et les mange tout crus. Ça aurait pu être un problème dans ce cas précis, puisque théoriquement la paire `background-clip: text;` nʼest reconnue que par les navigateurs basés sur WebKit. Là encore : surprise ! Edge lʼapplique sans rechigner.  
[Gaël Poupard, Le web en kit](http://www.ffoodd.fr/le-web-en-kit/)

Microsoft revient sur le devant de la scène web -- après la longue agonie d'Internet Explorer --, mais cela semble plutôt positif. À suivre.

>De nombreuses techniques ont vu le jour et nʼont vécu que pour WebKit. Je mʼinterroge donc : parmi ces techniques que nous pensons réservées à WebKit, combien dʼautres encore ont atterri dans Edge discrètement ?