---
layout: post
title: "La différence entre des mockups et la réalité"
date: "2015-11-11T22:00:00"
comments: true
published: true
categories: 
- flux
---
>It's cool to show beautiful things to make a good impression, but if we want to be closer to the user interaction, we should probably test UIs in different contexts. Be site which are not mobile friendly, sites which are ugly, sites with Web compatibility issues. I have the feeling that would help to design not only beautiful UIs but also UIs more resilient in a frustration or uncomfortable mood of the user.  
[Karl Dubost, Interface Mockup and User Reality](http://www.otsukare.info/2015/10/22/interface-mockup)

Les *mockups* -- ou maquettes d'interfaces -- sont parfois loin, très loin de la réalité. Il est bon de rappeler la majeure partie du web n'a pas de design épuré !