---
layout: post
title: "Présenter Markdown"
date: 2015-05-21
comments: true
published: true
categories: 
- flux
tags:
- structuration
---
>What we really need is a great Markdown tutorial and reference page, one that we can refer anyone to, anywhere in the world, from someone who barely touches computers to the hardest of hard-core coders. I don't want to build another one for these kinds of help pages for Discourse, I want to build one for everyone. Since it is for everyone, I want to involve everyone. And by everyone, I mean you.  
[Toward a Better Markdown Tutorial, Jeff Atwood](http://blog.codinghorror.com/toward-a-better-markdown-tutorial/)

L'enjeu autour de Markdown est bien d'expliquer *simplement* comment il fonctionne et surtout qu'est-ce qu'il peut apporter. Et il ne faut pas l'expliquer qu'aux *connaisseurs*.