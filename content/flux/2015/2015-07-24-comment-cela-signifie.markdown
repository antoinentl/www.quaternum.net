---
layout: post
title: "Comment cela signifie"
date: "2015-07-24T22:00:00"
comments: true
published: true
categories: 
- flux
---
>L'attention s'est déplacée des intentions de l'auteur aux fonctionnements internes de l'écriture : c'est-à-dire non pas ce que cela signifie mais comment cela signifie.  
[Michael Rock, Le designer comme auteur, in *n+1*, page 41](http://www.citedudesign.com/fr/editions/260712-n-1)

