---
layout: post
title: "Une synthèse de Paris Web 2015 (2)"
date: "2015-10-14T22:00:00"
comments: true
published: true
categories: 
- flux
slug: une-synthese-de-paris-web-2015
---
>Paris Web, ce sont des dizaines de conférences très enrichissantes animées par des professionnels passionnés. C’est aussi l’occasion de rencontrer des gens que l’on suit sur Twitter ou dont on lit les blogs, en vrai. C’est un peu une cure de santé annuelle.  
[Mylène Chandelier, Paris Web 10e édition](http://www.wax-interactive.com/fr-ch/paris-web-10eme-edition-wax-interactive-suisse-y-a-assiste/)

Une synthèse de **toutes** les conférences de Paris Web 2015 ! C'est parfois très succint, mais cet article de Mylène Chandelier semble donner une bonne vision de l'ensemble de l'événement.