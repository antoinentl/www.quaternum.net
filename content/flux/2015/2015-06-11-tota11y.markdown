---
layout: post
title: "tota11y"
date: "2015-06-11T22:00:00"
comments: true
published: true
categories: 
- flux
tags:
- accessibilité
---
>an accessibility visualization toolkit  
[tota11y](http://khan.github.io/tota11y/)

tota11y est un outil -- un fichier Javascript qui prend la forme d'un script à intégrer dans une page ou un bookmarklet à ajouter dans son navigateur -- permettant de vérifier certains points précis de l'accessibilité web :

- headings ;
- contrast ;
- link text ;
- labels ;
- image alt-text ;
- landmarks (les labels utilisés pour <abbr title="Accessible Rich Internet Applications">ARIA</abbr>).