---
layout: post
title: "Responsive vs adaptative"
date: "2015-05-12T22:00:00"
comments: true
published: true
categories: 
- flux
tags:
- design
---
>Put simply, responsive is fluid and adapts to the size of the screen no matter what the target device. Responsive uses CSS media queries to change styles based on the target device such as display type, width, height etc., and only one of these is necessary for the site to adapt to different screens.  
Adaptive design, on the other hand, uses static layouts based on breakpoints which don’t respond once they’re initially loaded. Adaptive works to detect the screen size and load the appropriate layout for it – generally you would design an adaptive site for six common screen widths [...].  
[Responsive vs. Adaptive Design: What’s the Best Choice for Designers?, Jerry Cao sur UXPin](http://blog.uxpin.com/6439/responsive-vs-adaptive-design-whats-best-choice-designers/)

La différence essentielle entre les designs *responsif* et *adaptatif* réside donc dans les points de rupture. Le design *responsif* est donc ce qui a de plus *liquide*.