---
layout: post
title: "Un point sur la publicité en ligne"
date: "2015-11-03T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Reste une morale à ne pas oublier : la publicité est une problématique créée de toute pièce par le refus de considérer que les contenus ont un coût. Nous arrivons aux limites du système, et nous arriverons aussi un jour aux limites des autres systèmes pseudo-gratuits, comme les réseaux sociaux.  
Si vous le pouvez, préférez toujours des business model basés sur la plus-value de vos contenus plutôt que sur leur proposition à bas coût, ou sans frais : vous envoyez un mauvais signal et il est dur d’en sortir a posteriori.  
[Boris Schapira, Publicité en ligne : un point sur la situation](https://borisschapira.com/2015/10/publicite-en-ligne-un-point/)

Une bonne synthèse de la situation concernant la publicité en ligne : un historique nécessaire, un avenir possiblement centralisé peu réjouissant, quelques pistes innovantes explorées, et un bon conseil sur la stratégie.