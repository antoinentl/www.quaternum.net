---
layout: post
title: "Minimalisme et web design"
date: "2015-07-08T22:00:00"
comments: true
published: true
categories: 
- flux
tags:
- design
---
>Unfortunately, some designers misinterpret minimalism as a purely visual-design strategy. They cut or hide important elements in pursuit of a minimalist design for its own sake—not for the benefits that strategy might have for users. They’re missing the core philosophy and the historical context of minimalism, and they risk increasing complexity rather than reducing it.  
[Kate Meyer, The Roots of Minimalism in Web Design](http://www.nngroup.com/articles/roots-minimalism-web-design/)

Le minimalisme, s'il est pris comme un style et non comme un principe, et particulièrement dans le domaine du *web design*, peut créer des interfaces mal pensées et complexes à utiliser. Ce n'est pas forcément le meilleur choix, tout dépend du projet.