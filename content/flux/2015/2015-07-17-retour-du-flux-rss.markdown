---
layout: post
title: "Retour du flux RSS ?"
date: "2015-07-17T22:00:00"
comments: true
published: true
categories: 
- flux
tags:
- standards
---
>There’s one thing that’s very different this time around for RSS and Atom and it’s the reason why this time it might be different. Back then ‘just the HTML, no CSS, JS, or Flash’ meant nothing more than rich text with images.  
Now, ‘just the HTML’ means rich text, video, audio, SVG, and more.  
[Baldur Bjarnason, The rules of the game have changed for RSS](https://www.baldurbjarnason.com/notes/the-rules-have-changed-for-rss/)

Serait-ce le retour du flux RSS/Atom dans les usages ? La progression des standards des technologies du Web -- HTML, CSS, SVG, no-Flash, évolution des navigateurs -- semble porter ses fruits ! Désormais un flux RSS/Atom peut être riche -- images, SVG, vidéo et audio -- et pas seulement limiter à du texte et des images.