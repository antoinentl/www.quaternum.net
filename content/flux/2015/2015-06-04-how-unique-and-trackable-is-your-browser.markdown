---
layout: post
title: "How Unique - and Trackable - Is Your Browser"
date: 2015-06-04
comments: true
published: true
categories: 
- flux
tags:
- outils
---
>Is your browser configuration rare or unique? If so, web sites may be able to track you, even if you limit or disable cookies.  
[Panopticlick](https://panopticlick.eff.org/)

L'utilisation des *cookies* n'est donc pas forcément la technique utilisée pour tracker les internautes, le simple fait de savoir quelle est la configuration de votre navigateur web y suffit.