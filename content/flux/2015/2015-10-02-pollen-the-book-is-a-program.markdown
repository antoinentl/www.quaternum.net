---
layout: post
title: "Pollen : the book is a program"
date: "2015-10-02T22:00:00"
comments: true
published: true
categories: 
- flux
---
>The book is a program.  
[Matthew Butterick, Pollen](http://pollenpub.com/)

Je découvre tardivement que [Practical Typography](http://practicaltypography.com/) est un livre web construit avec Pollen, un outil permettant de structurer et de mettre en ligne des contenus sous forme de livre (web).