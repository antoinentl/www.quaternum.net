---
layout: post
title: "Développez des systèmes"
date: "2015-11-05T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Plutôt que de tâtonner pendant des heures, prenez deux secondes pour créer un système. Quelque chose de très direct, qui repose sur 2 ou 3 réglages/variations et est susceptible de vous amener à un résultat intéressant.  
Sans forcément vous en rendre compte, vous créez des systèmes en permanence. Il y a juste que vous ne les réutilisez et remixez pas. Pourtant, un tout petit changement peut vous amener sur un résultat totalement différent.  
[Jiminy Panoz, Développez des systèmes](http://jiminy.chapalpanoz.com/systemes/)

Vivifiant billet de Jiminy Panoz, *ebook designer*, qui propose une méthode pour résoudre des problèmes récurrents de design -- mais qui peuvent être également techniques. Je me rends compte que j'ai une pratique similaire lors de la phase de conception d'un projet.

>Ces systèmes sont à envisager comme des aides, des bases sur lesquelles construire quelque chose de solide.  
[...]  
Alors créez des systèmes, soyez totalement obnubilés par l’idée de les développer, n’imaginez pas concevoir autrement un seul instant et ensuite seulement, ajoutez-y votre touche personnelle.