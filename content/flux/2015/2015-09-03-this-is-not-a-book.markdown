---
layout: post
title: "This is not a book"
date: "2015-09-03T22:00:00"
comments: true
published: true
categories: 
- flux
---
>This is not a book is a series of observations on writing in a digital age written by Baldur Bjarnason and Tom Abba. It is our attempt to boil down into a manageable website our combined experience in creating, developing, and teaching interactive media. It’s a deliberately opinionated guide to the current landscape of digital media storytelling and writing.  
[Baldur Bjarnason, Launching "This is not a book" – what it is and why you should be interested](https://www.baldurbjarnason.com/notes/launching-thisisnotabook/)

Sous-titrée "Writing in the age of the web", [This is not a book](http://thisisnotabook.baldurbjarnason.com/) est une synthèse, une reformulation et une remise en contexte des connaissances et des expériences de [Baldur Bjarnason](https://www.baldurbjarnason.com/) et [Tom Abba](http://tomabba.com/) dans le domaine du *livre numérique* et des *médias interactifs*. Une lecture, semble-t-il, à ne pas rater !

>an old-fashioned hypertext about writing for digital

Par ailleurs le site web est simplissime mais très bien fait.