---
layout: post
title: "Letterspacing Type"
date: "2015-09-05T17:00:00"
comments: true
published: true
categories: 
- flux
---
>Effectively letterspacing text can make the difference between good typography and *great* typography.  
[John D. Jameson, Guidelines for Letterspacing Type](http://johndjameson.com/blog/guidelines-for-letterspacing-type/)

Une bonne introduction en ce qui concerne l'espacement des lettres pour les sites web, à prolonger.