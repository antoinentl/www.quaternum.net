---
layout: post
title: "Do you have something to hide?"
date: "2015-06-19T22:00:00"
comments: true
published: true
categories: 
- flux
tags:
- privacy
---
>Do you know that you are being watched?  
[ihavesomethingtohi.de](https://ihavesomethingtohi.de/)

Ce qui est intéressant dans ce projet, ce n'est pas tant les outils qui sont présentés que la façon dont cela est proposé : temps estimé pour l'installation et la configuration, niveau de difficulté, apport, inconvénients.