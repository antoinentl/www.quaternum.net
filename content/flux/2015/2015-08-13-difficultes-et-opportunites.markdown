---
layout: post
title: "Difficultés et opportunités"
date: "2015-08-13T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Il [le designer] doit pouvoir évaluer avec objectivité les difficultés tout comme les opportunités d'après leurs caractéristiques (et selon sa propre expérience), mais également faire des choix, ajuster et arrêter ses décisions en conséquence.  
[Norman Potter, *Qu'est-ce qu'un designer*, page 21](http://editions-b42.com/books/quest-ce-quun-designer/)

Une définition du designer très concise.