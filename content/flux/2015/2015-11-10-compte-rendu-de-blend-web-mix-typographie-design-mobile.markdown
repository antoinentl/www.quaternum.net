---
layout: post
title: "Compte rendu de BlendWebMix : typographie, design et mobile"
date: "2015-11-10T22:00:00"
comments: true
published: true
categories: 
- flux
slug: compte-rendu-de-blend-web-mix-typographie-design-mobile
---
>Certes, la dimension « business » et l’ambiance « start-up » de l’évènement peut faire un peu peur aux non-initiés (c’était mon cas) ; mais une fois sur place, la diversité des sujets proposés permet sans mal de s’en sortir en échappant au jargon franglais et aux (rares) discours marketing à papa.  
Toutefois, l’intérêt du Blend, c’est justement ce mélange des genres un peu improbable entre designers, développeurs, entrepreneurs, investisseurs et chercheurs. La diversité des conférenciers et des participants crée une alchimie très originale.  
Cela aide à sortir un peu de son pré carré et à s’ouvrir à des sujets qui ne nous sont pas forcément familiers.  
[Marie Guillaumet, BlendWebMix 2015 : l’expérience utilisateur à l’honneur (2)](http://residence-mixte.com/blendwebmix-2015-l-experience-utilisateur-a-l-honneur-2/)

Deuxième partie du compte rendu de Marie Guillaumet concernant les deux jours de conférences de BlendWebMix, au programme : typographie, chargement et performances ; design de produit dans le cas d'un service qui touche *beaucoup* de personnes ; et conception d'une navigation sur mobile.