---
layout: post
title: "DOM et HTML"
date: "2015-11-17T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Contrairement à ce que laisse penser la définition erronée du HTML (« langage de description de contenu »), la page affichée n’est qu’un sous-produit du DOM, qui est le véritable résultat du HTML.  
Et cette nuance change tout.  
[...]  
Une soupe de `<div>`, ce n’est pas juste quelques octets en trop dans le HTML. C’est une structure pesante, inutile, qui va alourdir toutes les opérations de *reflow* et *repaint*.  
[...]  
Comprendre le DOM et son fonctionnement est donc un prérequis essentiel pour créer des pages performantes et économes en ressources.  
[Olivier Nourry, Le DOM, le coté lumineux du HTML](http://accessiblog.fr/2015/11/dom-cote-lumineux-html/)

À la source du web, donc.