---
layout: post
title: "World Brain, un documentaire"
date: "2015-12-17T22:00:00"
comments: true
published: true
categories: 
- flux
---
>World Brain traite de l’architecture des data centers, de l'intelligence collective des chatons, du trading à haute fréquence, de la survie dans la forêt à l’ère de Wikipédia, du bricolage de rats transhumanistes…  
[Arte Creative, World Brain](http://worldbrain.arte.tv/)

World Brain, le documentaire Stéphane Degoutin et Gwenola Wagon diffusé sur Arte Creative, est à regarder ! On y découvre le fonctionnement du réseau Internet, des recherches prospectives sur les connexions et l'intelligence artificielle, des démarches disruptives, etc.