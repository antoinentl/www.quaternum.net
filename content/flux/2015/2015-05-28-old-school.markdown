---
layout: post
title: "Old school"
date: 2015-05-28
comments: true
published: true
categories: 
- flux
tags:
- design
---
>Put down the jQuery, step away from the non-relational database: we have more important things to talk about.  
[Only 90s Web Developers Remember This, Zach Holman](http://zachholman.com/posts/only-90s-developers/)

Une plongée dans l'intégration web des années 1990 : du bricolage ? Non, plutôt des moyens ingénieux de contourner les limites des navigateurs.