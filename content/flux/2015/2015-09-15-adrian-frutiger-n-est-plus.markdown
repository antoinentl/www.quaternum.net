---
layout: post
title: "Adrian Frutiger n'est plus"
date: "2015-09-15T22:00:00"
comments: true
published: true
categories: 
- flux
slug: adrian-frutiger-n-est-plus
---
>He was one of the few typographers whose worked with hot metal, photographic and digital typesetting during his long career.  
[swissinfo.ch](http://www.swissinfo.ch/eng/master-of-the-univers_swiss-font-legend-adrian-frutiger-dies/41659284)

Je connais mal le travail d'Adrian Frutiger, mais les quelques images du maître aperçues sur des vidéos à l'occasion des Rencontres de Lure m'ont permis d'entrevoir un typographe aux méthodes originales, un homme plein de créativité.