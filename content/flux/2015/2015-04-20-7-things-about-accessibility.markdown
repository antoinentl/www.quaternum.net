---
layout: post
title: "7 Things Every Designer Needs to Know about Accessibility"
date: 2015-04-20
comments: true
published: true
categories: 
- flux
tags:
- accessibilité
slug: 7-things-about-accessibility
---
>1. Accessibility is not a barrier to innovation.  
2. Don’t use color as the only visual means of conveying information.  
3. Ensure sufficient contrast between text and its background.  
4. Provide visual focus indication for keyboard focus.  
5. Be careful with forms.  
6. Avoid component identity crises.  
7. Don’t make people hover to find things.
[Jesse Hausler, Medium](https://medium.com/salesforce-ux/7-things-every-designer-needs-to-know-about-accessibility-64f105f0881b)

La plupart de ces *principes* peuvent être appliqués en dehors de toute considération *accessible*, pour tout projet *inclusif*.

Mise à jour : [une traduction, en français, de cet article sur Medium France](https://medium.com/france/7-choses-que-tout-designer-doit-savoir-sur-l-accessibilit%C3%A9-822584593d4d#.lhv8yslqw).