---
layout: post
title: "Let Links Be Links"
date: "2015-06-12T22:00:00"
comments: true
published: true
categories: 
- flux
tags:
- standards
---
>People consume websites through apps like Pocket or Instapaper, which try to use the structured information of a web page to extract its relevant content. A browser on a smartwatch might ignore your layout and present your information in a way that’s more suitable for a one-inch screen.  
[Let Links Be Links, Ross Penman](http://alistapart.com/article/let-links-be-links)

Depuis plusieurs mois/semaines, les articles qui font l'éloge d'un web structuré, standard, accessible, sont de plus en plus nombreux. Contenus pas ou peu structurés ? Sites web trop gourmands en JavaScript ? C'est l'assurance d'être moins visible, et moins lisible.