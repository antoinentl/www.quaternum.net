---
layout: post
title: "L'origine du design"
date: "2015-07-27T22:00:00"
comments: true
published: true
categories: 
- flux
slug: l-origine-du-design
---
>Il ne s'agit pas de substituer une formule par une autre mais de remplacer ce qui se constitue comme une réponse -- le graphisme d'utilité publique -- par des questions -- quelle utilité ? comment ? pourquoi ? Autrement dit, il faudrait parvenir à quitter un cadre de pensée qui fit la force, l'efficacité et la légitimité du design historique, pour revenir aux questions à l'origine même du design.  
[Annick Lantenois, *Le vertige du funambule*, page 39](http://editions-b42.com/books/vertige-du-funambule/)