---
layout: post
title: "Anatomy of a Typeface"
date: "2015-06-20T22:00:00"
comments: true
published: true
categories: 
- flux
tags:
- typographie
---
>Baseline : The invisible line where letters sit.  
[Anatomy of a Typeface, Typedia](http://typedia.com/learn/only/anatomy-of-a-typeface/)

Une ressource classique mais bien pratique pour ne oublier les classiques !