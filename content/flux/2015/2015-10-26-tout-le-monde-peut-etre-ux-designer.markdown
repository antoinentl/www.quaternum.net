---
layout: post
title: "Tout le monde peut être UX designer ?"
date: "2015-10-26T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Alors, non, je ne sais pas ce qui se passe dans le cerveau de tel ou tel utilisateur, pas plus que je ne vois l’activité bactériologique dans le levain. Les chercheurs le peuvent, dans les deux cas, avec des outils adaptés. Moi, je ne peux qu’observer de l’extérieur, les comportements. Par contre, je connais le fonctionnement cognitif théorique de l’être humain. J’ai donc des méta-connaissances qui me permettent d’expliquer ce que j’observe, d’en tirer des conclusions et de trouver des solutions.  
[Raphaël Yharrassarry, Tout le monde peut être UX designer ? Et autres réflexions.](http://blocnotes.iergo.fr/articles/tout-le-monde-peut-etre-ux-designer-et-autres-reflexions/)

Quelques arguments bien pensés pour *défendre* le rôle de l'UX designer, dans un contexte d'industrualisation du développement web.