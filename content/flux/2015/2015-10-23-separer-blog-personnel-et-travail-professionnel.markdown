---
layout: post
title: "Séparer blog personnel et travail professionnel ?"
date: "2015-10-23T22:00:00"
comments: true
published: true
categories: 
- flux
---
>In the past few months weighing the pros and cons, I have decided to separate my personal blog from my professional work. The decision is hard to make because Visualgui has become both my personal and professional presence on the web for so many years.  
[Donny Truong, Introducing ON Designs](https://www.visualgui.com/2015/10/14/introducing-on-designs/)

Donny Truong, l'auteur de [Professionnal Web Typography](https://prowebtype.com/), lance le site web de son activité de designer *indépendant*, l'occasion d'aborder la question de la séparation d'un carnet personnel et d'un site *portfolio* reflétant uniquement une activité professionnelle.