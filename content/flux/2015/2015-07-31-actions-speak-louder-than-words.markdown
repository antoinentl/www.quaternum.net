---
layout: post
title: "Actions speak louder than words"
date: "2015-07-31T22:00:00"
comments: true
published: true
categories: 
- flux
---
>So what did we learn? First of all, the user is always right… but sometimes it’s not what they say, but what they do. Of course you need to take user feedback into consideration, but remember that actions speak louder than words.  
[Gillian Morris, Don’t Listen to Your Users](https://medium.com/what-i-learned-building/the-user-is-always-right-eab73c620e7d)