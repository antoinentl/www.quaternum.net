---
layout: post
title: "Les bases de l'accessibilité web"
date: "2015-12-16T22:00:00"
comments: true
published: true
categories: 
- flux
slug: les-bases-de-l-accessibilite-web
---
>I’ve been asked again and again over the years what the absolute basics of web accessibility are. And while I always thought that it is not so difficult to find resources about these basics, the recurrence of that question prompted me to finally write my own take on this topic. So here it is, my list of absolute web accessibility basics every web developer should know about.  
[Marco, The web accessibility basics](https://www.marcozehe.de/2015/12/14/the-web-accessibility-basics/)

Les *basiques* de l'accessibilité web : de l'alternative pour les images aux listes, en passant par les contrastes texte-fond et les structures sémantiques. Un incontournable !

>If there’s one wish I have for Christmas from the web developer community at large, it is this: Be good citizens of the web, and learn *proper* HTML before you even so much as touch any JavaScript framework. Those frameworks are great and offer a lot of features, no doubt. But before you use hundreds of kilobytes of JavaScript to make something clickable, you may want to try if a simple button element doesn’t do the trick just as fine!