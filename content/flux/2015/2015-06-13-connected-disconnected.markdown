---
layout: post
title: "Connected - Disconnected"
date: "2015-06-13T22:00:00"
comments: true
published: true
categories: 
- flux
tags:
- design
---
>As a mobile designer, perhaps I should feel satisfaction that I’ve created such engaging experiences. But I can’t help but think: maybe we mobile designers have done our jobs a little too well. And maybe “engagement” was the wrong goal to chase in the first place.  
[Connected // Disconnected, Josh Clark](https://the-pastry-box-project.net/josh-clark/2015-june-10)

Penser des interfaces et des applications ne veut plus dire prendre toute l'attention de l'utilisateur, mais répondre au besoin et disparaître. Cela me fait également penser au *marketing de l'interstice* qu'a mis en place Capitaine train -- voir [la conférence de Jean-Daniel Guyot à l'occasion de Web2day](https://www.youtube.com/watch?v=4Y8NregOZqc&index=63&list=PLAgO0SKz6lC-y0VdUfT89us3nJgt5T-hN) -- peut-être que l'on pourrait travailler beaucoup plus autour du *design de l'interstice*.