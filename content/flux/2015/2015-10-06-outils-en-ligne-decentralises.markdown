---
layout: post
title: "Des outils en ligne et décentralisés"
date: "2015-10-06T22:00:00"
comments: true
published: true
categories: 
- flux
slug: outils-en-ligne-decentralises
---
>Nous ne voulons pas remplacer Google, ni GAFAM, pour « Framasoftiser Internet ». Bien entendu, nous n’en avons pas les moyens… ni l’envie : devenir une asso avec plus d’employés que de bénévoles, très peu pour nous ! Notre but est simplement de sensibiliser les gens, de démontrer que le Libre offre des solutions et alternatives viables, et qu’un maximum de gens y goûtent.  
[Framasoft, Dégooglisons saison 2 : ils ne savaient pas que c’était impossible, alors ils l’ont fait !](http://framablog.org/2015/10/05/degooglisons-saison-2-ils-ne-savaient-pas-que-cetait-impossible-alors-ils-lont-fait/)

Les démarches de Framasoft prennent beaucoup d'ampleur en terme d'outils disponibles (*pads*, tableur, réseau social, raccourcisseur d'URL, Gitlab, etc.) !