---
layout: post
title: "Service Workers : le cache pour l'offline"
date: "2015-11-25T22:00:00"
comments: true
published: true
categories: 
- flux
slug: service-workers-le-cache-pour-l-offline
---
>Au-delà de l’aspect offline décrit par le Guardian et CSS Tricks, on peut aller plus loin comme faire du chargement dynamique d’images en fonction du support par le navigateur ! Même si c’est peu supporté pour l’instant, ça donne envie d’essayer pour ceux qui en ont la capacité (sans effets de bords pour les autres).  
[David, Service Workers](https://larlet.fr/david/stream/2015/11/24/)

Un pas de plus vers un web *offline*, mais également vers un chargement des contenus en mode *responsive*.