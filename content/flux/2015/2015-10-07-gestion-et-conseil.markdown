---
layout: post
title: "Gestion et conseil ?"
date: "2015-10-07T22:00:00"
comments: true
published: true
categories: 
- flux
---
>De nouveaux métiers émergent, les formations autour du web sont toujours plus nombreuses et le média s’industrialise à vitesse accélérée. Les jeunes profils qui-en-veulent retournent le web à la recherche d’informations, écrivent cinq nouveaux frameworks Javascript par semaine et font leurs nuits blanches sur du code comme je le faisais il y a dix ans. Ma valeur ajoutée n’est plus là, et je capitalise davantage sur mon expérience du terrain, ma capacité à mener un projet proprement à son terme et la valeur de mon conseil. C’est peut-être ça, vieillir ?  
[STPo, Dix ans de freelance](http://www.stpo.fr/blog/dix-ans-de-freelance/)

STPo raconte ses 10 années d'indépendant *dans le web*, avec beaucoup de sincérité, de pragmatisme et d'humour.