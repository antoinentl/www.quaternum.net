---
layout: post
title: "f.lux : un écran au rythme du soleil"
date: "2015-11-06T23:00:00"
comments: true
published: true
categories: 
- flux
slug: f-lux-un-ecran-au-rythme-du-soleil
---
>Ever notice how people texting at night have that eerie blue glow?  
Or wake up ready to write down the Next Great Idea, and get blinded by your computer screen?  
During the day, computer screens look good—they're designed to look like the sun. But, at 9PM, 10PM, or 3AM, you probably shouldn't be looking at the sun.  
[f.lux](https://justgetflux.com/)

Sur les conseils de [Nicolas](http://polylogue.org/), je découvre et installe [f.lux](https://justgetflux.com/), un petit utilitaire disponible pour Mac-Linux-Windows qui permet de modifier les teintes de son écran selon l'heure du jour.  
Les écrans rétro-éclairés à LED -- ordinateurs, smartphones, tablettes -- dégagent une lumière bleue qui peut avoir pour conséquence de bouleverser les rythmes du sommeil ([un article sur le sujet](http://www.clubic.com/technologies-d-avenir/actualite-746851-lecture-ecran-dormir-nuirait-sommeil.html)). Avec f.lux, en pleine journée l'écran reste blanc, puis en fin de journée il prend une tonalité jaune voir orange, afin de respecter les cycles naturels.

Après quelques heures (en soirée) puis plusieurs jours d'usage, le confort gagné est très important !