---
layout: post
title: "Hyper-spécialisation"
date: "2015-06-24T22:00:00"
comments: true
published: true
categories: 
- flux
tags:
- métier
---
>Peut-être qu'un développeur (Web) sage est celui qui a enfin compris qu'il ne faut pas avoir peur de l'abondance et du chaos, car on aura su trouver son chemin.  
[Par la fenêtre de la cuisine, Karl](http://www.la-grange.net/2015/06/11/cuisine)

[Karl](http://www.la-grange.net/) relaie [un billet](http://www.agmweb.ca/2015-06-10-not-keeping-up/) d'Andy McKay, de nombreuses réactions surgissent face à l'imposante palette de compétences et d'outils -- et leurs évolutions très rapides -- que doivent intégrer et gérer les *développeurs Web*. Et pourtant le Web repose sur des principes -- relativement -- simples. Peut-être que cela deviendra un choix éthique ou politique d'utiliser un *workflow* basé sur quelques briques et en partie pérenne et maitrisable. Et accepter de ne pas tout maîtriser, surtout.