---
layout: post
title: "Rechercher passionnément"
date: "2015-08-09T17:00:00"
comments: true
published: true
categories: 
- flux
---
>Alors pourquoi ne pas rechercher passionnément ce qui pourrait permettre de rendre le monde moins difficile ?  
[...]  
Notre seule clôture qui est aussi notre seule certitude est celle de notre finitude.  
[Annick Lantenois, *Le vertige du funambule*, page 85](http://editions-b42.com/books/vertige-du-funambule/)

La conclusion de l'ouvrage d'Annick Lantenois, *Le vertige du funambule*, sonne comme une invitation aux [Rencontres de Lure](http://delure.org/) -- qui auront lieu du 23 au 29 août --, une recherche passionnée, et collective.