---
layout: post
title: "L'infini de l'écriture"
date: "2015-07-21T22:00:00"
comments: true
published: true
categories: 
- flux
slug: l-infini-de-l-ecriture
---
>Déchiffrer le monde, c’est faire le pari permanent d’une lecture qui précède la pensée, le langage, le graphisme, l’écriture. Nous "lisons" depuis l’éveil de la conscience, "parlons" depuis plus de deux millions d’années, "dessinons" depuis plus de 40 000 ans et "écrivons" depuis plus de 5 000 ans.  
[Sébastien Morlighem, Ouvrir l'Infini](http://www.cnap.graphismeenfrance.fr/infini/specimen/ouvrir-l-infini)

En théorie, [l'Infini](http://www.cnap.graphismeenfrance.fr/infini/) est une bien belle création. J'attends encore avant de l'utiliser.

>En ce sens, ce modèle est un départ, la première borne d’une route ponctuée de rencontres, de retrouvailles, de surprises, une traversée de la lettre latine ; un recommencement, comme chaque nouvelle création typographique s’évertue – ou devrait s’évertuer – à le faire. L’Infini en est le songe, le récit, la somme.