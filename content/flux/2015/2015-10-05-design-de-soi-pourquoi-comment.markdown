---
layout: post
title: "Design de soi : pourquoi, comment"
date: "2015-10-05T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Le web francophone est devenu passif. Quelques uns publient des contenus, mais beaucoup les consomment de manière passive.  
Or, on est tous en attente de contenus intéressants, riches et originaux.  
Aussi, si chacun d'entre nous se met à publier, ne serait-ce qu'un petit peu, cela provoquera un effet boule de neige, et tout le monde y gagnera.  
Ce n'est pas une question de "se vendre", mais juste d'apporter quelque chose à la communauté en échange de ce qu'elle vous a déjà appris ou donné.  
[Marie Guillaumet, Design de soi : valoriser son identité et son expertise sur le web](http://marieguillaumet.com/design-de-soi-paris-web-2015)

Le design de soi c'est aussi le design de sa communauté : ce que l'on souhaite partager et construire avec les autres !