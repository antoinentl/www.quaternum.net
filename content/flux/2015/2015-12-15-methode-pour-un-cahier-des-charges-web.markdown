---
layout: post
title: "Méthode pour un cahier des charges web"
date: "2015-12-15T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Le déroulé varie d’un client à l’autre en fonction de la maturité du projet et des objectifs souhaités en termes de livrable à la fin de l’atelier. Voici les grandes étapes :  
1. Co-construction d’une vision commune pour le projet ;  
2. Analyse des cibles ;  
3. Définition du besoin par cible ;  
4. Écriture des messages clés ;  
5. Storytelling de la page d’accueil ;  
6. Atelier de prototypage ;  
7. Debrief et plan d’action.  
[Maëlle Gaultier, Rédiger et valider un cahier des charges web en une journée](http://www.24joursdeweb.fr/2015/rediger-et-valider-un-cahier-des-charges-web-en-une-journee/)

Sur [24 jours de web](http://www.24joursdeweb.fr/2015/rediger-et-valider-un-cahier-des-charges-web-en-une-journee/) -- "le calendrier de l'avent des gens qui font le web d'après" --, Maëlle Gaultier propose une méthode pour établir un cahier des charges avec un client. Quelques règles simples qui permettent d'intégrer plus concrètement le commanditaire dans l'établissement d'un projet, et de mieux cerner les contraintes et l'organisation globale. La méthode semble éprouvée, mais je me demande si certains freins ne rentrent pas en jeu, notamment lors de l'implication des équipes graphiques ou techniques.