---
layout: post
title: "Définition du DOM"
date: "2015-11-18T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Est-ce que le code que je vois dans DevTools est le DOM ? Oui, d'une certaine façon ! Quand vous regardez l'affichage de n'importe quel DevTool (Chrome DevTool, Firefox developer tools,...) vous voyez une représentation visuelle du DOM. On y est !  
[Chris Coyier, Qu'est-ce que le DOM ?](https://la-cascade.io/quest-ce-que-le-dom/)

Différencier le DOM -- Document Object Model -- et le HTML, c'est différencier le code originel, le code affiché dans le navigateur -- le *code source* --, et le code interprété.