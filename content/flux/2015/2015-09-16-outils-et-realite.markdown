---
layout: post
title: "Outils et réalité"
date: "2015-09-16T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Peut-être que je me pose trop de questions et qu'il faille embracer l'impermanence de nos outils car d'autres seront là répondant aux besoins du moment, mais pendant ce temps nous aurons créé quelque chose au dessus de ce substrat. Les constructions culturelles, les conversations et l'histoire qui les accompagnent auront elles pris une réalité tangible.  
[Karl Dubost, IRC. Slack. Clash](http://www.la-grange.net/2015/08/05/culture)

Ne pas s'enfermer dans des pratiques guidées par des outils, et à l'inverse ne pas oublier que nous développons des pratiques, une culture, autour de certains outils. Changer d'outils c'est abandonner une certaine culture -- pour une autre, peut-être.