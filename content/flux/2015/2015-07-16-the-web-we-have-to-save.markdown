---
layout: post
title: "The Web We Have to Save"
date: "2015-07-16T22:00:00"
comments: true
published: true
categories: 
- flux
tags:
- publication
---
>But hyperlinks aren’t just the skeleton of the web: They are its eyes, a path to its soul.  
[Hossein Derakhshan, The Web We Have to Save](https://medium.com/matter/the-web-we-have-to-save-2eb1fe15a426)

Une réflexion très pertinente d'un blogueur iranien qui sort de six ans de prison : le Web a complètement changé, ou plutôt la manière dont nous accédons à l'information a été totalement bouleversée. Que reste-t-il de la puissance des liens hypertextes et du potentiel de création dans un *écosystème* où l'usage des réseaux sociaux domine ?  
Étrange sensation de découvrir ce texte sur Medium via [Twitter](https://twitter.com/jsutt/status/621503145001095170)... Mais j'aurais aussi bien pu découvrir cet article via [la veille de David](https://larlet.fr/david/stream/2015/), ou les nombreux sites et carnets que je suis via leur flux RSS.  
C'est aussi le sens [de cette veille](/flux), proposer un autre circuit de l'information. Cette *tribune* de Hossein Derakhshan est plus salvatrice que déprimante, il y a de belles alternatives à construire.

>The centralization of information also worries me because it makes it easier for things to disappear.