---
layout: post
title: "Rencontres de Lure 2015"
date: "2015-07-07T22:00:00"
comments: true
published: true
categories: 
- flux
tags:
- typographie
---
>Ouverts, A4, jésus, oubliés, libres, courts, dynamiques, pdf, propriétaires,
jpg, raisin… Les formats engagent notre rapport au monde et aux autres. Loin d’être une mécanique passive, ils sont un principe moteur : ils transforment en même temps qu’ils véhiculent.  
[Rencontres de Lure 2015 : Portrait/paysage, un monde de formats](http://delure.org/-Rencontres-2015-.html)

C'est parti pour une nouvelle édition des Rencontres de Lure, avec un sujet qui me tient particulièrement à cœur ! Programme en cours de finalisation, mais les premiers intervenants sont visibles [par ici](http://delure.org/-Rencontres-2015-.html).