---
layout: post
title: "Sud Web 2015 en vidéo"
date: "2015-09-22T22:00:00"
comments: true
published: true
categories: 
- flux
---
Les conférences de Sud Web 2015 -- apparemment toutes -- sont disponibles en vidéo, en ligne et téléchargeables : de "Voir le web comme un espace" à "Ma pédale c'est HTML5" en passant par "Le design d'expérience utilisateur n'est pas un métier", à voir !  

[https://vimeo.com/album/3467338](https://vimeo.com/album/3467338)