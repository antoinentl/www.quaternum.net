---
layout: post
title: "Fortuitement Trivial"
date: 2015-05-05
comments: true
published: true
categories: 
- flux
tags:
- média
---
>Condensé de connaissances hebdomadaires et mélangées  
[Fortuitement Trivial](http://fortuitement.com/)

Une démarche originale&nbsp;: diffuser trois informations, une fois par semaine, sur des sujets aussi divers que variés. Un beau *concept* porté par [Marie et Julien](http://www.mariejulien.com/).