---
layout: post
title: "Questionner notre regard"
date: "2015-08-24T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Questionner notre regard, c'est questionner notre rapport au réel.  
François Chevret, lors des Rencontres internationales de Lure 2015