---
layout: post
title: "Production de livres et nouvelles pratiques numériques"
date: "2015-08-08T18:00:00"
comments: true
published: true
categories: 
- flux
slug: livre-et-numerique
---
>Multiplier la conception et la production de livres peut se constituer aussi comme une réponse à la pression exercée par l'extension de nouvelles pratiques suscitée par les médias numériques.  
[Annick Lantenois, *Le vertige du funambule*, page 70](http://editions-b42.com/books/vertige-du-funambule/)

Le mouvement de surproduction de livres, observés depuis plusieurs années, pourrait être une tentative de *s'opposer* à une culture numérique naissante mal comprise ou vue comme néfaste.