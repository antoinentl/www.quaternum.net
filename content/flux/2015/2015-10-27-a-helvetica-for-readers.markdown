---
layout: post
title: "A Helvetica For Readers"
date: "2015-10-27T22:00:00"
comments: true
published: true
categories: 
- flux
---
>ACUMIN by Robert Slimbach is a new type family from Adobe that does for book (and ebook) designers what Helvetica has always done for graphic designers. Namely, it provides a robust yet water-neutral sans-serif, in a full suite of weights and widths. And unlike the classic pressing of Helvetica that comes on everyone’s computers—but like Helvetica Neue—Acumin contains real italics for every weight and width.  
[L. Jeffrey Zeldman, A Helvetica For Readers](http://www.zeldman.com/2015/10/15/a-helvetica-for-readers/)

Jeffrey Zeldman présente rapidement cette nouvelle fonte, et décrit comment [le site de démonstration](http://acumin.typekit.com/), créé par [Nick Sherman](http://nicksherman.com/), a été pensé et développé. Un cas pratique et complexe d'un site web *responsive* qui affiche plusieurs dizaines de fontes -- la gestion de la performance est quelque peu délicate --, et qui utilise des colonnes en CSS3.