---
layout: post
title: "Un entretien avec Raphaël Bastide"
date: "2015-12-22T22:00:00"
comments: true
published: true
categories: 
- flux
slug: entretien-avec-raphael-bastide
---
>Git et plus largement les systèmes de gestion de versions qui sont largement utilisés par les développeurs pour versionner, comparer, archiver, sont plus que des programmes : je les considère comme des outils méthodologiques. Si leur apprentissage nécessite du temps et certains acquis, ils sont très inspirants et provoquent de bons réflexes, notamment pour la documentation et l’archivage, et posent frontalement la question des licences : quelles libertés laisser au public du projet ? Accès au code ? Modification ? Redistribution ?  
[Entretien avec Raphaël Bastide avec Sarah Discours, Matière code](http://from.esad-gv.fr/to/#56:Rapha%C3%ABl%20Bastide)

Sur la revue [From—To](http://from.esad-gv.fr/to/) de l’ÉSAD Grenoble Valence -- j'en ai parlé [ici](/2015/07/07/from-to/) --, Sarah Discours interroge Raphaël Bastide sur son travail d'*artiste*, de *designer*, de *développeur*, d'*enseignant*. Passionnant pour plusieurs raisons : le rôle que joue la documention dans le travail de Raphaël Bastide ; son utilisation des logiciels, programmes et applications en lien avec l'art ; et son usage du libre.

>Travailler avec du libre devient pour tous plus captivant, cela permet notamment de découvrir tout un écosystème autour du design. Le design libre a aussi l’énorme potentiel de nous faire découvrir de nouvelles choses et de faire émerger de nouvelles formes, c’est pour moi un avantage majeur. [...]  
> Le choix du libre configure mon espace de travail, harmonise mes projets, il devient un repère et une contrainte féconde. Ce choix est naturel pour moi et chaque jour j’en mesure les bénéfices.