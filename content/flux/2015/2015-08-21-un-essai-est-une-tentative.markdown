---
layout: post
title: "Un essai est une tentative"
date: "2015-08-21T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Un essai est une tentative, Jim. Rien d'autres. Et fondamentalement, depuis des siècles, ça n'a jamais été autre chose. Même par son étymologie, "essai" veut dire "tentative". Et donc, en tant qu'auteur d'essais, mon interprétation de ce qui m'incombe, c'est de tenter -- d'*essayer* -- d'avoir prise sur une chose avant qu'elle ne soit perdue à jamais en retournant au chaos.  
[John D'Agata, *Que faire de ce corps qui tombe ?*, page 109](http://www.vies-paralleles.org/portfolio/que_faire_de_ce_corps_qui_tombe/)

L'une des premières publications de la nouvelle maison d'édition Vies Parallèles, un autre projet d'Alexandre Laumonier, *Que faire de ce corps qui tombe ?*, est passionnante. La forme permet de servir le fond -- un texte original au centre et une discussion entre son auteur et un *fact-checker* dans les marges.