---
layout: post
title: "Économie de la performance web"
date: "2015-11-23T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Le web grossit. Les pages s’alourdissent, le nombre de ressources (images, scripts, css, etc) nécessaires pour afficher une page web ne cesse de croître.  
Pourquoi ? Parce qu’aujourd’hui les sites web sont des applications à part entière, avec des fonctionnalités qui peuvent être très riches, et offrant de nombreux contenus multimédias.  
[Damien Jubeau, Budget de performance : un indispensable à la rapidité des sites web](https://medium.com/@DamienJubeau/budget-de-performance-indispensable-rapidite-sites-web-a771922e89e8#.ia50v6ykj)

Quelques grands principes autour de la performance web, et de pourquoi il est important de s'en inquiéter -- pour des raisons directement ou indirectement économiques. Une déclinaison de la *qualité web*, autour de l'outil [Dareboost](https://www.dareboost.com/fr/home).