---
layout: post
title: "Uniformisation et originalité du web design"
date: "2015-07-29T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Have designers lost that pioneer spirit? Has creativity been sacrificed on the altar of convenience?  
[Joshua Johnson, Beyond The Boring: The Hunt For The Web’s Lost Soul](http://www.smashingmagazine.com/2015/07/hunt-for-the-webs-lost-soul/)

Quelques exemples d'un design web ennuyeux, répétitif et uniformisé, et à l'inverse de *grilles* originales qui se jouent des modes actuelles. Quelques *chemins de traverse* en architecture de l'information.