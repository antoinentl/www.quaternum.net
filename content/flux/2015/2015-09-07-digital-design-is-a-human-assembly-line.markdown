---
layout: post
title: "Digital design is a human assembly line"
date: "2015-09-07T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Digital design is a human assembly line.  
[Travis Gertz, Design machines](https://louderthanten.com/articles/story/design-machines)

Un long article qui démontre avec succès combien le webdesign est actuellement répétitif et peu original. Travis Gertz explique comment nous sommes arrivés à une telle uniformité, et esquisse quelques solutions.  
Une petite claque qui nous rappelle que pour résoudre ce défi, il faut aller chercher du côté de l'humain, des pratiques et des outils.

>Without the logos, could you tell which companies own which screenshots? Does it matter? The pattern’s become its own trademark. Just one of the popular yet mediocre ones plaguing modern screen-based design.  
What’s wrong?

>The best and worst thing about the information age is the ability and our penchant for sharing every damn thought that enters our minds. When designing, testing, and marketing our digital products, we feel compelled to blog our findings, tweet our opinions, and speak about the shit that works and doesn’t work. It doesn’t take long for opinions to morph from one organization’s experience, to industry-wide opinion, to black and white standards and best practices.

>Designing better systems and treating our content with respect are two wonderful ideals to strive for, but they can’t happen without institutional change. If we want to design with more expression and variation, we need to change how we work together, build design teams, and forge our tools.