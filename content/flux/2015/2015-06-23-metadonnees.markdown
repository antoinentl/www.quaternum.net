---
layout: post
title: "Métadonnées"
date: "2015-06-23T22:00:00"
comments: true
published: true
categories: 
- flux
tags:
- privacy
---
>L’interception systématique et la collecte des données techniques des communications sont avant tout une mesure pour dissuader les gens de communiquer.  
[Métadonnées, Clochix](http://esquisses.clochix.net/2015/06/13/0613/)

La solution pour rendre des échanges réellement privés n'est pas tant dans la technique que dans les pratiques adoptées, par exemple en supprimant un peu de numérique -- courriers papiers, discussions hors réseaux, etc -- de la chaîne numérique complète, lorsque cela est possible.