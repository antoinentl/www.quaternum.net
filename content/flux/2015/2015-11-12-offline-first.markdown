---
layout: post
title: "Offline First : prendre en compte la réalité des connexions"
date: "2015-11-12T22:00:00"
comments: true
published: true
categories: 
- flux
slug: offline-first
---
>We’ve generally been rather optimistic about the whole problem, often thinking that coverage and bandwidth are sure to increase continually, but our recent experiences have made this attitude appear increasingly naïve. Capacity problems, varying coverage and zones with zero reception, unreliable connections (both wifi and telco) and problems incurred through travelling will likely persist for quite a while.  
[Alex, Say Hello to Offline First](http://hood.ie/blog/say-hello-to-offline-first.html)

Imaginer des solutions pour les applications web dans des cas d'absence ou de manque de réseau, c'est l'objectif d'[Offline First !](http://offlinefirst.org/), une mine d'informations dans ce domaine.