---
layout: post
title: "Le designer"
date: "2015-07-25T22:00:00"
comments: true
published: true
categories: 
- flux
---
>En dialogue avec le commanditaire, le designer graphique anticipe et projette dans l'espace et le temps les conditions d'accès aux informations, aux savoirs et aux fictions, et celles de leur appropriation. [...] Le design est avant tout une prise de position que le designer affirme par les choix de ses commanditaires et dans les diverses solutions d'un même problème.  
[Annick Lantenois, *Le vertige du funambule*, page 12](http://editions-b42.com/books/vertige-du-funambule/)

Une définition très intéressante du designer et du design, dans le passionnant ouvrage d'Annick Lantenois, *Le vertige du funambule*, paru en 2010 aux éditions B42.