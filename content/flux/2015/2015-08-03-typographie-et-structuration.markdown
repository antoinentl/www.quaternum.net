---
layout: post
title: "Typographie et structuration"
date: "2015-08-03T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Chaque mot, chaque phrase composée dans un caractère plus fort que celui du texte instaure un balisage, constitue une constellation parallèle à l’intérieur de paragraphes foisonnants, complexes, invitant ainsi à une lecture a priori plus superficielle mais souvent orientée vers une synthèse claire de l’ensemble.  
[Sébastien Morlighem, Le gras, une force visuelle moderne](http://www.cnap.graphismeenfrance.fr/infini/specimen/le-gras-une-force-visuelle-moderne)

L'évolution de la typographie est une histoire de structuration.