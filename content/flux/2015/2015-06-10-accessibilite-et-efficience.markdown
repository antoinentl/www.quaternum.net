---
layout: post
title: "Accessibilité et efficience"
date: "2015-06-10T22:00:00"
comments: true
published: true
categories: 
- flux
tags:
- accessibilité
---
>100% accessible n’existe pas  
Autant attaquer un moulin à vent sur une licorne
Le w3c lui-même — au travers de la wai et des wcag — recommande de ne pas chercher à atteindre la conformité maximale.  
Il faut privilégier une **démarche** de mise en accessibilité.   
[Accessibilité et efficience, Gaël Poupard](http://www.ffoodd.fr/wpmx/index.html)

Il s'agit du support d'une intervention de Gaël Poupard lors du [WPMX Day 2015](http://2015.wpmx.org/), sous-titré "Ce que vous faites et ce dont vous êtes capables". Une présentation très claire sur ce qu'il est possible de faire en terme d'accessibilité web, à différents niveaux (responsabilité, conception, réalisation, etc), et avec quelques ressources très riches.  
À noter que les *slides* sont réalisés avec l'outil [AccesSlide](https://github.com/access42/accesSlide), une sorte de *fork* de [reveal.js](/2012/11/05/reveal-js-des-presentations-en-html/), en version accessible.