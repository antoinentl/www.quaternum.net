---
layout: post
title: "Design graphique, perfectibilité et structuration"
date: "2015-07-26T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Le designer graphique se donne la mission de remplir l'une des conditions de la perfectibilité de l'homme : la maîtrise des systèmes de traitement et de régulation des informations et des savoirs. Le design graphique fut donc l'un des instruments de ce savoir salvateur avec lequel se construisent des syntaxes scripto-visuelles fonctionnelles, efficaces. Éliminer le maximum d'obstacles, de bruits visuels, susceptibles de freiner la lecteur et l'identification fut l'objectif visant à la clarification, la structuration, la hiérarchisation du texte du monde écrit par les occidentaux.  
[Annick Lantenois, *Le vertige du funambule*, page 34](http://editions-b42.com/books/vertige-du-funambule/)

Ces notions de perfection et de maîtrise -- ultime -- ne sont pas rassurantes. Il y a quelque chose de très -- trop ? -- rationnel ici. Même si je reconnais que cette définition peut être rassurante, il faut savoir sortir de sa zone de confort, en tant que designer et en tant que lecteur.