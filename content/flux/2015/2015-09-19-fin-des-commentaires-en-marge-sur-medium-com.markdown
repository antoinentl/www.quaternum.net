---
layout: post
title: "Fin des commentaires en marge sur Medium.com ?"
date: "2015-09-19T12:00:00"
comments: true
published: true
categories: 
- flux
slug: fin-des-commentaires-en-marge-sur-medium-com
---
Je découvre pour la première fois -- sauf erreur de ma part -- qu'un article sur Medium.com peut être commenté en fin, et non plus dans les marges. Je ne trouve pas d'information sur ce changement, qui date peut-être depuis plusieurs mois. C'est dommage qu'une telle fonctionnalité disparaisse, c'est ce qui faisait -- à mon sens -- une des forces et une des originalités de cette plateforme.  
Voici [un exemple d'article](https://medium.com/@kubachrzecijanek/how-to-build-an-awesome-form-1e9b2c1bd00d) avec les commentaires en fin de page, et [voici](/2013/10/14/readmill-et-medium-le-livre-et-le-web/) comment fonctionnait Medium.com.