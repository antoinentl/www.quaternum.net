---
layout: post
title: "Vincent Valentin : manifeste pour un travail différent"
date: "2015-10-28T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Je crois par exemple en l’enrichissement progressif, à la sémantique HTML et au bon usage des standards web. J’aime le web ouvert et interopérable, et je m’efforce de comprendre et d’embrasser au mieux ce média et ses spécificités.  
[Vincent Valentin, Mon manifeste pour un travail différent](http://vincent-valentin.name/articles/mon-manifeste-pour-un-travail-different)

Vincent cherche de nouvelles aventures d'intégrateur web, et expose ses idéaux pour travailler dans un cadre ou contexte qui lui conviendrait mieux. Accessibilité, éthique, partage d'expérience, apprentissage, qualité, autant d'*idéaux* que je partage, sans pour autant être un intégrateur.