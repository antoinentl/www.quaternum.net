---
layout: post
title: "Livre blanc sur l'accessibilité des CMS"
date: "2015-09-30T22:00:00"
comments: true
published: true
categories: 
- flux
slug: livre-blanc-sur-l-accessibilite-des-cms
---
>Cette étude détaille les points forts et faibles de quatre CMS largement répandus et résume leur aptitude à produire et gérer des sites Web accessibles. Ces résultats seront utiles aux développeurs, aux chefs de projet, et à tout responsable amené à s’interroger sur la stratégie à mettre en œuvre pour améliorer l'accessibilité d’un service en ligne et le choix d’outils  appropriés. Cette étude propose aussi une méthode qui permettra de faire évoluer cette base de connaissances sur l’accessibilité.  
[Dominique Burger, Choisir et utiliser un CMS pour créer des contenus accessibles](http://www.accessiweb.org/tl_files/doc_telechargement/gta21/GTA21_LivreBlanc_CMS_v1.0.pdf)

Un document qui résulte de plusieurs années de travail, et notamment suite à une journée du groupe de travail [ATAG2](http://www.w3.org/TR/ATAG20/) en France.  

Le document est construit en quatre parties :

1. Le cadre normatif
2. La politique d’accessibilité de trois grands CMS
3. L’amélioration de l’accessibilité d’un CMS
4. Étude comparative de la capacité des CMS à produire des contenus accessibles