---
layout: post
title: "Web : performances et simplicité"
date: 2015-05-31
comments: true
published: true
categories: 
- flux
tags:
- performance
---
>Et si dans notre recherche de performances nous restions simples que ce soit dans notre pratique, dans notre recherche créative, ainsi que dans le code que nous développons.  
[Les performances du Web, Karl Dubost](http://www.la-grange.net/2015/05/28/web-avant)

Optimiser n'est peut-être pas le meilleur choix pour obtenir un site web *performant*. Penser un site web -- dès sa conception -- avec autant de simplicité que possible.