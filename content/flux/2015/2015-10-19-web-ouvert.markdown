---
layout: post
title: "Web ouvert"
date: "2015-10-19T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Une fois votre nom de domaine en main et votre espace d'hébergement, vous êtes libre de publier dans le format, dans la forme et le style de votre choix.  
[Karl, Un Web à sa mesure](http://www.la-grange.net/2015/08/23/web)

Le web *peut* permettre une certaine liberté, il ne faut pas oublier ce qu'implique les silos.

>Pour favoriser un Web ouvert, nous devons continuer à promouvoir les techniques qui rendent indépendantes les individus.