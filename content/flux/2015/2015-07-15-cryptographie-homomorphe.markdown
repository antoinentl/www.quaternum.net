---
layout: post
title: "Cryptographie homomorphe"
date: "2015-07-15T22:00:00"
comments: true
published: true
categories: 
- flux
tags:
- privacy
---
>Le cloud travaille à l’aveugle sur les données que vous avez chiffrées avant de vous retourner le résultat final que vous seul êtes capable de déchiffrer, car vous seul possédez la clé de déchiffrement.  
[Julien Bourdet, Un cryptage révolutionnaire pour sécuriser le cloud](https://lejournal.cnrs.fr/articles/un-cryptage-revolutionnaire-pour-securiser-le-cloud)

Une solution -- ou un principe -- de cryptage qui offre des applications intéressantes, le problème étant que pour le moment cela demande trop de puissance de calculs -- avec le risque d'une *fracture cryptographique*.