---
layout: post
title: "De l’audit de contenu aux idées de design"
date: "2015-10-22T22:00:00"
comments: true
published: true
categories: 
- flux
slug: de-l-audit-de-contenu-aux-idees-de-design
---
>Si on les aborde correctement, les audits de contenu sont des outils d’une puissance exceptionnelle. Au delà de l’évaluation de la quantité et de la qualité du contenu, ils aident les concepteurs à comprendre l’environnement informationnel, facilitent les discussions stratégiques et permettent la découverte d’informations qui ont directement influencés la direction du design et la stratégie. En résumé, nous avons augmenté les chances de succès de notre redesign en approchant les audits en gardant ces buts à l’esprit.  
[Christopher Detzi, De l’audit de contenu aux idées de design](http://www.pompage.net/traduction/de-l-audit-de-contenu-aux-idees-de-design)

Une tâche *classique* qu'est l'audit de contenus peut aussi permettre de repenser un design en adéquation avec les contenus qu'il met en forme. Un article très complet qui reprend les différentes étapes d'un cas concret.