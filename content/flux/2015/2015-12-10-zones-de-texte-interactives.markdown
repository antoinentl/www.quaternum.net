---
layout: post
title: "Zones de texte interactives : des exemples originaux"
date: "2015-12-10T22:00:00"
comments: true
published: true
categories: 
- flux
slug: zones-de-texte-interactives
---
>Some inspiration for effects on text inputs using CSS transitions, animations and pseudo-elements.  
[Mary Lou, Inspiration for Text Input Effects](http://tympanus.net/codrops/2015/01/08/inspiration-text-input-effects/)

Quelques exemples très originaux de champs ou zones de texte interactifs, pour transformer des formulaires en des objets web bien plus attirants -- tout est dans le détail.