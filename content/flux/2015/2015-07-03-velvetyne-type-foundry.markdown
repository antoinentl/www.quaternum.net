---
layout: post
title: "Un nouveau site pour Velvetyne Type Foundry"
date: "2015-07-03T22:00:00"
comments: true
published: true
categories: 
- flux
tags:
- typographie
slug: velvetyne-type-foundry
---
>We, VTF, dauntless offspring of the Postscript generation, we chase at the speed of the electron, the course of typography and its attendant of wrestling writing warriors. Like Spartacus, we unchain glyphs from unfair laws and arbitrary conventions, like Ben Hur on a gigantic tank, we welcome script horsewomen and type charioteers and all tremendous, armed with bulletpoint pen and bold crossbows, wearing draw hats, we challenge here and now the tantalizing palimpsest of today.  
[VTF, www.velvetyne.fr](http://www.velvetyne.fr/about/)

VTF -- Velvetyne Type Foundry -- dispose d'un nouveau site web ! Raphaël Bastide et Étienne Ozeray l'ont réalisé avec [ofont](http://ofont.net/). On notera quelques éléments remarquables -- parmi d'autres :

- l'abandon du JavaScript à outrance ;
- le design lisible mais débridé, structuré mais *punk* ;
- un site qui s'adapte à la taille des écrans ;
- des fontes plus ou moins abouties, à utiliser telles quelles ou à compléter ;
- un beau projet.