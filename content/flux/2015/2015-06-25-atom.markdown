---
layout: post
title: "Atom 1.0"
date: "2015-06-25T22:00:00"
comments: true
published: true
categories: 
- flux
tags:
- outils
slug: atom
---
>Atom started as a side project of GitHub founder @defunkt (Chris Wanstrath) way back in mid 2008, almost exactly seven years ago. He called it Atomicity. His dream was to use web technologies to build something as customizable as Emacs and give a new generation of developers total control over their editor.  
[Atom 1.0, benogle](http://blog.atom.io/2015/06/25/atom-1-0.html)

Atom est un éditeur de texte produit par GitHub, qui repose principalement sur un concept : (re)donner un outil très personnalisable aux développeurs. Une alternative intéressante à [Sublime Text](http://www.sublimetext.com/) ? Un projet encore jeune mais plein de promesses.