---
layout: post
title: "Portable Web Publications"
date: "2015-10-15T22:00:00"
comments: true
published: true
categories: 
- flux
---
>The Digital Publishing Interest Group has published a Working Draft of Portable Web Publications for the Open Web Platform. This document introduces Portable Web Publications, a vision for the future of digital publishing that is based on a fully native representation of documents within the Open Web Platform. Portable Web Publications achieve full convergence between online and offline/portable document publishing: publishers and users won’t need to choose one or the other, but can switch between them dynamically, at will.  
[W3C, First Public Working Draft: Portable Web Publications for the Open Web Platform](https://www.w3.org/blog/news/archives/5072)

Une grande avancée pour la publication *numérique*, enfin la possibilité de travailler avec des standards pour du *en ligne* et du *hors ligne*. Il s'agit là des prémisses de la rencontre entre le web et le livre numérique -- le format EPUB.