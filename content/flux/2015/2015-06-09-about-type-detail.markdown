---
layout: post
title: "About Type Detail"
date: "2015-06-09T22:00:00"
comments: true
published: true
categories: 
- flux
tags:
- typographie
---
>The rule and goal [of this project] are to annotate a web typeface each day, pointing out the beautiful details of the typefaces that one often takes for granted. This project is going to last for 100 days between May 18 and Aug 26 [2015].  
[About Type Detail, Wenting Zhang](http://typedetail.com/)

Un beau projet pour découvrir des typographies. À noter également quelques éléments de l'interface du site (le détail dans le détail)&nbsp;:

- une couleur de la charte graphique change à chaque *typeface* présentée&nbsp;;
- la description des détails n'est pas une image mais du CSS&nbsp;;
- certains détails de la navigation sont bien beaux et bien pensés&nbsp;: le `hover` des flèches pour passer d'une *typeface* à une autre, le fonctionnement par étiquettes, la navigation au clavier pour passer d'une présentation à une autre, etc.