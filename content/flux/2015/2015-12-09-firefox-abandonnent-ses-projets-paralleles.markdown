---
layout: post
title: "Firefox abandonnent ses projets parallèles ?"
date: "2015-12-09T22:00:00"
comments: true
published: true
categories: 
- flux
---
>En divorçant de sa messagerie Thunderbird et en arrêtant la commercialisation des smartphones sous Firefox OS, la fondation Mozilla se recentre sur le cœur de son métier : le navigateur Firefox, le Web ouvert et la protection de la vie privée.  
[Camille Gévaudan, Mozilla se concentre sur l’essentiel](http://www.liberation.fr/futurs/2015/12/09/mozilla-se-concentre-sur-l-essentiel_1419479)

Mozilla abandonne -- ou presque -- deux projets que -- personnellement -- j'utilise au quotidien. Je suis peut-être isolé pour Firefox OS, mais je ne le suis certainement pas pour Thunderbird. Je m'interroge sur ma faculté de *geek* à soutenir la fondation Mozilla suite à ces choix stratégiques, tout en étant conscient des *contraintes économiques* -- appelons cela comme ça. Je ne suis pas certain que le navigateur Firefox sera le seul moyen d'*innover*, même si ce recentrage paraît nécessaire...