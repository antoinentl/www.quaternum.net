---
layout: post
title: "Méthodologie et intuition"
date: "2015-09-12T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Cependant, s'il y a de réels dangers à prendre la méthodologie et la résolution de problèmes trop au sérieux, les prétendus libertés qu'offrent certaines approches aléatoirement "intuitives" sont, comme chacun sait, tout aussi périlleuses lorsque la tâche s'avère complexe ou demande un gros effort du point de vue technique ou fonctionnel.  
[Norman Potter, *Qu'est-ce qu'un designer*, page 53](http://editions-b42.com/books/quest-ce-quun-designer/)

Trouver l'équilibre.