---
layout: post
title: "Feeling Like An Unwelcome Guest"
date: "2015-09-02T22:00:00"
comments: true
published: true
categories: 
- flux
---
>After two actions required to visit the website (close the app install banner, click ‘no thanks’), you still think I might not want to read the content in my browser?  
[Peter Gasston, Feeling Like An Unwelcome Guest on medium.com](http://www.broken-links.com/2015/09/01/feeling-like-an-unwelcome-guest-on-medium-com/)

Une histoire assez banale -- la récurrence d'une invitation à télécharger une application plutôt qu'à consulter directement un site web *mobile* --, mais qui a le mérite de rappeler que les plateformes font bien ce qu'elles veulent en terme d'accès à l'information.