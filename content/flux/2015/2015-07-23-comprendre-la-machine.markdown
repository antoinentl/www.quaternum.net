---
layout: post
title: "Comprendre la machine"
date: "2015-07-23T09:00:00"
comments: true
published: true
categories: 
- flux
---
>I believe to become a better developer you MUST get a better understanding of the underlying software systems you use on a daily basis and that includes programming languages, compilers and interpreters, databases and operating systems, web servers and web frameworks. And, to get a better and deeper understanding of those systems you MUST re-build them from scratch, brick by brick, wall by wall.  
[Ruslan, Let’s Build A Web Server. Part 1.](http://ruslanspivak.com/lsbaws-part1/)

Expérimenter et comprendre ne veut pas forcément dire savoir administrer un serveur web dans un cadre professionnel. Mais essayer avec ses propres moyens, rencontrer des difficultés et des échecs, réussir, comprendre les enjeux et les compétences, voilà ce que vous apprendre de gérer un serveur ou une machine virtuelle.  
Il faudrait que je remette en place mon Raspberry Pi, c'était une expérience enrichissante.  

Via [le carnet de Karl](http://www.la-grange.net/2015/06/26/whistler).