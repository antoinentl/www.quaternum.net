---
layout: post
title: "'Do We Even Need CSS Anymore?'"
date: "2015-07-04T22:00:00"
comments: true
published: true
categories: 
- flux
tags:
- standards
---
>CSS is the abstraction of style away from anything else.  
[Chris Coyier, The Debate Around "Do We Even Need CSS Anymore?"](https://css-tricks.com/the-debate-around-do-we-even-need-css-anymore/)

Une question issue d'un problème pratique -- "Smart people on great teams cede to the fact that they are afraid of their own CSS" -- qui a des incidences techniques fortes, et qui touche au principe même du web tel que nous le connaissons encore. L'utilisation de JavaScript -- c'est ici une alternative au CSS pour faire de la mise en forme en *inlines* et non dans un fichier CSS -- dans tant de domaines m'inquiète quelque peu, peut-être à tort.  
Le problème de départ est peut-être un faux problème, qui pourrait être résolu avec des bonnes pratiques et des outils -- Sass a semble-t-il été conçu dans ce sens.