---
layout: post
title: "Grav"
date: 2015-04-28
comments: true
published: true
categories: 
- flux
tags:
- outils
---
>A Modern Flat-File CMS  
[Grav](http://getgrav.org/)

Un énième gestionnaire de contenus sans base de données, mais qui nécessite PHP. Je ne sais pas s'il vaut la peine par rapport à [Jekyll](/2014/12/29/un-point-sur-jekyll/) par exemple.