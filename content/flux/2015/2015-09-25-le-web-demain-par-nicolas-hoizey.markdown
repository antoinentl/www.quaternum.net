---
layout: post
title: "Le web demain par Nicolas Hoizey de Clever Age"
date: "2015-09-25T08:00:00"
comments: true
published: true
categories: 
- flux
slug: le-web-demain-par-nicolas-hoizey
---
>Les plus importants des nouveaux utilisateurs du Web dans les années à venir ne seront pas les possesseurs de montres connectées, des derniers smartphones haut de gamme, ou des tablettes professionnelles, mais bien ceux d’appareils d’entrée de gamme, n’ayant aucun autre moyen de connexion, et ayant pour autant les mêmes besoins que nous aujourd’hui. Les enjeux de performance et sobriété des services Web n’ont jamais été autant importants, et nécessitent une prise de conscience générale. C’est un paramètre supplémentaire dans le souci permanent pour l'accessibilité universelle des services Web.  
[Paris Web : les coulisses, Interview Partenaire : Clever Age](http://coulisses.pw/post/129342906078/interview-partenaire-clever-age)