---
layout: post
title: "Une expérience de management"
date: "2015-12-07T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Il donne les ordres, il répartit les tâches, comme un sergent avant d’entamer le franchissement de la colline 235, le bout de cigare mâchonné au coin de la bouche : « Toi, le designer, tu attaques de front, et toi l’inté, tu marches exactement dans ses pas derrière lui, je veux voir la même chose. Allez, en avant ! » Ensuite, le casque juché de guingois sur le crâne, il se tourne vers le client et le rassure : « Vous inquiétez pas, mes gars savent ce qu’ils font, on va vous sécuriser tout ça dans le délai prévu. »  
[Stéphane Deschamps, Le manager, ce gendre idéal méconnu](http://www.24joursdeweb.fr/2015/le-manager-ce-gendre-ideal-meconnu/)

Stéphane Deschamps raconte de façon très claire à quoi peut ressembler le travail de manager, en partant de sa propre expérience. Positif, pédagogue, drôle, et interrogateur, Stéphane décrit cela avec beaucoup de brio !