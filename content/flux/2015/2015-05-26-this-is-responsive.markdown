---
layout: post
title: "This Is Responsive"
date: 2015-05-26
comments: true
published: true
categories: 
- flux
tags:
- design
---
>This Is Responsive.  
Patterns, resources and news for creating responsive web experiences.  
[This Is Responsive](http://bradfrost.github.io/this-is-responsive/)

Outils et ressources sur le <abbr title="Responsive Web Design">RWD</abbr>, beaucoup d'informations, et semble-t-il très pertinentes, proposées notamment par [Brad Frost](http://bradfrost.com/).