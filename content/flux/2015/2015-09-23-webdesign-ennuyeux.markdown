---
layout: post
title: "Webdesign ennuyeux"
date: "2015-09-23T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Yes, the old Web was ugly and insane, but it was also much more fun than a sea of sites that look weirdly similar. Now it feels like everywhere you go online looks strangely similar to every other site. [...]  
Websites from small businesses to startups and giant companies have all started to use similar layouts and frankly, it’s boring. [...]  
Let’s start again, ignore the rules and stop building websites that look the same. We should start tinkering and breaking rules. Let’s not lose the magic of the early Web.  
[Owen Williams, Web design is now completely boring](http://thenextweb.com/opinion/2015/09/23/zzzzzz/)

La créativité serait mise à mal à cause de *frameworks* comme Bootstrap, et pourtant les possibilités techniques sont grandes pour imaginer des sites web originaux. Il faut sortir du cadre que des outils récents ont créés.  
Owen Williams ne parle pas de design responsif ou adaptatif, et c'est là aussi l'une des raisons de cette homogénéisation.