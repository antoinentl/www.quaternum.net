---
layout: post
title: "Forme et sens"
date: "2015-08-17T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Pour le designer, le bon design correspond à la réponse généreuse et pertinente apportée au contexte global d'une opportunité de design bien précise, quelle que soit son échelle, alors que la qualité du design réside pour sa part dans une corrélation étroite et fidèle entre la forme et le sens.  
[Norman Potter, *Qu'est-ce qu'un designer*, page 35](http://editions-b42.com/books/quest-ce-quun-designer/)

Une définition du *design* très réaliste.