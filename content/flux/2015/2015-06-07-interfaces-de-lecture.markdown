---
layout: post
title: "Interfaces de lecture : Medium et Pocket"
date: 2015-06-07
comments: true
published: true
categories: 
- flux
tags:
- design
slug: interfaces-de-lecture
---
>[...] nous ne ressentons pas forcément le besoin d’avoir plus de réglages, nous acceptons le « compromis » parce qu’il est suffisamment bon.  
[Jiminy Panoz](http://jiminy.chapalpanoz.com/inspiration-design-medium-pocket/)

Jiminy Panoz décortique Medium et Pocket&nbsp;: ces deux plateformes -- ou plutôt ce média et ce service -- ont fait le choix de la lisibilité et de la simplicité. Deux questions sous-jacentes&nbsp;:

- ne pas laisser de choix de *réglages* aux utilisateurs est-il la bonne voie&nbsp;? Et en même temps n'est-ce pas le principe du web de proposer des interfaces hétérogènes. Le risque est le *lissage* ;
- est-ce que ces modèles sont tenables, économiquement parlant&nbsp;? Medium semble dans une situation délicate.