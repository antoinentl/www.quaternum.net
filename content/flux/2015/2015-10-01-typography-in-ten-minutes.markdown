---
layout: post
title: "Typography in ten minutes"
date: "2015-10-01T22:00:00"
comments: true
published: true
categories: 
- flux
---
>This is a bold claim, but i stand be­hind it: if you learn and fol­low these five ty­pog­ra­phy rules, you will be a bet­ter ty­pog­ra­pher than 95% of pro­fes­sional writ­ers and 70% of pro­fes­sional de­sign­ers. (The rest of this book will raise you to the 99th per­centile in both categories.)  
[Matthew Butterick, Practical Typography](http://practicaltypography.com/typography-in-ten-minutes.html)

Un classique à ne pas oublier.