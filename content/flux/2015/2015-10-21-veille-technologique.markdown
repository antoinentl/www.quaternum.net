---
layout: post
title: "Veille technologique"
date: "2015-10-21T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Quelle est ma valeur ajoutée, qu'attendent de moi mes clients, qu'est-ce que je dois faire pour bien faire mon boulot ? Avoir une bonne vision de l'écosystème numérique, et proposer des solutions pertinentes en fonction du contexte. Je n'ai pas besoin de tout savoir pour être un bon professionnel.  
[Thibault Jouannic, La veille techno pour les vieux croûtons](http://www.miximum.fr/veille-techno-vieux-croutons-paris-web-2015.html)

Superbe, drôle, réaliste et déculpabilisant, ce billet déclenche une prise de conscience saine sur les enjeux de la veille en général et plus particulièrement dans le domaine du développement *web*.  
Résumé : trouver l'équilibre entre faire de la veille pour rester *au courant*, et savoir décrocher pour ne pas être écœuré.

>Pourtant on sait que c'est comme ça que nait l'innovation : en combinant des idées qui à la base n'ont pas de lien direct entre elles. Donc plus on s'ouvre à des idées et concepts divers et variés, plus on va s'enrichir et plus on va s'ouvrir des portes pour trouver des solutions innovantes aux problèmes du quotidien.