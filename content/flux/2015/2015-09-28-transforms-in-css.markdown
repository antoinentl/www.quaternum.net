---
layout: post
title: "Transforms in CSS"
date: "2015-09-28T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Ever since the inception of Cascading Style Sheets (CSS), elements have been rectangular and firmly oriented on the horizontal and vertical axes. A number of tricks arose to make elements look like they were tilted and so on, but underneath it all was a rigid grid. In the late 2000s, an interest grew in being able to break the shackles of that grid and transform objects in interesting ways—and not just in two dimensions.  
[Eric A. Meyer, Transforms in CSS](http://www.oreilly.com/free/transforms-in-css.csp?cmp=tw-web-free-lp-lgen_emeyer)

Petit guide, "gratuit", pour manipuler, en 3D et en CSS, les images.