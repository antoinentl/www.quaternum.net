---
layout: post
title: "Travail en transition"
date: "2015-06-28T22:00:00"
comments: true
published: true
categories: 
- flux
tags:
- métier
---
>Dire non à quelque chose, c’est dire oui à plein d’autres choses.  
[Travail en transition, David Larlet](https://larlet.fr/david/blog/2015/travail-transition/)

David Larlet expose et analyse les bouleversements subit par le travail -- principalement le travail salarié -- au contact du numérique, à travers l'expérience de Scopyleft. Une des questions principales que je me pose, est l'application de ces propositions dans d'autres champs que le développement web, l'informatique ou le design.