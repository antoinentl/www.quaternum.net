---
layout: post
title: "Autopsie d'une recherche"
date: "2015-12-21T22:00:00"
comments: true
published: true
categories: 
- flux
slug: autopsie-d-une-recherche
---
>Je n’utilise pour ainsi dire plus Google (ou occasionnellement, par la bande), et je constate qu’utiliser une gamme plus large de moteurs me permet de tomber sur des informations que Google ne m’aurait très probablement pas remonté dès la première page.  
[Emmanuel Clément, Autopsie d’une recherche](http://emmanuel.clement.free.fr/2015/12/18/autopsie-une-recherche)

Emmanuel cartographie ses méthodes de recherche, avec deux objectifs : être plus précis, et se passer de Google ou d'autres moteurs de recherche qui pistent. Très intéressant d'envisager une application plus ou moins libre de cette méthode dans un cadre professionnel.