---
layout: post
title: "OLA #0 Expérimente l’édition libre"
date: 2015-05-27
comments: true
published: true
categories: 
- flux
tags:
- libre
slug: ola-experimente-l-edition-libre
---
>Outils Libres Alternatifs est une initiative centrée sur la pratique, dont l'objectif est la promotion et la diffusion d'outils libres pour la création. Pour cela nous organisons des workshops durant lesquels un artiste, designer ou praticien se propose de partager sa pratique libre.  
[OLA #0 Expérimente l’édition libre](http://outilslibresalternatifs.org/ola0)

<abbr title="Outils Libres Alternatifs">OLA</abbr> lance un événement de deux jours autour de l'édition libre, un workshop ouvert, centré sur Scribus, une belle initiative. Du 29 au 31 mai 2015 à Paris.