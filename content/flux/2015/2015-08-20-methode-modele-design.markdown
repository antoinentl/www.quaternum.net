---
layout: post
title: "Méthode, modèle de pensée pour le design"
date: "2015-08-20T22:00:00"
comments: true
published: true
categories: 
- flux
slug: methode-modele-design
---
>La forme des lettres n'est plus gravée, ni dessinée mais (d)écrite.  
[...]  
Le code devient ici une méthode, un modèle de pensée pour le design.  
[David Vallance, "Décrire des modèles", *.TXT 2*](http://editions-b42.com/books/txt-2/)

Cet article de David Vallance fait le point sur ses recherches sur [MetaFont](https://fr.wikipedia.org/wiki/Metafont), un texte passionnant.