---
layout: post
title: "Le faux débat entre les applications natives et le web"
date: "2015-08-05T22:00:00"
comments: true
published: true
categories: 
- flux
slug: applications-natives-et-web
---
>Some people spend a lot of time on messaging and social apps on mobile. And it is perfectly fine. I personally do not want to use a Web browser for accessing my mail or messaging my friend. I do want a Web browser for reading, linking, sharing links.  
[Karl Dubost, The Wrong Debate About Native And Web](http://www.otsukare.info/2015/08/05/native-mobile-wrong-debate)

L'usage des *applications natives* correspond à des pratiques particulières, comme la communication (mails, réseaux sociaux) ou le jeu, alors que le web est plutôt destiné à la consultation d'informations et à la lecture. Il faut donc plutôt analyser l'évolution des usages -- ouvrir une application comme on allume la télé -- plutôt que conclure que les applications natives doivent être préférées au web.