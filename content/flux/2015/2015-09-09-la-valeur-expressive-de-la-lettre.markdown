---
layout: post
title: "La valeur expressive de la lettre"
date: "2015-09-09T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Ceci pour vous montrer que la lettre a, consciemment à mon sens, peut-être plus inconsciemment au sens d’autres personnes, une valeur expressive, que souvent on néglige de lui donner, tout au moins dans trop d’œuvres de mon pays.  
[Charles Peignot, Esthétique et typographie](http://strabic.fr/Charles-Peignot-Esthetique-et-Typographie)

Peut-être pouvons-nous espérer que l'on *néglige* beaucoup moins la valeur expressive de la lettre, aujourd'hui, dans le pays de Charles Peignot.