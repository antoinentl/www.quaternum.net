---
layout: post
title: "La différence entre responsif et adaptatif"
date: "2015-11-15T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Responsive sites and adaptive sites are the same in that they both change appearance based on the browser environment they are being viewed on (the most common thing: the browser's width).  
Responsive websites respond to the size of the browser at any given point. No matter what the browser width may be, the site adjusts its layout (and perhaps functionality) in a way that is optimized to the screen. Is the browser `300px` wide or `30000px` wide? It doesn't matter because the layout will respond accordingly. Well, at least if it's done correctly!  
Adaptive websites `adapt` to the width of the browser at a specific points. In other words, the website is only concerned about the browser being a specific width, at which point it adapts the layout.  
[Geoff Graham, The Difference Between Responsive and Adaptive Design](https://css-tricks.com/the-difference-between-responsive-and-adaptive-design/)

Une définition, et des explications techniques plus poussées. Quelques points essentiels à retenir :

- *responsive* :
  + unités relatives (pourcentage par exemple) ;
  + vectorisation ;
  + fluide ;
  + ...
- *adaptative* :
  + unités statiques (pixels par exemple) ;
  + pixel ;
  + statique ;
  + ...

Pourquoi vouloir faire à ce point la différence ? Parce qu'en terme de conception, cela a beaucoup d'implications !