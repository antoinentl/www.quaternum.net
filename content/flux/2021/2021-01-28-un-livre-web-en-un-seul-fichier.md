---
layout: post
title: "Un livre web en un seul fichier"
date: 2021-01-28
published: true
description: "Un site web de plusieurs pages mais fait d'un seul fichier HTML, cette idée n'est pas neuve mais elle mérite d'être regardée de plus près quand on pense à des livres (numériques) lisibles sur tout support."
categories:
- flux
tags:
- livre
---
> Anchor #links are a good and proper mechanism for navigating [a single document](https://portable.fyi/#2020-11-27-this-blog-is-a-single-html-file), and using `:target` to show and hide content allows for a normal hyperlink behaviour within pseudo-pages, without reinventing any wheel. The desired outcome is a simple, resilient, portable document.  
> Gregory Cadars, Portable HTML, an idea, [https://portable.fyi/#2020-12-06-portable-html-an-idea](https://portable.fyi/#2020-12-06-portable-html-an-idea)

Un site web de plusieurs pages mais fait d'un seul fichier HTML, cette idée n'est pas neuve mais elle mérite d'être regardée de plus près quand on pense à des livres (numériques) lisibles sur tout support.
Un [livre web](https://www.quaternum.net/2016/03/07/le-livre-web-comme-objet-d-edition/) fait d'un seul fichier ?
Un seul fichier, donc facilement _portable_, en utilisant quelques propriétés HTML et CSS, et en embarquant les images en [Base64](https://fr.wikipedia.org/wiki/Base64).

Abrüpt fait cela depuis quelque temps, allant jusqu'à intégrer des scripts pour des interactions plus poussées.

À noter que le rendu — ce fichier HTML qui contient tout — peut très bien être généré avec tout type de _fabriques_.
