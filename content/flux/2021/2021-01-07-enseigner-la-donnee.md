---
layout: post
title: "Enseigner la donnée"
date: 2021-01-07
published: true
description: "Des ressources exceptionnelles pour les domaines du traitement des données, le web sémantique ou encore le stockage (des données)."
categories:
- flux
tags:
- web
---
> [Les formations] étaient orientées autour des technologies du Web sémantique et elles ont peu à peu évolué vers la question des données en général pour essayer d’embrasser aujourd’hui toutes les composantes de la gouvernance des données.  
> [Gautier Poupeau, Des supports pour former à la question de la donnée, http://www.lespetitescases.net/des-supports-pour-former-a-la-question-de-la-donnee](http://www.lespetitescases.net/des-supports-pour-former-a-la-question-de-la-donnee)

Des ressources exceptionnelles pour les domaines du traitement des données, le web sémantique ou encore le stockage (des données).
