---
layout: post
title: "Liberté de création et de publication"
date: 2021-01-19
published: true
description: "Le Web reste un espace de création, d'expérimentation et de publication relativement libre et ouvert."
categories:
- flux
tags:
- web
---
> Sur le Web, personne, absolument personne, n’a d’autorité pour vous dire ce que vous avez le droit de faire ou de ne pas faire. Vous n’avez pas à obtenir l’aval de Google, Apple ou Amazon pour publier votre propre site web. Et vous êtes donc totalement libre de faire ce que vous voulez, comme vous le voulez.
> [Rémi Parmentier, Deux choses que je crois fermement à propos du Web, https://www.24joursdeweb.fr/2020/deux-choses-que-je-crois-fermement-a-propos-du-web/](https://www.24joursdeweb.fr/2020/deux-choses-que-je-crois-fermement-a-propos-du-web/)

Le Web reste un espace de création, d'expérimentation et de publication relativement libre et ouvert.
Je suis un petit peu moins optimiste que Rémi Parmentier, mais quel bel hommage à ce médium que j'aime (et critique) tant.
