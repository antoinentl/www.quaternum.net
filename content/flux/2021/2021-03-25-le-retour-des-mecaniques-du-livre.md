---
layout: post
title: "Le retour des Mécaniques du livre"
date: 2021-03-25
published: true
description: "La première de la Mécanique du livre était passionnante, le nouveau forme _des_ Mécaniques du livre est plus ouvert à la discussion, et peut-être au débat."
categories:
- flux
tags:
- livre
---
> Nous voici de retour pour une nouvelle saison ! Nouveau nom (passage au pluriel), nouveau logo, nouvelle équipe, nouveau format !  
> Éditions du commun, Mécaniques du livre, [https://www.editionsducommun.org/blogs/podcasts/](https://www.editionsducommun.org/blogs/podcasts/)

La première de la Mécanique du livre était passionnante, le nouveau forme _des_ Mécaniques du livre est plus ouvert à la discussion, et peut-être au débat.
C'est chouette si je puis dire.