---
title: Arguments discutables
date: 2021-09-27
description: "Intéressant de voir que même iA Writer tente de gagner des utilisateurs et des utilisatrices du côté de l'écriture/édition scientifique."
categorie:
- flux
tags:
- publication
---
> Markdown helps users focus on writing instead of browsing formatting options. This is useful in the context of lengthy academic papers. iA Writer doubles down on focus thanks to its clearcut interface, handy key binds, and the original Focus Mode that keeps attention on the sentence or paragraph the user is typing at any given moment.  
> iA, Markdown in Academic Writing, [https://ia.net/writer/blog/markdown-in-academic-writing](https://ia.net/writer/blog/markdown-in-academic-writing)

Intéressant de voir que même [iA Writer](https://ia.net/writer) tente de gagner des utilisateurs et des utilisatrices du côté de l'écriture/édition scientifique.
À noter que iA arrive sur ce terrain bien après des solutions _complexes_ comme Markdown+Pandoc, et d'autres plus accessibles comme Zettlr ou Stylo.
Et surtout le plus étonnant c'est l'absence de prise en compte des besoins du milieu académique : une bibliographie structurée et dynamique.

Je ne suis pas convaincu par certains arguments, mais il faut reconnaître que le fait de proposer des modèles typographiquement élégants est un atout.
Il y a une volonté de rendre le _web to print_ une réalité voir même un argument en terme de facilité de compréhension et de développement d'un thème spécifique.
Reste à connaître le moteur ou le convertisseur qui permet de produire le format PDF.
