---
layout: post
title: "Technique d'impression et mots"
date: 2021-02-15
published: true
description: "Cas intéressant — et très bien raconté par Nicolas Morin —, de l'influence des contraintes techniques sur le contenu."
categories:
- flux
tags:
- publication
slug: technique-d-impression-et-mots
---
> Caxton a un autre souci : un livre manuscrit est justifié à gauche et à droite par la main du copiste, ce qui est difficile à réaliser avec une presse. Les caractères métalliques ont une taille fixe, on peut jouer un peu des espaces entre les mots, mais si on veut finir aligné à droite, ça donne vite un aspect étrange à la ligne, qui se trouve pleine de trous. Il y a un autre moyen de remédier à ce problème, à cette époque où l’orthographe n’est pas encore fixée : faire varier la graphie des mots. Besoin de plus ou de moins de place ? Écrivez _pity_ (pitié, dommage), ou _pitty_, ou même _pittye_, avec un e muet à la fin. Ou, dans l'extrait du dessus, _then_ transformé en _thenne_. L’esthétique de la page compte finalement plus que l’orthographe.  
> Nicolas Morin, Eggys ou eyren ?, [https://www.nicomo.io/cuisine/eggys-ou-eyren/](https://www.nicomo.io/cuisine/eggys-ou-eyren/)

Cas intéressant — et très bien raconté par Nicolas Morin —, de l'influence des contraintes techniques sur le contenu.
À des fins de simplification de mise en page dûe à des contraintes de fabrication, les mots sont modifiés, voilà une idée de William Caxton qui n'est pas si éloignée de certaines techniques du web.
