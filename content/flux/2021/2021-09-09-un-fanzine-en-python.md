---
title: Un fanzine écrit en Python
date: 2021-09-09
description: "Ce fanzine expérimental, résolument dans le domaine de l'art numérique, ou de la vulgarisation scientifique artistique, est une exploration algorithmique."
categories:
- flux
tags:
- publication
---
> _Plus Equals_ is a quarterly zine in which Rob Weychert explores algorithmic art with a focus on combinatorics.  
> Rob Weychert, Plus Equals, [https://plusequals.art](https://plusequals.art)

Ce fanzine expérimental, résolument dans le domaine de l'art numérique, ou de la vulgarisation scientifique artistique, est une "exploration algorithmique".
Pour chaque numéro la source des programmes est disponible en ligne.
