---
layout: post
title: "Liste de lecture (automatisée)"
date: 2021-02-05
published: true
description: "Katy DeCorah décrit comment elle utilise la plateforme GitHub pour suivre ses propres lecture (de livres), en automatisant le tout avec le déploiement continu de GitHub."
categories:
- flux
tags:
- outils
---
> I built read action, a GitHub Action, that tracks the books that you’ve read by storing the book metadata in a yaml file. Services like Goodreads exist, but I want to own my data.  
> [Katy DeCorah, Track your books with GitHub Actions](https://katydecorah.com/code/read/)

Katy DeCorah décrit comment elle utilise la plateforme GitHub pour suivre ses propres lecture (de livres), en automatisant le tout avec le déploiement continu de GitHub.
Tentant.

Via [Robin Rendle](https://www.robinrendle.com/notes/katys-book-bloggin-setup).