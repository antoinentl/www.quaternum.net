---
layout: post
title: "À propos d'italique"
date: 2021-02-03
published: true
description: "Même si je doute avoir le temps de lire cette thèse, je mets de côté ce qui me semble être absolument passionnant."
categories:
- flux
tags:
- typographie
slug: a-propos-d-italique
---
> This thesis contains the complete results of a five-year study into the italic design process.  
> [Victor Gaultney, Designing italics, https://gaultney.org/jvgtype/italics/designing-italics/](https://gaultney.org/jvgtype/italics/designing-italics/)

Même si je doute avoir le temps de lire cette thèse, je mets de côté ce qui me semble être absolument passionnant.
