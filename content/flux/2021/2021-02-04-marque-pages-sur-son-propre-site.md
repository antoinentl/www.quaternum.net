---
layout: post
title: "Marque-pages sur son propre site"
date: 2021-02-04
published: true
description: "Comment récupérer les données du service Pocket et les afficher sur son propre site web ?"
categories:
- flux
tags:
- outils
---
> I have been using Pocket since many years to organize my bookmarks and reading list and thought it would be great to show them on my blog where I use Eleventy. In this article you will learn how to get your data from Pocket and show them on your Eleventy site.  
> [Michael Scharnagl, Integrate Pocket in Eleventy](https://justmarkup.com/articles/2021-01-19-integrate-pocket-in-eleventy/)

Comment récupérer les données du service Pocket et les afficher sur son propre site web ?
Exemple avec le générateur de site statique Eleventy.
Loin d'être prouesse technique incroyable, la démarche de récupération de données est toujours intéressante.
