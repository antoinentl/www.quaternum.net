---
title: "Adieu InDesign et les éditeurs XML"
date: 2021-09-30
description: "Adam Hyde décrit une situation assez unique depuis de nombreuses années : le remplacement des outils classiques d'édition."
categorie:
- flux
tags:
- publication
---
> We can now achieve ‘InDesign level’ output  
> Adam Hyde, One Enormous Step at a Time – Now JATS, [https://www.adamhyde.net/one-enormous-step-at-a-time-now-jats/](https://www.adamhyde.net/one-enormous-step-at-a-time-now-jats/)

Adam Hyde décrit une situation assez unique depuis de nombreuses années : le remplacement des outils classiques d'édition.
Grâce à une série d'outils développés par Coko et coordonnés en grande partie par Adam Hyde, il est désormais possible d'obtenir une qualité de publication (notamment du PDF) équivalente à celle réalisée avec des outils comme InDesign.
Si le résultat est d'aussi bonne qualité, en revanche le mode de conception et de fabrication est très différent : _dessiner_ une page ne se fait pas avec les mêmes processus ou méthodes.

Dans le reste de ce billet Adam Hyde prend surtout position pour défendre le fait que du XML peut être obtenu à partir de HTML, sans passer par un éditeur XML.
Et il a bien raison (même si je sais que beaucoup de personnes ne sont pas d'accord avec cela).

> Generally folks have considered the creation of XML to be a complex problem, requiring complex solutions. Need XML? Ask the XML experts how to do it. What will they say? Get a tool that manages XML. This is ‘XML thinking’. It doesn’t allow for out of the XML-box thinking. It is also overly complex.