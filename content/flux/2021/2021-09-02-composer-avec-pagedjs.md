---
title: "Composer avec Paged.js"
date: 2021-09-02
description: "Nicolas Taffin détaille avec précision son utilisation de Paged.js pour fabriquer des livres imprimés dont la mise en page est complexe."
categories:
- flux
tags:
- livre
---
> Le designer dispose ainsi de deux feuilles de style : une pour la composition générale, et donc “automatique”, applicable à l’ensemble du manuscrit, et cette autre, nommée tweaks.css pour procéder à des ajustements au vu du résultat une fois composé.  
> Nicolas Taffin, Dans les recoins de la double page (Paged.js à la maison, saison 2), [https://polylogue.org/apres-la-page-la-double-page/](https://polylogue.org/apres-la-page-la-double-page/)

Nicolas Taffin détaille avec précision son utilisation de Paged.js pour fabriquer des livres imprimés dont la mise en page est complexe.
Points notables : l'utilisation d'un langage de balisage léger, ici AsciiDoc ; et la volonté d'intégrer l'autrice ou d'autres personnes dans les processus de composition et d'édition.

> Un des gros problèmes de la fabrication numérique de livres est l’articulation des différents moments et acteurs éditoriaux avec les logiciels et formats de fichiers.

Nicolas a présenté cette expérience lors d'une rencontre d'Inachevé d'imprimer : [https://inacheve-dimprimer.net/articles/2021-03-18-nicolas-taffin.html](https://inacheve-dimprimer.net/articles/2021-03-18-nicolas-taffin.html).