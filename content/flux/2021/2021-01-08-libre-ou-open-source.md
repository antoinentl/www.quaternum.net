---
layout: post
title: "Libre ou Open Source"
date: 2021-01-08
published: true
description: "Des ressources exceptionnelles pour les domaines du traitement des données, le web sémantique ou encore le stockage (des données)."
categories:
- flux
tags:
- privacy
---
> Les logiciels dits « open source » sont appréciés pour leur robustesse et leur performance, des considérations pratiques qui sont souvent les seules à guider le développement logiciel. Le mouvement du libre porte avec lui des valeurs inaliénables qui promeuvent une réelle liberté.  
> Louis-Olivier Brassard, Open source v. libre, [https://journal.loupbrun.ca/n/066/](https://journal.loupbrun.ca/n/066/)

Voici une distinction importante à réaliser, que j'utilise moi-même dans mes cours, mais je trouve que Louis-Olivier a trouvé ici des formulations synthétiques et très claires.
J'ajouterais que ces deux concepts ont une histoire, et que l'un (open source) s'appuie sur l'autre (libre) à des fins mercantiles.
