---
layout: post
title: "De la difficulté d'écrire sur le Web"
date: 2021-01-06
published: true
description: "Un nouvel essai de Robin Rendle qui pointe des interrogations très pertinentes au sujet de la place que prennent les lettres d'information face aux sites web, et pourquoi ce média est tant plebiscité actuellement."
categories:
- flux
tags:
- web
slug: de-la-difficulte-d-ecrire-sur-le-web
---
> These are the main problems today but now, because of newsletters, I look back on writing for the web as this clunky, annoying process in comparison. The machine sure is beautiful but it requires so much damn work to get singing.  
> Robin Rendle, Newsletters, [https://www.robinrendle.com/essays/newsletters](https://www.robinrendle.com/essays/newsletters)

Un nouvel essai de Robin Rendle qui pointe des interrogations très pertinentes au sujet de la place que prennent les lettres d'information face aux sites web, et pourquoi ce média est tant plebiscité actuellement.
Que ce soit la facilité de création, d'édition et de diffusion, la puissance du système de notification (le courrier électronique) ou la monétisation, les avantages de la newsletter sont nombreux.
Face à cela Robin Rendle liste plusieurs éléments qui font que créer et maintenir un site web est encore quelque chose de compliqué, même si les évolutions récentes et à venir sont plutôt enthousiasmantes, comme les générateurs de site statique.
Nous avons raté quelque chose avec les flux RSS, cette technologie est encore trop fastidieuse et l'aspect économique y est presque absent (mais est-ce un mal ?).
Il y a encore beaucoup de choses à imaginer, et les dernières années devraient nous rendre optimistes.
