---
title: De la gittérature
date: 2021-09-23
description: "Je n'avais pas vu passer ça, il s'agit d'un moyen pour gérer au mieux la _micro-typographie_ avec le générateur de site statique Hugo."
categories:
- flux
tags:
- édition
---
> L'abrüpt manifeste de l'antilivre -- et de l'antilittérature -- détaille un programme aussi poétique que politique, où la références au mouvement des communs s'accompagne d'un appel à bousculer le verbe, la langue, et plus largement l'institution culturelle dans son ensemble.  
> Servanne Monjour, Abrüpt : vers une Gittérature ?, [https://smonjour.gitpages.huma-num.fr/litterature-git/](https://smonjour.gitpages.huma-num.fr/litterature-git/)

Servanne Monjour dresse le portrait de cette [gittérature](https://www.cyberpoetique.org/gitterature/) en construction, prenant le temps de définir le contexte et les mécanismes à l'œuvre.
Un travail remarquable, à prolonger.