---
title: "La littérature se nourrit de ruptures technologiques"
date: 2021-10-04
description: "Entretien court mais dense avec François Bon, donnant un aperçu de ce qu'est le paysage littéraire aujourd'hui (pluriel et en partie d'avant-garde), relevant des points qui peuvent sembler évidents, mais qu'il est bon de rappeler, et surtout dit comme ça."
categorie:
- flux
tags:
- média
---
> La littérature s’est toujours inventée depuis elle-même, en s’appropriant les ruptures de supports qui lui sont contemporains.  
> François Bon, La révolution numérique de la littérature, [https://esprit.presse.fr/article/francois-bon/la-revolution-numerique-de-la-litterature-43457](https://esprit.presse.fr/article/francois-bon/la-revolution-numerique-de-la-litterature-43457)

Entretien court mais dense avec François Bon, donnant un aperçu de ce qu'est le paysage littéraire aujourd'hui (pluriel et en partie d'avant-garde), relevant des points qui peuvent sembler évidents, mais qu'il est bon de rappeler, et surtout dit comme ça.

> Je suis même assez fasciné par ce qu’inventent, en composition, impression, distribution, ici ou là, des collectifs de jeunes auteurs impliqués aussi sur la scène et les « réseaux » : c’est très sain tout ça.