---
layout: post
title: "Sans générateur"
date: 2021-02-01
published: true
description: "Démarche intéressante de gérer un site entier avec un simple fichier Makefile et sans autre outil."
categories:
- flux
tags:
- publication
---
> Après avoir utilisé et même développé des générateurs de sites statiques pendant des années, j'ai désormais avec un simple Makefile le plaisir de pouvoir contrôler le rendu sans prise de tête.  
> [ArraKISS, Maintenir un site statique avec un Makefile, https://ybad.name/log/2021-01-27.html](https://ybad.name/log/2021-01-27.html)

Démarche intéressante de gérer un site entier avec un _simple_ fichier Makefile et sans autre outil.
Je trouve tout de même ce positionnement quelque peut trompeur, pour au moins trois raisons :

- produire une page web ne consiste pas à générer un document balisé. On peut par exemple constater l'absence de métadonnées sur cette page web, a priori plus par non prise en compte que par choix ;
- construire un site web n'est pas simplement rassembler plusieurs documents. Les éléments de navigation peuvent être gérés de façon plus qu'ici ;
- personnellement je trouve plus facile d'apprendre à utiliser un générateur de site statique comme Hugo que devoir gérer un fichier Makefile.

Mais la prouesse est belle !
