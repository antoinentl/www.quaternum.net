---
title: Hugo Microtypo
date: 2021-09-16
description: "Je n'avais pas vu passer ça, il s'agit d'un moyen pour gérer au mieux la _micro-typographie_ avec le générateur de site statique Hugo."
categories:
- flux
tags:
- web
---
> That project is a try to build a Hugo module by porting [Jekyll Microtypo](https://github.com/borisschapira/jekyll-microtypo)  
> Hugo Microtypo, [https://github.com/jygastaud/hugo-microtypo](https://github.com/jygastaud/hugo-microtypo)

Je n'avais pas vu passer ça, il s'agit d'un moyen pour gérer au mieux la _micro-typographie_ avec le générateur de site statique Hugo.
Typiquement : les espaces insécables, les guillemets, etc.
Il s'agit en fait [d'une série de règles de remplacement](https://github.com/jygastaud/hugo-microtypo/blob/master/microtypo/layouts/partials/content.html) indiquées avec des regex.
