---
layout: post
title: "Conscience numérique de l'éditeur"
date: 2021-01-17
published: true
description: "La démarche de l'éditeur est intéressante dans cet objectif de s'émanciper numériquement avec comme premier argument de se séparer des outils et des services de Google."
categories:
- flux
tags:
- livre
slug: conscience-numerique-de-l-editeur
---
> Il y a clairement plusieurs objectifs dans cette démarche.
>
> - Une démarche militante : montrer qu’il est possible de faire autrement.
> - Le souhait de mieux maîtriser les données de l’entreprise et de nos clients.
> - Le besoin d’avoir des outils qui répondent au mieux à nos besoins et que nous pouvons faire évoluer.
> - Quitte à développer quelque chose pour nous autant que cela serve à d’autres.
> - Les fonds d’aides publiques retournent au public sous forme de licence libre.
> - Création d’un réseau d’acteurs et actrices culturelles autour de la question du numérique libre.
>
> Albert, La dégooglisation de l’éditeur, [https://framablog.org/2020/11/19/la-degooglisation-de-lediteur/](https://framablog.org/2020/11/19/la-degooglisation-de-lediteur/)

La démarche de l'éditeur est intéressante dans cet objectif de s'émanciper numériquement avec comme premier argument de se séparer des outils et des services de Google.
Mais comme souvent les modifications sont d'abord opérées sur les parties logistiques et organisationnelles, et moins sur les outils de production — ici les traitements de texte et les logiciels de publication assistée par ordinateur.

> Côté création, j’aimerais beaucoup que nous puissions utiliser des outils libres tels que Scribus, Inkscape, Krita ou GIMP plutôt que la suite Adobe. Mais aujourd’hui ces outils ne sont pas adoptés par l’équipe de création car trop compliqués d’utilisation et pas nativement adaptés à l’impression en CMJN.

Il y a à la fois un problème réel de prise en compte de certains critères par des logiciels libres, et de l'autre une culture du logiciel qui pourrait être déportée sur des approches plus _programmatiques_ et modulaires.
