---
layout: post
title: "La block chain expliquée simplement"
date: "2016-06-21T17:00:00"
comments: true
published: true
description: "La block chain expliquée simplement, très utile pour démontrer l'intérêt de cette technologie – permettant notamment de créer une monnaie décentralisée, dont le Bitcoin."
categories: 
- flux
---
>Il rappelle alors les 3 règles suivantes :
>  
>- “Vous devez faire chacun votre tour la vaisselle”
- “À chaque tour de vaisselle que vous réalisez, vous devez vous munir de votre palet et venir le placer en haut du tube”
- “Les autres frères et soeurs doivent ensuite valider le fait que la corvée ait bien été réalisée en ouvrant ensemble le couvercle grâce à leurs clés respectives ce qui a pour conséquence de faire tomber le palet de couleur à l’intérieur du tube. La majorité des clés suffit à faire tomber le palet.
> 
>[Collectif Bam et Block Chain France, La blockchain expliquée à un enfant de 5 ans](http://consocollaborative.com/tribune/la-blockchain-expliquee-a-un-enfant-de-5-ans/)

La block chain expliquée simplement, très utile pour démontrer l'intérêt de cette technologie -- permettant notamment de créer une monnaie décentralisée, dont le Bitcoin.