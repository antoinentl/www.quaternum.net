---
layout: post
title: "Les webfonts en débat"
date: "2016-09-13T12:00:00"
comments: true
published: true
description: "Un débat récurrent dans le milieu du web : les webfonts sont-elles une fausse bonne idée ? Cet article n'apporte pas de réponse, mais pointe -- malgré l'intention initiale de l'auteur -- la complexité de la question..."
categories: 
- flux
---
>But the truth is this: you can’t and won’t be able to count on the local operating system of every device to support all of the languages demanded by a truly worldwide web.  
[Richard Fink, Webfonts on the Prairie](http://alistapart.com/article/webfonts-on-the-prairie)

Un débat récurrent dans le milieu du web : les webfonts sont-elles une fausse bonne idée ? Cet article n'apporte pas de réponse, mais pointe -- malgré l'intention initiale de l'auteur -- la complexité de la question...