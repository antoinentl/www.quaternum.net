---
layout: post
title: "Architecture de l'information"
date: "2016-01-30T12:00:00"
comments: true
published: true
categories: 
- flux
slug: architecture-de-l-information
---
>Depuis sa naissance dans les années 1990, le Web a connu un essor considérable, qui a notamment fait apparaître de nouveaux métiers, qui n’existaient pas auparavant. Parmi eux, nous pouvons citer les webmestres1, les édimestres2, les développeurs et programmeurs, ou encore, plus récemment, les architectes de l’information.  
[Marine Mayon, Fiche de lecture : Architecture de l’information : méthodes, outils, enjeux, J.-M. Salaün et B. Habert](https://mondedulivre.hypotheses.org/4844)

Résumé contextualisé du livre de Jean-Michel Salaün et Benoît Haber sur l'architecture de l'information.

>Les architectes de l’information sont donc les spécialistes de l’organisation et de la repérabilité de l’information.