---
layout: post
title: "Structure et mise en forme"
date: "2016-02-11T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Dans le dépouillement, l'absence de substance se révèle.  
[Karl, Le dépouillement Web](http://www.la-grange.net/2015/11/24/circonstances)

Le web mis à nu, en quelque sorte. Et l'absence de mise en forme -- CSS + JS -- modifie notre appréhension des contenus, notre façon de lire.  
L'importance de s'interroger sur nos pratiques par rapport à un web *omniprésent* -- pour reprendre l'expression de [David](https://larlet.fr/david/blog/2014/un-web-omni-present/) -- et *omnimouvant*.