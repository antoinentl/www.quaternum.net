---
layout: post
title: "Les faces cachées du web"
date: "2016-08-26T17:00:00"
comments: true
published: true
categories: 
- flux
---
>Over the years I’ve come to realize that most difficult part of making websites isn’t the code, it’s the “hidden expectations”, the unseen aspects I didn’t know were my responsibility when I started: Accessibility, Security, Performance, and Empathy.  
[Dave Rupert, Hidden Expectations](http://daverupert.com/2016/08/hidden-expectations/)

Dave Rupert explore les faces cachées du web -- l'accessibilité, la sécurité, la performance et l'empathie --, et pose la question de la difficulté d'être *généraliste* :

>Becoming a generalist in these areas can be difficult.