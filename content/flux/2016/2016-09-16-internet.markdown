---
layout: post
title: "Internet et ses failles"
date: "2016-09-16T12:00:00"
comments: true
published: true
description: "Internet serait détruisible ?... Une partie de l'infrastructure, oui."
categories: 
- flux
slug: internet
---
>C’est très sérieux. C’est LE grand expert de la cybersécurité Bruce Schneier qui l’a écrit sur son blog hier “quelqu’un est en train d’apprendre à détruire Internet” ce sont ses mots. Quand Bruce Schneier dit quelque chose comme ça, il faut mieux y prêter attention…  
[Xavier de La Porte, Quelqu'un se prépare à détruire Internet](http://www.franceculture.fr/emissions/la-vie-numerique/quelquun-se-prepare-detruire-internet)

Internet serait *détruisible* ?... Une partie de l'infrastructure, oui.