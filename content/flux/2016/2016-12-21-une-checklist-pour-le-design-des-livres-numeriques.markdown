---
layout: post
title: "Une checklist pour le design des livres numériques"
date: "2016-12-21T23:00:00"
comments: true
published: true
description: "Après The eBook Performance Checklist, Jiminy Panoz a mis en place une checklist pour le design des livres numériques. Il s'agit encore d'une Progressive Web App, ce qui veut dire que ce site web peut être installé comme une application, et être utilisé hors connexion."
categories: 
- flux
tags:
- design
---
>Design is not just what it looks like and feels like, design is how it works.  
[The eBook Design Checklist](https://friendsofepub.github.io/eBookDesignChecklist/)

Après [The eBook Performance Checklist](https://friendsofepub.github.io/eBookPerfChecklist/), [Jiminy Panoz](http://jiminy.chapalpanoz.com/) a mis en place une checklist pour le design des livres numériques. Il s'agit encore d'une Progressive Web App, ce qui veut dire que ce site web peut être installé comme une application, et être utilisé hors connexion.