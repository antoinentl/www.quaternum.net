---
layout: post
title: "Longseller"
date: "2016-02-09T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Devenir un auteur local *sur internet*, c’est pour moi essayer de prouver qu’on n’est pas obligé de chercher la *starification* dans la création, qu’on n’est pas obligé de souffrir du syndrome du *bestseller*. Qu’on peut chercher des voies modestes sans en éprouver ni gêne ni honte — simplement un puissant sentiment partagé d’ancrage dans une réalité ; un immense enthousiasme aussi. Et me donner une chance, à ma mesure et à mon rythme, de faire l’expérience du *longseller*.  
[Neil Jomunsi, Contre le bestseller, créer local](http://page42.org/contre-le-bestseller-creer-local/)

Neil Jomunsi entrevoit la possibilité de créer une communauté, des communautés, autour de créatifs et de créations. Beau projet à taille humaine et pourtant *numérique* -- en tout cas diffusé via le web --, qui est déjà en place dans une certaine mesure, et notamment via les carnets web.