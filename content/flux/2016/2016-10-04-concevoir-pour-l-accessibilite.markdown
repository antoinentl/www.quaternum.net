---
layout: post
title: "Concevoir pour l'accessibilité : à faire et ne pas faire"
date: "2016-10-04T17:00:00"
comments: true
published: true
description: "Six posters pour concevoir en prenant en compte l'accessibilité : ce qu'il faut faire et ce qu'il faut éviter dans des cas pratiques et concrets pour les personnes en situation de handicaps -- dont l'autisme ! Clairs, concis, des outils très utiles !"
categories: 
- flux
slug: concevoir-pour-l-accessibilite
---
>The dos and don’ts of designing for accessibility are general guidelines, best design practices for making our services accessible.  
[Karwai Pun, Dos and don'ts on designing for accessibility](https://accessibility.blog.gov.uk/2016/09/02/dos-and-donts-on-designing-for-accessibility/)

Six posters pour concevoir en prenant en compte l'accessibilité : ce qu'il faut faire et ce qu'il faut éviter dans des cas pratiques et concrets pour les personnes en situation de handicaps -- dont l'autisme ! Clairs, concis, des outils très utiles !

Une version française de ces posters est également disponible : [https://github.com/UKHomeOffice/posters/tree/master/accessibility/posters_fr](https://github.com/UKHomeOffice/posters/tree/master/accessibility/posters_fr).

[Via la veille d'Opquast (Temesis)](http://opquast.com/).