---
layout: post
title: "L'espace insécable"
date: "2016-01-20T22:00:00"
comments: true
published: true
categories: 
- flux
slug: l-espace-insecable
---
>Sur le web, consultable sur des périphériques de tailles variées, avec des préférences utilisateurs diverses, c’est d’autant plus important **qu’on ne peut savoir où se feront les retours à la ligne**.  
[Vincent Valentin, L’espace insécable](http://www.internetactu.net/2015/12/01/avons-nous-besoin-dune-vitesse-limitee-sur-linternet/)

Qu'est-ce qu'un espace insécable ? Comment le gérer sur le web ? Vincent Valentin répond à ces questions de façon très pédagogique, avec des exemples concrets et des astuces -- et quelques interrogations, aussi.