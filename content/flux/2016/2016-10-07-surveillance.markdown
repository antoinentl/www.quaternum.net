---
layout: post
title: "Surveillance"
date: "2016-10-07T22:00:00"
comments: true
published: true
description: "C&F éditions publient le livre de Tristan Nitot, Surveillance://, commencé sur le blog de Tristan il y a plus d'un an, remis en forme et retravaillé pour cette édition. Un livre important pour comprendre ce que nos usages du numérique impliquent aujourd'hui."
categories: 
- flux
---
>C’est ce que je voudrais explorer dans ce petit livre : comment saisir le potentiel positif de l’informatique connectée sans devenir victime de la surveillance de masse. Au-delà des dénonciations, il me semble important de comprendre les ressorts de l’usage de nos traces, mais également de montrer les outils et les méthodes qui permettent de conserver une part de libre arbitre dans nos usages numériques.  
[Tristan Nitot, Surveillance://](http://cfeditions.com/surveillance/)

C&F éditions publient le livre de Tristan Nitot, *Surveillance://*, commencé [sur le blog de Tristan](http://standblog.org/blog/category/Flicage-brouillon) il y a plus d'un an, remis en forme et retravaillé pour cette édition. Un livre important pour comprendre ce que nos usages du numérique impliquent aujourd'hui. (Et j'ai participé à la production de la version EPUB de ce livre.)

Pour en savoir plus sur le livre et l'auteur :

- [le site de l'éditeur sur lequel on peut commander les versions papier et numérique, ou les deux]() ;
- [Framasoft a fait un entretien avec Tristan Nitot, à lire](https://framablog.org/2016/10/05/tristan-nitot-livre-surveillance/).