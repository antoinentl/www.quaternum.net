---
layout: post
title: "Structure d'un site web"
date: "2016-10-27T17:00:00"
comments: true
published: true
description: "Une façon simple de voir ou visualiser un site web avec la feuille de style par défaut de votre navigateur préféré, et donc de voir comment les contenus sont structurés !"
categories: 
- flux
tags:
- outils
slug: structure-d-un-site-web
---
>View a website’s skeleton by resetting all styling to browser default.  
[structure.exposed](https://structure.exposed/)

Une façon simple de *voir* ou visualiser un site web avec la feuille de style par défaut de votre navigateur préféré, et donc de voir comment les contenus sont structurés !