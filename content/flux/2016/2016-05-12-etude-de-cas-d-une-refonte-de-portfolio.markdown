---
layout: post
title: "Étude de cas d'une refonde de portfolio"
date: "2016-05-12T22:00:00"
comments: true
published: true
description: "Julien présente, pas à pas, le cas de la refonte de son portfolio. C'est bien écrit, avec des conseils très pragmatiques."
categories: 
- flux
slug: etude-de-cas-d-une-refonte-de-portfolio
---
>Le fait d’avoir fait de longues phases séparées et d’améliorer le portfolio graduellement m’a semblé être une méthode efficace, et chose rare pour un book perso, le projet ne me sort pas par les yeux et j’ai plaisir à y ajouter des petites fonctionnalités ou à en améliorer des existantes.  
[Julien, Refonte de judbd.com : Étude de cas (1ere partie)](http://mariejulien.com/post/2016/05/03/Refonte-de-judbd.com-%3A-%C3%89tude-de-cas-%281ere-partie%29)

Julien présente, pas à pas, le cas de la refonte de son portfolio. C'est bien écrit, avec des conseils très pragmatiques.

[Dans la deuxième partie](http://mariejulien.com/post/2016/05/05/Refonte-de-judbd.com-%3A-%C3%89tude-de-cas-%282e-partie-%3A-le-CMS%29), il est question de l'utilisation de [Kirby](https://getkirby.com/), le flat CMS bien pensé.