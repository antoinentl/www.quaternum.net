---
layout: post
title: "25 ans de web"
date: "2016-08-10T17:00:00"
comments: true
published: true
description: "Le 6 août 1991, Tim Berners-Lee postait un message annonçant le WorldWideWeb project. 25 ans déjà."
categories: 
- flux
slug: vingt-cinq-ans-de-web
---
>Thank you Tim and thanks to all who have, by their efforts, helped to create the Web  – from its earliest beginnings, to its inestimable impact on our lives now and for all the exciting ways it will continue to evolve in the future.  
[Amy van der Hiel, 25 years ago the world changed forever](https://www.w3.org/blog/2016/08/25-years-ago-the-world-changed-forever/)

Le 6 août 1991, Tim Berners-Lee postait un message annonçant le "WorldWideWeb project". 25 ans déjà.