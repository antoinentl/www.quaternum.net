---
layout: post
title: "De la communication entre un client et un prestataire (appliqué au web)"
date: "2016-02-02T22:00:00"
comments: true
published: true
categories: 
- flux
slug: de-la-communication-entre-un-client-et-un-prestataire
---
>Cher ami, la rédaction des spécifications fonctionnelles & techniques détaillées, permet de définir de manière précise le fonctionnement de votre application. Cela permet à l'ensemble des membres du projet d'avoir une vision précise du projet. Cela évite, par exemple, des phrases comme : “Mais c'est logique”, “Mais tout site e-commerce a un zoom” et bien d'autres, qui ont la fâcheuse tendance à perturber la bonne marche de votre merveilleux projet.  
[Alors toi aussi tu veux un site web ?](http://fuuuccckkk.tumblr.com/post/51512384323/alors-toi-aussi-tu-veux-un-site-web)

Un peu d'humour pour aborder la relation et la communication entre un client et un prestataire dans le domaine du web. Un dialogue -- peut-être pas si fictif -- qui lève des fantasmes et appuie sur des réalités souvent occultées. C'est drôle et juste !