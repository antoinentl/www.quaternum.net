---
layout: post
title: "Fonctionnement d'un navigateur"
date: "2016-02-19T22:00:00"
comments: true
published: true
categories: 
- flux
slug: fonctionnement-d-un-navigateur
---
>Dans cet article j’essaye de comprendre et d’expliquer, en m’appuyant sur de nombreuses sources, le fonctionnement global du navigateur lorsqu’un utilisateur requiert une page web en tapant son nom dans une barre d’adresse.  
[Thibault Mahé, Optimiser le frontend (2) : comprendre le navigateur, de la requête à l’affichage](http://thibault.mahe.io/journal/front-end-comprendre-le-navigateur.html)

Billet très détaillé et très documenté sur le fonctionnement complet d'un navigateur. Très utile de revenir à des bases pour comprendre comment *intégrer*, et comment travailler avec le web.