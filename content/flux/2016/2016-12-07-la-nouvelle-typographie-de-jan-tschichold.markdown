---
layout: post
title: "La Nouvelle Typographie de Jan Tschichold"
date: "2016-12-07T08:00:00"
comments: true
published: true
description: "Les Éditions Entremonde publie une traduction française du livre emblématique de Jan Tschichold, La Nouvelle Typographie, et c'est un événement en soit."
categories: 
- flux
tags:
- typographie
---
>L’ouvrage emblé­ma­ti­que de Tschichold, *La Nouvelle Typographie* (1928) est un véri­ta­ble mani­­feste pour la moder­nité, ana­lo­gue dans son domaine au livre du Corbusier *Vers une archi­­tec­­ture*. Pre­nant la forme d’un manuel, l’auteur y fait table rase de la typo­gra­phie ancienne qui ne trouve grâce à ses yeux que dans son contexte his­to­ri­que, main­te­nant dépassé.  
[Éditions Entremonde, La Nouvelle Typographie de Jan Tschichold](https://entremonde.net/la-nouvelle-typographie)

Les Éditions Entremonde publie une traduction française du livre emblématique de Jan Tschichold, *La Nouvelle Typographie*, et c'est un événement en soit.

Pour rappel, B42 avait publié un livre résumant la querelle entre Max Bill et Jan Tschichold, [Max Bill / Jan Tschichold. La querelle typographique des modernes](http://www.editions-b42.com/books/max-bill-jan-tschichold/), dont la source de ces échanges était justement *La Nouvelle Typographie* et le changement de position de Jan Tschichold. J'avais réalisé une chronologie pour mieux appréhender ce texte : [Max Bill, Jan Tschichold : une chronologie](https://www.quaternum.net/2015/10/06/max-bill-jan-tschichold-une-chronologie/).

Via [Emmanuel](http://emmanuel.clement.free.fr/2016/12/04/notes.html).