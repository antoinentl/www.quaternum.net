---
layout: post
title: "Choisir un CMS"
date: "2016-07-21T17:00:00"
comments: true
published: true
description: "Dommage que Baldur Bjarnason ne mette pas plus en avant l'argument de la maîtrise de l'outil et des données, dans ce billet sur les choix d'un CMS."
categories: 
- flux
---
>From the pros and cons listed above, it seems to me that as soon as a small to medium-sized business or organisation has any budget, Squarespace becomes the natural choice. The rest are either limited in how they integrate with your mailing list (essential) or analytics (less essential but still a common business practice).  
[Baldur Bjarnason, Which CMS/blog system would you choose?](https://www.baldurbjarnason.com/notes/choosing-a-host/)

Dommage que Baldur Bjarnason ne mette pas plus en avant l'argument de la maîtrise de l'outil et des données, dans ce billet sur les choix d'un CMS.