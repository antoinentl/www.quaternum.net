---
layout: post
title: "Livre web hors ligne"
date: "2016-06-08T17:00:00"
comments: true
published: true
description: "Voilà, Jeremy Keith l'a fait : il avait créé il y a déjà plusieurs années un livre web, HTML5 For Web Designers, il est désormais consultable hors ligne (après le chargement d'une seule page). En regardant le script serviceworker.js utilisé par Jeremy, on comprend mieux comment cela fonctionne. Cela ouvre des perspectives immenses pour la lecture numérique !"
categories: 
- flux
---
>I’d love to see more experimentation around online/offline hypertext/books. For now, you can visit HTML5 For Web Designers, add it to your home screen, and revisit it whenever and wherever you like.  
[Jeremy Keith, Taking an online book offline](https://adactio.com/journal/10754)

Voilà, Jeremy Keith l'a fait : il avait créé il y a déjà plusieurs années un *livre web* -- ou *web book* ou encore *online book* --, [HTML5 For Web Designers](https://html5forwebdesigners.com/), ce livre site est désormais consultable hors ligne (après le chargement d'une seule page). En regardant le script `serviceworker.js` ([ici](https://html5forwebdesigners.com/serviceworker.js)) utilisé par Jeremy, on comprend mieux comment cela fonctionne.

Cela ouvre des perspectives immenses pour la lecture numérique !