---
layout: post
title: "Est-ce que le design est devenu trop difficile ?"
date: "2016-06-05T23:00:00"
comments: true
published: true
description: "launching sites and applications based on Bootstrap or any other heavy framework is like using Microsoft Word to send a text message."
categories: 
- flux
---
>Whether you use a framework as part of your design process or not, when it’s time to go public, nothing will ever beat lean, hand-coded HTML and CSS.  
[Jeffrey Zeldman, Has Design Become Too Hard?](http://www.commarts.com/column/has-design-become-too-hard)

Via [HTeuMeuLeu](https://twitter.com/HTeuMeuLeu/status/739030965251678208).

>[...] launching sites and applications based on Bootstrap or any other heavy framework is like using Microsoft Word to send a text message.