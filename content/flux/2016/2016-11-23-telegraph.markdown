---
layout: post
title: "Telegraph"
date: "2016-11-23T19:00:00"
comments: true
published: true
description: "Telegram, l'application de communication chiffrée, lance un outil minimaliste pour publier du contenu, Telegraph. Un titre, un auteur et un contenu, le support de Markdown, et un minimalisme qui dépasse Medium."
categories: 
- flux
tags:
- publication
---
>Today we are launching Telegraph – a publishing tool that lets you create rich posts with markdown, photos, and all sorts of embedded stuff. Telegraph posts get beautiful Instant View pages on Telegram.  
[Telegram, Instant View, Telegraph, and Other Goodies](https://telegram.org/blog/instant-view)

[Telegram](https://telegram.org/), l'application de communication chiffrée, lance un outil minimaliste pour publier du contenu, [Telegraph](http://telegra.ph/). Un titre, un auteur et un contenu, le support de Markdown, et un minimalisme qui dépasse [Medium](https://medium.com/).