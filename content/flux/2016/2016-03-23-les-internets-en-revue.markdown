---
layout: post
title: "Les internets en revue"
date: "2016-03-23T13:00:00"
comments: true
published: true
categories: 
- flux
---
>Il y a deux ans, nous publiions un petit guide des magazines et revues incontournables dédiées aux technologies numériques. Maintenant que certaines de ces revues sont devenues davantage mainstream, à l’instar de dis magazine invité cette saison à commissionner l’exposition Co-Workers au MAM, il est temps de revenir à la charge avec un florilège de nouvelles publications suaves et exotiques.  
[Loup Cellard, Suaves et exotiques revues des internets](http://strabic.fr/Suaves-et-exotiques-revues-des-internets)

Des ressources fortement intéressantes !