---
layout: post
title: "PWA en pratique"
date: "2016-12-12T23:00:00"
comments: true
published: true
description: "Henrik Joreteg aborde la question pratique de l'installation des Progressive Web Apps, et la façon dont les navigateurs et les OS réagissent, très instructif."
categories: 
- flux
tags:
- design
---
>It's a good time to go all in on the web. I can't wait to see what the next few years bring. Personally, I feel like the web is well poised to replace the majority of apps we now get from app stores.  
[Henrik Joreteg, Installing web apps on phones (for real)](https://joreteg.com/blog/installing-web-apps-for-real)

[Henrik Joreteg](https://joreteg.com/) aborde la question pratique de l'installation des Progressive Web Apps, et la façon dont les navigateurs et les OS réagissent, très instructif.

Via [Jiminy](https://twitter.com/JiminyPan/status/808015135919001600).