---
layout: post
title: "Bonnes pratiques des fils d'Ariane"
date: "2016-09-15T12:00:00"
comments: true
published: true
description: "Des conseils à suivre et des écueils à éviter concernant les breadcrumbs -- fil d'Ariane en français --, un élément essentiel de la navigation sur des sites complexes."
categories: 
- flux
slug: bonnes-pratiques-des-fils-d-ariane
---
>All that breadcrumbs do is make it easier for visitors to move around the site, assuming its content and overall structure make sense. It’s one of the few simple things that enhances usability and fosters user comfort. And that’s sufficient contribution for something that takes up only one line in the design.  
[Nick Babich, Breadcrumbs For Web Sites: What, When and How](https://uxplanet.org/breadcrumbs-for-web-sites-what-when-and-how-9273dacf1960#.5ikz844dr)

Des conseils à suivre et des écueils à éviter concernant les *breadcrumbs* -- fil d'Ariane en français --, un élément essentiel de la navigation sur des sites *complexes*.