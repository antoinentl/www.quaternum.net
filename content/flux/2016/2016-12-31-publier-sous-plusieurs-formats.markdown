---
layout: post
title: "Publier sous plusieurs formats"
date: "2016-12-31T12:00:00"
comments: true
published: true
description: "Jeremy Keith nous propose un très bel exemple de publication numérique sous plusieurs formats -- web, EPUB, PDF, audio -- pour son livre Resilient Web Design. Cela est possible pour deux raisons : une distinction entre le fond et la forme, et une ouverture des sources qui permet à n'importe qui de proposer une version."
categories: 
- flux
tags:
- publication
---
>If you think the book should be available in any other formats, and you fancy having a crack at it, please feel free to [use the source files](https://github.com/adactio/resilientwebdesign).  
[Jeremy Keith, The many formats of Resilient Web Design](https://adactio.com/journal/11670)

Jeremy Keith nous propose un très bel exemple de publication numérique sous plusieurs formats -- web, EPUB, PDF, audio -- pour son livre [Resilient Web Design](https://resilientwebdesign.com/). Cela est possible pour deux raisons : une distinction entre le fond et la forme, et une ouverture des sources qui permet à n'importe qui de proposer une version.