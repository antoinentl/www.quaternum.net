---
layout: post
title: "Passer de Mac à Linux"
date: "2016-12-28T12:00:00"
comments: true
published: true
description: "David Dufresne, auteur et réalisateur de documentaires, prend le temps d'expliquer son passage de Mac à Linux : clair et concis, idéal à lire pour quelqu'un qui n'aurait pas encore sauté le pas."
categories: 
- flux
tags:
- libre
slug: passer-de-mac-linux
---
>Et c’est évidemment de politique dont il est question ici.  
[David Dufresne, De Mac à Linux. Parce qu’il n’y a pas d’alternative.](http://www.davduf.net/de-mac-a-linux-parce-qu-il-n-y-a-pas-d)

David Dufresne, auteur et réalisateur de documentaires, prend le temps d'expliquer son passage de Mac à Linux : clair et concis, idéal à lire pour quelqu'un qui n'aurait pas encore sauté le pas.

>De politique pure. De choix des armes. De construire notre monde de demain, *maintenant*.