---
layout: post
title: "Générateur de site statique : cas pratique hors blog"
date: "2016-08-24T17:00:00"
comments: true
published: true
categories: 
- flux
slug: generateur-de-site-statique
---
>Static site generators are wonderful, even when they have to deal with work for which they weren’t initially created.  
[Stefan Baumgartner, Using A Static Site Generator At Scale: Lessons Learned](https://www.smashingmagazine.com/2016/08/using-a-static-site-generator-at-scale-lessons-learned/)

Stefan Baumgartner présente le retour d'expérience d'une utilisation d'un générateur de site statique -- Jekyll --, pour un site web de plus de 2000 pages et 40 auteurs ! La *stack* complète est détaillée, un imposant workflow qui permet de faciliter la rédaction et la gestion des contenus (Markdown + Git), de produire des pages et un site web léger (Jekyll), et de faciliter le travail des développeurs et webmasters (Liquid).  
La partie "Editing With A Static Site Generator" est notamment très instructive !