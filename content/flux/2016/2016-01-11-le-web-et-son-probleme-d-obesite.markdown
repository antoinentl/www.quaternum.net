---
layout: post
title: "Le web et son problème d'obésité"
date: "2016-01-11T22:00:00"
comments: true
published: true
categories: 
- flux
slug: le-web-et-son-probleme-d-obesite
---
>I want to share with you my simple two-step secret to improving the performance of any website.  
1. Make sure that the most important elements of the page download and render first.  
2. Stop there.  
You don't need all that other crap. Have courage in your minimalism.  
[Maciej Cegłowski, The Website Obesity Crisis](http://idlewords.com/talks/website_obesity.htm)

Il s'agit de la transcription d'une intervention de [Maciej Cegłowski](http://idlewords.com/) : le web a un problème, les pages web sont trop lourdes, et ça ne va pas en s'améliorant. Maciej Cegłowski décortique ces questions de chargement, de poids, de performance, de choix techniques et technologiques. Comment et pourquoi le problème n'a-t-il pas été résolu pour le moment ? Quelles solutions envisager ?

>Let’s preserve the web as the hypertext medium it is, the only thing of its kind in the world, and not turn it into another medium for consumption, like we have so many examples of already.  
Let’s commit to the idea that as computers get faster, and as networks get faster, the web should also get faster.

>Keeping the Web simple keeps it awesome.