---
layout: post
title: "Deux kits pour des projets à base de site statique"
date: "2016-11-15T18:00:00"
comments: true
published: true
description: "Deux kits pour démarrer facilement, et rapidement, un projet à base de générateur de site statique : avec Middleman ou Jekyll."
categories: 
- flux
tags:
- outils
---
>Proteus is a collection of useful starter kits to help you launch your static site or prototype faster.  
[Proteus](http://thoughtbot.github.io/proteus/)

Deux kits pour démarrer facilement, et rapidement, un projet à base de générateur de site statique : avec Middleman ou Jekyll.

Via [Jamstatic-fr](https://twitter.com/jamstatic_fr/status/798080646920937472).