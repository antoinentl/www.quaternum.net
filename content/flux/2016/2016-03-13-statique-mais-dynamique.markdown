---
layout: post
title: "Statique (mais dynamique)"
date: "2016-03-13T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Revenir à des choses simples et performantes, c’est la philosophie du gestionnaire de contenu statique. Beaucoup plus rapides par défaut, plus sécurisés, et donc moins onéreux. Ils connaissent une popularité grandissante, due en partie à leur simplicité d’utilisation et à la facilité avec laquelle ils permettent de faire du déploiement continu, à savoir plusieurs dizaines de mises en production par jour. Cette philosophie qui a toujours animé la communauté open-source anime aujourd’hui celle des startups et des organisation agiles.  
[Frank Taillandier, La mouvance statique](http://frank.taillandier.me/2016/03/08/les-gestionnaires-de-contenu-statique/)

Frank Taillandier parle de *gestionnaires de contenu statique*, et il en parle bien ! J'avais écrit [un article à ce sujet](https://www.quaternum.net/2016/01/09/generateur-de-site-statique-un-modele-alternatif/), bien moins documenté, et moins axé sur la performance.  
Frank aborde des arguments de poids pour comprendre *et* adopter cette voie !

>Non décidément le statique n’est pas qu’une mode destinée à rester confidentielle parmi les hackers. C’est une solution qui vous devriez sérieusement considérée si vous souhaiter atteindre des objectifs de qualité à moindre coût pour des sites de contenus (landing page, documentation, blogs, etc.) ou des single page app. Son écosystème est lui aussi en plein essor et va continuer de se développer.