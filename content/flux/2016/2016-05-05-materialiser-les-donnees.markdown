---
layout: post
title: "Matérialiser les données"
date: "2016-05-05T10:00:00"
comments: true
published: true
description: "Strabic présente les résultats d’un workshop mené par Stéphane Buellet du studio Cheval vert : des expérimentations pour donner de la matière aux données, et archiver – d’une certaine façon – celles-ci. C’est original, parfois disruptif et souvent poétique."
categories: 
- flux
---
>Que ce soit à la surface d’un disque dur ou dans la mémoire d’un serveur web, nos données semblent volatiles et désincarnées. Est-il possible d’imaginer des entités qui représentent ces données à la fois visuellement et de façon physique ? C’est à cette question que se sont attelés les participants du workshop proposé par Stereolux et mené par le studio Chevalvert en mars dernier.  
[Strabic, Et si nous fossilisions nos données ?](http://strabic.fr/Et-si-nous-fossilisions-nos-donnees)

[Strabic](http://strabic.fr/) présente les résultats d'un workshop mené par Stéphane Buellet du [studio Cheval vert](http://www.chevalvert.fr/) : des expérimentations pour donner de la matière aux données, et archiver -- d'une certaine façon -- celles-ci. C'est original, parfois disruptif et souvent poétique.