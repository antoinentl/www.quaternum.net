---
layout: post
title: "Libération des GitLab Pages"
date: "2016-12-27T12:00:00"
comments: true
published: true
description: "GitLab, comme GitHub, permet d'héberger des pages web et d'utiliser des générateurs de sites statiques : un simple git push génère un site web complet, basé sur Jekyll ou d'autres static site generators. Contrairement à GitHub, GitLab peut être installé librement sur un serveur, pour héberger du code et gérer des projets. À partir de janvier cette fonctionnalité de génération de site web sera ajoutée à cette version installable, ce qui veut dire que n'importe qui pourra désormais avoir une instance de GitLab, héberger le code source de son site web, générer les pages HTML de ce site, et héberger le site. Une très bonne nouvelle !"
categories: 
- flux
tags:
- outils
---
>GitLab, une des alternatives à GitHub, a fait un superbe cadeau de Noël à sa communauté : l’intégration des GitLab Pages à la version communautaire de GitLab !  
[Framasky, GitLab libère les GitLab Pages](http://linuxfr.org/news/gitlab-libere-les-gitlab-pages)

[GitLab](https://gitlab.com/), comme GitHub, permet d'héberger des pages web et d'utiliser des générateurs de sites statiques : un simple `git push` génère un site web complet, basé sur Jekyll ou d'autres *static site generators*. Contrairement à GitHub, GitLab peut être installé librement sur un serveur, pour héberger du code et gérer des projets. À partir de janvier cette fonctionnalité de génération de site web sera ajoutée à cette version installable, ce qui veut dire que n'importe qui pourra désormais avoir une instance de GitLab, héberger le code source de son site web, générer les pages HTML de ce site, et héberger le site. Une très bonne nouvelle !