---
layout: post
title: "Brutal Websites"
date: "2016-04-18T22:00:00"
comments: true
published: true
categories: 
- flux
---
>In its ruggedness and lack of concern to look comfortable or easy, Brutalism can be seen as a reaction by a younger generation to the lightness, optimism, and frivolity of todays webdesign.  
[Brutal Websites](http://brutalistwebsites.com/)

Souvent épurés et typographiquement minimalistes, des sites web qui vont -- un peu -- à l'encontre de la mode actuelle. Ça fait du bien.

(Il y a des interviews avec la plupart des designers/propriétaires des sites présentés !)

Via [bachbach](https://twitter.com/bachysoucychymy/status/722005811917168642).