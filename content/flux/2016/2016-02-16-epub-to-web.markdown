---
layout: post
title: "EPUB to web"
date: "2016-02-16T22:00:00"
comments: true
published: true
categories: 
- flux
---
>EPUB as it exists today is not directly usable by a web browser. The web-friendly content files are inside a zip package, which also contains container and package files expressed in a custom XML vocabulary.  
The goal of a browser-friendly format (henceforth EPUB-BFF) is to make it easier for web developers to display EPUB content by [1] allowing an unzipped ("exploded") publication, and [2] by providing an alternative serialization of the information in container.xml and the package document(s).  
[Dave Cramer, Browser-friendly format for EPUB 3.1](https://github.com/dauwhe/epub31-bff)

Des pistes intéressantes pour passer facilement du format EPUB à un format lisible dans un navigateur.