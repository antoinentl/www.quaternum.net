---
layout: post
title: "Icônes + accessibilité : le SVG est encore complexe à utiliser"
date: "2016-03-30T14:00:00"
comments: true
published: true
categories: 
- flux
slug: icones-accessibilite-svg-fontes
---
>Même si les problématiques d’accessibilité du SVG qui peuvent exister devraient être traitées dans les temps à venir, il n’en reste pas moins que les polices d’icônes resteront la solution la plus pertinente à mes yeux pour l’intégration de petits éléments iconographiques.  
**Que ce soit en terme de maintenance, de lisibilité du code ou encore récupération d’un projet par des développeurs tiers, les polices d’icônes gagnent de loin la bataille.**  
[Damien Senger, Police d’icônes vs. SVG : l’exigence d’accessibilité au cœur du combat.](https://medium.com/@iamhiwelo/police-d-ic%C3%B4nes-vs-svg-l-exigence-d-accessibilit%C3%A9-au-c%C5%93ur-du-combat-1e2862310345#.1x13edtxr)

Pour faire court :

- l'utilisation du <abbr title="Scalable Vector Graphics">SVG</abbr> [se répand](https://github.com/blog/2112-delivering-octicons-with-svg), et notamment pour l'affichage d'icônes, jusqu'ici la technique consistait à utiliser des *polices d'icônes* ;
- pour qu'il soit accessible le SVG nécessite plusieurs contraintes ;
- pour le moment si l'on souhaite rendre des icônes *accessibles*, le plus simple est encore d'utiliser des *polices d'icônes* avec quelques bonnes pratiques faciles à implémenter.

>On a tendance à l’oublier mais le code que nous produisons peut être amené à être vu, maintenu ou réutilisé par d’autres développeurs.