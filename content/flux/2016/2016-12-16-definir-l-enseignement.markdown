---
layout: post
title: "Définir l'enseignement"
date: "2016-12-16T23:00:00"
comments: true
published: true
description: "Boris propose une définition de l'enseignement, et je me retrouve dans cette description."
categories: 
- flux
tags:
- métier
slug: definir-l-enseignement
---
>Armés des bons outils méthodologiques et d’un jeu de compétences techniques de base, il me semblerait normal qu’ils deviennent tous, à terme, de meilleurs profesionnels que je ne le suis.  
[Boris Schapira, Enseigner et apprendre](https://borisschapira.com/2016/11/enseigner-et-apprendre/)

Boris propose une définition de l'enseignement, et je me retrouve dans cette description.