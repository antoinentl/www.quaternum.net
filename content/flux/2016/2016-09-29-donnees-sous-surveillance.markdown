---
layout: post
title: "Données sous surveillance"
date: "2016-09-29T14:00:00"
comments: true
published: true
description: "Le transcript complet de la conférence -- lumineuse -- de Thibault Jouannic lors de Paris Web 2016."
categories: 
- flux
---
>Qui exploite nos données ? Comment sont-elles exploitées ? Quelle est notre responsabilité là dedans ? Et quelles sont les mesures à prendre pour protéger la vie privée de "nos" internautes.  
[Thibault Jouannic, Anatomie d'une désintoxication au Web sous surveillance : la conf Paris-Web](https://www.miximum.fr/blog/conf-pw-2017/)

Le transcript complet de la conférence -- lumineuse -- de Thibault Jouannic lors de [Paris Web 2016](http://www.paris-web.fr/).