---
layout: post
title: "Chaîne de publication"
date: "2016-11-11T18:00:00"
comments: true
published: true
description: "Ruth Evans Lane, de Getty Publications, la maison d'édition de The Getty (institution, musée, etc.), expose sa chaîne de publication pour l'édition papier et numérique. Basée notamment sur Markdown, Git et Middleman, ce workflow est original pour une maison d'édition, il reprend les principes des générateurs de sites statiques et de la JAMstack."
categories: 
- flux
tags:
- publication
---
>Our digital publications team came up with an innovative solution, which presents the content as a responsive website and paperback and offers free downloads of the book as EPUB, MOBI, and PDF, as well as exports of catalogue data and image sets. Under the hood for all these is a static-site generator that unites images and style sheets with text files that contain the entire content of the book.  
[Ruth Evans Lane, An Editor’s View of Digital Publishing](http://blogs.getty.edu/iris/an-editors-view-of-digital-publishing/)

Ruth Evans Lane, de Getty Publications, la maison d'édition de The Getty (institution, musée, etc.), expose sa chaîne de publication pour l'édition papier *et* numérique. Basée notamment sur Markdown, Git et Middleman, ce workflow est original pour une maison d'édition, il reprend les principes des générateurs de sites statiques et de la [JAMstack](http://jamstack.org/fr/). J'espère pouvoir faire une série d'articles pour expliquer plus longuement ce principe.

Via [Frank Taillandier](https://frank.taillandier.me/2016/10/02/ne-passons-pas-a-cote-des-choses-simples/).

>The editing craft is timeless, but e-books require new tools and new workflows