---
layout: post
title: "La révolution invisible de l'impression à la demande"
date: "2016-12-30T12:00:00"
comments: true
published: true
description: "Ainsi commence l'article de François Bon. Il y est question de Print On Demand, ou impression à la demande, ce processus d'impression qui permet de fabriquer un livre en un seul exemplaire, s'affranchissant ainsi des contraintes de tirage. Ce changement de paradigme, débuté il y a déjà plusieurs années, permet aux auteurs ou aux éditeurs de publier beaucoup plus facilement, en version papier. Et tout cela est très fortement lié au numérique, principalement au niveau de la chaîne de publication."
categories: 
- flux
tags:
- livre
slug: la-revolution-invisible-de-l-impression-a-la-demande
---
>Le Print On Demand est mort. Bonne nouvelle, diront ceux qui s’en inquiétaient mais n’avaient même pas eu le temps d’apprendre de quoi il s’agissait.  
[François Bon, le Print on Demand est mort (et imprimé)](http://www.tierslivre.net/spip/spip.php?article4376)

Ainsi commence l'article de François Bon. Il y est question de Print On Demand, ou impression à la demande, ce processus d'impression qui permet de fabriquer un livre en un seul exemplaire, s'affranchissant ainsi des contraintes de tirage. Ce changement de paradigme, débuté il y a déjà plusieurs années, permet aux auteurs ou aux éditeurs de publier beaucoup plus facilement, en version papier. Et tout cela est très fortement lié au numérique, principalement au niveau de la chaîne de publication.