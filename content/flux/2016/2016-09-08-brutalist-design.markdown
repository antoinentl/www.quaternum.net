---
layout: post
title: "Brutalist Design"
date: "2016-09-08T12:00:00"
comments: true
published: true
description: "Depuis quelques mois, et surtout depuis la création du site web www.brutalistwebsites.com, le design brutaliste est en vogue. Et pourtant ce n'est pas nouveau, mais, comme l'explique Marc Schenker, probablement plus visible."
categories: 
- flux
---
>As a result, brutalism is compelling, if for nothing else than to provide an alternative to the safe confines of design conventionalism.  
[Marc Schenker, The rise and rise of the brutalist design trend](http://www.webdesignerdepot.com/2016/08/the-rise-and-rise-of-the-brutalist-design-trend/)

Depuis quelques mois, et surtout depuis la création du site web [www.brutalistwebsites.com](http://www.brutalistwebsites.com/), le *design brutaliste* est en vogue. Et pourtant ce n'est pas nouveau, mais, comme l'explique Marc Schenker, probablement plus visible.