---
layout: post
title: "Code = design"
date: "2016-07-19T17:00:00"
comments: true
published: true
description: "À l'origine publié dans le numéro 18 de Graphisme en France, cet article de Kévin Donnot explore les méthodes et outils des designers, et constate que, lorsque la machine n'est pas utilisée comme un outil créatif, il y a des effets d'uniformisation."
categories: 
- flux
---
>Comme Morris, on peut constater aujourd’hui une uniformisation de la production graphique. Par ailleurs, la grande majorité des designers utilisent les mêmes outils, créés par la même société (Adobe). L’homogénéisation des outils et celle de la production ne sont-elles pas liées ?  
[Kévin Donnot, Code = design](http://www.cnap.fr/code-design)

À l'origine publié dans le numéro 18 de Graphisme en France, cet article de Kévin Donnot explore les méthodes et outils des designers, et constate que, lorsque la machine n'est pas utilisée comme un outil créatif, il y a des effets d'uniformisation.

Un texte à relire régulièrement.