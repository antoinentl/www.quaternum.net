---
layout: post
title: "Simplifier"
date: "2016-04-26T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Une forme de décroissance technique pour revenir aux fondamentaux sans pour autant s’en tenir à des expériences utilisateurs et développeurs de la décennie précédente.  
[David Larlet, Simplicité par défaut](https://larlet.fr/david/blog/2016/simplicite-defaut/)

David s'interroge sur la notion de *simplicité* dans le cas du développement web : définition, objectifs, enjeux, méthode, etc. Les implications sont autant du côté de l'expérience utilisateur, de la question de [la dette technique](https://www.quaternum.net/2016/02/24/dette-technique), mais également de l'accessibilité -- en creux.

>Me battre pour une meilleure expérience utilisateur plutôt que contre un framework, chercher à se faire plaisir davantage via ce qui est produit que par un contentement technique.