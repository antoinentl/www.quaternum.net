---
layout: post
title: "Agilité, MVP et réalité"
date: "2016-02-26T22:00:00"
comments: true
published: true
categories: 
- flux
---
>"Hé Monsieur, voici notre première itération, un pneu avant. Qu’en pensez-vous ?"  
Le client réagit de la sorte : "Mais qu’est-ce que vous fichez à me livrer un pneu ? J’ai commandé une VOITURE ! Qu’est-ce que je suis supposé faire avec ça ?"  
[Frank Taillandier, Comprendre le MVP (Produit Minimal Valable ) - et pourquoi je lui préfère un produit rapidement testable, utilisable et adorable](http://frank.taillandier.me/agile/2016/01/28/comprendre-le-mvp/)

Frank traduit un article d’Henrik Kniberg qui vise à mieux expliquer le concept de MVP (Produit minimal valable), et lui préférer l'expression *produit rapidement testable, utilisable et adorable*.  
Article qui contient beaucoup d'exemples très instructifs !