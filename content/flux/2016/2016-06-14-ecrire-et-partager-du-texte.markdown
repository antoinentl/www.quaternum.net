---
layout: post
title: "Écrire et partager du texte"
date: "2016-06-14T17:00:00"
comments: true
published: true
description: "Dave Winer lance son CMS, un outil de blogging simple et efficace : 1999.io. Il faut courir voir les fonctionnalités de cet outil : http://1999.io/about/."
categories: 
- flux
---
>The only file format that works as expected anywhere is no file format, in other words: Plain Text.  
[...]  
Because of its universality and simplicity, Plain Text gets us further than any file format. Yet, Plain Text editors are not made to visually structure text, optimize complex layouts, fiddle with detailed typography, or interlink text bodies. They are great for finding the right words, but fall short the longer the text gets. A contemporary writing process needs to allow us to freely move back and forth between plain words and formatted text, via automated workflows or copy-paste.  
[iA, Multichannel Text Processing](https://ia.net/know-how/multichannel-text-processing)

[IA](https://ia.net/) a écrit un article sur les workflows d'écriture et de publication d'une très grande clarté. Face au <abbr title="What You See Is Waht You Get">WYSIWYG</abbr>, le fichier texte sans formatage est le moyen le plus simple, interopérable et résilient d'écrire et de partager du texte. Et le langage sémantique ultra simple qu'est Markdown permet de transformer ce principe en réalité.

Même si cet article est également une promotion pour l'application [iA Writer](https://ia.net/writer) (iOS, Mac et Android), ce texte est à faire lire à toute personne ayant un lien avec l'écriture et le partage de texte (auteurs, éditeurs de livres, éditeurs de logiciels, universitaires, étudiants, etc.).