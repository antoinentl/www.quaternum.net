---
layout: post
title: "Typography Handbook"
date: "2016-06-15T17:00:00"
comments: true
published: true
description: "Dave Winer lance son CMS, un outil de blogging simple et efficace : 1999.io. Il faut courir voir les fonctionnalités de cet outil : http://1999.io/about/."
categories: 
- flux
---
>A concise, referential guide on best web typographic practices.  
[Typography Handbook](http://typographyhandbook.com/)

Un livre web d'une page sur les bonnes pratiques typographiques sur le web, par [Kenneth Wang](http://kendevdesigns.com/), une bonne introduction à [Professionnal Web Typography de Donny Truong](https://prowebtype.com/).