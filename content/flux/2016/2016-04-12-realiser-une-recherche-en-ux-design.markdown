---
layout: post
title: "Réaliser une recherche en UX design"
date: "2016-04-12T07:00:00"
comments: true
published: true
categories: 
- flux
---
>User experience designers do so many things, but research is, for me, absolutely central to what we do and the contribution we offer to a design process.  
[Andrew Travers, Interviewing for research](http://trvrs.co/book)

Un petit livre, publié initialement en 2013 chez -- feu -- [Five Simple Steps](https://www.fivesimplesteps.com/), qui semble bien utile pour mener des enquête d'usage en <abbr title="User eXperience">UX</abbr> design.