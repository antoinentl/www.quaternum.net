---
layout: post
title: "Frontend designer"
date: "2016-02-18T22:00:00"
comments: true
published: true
categories: 
- flux
---
>I personally think that people who are skilled at frontend design are in a great position to help bridge the divide between the design and development worlds.  
[Brad Frost, Frontend Design](http://bradfrost.com/blog/post/frontend-design/)

L'éternel questionnement sur la définition du *webdesigner* ou *frontend designer*, avec le regard très juste de Brad Frost.

Pour rappel, lire également le billet de STPo : [Je ne suis pas développeur](http://www.stpo.fr/blog/je-ne-suis-pas-developpeur/).