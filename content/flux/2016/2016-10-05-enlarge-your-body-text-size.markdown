---
layout: post
title: "Enlarge your body text size"
date: "2016-10-05T17:00:00"
comments: true
published: true
description: "Un article qui traîte trois points de la question -- essentielle -- de la taille du texte sur le web : constats et histoire, pourquoi les textes devraient être plus, quelques conseils -- typographiques -- pratiques."
categories: 
- flux
---
>This is not about having the biggest body text, because biggest isn’t best. It’s about optimizing for the best reading experience you can possibly give your users, and smaller body text doesn’t fulfill that potential.  
[Christian Miller, Your Body Text Is Too Small](http://xtianmiller.com/notes/your-body-text-is-too-small/)

Un article qui traîte trois points de la question -- essentielle -- de la taille du texte sur le web : constats et histoire, pourquoi les textes devraient être plus, quelques conseils -- typographiques -- pratiques.