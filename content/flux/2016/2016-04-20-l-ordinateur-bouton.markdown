---
layout: post
title: "L'ordinateur bouton"
date: "2016-04-20T07:00:00"
comments: true
published: true
categories: 
- flux
slug: l-ordinateur-bouton
---
>J’aime le fait qu’on uti­lise un sen­seur interne, conçu pour détecter les mou­ve­ments propres de l’appareil, pour un usage externe. J’aime aussi le fait que l’ordinateur devient entiè­re­ment un bouton : on peut appuyer dessus n’importe où.  
[Baptiste, Cogner à son ordi](http://toutcequibouge.net/2016/04/cognez-a-votre-ordi/)

Utiliser certaines fonctions de certains ordinateurs pour faire ce que font d'autres appareils mobiles, un usage *low tech* séduisant par son minimalisme et son ingéniosité.