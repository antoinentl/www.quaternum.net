---
layout: post
title: "What Comes Next Is The Future"
date: "2016-12-26T12:00:00"
comments: true
published: true
description: "Le documentaire de Matt Griffin est disponible depuis plusieurs mois, c'est un documentaire qui parle du web, de son histoire, de l'opportunité qu'il représente, et de la façon dont il est conçu. Il prend la forme d'entretiens avec les figures du web : de Tim Berners-Lee à Jeffrey Zeldman en passant par Brad Frost ou Jeremy Keith."
categories: 
- flux
tags:
- web
---
>What happens next is fun.    
[Jenn Lukas in What Comes Next Is The Future](http://www.futureisnext.com/)

Le documentaire de Matt Griffin, *What Comes Next Is The Future*, est disponible depuis plusieurs mois, en ligne. C'est un film qui parle du web, de son histoire, de l'opportunité qu'il représente, de la façon dont il est conçu et de son avenir. C'est un film qui aborde essentiellement des questions de design, il prend la forme d'entretiens avec les figures du web : de Tim Berners-Lee à Jeffrey Zeldman en passant par Brad Frost ou Jeremy Keith.

Critiquable sur le fond comme sur la forme, ce film est néanmoins incontournable pour toute personne travaillant dans le web, le numérique, dans l'édition, ou dans le domaine très vaste des sciences de l'information.

Le film est en version originale, en langue anglaise, mais il est possible d'afficher des sous-titres -- anglais.

[Via Jiminy](https://twitter.com/JiminyPan/status/812963861011824641).

Voici quelques citations extraites du documentaire :

>The web taught me how to make websites which is a brilliant idea when you think of this thing that can teach people how to extend itself. So the fact that people were sharing and connecting right from the start has had a huge
effect on my life and on my work.  
Jeremy Keith

>Anyone can publish from anywhere in the world.  
Jeffrey Zeldman

>In this industry and especially in front of development because you can view anyone's source, I think it's really neat because we can see how other people are building things and we can talk about how we are building things.  
Jenn Lukas

>So clearly the key to the web was going to be, should be a virtual system which existed on the top of all these existing systems and allowed you to see them all, so they were all part of one great big hypertext book.  
Tim Berners-Lee

>It [Flash] exactly inverted the web's fundamental set up. So the web from its inception prizes ubiquity over consistency. Right, the whole idea is that I don't care, you know, I don't care what kind of device you have, if you have a thing that can consume HTTP and HTML, you will get data. You will get the content. Flash was exactly the opposite.  
Eric Meyer

>CSS Zen Garden was the first sort of universal sense across the community. The first glimmer of how the separation of content and presentation could really work.  
Lyza Danger Gardner

>If my content is separated from my design and my content is semantically marked up it's going to be easier for let's say a blind person who uses the screen reader to navigate my content.  
Jeffrey Zeldman

>The web is flexible by default, right? I mean it's able if nobody designed it. If nobody showed up for work tomorrow and we were just serving plain HTML text documents to the entire internet, any screen any device that understood HTML would be able to understand them. You know, it's the designers and the developers that kind of bring extra complexity to this medium. John Allsopp was talking about this, you know, back in 2001 where he was basically saying that this is this medium that has an ebb and flow to it. You know that this is just truly flexible completely adapted medium and we should design for that.
Ethan Marcotte

>In an app store, there's this intermediary that has to evaluate each contribution, each app before it can be put up there for you to download and use. Imagine if you're publishing a website and you had to... every time you published it or made any change submitted to some authority and then wait days potentially for that change to go through and they could reject it for any reason. We wouldn't have the web that we have today.  
Tantek Çelik

>Watching people understand that the web can be a first-class application platform if only we let it. Watching people's excitement at being able to be a real application without changing anything about how you build and how you deliver it. Just URLs, HTML and the real honest to goodness web all the way down. That has been incredibly gratifying for me.  
Alex Russell

>What happens next is fun.  
Jenn Lukas