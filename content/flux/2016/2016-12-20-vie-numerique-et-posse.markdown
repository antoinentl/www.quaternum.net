---
layout: post
title: "Vie numérique et POSSE"
date: "2016-12-20T23:00:00"
comments: true
published: true
description: "Clochix explique assez clairement que le contrôle de nos données numériques n'est pas simple."
categories: 
- flux
tags:
- privacy
---
>Avoir le contrôle de sa vie numérique n’est donc pas forcément synonyme d’en garantir la pérennité.  
[Clochix, Pérénnité](https://clochix.net/marges/161218-perennite)

Clochix explique assez clairement que le contrôle de nos vies numériques n'est pas simple : placer nos données sur des services tiers ne nous assure pas de les retrouver quelques années plus tard, mais auto-héberger nos données présente le risque de les perdre (arrêt de l'hébergement, perte d'un nom de domaine, etc.)... Pour compléter ce que dit Clochix, on peut aussi adopter la stratégie [POSSE, Publish (on your) Own Site, Syndicate Elsewhere](https://indieweb.org/POSSE) pour une bonne partie de notre vie numérique : déposer ses fichiers d'abord sur nos hébergements personnels, et ensuite envisager de les mettre également sur des services tiers.