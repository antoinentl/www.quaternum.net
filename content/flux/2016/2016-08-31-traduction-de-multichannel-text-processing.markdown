---
layout: post
title: "Traduction de Multichannel Text Processing"
date: "2016-08-31T17:00:00"
comments: true
published: true
categories: 
- flux
---
>Markdown est le format par défaut des contenus dans la plupart des générateurs de site statique et il devrait devenir aussi le format par défaut dans lequel vous rédigez vos notes, vos rapports, vos articles de blog ou vos livres.  
[Frank Taillandier, Traitement de texte multicanal](http://frank.taillandier.me/2016/08/28/traitement-de-texte-multicanal/)

Frank Taillandier a traduit l'article d'iA, [Multichannel Text Processing](https://ia.net/know-how/multichannel-text-processing), quelques jours avant que je publie [la traduction](https://www.quaternum.net/2016/08/31/traitement-de-texte-multicanal/) que j'ai réalisée du même texte, avec l'aide de plusieurs personnes. Heureux hasard qui permettra une fusion des versions !