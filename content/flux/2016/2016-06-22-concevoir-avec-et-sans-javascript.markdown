---
layout: post
title: "Concevoir avec et sans JavaScript"
date: "2016-06-22T17:00:00"
comments: true
published: true
description: "Aaron Gustafson rappelle la difficulté de concevoir des applications – web – avec JavaScript, et de ne pas oublier de prévoir les cas où JavaScript ne sera supporté, ou mal interprété."
categories: 
- flux
---
>We do not control the environment executing our JavaScript code, interpreting our HTML, or applying our CSS. Our users control the device (and, thereby, its processor speed, RAM, etc.). Our users choose the operating system. Our users pick the browser and which version they use. Our users can decide which add-ons they put in the browser. Our users can shrink or enlarge the fonts used to display our Web pages and apps. And the Internet providers that sit between us and our users, dictating the network speed, latency, and ultimately controlling how—and what part of—our content makes it to our users.  
All we can do is author a compelling, adaptive experience, cross our fingers, and hope for the best.  
The fundamental problem with viewing JavaScript as the new VM is that it creates the illusion of control.  
[Aaron Gustafson, A Fundamental Disconnect](https://www.aaron-gustafson.com/notebook/a-fundamental-disconnect/)

Aaron Gustafson rappelle la difficulté de concevoir des applications -- web -- avec JavaScript, et de ne pas oublier de prévoir les cas où JavaScript ne sera supporté, ou mal interprété.  
Cet article a été écrit en septembre 2014, mais il semble toujours aussi pertinent.

>The history of the Web is littered with JavaScript disaster stories. That doesn’t mean we shouldn’t use JavaScript or that it’s inherently bad. It simply means we need to be smarter about our approach to JavaScript and build robust experiences that allow users to do what they need to do quickly and easily even if our carefully-crafted, incredibly well-designed JavaScript-driven interface won’t run.