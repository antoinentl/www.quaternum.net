---
layout: post
title: "Réinventer le web ?"
date: "2016-06-10T17:00:00"
comments: true
published: true
description: "À l'occasion du Decentralized Web Summit, plusieurs acteurs du web ouvert -- dont Tim Berners-Lee ou Brewster Kahle (Internet Archive) -- ont travaillé pour trouver des solutions aux effets de centralisation du web, ou au problème du modèle économique du web basé trop souvent sur la publicité, ou encore aux solutions de paiement."
categories: 
- flux
---
>“The web is already decentralized,” Mr. Berners-Lee said. “The problem is the dominance of one search engine, one big social network, one Twitter for microblogging. We don’t have a technology problem, we have a social problem.”  
[Tim Berners-Lee dans l'article de Quentin Hardy, The Web’s Creator Looks to Reinvent It](http://www.nytimes.com/2016/06/08/technology/the-webs-creator-looks-to-reinvent-it.html)

À l'occasion du Decentralized Web Summit, plusieurs acteurs du web ouvert -- dont Tim Berners-Lee ou Brewster Kahle (Internet Archive) -- ont travaillé pour trouver des solutions aux effets de centralisation du web, ou au problème du modèle économique du web basé trop souvent sur la publicité, ou encore aux solutions de paiement.