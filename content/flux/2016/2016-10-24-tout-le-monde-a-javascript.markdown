---
layout: post
title: "Tout le monde a JavaScript"
date: "2016-10-24T17:00:00"
comments: true
published: true
description: "Le débat est régulier, est-ce qu'il faut partir du fait que tout le monde a JavaScript ? Mauvaise question ! Beaucoup d'éléments sont à prendre en compte, et donc il vaut mieux privilégier une amélioration progressive, ou une utilisation dégressive."
categories: 
- flux
tags:
- design
---
>Progressive enhancement. Because sometimes your JavaScript just won’t work.  
Be prepared.  
[Everyone has JavaScript, right?](http://kryogenix.org/code/browser/everyonehasjs.html)

Le débat est régulier, est-ce qu'il faut partir du fait que tout le monde a JavaScript ? Mauvaise question ! Beaucoup d'éléments sont à prendre en compte, et donc il vaut mieux privilégier une amélioration progressive, ou une utilisation dégressive.

À lire également, [That “JavaScript not available” case](http://www.christianheilmann.com/2011/12/06/that-javascript-not-available-case/) par Christian Heilmann.