---
layout: post
title: "La lecture, l'écriture et l'hypertexte"
date: "2016-09-22T19:00:00"
comments: true
published: true
description: "Citation d'Alessandro Ludovico."
categories: 
- flux
slug: la-lecture-l-ecriture-et-l-hypertexte
---
>Une véritable révolution s'est bel et bien opérée dans nos pratiques de lecture et d'écriture, moins par le fait d'un nouveau medium que par celui d'un nouveau concept appliqué à un nouveau medium. Ce n'est pas l'ordinateur lui-même qui a changé pour toujours la linéarité du texte -- c'est la possibilité, grâce à des logiciels, de créer dans l'espace numérique abstrait une structure de texte absolument nouvelle et fonctionnelle : l'hypertexte.  
[Alessandro Ludovico, Post-Digital Print, page 29](http://editions-b42.com/books/post-digital-print/)