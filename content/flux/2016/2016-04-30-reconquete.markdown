---
layout: post
title: "Reconquête"
date: "2016-04-30T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Reconquérir une meilleure attention à notre environnement immédiat, sans pour autant sacrifier les avantages de certaines connexions lointaines, tel est peut-être le défi principal qui nous fait face.  
[Yves Citton, Notre-Dame-des-Média, Ceci tuera ceux-là, qui ne mourront pas](http://www.esadse.fr/fr/post-diplome/171012-la-revue-azimuts?p=171012-la-revue-azimuts)

Dans le numéro 43 de la revue [Azimuts](http://www.esadse.fr/fr/post-diplome/171012-la-revue-azimuts?p=171012-la-revue-azimuts) -- "The end of the world as we know it" -- Yves Citton propose une lecture contemporaine du chapitre de Victor Hugo, "Ceci tuera cela" -- *Notre-Dame de Paris*, Livre II, chapitre 2 --, également présent dans ce numéro d'Azimuts. Un texte incontournable, puissant et moteur, pour qui s'intéresse aux problématiques du numérique -- web, internet, IoT, etc.