---
layout: post
title: "The New Web Typography"
date: "2016-08-22T22:00:00"
comments: true
published: true
categories: 
- flux
---
>According to Tschichold, however, books designed in the 19th century were for the people and the technology of the time; they solved problems that were entirely inapplicable to the needs of his own century a hundred years later.  
[Robin Rendle, The New Web Typography](https://robinrendle.com/essays/new-web-typography/)

En février 2016, Robin Rendle proposait un long article sur la "nouvelle typographie web". En partant de *La nouvelle typographie* de Jan Tschichold, puis en abordant la question du design numérique, Robin Rendle interroge notre rapport à la typographie sur le web et la façon dont nous mettons en forme les textes que nous lisons sur écran.

Lecture, édition, typographie, design, les points d'entrée de ce texte sont nombreux.

Quelques citations :

>[...] with the web, it’s hard not to dream of a Utopian media format.

>I’m interested in what happens to typography, or the mechanical process of setting letters into legible text, when we design interfaces on top of flimsy networks and unpredictable browsing environments. I think these technical implications have drastic consequences on how designers and developers ought to see the art of typography itself.

>[...] I think we need a guide of some description, we need rules for this new form of typography and in order to make these rules I think we need to return to Jan Tschichold and his “common, everyday book.”

>Principles of the New Web Typography:  
> 
>1. We must prioritise the text over the font, or semantics over style.
2. We ought to use and/or make tools that reveal the consequences of typographic decisions.
3. We should acknowledge that web typography is only as strong as its weakest point.

>The New Web Typography demands to be seen; bad typography is better than none at all.

