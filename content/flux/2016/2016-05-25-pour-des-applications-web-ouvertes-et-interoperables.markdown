---
layout: post
title: "Pour des applications web ouvertes et interopérable"
date: "2016-05-25T17:00:00"
comments: true
published: true
description: "Suite à l'événement Google I/O, Jeremy Keith rappelle ce que devrait être une Progressive Web Apps, en démontrant les incohérences des développements de certaines grandes firmes (Google, Facebook, etc.). Des pistes de bonnes pratiques pour des applications web ouvertes et réellement interopérable."
categories: 
- flux
slug: pour-des-applications-web-ouvertes-et-interoperables
---
>I hope we’ll see more examples of Progressive Web Apps that don’t require JavaScript to render content, and don’t throw away responsiveness in favour of a return to device-specific silos. But I’m not holding my breath. People seem to be so caught up in the attempt to get native-like functionality that they’re willing to give up the very things that make the web great.  
[Jeremy Keith, Regressive Web Apps](https://adactio.com/journal/10708)

Suite à l'événement Google I/O, Jeremy Keith rappelle ce que devrait être une *Progressive Web Apps*, en démontrant les incohérences des développements de certaines grandes firmes (Google, Facebook, etc.). Des pistes de bonnes pratiques pour des applications web ouvertes et *réellement* interopérable.