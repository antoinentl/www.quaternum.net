---
layout: post
title: "Lisibilité et résilience des URLs"
date: "2016-04-13T22:00:00"
comments: true
published: true
categories: 
- flux
---
>What makes a cool URI?  
A cool URI is one which does not change.  
What sorts of URI change?  
URIs don't change: people change them.  
[W3C, Cool URIs don't change](https://www.w3.org/Provider/Style/URI.html)

L'éternel débat sur la forme et la pérennité des URLs -- et donc des URIs -- est ici vite réglé : il apparaît évident que les adresses doivent être compréhensibles, et pas uniquement par les machines, mais dans la pratique c'est plus complexe... Même avec une gestion fine des redirections -- dans le cas d'un changement --, il faut tenter de concevoir des systèmes, des organisations et des arborescences les plus logiques, les plus lisibles, et les plus maintenables possible.

Chrome avait promis la disparition -- visuelle -- du champ URL dans le navigateur, nous n'y sommes pas encore. Et quand bien même, il faudra continuer à concevoir des systèmes compréhensibles afin de favoriser leur résilience.

Via [Nicolas Hoizey](https://nicolas-hoizey.com/).