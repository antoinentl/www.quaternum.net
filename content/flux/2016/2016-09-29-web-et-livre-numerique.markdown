---
layout: post
title: "Web et livre numérique"
date: "2016-09-29T17:00:00"
comments: true
published: true
description: "Jiminy Panoz a proposé une mini-conférence de 15 minutes intitulée 'Pourquoi le web devrait s'intéresser au livre numérique', en explorant des points de convergence entre web et livre numérique. Jiminy a également mis en ligne sa présentation : https://jaypanoz.github.io/pw2016/."
categories: 
- flux
---
>We didn’t learn anything from the web.  
[Jiminy Panoz, Why the web should take a look at eBooks](https://medium.com/@jiminypan/why-the-web-should-take-a-look-at-ebooks-6f14ebea9b4c#.hdq64qwra)

À l'occasion de [Paris Web 2016](http://www.paris-web.fr/), [Jiminy Panoz](http://jiminy.chapalpanoz.com/) a proposé une mini-conférence de 15 minutes intitulée "Pourquoi le web devrait s'intéresser au livre numérique", en explorant des points de convergence entre web et livre numérique. Jiminy a également mis en ligne sa présentation : [https://jaypanoz.github.io/pw2016/](https://jaypanoz.github.io/pw2016/).