---
layout: post
title: "Internet est un espace de décision"
date: "2016-07-28T17:00:00"
comments: true
published: true
description: "Citation de Xavier de La Porte"
categories: 
- flux
---
>Internet, ce n'est pas seulement le téléphone ou la télévision. Internet est un espace où l'on exerce sa décision, en permanence. Celle de cliquer ici ou là, de chercher ceci ou cela, d'aller ici ou là. Si, à la limite, on peut admettre qu'on punisse quelqu'un en le privant temporairement de sa liberté, peut-on vouloir priver quelqu'un de sa capacité à faire des choix, même minuscules, *ad vitam* ? C'est peut-être cela qu'Internet pourrait éviter.  
[Xavier de La Porte, La tête dans la toile](http://cfeditions.com/toile/)