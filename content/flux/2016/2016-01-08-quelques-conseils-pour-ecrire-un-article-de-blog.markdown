---
layout: post
title: "Quelques conseils pour écrire un article de blog"
date: "2016-01-08T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Quand nous décidons d'écrire, c'est parce que nous pensons avoir modestement quelque chose à apporter, peu importe la taille de l'audience. Nous écrivons parfois pour nos collègues, pour nos clients, pour nos amis des communautés open source auxquelles nous appartenons, par jeu entre collègues, pour surprendre…  
[Simon Georges, Bien commencer à écrire un article de blog](http://makina-corpus.com/blog/metier/2015/bien-commencer-a-ecrire-un-article-de-blog)

L'idée, la recherche, le plan, le fond, la forme, etc., Simon Georges de Makina Corpus explique très clairement les différentes étapes de rédaction d'un article de blog ou carnet.