---
layout: post
title: "typora, une application wysiwyg pour Markdown"
date: "2016-11-14T18:00:00"
comments: true
published: true
description: "typora est un éditeur Markdown qui permet d'utiliser cette syntaxe tout en la visualisant -- ce qui est d'habitude séparé en deux étapes et deux écrans est ici rassemblé. Cette application fonctionne sur Linux, Windows et Mac."
categories: 
- flux
tags:
- outils
slug: typora
---
>New way to read & write Markdown.  
[typora](http://www.typora.io/)

[typora](http://www.typora.io/) est un éditeur Markdown qui permet d'utiliser cette syntaxe tout en la visualisant -- ce qui est d'habitude séparé en deux étapes et deux écrans est ici rassemblé. Cette application fonctionne sur Linux, Windows et Mac.

Via [Anthony Masure](https://twitter.com/AnthonyMasure/status/797031136375488512).