---
layout: post
title: "Littérature + internet"
date: "2016-09-05T17:00:00"
comments: true
published: true
description: "François Bon fait le point, en fêtant 20 ans de connexion internet, sur la place des auteurs, des livres, des textes sur internet et sur le web."
categories: 
- flux
slug: litterature-plus-internet
---
>[...] moi finalement ce qui me surprendrait le plus, après 8 ans d’Internet et maintenant que l’e-mail s’est généralisé, que les outils techniques se sont simplifiés (comme ce spip sur lequel j’écris ici), pourquoi aussi peu d’auteurs se soient lancés dans un site personnel qui paraîtrait presque comme une politesse : documentation, archives, dialogue, pourquoi aussi peu d’auteurs dans ceux qui me sont les plus proches se sont jamais sentis l’envie de mettre simplement la main à la pâte...  
[François Bon, où en sont les pionniers du Net](http://www.tierslivre.net/spip/spip.php?article69)

François Bon fait le point, en fêtant 20 ans de connexion internet, sur la place des auteurs, des livres, des textes sur internet et sur le web.