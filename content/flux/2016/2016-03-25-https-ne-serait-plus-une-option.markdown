---
layout: post
title: "HTTPs ne serait plus une option"
date: "2016-03-25T17:00:00"
comments: true
published: true
categories: 
- flux
---
>Finally, I can only advice you to consider migrating as soon as possible to HTTPs.  
[Damien Jubeau, Passer au HTTPs est nécessaire, et pas uniquement pour le SEO](http://blog.dareboost.com/fr/2016/03/https-necessaire-pas-uniquement-pour-le-seo/)

Les arguments en faveur d'HTTPs commencent à être nombreux :

- SEO (référencement, en effet Google promet une meilleur référencement pour les sites web en HTTPs) ;
- les navigateurs (Chrome mais aussi Firefox) afficheront bientôt des messages d'alerte pour le HTTP simple ;
- cela semble cohérent avec HTTP 2.

[Version anglaise de l'article](http://blog.dareboost.com/en/2016/03/https-requirement-for-your-website/)