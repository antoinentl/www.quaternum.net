---
layout: post
title: "Quelques liens suite à l'EPUB Summit"
date: "2016-04-15T14:00:00"
comments: true
published: true
categories: 
- flux
slug: quelques-liens-suite-a-l-epub-summit
---
>Publishers, authors, bookstores, distribution channels: the publishing world is facing the digitalisation of content. This opens new opportunities to create and distribute ebooks and other digital publications, but it also generates changes into the value chain, that require professionals to collaborate to be at the edge of innovation.  
[EDRLab, EPUB Summit](http://edrlab.org/edrlab/epub-summit/)

L'EPUB Summit -- qui s'est déroulé les 7 et 8 avril 2016 à Bordeaux -- c'était "le rendez-vous des experts mondiaux du livre numérique", et plus globalement de la publication numérique. Stratégie, rencontres et débats des acteurs de l'industrie du livre et du numérique -- dont le W3C --, discussions autour du format [EPUB](https://fr.wikipedia.org/wiki/EPUB_%28format%29), enjeux des <abbr title="Digital Rights Management">DRM</abbr> et de l'interopérabilité, etc. N'ayant pas pu m'y rendre, voici quelques liens qui synthétisent ces deux jours :

- le programme complet des deux jours de l'EPUB Summit : [http://edrlab.org/edrlab/epub-summit/epub-summit-program/](http://edrlab.org/edrlab/epub-summit/epub-summit-program/) ;
- le discours d'entrée de Virginie Clayssen, directrice en charge de l'Innovation pour le groupe Editis, qui résumé bien les enjeux liés à cet événement : [http://www.archicampus.net/wordpress/?p=2636](http://www.archicampus.net/wordpress/?p=2636) ;
- le flux Twitter complet avec [#epubsummit](https://twitter.com/hashtag/epubsummit?f=tweets&vertical=default&src=hash) : [https://twitter.com/hashtag/epubsummit?f=tweets&vertical=default&src=hash](https://twitter.com/hashtag/epubsummit?f=tweets&vertical=default&src=hash) ;
- un Storify rassemblant les tweets les plus pertinents, réalisé par Virginie Clayssen : [https://storify.com/v_clayssen/epub-summit-bordeaux-6-7-april-2016](https://storify.com/v_clayssen/epub-summit-bordeaux-6-7-april-2016) ;
- le support de présentation d'Ivan Herman du W3C, autour des *Portable Web Publications* : [https://www.w3.org/2016/Talks/EPUBSummit-IH/](https://www.w3.org/2016/Talks/EPUBSummit-IH/) ;
- un bilan de l'EPUB Summit proposé par le site [ActuaLitté](https://www.actualitte.com/) : [https://www.actualitte.com/article/lecture-numerique/bilan-de-l-epubsummit-l-epub-au-dela-du-monde-du-livre/64430](https://www.actualitte.com/article/lecture-numerique/bilan-de-l-epubsummit-l-epub-au-dela-du-monde-du-livre/64430) ;
- un autre résumé sur le site de L'Express, centré sur le format EPUB et son caractère interopérable : [http://www.lexpress.fr/actualites/1/culture/livre-numerique-l-epub-format-libre-et-ouvert-pour-contrer-amazon-et-les-geants-du-web_1782605.html](http://www.lexpress.fr/actualites/1/culture/livre-numerique-l-epub-format-libre-et-ouvert-pour-contrer-amazon-et-les-geants-du-web_1782605.html) ;
- une synthèse intervenant par intervenant sur le site EPUBZone : [http://epubzone.org/news/the-epub-summit-a-triumph-in-bordeaux](http://epubzone.org/news/the-epub-summit-a-triumph-in-bordeaux).