---
layout: post
title: "Designer une interface de recherche avancée"
date: "2016-09-01T17:00:00"
comments: true
published: true
description: "Comment repenser une interface de recherche avancée ? Ce cas concret permet de voir quels choix -- simples -- sont faits, entre des designers, des intégrateurs et des développeurs."
categories: 
- flux
---
>We abandoned the expectation that users are experts. Anyone should be able to figure out how to use AS, even if it’s their first time visiting.  
[Ali Torbati, Death to Complexity: How we Simplified Advanced Search](https://uxdesign.cc/death-to-complexity-how-we-simplified-advanced-search-a9ab2940acf0#.5da8v3p0o)

Comment repenser une interface de recherche avancée ? Ce cas concret permet de voir quels choix -- simples -- sont faits, entre des designers, des intégrateurs et des développeurs.