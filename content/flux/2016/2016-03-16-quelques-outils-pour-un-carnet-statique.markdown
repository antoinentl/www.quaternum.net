---
layout: post
title: "Quelques outils pour un carnet statique"
date: "2016-03-16T23:00:00"
comments: true
published: true
categories: 
- flux
---
>Depuis plusieurs mois, ce blog est propulsé par Jekyll et Codeship. Une page explicative existe, mais elle est « vivante » et si je change quelque chose demain, elle évoluera. J’utilise donc un article daté pour figer un peu les choses, et tant pis pour le contenu dupliqué.  
[Boris Schapira, Un blog avec Jekyll et Codeship](https://borisschapira.com/2016/02/jekyll-codeship/)

Boris Schapira explique tous les outils qu'il utilise pour son carnet statique, du générateur (Jekyll) à la publication (via Codeship) en passant par un peu de technologies *front* (CSS, accessibilité et chargement des polices).