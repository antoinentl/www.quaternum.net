---
layout: post
title: "Global Accessibility Awareness Day"
date: "2016-05-19T17:00:00"
comments: true
published: true
description: "Le 19 mai est donc la Journée Mondiale de Sensibilisation à l’Accessibilité, et c'est une très bonne chose !"
categories: 
- flux
---
>Premièrement, mettons-nous d’accord sur une Journée Mondiale de Sensibilisation à l’Accessibilité (Global Accessibility Awareness Day). Ce sera une journée dans l’année durant laquelle les développeurs Web, partout dans le monde, essaieront d’améliorer la prise de conscience et le savoir-faire en accessibilité du web.  
Ce jour-là, chaque développeur web sera incité à tester au moins une page de son site dans un outil d’accessibilité. Après avoir réparé la page, il sera incité à écrire un billet de blog sur ce qu’il a changé, pour inspirer d’autres à les suivre.  
[Global Accessibility Awareness Day](http://www.globalaccessibilityawarenessday.org/gaadfr.html)

Le 19 mai est donc la Journée Mondiale de Sensibilisation à l’Accessibilité, et c'est une très bonne chose ! Même si il semble que cette initiative soit suivie surtout par des anglophones, il y a beaucoup de choses à découvrir -- par exemple via [#GAAD](https://twitter.com/search?f=tweets&vertical=default&q=%23gaad&src=typd). À noter dans l'espace francophone :

- [Journée Mondiale de sensibilisation à l'accessibilité : un livre Qualité Web à gagner](http://blog.temesis.com/post/2016/05/19/Gaad2016-livre-Qualite-Web) ;
- [GAAD (Global Accessibility Awareness Day) 2016](https://access42.net/GAAD-Global-Accessibility-Awareness-Day-2016.html).