---
layout: post
title: "Netflix ne gère plus de data centers"
date: "2016-02-15T20:00:00"
comments: true
published: true
categories: 
- flux
---
>The process, which started seven years ago, means Netflix no longer uses its own data centres to host its video service and will now use Amazon Web Services, which is run by Amazon. The company will also use some Google services for its archives.  
[Max Slater-Robins, Netflix has completed its move to the cloud — and it's a big win for Amazon](http://uk.businessinsider.com/netflix-completes-its-move-to-aws-2016-2)

Ce n'est pas un détail, Netflix n'utilise désormais que les services d'Amazon -- Amazon Web Services -- ou de Google pour son infrastucture technique. Même si c'est Netflix cela pose beaucoup de questions, est-ce un réel changement de paradigme ?