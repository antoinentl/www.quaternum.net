---
layout: post
title: "Le workflow de GitLab"
date: "2016-11-02T18:00:00"
comments: true
published: true
description: "Plongée dans le workflow de GitLab, la plateforme d'hébergement de codes : étapes des développements, gestion des issues, utilisation de git, etc."
categories: 
- flux
tags:
- design
---
>The GitLab Workflow is a logical sequence of possible actions to be taken during the entire lifecycle of the software development process, using GitLab as the platform that hosts your code.  
[Marcia Ramos, GitLab Workflow: An Overview](https://about.gitlab.com/2016/10/25/gitlab-workflow-an-overview/)

Plongée dans le *workflow* de GitLab, la plateforme d'hébergement de codes : étapes des développements, gestion des *issues*, utilisation de git, etc.