---
layout: post
title: "Créer des applications avec les standards du web ?"
date: "2016-06-13T17:00:00"
comments: true
published: true
description: "Même si les technologies sont disponibles et relativement mûres, il est aujourd'hui très compliqué de construire une application basée sur les standards du web : difficultés de compatibilité avec les navigateurs, lenteur d'évolution des standards, coûts engendrés, etc. Eran Hammer constate, avec regret et un humour très sombre, que développer des applications natives est tout simplement plus simple et la seule solution pour faire vivre sa start-up."
categories: 
- flux
---
>Yes, you can build 2007 websites much better now. They will be consistent across platforms and perform great. But my 12 year olds don’t want 2007 websites. They want 2016 apps.  
[...]  
Web progress is open progress and open is slow, painful, and by definition designed by a committee. It takes about a decade longer for open technology to reach the maturity of closed systems.  
At the end of the day it is not about technology but about cost.  
[Eran Hammer, The Fucking Open Web](https://hueniverse.com/2016/06/08/the-fucking-open-web/)

Même si les technologies sont disponibles et relativement mûres, il est aujourd'hui très compliqué de construire une application basée sur les standards du web : difficultés de compatibilité avec les navigateurs, lenteur d'évolution des standards, coûts engendrés, etc. Eran Hammer constate, avec regret et un humour très sombre, que développer des applications natives est tout simplement plus simple et la seule solution pour faire vivre sa start-up.

>The web is the future. The web will always be the future. But that’s the problem. I need to ship products now.