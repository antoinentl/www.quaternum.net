---
layout: post
title: "Limiter n'est pas étendre"
date: "2016-01-07T22:00:00"
comments: true
published: true
categories: 
- flux
slug: limiter-n-est-pas-etendre
---
>Notre cerveau est formidable mais il a ses limites. Surtout le mien.  
J’ai remarqué que l’utiliser comme une mémoire induisait un stress, celui d’oublier, et empiétait sur ma créativité et mon humeur. Sans compter que j’oubliais malgré tout certaines choses. J’ai donc graduellement confié tout ce qui concernait le fait de “se rappeler” à un outil externe. Mon cerveau est donc entièrement consacré à la créativité. Une véritable source de bonheur et d’apaisement !  
[Lionel Dricot, Comment j’ai étendu les capacités de mon cerveau](https://ploum.net/comment-jai-etendu-les-capacites-de-mon-cerveau/)

Étranges conceptions, et notamment celle du bonheur -- possible uniquement sans stress d'après Lionel -- ou celle de la créativité -- sans contrainte. Une créativité sans contrainte, lisse, je me demande à quoi ça ressemble.  
Je ne pense pas que je pourrais être *apaisé* ou plus *créatif* si je déléguais ma mémoire à des machines. Vouloir limiter le *travail* du cerveau ne va pas étendre ses possibilités, bien au contraire. Plutôt que de mettre en place un workflow -- complexe et reposant sur des services tiers dont j'espère que Lionel a bien lu les conditions générales d'utilisation -- pour tout enregistrer, peut-être qu'il faut se poser la question de l'exhaustivité, et de son utilité.

Dissocier la mémoire de la créativité, vraiment je ne comprends pas.

>Le transhumanisme n’est donc pour moi pas de l’anticipation mais bel et bien une réalité que nous vivons tous au quotidien, à des niveaux différents.