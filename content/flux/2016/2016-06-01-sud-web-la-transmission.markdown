---
layout: post
title: "Sud Web : la transmission"
date: "2016-06-01T23:00:00"
comments: true
published: true
description: "Voici le retour de Sud Web 2016 de Marie, qui insiste sur la dimension humaine de l'événement, et elle a bien raison !"
categories: 
- flux
---
>De la même façon, la transmission – enseigner, partager sur un blog nos réflexions et nos expériences, échanger de vive voix avec plus junior que soi, collaborer avec des apprentis – est très importante.  
C'est même l'un des piliers de notre communauté, initialement autodidacte. Et les évènements comme Sud Web permettent justement d'apprendre et de transmettre, en dehors de nos lieux de travail, et en dehors de toute contrainte de production.  
Le simple fait de se voir et d'échanger dans des lieux neutres, loin de nos bureaux, active déjà notre capacité à prendre du recul sur notre activité.  
[Marie Guillaumet, Sud Web 2016 : en un mot, génial !](http://marieguillaumet.com/sud-web-2016-en-un-mot-genial/)

Voici le retour de [Sud Web 2016](https://sudweb.fr/2016/) de [Marie](http://marieguillaumet.com/), qui insiste sur la dimension humaine de l'événement, et elle a bien raison !