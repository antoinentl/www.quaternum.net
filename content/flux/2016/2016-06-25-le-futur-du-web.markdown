---
layout: post
title: "Le futur du web"
date: "2016-06-25T17:00:00"
comments: true
published: true
description: "Plutôt qu'une vision du futur du web, il s'agit plutôt de constats, positifs ou résignés, mais importants, en vrac : l'importance de développer des applications web plutôt que des applications natives, les effets de répétition liés à certaines évolutions technologiques (Flash, JavaScript), la culture de l'erreur et de l'échec, l'importance du contexte, le best effort, etc."
categories: 
- flux
---
>The web is a mess. It is, like its creators, imperfect. It’s the most human of mediums. And that messiness, that fluidly shifting imperfection, is why it’s survived this long. It makes it adaptable to our quickly-shifting times.  
[Matt Griffin, The Future of the Web](http://alistapart.com/article/the-future-of-the-web)

Plutôt qu'une vision du futur du web, il s'agit plutôt de constats, positifs ou résignés, mais importants, en vrac : l'importance de développer des applications web plutôt que des applications natives, les effets de répétition liés à certaines évolutions technologiques (Flash, JavaScript), la culture de l'erreur et de l'échec, l'importance du contexte, le *best effort*, etc.