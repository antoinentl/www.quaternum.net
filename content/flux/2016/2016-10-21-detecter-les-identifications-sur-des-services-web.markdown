---
layout: post
title: "Détecter les identifications sur des services web"
date: "2016-10-21T17:00:00"
comments: true
published: true
description: "Certaines plateformes web utilisent une méthode simple pour vérifier si l'on est connecté à un service -- Gmail, Paypal, Twitter, etc. À découvrir !"
categories: 
- flux
tags:
- privacy
---
>Without your consent most major web platforms leak whether you are logged in. This allows any website to detect on which platforms you're signed up. Since there are lots of platforms with specific demographics an attacker could reason about your personality, too.  
[Your Social Media Fingerprint](https://robinlinus.github.io/socialmedia-leak/)

Certaines plateformes web utilisent une méthode simple pour vérifier si l'on est connecté à un service -- Gmail, Paypal, Twitter, etc. À découvrir !