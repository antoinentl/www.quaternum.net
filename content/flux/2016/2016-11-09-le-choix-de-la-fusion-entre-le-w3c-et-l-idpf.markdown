---
layout: post
title: "Le choix de la fusion entre le W3C et l'IDPF"
date: "2016-11-09T18:00:00"
comments: true
published: true
description: "La fusion annoncée entre le W3C et l'IDPF est donc désormais officiellement actée, après quelques remous dans le milieu -- oppositions affirmées de grands groupes d'édition."
categories: 
- flux
tags:
- ebook
slug: le-choix-de-la-fusion-entre-le-w3c-et-l-idpf
---
>The member organizations of the International Digital Publishing Forum (IDPF) have overwhelmingly approved, by a margin of 72 (88%)  in favor  to 10 (12%) opposed, a plan proposed by the IDPF Board of Directors to combine IDPF with the World Wide Web Consortium (W3C). The plan will now proceed, with anticipated completion of the combination by January, 2017 still subject to finalization of the necessary definitive agreements.  
[IDPF, IDPF Members Approve Plan to Combine with W3C](http://idpf.org/news/idpf-members-approve-plan-to-combine-with-w3c)

La fusion annoncée entre le <abbr title="World Wide Web Consortium">W3C</abbr> et l'<abbr title="International Digital Publishing Forum">IDPF</abbr> est donc désormais officiellement actée, après quelques remous dans le milieu -- oppositions affirmées de grands groupes d'édition.