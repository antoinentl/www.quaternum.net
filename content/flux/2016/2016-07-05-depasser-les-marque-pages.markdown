---
layout: post
title: "Dépasser les marque-pages"
date: "2016-07-05T17:00:00"
comments: true
published: true
description: "Les marque-pages sont morts, nous dit Zeh Fernandes. Mais, souligne-t-il avec beaucoup de pertinence, à l'heure des graphes et des moteurs de recommandation, pourquoi personne n'a encore développé un système simple de recherche/navigation dans des liens visités/marqués ? La solution n'est peut-être pas dans les marque-pages mais dans la recherche/visualisation d'un historique de navigation ?"
categories: 
- flux
---
>Bookmark services are not enough. They have already failed, becoming weird, accumulating useless features, prioritizing recommendations ​rather​ than helping ​users​ collect and save links.  
[Zeh Fernandes, Bookmark is dead](http://zehfernandes.com/bookmark-is-dead/)

Les marque-pages sont morts, nous dit Zeh Fernandes. Mais, souligne-t-il avec beaucoup de pertinence, à l'heure des graphes et des moteurs de recommandation, pourquoi personne n'a encore développé un système simple de recherche/navigation dans des liens visités/marqués ? La solution n'est peut-être pas dans les *marque-pages* mais dans la recherche/visualisation d'un historique de navigation ?

[Via HTeuMeuLeu](https://twitter.com/HTeuMeuLeu/status/747831779478896640).

>Technological speaking that’s something possible today: we already store a lot of user data, and the natural search was pretty advanced. So please Google, Firefox or any browser vendor: help me navigate my path of hyperlinks and stop ​creating​ this old and painful bookmarking experiences.