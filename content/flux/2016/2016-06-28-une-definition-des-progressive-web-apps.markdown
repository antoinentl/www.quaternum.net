---
layout: post
title: "Une définition des Progressive Web Apps"
date: "2016-06-28T22:00:00"
comments: true
published: true
description: "Frank et Enguerran on traduit l'article de Max Lynch sur les Progressive Web Applications, publié initialement sur le site de Ionic, merci à eux !"
categories: 
- flux
---
>Les *Progressive Web Apps* apportent les fonctionnalités attendues des applications natives à l’expérience de navigation Web sur un mobile, en utilisant des technologies basées sur les standards, et en tournant dans un conteneur sécurisé accessible à tous sur le Web.  
En somme, les *Progressive Web Apps* décrivent toute une série de technologies, de concepts d’architecture et d’API Web qui travaillent de concert pour proposer une expérience similaire aux application natives sur le Web mobile. Voyons ensemble les quelques tenants de base des *Progressive Web Apps*.  
[Que sont les Progressive Web Apps ?](http://frank.taillandier.me/2016/06/28/que-sont-les-progressive-web-apps/)

Frank et Enguerran on traduit l'article de Max Lynch sur les Progressive Web Applications, publié initialement [sur le site de Ionic](http://blog.ionic.io/what-is-a-progressive-web-app/), merci à eux !

>Les *Progressive Web Apps* sont à la fois de nouvelles API, des modèles de conception et du jargon marketing.