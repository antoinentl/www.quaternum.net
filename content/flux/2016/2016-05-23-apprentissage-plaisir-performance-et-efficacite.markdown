---
layout: post
title: "Apprentissage, plaisir, performance et efficacité"
date: "2016-05-23T17:00:00"
comments: true
published: true
description: "Finalement, questionner et réinventer — en partie — les outils que l'on utilise, c'est s'assurer d'apprendre, de prendre du plaisir, d'augmenter les performances de l'outil et d'être efficace — sur le long terme."
categories: 
- flux
---
>Mais alors, à quoi bon réinventer la ratatouille ?  
Le premier argument qui me vient à l’esprit, c’est bien entendu la qualité. En cuisinant moi même une ratatouille, je connais la qualité de mes ingrédients.  
Le second argument, c’est la facilité de personnalisation. Vous n’aimez pas les poivrons ? Aucun problème, je vous fait une ratatouille sans poivron.  
Le dernier argument, c’est le plaisir. Découper soi même amoureusement ses aubergines, courgettes et tomates, et les faire revenir langoureusement à la poêle, c’est quand même autre chose que jeter un sachet plastique surgelé dans le micro-onde.  
[Rémi, Réinventer la roue](http://www.hteumeuleu.fr/reinventer-la-roue/)

Finalement, questionner et réinventer -- en partie -- les outils que l'on utilise, c'est s'assurer d'apprendre, de prendre du plaisir, d'augmenter les performances de l'outil et d'être efficace -- sur le long terme.