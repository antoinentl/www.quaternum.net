---
layout: post
title: "Sud Web : éloge du pourquoi pas"
date: "2016-05-30T23:00:00"
comments: true
published: true
description: "David revient sur Sud Web 2016 en quelques phrases, il résume bien ce qui s'y est passé."
categories: 
- flux
---
>Les éditions de cette conférence se suivent mais ne se ressemblent pas si ce n’est dans leur recherche de singularité. Chaque intervention donne envie d’aller interagir avec l’orateur pour échanger plus que d’ouvrir son *laptop*. Derrière ces sujets non-techniques se cachent des réflexions plus profondes qui n’interrogent plus le *comment* mais le *pourquoi* et de plus en plus le *pourquoi pas* ?  
[David, SudWeb 2016](https://larlet.fr/david/blog/2016/sudweb-2016/)

David revient sur [Sud Web 2016](https://sudweb.fr/2016/) en quelques phrases, il résume bien ce qui s'y est passé.