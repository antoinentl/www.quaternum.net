---
layout: post
title: "Blogging like it's 1999"
date: "2016-06-11T17:00:00"
comments: true
published: true
description: "Dave Winer lance son CMS, un outil de blogging simple et efficace : 1999.io. Il faut courir voir les fonctionnalités de cet outil : http://1999.io/about/."
categories: 
- flux
slug: blogging-like-it-s-1999
---
>I firmly believe people running their own servers will be a big deal in the years to come, as the Minecraft generation comes of age, and at the same time the cloud will continue to grow with hosted apps. It's all going to be big. And people will always need writing tools, and that's what 1999.io is -- the best writing tool I could imagine and implement in 2016.  
[Dave Winer, Where we're at with 1999.io](http://scripting.com/2016/06/08/1311.html)

Dave Winer lance son CMS, un outil de blogging simple et efficace : [1999.io](http://1999.io/). Il faut courir voir les fonctionnalités de cet outil : [1999.io/about/](http://1999.io/about/).