---
layout: post
title: "Une checklist pour la performance des livres numériques"
date: "2016-11-16T18:00:00"
comments: true
published: true
description: "Jiminy Panoz propose une checklist pour la performance des livres numériques au format EPUB : une application web sous forme de liste de points à cocher, accompagnés d'explications précises et synthétiques. Bien pratique pour ne pas oublier les points essentiels de la performance des EPUBs !"
categories: 
- flux
tags:
- outils
---
>Reading Systems constrain your performance budget and if you’re not careful enough, your users will suffer. We’re glad to help with this checklist!  
[The eBook Performance Checklist](https://friendsofepub.github.io/eBookPerfChecklist/)

[Jiminy Panoz](http://jiminy.chapalpanoz.com/) propose une *checklist* pour la performance des livres numériques au format EPUB : une application web sous forme de liste de points à cocher, accompagnés d'explications précises et synthétiques. Bien pratique pour ne pas oublier les points essentiels de la performance des EPUBs !

À noter que cette checklist est un site web qui peut être installé et utilisé hors ligne sur Android, c'est un exemple de Progressive Web Application.