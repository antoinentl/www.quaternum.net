---
layout: post
title: "Vous n'avez pas besoin d'un site web"
date: "2016-03-27T13:00:00"
comments: true
published: true
categories: 
- flux
slug: vous-n-avez-pas-besoin-d-un-site-web
---
>Bref, ne faites pas un site, offrez un service unique, nouveau, tellement captivant et vraiment utile.  
[Fuuuuuuuuuu, Vous n’avez pas besoin d’un site web.](http://fuuuccckkk.tumblr.com/post/141622101754/vous-navez-pas-besoin-dun-site-web)

Réflexion drôle et provocatrice, et pourtant très juste : disposer d'un site web ne veut pas dire grand chose, et ce n'est pas une bonne approche pour un besoin qui peut *potentiellement* prendre la forme d'un site web.

Outil de recherche d'un produit, service de réservation, expérience, instrument de suivi ou de mesure d'audience, dispositif de gestion de comptes clients, etc. Il ne s'agit plus de "présence en ligne" mais de *services et applications*.

[Via Boris Schapira](https://borisschapira.com/2016/03/vous-n-avez-pas-besoin-un-site-web/).