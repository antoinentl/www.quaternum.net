---
layout: post
title: "Retour sur la conception de gov.uk"
date: "2016-06-09T17:00:00"
comments: true
published: true
description: "Joshua Chambers a interviewé Ben Terrett, former head of design at the UK Government Digital Service, à propos de la refonte complète des services en ligne du gouvernement de Grand Bretagne, gov.uk. Ben Terrett explique les choix et les méthodes : centrés et testés sur les utilisateurs, et non sur les intuitions des dirigeants ; avec une équipe réduite mais pluridisciplinaire, et en capacité de prototyper rapidement ; en étant en développement continu et en acceptant les retours d'utilisation (par exemple : suppression des boutons de partage vers les réseaux sociaux car utilisés par seulement 0,1% des utilisateurs) ; etc."
categories: 
- flux
slug: retour-sur-la-conception-de-gov-uk
---
>How did the UK reach an increasingly mobile population? Responsive websites, he replies. “For government services that we were providing, the web is a far far better way… and still works on mobile.”  
Sites can adapt to any screen size, work on all devices, and are open to everyone to use regardless of their device. “If you believe in the open internet that will always win,” he says. And they’re much cheaper to maintain, he adds, because when an upgrade is required, only one platform needs recoding.  
[Joshua Chambers, Why Britain banned mobile apps](https://govinsider.asia/smart-gov/why-britain-banned-mobile-apps/)

Joshua Chambers a interviewé Ben Terrett, *former head of design at the UK Government Digital Service*, à propos de la refonte complète des services en ligne du gouvernement de Grand Bretagne, [gov.uk](http://gov.uk). Ben Terrett explique les choix et les méthodes : centrés et testés sur les utilisateurs, et non sur les intuitions des dirigeants ; avec une équipe réduite mais pluridisciplinaire, et en capacité de prototyper rapidement ; en étant en développement continu et en acceptant les retours d'utilisation (par exemple : suppression des boutons de partage vers les réseaux sociaux car utilisés par seulement 0,1% des utilisateurs) ; etc.

>But if Terrett could leave readers in Asia with one bit of advice, it’s this: remember the user needs. Don’t ever let agencies suggest ideas without justifying why it benefits citizens. The temptation is always there for them to meet internal objectives without building a simple service – and sometimes even complicating the citizen experience with an unnecessary app or web page.