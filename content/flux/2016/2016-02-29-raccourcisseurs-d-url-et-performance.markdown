---
layout: post
title: "Raccourcisseurs d'URL et performance"
date: "2016-02-29T22:00:00"
comments: true
published: true
categories: 
- flux
slug: raccourcisseurs-d-url-et-performance
---
>Les raccourcisseurs d’URL sont devenus incontournables sur le Web depuis quelques années, mais initialement pensés comme outils de facilitation de partage, ils ont beaucoup évolué et sont surtout devenus des outils de *tracking*.  
[...]  
Le cumul des temps passés pour chaque redirection avant d’arriver à la "vraie" requête pour la page voulue est de 1 300 millisecondes, **presque une seconde et demi de perdue** !  
[Nicolas Hoizey, Le gros impact des raccourcisseurs d'URL sur la performance](http://nicolas-hoizey.com/2016/02/un-exemple-d-impact-des-raccourcisseurs-d-url.html)

Le coût du *tracking* est aujourd'hui celui de la performance, ce qui n'est pas forcément une bonne stratégie sur le long terme (rapidité de chargement et *sentiment* associé de l'utilisateur, sécurité, et référencement).