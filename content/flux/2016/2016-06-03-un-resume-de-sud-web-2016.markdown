---
layout: post
title: "Un résumé de Sud Web 2016"
date: "2016-06-03T23:00:00"
comments: true
published: true
description: "Stéphane fait son résumé de Sud Web 2016 : assez complet pour donner une bonne idée de ce qui s'est passé et de ce qui s'y est dit."
categories: 
- flux
---
>Avec Sud Web on ne sait jamais ce qu’on tirera des conférences, et c’est tant mieux. On repart avec des réflexions qui ont rebondi d’un métier à l’autre, on apprend en décloisonnant – c’est ce qui fait toute la pertinence d’un sujet inattendu (la couture dans une conférence de gens qui travaillent dans le Web ?), qui ouvre à de nouvelles idées et permet d’envisager de nouvelles perspectives sur nos métiers.  
[Stéphane, Sud Web 2016 : toujours inattendu, toujours satisfaisant](http://nota-bene.org/Sud-Web-2016-toujours-inattendu-toujours-satisfaisant)

Stéphane fait son résumé de [Sud Web 2016](https://sudweb.fr/2016/) : assez complet pour donner une bonne idée de ce qui s'est passé et de ce qui s'y est dit.