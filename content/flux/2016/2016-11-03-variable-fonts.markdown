---
layout: post
title: "Variable fonts"
date: "2016-11-03T17:00:00"
comments: true
published: true
description: "Les variable fonts arrivent, et elles devraient permettre de résoudre beaucoup de problèmes de design et de performance."
categories: 
- flux
tags:
- typographie
---
>Variable fonts will save significant space in apps and operating systems, cutting down on download speeds, load times, and server costs. They'll make the web typographically richer, and give designers new tools to create beautiful designs. Variable fonts will also help create single fonts that work as well on large screens, like desktops, as they do on modern devices with small screens, like wearables.  
[John Brownlee, Google, Apple, Adobe, And Microsoft Are Quietly Developing A New Type Of Font](https://www.fastcodesign.com/3064032/google-apple-and-microsoft-are-quietly-developing-a-new-type-of-font)

Les *variable fonts* arrivent, et elles devraient permettre de résoudre beaucoup de problèmes de design -- côtés dessinateurs de caractères et designers graphiques -- et de performance -- surtout côté web.