---
layout: post
title: "Une base de données statique"
date: "2016-09-28T19:00:00"
comments: true
published: true
description: "Quelques jours (semaines ?) avant Paris Web, Tim Carry propose un outil pour naviguer et rechercher dans 10 ans de conférences Paris Web. Au-delà de l'utilité sur le fond, c'est un bon exemple de ce que l'on peut faire avec un workflow statique, sans base de données, mais avec un service web axé sur la recherche. Ça va vite !"
categories: 
- flux
---
>I cannot say how much I learned from this event [Paris Web]. I used to say that I could learn more in two days at ParisWeb than in 6 months of technical watch on my own, reading blogs. That's the conference that made me the web developer I am today. It has this incredible energy, this special #sharethelove mood.  
[Tim Carry, Searching the ParisWeb conferences](http://blog.pixelastic.com/2016/09/05/building-a-search-in-all-the-parisweb-conferences/)

Quelques jours (semaines ?) avant Paris Web, [Tim Carry](http://www.pixelastic.com/) propose un outil pour naviguer et rechercher dans 10 ans de conférences [Paris Web](https://www.paris-web.fr/). Au-delà de l'utilité sur le fond, c'est un bon exemple de ce que l'on peut faire avec un *workflow* statique, sans base de données, mais avec un service web axé sur la recherche. Ça va vite !