---
layout: post
title: "L'alternative textuelle en détails"
date: "2016-10-11T17:00:00"
comments: true
published: true
description: "Pour rendre une image accessible, il y a la balise alt, cet article regroupe tout ce qu'il faut savoir sur l'alternative textuelle !"
categories: 
- flux
slug: l-alternative-textuelle-en-details
---
>The most accessible format for any content on the web is plain text.  
[Ire Aderinokun, Alternative Text and Images](https://bitsofco.de/alternative-text-and-images/)

Pour rendre une image *accessible*, il y a l'attribut `alt` de la balise `img`, cet article regroupe tout ce qu'il faut savoir sur l'alternative textuelle !