---
layout: post
title: "Quelle visibilité pour la recherche ?"
date: "2016-05-18T14:00:00"
comments: true
published: true
description: "Olivier Ertzscheid n'est pas content, et il explique très bien pourquoi. Plus globalement, la question du fonctionnement des revues scientifiques pose celle de la visibilité de la recherche, de la réception et de la transmission de celle-ci, et de la réappropriation de ces contenus et données dans un espace public."
categories: 
- flux
---
>Ça me rend dingue. Depuis 20 ans donc (en comptant ma thèse et mon post-doc) je croise souvent plein de collègues qui ont des choses passionnantes à raconter. Et qui ne les racontent pas. Qui ne les raconteront jamais autrement que sous la forme d'un jargon imbitable dans des revues hors de prix pour un public inexistant.  
[Olivier Ertzscheid, Pourquoi je ne publie(rai) plus (jamais) dans des revues scientifiques.](http://affordance.typepad.com//mon_weblog/2016/05/pourquoi-je-ne-publierai-plus-dans-des-revues-scientifiques.html)

Olivier Ertzscheid n'est pas content, et il explique très bien pourquoi. Plus globalement, la question du fonctionnement des revues scientifiques pose celle de la visibilité de la recherche, de la réception et de la transmission de celle-ci, et de la réappropriation de ces contenus et données dans un espace public.