---
layout: post
title: "HTTPS et SEO"
date: "2016-02-08T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Le HTTPS est une excellente chose sur le fond. Le principe même de rendre la navigation plus sécurisée est forcément louable. Une migration peut très bien se passer en étant bien gérée.  
Mais ce type de migration qui va vous coûter du travail ou des sous, vous faire prendre des risques sur le plan SEO, ne la faites pas en espérant un quelconque bonus de Google.  
[Sylvain, Ne passez pas au HTTPS pour de mauvaises raisons...SEO](http://blog.axe-net.fr/ne-passez-pas-au-https-pour-de-mauvaises-raisons-seo/)

Article bien documenté et très clair sur l'intérêt, ou non, de passer en HTTPS pour des questions de référencement.

>Si vous implémentez le HTTPS, faites-le pour vos visiteurs !