---
layout: post
title: "Flux et archive"
date: "2016-11-07T23:00:00"
comments: true
published: true
description: "Robin Rendle réagit suite à l'article de Jeremy Keith, A decade on Twitter, et souligne l'importance de disposer de son propre espace sur le web -- en dehors des silos fermés des réseaux sociaux --, et de ne pas en faire simplement une carte de visite professionnelle. Robine Rendle parle de flux et d'archive, je le rejoins largement sur cette dualité."
categories: 
- flux
tags:
- publication
---
>Hopefully my website will even outlive me, too. That’s because I don’t want my presence online to resolve into a tacky business card once I’m gone — I’m an XYZ in San Francisco — instead, I want it to be an archive of everything that I’ve ever thought was worth keeping around. All the things I’ve pointed at, linked to, discussed, argued about. Once I’m gone I want this place to be an archive of all the things I’ve ever loved, even if they were messy and fragile and a little broken.  
[Robin Rendle, Blogging and Atrophy](https://robinrendle.com/notes/blogging-and-atrophy/)

[Robin Rendle](https://robinrendle.com/) réagit suite à l'article de Jeremy Keith, [A decade on Twitter](https://adactio.com/journal/11436), et souligne l'importance de disposer de son propre espace sur le web -- en dehors des silos fermés des réseaux sociaux --, et de ne pas en faire simplement une carte de visite professionnelle. Robine Rendle parle de flux et d'archive, je le rejoins largement sur cette dualité -- nécessaire.

>Once I’m gone I want this place to be an archive of all the things I’ve ever loved, even if they were messy and fragile and a little broken.