---
layout: post
title: "Scrum en vrai"
date: "2016-01-03T23:00:00"
comments: true
published: true
categories: 
- flux
---
>Plutôt que d'y chercher un changement d'état d'esprit, on n'y prend que la méthodologie.  
[Eric Brehault, Bien démarrer avec Scrum](http://makina-corpus.com/blog/metier/2015/bien-commencer-avec-scrum)

Description très claire, et vision très réaliste d'une méthode *agile* : Scrum.

>Le contrat implicite de Scrum est simple, il ne contient qu'une seule clause: cette démo doit être bien.  
Cette démonstration va tout conditionner : la perception que le client a de notre travail, la confiance qu'il va avoir en nous, et partant, le degré de liberté qu'on aura sur les sprints suivants.