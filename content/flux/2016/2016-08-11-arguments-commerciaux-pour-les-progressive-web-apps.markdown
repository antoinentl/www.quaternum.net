---
layout: post
title: "Arguments commerciaux pour les Progressive Web Apps"
date: "2016-08-11T17:00:00"
comments: true
published: true
description: "Frank Taillandier continue de promouvoir les Progressive Web Apps avec cette traduction d'un article de Jason Grigsby. Ce dernier expose très clairement les avantages commerciaux des PWA, avec exemples à l'appui."
categories: 
- flux
---
>Les Progressive Web Apps sont encore un terrain vierge. C’est maintenant qu’il faut s’y mettre.  
[Jason Grigsby, L’argumentaire commercial pour les Progressive Web Apps](http://frank.taillandier.me/2016/08/09/argumentaire-commercial-pour-les-progressive-web-apps/)

[Frank Taillandier](http://frank.taillandier.me/) continue de promouvoir les *Progressive Web Apps* avec cette traduction d'un article de Jason Grigsby. Ce dernier expose très clairement les avantages commerciaux des PWA, avec exemples à l'appui.