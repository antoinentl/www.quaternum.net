---
layout: post
title: "Livre numérique et dette technique"
date: "2016-02-05T22:00:00"
comments: true
published: true
categories: 
- flux
---
>En observant les ePubs produits de manière industrielle dans les pays à bas coût, il apparaît clairement que la qualité du code produit – un ePub n'est rien d'autre que du code HTML et des CSS, à l'égal d'un site Internet – va empêcher une maintenance aisée au fur et à mesure des années, même pour effectuer des modifications mineures.  
[Lekti, Production de livres numériques (ePub) et dette technique](http://www.lekti-ecriture.com/bloc-notes/index.php/post/2016/La-production-de-livres-num%C3%A9riques-est-elle-homog%C3%A8ne)

J'y avais songé en lisant [*La dette technique* de Bastien Jaillot](http://boutique.letrainde13h37.fr/products/la-dette-technique-bastien-jaillot), Joël Faucilhon l'exprime très bien en quelques paragraphes, comme [Jiminy Panoz](http://jiminy.chapalpanoz.com/) l'évoque régulièrement sur les réseaux. Il est temps de prendre conscience de problème.