---
layout: post
title: "Ce que les livres peuvent apprendre du web (et inversement)"
date: "2016-07-01T17:00:00"
comments: true
published: true
description: "Hugh McGuire tente de définir le livre, de comprendre pourquoi le livre et le web sont s'y éloignés — et pourtant tous les deux si essentiels dans nos cultures basées sur les savoirs —, et d'imaginer comment l'un peut apprendre de l'autre. Un article à lire pour mieux comprendre comment le livre numérique pourrait évoluer."
categories: 
- flux
slug: ce-que-les-livres-peuvent-apprendre-du-web
---
>I am interested in what the Web can learn from books and what books can learn from the Web.  
And I think in order to answer those questions it’s important to try to define what we mean by “books,” what are the qualities that are essential to “a book” that are not essential to, for instance, a website.
[Hugh McGuire, What books can learn from the Web / What the Web can learn from books](https://medium.com/@hughmcguire/what-books-can-learn-from-the-web-what-the-web-can-learn-from-books-64670171908f#.cfq8wfa9j)

Hugh McGuire tente de définir le livre, de comprendre pourquoi le livre et le web sont s'y éloignés -- et pourtant tous les deux si essentiels dans nos cultures basées sur les savoirs --, et d'imaginer comment l'un peut apprendre de l'autre. Un article à lire pour mieux comprendre comment le livre numérique pourrait évoluer.

>Books can learn from the web how to be bounded, but open.  
The web can learn from books how to be open, but bounded.

Cet article est également une introduction aux *webbooks*, ou *web books*, ou [livre web](https://www.quaternum.net/2016/03/07/le-livre-web-comme-objet-d-edition).