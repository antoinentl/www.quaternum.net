---
layout: post
title: "En haut à gauche"
date: "2016-07-12T17:00:00"
comments: true
published: true
description: "Intéressante étude qui prouve qu'un logo aligné au centre ne favorise pas une bonne navigation, et qu'il vaut mieux un logo placé à gauche."
categories: 
- flux
---
>Getting back to the homepage is about 6 times harder when the logo is placed in the center of a page compared to when it’s in the top left corner.  
[Kathryn Whitenton, Centered Logos Hurt Website Navigation](https://www.nngroup.com/articles/centered-logos/)

Intéressante étude qui prouve qu'un logo aligné au centre ne favorise pas une bonne navigation, et qu'il vaut mieux un logo placé à gauche.