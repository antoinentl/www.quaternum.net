---
layout: post
title: "Vie privée"
date: "2016-03-17T23:30:00"
comments: true
published: true
categories: 
- flux
---
>Défendre sa vie privée passe par deux réactions principales :  
Primo, partagez avec vos proches ces arguments pour la défense de l’intimité et son importance dans notre monde hyper connecté. Faites-en un sujet de nombreuses discussions et défendez-la comme un droit fondamental, à l’aide des armes dont vous disposez désormais.  
Secundo, protégez-vous, formez-vous peu à peu à la compréhension des enjeux du numérique, puisqu’il est désormais omniprésent dans nos vies.  
[Benjamin Sonntag, De l’intimité et de sa nécessité](https://www.laquadrature.net/fr/de-l-intimite-et-de-sa-necessite)

Un beau texte qui défend la vie privée, il est temps de considérer l'intimité *numérique* et ses implications, pour nous permettre de vivre ensemble dans les meilleures conditions.