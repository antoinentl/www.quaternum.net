---
layout: post
title: "Des podcasts sur le numérique"
date: "2016-12-22T12:00:00"
comments: true
published: true
description: "Des podcasts sur le numérique : Studio 404, C'est Cool C'est Quoi ?, Trajectoires et Le Log. À télécharger, à écouter sur iTunes ou Soundcloud, et à débattre sur les forums dédiés. Une belle initiative !"
categories: 
- flux
tags:
- média
---
>Petite maison de production de podcasts.  
[Qualiter](http://dequaliter.com/)

Des podcasts sur le numérique : Studio 404, C'est Cool C'est Quoi ?, Trajectoires et Le Log. À télécharger, à écouter sur iTunes ou Soundcloud, et à débattre sur les forums dédiés. Une belle initiative !

Via [Anthony](https://twitter.com/AnthonyMasure/status/811544000180191232).