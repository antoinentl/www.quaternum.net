---
layout: post
title: "Typographie sur le web et performance"
date: "2016-11-04T17:00:00"
comments: true
published: true
description: "Les variable fonts arrivent, et elles devraient permettre de résoudre beaucoup de problèmes de design et de performance."
categories: 
- flux
tags:
- typographie
---
>Web fonts are okay. They make your blog prettier. They’re also slow and kind of an annoying experience, but if you need to use them, use them. Just remember that it’s also your responsibility to make your site load super fast, and if you don’t, it’s totes fair game for people (me) to whine about it on Twitter.  
[Monica Dinculescu, Web fonts, boy, I don't know](http://meowni.ca/posts/web-fonts/)

Un court article sur les *web fonts* et la performance.