---
layout: post
title: "Des bonnes pratiques pour la publicité ?"
date: "2016-04-04T18:00:00"
comments: true
published: true
categories: 
- flux
---
>Sanctuarisez vos contenus, respectez-vous et ça va payer.  
[Élie Sloïm, Chers producteurs de contenus, voici les bonnes pratiques qui pourraient peut-être conduire vos utilisateurs à désactiver adblock](http://blog.temesis.com/post/2016/03/21/Chers-producteurs-de-contenus-les-bonnes-pratiques)

Quelques bonnes pratiques qui pourraient -- peut-être -- permettre aux sites de contenus d'être plus respectueux envers leurs lecteurs.  
Il faut également lire les commentaires, et notamment ce projet intéressant et intriguant : [Obop](http://www.obop.co/).