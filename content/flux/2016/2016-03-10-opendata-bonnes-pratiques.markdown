---
layout: post
title: "OpenData (bonnes pratiques)"
date: "2016-03-10T22:00:00"
comments: true
published: true
categories: 
- flux
---
>72 règles destinées aux producteurs de données ouvertes.  
[Opquast Check-Lists, OpenData](http://checklists.opquast.com/fr/opendata/)

Les référentiels et *check-lists* d'Opquast concernent aussi l'*open data*, et pas uniquement la qualité web.  
API, animation, format, licence, nommage, vie privée, autant de catégories pour les 72 règles à travers ce projet animé par [Temesis](http://temesis.com/).