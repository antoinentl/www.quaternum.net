---
layout: post
title: "Le Millimetre"
date: "2016-03-12T23:00:00"
comments: true
published: true
categories: 
- flux
---
>Millimetre is a series of fonts constructed on a grid based on the metric system. It follows the decimal logic of the latter.  
[VTF, Millimetre](http://www.velvetyne.fr/fonts/millimetre/)

Nouvelle font créée par [Jérémy Landes-Nones](http://www.jjllnn.fr/) et publiée par [Velvetyne Type Foundry](http://www.velvetyne.fr/about/), sous licence SIL Open Font License, Version 1.1.