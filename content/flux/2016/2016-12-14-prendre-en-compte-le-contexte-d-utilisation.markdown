---
layout: post
title: "Prendre en compte le contexte d'utilisation"
date: "2016-12-14T12:00:00"
comments: true
published: true
description: "Comment les utilisateurs accèdent réellement à un site web ? Quel(s) navigateur(s) utilisent-ils ? Quelle est la qualité de leur connexion internet ? Est-ce que certains scripts peuvent dans tous les contextes ? Etc. Autant de paramètres qu'il faut prendre en compte au moment de créer une application web, et qui nécessitent des choix."
categories: 
- flux
tags:
- design
slug: prendre-en-compte-le-contexte-d-utilisation
---
>Every developer will understand and admit that we’re biased which browsers we want to support. Therefore a decision made with that bias will likely not reflect reality of people who will try to access the website/app.  
[Anselm Hannemann, The World uses the Internet (differently)](https://helloanselm.com/2016/the-world-uses-the-internet/)

Comment les utilisateurs accèdent *réellement* à un site web ? Quel(s) navigateur(s) utilisent-ils ? Quelle est la qualité de leur connexion internet ? Est-ce que les scripts peuvent être chargés dans tous les contextes ? Etc. Autant de paramètres qu'il faut prendre en compte au moment de créer une application web, et qui nécessitent des choix.