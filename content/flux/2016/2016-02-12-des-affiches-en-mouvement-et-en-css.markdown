---
layout: post
title: "Des affiches en mouvement et en CSS"
date: "2016-02-12T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Swiss in CSS is a homage to the International Typographic Style and the designers that pioneered the ideas behind the influential design movement.  
[swiss in css](http://swissincss.com/)

Magnifique travail de design graphique, suisse et en CSS -- et un peu de JavaScript pour les affiches en mouvement.