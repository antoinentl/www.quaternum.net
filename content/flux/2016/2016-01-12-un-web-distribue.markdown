---
layout: post
title: "Un web distribué"
date: "2016-01-12T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Early this year, the Internet Archive put out a call for a distributed web. We heard them loud and clear.  
[kyledrake, HTTP is obsolete. It's time for the distributed, permanent web](https://ipfs.io/ipfs/QmNhFJjGcMPqpuYfxL62VVB9528NXqDNMFXiqN5bgFYiZ1/its-time-for-the-permanent-web.html)

[Stéphane](https://twitter.com/SBuellet) m'avait signalé ce protocole il y a déjà plusieurs mois, IPFS -- InterPlanetary File System -- est un moyen de contourner les contraintes du protocole HTTP et les phénomènes de centralisation :

>IPFS is a distributed file system that seeks to connect all computing devices with the same system of files. In some ways, this is similar to the original aims of the Web, but IPFS is actually more similar to a single bittorrent swarm exchanging git objects. IPFS could become a new major subsystem of the internet. If built right, it could complement or replace HTTP. It could complement or replace even more. It sounds crazy. It is crazy.

L'article résume très clairement les objectifs de [IPFS](https://ipfs.io/), *The Permanent Web*.