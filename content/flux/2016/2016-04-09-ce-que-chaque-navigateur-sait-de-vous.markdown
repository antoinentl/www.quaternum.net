---
layout: post
title: "Ce que chaque navigateur sait de vous"
date: "2016-04-09T17:00:00"
comments: true
published: true
categories: 
- flux
---
>This is a demonstration of all the data your browser knows about you. All this data can be accessed by any website without asking you for any permission.
Most of the data points are educated guesses and not considered to be accurate.  
[What every Browser knows about you](http://webkay.robinlinus.com/)

Robin Linus, avec cet outil, nous démontre -- dans le cas où l'on ne le saurait pas encore ou si nous l'avions oublié -- que nous donnons beaucoup d'informations simplement en *visitant* un site web : type de machine, système d'exploitation, navigateur (et version précise), extensions du navigateur, adresses IP, débit de la connexion, connexion à des services en ligne (et oui), etc.