---
layout: post
title: "Imaginer des fonctionnalités pour la lecture numérique active"
date: "2016-05-08T17:00:00"
comments: true
published: true
description: "Chiara Lino fait le constat – que je partage largement – que le livre numérique ne remplit pas toutes ses promesses, et principalement pour une lecture dite active, lorsqu’on étudie, par exemple. Chiara explore huit fonctionnalités – qui rejoignent certains éléments que nous avions imaginés pour Annothèque."
categories: 
- flux
---
>Forget about our romantic, endangered memories of “old book smell”. Most of my university books and manuals smelled of plastic, but that’s beyond the point. There are many ways in which the physicality of the support helps us handle knowledge: understanding and memorizing concepts, making connections, keeping references, aiding visual memory. There are as many, if not more ways in which a digital tool could do just the same, with the added benefit of being always with you(something a few 500-pages manuals cannot boost to be). And yet, it doesn’t.  
[...]  
Yet, if we talk about active reading, many features are missing across all these apps. They concern the way we study, and the many tricks we use to understand, assimilate, and memorize written text. I tried to analyze how we approach the texts we use to study, how we use them, then listed some of the features I think are most needed.  
[Chiara Lino, Unexploited design opportunities: Ebooks still suck for studying](https://blog.prototypr.io/unexploited-design-opportunities-ebooks-still-suck-for-studying-1facb006a014#.n0cfsoohz)

[Chiara Lino](https://medium.com/@chiara_bab) fait le constat -- que je partage largement -- que le livre numérique ne remplit pas toutes ses promesses, et principalement pour une lecture dite *active*, lorsqu'on étudie, par exemple. Chiara explore huit fonctionnalités -- qui rejoignent certains éléments que nous avions imaginés pour [Annothèque](http://annotheque.fr/) :

1. Merge two highlighted sentences into a single note
2. Add a sketch, an image, a photo, a map, a video…
3. Create connections to other ebooks, websites, etc…
4. Use the margins
5. Share your notes
6. Link concepts with definitions
7. Generate your book within a book
8. And all of this, while keeping it simple

Via [Emeline Mercier](http://emelinemercier.com/).