---
layout: post
title: "Féodalisme 2.0"
date: "2016-05-02T18:00:00"
comments: true
published: true
description: "Données personnelles, interfaces, outils, Silicon Valley, GAFA, web, usages numériques, économie mondial, bourse, services publics, micro-paiement, etc. Ce qu'expose -- et prédit en partie -- Evgeny Morozov est assez terrifiant."
categories: 
- flux
slug: feodalisme-deux-point-zero
---
>Depuis que les données — ce carburant des marchés publicitaires — leur assurent des profits faramineux, les cadors de la Silicon Valley ne demandent pas mieux que d’offrir, à des prix imbattables, des services et des biens conçus pour leur délivrer plus de données encore. Une offre infiniment variée, qui inclut bien sûr systèmes de navigation et réseaux sociaux, mais aussi mouchards capables de nous traquer pendant que nous faisons du sport, mangeons ou conduisons, ou même pendant que nous faisons l’amour : pour eux, tout cela n’est rien d’autre que des données, or les données, c’est de l’argent.    
[Evgeny Morozov, Féodalisme 2.0](http://blog.mondediplo.net/2016-04-27-Feodalisme-2-0)

Données personnelles, interfaces, outils, Silicon Valley, GAFA, web, usages numériques, économie mondial, bourse, services publics, micro-paiement, etc. Ce qu'expose -- et prédit en partie -- Evgeny Morozov est assez terrifiant.

>[...] il paraît assez vraisemblable que Google, Facebook et les autres finiront par opérer les infrastructures de base grâce auxquelles le monde d’aujourd’hui fonctionne. Nul doute qu’elles y aspirent, sachant la pluie de données qu’un tel contrôle leur permettrait de recueillir.

>Songez-y. Compte tenu de l’instabilité économique générale, est-il raisonnable de tenir pour acquis que la publicité restera cette vache à lait magique qui apporte l’Internet gratuit jusqu’au Sri Lanka et permet à des millions de personnes d’échanger messages et fichiers sans débourser un sou ?

