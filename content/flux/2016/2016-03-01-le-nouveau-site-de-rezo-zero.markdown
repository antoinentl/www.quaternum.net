---
layout: post
title: "Le nouveau site de Rezo Zero"
date: "2016-03-01T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Notre vision de designer nous pousse à soigner tant le résultat que le processus de création.  
[Rezo Zero](https://www.rezo-zero.com/)

Le studio/agence Rezo Zero a mis en ligne son nouveau site web, graphique et dynamique : beaucoup d'effets, de belles références bien présentées, un positionnement plus axé design et contenu que web, et le tout avec leur CMS [Roadiz](https://www.roadiz.io/).  
Beaucoup de choses à dire sur ce site, qui mériterait un article assez long.