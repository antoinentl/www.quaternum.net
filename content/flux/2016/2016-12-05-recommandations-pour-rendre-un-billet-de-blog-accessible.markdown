---
layout: post
title: "Recommandations pour rendre un billet de blog accessible"
date: "2016-12-05T08:00:00"
comments: true
published: true
description: "Quelques recommandations pour rendre un billet de blog accessible, proposées par Amy McNichol du GDS Digital Engagement. Classiques mais claires et bien formulées."
categories: 
- flux
tags:
- accessibilité
---
>Blog posts should be accessible to everyone too though and we haven’t spoken enough about how we should all be doing this. I’ve pulled out the things that blog owners and admins will need to consider in almost every post they publish.  
They definitely don’t make up an exhaustive list, but they’re a start.    
[Amy McNichol, How to make blog posts accessible](https://gdsengagement.blog.gov.uk/2016/11/28/how-to-make-blog-posts-accessible/)

Quelques recommandations pour rendre un billet de blog accessible, proposées par Amy McNichol du GDS Digital Engagement. Classiques mais claires et bien formulées :

- Alt text describes an image
- Get the image size right
- Link to meaningful words
- Use the correct HTML for headings
- Avoid italics
- Making videos accessible
- Using infographics
- etc.