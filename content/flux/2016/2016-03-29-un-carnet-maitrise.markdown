---
layout: post
title: "Un carnet maîtrisé"
date: "2016-03-29T14:00:00"
comments: true
published: true
categories: 
- flux
---
>I need the source code to my blogging system. That's part of my blogging experience, being able to evolve the software.  
[Dave Winer, Blogging like it's 1999](http://scripting.com/2015/10/07/iNeedABetterBloggingSystem.html)

Dave Winer explique en quelques lignes, dans cet article d'octobre 2015, comment son blog de 1999 était bien plus efficient, résilient, modulable et évolutif qu'aujourd'hui, comparé notamment à des outils de diffusion comme Facebook ou Twitter.  
Entre la volonté de maîtrise, la puissance de la diffusion et les potentielles évolutions.

>So in a way, my "better blogging system" would just be what I already had working 15 years ago.

Via [Karl](http://la-grange.net/) (si je me souviens bien).