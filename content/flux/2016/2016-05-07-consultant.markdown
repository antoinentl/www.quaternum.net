---
layout: post
title: "Consultant"
date: "2016-05-07T15:00:00"
comments: true
published: true
description: "Boris Schapira décrit très bien son travail : il est consulté par un commanditaire, il consulte ses équipes, il rassemble et organise aussi clairement que possible des éléments et des réponses. Le bilan semble presque trop parfait, mais peut-être que cela vient de la position de Boris – consultant-expert."
categories: 
- flux
---
>Mes amis pensent que j’aime mon métier pour son côté technique. J’adore l’informatique et je passe mes soirées à coder, je ne m’en cache pas. Mais ce n’est pas ce qui me permet de gagner ma vie. Comme dans toutes les activités de service, ce qui me lie à mes clients, c’est la **confiance**.  
[Boris Schapira, Confiance](https://borisschapira.com/2016/04/confiance/)

[Boris Schapira](https://borisschapira.com/2016/04/confiance/) décrit très bien son travail : il est consulté par un commanditaire, il consulte ses équipes, il rassemble et organise aussi clairement que possible des éléments et des réponses. Le bilan semble presque trop parfait, mais peut-être que cela vient de la position de Boris -- consultant-expert.