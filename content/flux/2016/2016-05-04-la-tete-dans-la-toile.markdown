---
layout: post
title: "La tête dans la toile"
date: "2016-05-04T10:00:00"
comments: true
published: true
description: "Olivier Ertzscheid parle du livre de Xavier de la Porte, La tête dans la toile, publié chez C&F Éditions, et il en parle bien.
Le livre, recueil d’un an de chronique sur France Culture, est disponible ici ou chez votre libraire le plus proche."
categories: 
- flux
---
>La capacité que chacune de ses chroniques, chacun de ses portraits, a de dresser, de pointer, de soulever, d'analyser, de relativiser, de subjectiver aussi, est l'essentiel de la qualité de ce livre. Xavier part du "je", il part de lui, pour nous emmener sur les internets, ces internets chafouins, libertaires, ces internets sécuritaires, morcelés, corsetés, fragmentés, et de nouveau ces internets émancipateurs, libérateurs, accessibles, toujours accessibles.  
[Olivier Ertzscheid, La tête dans la toile.](http://affordance.typepad.com//mon_weblog/2016/04/la-tete-dans-la-toile.html)

Olivier Ertzscheid parle du livre de Xavier de la Porte, *La tête dans la toile*, publié chez C&F Éditions, et il en parle bien.  
Le livre, recueil d'un an de chronique sur France Culture, [est disponible ici](http://cfeditions.com/toile/) ou chez votre libraire le plus proche.