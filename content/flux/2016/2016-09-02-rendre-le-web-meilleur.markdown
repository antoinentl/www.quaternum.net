---
layout: post
title: "Rendre le web meilleur"
date: "2016-09-02T17:00:00"
comments: true
published: true
description: "À propos du web et des standards, et de comment avancer en travaillant de concert."
categories: 
- flux
---
>What we need then is a way to *incubate* ideas, build prolyfills and somehow get lots of eyeballs, use and participation.  We need to see what sticks, and what can be better.  We need ideas to fail safely without breaking the economics of participation or breaking the Web.  
[Brian Kardell, The Future Web Wants You.](https://briankardell.wordpress.com/2016/08/24/the-future-web-wants-you/)

À propos du web et des standards, et de comment avancer en travaillant *de concert*.