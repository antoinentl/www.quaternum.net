---
layout: post
title: "Voyage dans les internets"
date: "2016-08-30T17:00:00"
comments: true
published: true
categories: 
- flux
---
>Raconter les internets en musiques, c'est traverser cinq décennies et révéler un nouveau monde, rêvé puis numérisé, connecté, démultiplié, surveillé et peut-être demain réinventé. Un voyage sonore dans une réalité tout sauf virtuelle qui façonne aujourd'hui nos sociétés.  
[France Culture, Voyage dans les internets](http://www.franceculture.fr/emissions/culture-musique-ete/voyage-dans-les-internets)

Cinq épisodes qui retracent l'histoire d'Internet, en musique, à écouter absolument !