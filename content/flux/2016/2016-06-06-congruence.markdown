---
layout: post
title: "Congruence"
date: "2016-06-06T22:00:00"
comments: true
published: true
description: "Nicolas parle de Sud Web 2016, et il me fait découvrir le terme congruence, un retour intéressant."
categories: 
- flux
---
>Le maitre mot que je retiens de cette édition est la congruence (être en accord entre ses actions, ses sentiments, son être, etc.). Ce n’est pas toujours facile, mais c’est la seule voie pour durer.  
[Nicolas, Retour sur Sud Web 2016 à Bordeaux](http://www.nicolas-hoffmann.net/source/1694-Retour-sur-Sud-Web-2016-a-Bordeaux.html)

Nicolas parle de [Sud Web 2016](https://sudweb.fr/2016/), et il me fait découvrir le terme *congruence*, un retour intéressant.