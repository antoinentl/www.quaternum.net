---
layout: post
title: "Expérience de lecture (numérique)"
date: "2016-04-19T12:00:00"
comments: true
published: true
categories: 
- flux
---
>C’est la première fois qu’un livre me force à penser « tiens, une liseuse, ce serait peut-être mieux » plutôt que de défendre dur comme fer le papier.  
[Stéphane, Livre pas-électronique, le retour](http://nota-bene.org/Livre-pas-electronique-le-retour)

Stéphane en avait [déjà parlé sur son carnet](http://nota-bene.org/Livre-numerique-pas-encore-probant-pour-moi), le livre numérique est une expérience qui ne le satisfait pas. Et là, devant la nécessité d'une lecture -- amplifiée par le fait qu'il dispose du texte, mais pas du bon design --, le livre numérique apparaît comme une option tangible.  
Intéressant en terme d'expérience utilisateur.