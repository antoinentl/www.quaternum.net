---
layout: post
title: "Lisibilité (typographique)"
date: "2016-04-11T10:00:00"
comments: true
published: true
categories: 
- flux
---
>Les porteurs de messages que nous sommes doivent respecter le lecteur et lui permettre ainsi de mémoriser facilement… Surtout que nous allons de plus en plus vers une information ou une distraction immatérielle.  
[Roger Bodin, Dis, c’est quoi la lisibilité ?](http://typo-graphe.com/2110-2/)