---
layout: post
title: "Comment le web est devenu illisible"
date: "2016-10-25T17:00:00"
comments: true
published: true
description: "Kevin Marks fait un constat sombre (sic) : le web est devenu illisible du fait de choix esthétiques qui ne permettent pas une bonne lisibilité des contenus, selon la situation des utilisateurs, le matériel et le contexte."
categories: 
- flux
tags:
- design
---
>My plea to designers and software engineers: Ignore the fads and go back to the typographic principles of print — keep your type black, and vary weight and font instead of grayness. You’ll be making things better for people who read on smaller, dimmer screens, even if their eyes aren’t aging like mine. It may not be trendy, but it’s time to consider who is being left out by the web’s aesthetic.  
[Kevin Marks, How the Web Became Unreadable](https://backchannel.com/how-the-web-became-unreadable-a781ddc711b6#.dbipxwr8l)

Kevin Marks fait un constat sombre (sic) : le web est devenu illisible du fait de choix esthétiques qui ne permettent pas une bonne *lisibilité* des contenus, selon la situation des utilisateurs, le matériel et le contexte.