---
layout: post
title: "Vous avez dit hors ligne ?"
date: "2016-10-29T17:00:00"
comments: true
published: true
description: "Un article assez court sur ce qu'est réellement le hors ligne, avec quelques éléments que j'avais soulignés par ici : https://www.quaternum.net/2016/06/23/quatre-bonnes-raisons-de-faire-du-web-hors-connexion/."
categories: 
- flux
tags:
- offline
---
>4G Will Be Everywhere Soon — Wrong [...]  
Developer Experience Is More Important Than UX — Wrong [...]  
Technical Architects Don’t Know What They Need — Wrong [...]  
Offline Is For Rare Scenarios — Wrong [...]  
Local Data Is Only For Offline — Wrong [...]  
Offline Can’t Be Done With Current Standards — Wrong [...]  
[Tiago Simões, I Was Wrong About Offline](https://medium.com/outsystems-engineering/i-was-wrong-about-offline-fe5426894740#.xg7i4zsah)

Un article assez court sur ce qu'est réellement le hors ligne, avec quelques éléments que j'avais soulignés [par ici](/2016/06/23/quatre-bonnes-raisons-de-faire-du-web-hors-connexion/).

Via [Jiminy Panoz](https://twitter.com/JiminyPan/status/791913635039752192).