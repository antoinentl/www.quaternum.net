---
layout: post
title: "Ethical Web Development"
date: "2016-02-17T23:00:00"
comments: true
published: true
categories: 
- flux
---
>As web developers, we are responsible for shaping the experiences of user's online lives. By making choices that are ethical and user-centered, we create a better web for everyone.  
[Ethical Web Development](https://ethicalweb.org/)

Un manifeste en [quatre points](https://ethicalweb.org/), des [articles](https://ethicalweb.org/posts/) et un livre en cours de rédaction [ouvert aux participations](https://github.com/ascott1/ethical-web-dev).  
Un beau projet d'[Adam Scott](http://adamscott.website/).

>Progressive enhancement is a term that often insights intense debate. For many, progressive enhancement can be summed up as “make your site work without JavaScript.” While, developing a site that works without JavaScript often does fall under the web of progressive enhancement, it can define a much more nuanced experience.