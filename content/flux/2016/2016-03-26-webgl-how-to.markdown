---
layout: post
title: "WebGL, how to"
date: "2016-03-26T07:00:00"
comments: true
published: true
categories: 
- flux
---
>"C’est bien sympa de faire de la 3D sur le web mais quand je vois la tronche de javascript ça donne pas envie de se lancer dans un truc aussi risqué."  
Pas de problème camarade développeur-web-râleur. Je vais te montrer le chemin pour faire du joli code bien solide. Tu vas voir, la 3D sera au final bien plus simple à gérer que si tu devais t’atteler à le faire en natif par exemple.  
[Alexis Bouhet, WebGL : Workflow](http://www.lahautesociete.com/blog/webgl-3d-javascript-typescript-grunt-workflow/)

Description d'un workflow -- complet ! -- pour utiliser la technologie WebGL, ici avec le cas du site web de l'agence grenobloise [La Haute Société](http://www.lahautesociete.com/).