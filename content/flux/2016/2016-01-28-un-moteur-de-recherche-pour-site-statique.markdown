---
layout: post
title: "Un moteur de recherche pour site statique"
date: "2016-01-28T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Un des soucis avec un site statique tel que celui-ci, c’est que par définition il rend compliquée la mise en œuvre de fonctionnalités habituellement dynamiques. Les commentaires viennent immédiatement à l’esprit, et seront évoqués ultérieurement, mais la mise en place d’un moteur de recherche interne n’est pas plus simple. Heureusement, Algolia est là.  
[Nicolas Hoizey, Un moteur de recherche sur un site statique ? Facile avec Algolia !](http://nicolas-hoizey.com/2015/06/un-moteur-de-recherche-sur-un-site-statique-facile-avec-algolia.html)

Nicolas propose des solutions pour intégrer un moteur de recherche *dans* un site statique, Algolia semble être une solution intéressante et facile à implémenter ! 