---
layout: post
title: "Portable Web Document"
date: "2016-05-15T14:00:00"
comments: true
published: true
description: "Peter Brantley aborde la question du rapprochement de l'IDPF et du W3C, et les enjeux autour de la convergence du livre numérique et du web."
categories: 
- flux
---
>Publishing books on the web isn’t a risk: it’s the future.  
[Peter Brantley, Books, in a browser](https://medium.com/@naypinya/books-in-a-browser-375df76207ce#.ku3j5tklg)

Peter Brantley aborde la question du rapprochement de l'IDPF et du W3C, et les enjeux autour de la convergence du livre numérique et du web :

>Berners-Lee noted four crucial affordances for publications on the web. *Permanence*: the open web is built on our most reproducible standards; materials published using HTML5 and its successors stand an excellent chance of remaining available in future networked environments. *Seamless*: ebook and digital magazine content can flow across different kinds of devices, from laptops to mobiles, with no fundamental disruption. *Linked*: publications can incorporate content that resides across the globe, in whatever form, as long as it is addressable on the web. *Trackable*: publications embodying standard identifiers such as DOIs or ISBNS, and persistent author identification, can be identified, and their use analyzed around the web.