---
layout: post
title: "Le livre est un terminal de lecture comme un autre"
date: "2016-08-23T22:00:00"
comments: true
published: true
categories: 
- flux
---
>On parle de dématérialisation, mais en réalité le livre électronique est toujours rematérialisé. Il est dématérialisé pendant un moment parce qu'il est sous forme de flux, et puis il arrive sur un terminal. Le terminal peut être le livre.  
Imaginez que je sois un chercheur, et que je consulte sur une base de données quinze articles sur la couleur rouge, qui est mon objet d'étude. À titre personnel, si on me propose un service d'impression d'un volume sur la couleur rouge que je compose pour moi -- avec ces quinze articles --, et qu'on me l'envoie par la Poste, je trouve cela confortable. C'est une des revanches possible du livre au format papier. C'est un terminal de lecture comme un autre.  
En revanche si je ne peux pas avoir un petit moteur de recherche qui me permette d'indexer les 15 articles en question, je suis frustré par ce que j'ai aussi besoin de pouvoir faire de l'exploration de données. Et donc il est évident que les deux vont cohabiter et que de nouvelles formes d'imprimés vont se développer.  
[Marin Dacos dans l'émission Place de la toile spéciale édition numérique en 2009](https://archive.org/details/PlaceDeLaToilespcialeditionNumrique)

En 2009, Xavier de La Porte consacrait une émission spéciale de Place de la toile à l'édition numérique, avec [Marin Dacos](http://marin.dacos.org/), [Virginie Clayssen](http://www.archicampus.net/wordpress/) et [Hubert Guillaud](https://twitter.com/hubertguillaud). 7 ans plus tard -- oui ça passe vite --, les choses ont changé, et en même temps pas tant que cela ! À réécouter cette émission d'une heure, on se prend à rêver à certains scénarios présentés par ces trois veilleurs et experts du livre numérique, il y a encore du chemin à faire (principalement sur l'expérience de lecture, la médiation et l'offre).

(Merci à celui ou celle qui a déposé le podcast sur archive.org !)