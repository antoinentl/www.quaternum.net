---
layout: post
title: "Référentiel de sécurité pour les applications web"
date: "2016-10-06T17:00:00"
comments: true
published: true
description: "Un outil pratique pour avoir une vision d'ensemble des questions de sécurité de sites ou applications web. À appliquer ou à connaître -- et faire connaître -- selon là où l'on intervient."
categories: 
- flux
---
>The goal of this document is to help operational teams with creating secure web applications.  
[Mozilla, Security/Guidelines/Web Security](https://wiki.mozilla.org/Security/Guidelines/Web_Security)

Un outil pratique pour avoir une vision d'ensemble des questions de sécurité de sites ou applications web. À appliquer ou à connaître -- et faire connaître -- selon là où l'on intervient.