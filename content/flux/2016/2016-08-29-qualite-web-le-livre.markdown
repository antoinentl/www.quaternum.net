---
layout: post
title: "Qualité Web, le livre"
date: "2016-08-29T17:00:00"
comments: true
published: true
categories: 
- flux
---
>Après deux années de travail, j'ai le plaisir de vous annoncer la parution de la deuxième édition du livre qualité Web aux éditions Eyrolles.  
[Élie Sloïm, Qualité Web, deuxième édition](http://blog.temesis.com/post/2016/08/25/Qualite-Web-deuxieme-edition)

Un ouvrage qui aborde les questions de bonnes pratiques et de qualité web, mais aussi --notamment -- d'accessibilité et de performance !