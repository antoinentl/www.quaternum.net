---
layout: post
title: "Internet et congestion"
date: "2016-01-15T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Internet est-il trop centralisé ? Sommes-nous au bord de la congestion du réseau des réseaux ?… Ces alarmes reviennent régulièrement.  
[Hubert Guillaud, Avons-nous besoin d’une vitesse limitée sur l’internet ?](http://www.internetactu.net/2015/12/01/avons-nous-besoin-dune-vitesse-limitee-sur-linternet/)

Un article d'Hubert Guillaud, bien documenté, sur la supposée *congestion* des réseaux. À noter que la façon dont sont conçus les sites web est un point important à ne pas négliger.