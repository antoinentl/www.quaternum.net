---
layout: post
title: "Design web et résilience"
date: "2016-12-13T23:00:00"
comments: true
published: true
description: "Jeremy Keith vient de publier un nouveau livre, un livre web ou web book : Resilient Web Design."
categories: 
- flux
tags:
- livre
---
>If you make websites in any capacity, I hope that this book will resonate with you. Even if you don’t make websites, I still hope there’s an interesting story in there for you.  
[Jeremy Keith, Introducing Resilient Web Design](https://adactio.com/journal/11608)

[Jeremy Keith](https://adactio.com/) vient de publier un nouveau livre, un [livre web ou web book](https://github.com/antoinentl/web-books-initiatives) : [Resilient Web Design](https://resilientwebdesign.com/).