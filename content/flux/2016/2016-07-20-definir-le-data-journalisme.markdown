---
layout: post
title: "Définir le data-journalisme"
date: "2016-07-20T17:00:00"
comments: true
published: true
description: "Long texte de Nicolas Kayser-Bril sur les données, à lire."
categories: 
- flux
---
>La nouveauté du datajournalisme repose sur le fait qu’une organisation ou un individu peut, avec de très faibles ressources, aller d’un bout à l’autre de la chaîne de création de valeur. C’est la raison pour laquelle le datajournalisme peut se définir comme l’action de mesurer ce qui ne l’est pas encore et qui devrait l’être, tout comme le journalisme traditionnel peut se définir comme l’action de donner la parole aux sans-voix. Le fait de pouvoir, pour toute personne, mesurer un phénomène de manière rigoureuse, est très nouveau. Il permet de concurrencer les mesureurs officiels qui, jusqu’à présent, avaient le monopole de la mesure de la réalité.  
[Nicolas Kayser-Bril, Informer avec des données structurées](http://blog.nkb.fr/informer-avec-des-donnees-structurees)

Long texte de Nicolas Kayser-Bril sur les données, à lire.