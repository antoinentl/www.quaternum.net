---
layout: post
title: "L'accessibilité web vue du côté des chercheurs"
date: "2016-01-13T22:00:00"
comments: true
published: true
categories: 
- flux
slug: l-accessibilite-web-vue-du-cote-des-chercheurs
---
>Malgré la présence de nombreuses normes, le WCAG ou le RGAA en France, les développeurs web en charge des sites et de la mise à disposition de ressources numériques intègrent encore peu ces normes et ces logiques informationnelles.  
[Vincent Liquète, L’accessibilité web comme porte et enjeu de médiation des savoirs](http://dms.revues.org/1200)

Intéressante approche de l'accessibilité web, vue par un universitaire.