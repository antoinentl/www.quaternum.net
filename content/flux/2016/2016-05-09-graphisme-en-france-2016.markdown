---
layout: post
title: "Graphisme en France 2016"
date: "2016-05-09T17:00:00"
comments: true
published: true
description: "Le numéro 22 de la revue Graphisme en France est paru, publié par le Centre national des arts plastiques, et mis en page cette année par Alice Jauneau et David Vallance."
categories: 
- flux
---
>Parcourir en fouillant. S’efforcer de connaître, de fréquenter, de rencontrer. Chercher à retrouver. Chercher à connaître. Faire une enquête. Tâcher d’acquérir, d’obtenir. Terminer une œuvre avec un soin extrême, jusque dans les plus petits détails. Essayer d’atteindre. Tenter de découvrir, de discerner une qualité. Être en quête. Désirer connaître. Être à la recherche. Examiner avec soin. Chercher avec soin, méthode, réflexion. Être attiré. Solliciter. Étudier ou bien examiner.  
Chercher  
[Graphisme en France 2016, Recherche, design graphique et typographie, un état des lieux](http://www.cnap.graphismeenfrance.fr/livre/graphisme-france-ndeg22)

Le numéro 22 de la revue Graphisme en France est paru, publié par le [Centre national des arts plastiques](http://www.cnap.fr/), et mis en page cette année par [Alice Jauneau](http://www.alicejauneau.fr/) et [David Vallance](http://www.davidvallance.com/).

Le sommaire :

- Pratiques de recherche en design graphique : état des lieux d’une construction. Éloïsa Pérez
- La recherche actuelle en design graphique. Alice Twemlow
- Panorama de la recherche
- Note introductive sur une recherche autour du travail de Pierre Faucheux. Catherine Guiral
- Chantier en cours, la recherche dans l’option "design graphique" de l’École supérieure d’art et design Grenoble-Valence. Annick Lantenois, Gilles Rouffineau
- Typographie, l’apprentissage d’une recherche. Sébastien Morlighem
- Un programme Grapus, chronique d’une recherche. Catherine de Smet

La revue est disponible gratuitement et plus ou moins facilement dans les lieux culturels. Le PDF est disponible [en ligne](http://www.cnap.fr/sites/default/files/article/146957_graphisme_en_france_2016_pdf_fr.pdf), en attendant, bientôt, une version numérique.