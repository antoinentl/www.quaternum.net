---
layout: post
title: "Savoir gérer l'échec"
date: "2016-01-25T22:00:00"
comments: true
published: true
categories: 
- flux
slug: savoir-gerer-l-echec
---
>Le point essentiel est donc de savoir gérer l’échec dans un projet. Ne pas le préparer revient à le garantir, car – Loi de Murphy ([http://fr.wikipedia.org/wiki/Loi_de_Murphy](http://fr.wikipedia.org/wiki/Loi_de_Murphy)) aidant – vous pouvez être certain que le pire scénario finira par se produire. Évitez de ne compter que sur votre bonne étoile.  
[Bastien Jaillot, La dette technique, page 63](http://boutique.letrainde13h37.fr/products/la-dette-technique-bastien-jaillot)

Le livre de Bastien Jaillot publié par [Le train de 13h37](http://letrainde13h37.fr/) est concis, clair, utile. Ce passage -- et sa suite -- en est un bon exemple.