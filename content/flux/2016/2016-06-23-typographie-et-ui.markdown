---
layout: post
title: "Typographie et UI"
date: "2016-06-23T17:00:00"
comments: true
published: true
description: "Viljami Salminen propose un article assez complet sur le rôle de la typographie dans les interfaces utilisateur, et notamment les dix points clés à prendre en compte."
categories: 
- flux
---
>Every word and letter in an interface matters. Good writing is good design. Text is ultimately interface, and it’s us, the designers, who are the copywriters shaping this information.  
[Viljami Salminen, Typography for User Interfaces](https://viljamis.com/2016/typography-for-user-interfaces/)

Viljami Salminen propose un article assez complet sur le rôle de la typographie dans les interfaces utilisateur, et notamment les dix points clés à prendre en compte.

Cet article est un résumé d'une conférence, dont le support est disponible par ailleurs [par ici](https://viljamis.com/type-for-ui/).

>To ease our work make our interfaces faster, more accessible, and eventually more legible and productive too, because ultimately, good typography enables productivity for everyone. It could even save your life.