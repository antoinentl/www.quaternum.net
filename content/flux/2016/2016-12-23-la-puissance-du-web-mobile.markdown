---
layout: post
title: "La puissance du web mobile"
date: "2016-12-23T12:00:00"
comments: true
published: true
description: "Stéphanie Walter explore les possibilités du web mobile, avec des exemples concrets et des mises en situation très réalistes !"
categories: 
- flux
tags:
- design
---
>Nous pouvons désormais accéder directement depuis le navigateur à l’appareil photo, prendre des vidéos, capturer de l’audio et utiliser WebRTC pour construire un système de messagerie dans le navigateur sans avoir besoin d’installer le moindre plugin. Il est également possible de construire des Progressive Web Apps qui, grâces à une icône de lancement, des notifications ou encore des fonctionnalités disponibles hors connexion, etc., permettront de reproduire une expérience proche de ce qu’offraient jusque-là les applications natives. Et pourquoi s’arrêter en si bon chemin ?  
[Stéphanie Walter, Les super pouvoirs des navigateurs mobiles](https://blog.stephaniewalter.fr/super-pouvoirs-navigateurs-mobiles/)

Stéphanie Walter explore les possibilités du web *mobile*, avec des exemples concrets et des mises en situation très réalistes !