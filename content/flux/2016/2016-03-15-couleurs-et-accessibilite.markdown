---
layout: post
title: "Couleurs et accessibilité"
date: "2016-03-15T23:00:00"
comments: true
published: true
categories: 
- flux
---
>Empowering designers with beautiful and accessible color palettes based on WCAG Guidelines of text and background contrast ratios.  
[Color Safe](http://colorsafe.co/)

La question du contraste entre la couleur du fond d'une page et la couleur du texte est déterminante ! Un texte en gris clair sur du blanc ? Le *ratio* ne sera pas suffisant pour une lecture par toutes et tous.

Calculer ce ratio n'est pas si simple à calculer (d'ailleurs Sébastien Delorme d'Atalan avait publié [des outils d'analyse des contraste d'une charte](http://blog.atalan.fr/grille-contrastes-accessibilite-charte-graphique/)), ColorSafe facilite la recherche de couleurs avec un outil en ligne très simple d'utilisation.