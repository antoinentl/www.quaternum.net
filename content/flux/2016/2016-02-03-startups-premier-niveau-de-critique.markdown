---
layout: post
title: "Startups : premier niveau de critique"
date: "2016-02-03T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Peut-être un changement de tendance. À vous de juger.  
[15marches, Ras-le-bol des startups](http://15marches.fr/numerique/ras-le-bol-startups)

[15marches](http://15marches.fr/) déroule un certain nombre de critiques argumentées contre le modèle des *startups*. Il manque des propositions et des exemples d'expériences humaines et de projets qui vont à contre courant.

>Peut-être que ce discours de bon sens nous permettra de mettre d’accord tout le monde autour d’un postulat simple : entreprendre est une aventure magnifique, qu’il faut encourager. Il n’a jamais été aussi simple d’innover à l’ère d’internet, et chacun doit prendre son destin entre ses mains. Changer le monde, mais chacun à sa mesure et à son rythme.