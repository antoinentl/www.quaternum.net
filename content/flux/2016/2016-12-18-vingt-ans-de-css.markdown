---
layout: post
title: "Vingt ans de CSS"
date: "2016-12-18T23:00:00"
comments: true
published: true
description: "Le CSS a vingt ans !"
categories: 
- flux
tags:
- web
---
>On December 17, 1996, W3C published the first standard for CSS. And thus from December 17, 2016 until one year later, CSS is 20 years old.  
[W3C, 20 Years of CSS](https://www.w3.org/Style/CSS20/)

Le CSS a vingt ans ! Le W3C a également publié une petite histoire du CSS : [https://www.w3.org/Style/CSS20/history.html](https://www.w3.org/Style/CSS20/history.html)