---
layout: post
title: "Typographie relative et expérience utilisateur"
date: "2016-05-16T17:00:00"
comments: true
published: true
description: "Quelle utilisation des unités relatives en prenant compte des préférences utilisateurs ? C'est ce qu'explore -- très clairement -- Nicolas Hoffmann."
categories: 
- flux
---
>Je suis surpris de voir que les unités relatives ne font pas encore consensus dans la communauté Web, notamment sur la typographie.  
[Nicolas Hoffmann, Les em/rem avec les préférences utilisateurs](http://www.nicolas-hoffmann.net/source/1692-Les-em-rem-avec-les-preferences-utilisateurs.html)

Quelle utilisation des unités relatives en prenant compte des préférences utilisateurs ? C'est ce qu'explore -- très clairement -- Nicolas Hoffmann.