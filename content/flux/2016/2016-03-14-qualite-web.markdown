---
layout: post
title: "Qualité web"
date: "2016-03-14T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Ce que nous aimerions voir plus souvent, ce sont des sites mieux conçus, qui inspirent confiance, qui facilitent la vie et qui ne jouent pas l’esbroufe.  
[Douglas & Douglas, Communiquer par la qualité: les bonnes pratiques Opquast](http://www.douglas-douglas.ch/communiquer-qualite/)

Arguments en faveur de la mise en qualité des sites web, pour des questions de performance, de stratégie, et de confiance. Intéressant.