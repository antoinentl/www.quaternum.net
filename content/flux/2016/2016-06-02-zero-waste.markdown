---
layout: post
title: "Zero waste"
date: "2016-06-02T23:00:00"
comments: true
published: true
description: "Delphine évoque Sud Web 2016 en faisant le rapprochement entre la dernière conférence – Zero waste par Mylène L'Orguilloux – et les métiers du web : est-ce qu'on ne pourrait pas tenter de fabriquer des sites et applications en s'inspirant du concept Zero waste utilisé dans le textile ? L'un des retours de Sud Web 2016 les plus revigorants !"
categories: 
- flux
---
>Pour ne ramener ce parallèle qu’à notre domaine professionnel, Sud Web nous parle là de garder l’esprit ouvert par rapport à nos habitudes (de conception, de développement) et à penser en fonction de notre contexte actuel (qui, dans le web, est toujours différent de ce qu’il était un an plus tôt).  
[Delphine Malassingne, Zéro gâchis mais plein de questions](http://www.ekino.com/zero-gachis-plein-de-questions/)

Delphine évoque [Sud Web 2016](https://sudweb.fr/2016/) en faisant le rapprochement entre la dernière conférence -- "Zero waste" par Mylène L'Orguilloux -- et les métiers du web : est-ce qu'on ne pourrait pas tenter de fabriquer des sites et applications en s'inspirant du concept *Zero waste* utilisé dans le textile ? L'un des retours de Sud Web 2016 les plus revigorants !