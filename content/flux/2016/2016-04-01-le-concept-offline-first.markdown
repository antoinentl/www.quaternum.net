---
layout: post
title: "Le concept Offline First"
date: "2016-04-01T14:00:00"
comments: true
published: true
categories: 
- flux
---
>In a world where online connectivity is always expected but often spotty, it's time we start looking to offline solutions. Check out this quick introduction to the "Offline First" web paradigm.  
[Siva Prasad Rao Janapati, Let's Welcome Offline First](https://dzone.com/articles/let-us-welcome-offline-first)

Une très bonne introduction au concept *Offline First*.