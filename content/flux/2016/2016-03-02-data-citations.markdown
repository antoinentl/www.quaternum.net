---
layout: post
title: "Data citations"
date: "2016-03-02T22:00:00"
comments: true
published: true
categories: 
- flux
---
>We identify a set of properties desirable in a concrete implementation of data citations, considering the full publishing pipeline as well as the needs of landing pages for datasets. Building on top of these properties, we describe the specific implementation approach that we take, built completely on top of Web standards. Web technology is found once again to be awesome and more versatile than people expect.  
[Robin Berjon, Web-First Data Citations](https://research.science.ai/article/web-first-data-citations)