---
layout: post
title: "L'objet livre en documentaire"
date: "2016-10-28T17:00:00"
comments: true
published: true
description: "Au-delà de l'accroche maladroite, ce documentaire présente brièvement des approches intéressantes : Steidl, Irma Boom, et même Jan Tschichold."
categories: 
- flux
tags:
- livre
slug: l-objet-livre-en-documentaire
---
>À l'heure du tout numérique, certains sont encore attachés aux ouvrages en bel et bon papier !  
[arte, La sensualité des livres](http://www.arte.tv/guide/fr/055845-000-A/la-sensualite-des-livres?country=FR)

Au-delà de l'accroche maladroite, ce documentaire présente brièvement des approches intéressantes : Steidl, Irma Boom, et même Jan Tschichold.