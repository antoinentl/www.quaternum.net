---
layout: post
title: "Quelques erreurs de CSS"
date: "2016-09-09T12:00:00"
comments: true
published: true
description: "Une petite liste d'erreurs CSS souvent commises, avec des solutions pour les éviter facilement. Des erreurs techniques qui proviennent bien souvent de problèmes de conception !"
categories: 
- flux
---
>These are some common causes of design, scalability, performance and maintainability issues I find when refactoring CSS for clients  
[Mariano Miguel, Common CSS Mistakes (And How To Fix Them)](https://blog.mariano.io/common-css-mistakes-and-how-to-fix-them-8ee0f5e88d64#.p0xumpu25)

Une petite liste d'erreurs CSS souvent commises, avec des solutions pour les éviter facilement. Des erreurs *techniques* qui proviennent bien souvent de problèmes de conception !