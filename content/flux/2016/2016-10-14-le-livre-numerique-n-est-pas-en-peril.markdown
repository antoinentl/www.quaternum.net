---
layout: post
title: "Le livre numérique n'est pas en péril"
date: "2016-10-14T17:00:00"
comments: true
published: true
description: "Plusieurs articles récents font le constat que, pour le livre numérique, la fête est finie. Entre une interprétation discutable de chiffres et d'études, il y a surtout un problème de positionnement."
categories: 
- flux
tags:
- ebook
slug: le-livre-numerique-n-est-pas-en-peril
---
>La réalité est autre, mais il faut avoir le goût de l’observation pour la reconnaître.  
[Joël Faucilhon, N'espérez pas vous débarrasser du livre numérique](http://www.faucilhon.me/carnet/index.php/post/N-esperez-pas-vous-debarasser-du-livre-numerique)

Plusieurs articles récents font le constat que, pour le livre numérique, la fête est finie. Entre une interprétation discutable de chiffres et d'études, il y a surtout un problème de positionnement :

- si le livre numérique ne représente pas un intérêt économique important pour les éditeurs avec le modèle *business to client*, ce n'est pas forcément le cas avec le *B2B* ;
- le domaine public est souvent absent de ces conclusions sur le livre numérique ;
- l'autoédition est globalement laissée de côté par les éditeurs ;
- et l'on parle, la plupart du temps, d'achat/acquisition de livres numériques au format EPUB.

L'article de Joël Faucilhon permet de se (re)mettre en tête une réalité que certains éditeurs ou médias oublient un peu vite.