---
layout: post
title: "J'ai abandonné"
date: "2016-01-14T22:00:00"
comments: true
published: true
categories: 
- flux
slug: j-ai-abandonne
---
>Nous construisons de petits mondes isolés, pour nous séparer de la société. Nous avons mis un casque sur nos yeux et nous marchons parmi la foule. Nous pourrions tout aussi bien être des zombies, personne ne s’en soucierait. (…) Nous avons la capacité, mais elle n’est pas distribuée. Nous centralisons tout. La nourriture, l’argent, le pouvoir, les décisions.  
[À lire ailleurs, Peter Sunde : “J’ai abandonné” - Motherboard](http://alireailleurs.tumblr.com/post/137077986546/peter-sunde-jai-abandonn%C3%A9-motherboard)

Constat sombre sur l'internet et le web, avec toutefois des pistes de résistance. Il nous appartient de changer les choses.