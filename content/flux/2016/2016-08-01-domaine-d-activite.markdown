---
layout: post
title: "Domaine d'activité"
date: "2016-08-01T17:00:00"
comments: true
published: true
description: "Citation de Gerard Unger, extraite de Pendant la lecture."
categories: 
- flux
slug: domaine-d-activite
---
>Celui qui connaît l'histoire de son domaine d'activité est capable de la prolonger. Celui qui sait d'où viennent les connaissances et les sciences est mieux en mesure de déterminer la direction qu'il souhaite prendre.  
[Gerard Unger, Pendant la lecture, page 109](http://editions-b42.com/books/pendant-la-lecture/)