---
layout: post
title: "Développement et éthique"
date: "2016-11-22T19:00:00"
comments: true
published: true
description: "Développement et éthique."
categories: 
- flux
tags:
- métier
---
>Ainsi, je propose que chaque ligne de code soit examinée à l’aune de la première formulation de l’impératif catégorique : "Agis de telle sorte que la maxime de ta volonté puisse toujours valoir en même temps comme principe d’une législation universelle." Et hop, c’est réglé.  
[Xavier de La Porte, L'histoire du développeur qui aurait dû lire Kant](https://www.franceculture.fr/emissions/la-vie-numerique/lhistoire-du-developpeur-qui-aurait-du-lire-kant)