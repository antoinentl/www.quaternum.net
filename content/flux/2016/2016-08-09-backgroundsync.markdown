---
layout: post
title: "BackgroundSync"
date: "2016-08-09T23:00:00"
comments: true
published: true
description: "Dean Hume présente une fonctionnalité intéressante des Service Workers, permettant de synchroniser des requêtes et pas uniquement des documents ou des fichiers !"
categories: 
- flux
---
>So what exactly is Background Sync? Well, it is a new web API that lets you defer actions until the user has stable connectivity. This makes it great for ensuring that whatever the user wants to send, is actually sent. The user can go offline and even close the browser, safe in the knowledge that their request will be sent when they regain connectivity.  
[Dean Hume, ServiceWorker: A Basic Guide to BackgroundSync](https://ponyfoo.com/articles/backgroundsync)

Dean Hume présente une fonctionnalité intéressante des Service Workers, permettant de synchroniser des requêtes et pas uniquement des documents ou des fichiers !

Via [Jeremy Keith](https://adactio.com/links/).