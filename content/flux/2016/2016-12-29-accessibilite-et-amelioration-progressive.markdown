---
layout: post
title: "Accessibilité et amélioration progressive"
date: "2016-12-29T12:00:00"
comments: true
published: true
description: "Comment adapter le design d'une typographie web pour la rendre plus accessible ? Et comment organiser un projet pour aller vers cette accessibilité ? Un article qui parle de design formel et de design de projet."
categories: 
- flux
tags:
- accessibilité
---
>The trouble with expecting perfection in one go is that it can be tempting to take the safe route, to go with the tried and tested. But giving ourselves room to test and refine also gives us the freedom to take risks and try original approaches.  
[Eleanor Ratliff, Accessibility Whack-A-Mole](http://alistapart.com/article/accessibility-whack-a-mole)

Comment adapter le design d'une typographie web pour la rendre plus *accessible* ? Et comment organiser un projet pour aller vers cette accessibilité ? Un article qui parle de design formel et de design de projet.