---
layout: post
title: "Écrire pour partager"
date: "2016-07-07T17:00:00"
comments: true
published: true
description: "Mark Llobrera écrivait, en décembre 2015, un manifeste pour que les designers et développeurs web écrivent ce qu'ils savent, qu'ils partagent leurs expériences."
categories: 
- flux
---
>Code has a story.  
[Mark Llobrera, Write What You Know (Now)](http://alistapart.com/column/write-what-you-know-now)

[Mark Llobrera](http://dirtystylus.com/) écrivait, en décembre 2015, un manifeste pour que les designers et développeurs web écrivent ce qu'ils savent, qu'ils partagent leurs expériences.

>I feel like a web designer’s life is full of those little stories, every day. And usually you tell your teammates over lunch, or over a beer, and you laugh and say, “Isn’t that nuts?” Well, I’m here to say, “write it up.” Let someone else hear that story, too.