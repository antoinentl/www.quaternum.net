---
layout: post
title: "Un générateur de site statique pour fabriquer des livres"
date: "2016-11-12T18:00:00"
comments: true
published: true
description: "Eric Gardner, designer et développeur pour Getty Publications, a choisi d'utiliser un générateur de site statique pour *fabriquer* des livres, un choix disruptif mais fonctionnel !"
categories: 
- flux
tags:
- publication
---
>At the Getty, I'm building a platform (working title: Octavo) for publishing digital art books. The goal is to generate multiple book formats (web, PDF, EPUB, print on demand) from a single set of text files.  
[Eric Gardner, Building Books with Middleman Extensions](http://egardner.github.io/posts/2015/building-books-with-middleman/)

Eric Gardner, designer et développeur pour Getty Publications, a choisi d'utiliser un générateur de site statique pour *fabriquer* des livres, un choix disruptif mais fonctionnel !