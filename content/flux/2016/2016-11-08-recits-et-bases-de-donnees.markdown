---
layout: post
title: "Récits et bases de données"
date: "2016-11-08T20:00:00"
comments: true
published: true
description: "Pendant 28 minutes Katherine Hayles répond aux questions de Sylvain Bourmeau, dans l'émission La Suite dans les idées sur France Culture."
categories: 
- flux
tags:
- livre
---
>Les *narratifs* semblent être intrinsèques à l'espèce humaine : toutes les cultures en tout moment et en tout lieu ont utilisé les récits. Et maintenant les humains sont confrontés à une ressource additionnelle : les bases de données, par le biais des ordinateurs. Certains théoriciens, comme Manovich, considèrent les narratifs et les bases de données comme des ennemis naturels, mais dans nos sociétés contemporaines je ne pense que ce soit des ennemis naturels, ce sont des combinaisons synergétiques. Je pense qu'avec la littérature électronique, beaucoup d'oeuvres de littérature aujourd'hui crée des *narratifs*, des récits, grâce à des bases de données. Ce ne sont plus des ennemis de ce point de vue là, ce sont des associés dans la compilation d'un travail littéraire.  
[Katherine Hayles, Bien penser à l’ère numérique](https://www.franceculture.fr/emissions/la-suite-dans-les-idees/bien-penser-lere-numerique)

Pendant 28 minutes Katherine Hayles répond aux questions de Sylvain Bourmeau, dans l'émission La Suite dans les idées sur France Culture. Et cela donne une forte envie de lire l'ouvrage de Katherine Hayles, *[Lire et penser en milieux numériques](http://ellug.univ-grenoble-alpes.fr/fr/publications/collections/savoirs-litteraires-et-imaginaires-scientifiques/lire-et-penser-en-milieux-numeriques-72853.kjsp?RH=ELLUG)*.