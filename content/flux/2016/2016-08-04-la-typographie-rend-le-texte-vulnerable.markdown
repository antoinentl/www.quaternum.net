---
layout: post
title: "La typographie rend le texte vulnérable"
date: "2016-08-04T17:00:00"
comments: true
published: true
description: "Citation de Gerard Unger, extraite de Pendant la lecture."
categories: 
- flux
---
>Même si des lettres mises en pages et imprimées selon les règles de la discipline peuvent dégager une impression sinistre, cette autorité n'est souvent qu'apparente. L'apport de la typographie est bien plus important : en fixant et en diffusant la langue sous une forme objective et irrévocable, les mots -- mais aussi les nombres, pensez aux rapports annuels par exemples, sans oublier les illustrations -- sont exposés aux regards scrutateurs et aux esprits critiques. La typographie rend le texte vulnérable.  
[Gerard Unger, Pendant la lecture, page 209](http://editions-b42.com/books/pendant-la-lecture/)