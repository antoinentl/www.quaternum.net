---
layout: post
title: "Reconnaissance typographique inconsciente"
date: "2016-08-02T17:00:00"
comments: true
published: true
description: "Citation de Gerard Unger, extraite de Pendant la lecture."
categories: 
- flux
---
>Les yeux et le cerveau fonctionnent à une vitesse fulgurante et surprenante, et se jouent de nous. Nous avons reconnu et lu les lettres avant même d'en prendre conscience.  
[Gerard Unger, Pendant la lecture, page 146](http://editions-b42.com/books/pendant-la-lecture/)