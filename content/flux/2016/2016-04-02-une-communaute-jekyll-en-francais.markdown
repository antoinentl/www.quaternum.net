---
layout: post
title: "Une communauté Jekyll en français"
date: "2016-04-02T18:00:00"
comments: true
published: true
categories: 
- flux
---
>C’est avec émotion que nous avons le plaisir de vous annoncer le lancement d’un site relayant les actualités de la communauté Jekyll en Français.  
[Bertrand Keller, Lancement de la communauté Jekyll France](http://jekyll-fr.org/2016/04/01/lancement/)

Les intéressés et passionnés par les générateurs de site statique peuvent désormais se retrouver autour de [cette jeune et belle initiave](http://jekyll-fr.org/) !