---
layout: post
title: "Back Office"
date: "2016-03-08T22:00:00"
comments: true
published: true
categories: 
- flux
---
>*Back Office* est une revue annuelle entre design graphique et pratiques numériques co-éditée par les Éditions B42 et Fork Éditions. Elle explore les processus de création en jeu dans la diversité des médias et des pratiques numériques contemporaines. En traitant de thématiques telles que le rapport code/forme, les enjeux des outils de création ou la perméabilité des médias, elle constitue un espace de réflexion unique en langue française et un vecteur de visibilité à l’international pour la communauté francophone.  
[Back Office](http://www.revue-backoffice.com/)

En référence à [Back Cover des éditions B42](http://editions-b42.com/back-cover/), la revue *Back Office* aborde les questions de design numérique. À suivre de près !