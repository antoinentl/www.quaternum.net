---
layout: post
title: "Ev Williams, Medium et le web ouvert"
date: "2016-06-19T17:00:00"
comments: true
published: true
description: "L'EDRLab publie les principes de LCP Readium, la mesure technique de protection pour les livres numériques au format EPUB, qui devrait remplacer la DRM Adobe."
categories: 
- flux
---
>That is the Medium appeal, in a nutshell. Keeping everything from being shit. It wants to do so by adopting many of the tics and habits of the original blogosphere—the intertextuality, the back-and-forth, the sense of amateurism—without being the open web.  
[Robinson Meyer, Ev Williams is The Forrest Gump of the Internet](http://www.theatlantic.com/technology/archive/2016/06/ev-williams-is-the-forrest-gump-of-the-internet/486899/)

Robinson Meyer a rencontré Ev Williams, l'un des fondateurs de Blogger et l'ancien CEO de Twitter, aujourd'hui CEO de [Medium](https://medium.com/) -- cette plateforme de publication [bien connue](https://www.quaternum.net/2016/06/02/le-fanzinat-des-gens-du-web#medium-un-cas--part). Ev Williams explique -- à travers différentes analogies plus ou moins réussies, comparant l'internet ou le web à des archipels et des continents, et parlant des contenus comme de la nourriture -- que le web doit se centraliser, que Medium doit être un espace de confiance permettant à toutes et tous de s'exprimer, face à certains *continents* moins bien intentionnés (mon regard n'est peut-être pas très objectif). Il y a beaucoup de choses intéressantes dans cet article, qui permettent de comprendre comment les grandes plateformes imaginent l'évolution du web -- même si c'est assez inquiétant.

>I proposed that Medium is trying to be the Whole Foods of content. He laughed.