---
layout: post
title: "Imprimer le web"
date: "2016-11-21T12:00:00"
comments: true
published: true
description: "Quelques conseils pour développer une version imprimable d'une page web."
categories: 
- flux
tags:
- publication
---
>Optimizing web pages for print is important because we want our sites to be as accessible as possible, no matter the medium.  
[Manuel Matuzovic, I totally forgot about print style sheets](https://medium.com/@matuzo/i-totally-forgot-about-print-style-sheets-f1e6604cfd6#.ckjnwvy9p)

Quelques conseils pour développer une version imprimable d'une page web.

Version française disponible : [CSS : n'oubliez pas l'impression papier!](https://la-cascade.io/css-noubliez-pas-limpression-papier/).