---
layout: post
title: "Medium évolue"
date: "2016-04-08T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Chez Medium, nous construisons un nouveau recoin de l'Internet spécialement conçu pour les personnes et les éditeurs souhaitant approfondir leur expérience de la conversation et de l’engagement.  
[...]  
Nous voulons que Medium soit votre espace d’écriture unique, votre seule destination pour publier du contenu.  
[Medium France, Rendre Medium encore plus puissant pour les éditeurs](https://medium.com/une-histoire/rendre-medium-encore-plus-puissant-pour-les-%C3%A9diteurs-76d0d1caa7cc#.wbfnnzayp)

Quel est le réel intérêt d'utiliser ce type de plateforme ? Disposer d'une plateforme en mode SAAS, un peu comme Wordpress ? Bénéficier du savoir-faire de Medium en terme de design, de technique (implémentation prévue de AMP notamment), de référencement, de performance et de disponibilité, et des outils de rédaction et de statistiques ? Ajouter à cela la dimension réseau social et l'interaction qui va avec ? Ce serait ce que Wordpress n'est pas totalement parvenu à faire, et le projet de Medium est séduisant.  
Toutefois plusieurs limites me semblent apparaître assez vite :

- l'uniformisation du design -- autant des pages d'accueil que des articles, même s'il faut admettre que la qualité est au rendez-vous ;
- la centralisation des plateformes de publication, ce qui est contraire à ce que le web *pourrait* être ;
- l'approche globale -- stratégie *numérique*, éditorialisation, rédaction, design, contenus sponsorisés -- risque elle aussi d'être uniformisée, au détriment de projets originaux et créatifs.

[Version anglaise de cet article](https://medium.com/the-story/making-medium-more-powerful-for-publishers-39663413a904#.ndey0tybg).

Via [Virginie Clayssen](https://twitter.com/v_clayssen/status/717392164628602882).