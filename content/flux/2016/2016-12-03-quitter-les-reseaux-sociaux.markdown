---
layout: post
title: "Quitter les réseaux sociaux"
date: "2016-12-03T08:00:00"
comments: true
published: true
description: "Neil Jomunsi quitte les réseaux sociaux, dégoûté. Sa critique est juste, attendons de voir ce qui en adviendra !"
categories: 
- flux
tags:
- privacy
---
>Les réseaux sociaux sont de grands dévorateurs d’énergie, d’optimisme et de temps. Ce sont des machines impitoyables. Ces machines se nourrissent de notre force, elles l’aspirent, jouent sur nos ambitions, notre peur du vide, notre besoin de reconnaissance et d’être aimés.  
[Neil Jomunsi, Vous êtes sur la messagerie de Neil Jomunsi, laissez un petit mot après le bip…](http://page42.org/laissez-un-message-apres-le-bip/)

Neil Jomunsi quitte les réseaux sociaux, dégoûté. Sa critique est juste, attendons de voir ce qui en adviendra !