---
layout: post
title: "Technologie et dépendances"
date: "2016-06-07T17:00:00"
comments: true
published: true
description: "Tristan Harris dénonce les manipulations des entreprises de la Silicon Valley à travers les interfaces et les choix qui y sont proposés aux utilisateurs. Il propose un manifeste et met en place des alternatives, par ici : http://timewellspent.io."
categories: 
- flux
---
>Are you upset that technology is hijacking your agency? I am too. I’ve listed a few techniques but there are literally thousands. Imagine whole bookshelves, seminars, workshops and trainings that teach aspiring tech entrepreneurs techniques like this. They exist.  
The ultimate freedom is a free mind, and we need technology to be on our team to help us live, feel, think and act freely.  
[Tristan Harris, How Technology Hijacks People’s Minds — from a Magician and Google’s Design Ethicist](http://www.tristanharris.com/2016/05/how-technology-hijacks-peoples-minds%E2%80%8A-%E2%80%8Afrom-a-magician-and-googles-design-ethicist/)

Tristan Harris dénonce les manipulations des entreprises de la Silicon Valley à travers les interfaces et les choix -- ou les non choix -- qui y sont proposés aux utilisateurs. Il propose un manifeste et met en place des alternatives, par ici : [http://timewellspent.io](http://timewellspent.io/).

En complément de cet article en anglais, un entretien en français sur Rue89, [par là](http://rue89.nouvelobs.com/2016/06/04/tristan-harris-millions-dheures-sont-juste-volees-a-vie-gens-264251).

Je suis assez impressionné du fait que l'article de Tristan Harris est également [sur Medium](https://medium.com/swlh/how-technology-hijacks-peoples-minds-from-a-magician-and-google-s-design-ethicist-56d62ef5edf3#.vk685dgya), ce qui me semble quelque peu en contradiction avec ce qu'il promeut...