---
layout: post
title: "Accessibilité : convaincre les développeurs"
date: "2016-09-26T19:00:00"
comments: true
published: true
description: "Un article qui vise à convaincre les développeurs front-end de produire des objets numériques accessibles, en quelques points facilement applicables."
categories: 
- flux
---
>The inaccessible web doesn’t need to stay the way it is. As front-end developers you have an important, powerful role in making the web the inclusive place it should be.  
[Mischa Andrews, Developers: get started with web accessibility](https://medium.com/@MischaAndrews/developers-get-started-with-web-accessibility-91bd67dea777#.kmds4owk9)

Un article qui vise à convaincre les *développeurs front-end* de produire des objets numériques *accessibles*, en quelques points facilement applicables :

1. Separate structure from style
2. Test your website without CSS
3. Test with a keyboard
4. Change your browser’s text size settings
5. When you’re ready, learn to use a screen reader