---
layout: post
title: "La NASA publie en accès libre ses recherches"
date: "2016-08-25T17:00:00"
comments: true
published: true
categories: 
- flux
---
>NASA just announced that any published research funded by the space agency will now be available at no cost, launching a new public web portal that anybody can access.  
[Peter Dockrill, NASA just made all the scientific research it funds available for free](http://www.sciencealert.com/nasa-just-made-all-the-scientific-research-it-funds-available-for-free)

Cela semble assez incroyable, vu d'ici, mais la NASA ouvre totalement ses recherches ! C'est un signal fort en faveur de l'Open Access.