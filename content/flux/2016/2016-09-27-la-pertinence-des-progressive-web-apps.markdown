---
layout: post
title: "La pertinence des Progressive Web Apps"
date: "2016-09-27T19:00:00"
comments: true
published: true
description: "Un énième discours ventant les mérites des Progressive Web Apps, axé sur l'aspect économique de la chose."
categories: 
- flux
---
>Not all of your customers are going to have your app installed. For those who visit via the web, providing them with a better experience will make them happier and generate more revenue for your business.  
It’s really that simple.  
[Jason Grigsby, Progressive Web Apps Simply Make Sense](https://cloudfour.com/thinks/progressive-web-apps-simply-make-sense/)

Un énième discours ventant les mérites des Progressive Web Apps, axé sur l'aspect économique de la chose :

1. Not every customer or potential customer will have your native app installed
2. You should provide a secure environment for your customers
3. You should provide a faster web experience for web customers
4. Your web customers would benefit from an offline experience
5. Your web users might benefit from push notifications

