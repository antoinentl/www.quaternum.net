---
layout: post
title: "Un regard sur le livre numérique"
date: "2016-01-26T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Je me demande, dans ma difficulté à les lire, ce qui vient des textes (leur style, leur sujet, les cheminements de l’auteur, du traducteur, que sais-je encore) et ce qui vient du support. Vue la variété de mes lectures d’une façon générale, je tends pour le support.  
[Stéphane, Livre numérique : pas encore probant pour moi](http://nota-bene.org/Livre-numerique-pas-encore-probant-pour-moi)

Le regard de Stéphane est plein de spontanéité. À lire avec attention, en prenant en compte les commentaires qui complètent certains points.

>Lecture et écriture, lecture passive et lecture active ; effectivement le sujet est plus vaste et ne se résume pas au support. Il faudrait que je creuse les types de lecture et leur adéquation *pour moi* à un support ou à l’autre.

J'attends un deuxième billet pour mieux comprendre les pratiques de Stéphane !