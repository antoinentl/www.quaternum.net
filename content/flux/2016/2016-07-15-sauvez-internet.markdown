---
layout: post
title: "Sauvez Internet"
date: "2016-07-15T17:00:00"
comments: true
published: true
description: "Cela prend vraiment deux minutes : rendez-vous sur https://www.savetheinternet.eu/ et envoyez un message type à votre régulateur national pour que les grandes entreprises ne soient pas favorisés via les fournisseurs d'accès à Internet. En clair : ne pas traiter de la même façon le trafic de n'importe quel site web serait la pire chose qui pourrait arriver -- économiquement, techniquement et démocratiquement."
categories: 
- flux
---
>Les régulateurs européens vont décider prochainement de donner aux grandes entreprises du secteur des télécommunications le pouvoir d’influencer ce que nous pouvons faire ou ne pas faire en ligne. L’Europe a besoin de toute urgence de fixer un cadre clair à la neutralité du Net afin de protéger nos libertés et nos droits sur Internet.  
[Sauvez Internet](https://www.savetheinternet.eu/fr/)

Cela prend *vraiment* deux minutes : rendez-vous sur [https://www.savetheinternet.eu/](https://www.savetheinternet.eu/) et envoyez un message type à votre *régulateur national* pour que les grandes entreprises ne soient pas favorisés via les fournisseurs d'accès à Internet. En clair : ne pas traiter de la même façon le trafic de n'importe quel site web serait la pire chose qui pourrait arriver -- économiquement, techniquement et démocratiquement.