---
layout: post
title: "Vingt ans d'archivage du web"
date: "2016-10-31T17:00:00"
comments: true
published: true
description: "Une façon simple de voir ou visualiser un site web avec la feuille de style par défaut de votre navigateur préféré, et donc de voir comment les contenus sont structurés !"
categories: 
- flux
tags:
- web
slug: vingt-ans-d-archivage-du-web
---
>Car vingt ans après les premiers pas de l’archivage, si d’énormes progrès ont été faits, de nombreuses questions se posent encore. Le Web ne cesse d’évoluer, et complique la tâche des archiveurs.  
[Morgane Tual, Vingt ans d’archivage du Web : les coulisses d’un projet titanesque](http://www.lemonde.fr/pixels/article/2016/10/26/vingt-ans-d-archivage-du-web-un-projet-titanesque_5020433_4408996.html)

L'archivage du web est presque aussi vieux que le web lui-même, Morgane Tual nous propose de plonger dans les structures et institutions qui opèrent cette tâche délicate.