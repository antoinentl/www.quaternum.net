---
layout: post
title: "Transclusion"
date: "2016-11-19T18:00:00"
comments: true
published: true
description: "iA présente la nouvelle version de son application iA Writer : à l'origine il s'agissait d'un éditeur de texte supportant le Markdown, avec un goût prononcé pour la simplicité et l'efficacité, désormais cela ressemble de plus en plus à une chaîne de publication. iA annonce un nouveau concept pour cette application : la transclusion."
categories: 
- flux
tags:
- publication
---
>Every file reference you insert adds a block of content to your document, be it an image, table, or plain text file. These content blocks can then be ordered, stacked and chained with ease.  
[iA Writer 4](https://ia.net/writer/updates/ia-writer-4)

[iA](https://ia.net/) présente la nouvelle version de son application iA Writer : à l'origine il s'agissait d'un éditeur de texte supportant le Markdown, avec un goût prononcé pour la simplicité et l'efficacité, désormais cela ressemble de plus en plus à une chaîne de publication. iA annonce un *nouveau* concept pour cette application : la transclusion. Cela permet d'intégrer des documents *dans* un document, et ceci simplement avec un chemin vers le fichier concerné (image, autre fichier Markdown, tableau au format CSV, etc.). Un *workflow* simple et efficace. Le concept, et son application, son très prometteurs !

>We think this syntax is a natural extension to Markdown, and it would please us to see other apps use it too.