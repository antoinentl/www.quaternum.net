---
layout: post
title: "L'ambition du design ?"
date: "2016-04-27T22:00:00"
comments: true
published: true
categories: 
- flux
slug: l-ambition-du-design
---
>[...] la poudre que contient le design sert aux feux d'artifices beaucoup plus souvent qu'aux explosifs.  
[Pierre Doze, Les joies irritantes des caresses infécondes, ambition du design et éloge du fourbi](http://www.esadse.fr/fr/post-diplome/171012-la-revue-azimuts?p=171012-la-revue-azimuts)

Dans le numéro 44 de la revue [Azimuts](http://www.esadse.fr/fr/post-diplome/171012-la-revue-azimuts?p=171012-la-revue-azimuts) -- "When design cherishes the ambition to save the world" -- Pierre Doze aborde le concept d'*ambition du design*. Critique, souvent virulent et impitoyable, et certainement lucide, cet article révèle la réalité (d'une partie) du design aujourd'hui. Pour autant il ne faut pas tomber dans un fatalisme béhat, mais plutôt prendre conscience de cette ambiguité qui réside, parfois -- et dans certains projets --, entre discours, motivation, conception et réalisations.

>Impérialismes multiples, condescendance, incongruité, autosatisfaction, sentimentalisme, cécité, prétention, narcissisme, hypocrisie -- là où il faudrait associer l'humilité intellectuelle à l'optimisme, une méfiance systématique à l'endroit de l'ethnocentrisme, le respect de principe à l'égard des contextes où on voudra s'inscrire, impliquant l'enquête et la recherche.

>Le designer est convoqué pour tenir une burette : il ne présente aucun péril pour l'ordre de ce monde qui va mal.