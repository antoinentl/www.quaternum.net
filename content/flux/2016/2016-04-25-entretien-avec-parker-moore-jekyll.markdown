---
layout: post
title: "Entretien avec Parker Moore (Jekyll)"
date: "2016-04-25T22:00:00"
comments: true
published: true
categories: 
- flux
---
>*Que souhaites-tu aux générateurs de site statique ?*  
Mon plus grand souhait c’est qu’ils soient mieux compris de tous et perçus comme une vraie solution, honnête et prête pour la production, pour les sociétés intéressées dans la réalisation de sites web.  
[Bertrand Keller, Entretien avec Parker Moore de Jekyll](http://jekyll-fr.org/2016/04/19/entretien-avec-parker-moore/)

L'un des enjeux autour des générateurs de site statique, pointé par Parker Moore dans cet entretien, est la question de l'*évangélisation* dans des contextes professionnels, un sujet que j'aimerai aborder lors de [la journée de samedi de Sud Web](https://sudweb.fr/2016/#samedi).