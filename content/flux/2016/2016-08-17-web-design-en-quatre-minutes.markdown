---
layout: post
title: "Web design en quatre minutes"
date: "2016-08-17T17:00:00"
comments: true
published: true
description: "Jeremy Thomas a créé une page qui explique, en quatre minutes, ce qu'est le web design - ou design web. C'est interactif et très pédagogique, à mettre entre toutes les mains !"
categories: 
- flux
---
>The *only* thing design can do is **facilitate** the access, consumption, or interaction of that content. **Design is simply a function with content as an input**.  
[Jeremy Thomas, Web Design in 4 minutes](http://jgthms.com/web-design-in-4-minutes/)

[Jeremy Thomas](http://jgthms.com/) a créé une page qui explique, en quatre minutes, ce qu'est le web design -- ou design web. C'est interactif et très pédagogique, à mettre entre toutes les mains ! Jeremy explique sa démarche [par ici](http://jgthms.com/why-i-wrote-web-design-in-4-minutes.html).

Via la [Web Development Reading List](https://wdrl.info/).