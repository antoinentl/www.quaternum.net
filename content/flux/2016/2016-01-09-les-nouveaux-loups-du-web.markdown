---
layout: post
title: "Les nouveaux loups du web"
date: "2016-01-09T17:00:00"
comments: true
published: true
categories: 
- flux
---
>[Les Nouveaux Loup du Web](http://framablog.org/2015/10/23/les-nouveaux-loups-du-web-lavant-premiere/) est un documentaire, et plus précisément la version française du documentaire [Terms and Conditions May Apply](http://tacma.net/), qui démontre ce que les entreprises et les gouvernements peuvent apprendre sur vous au travers de votre vie numérique, le plus souvent à partir d’informations confiées volontairement à des services en ligne.  
[Framasoft, Les Nouveaux Loups du Web](http://framablog.org/2015/10/23/les-nouveaux-loups-du-web-lavant-premiere/)

Rien à voir avec [Citizen Four](https://citizenfourfilm.com/) de Laura Poitras, extraordinaire tant sur le fond que sur la forme, *Les nouveaux loups du web* est une bonne entrée en matière pour découvrir ce qui se passe *derrière* le web d'aujourd'hui, et en prendre conscience.