---
layout: post
title: "Des mots et des emoji"
date: "2016-06-20T17:00:00"
comments: true
published: true
description: "L'équipe de iA a créé un outil permettant de transformer 2 000 mots (anglais) en emoji. C'est drôle, mais surtout très intéressant en terme d'usages !"
categories: 
- flux
---
>*Q: What happens if every word translates into an emoji?*  
A: It’s not useful but it’s a lot of fun.  
[iA, Roger That: Emoji Overdrive](https://ia.net/know-how/roger-that-emoji-overdrive)

L'équipe de iA a créé un outil permettant de transformer 2 000 mots (anglais) en *emoji*. C'est drôle, mais surtout très intéressant en terme d'usages !