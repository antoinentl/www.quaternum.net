---
layout: post
title: "Design Technologist : rassembler"
date: "2016-09-07T17:00:00"
comments: true
published: true
description: "Qu'est-ce qu'un Design Technologist ? Jory Cunningham y répond : ce n'est un développeur web full stack mais la personne qui saura accorder une équipe de designers, de graphistes, de développeurs front end, de développeurs back end, autour de projets numériques/web."
categories: 
- flux
---
>Designers have realized that front-end code is not that much more complicated than mastering Sketch or Illustrator and a knowledge of their materials (browsers, native apps) strengthens their design.  
Engineers have realized that there is a relationship between systematic design pattern thinking and creating DRY code, that there is a mathematic elegance to typographic theory and they can express it in code.  
Organizations understand that designers and the people who realize those designs should have a shared context, timeframe, language, and incentives.  
[Jory Cunningham, Design Technologists: Front-End Development in the Design System Era](https://blog.prototypr.io/design-technologists-front-end-development-in-the-design-system-era-5b0f1fa7b3de#.7g0vwkex5)

Qu'est-ce qu'un *Design Technologist* ? Jory Cunningham y répond : ce n'est pas un développeur web *full stack* mais la personne qui saura accorder une équipe de designers, de graphistes, de développeurs front end, de développeurs back end, autour de projets numériques/web.

>Design Technologists bring a constructive pessimism to the design process.