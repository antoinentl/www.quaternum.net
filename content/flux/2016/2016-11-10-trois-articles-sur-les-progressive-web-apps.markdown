---
layout: post
title: "Trois articles sur les Progressive Web Apps"
date: "2016-11-10T18:00:00"
comments: true
published: true
description: "À travers trois articles complets, Ire Aderinokun donne toutes les informations nécessaires pour la fabrication d'une PWA."
categories: 
- flux
tags:
- outils
---
>So, I decided to create a “Progressive Web Application” for this blog.  
[Ire Aderinokun, “Offline First” with Service Worker (Building a PWA, Part 1)](https://bitsofco.de/bitsofcode-pwa-part-1-offline-first-with-service-worker/)

À travers trois articles complets, [Ire Aderinokun](https://bitsofco.de/) donne toutes les informations nécessaires pour la fabrication d'une <abbr title="Progressive Web Application”">PWA</abbr>.