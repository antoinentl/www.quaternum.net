---
layout: post
title: "Coder son site web personnel"
date: "2016-08-27T17:00:00"
comments: true
published: true
categories: 
- flux
---
>When designers code their own site they have total control.  
[Donny Truong, Do Designers Create Their Own Site Anymore?](https://www.visualgui.com/2016/08/23/do-designers-create-their-own-site-anymore/)

Est-ce que les *designers* ne devraient pas *coder* leur site web personnel plutôt que de se reposer sur des plateformes comme Squarespace ou Adobe Portfolio ? Définitivement oui. Mais cela ne veut pas forcément dire créer un CMS complet ou écrire une feuille de style complexe *from scratch*, il y a des frameworks et des bibliothèques qui existent, à chacun d'explorer les possibilités du web.