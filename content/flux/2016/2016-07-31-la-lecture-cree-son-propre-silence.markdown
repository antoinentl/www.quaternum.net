---
layout: post
title: "La lecture crée son propre silence"
date: "2016-07-31T17:00:00"
comments: true
published: true
description: "Citation de Gerard Unger, extraite de Pendant la lecture."
categories: 
- flux
---
>La lecture crée son propre silence.  
[...]  
La naissance du silence s'accompagne d'un autre phénomène prodigieux : non seulement l'environnement semble s'estomper, mais aussi l'objet sur lequel notre attention s'était tout d'abord concentrée. Les caractères d'imprimerie se fondent dans nos pensées comme un comprimé effervescent dans un verre d'eau.  
[Gerard Unger, Pendant la lecture, pages 49 et 50](http://editions-b42.com/books/pendant-la-lecture/)