---
layout: post
title: "TypoDiplo"
date: "2016-12-02T19:00:00"
comments: true
published: true
description: "Belle initiative du Monde diplomatique qui met à disposition ses règles typographiques. Le site est basé sur SeenThis, service de short-blogging."
categories: 
- flux
tags:
- typographie
---
>TypoDiplo, le site des usages du Monde diplomatique, a été créé par les correcteurs et l’équipe Internet du journal. Il ambitionne d’être une référence originale aussi bien en matière orthotypographique que pour des données géopolitiques. Depuis décembre 2016, il est proposé librement aux internautes.  
[TypoDiplo](http://typo.mondediplo.net/)

Belle initiative du [Monde diplomatique](http://www.monde-diplomatique.fr/) qui met à disposition ses règles typographiques. Le site est basé sur [SeenThis](https://seenthis.net/), service de *short-blogging*.