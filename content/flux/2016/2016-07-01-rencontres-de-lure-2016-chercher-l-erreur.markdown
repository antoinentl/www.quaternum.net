---
layout: post
title: "Rencontres de Lure 2016 : Chercher l'erreur"
date: "2016-07-01T17:00:00"
comments: true
published: true
description: "Hugh McGuire tente de définir le livre, de comprendre pourquoi le livre et le web sont s'y éloignés — et pourtant tous les deux si essentiels dans nos cultures basées sur les savoirs —, et d'imaginer comment l'un peut apprendre de l'autre. Un article à lire pour mieux comprendre comment le livre numérique pourrait évoluer."
categories: 
- flux
slug: rencontres-de-lure-2016-chercher-l-erreur
---
>Chercher l’erreur – Égarements et odyssées graphiques  
[Les Rencontres internationales de Lure](http://delure.org/les-rencontres/rencontres-2016)

L'inscription à la semaine d'été des Rencontres de Lure est ouverte depuis plusieurs semaines, **inscrivez-vous vite avant le 14 juillet !** La semaine d'été des Rencontres de Lure en quelques points :

1. des conférences autour du design graphique, de la typographie, du texte, des livre, du numérique, de l'illustration ;
2. une bonne vingtaine d'intervenants à découvrir et avec qui débattre ;
3. une centaine de participants à rencontrer, pour échanger ou plus si affinités (notamment monter des projets, échanger des livres, apprendre) ;
4. un cadre exceptionnel pour une mise à l'écart du monde pendant six jours.

La thématique de cette édition 2016 : l'erreur.

>De la simple bourde à la faute en passant par la coquille, l’erreur peut être de jeunesse, 404, de justice, voire fatale. Au fil de nos corrections, ctrl+Z, relectures et repentirs, elle s’efface devant l’ardeur avec laquelle nous la chassons. Cherchez l’erreur et vous trouverez bavure, bug, impair, défaut, illusion, mais aussi de sympathiques maladresses. Cultivez l’imperfection et vous courez à la rencontre de perdants magnifiques, d’anti-héros édifiants, d’imperturbables et dignes mordus de poussière.  
Et pourtant, l’erreur révèle aussi les failles d’un système que l’on croyait trop parfait. Elle nous renvoie à nos limites, défie notre imagination, nous met face à l’imprévu et force notre créativité. Cette déviance parfois infime, toujours déroutante, nous bouscule hors des sentiers battus. On tombe, on s’en étonne, on en rit, ou bien on s’effraie du chaos qui menace.  
Le design graphique et la typographie se nourrissent d’avaries et de soubresauts, qui sont autant d’invitations à une réinvention permanente. Vous aussi venez échouer avec nous aux Rencontres internationales de Lure du 21 au 27 août 2016 à Lurs, Alpes-de-Haute-Provence.