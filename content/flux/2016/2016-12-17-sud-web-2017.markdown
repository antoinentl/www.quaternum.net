---
layout: post
title: "Sud Web 2017"
date: "2016-12-17T23:00:00"
comments: true
published: true
description: "Sud Web -- la conférence itinérante des travailleurs du web -- 2017 aura lieu les 19 et 20 mai 2017 à Aix-en-Provence !"
categories: 
- flux
tags:
- web
---
>La 7e édition de Sud Web aura lieu à Aix-en-Provence, les 19 et 20 mai 2017 [...].  
[Sud Web, Ouverture des inscriptions pour Sud Web](https://sudweb.fr/blog/2017/ouverture-des-inscriptions-pour-sud-web/)

Sud Web -- "*la* conférence itinérante des travailleurs du web" -- 2017 aura lieu les 19 et 20 mai 2017 à Aix-en-Provence ! Les inscriptions sont ouvertes, et il y a toujours un appel à propositions en cours, [proposez un sujet](https://sudweb.fr/2017/appel-a-sujets/) !