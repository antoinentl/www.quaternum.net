---
layout: post
title: "Le code source de www.010101book.net sur GitHub"
date: "2016-12-19T23:00:00"
comments: true
published: true
description: "J'ai enfin déposé le code source du livre web Le livre 010101 (1971-2015) de Marie Lebert, www.010101book.net, sur GitHub."
categories: 
- flux
tags:
- ebook
slug: le-code-source-de-www-010101-book-net-sur-github
---
>Datée de novembre 2015, une grande saga du livre numérique de juillet 1971 à nos jours.  
[Le livre 010101 (1971-2015), www.010101book.net](https://github.com/antoinentl/010101book.net)

J'ai enfin déposé le code source du livre web *Le livre 010101 (1971-2015)* de Marie Lebert, [www.010101book.net](https://www.010101book.net/), sur GitHub. Le site web est généré avec Jekyll, et il y a quelques modifications pour la génération de la table des matières notamment.