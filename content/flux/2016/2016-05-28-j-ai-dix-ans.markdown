---
layout: post
title: "J'ai dix ans"
date: "2016-05-28T23:00:00"
comments: true
published: true
description: "À défaut d'avoir pu être présent à Sud Web, Bastien a écrit une lettre qui a été lue sur scène, un beau texte qui donne envie de ne pas oublier qu'on peut encore avoir dix ans."
categories: 
- flux
slug: j-ai-dix-ans
---
>Je fais aujourd’hui ce que vous me voyez faire depuis mes 10 ans : j’écris des programmes, je pianote, j’écris des poèmes.  
[Bastien, J'ai dix ans](https://bzg.fr/jai-dix-ans.html)

À défaut d'avoir pu être présent à [Sud Web](https://sudweb.fr/2016/), [Bastien](https://bzg.fr/) a écrit une lettre qui a été lue sur scène, un beau texte qui donne envie de ne pas oublier qu'on peut encore avoir dix ans.

>Mais quand tout va bien, j’ai dix ans : je joue, je construis, je code, je partage, j’écris, j’avance, j’m’en fous.  
Cher parents, voici donc ce que je fais : j’essaie d’avoir tous les jours 10 ans et de me sentir aussi joyeusement libre que je l’étais sous votre règne discret.