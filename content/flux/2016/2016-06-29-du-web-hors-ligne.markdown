---
layout: post
title: "Du web hors ligne"
date: "2016-06-29T17:00:00"
comments: true
published: true
description: "Rapide retour d'Alexis sur un hackathon autour du web hors connexion, questionnements assez éloignés de simples sites web consultables hors ligne, mais plutôt dans l'idée de pouvoir accéder à de grandes quantités d'informations, dans des environnements où il n'y a pas du tout d'accès internet."
categories: 
- flux
---
>[...] nous nous sommes rapidement rendu compte que l'un des points intéressants était la production de contenus hors-ligne, sa mise à disposition et sa mise à jour.  
[Alexis, Consultez vos sites web hors-ligne](https://blog.notmyidea.org/consultez-vos-sites-web-hors-ligne.html)

Rapide retour d'Alexis sur un hackathon autour du web hors connexion, questionnements assez éloignés de [simples sites web consultables hors ligne](https://www.quaternum.net/2016/06/23/quatre-bonnes-raisons-de-faire-du-web-hors-connexion), mais plutôt dans l'idée de pouvoir accéder à de grandes quantités d'informations, dans des environnements où il n'y a pas *du tout* d'accès internet.