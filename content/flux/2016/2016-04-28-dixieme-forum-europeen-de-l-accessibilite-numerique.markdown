---
layout: post
title: "Dixième Forum européen de l'accessibilité numérique"
date: "2016-04-28T07:00:00"
comments: true
published: true
categories: 
- flux
slug: dixieme-forum-europeen-de-l-accessibilite-numerique
---
>La frontière entre le monde des objets physiques et celui des systèmes d’information numériques s’estompe un peu plus chaque jour de sorte que nous sommes de plus en plus souvent connectés dans notre vie quotidienne, à titre individuel et professionnel. Cet environnement constitue une infrastructure que l’on appelle aussi l’Internet des Objets dont l’étendue et l’accroissement ne cessent d’étonner.  
[...]  
On pourrait espérer que ces des objets intelligents et connectés par le moyen d’Internet facilitent la vie des personnes handicapées, des personnes âgées, dans bien des situations. Invisibles à la maison, dans les transports en commun, sur le lieu de travail, les objets équipés de capteurs peuvent doter les services de la capacité à s’adapter aux besoins particuliers de leurs usagers. Des exemples concrets du confort et de la simplicité apportés par de tels objets commencent à émerger et de nombreux projets de recherche ont été lancés pour explorer et développer les possibilités de ces technologies pour réduire l’exclusion numérique.  
[10e Forum européen de l'accessibilité numérique, L’accessibilité numérique dans un monde connecté](http://inova.snv.jussieu.fr/evenements/colloques/colloques/89_index_fr.html)

Belle thématique pour le 10e Forum européen de l'accessibilité numérique : les *objets connectés*.