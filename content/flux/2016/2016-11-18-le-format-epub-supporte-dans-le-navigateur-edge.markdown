---
layout: post
title: "Le format EPUB supporté dans le navigateur Edge"
date: "2016-11-18T18:00:00"
comments: true
published: true
description: "Le navigateur Edge de Microsoft Windows est donc capable de lire un fichier EPUB, sans application ou extension particulières, c'est une très bonne nouvelle !"
categories: 
- flux
tags:
- ebook
---
>The reading experience will get even better with the Windows 10 Creators Update! In addition to providing a great reading experience for PDF files – you can now read any unprotected e-book in the EPUB file format with Microsoft Edge.  
[Dona Sarkar, Announcing Windows 10 Insider Preview Build 14971 for PC](https://blogs.windows.com/windowsexperience/2016/11/17/announcing-windows-10-insider-preview-build-14971-for-pc/)

Le navigateur Edge de Microsoft Windows est donc capable de lire un fichier EPUB, sans application ou extension particulières, c'est une très bonne nouvelle !