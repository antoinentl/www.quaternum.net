---
layout: post
title: "Service Workers, en pratique"
date: "2016-02-10T22:00:00"
comments: true
published: true
categories: 
- flux
slug: service-workers
---
>Recently, Service Workers made quite some noise and people encourage to use this very newest technology on every website. But when I looked at the implementation and specification, I ended up having more questions than answers to it.  
[Anselm Hannemann, Open Service Worker Questions](https://helloanselm.com/2016/open-service-worker-questions/)

Retours et questions -- et quelques réponses -- sur Service Workers, le protocole qui permet de gérer le hors connexion pour le web.