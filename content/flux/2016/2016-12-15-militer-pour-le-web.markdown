---
layout: post
title: "Militer pour le web"
date: "2016-12-15T23:00:00"
comments: true
published: true
description: "Expliquer à son entourage l'état du web et les alternatives qui existent face aux silos, une bonne idée -- militer et vulgariser -- de David en cette d'année."
categories: 
- flux
tags:
- privacy
---
>Pourquoi ne pas profiter de l’ambiance festive entre les trolls sportifs et les discussions politiques (ou l’inverse) pour éduquer sur l’état du web et pointer quelques pistes pour améliorer la situation ?  
[David Larlet, Militantisme festif](https://larlet.fr/david/blog/2016/militantisme-festif/)

Expliquer à son entourage *l'état du web* et les alternatives qui existent face aux silos, une bonne idée -- militer et vulgariser -- de [David](https://larlet.fr/david/) en cette d'année.