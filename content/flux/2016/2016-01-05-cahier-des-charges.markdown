---
layout: post
title: "Un cahier des charges ouvert, ça n'existe pas"
date: "2016-01-05T22:00:00"
comments: true
published: true
categories: 
- flux
slug: cahier-des-charges
---
>Ce qu'il faut bien comprendre, c'est qu'au-delà de ce que griffonne le designer dans son carnet à dessins, un cahier des charges ouvert, ça n'existe pas. Il y aura toujours des restrictions imposées par le contexte, si elles ne le sont pas déjà par le contrat, ainsi que des contraintes financières, matérielles et d'autres relatives aux conditions de production.  
[Norman Potter, *Qu'est-ce qu'un designer*, page 67](http://editions-b42.com/books/quest-ce-quun-designer/)