---
layout: post
title: "Un entretien sous la forme d'une conversation"
date: "2016-08-16T17:00:00"
comments: true
published: true
description: "Cet entretien n'est pas un texte linéaire : il prend la forme d'une conversation entre le chatbot d'uxdesign.cc, Oliver Reichenstein et le lecteur. Cette nouvelle forme, dynamique - ou supposée dynamique -, rend le lecteur actif, un peu à la manière des livres dont vous êtes le héros. Cette nouvelle forme, assez disruptive pour un lecteur habitué à un contenu figé, pose beaucoup de questions : quels usages ? quel statut par rapport à un texte classique ? le lecteur a-t-il une certaine liberté ? etc."
categories: 
- flux
slug: un-entretien-sous-la-forme-d-une-conversation
---
>Icons are weak at transporting exact meaning, but powerful at transporting emotion.  
[Oliver Reichenstein on Icons - UX Chat](http://uxchat.me/oliver-reichenstein-on-icons/)

Cet entretien d'[uxdesign.cc](https://uxdesign.cc/) avec Oliver Reichenstein d'[iA](https://ia.net/) aborde la question des icônes, dans le cas de l'UX design.

Cet *entretien* n'est pas un texte linéaire : il prend la forme d'une conversation entre le [chatbot](https://fr.wikipedia.org/wiki/Agent_conversationnel) d'uxdesign.cc, Oliver Reichenstein et le lecteur. Cette nouvelle forme, dynamique -- ou supposée dynamique --, rend le lecteur actif, un peu à la manière des [livres dont vous êtes le héros](https://fr.wikipedia.org/wiki/Livre-jeu). Cette nouvelle forme, assez disruptive pour un lecteur habitué à un contenu figé, pose beaucoup de questions : quels usages ? quel statut par rapport à un texte *classique* ? le lecteur a-t-il une certaine liberté ? etc.