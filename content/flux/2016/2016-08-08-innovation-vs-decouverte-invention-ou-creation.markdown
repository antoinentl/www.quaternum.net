---
layout: post
title: "Innovation VS découverte, invention ou création"
date: "2016-08-08T23:00:00"
comments: true
published: true
description: "Xavier de La Porte définit avec beaucoup de pertinence, et non sans une critique fondée, la notion d'innovation, et le mouvement de fond qui s'opère sur la recherche. Cette chronique est toujours en écoute (ou en lecture) sur France Culture, mais optez plutôt pour l'ouvrage regroupant ses chroniques Ce qui nous arrive sur la toile publié par C&F Édition."
categories: 
- flux
---
>Pourquoi innover ? C'est quoi les perspectives d'une civilisation qui préfère innover plutôt que découvrir, inventer ou créer ? C'est quoi l'horizon ?  
[Xavier de La Porte, La tête dans la toile](http://cfeditions.com/toile/)

Xavier de La Porte définit avec beaucoup de pertinence, et non sans une critique fondée, la notion d'innovation, et le mouvement de fond qui s'opère sur la recherche. Cette chronique est toujours en écoute (ou en lecture) [sur France Culture](http://www.franceculture.fr/emissions/ce-qui-nous-arrive-sur-la-toile/ce-qui-se-cache-derriere-linnovation), mais optez plutôt pour l'ouvrage regroupant ses chroniques, "Ce qui nous arrive sur la toile", [publié par C&F Éditions](http://cfeditions.com/toile/).