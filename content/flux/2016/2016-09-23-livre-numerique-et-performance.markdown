---
layout: post
title: "Livre numérique et performance"
date: "2016-09-23T19:00:00"
comments: true
published: true
description: "Jiminy Panoz soulève un sujet peu abordé dans le domaine du livre numérique : la performance. Il attire l'attention sur un point essentiel de l'expérience utilisateur, souvent délaissée par les producteurs/développeurs de livres numériques -- au détriment des éditeurs..."
categories: 
- flux
---
>Performance is not an afterthought, it’s a main objective. And I hope I have convinced you that you should care.  
[Jiminy Panoz, Let’s talk about eBook performance](https://medium.com/@jiminypan/lets-talk-about-ebook-performance-801b83745ea4#.wqhzdh4ke)

Jiminy Panoz soulève un sujet peu abordé dans le domaine du livre numérique : la performance. Il attire l'attention sur un point essentiel de l'expérience utilisateur, souvent délaissée par les producteurs/développeurs de livres numériques -- au détriment des éditeurs...

>How many of us, eBook producers, actually have a precise idea of mobile apps’ or eInk Readers’ performance? Can you list CSS or JS bottlenecks? Do you define rules or best practices based on performance? Do you know how Reading Systems work? Really?