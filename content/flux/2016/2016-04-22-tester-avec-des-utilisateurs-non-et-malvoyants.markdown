---
layout: post
title: "Tester avec des utilisateurs non et malvoyants"
date: "2016-04-22T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Inclure des utilisateurs non-voyants dans les tests utilisateur, une démarche encore isolée.  
[Marion Lévêque, 4 recommandations pour mener un test utilisateur avec des non et malvoyants](http://newflux.fr/4-recommandations-pour-mener-un-test-utilisateur-avec-des-non-et-malvoyants/)

Retour d'expérience court mais intéressant, concernant un test utilisateur qui inclut des personnes non et malvoyantes.