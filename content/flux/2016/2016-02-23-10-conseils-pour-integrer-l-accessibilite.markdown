---
layout: post
title: "10 conseils pour intégrer l'accessibilité"
date: "2016-02-23T22:00:00"
comments: true
published: true
categories: 
- flux
slug: 10-conseils-pour-integrer-l-accessibilite
---
>1. Use the correct semantic structure for headings  
[Rebecca Topps, 10 ways you can consider web accessibility in design](http://toppsusability.co.uk/posts/10-ways-you-can-consider-web-accessibility-in-design.html)

Rebecca Topps propose 10 façons de prendre en compte l'accessibilité en design. Il s'agit plutôt de 10 points importants -- de la structuration des contenus aux formulaires en passant par le repérage dans le site -- à intégrer à chaque conception.