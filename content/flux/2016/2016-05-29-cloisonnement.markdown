---
layout: post
title: "Cloisonnement ?"
date: "2016-05-29T23:00:00"
comments: true
published: true
description: "Un retour de Sud Web par l'un des participants, Alexis, qui résume très bien ce qu'est Sud Web : une réflexion sur la façon dont nous souhaitons travailler – dans le web ou ailleurs –, et peut-être vivre."
categories: 
- flux
---
>Jusqu'ici je me suis dit que je devais choisir. J'ai pensé naivement que je ne pouvais pas être et un brasseur et un developpeur, mais la réalité c'est que c'est exactement ce que je suis: les deux.  
[Alexis, Cloisonnement des activités ?](https://blog.notmyidea.org/cloisonnement-des-activites.html)

Un retour de [Sud Web](https://sudweb.fr/2016/) par l'un des participants, [Alexis](https://blog.notmyidea.org/), qui résume très bien ce qu'est Sud Web : une réflexion sur la façon dont nous souhaitons travailler -- dans le web ou ailleurs --, et peut-être vivre.