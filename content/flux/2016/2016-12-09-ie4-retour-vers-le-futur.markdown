---
layout: post
title: "IE4, retour vers le futur"
date: "2016-12-09T08:00:00"
comments: true
published: true
description: "Pour #nowwwel Gaël Poupard présente les nouveautés d'Internet Explorer 4, le navigateur de Microsoft sorti en 1997, oui oui ! Et finalement ce navigateur posait, à cette époque, des concepts et projets qui sont encore d'actualité."
categories: 
- flux
tags:
- web
---
>C'est rigolo comme les thèmes abordés dans les notes de version sont les mêmes en 1997 qu'en 2016, non ?  
[Gaël Poupard, Les nouveautés dʼIE4](http://www.ffoodd.fr/les-nouveautes-d%ca%bcie4/)

Pour [#nowwwel](https://www.twitter.com/search?f=tweets&vertical=default&q=%23nowwwel) Gaël Poupard présente les nouveautés d'Internet Explorer 4, le navigateur de Microsoft sorti en 1997, oui oui ! Et finalement ce navigateur posait, à cette époque, des concepts et projets qui sont encore d'actualité (hors connexion, CSS, pagination, etc.).