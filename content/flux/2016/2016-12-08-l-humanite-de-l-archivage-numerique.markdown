---
layout: post
title: "L'humanité de l'archivage numérique"
date: "2016-12-08T17:00:00"
comments: true
published: true
description: "L'archivage numérique comporte forcément des vides, et c'est peut-être ce qu'il reste de poésie ou d'humain au numérique."
categories: 
- flux
tags:
- web
slug: l-humanite-de-l-archivage-numerique
---
>Malgré notre volonté de tout garder, malgré le haut niveau de technicité que nous mettons à cela, nous sommes condamnés à ne conserver que des instantanés. Nous devons admettre les vides et les absences.  
[Xavier de La Porte, Archiver le web, c'est être condamné aux vides](https://www.franceculture.fr/emissions/la-vie-numerique/archiver-le-web-cest-etre-condamne-aux-vides)

L'archivage numérique comporte forcément des vides, et c'est peut-être ce qu'il reste de poésie ou d'humain au numérique.