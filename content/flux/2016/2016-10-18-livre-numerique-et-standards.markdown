---
layout: post
title: "Livre numérique et standards"
date: "2016-10-18T17:00:00"
comments: true
published: true
description: "La fusion de l'IDPF et du W3C révèle des tensions jusque-là *étouffée*, Dave Cramer rappelle les enjeux et contraintes pour travailler autour des standards."
categories: 
- flux
tags:
- ebook
---
>Standards work is political—people with different interests, perspectives, and goals trying to agree on what should be done.  
[Dave Cramer, Thirteen Ways of Looking at Ebook Standards](https://medium.com/@dauwhe/thirteen-ways-of-looking-at-ebook-standards-affdc75091bf#.c08hoe1ha)

La fusion de l'<abbr title="International Digital Publishing Forum">IDPF</abbr> et du <abbr title="World Wide Web Consortium">W3C</abbr> révèle des tensions jusque-là *étouffée*, Dave Cramer rappelle les enjeux et contraintes pour travailler autour des standards.