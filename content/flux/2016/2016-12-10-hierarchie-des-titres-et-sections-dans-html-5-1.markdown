---
layout: post
title: "Hiérarchie des titres et sections dans HTML 5.1"
date: "2016-12-10T08:00:00"
comments: true
published: true
description: "Ire Aderinokun présente un changement important dans HTML 5.1, la structuration avec les sections, qui semble désormais plus logique, puisque la hiérarchie des titres est désormais respectée."
categories: 
- flux
tags:
- web
slug: hierarchie-des-titres-et-sections-dans-html-5-1
---
>Now, the advised method for creating a document outline is to still make use of nested <section> elements, but in combination with the appropriate heading rank for each section.  
[Ire Aderinokun, Document Outlines in HTML 5.1](https://bitsofco.de/document-outlines-in-html-5-1/)

Ire Aderinokun présente un changement important dans HTML 5.1, la structuration avec les `<section>`, qui semble désormais plus logique, puisque la hiérarchie des titres est désormais respectée.