---
layout: post
title: "Dégooglisons"
date: "2016-10-20T17:00:00"
comments: true
published: true
description: "Framasoft continue de mettre en place des alternatives aux services de Google et autres plateformes en ligne, et c'est impressionnant."
categories: 
- flux
tags:
- outils
---
>6 nouveaux services et une annonce majeure pour attaquer la 3e – et dernière – année de notre (modeste) plan de libération du monde… De moins en moins modeste : déjà trente services à ce jour !  
[Framasoft, Dégooglisons saison 3 : 30 services alternatifs aux produits de Google & co](https://framablog.org/2016/10/03/degooglisons-saison-3-30-services-alternatifs-aux-produits-de-google-co/)

Framasoft continue de mettre en place des alternatives aux services de Google et autres plateformes en ligne, et c'est impressionnant.