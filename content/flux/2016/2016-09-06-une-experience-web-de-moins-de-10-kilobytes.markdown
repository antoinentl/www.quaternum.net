---
layout: post
title: "Une expérience web de moins de 10 kilobytes"
date: "2016-09-06T17:00:00"
comments: true
published: true
description: "A List Apart lance un concours, la règle : créer un site web fonctionnel avec 10Kb de fichiers maximum, et sans JavaScript ! Un retour aux bases, avec un accent sur la performance, l'accessibilité et le lazy load, excitant !"
categories: 
- flux
---
>It gives us great pleasure to announce the 2016 10k Apart competition. Create a fully functioning website in 10 KB or less! Amaze your friends! Astound the world! Compete for fabulous prizes!  
[Jeffrey Zeldman, Another 10k Apart: Create a Website in 10 KB, Win Prizes!](http://alistapart.com/article/another-10k-apart)

A List Apart lance un concours, la règle : créer un site web fonctionnel avec 10Kb de fichiers maximum, et sans JavaScript ! Un retour aux bases, avec un accent sur la performance, l'accessibilité et le *lazy load*, excitant ! Toutes les informations sur le site de l'événement : [https://a-k-apart.com](https://a-k-apart.com/)