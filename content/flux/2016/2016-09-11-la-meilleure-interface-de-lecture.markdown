---
layout: post
title: "La meilleure interface de lecture"
date: "2016-09-11T12:00:00"
comments: true
published: true
description: "Citation d'Alessandro Ludovico."
categories: 
- flux
---
>[...] si vous lisez ces lignes sur papier (ce qui est certainement le cas), c'est donc que vous avez, pour une raison ou pour une autre, choisi d'utiliser le "vieux" medium. Pourquoi ? Probablement parce qu'il propose toujours la meilleure "interface" jamais conçue à ce jour.  
[Alessandro Ludovico, Post-Digital Print, page 7](http://editions-b42.com/books/post-digital-print/)