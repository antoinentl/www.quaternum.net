---
layout: post
title: "Renforcement de l'accessibilité des sites web du secteur public en Europe"
date: "2016-05-06T18:00:00"
comments: true
published: true
description: "Voilà une bonne nouvelle -- datée du 3 mai 2016 -- pour le web (européen), et c'est suffisamment rare pour être signalé : des règles vont être mises en place pour renforcer l'accessibilité des sites web et applications mobiles du secteur public."
categories: 
- flux
slug: renforcement-de-l-accessibilite-des-sites-web-du-secteur-public-en-europe
---
>Quelque 80 millions de personnes dans l’Union sont touchées par un handicap. Avec le vieillissement de la population, ce chiffre devrait passer à 120 millions d’ici à 2020. Une approche commune dans le domaine de l'accessibilité du web contribuera à rendre la société numérique plus inclusive et à libérer le potentiel du marché unique numérique pour tous les Européens.  
La directive couvrira les sites web et les applications mobiles des organismes du secteur public, des administrations aux universités en passant par les tribunaux, les services de police, les hôpitaux publics et les bibliothèques. Elle les rendra accessible à tous, et notamment aux personnes malvoyantes, malentendantes et souffrant de handicaps fonctionnels.  
[Commission européenne, La Commission salue l’accord visant à rendre plus accessibles les sites web et les applications mobiles du secteur public](http://europa.eu/rapid/press-release_IP-16-1654_fr.htm)

Voilà une bonne nouvelle -- datée du 3 mai 2016 -- pour le web (européen), et c'est suffisamment rare pour être signalé : des règles vont être mises en place pour renforcer l'accessibilité des sites web et applications mobiles du secteur public.

Parmi les points de la directive, un élément me semble intéressant à noter : "Le texte de la directive [...] prévoit un suivi régulier des sites web et des applications mobiles par les États membres, lesquels devront établir des rapports à ce sujet. Ces rapports devront être communiqués à la Commission et rendus publics."

Espérons que cette directive dépasse l'effet d'annonce.