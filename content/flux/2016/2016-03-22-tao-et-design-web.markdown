---
layout: post
title: "Tao et design web"
date: "2016-03-22T12:00:00"
comments: true
published: true
categories: 
- flux
---
>Lorsqu'un nouveau média emprunte à un média préexistant, une partie de cet emprunt est sensée, mais le reste n'est pas réfléchi, il est « rituel », et limite souvent le nouveau support. Avec le temps, le dernier-né développe ses propres conventions, rejetant celles qui ne veulent plus rien dire.  
[...]  
Il est temps de reléguer aux oubliettes les rites de l'imprimé, et de s'engager à fond dans le média virtuel et sa nature propre.  
[...]  
En tant que designers, nous devons repenser ce rôle déifié, abandonner l'idée du contrôle, et explorer un nouveau mode relationnel avec la page.  
[John Allsopp, Le tao du design Web](http://www.pompage.net/traduction/dao)

Cet article a été publié en février 2003 -- et en avril 2000 pour le texte original en anglais -- et pourtant il est intéressant de se (re)plonger dans ces enjeux et relations entre imprimé et numérique, qui sont encore bien présents aujourd'hui -- même 16 ans plus tard. Quelques extraits :

>Construisez des pages qui sont accessibles, indifféremment de la plateforme ou de l'écran que votre lecteur a choisi ou qu'il doit utiliser pour parvenir jusqu'à vos pages. Cela signifie faire des pages qui sont lisibles indépendamment de la résolution ou de la taille de l'écran, du nombre de couleurs (et rappelez-vous aussi que certaines pages doivent être imprimées ou lues par des logiciels de synthèse vocale, ou encore lues via des navigateurs braille). Cela signifie des pages qui s'adaptent aux besoins d'un lecteur dont la vue est tout sauf parfaite et qui désire lire des pages avec des polices de caractères de très grande taille.  
Concevoir des pages que l'on peut adapter, c'est concevoir des pages accessibles.

>Laissez la forme emboîter le pas à la fonction, plutôt que de tenter de prendre un design spécifique et de le faire "fonctionner".

>Divers périphériques (webtélé, moniteurs à haute résolution, assistants numériques personnels) ont des tailles de fenêtre minimales et maximales fort différentes.

>Le temps est venu pour le média Web de se redéfinir au-delà de ses origines, au-delà de l'imprimé. Non pour le simple plaisir d'abandonner autant d'érudition et d'expérience, mais pour tracer, lui aussi, sa propre voie, lorsque cela est indiqué.

>Je crois que la plus grande force du Web est souvent perçue comme une limite, comme un défaut. La flexibilité fait partie intégrante de la nature du Web et c'est notre rôle à nous, concepteurs et développeurs, d'épouser cette souplesse, et de créer des pages qui, fortes de cette adaptabilité, deviendront accessibles à tous.

[Via Baptiste (Tout ce qui bouge)](http://toutcequibouge.net/2016/03/six-figures-du-designer/).