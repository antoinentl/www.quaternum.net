---
layout: post
title: "Livres numériques standards"
date: "2016-08-13T17:00:00"
comments: true
published: true
description: "Des livres numériques de bonne qualité - traduction, OCR, règles typographiques."
categories: 
- flux
---
>Free and liberated ebooks, carefully produced for the true book lover.  
[Standard Ebooks](https://standardebooks.org/)

Des livres numériques de bonne qualité -- traduction, OCR, règles typographiques.

Via [iGor](https://igor.milhit.ch/)