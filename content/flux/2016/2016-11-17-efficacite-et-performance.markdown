---
layout: post
title: "Efficacité et performance"
date: "2016-11-17T18:00:00"
comments: true
published: true
description: "Frank Taillandier propose une nouvelle traduction d'un article écrit initialement en anglais : Ben Balter présente plusieurs des qualités communes qu'il a remarqué chez les personnes travaillant chez GitHub, et principalement sous l'angle de la performance."
categories: 
- flux
tags:
- performance
---
>GitHub a une approche du travail très particulière et étant donné que je me suis démené dernièrement pour décrire cette culture à mes collègues, j’ai décidé de documenter quelques-uns des points communs que j’ai pu identifier à plusieurs reprises chez les GitHubbers les plus performants.  
[Ben Balter, Les sept habitudes des gens super efficaces chez GitHub](https://frank.taillandier.me/2016/11/15/sept-habitudes-productives-chez-github/)

[Frank Taillandier](https://frank.taillandier.me/) propose une nouvelle traduction d'un article écrit initialement en anglais : Ben Balter présente plusieurs des qualités communes qu'il a remarqué chez les personnes travaillant chez GitHub, et principalement sous l'angle de la performance :

1. Professionnel, mais pas formel
2. Livrez tôt, livrez souvent
3. Si vous remarquez quelque chose, dites quelque chose.
4. Curiosité et amélioration personnelle
5. Toujours prêt à aider (mais savoir quand dire “non”)
6. Contribuez à l’économie de la valorisation
7. Honnêteté, intégrité et authenticité