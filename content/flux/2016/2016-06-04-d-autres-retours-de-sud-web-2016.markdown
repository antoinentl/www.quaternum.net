---
layout: post
title: "D'autres retours de Sud Web 2016"
date: "2016-06-04T22:00:00"
comments: true
published: true
description: "Ce n'est peut-être pas si étonnant si autant de participants écrivent sur leur expérience de Sud Web !"
categories: 
- flux
slug: d-autres-retours-de-sud-web-2016
---
Ce n'est peut-être pas si étonnant si autant de participants écrivent sur leur expérience de Sud Web !

>Ce qui m’a surpris en premier, c’est que pendant ces deux jours, je n’ai pas vu beaucoup de code, et je n’ai presque pas vu de web, du moins en première lecture.  
[Pierre-Yves Desnoues, Sud Web 2016 @ Bordeaux](http://www.ekino.com/sud-web-2016-bordeaux/)

>Sud Web est définitivement une conférence particulière. Ne cherchez pas à la saisir, elle viendra tout simplement à vous, honnête et bienveillante. Sa bonhomie ne cessera de vous étonner. Après cinq ans d’existence, elle reste surprenante, mouvante, en témoigne le thème de cette édition 2016 : Partageons nos super-pouvoirs.  
[Baptiste Adrien, Sud Web 2016 Bordeaux : un excellent millésime](http://jolicode.com/blog/sud-web-2016-bordeaux-un-excellent-millesime)

>Pour moi, le mot qui est le plus revenu cette année est celui de la valeur  
[Maxime Warnier, SudWeb 2016 : la valeur du travail et des chocolatines](http://maxlab.fr/2016/05/sudweb-2016-valeur-travail-chocolatines/)

>On m’avait vendu Sud Web comme la conférence super humaine, un peu moins technique que certaines autres, plus centrée sur l’humain. Et je ne peux que confirmer.  
[Nathalie Pauchet, Mon premier Sud Web, retour d’expérience](http://lunatopia.fr/blog/premier-sud-web-retour-experience)

>Et puis les présentations ont démarrés et ce sont enchaînés et je me suis rappelé. Ce n’est pas tant que les participants et les orateurs sont des personnes exceptionnelles et bienveillantes. Elles le sont bien sur. C’est surtout qu’elles sont plus de 180…  
[Brice, SudWeb : La conférence qui fait du bien](http://blog.pelmel.org/2016/06/01/sudweb-la-conference-qui-fait-du-bien/)

>Un weekend créatif avec de vrais morceaux de bienveillances et partages à l’intérieur. Ça a le mérite d’être reboostant & de donner le smille.  
[Laurie, Bienveillance & partage : bienvenue à Sud Web](http://www.nomaditude.fr/bienveillance-partage-bienvenue-a-sud-web/)

>As Pauline's character grew up from her fifth birthday to being an accomplished professional looking for the meaning of her life, the talks addressed these issues with an incredible diversity.  
[Basile Simon, Sud Web 2016](http://blog.basilesimon.fr/2016/05/30/sudweb/)

>Sud Web est ce que les gens y apportent. On y apprend beaucoup, on échange beaucoup et on rencontre plein de gens passionnés et passionnants.  
[Olivier Keul, Sud Web 2016 – Bordeaux](http://blog.clever-age.com/fr/2016/05/30/sud-web-2016-bordeaux/)