---
layout: post
title: "AMP et les liens morts"
date: "2016-08-19T17:00:00"
comments: true
published: true
description: "AMP, ce système mis en place par Google pour améliorer les performances des sites web -- principalement de presse, permettant au passage aux éditeurs de mieux placer des publicités --, pose un vrai problème de pérennité."
categories: 
- flux
---
>[...] this is what will happen to most AMP URLs: AMP URLs will break.  
[Anselm Hanneman, How AMP supercharges link rot](https://helloanselm.com/2016/amp-supercharges-link-rot/)

<abbr title="Accelerated Mobile Pages Project">AMP</abbr>, ce système mis en place par Google pour améliorer les performances des sites web -- principalement de presse, permettant au passage aux éditeurs de mieux placer des publicités --, pose un vrai problème de pérennité.