---
layout: post
title: "Coût de la qualité et stratégie"
date: "2016-05-10T17:00:00"
comments: true
published: true
description: "La question de la qualité web, abordée avec une certaine dose d'humour."
categories: 
- flux
---
>Combien de qualité êtes-vous prêts à payer, ou, plutôt, à quel moment la qualité a un [retour sur investissement] maximal dans votre projet web ?  
[Fuuuuuuuuuu, Vous n’avez pas assez d’argent pour de la qualité.](http://fuuuccckkk.tumblr.com/post/141920460169/vous-navez-pas-assez-dargent-pour-de-la-qualit%C3%A9)

La question de la qualité web, abordée avec une certaine dose d'humour. Il faudrait probablement ajouter un peu de contraste, puisque la qualité web n'est pas toujours une question de coûts, mais aussi de méthode et de culture.