---
layout: post
title: "Gérer un projet web"
date: "2016-04-06T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Combien peut coûter un site web ? Combien de temps cela peut-il prendre de le faire réaliser ? Passer par une agence , un freelance, oui, mais comment trouver le bon prestataire ? Que faut-il préparer en amont avant de le contacter, le rencontrer ? Un cahier des charges, ok, mais qu’est ce qu’on y met ? Un contrat, un avenant, pour quoi faire ? Et puis après, l’étape suivante c’est quoi ?  
[Stéphanie Walter, Bien préparer son projet numérique](https://blog.stephaniewalter.fr/bien-preparer-projet-numerique-bizz-and-buzz-2016/)

[Stéphanie Walter](https://blog.stephaniewalter.fr/) met en ligne le support de la présentation qu'elle a réalisé avec [Damien Senger](https://twitter.com/iamhiwelo) autour de l'approche d'un projet "numérique" (mais il s'agit essentiellement de web) : c'est clair, concis, avec des rappels justes (la question des budgets pourrait être débattue).