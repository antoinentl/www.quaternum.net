---
layout: post
title: "Un framework pour le livre numérique"
date: "2016-06-30T22:00:00"
comments: true
published: true
description: "Jiminy et Quentin ont créé, il y a quelques mois déjà, un framework pour le livre numérique, l'initiative est suffisamment rare pour être signalée !"
categories: 
- flux
---
>An eBook framework (CSS + template) which mantra is to “find simple solutions to complex issues.”  
[FriendsOfEpub, Blitz](https://github.com/FriendsOfEpub/Blitz)

[Jiminy](http://jiminy.chapalpanoz.com/) et Quentin ont créé, il y a quelques mois déjà, un *framework* pour le livre numérique, l'initiative est suffisamment rare pour être signalée ! Je n'ai pas testé, mais les objectifs sont assez clairs :

>Blitz was designed to deal with the significant obstacles a newcomer or even an experienced producer might encounter. Its major goals are:  
> 
>1. to be simple and robust enough;
2. to offer a sensible default;
3. to manage backwards compatibility (ePub 2.0.1 + mobi 7);
4. to provide useful tools (LESS/SASS);
5. to get around reading modes (night, sepia, etc.);
6. to not disable user settings.