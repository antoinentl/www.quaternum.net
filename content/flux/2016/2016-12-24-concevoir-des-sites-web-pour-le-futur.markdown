---
layout: post
title: "Concevoir des sites web pour le futur"
date: "2016-12-24T12:00:00"
comments: true
published: true
description: "Boris Schapira parle de résilience : comment concevoir un site web qui vieillira bien, qui sera modulable et reprenable ?"
categories: 
- flux
tags:
- design
---
>Vous l’aurez compris : pour Noël, je voudrais que nous réalisions tous que faire un site Web aujourd’hui, c’est avant tout le réaliser pour demain, pour la personne qui s’en occupera après nous et dans l’optique que l’ensemble dure plusieurs années et soit aisément maintenu.  
[Boris Schapira, Un Web au Futur](https://borisschapira.com/2016/12/le-web-au-futur/)

À l'occasion de [nowwwel](https://www.twitter.com/search?f=tweets&vertical=default&q=%23nowwwel), Boris Schapira parle de résilience : comment concevoir un site web qui vieillira bien, qui sera modulable et *reprenable* ?