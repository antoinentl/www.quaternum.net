---
layout: post
title: "La sémantique"
date: "2016-06-27T17:00:00"
comments: true
published: true
description: "Gaël revient sur les rôles des langages HTML et CSS, et remet au centre du débat la question, celle de la sémantique."
categories: 
- flux
---
>CSS ne devrait pas être utilisé pour altérer lʼimportance sémantique d’un contenu. Et par extension, utiliser les sélecteurs dʼéléments ou dʼattributs semble une bonne idée. Je reformule au cas où : séparer le fond de la forme est une bonne idée, mais pas séparer la forme du fond. La forme dépend du fond, là où le fond ne dépend pas de la forme.  
[Gaël Poupard, Le sens de la sémantique](http://www.ffoodd.fr/le-sens-de-la-semantique/)

Gaël revient sur les rôles des langages HTML et CSS, et remet au centre [du débat](http://putaindecode.io/fr/articles/css/stop-css/) **la** question, celle de la sémantique.