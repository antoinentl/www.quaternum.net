---
layout: post
title: "Viser l'excellence"
date: "2016-09-17T12:00:00"
comments: true
published: true
description: "Frank Taillandier publie une traduction de 10 ways to make a product great de Ben Balter, ce dernier propose 10 points très clairs pour rendre un produit excellent, avec à chaque fois des exemples issus de son expérience de GitHub Pages -- le service d'hébergement de sites web de GitHub."
categories: 
- flux
slug: viser-l-excellence
---
>Si vous n’avez pas honte de la première version de votre produit, c’est que vous l’avez lancé trop tard. Pas besoin que ce soit parfait ou complet. Ça ne devrait pas l’être pour tout dire. Publiez tôt, publiez souvent.  
[Ben Balter, 10 façons de rendre un produit excellent](http://frank.taillandier.me/2016/08/30/dix-facons-de-rendre-un-produit-excellent/)

[Frank Taillandier](http://frank.taillandier.me/) publie une traduction de [10 ways to make a product great](http://ben.balter.com/2016/08/22/ten-ways-to-make-a-product-great/) de Ben Balter, ce dernier propose 10 points très clairs pour *rendre un produit excellent*, avec à chaque fois des exemples issus de son expérience de [GitHub Pages](https://pages.github.com/) -- le service d'hébergement de sites web de GitHub.