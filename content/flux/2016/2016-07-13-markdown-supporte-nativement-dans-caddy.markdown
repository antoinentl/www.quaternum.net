---
layout: post
title: "Markdown supporté nativement dans Caddy"
date: "2016-07-13T17:00:00"
comments: true
published: true
description: "Caddy est un serveur web HTTP/2 écrit en Go, et il supporte nativement le Markdown !"
categories: 
- flux
---
>markdown serves Markdown files as HTML pages on demand, but it can also generate a static site from Markdown so it doesn't have to render on-the-fly. You can specify whole custom templates or just the CSS and JavaScript files to be used on the pages to give them a custom look and behavior.  
[Caddy, markdown](https://caddyserver.com/docs/markdown)

Caddy est un serveur web HTTP/2 écrit en Go, et il supporte nativement le Markdown !

Via [Bruno Bord](https://twitter.com/brunobord/status/752995197890551808).