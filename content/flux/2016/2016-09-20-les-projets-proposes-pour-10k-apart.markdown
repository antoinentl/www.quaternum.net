---
layout: post
title: "Les projets proposés pour 10K Apart"
date: "2016-09-20T19:00:00"
comments: true
published: true
description: "Un certain nombre de projets du concours 10K Apart sont déjà visibles en ligne, certains sont très inspirants !"
categories: 
- flux
---
>Inspiring the Web with just 10k  
[10K Apart, Project Gallery](https://a-k-apart.com/gallery)

Un certain nombre de projets du concours [10K Apart](https://a-k-apart.com/) sont déjà visibles en ligne, certains sont très inspirants !