---
layout: post
title: "Intégrer facilement du Markdown dans une page HTML"
date: "2016-09-12T12:00:00"
comments: true
published: true
description: "Une façon simple d'intégrer des données structurées en Markdown dans un fichier HTML, pour obtenir une page HTML -- cela nécessite du JavaScript, mais sans avoir de compilation côté serveur. C'est très intéressant, même si les usages semblent limités en pratique. Et le problème principal vient de l'utilisation de JavaScript."
categories: 
- flux
---
>Strapdown.js makes it embarrassingly simple to create elegant Markdown documents. No server-side compilation required. Use it to quickly document your projects, create tutorials, home pages, etc.  
[Artur Adib, Strapdown.js](http://strapdownjs.com/)

Une façon simple d'intégrer des données structurées en Markdown dans un fichier HTML, pour obtenir une page HTML -- cela nécessite du JavaScript, mais sans avoir de compilation côté serveur. C'est très intéressant, même si les usages semblent limités en pratique. Et le problème principal vient de l'utilisation de JavaScript.