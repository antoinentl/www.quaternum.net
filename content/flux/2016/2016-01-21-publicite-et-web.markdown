---
layout: post
title: "Publicité et web"
date: "2016-01-21T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Ça va faire près de vingt ans que j’ai accès au Web. Et quasiment autant de temps que j’écris, partage, et publie gratuitement du contenu en ligne. Je n’ai pas vraiment de modèle économique dans tout ça. Je le fais parce que ça me plaît. Je le fais parce que j’apprends du retour des autres. Je m’enrichis, mais pas financièrement. Et ça vaut largement la quarantaine d’euros annuelle en hébergement que me coûte ce présent blog.  
[HTeuMeuLeu, La publicité n’est pas le modèle économique du Web](http://www.hteumeuleu.fr/la-publicite-n-est-pas-le-modele-economique-du-web/)

Le grand débat du modèle économique du *web* revient régulièrement, et ce d'autant plus depuis quelques mois. HTeuMeuLeu offre une vision très intéressante.

>Je suis pratiquement sûr que si on enlevait toute la publicité du Web, il resterait plein de sites très bien.