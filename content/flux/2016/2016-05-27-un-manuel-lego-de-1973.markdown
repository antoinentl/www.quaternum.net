---
layout: post
title: "Un manuel Lego de 1973"
date: "2016-05-27T23:00:00"
comments: true
published: true
description: "À l'occasion de son intervention lors de Sud Web (Les valeurs de LEGO™), Thomas Gasc a scanné, mis en page et déposé en ligne un manuel Lego™ de 1973. On y apprend beaucoup de choses, et ce petit livre pourrait presque être des conseils de design !"
categories: 
- flux
---
>Vous pensez certainement que ça ne ressemble pas beaucoup à une ambulance. C'est votre bon sens qui vous le dit. Mais le bon sens est une invention des adultes. Ça n'existe pas dans le monde des enfants. Les enfants n'ont pas d'interdits, ils font les choses librement. Ils finissent toujours pas apprendre à être réalistes. Ne leur demandez pas de l'être trop tôt.  
[Un petit livre à l'usage des parents, pour mieux comprendre leurs enfants à travers les jeux.](http://lego.methylbro.fr/)

À l'occasion de son intervention lors de [Sud Web](https://sudweb.fr/2016/) (Les valeurs de LEGO™), [Thomas Gasc](http://methylbro.fr/) a scanné, mis en page et déposé en ligne un manuel Lego™ de 1973. On y apprend beaucoup de choses, et ce petit livre pourrait presque être des conseils de design !

>C'est pourquoi il est difficile de séparer le jeu des choses sérieuses. Les distractions des enfants ne devraient jamais être évaluées froidement. Le jeu est avant tout une libre expression. Sous sa meilleure forme, il est profondément sérieux. Er utile. Les enfants commencent à jouer quand ils sont âgés de quelques mois et ne s'arrêtent jamais vraiment. Le jeu grandit avec eux.