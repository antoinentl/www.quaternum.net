---
layout: post
title: "Simplifier le développement web"
date: "2016-09-30T14:00:00"
comments: true
published: true
description: "Passer d'un backend lourd à un worklow basé sur des fichiers statiques standards : un sujet passionnant que la modification des processus de travail des développeurs web, abordé avec succès par Frank Taillandier et Bertrand Keller lors de Paris Web 2016."
categories: 
- flux
---
>Avec [Bertrand](http://bertrandkeller.info/), nous voulions surtout partager nos retours d’expérience et le plaisir retrouvé que nous avions à apprendre et à concevoir itérativement des sites de qualité où le contenu est roi. Donner envie aux développeurs front de reprendre la main sur leur code tout en proposant une expérience optimale aux utilisateurs.  
[Frank Taillandier, Ne passons pas à côté des choses simples](http://frank.taillandier.me/2016/10/02/ne-passons-pas-a-cote-des-choses-simples/)

Passer d'un *backend* lourd à un worklow basé sur des fichiers *statiques* standards : un sujet passionnant que la modification des processus de travail des développeurs web, abordé avec succès par Frank Taillandier et Bertrand Keller lors de [Paris Web 2016](http://www.paris-web.fr/).

Le support de leur présentation est également [disponible en ligne](http://frank.taillandier.me/presentations/ne-passons-pas-a-cote-des-choses-simples/).