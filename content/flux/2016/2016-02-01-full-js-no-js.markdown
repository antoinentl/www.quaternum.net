---
layout: post
title: "Full-js + no-js ?"
date: "2016-02-01T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Lorsqu’une action est réalisée par l’utilisateur :
- soit elle est interceptée par JavaScript côté client, l’action est alors traitée par la partie Redux de l’application pour mettre à jour les données, puis la vue est rendue grâce à React ;  
- soit elle est envoyée sous forme d’URL au serveur, l’action est alors traitée par la partie Redux de l’application pour mettre à jour les données, puis la vue est rendue grâce à React côté serveur avant d’être envoyée en pur HTML au client.  
[Samuel Bouchet, « Full-js » et « no-js » avec React + Redux](http://blog.clever-age.com/fr/2016/02/01/full-js-et-no-js-avecreactredux/)

Samuel Bouchet explique de façon très pédagogique l'association de deux composants pour la construction d'une application web pouvant répondre à la fois à une volonté de réactivité ou à une contrainte contextuelle -- sans JavaScript.