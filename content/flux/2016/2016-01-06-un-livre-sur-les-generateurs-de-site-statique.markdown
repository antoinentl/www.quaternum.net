---
layout: post
title: "Un livre sur les générateurs de site statique"
date: "2016-01-06T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Static websites today are just like vinyl LPs: they’re coming back.  
[Brian Rinaldi, Static site generators: the latest tools for building static websites](http://conferences.oreilly.com/fluent/javascript-html-us/public/content/static-site-generators?download=true)

Un livre, gratuit et en anglais, sur les générateurs de site statique, idéal pour celles et ceux qui veulent découvrir Jekyll et autres *static site generators* -- j'en ai parlé [ici](https://www.quaternum.net/2012/12/23/pourquoi-quitter-wordpress/) et [là](https://www.quaternum.net/2014/12/29/un-point-sur-jekyll/).