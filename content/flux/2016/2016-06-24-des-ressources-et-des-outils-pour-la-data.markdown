---
layout: post
title: "Des ressources et des outils pour la data"
date: "2016-06-24T17:00:00"
comments: true
published: true
description: "Des ressources et des outils utiles pour manipuler et mettre en forme de la data."
categories: 
- flux
---
>Through my research on the web, I stumble upon a lot of resources like tools, websites and tutorials.  
[Damiano Bacci, The definitive data journalism resources list](https://damianobacci.github.io/blog/definitive-data-journalism-resource-list)

Des ressources et des outils utiles pour manipuler et mettre en forme de la *data*.