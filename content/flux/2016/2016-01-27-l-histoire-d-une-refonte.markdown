---
layout: post
title: "L'histoire d'une refonte"
date: "2016-01-27T22:00:00"
comments: true
published: true
categories: 
- flux
slug: l-histoire-d-une-refonte
---
>Il y a 9 mois, toute l’équipe de Paris Numérique s’est lancée dans la refonte de Paris.fr. Flash-back en coulisses.  
[Comment Paris Numérique a refait Paris.fr.](http://numerique.paris.fr/2015/06/comment-paris-numerique-a-refait-paris-fr/)

Le *nouveau* site de la Ville de Paris -- sorti en juin 2015 -- est remarquable pour de nombreuses raisons -- accès, arborescence, structuration, ergonomie, architecture, accessibilité, etc. --, l'équipe de Paris Numérique raconte un peu comment ils ont procédé. Un beau projet avec une équipe intégrée et non une prestation extérieure.