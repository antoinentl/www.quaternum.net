---
layout: post
title: "Flux Atom, mode d'emploi"
date: "2016-08-20T17:00:00"
comments: true
published: true
description: "Karl Dubost proposait, en 2013 et sur Open Web, un mode d'emploi complet pour constuire convenablement un flux Atom, et c'est assez simple à suivre !"
categories: 
- flux
slug: flux-atom-mode-d-emploi
---
>Atom (RFC 4287) est l’un des formats XML qui permet d’avertir et de distribuer les mises à jour de votre site Web. Il a été conçu afin de réduire les ambiguïtés du format RSS 2.0. Après de longues batailles, les acteurs professionnels des outils traitant les flux RSS ont atteint un consensus à l’IETF en décembre 2005.  
[Karl Dubost, Comment construire un flux Atom ?](http://openweb.eu.org/articles/comment-construire-un-flux-atom)

Karl Dubost proposait, en 2013 et sur Open Web, un mode d'emploi complet pour constuire convenablement un flux Atom, et c'est assez simple à suivre !

Via [Nicolas Hoffmann](https://twitter.com/Nico3333fr/status/765271398851960832).