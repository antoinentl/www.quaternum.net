---
layout: post
title: "Sécuriser (en partie) son site web"
date: "2016-09-14T12:00:00"
comments: true
published: true
description: "Nicolas Hoffmann explique -- patiemment -- comment améliorer la sécurité de sites web que l'on administre, au moment où Mozilla met un outil de vérification à disposition : https://observatory.mozilla.org/."
categories: 
- flux
---
>Quoi qu’il en soit, cet outil Mozilla Observatory arrive à point nommé, il met en exergue certains efforts à fournir pour proposer des sites toujours plus sûrs, et s’inscrit parfaitement dans une démarche de qualité et d’amélioration continue. Bien entendu, la sécurité de vos sites ne s’arrêtera pas là.  
[Nicolas Hoffmann, Obtenir une bonne note sur Mozilla Observatory : HTTPS/CSP/SRI/CORS/HSTS/HPKP/etc.](https://www.nicolas-hoffmann.net/source/1697-Obtenir-une-bonne-note-sur-Mozilla-Observatory-HTTPS-CSP-SRI-CORS-HSTS-HPKP-etc.html)

Nicolas Hoffmann explique -- patiemment -- comment améliorer la sécurité de sites web que l'on administre, au moment où Mozilla met un outil de vérification à disposition : [https://observatory.mozilla.org/](https://observatory.mozilla.org/).