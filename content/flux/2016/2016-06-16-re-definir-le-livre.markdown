---
layout: post
title: "Re-définir le livre"
date: "2016-06-16T17:00:00"
comments: true
published: true
description: "Jean Kaplansky réagit à un article de Jiminy Panoz : la question ou le débat concernant l'articulation entre l'EPUB et le web n'est pas tant une question d'interface ou de technique, que de définition de ce qu'est un livre et un document, numérique ou non numérique."
categories: 
- flux
---
>I’m not convinced that it should be all about “books in browser-based reading systems” or “books on the web.” I think we may have evolved past our definition of book. We need a new noun to describe long-form content on the web that is packaged an connected. Publishers need to help us figure out exactly what this is in specific contexts.  
[Jean Kaplansky, Absolute truth](https://medium.com/@jeankaplansky/absolute-truth-c28b7b652242#.gtfto4s2v)

Jean Kaplansky réagit à [un article de Jiminy Panoz](https://medium.com/@jiminypan/a-css-api-for-ebooks-df469a347c0c#.c9n9qsq6b) : la question ou le débat concernant l'articulation entre l'EPUB et le web n'est pas tant une question d'interface ou de technique, que de définition de ce qu'est un livre et un document, numérique ou non numérique.