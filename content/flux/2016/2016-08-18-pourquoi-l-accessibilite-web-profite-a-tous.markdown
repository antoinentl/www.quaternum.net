---
layout: post
title: "Pourquoi l'accessibilité web profite à tous"
date: "2016-08-18T17:00:00"
comments: true
published: true
description: "Quatre critères d'accessibilité présentés et expliqués, dans le but de montrer combien l'accessibilité web est bénéfique pour tous le monde, et pas uniquement pour les personnes en situation de handicap."
categories: 
- flux
slug: pourquoi-l-accessibilite-web-profite-a-tous
---
>Accessibility is something you have to think about with every new bit of content added to your site. It can’t be an afterthought. However no site is perfect so you as long as you a making incremental improvements and reacting to feedback you are doing a good job.  
[Peter Brumby, Why web accessibility benefits us all](https://www.pbrumby.com/2016/08/17/why-web-accessibility-benefits-us-all/)

Quatre critères d'accessibilité présentés et expliqués, dans le but de montrer combien l'accessibilité web est bénéfique pour tous le monde, et pas uniquement pour les personnes en situation de handicap.