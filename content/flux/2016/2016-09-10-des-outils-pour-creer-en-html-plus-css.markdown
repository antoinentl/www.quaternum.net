---
layout: post
title: "Des outils pour créer en HTML+CSS"
date: "2016-09-10T12:00:00"
comments: true
published: true
description: "Une petite liste d'erreurs CSS souvent commises, avec des solutions pour les éviter facilement. Des erreurs techniques qui proviennent bien souvent de problèmes de conception !"
categories: 
- flux
slug: des-outils-pour-creer-en-html-plus-css
---
>Here is the complete list of tools I use when I design in the browser using css and html.  
[Adam Morse, Tools I Use for Designing in the Browser](http://mrmrs.io/writing/2015/09/25/tools-for-designing-in-the-browser/)

Adam Morse livre sa liste d'outils pour *designer* des sites web, une dizaine au total, sans oublier du papier et un crayon.