---
layout: post
title: "Les principes de LCP Readium"
date: "2016-06-17T17:00:00"
comments: true
published: true
description: "L'EDRLab publie les principes de LCP Readium, la mesure technique de protection pour les livres numériques au format EPUB, qui devrait remplacer la DRM Adobe."
categories: 
- flux
---
>Readium LCP is therefore an effort to find a new balance between the needs of the rights owners and those of end users.  
[...]  
LCP is really more than a password based protection scheme.  
[EDRLab, Readium LCP Principles](https://edrlab.org/edrlab/readium-lcp-principles/)

L'<abbr title="European Digital Reading Lab">EDRLab</abbr> publie les principes de <abbr title="Licensed Content Protection">LCP</abbr> Readium, la mesure technique de protection pour les livres numériques au format EPUB qui devrait remplacer la DRM Adobe. Pourquoi remplacer la DRM Adobe ? Essentiellement pour deux raisons : ne plus être tributaire d'Adobe -- qui, en plus, n'est pas un acteur de la chaîne du livre, même s'il l'est devenu, de fait --, et permettre plus facilement certains usages -- comme une meilleure gestion du prêt numérique.

>I will summarize the Readium LCP main objectives as a set of bullet points:
> 
>- Effective enforcement of classical usage restrictions
- Support for different workflows, especially retail, library lending, subscription
- Ease of use for the end user
- Offline access to the publications always possible
- Perfect system interoperability
- No interference with content accessibility (a11y)
- Unlimited access (in time) to the publications where allowed by rights holders
- Friendly sharing possible (but anti-oversharing enforcement)
- Ease of implementation via the release of open-source software
- Independence from individual technology vendors
- Management by a non-profit organization

LCP Readium se veut un système distribué, avec les principes suivants (grossièrement résumés) :

1. le contenu EPUB est chiffré ;
2. une licence est attribué à un utilisateur (cette licence peut notamment comporter des informations de durée de prêt) ;
3. pour déchiffrer le contenu d'un EPUB, l'utilisateur doit activer sa licence via une application de lecture ;
4. cette application de lecture est connecté à un serveur LCP Readium, soit celui du revendeur/distributeur, soit celui d'EDRLab.

Quelques questions subsistent :

- est-ce que EDRLab va proposer un serveur LCP Readium, et dans quel cadre cela est-il possible (notamment en terme d'usage des données) ?
- est-ce que la solution TEA Care (l'implémentation LCP par The Ebook Alternative) est *réellement* interopérable avec LCP Readium ([c'est annoncé comme étant le cas](https://www.tea-ebook.com/wp-content/uploads/2016/04/TEA_CARE_WP_MARS-2016.pdf)) ?

Votre centre d'intérêt n'est pas le livre numérique ? Suivez tout de même de près LCP Readium, car c'est une tentative de vendre des contenus numériques avec des mesures techniques de protection !