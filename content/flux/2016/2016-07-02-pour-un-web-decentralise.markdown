---
layout: post
title: "Pour un web décentralisé"
date: "2016-07-02T17:00:00"
comments: true
published: true
description: "Cory Doctorow est doué pour présenter les choses de façon claire, pertinente et drôle, c'est le cas ici lorsqu'il parle de web décentralisé. Sans être accusateur, il parvient à éveiller la conscience des utilisateurs d'internet, et également celle de ses fondateurs."
categories: 
- flux
---
>Earlier this month, I gave the afternoon keynote at the Internet Archive's Decentralized Web Summit, speaking about how the people who are building a new kind of decentralized web can guard against their own future moments of weakness and prevent themselves from rationalizing away the kinds of compromises that led to the centralization of today's web.  
[Cory Doctorow, Guarding the Decentralized Web from its founders' human frailty](https://boingboing.net/2016/06/20/video-guarding-the-decentrali.html)

Cory Doctorow est doué pour présenter les choses de façon claire, pertinente et drôle, c'est le cas ici lorsqu'il parle de web décentralisé. Sans être accusateur, il parvient à éveiller la conscience des utilisateurs d'internet, et également celle de ses fondateurs.