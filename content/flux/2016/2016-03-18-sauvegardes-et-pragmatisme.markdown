---
layout: post
title: "Sauvegardes et pragmatisme"
date: "2016-03-18T13:00:00"
comments: true
published: true
categories: 
- flux
---
>At the end I asked the room, “who is happy with their solution?”. Only one person put their hand up. She had tried every nerdy backup tool imaginable, and abandoned them all.  
Instead, she keeps files on her computer and her relative’s computer. If she’s making a document she cares about, she transfers it by USB stick between the computers, to make sure there are multiple copies in different places. And that’s it.  
Unsophisticated? Not at all. Way easier than backing up paper. Understandable by anyone. Full control. Cheap.  
[Francis Irving, Sync/Backup workshop at Redecentralize Conference](https://www.flourish.org/2015/11/syncbackup-workshop-at-redecentralize-conference/)

J’ai moi aussi testé nombre de services de stockage et de synchronisation, la meilleure solution est la plus simple, et la plus résiliente.

[Via David](https://larlet.fr/david/stream/2015/11/13/).