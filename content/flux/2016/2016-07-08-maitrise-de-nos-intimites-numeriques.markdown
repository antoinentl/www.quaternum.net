---
layout: post
title: "Maîtrise de nos intimités numériques"
date: "2016-07-08T17:00:00"
comments: true
published: true
description: "Ce n'est pas parce qu'on ne maîtrise pas tout qu'il ne faut pas conserver une intimité numérique, et Clochix explique très bien cela !"
categories: 
- flux
---
>Je suis de plus en plus exaspéré par le discours de certains technophiles experts (réels ou auto-proclamés, je ne suis pas apte à en juger) débinant la majorité des solutions de sécurité parce qu’elles ne sont pas assez fortes, parce qu’elles ne sont pas invulnérables. Ce discours est à mon sens contre-productif, car il présente la protection de son intimité comme un but impossible à atteindre, ou seulement au prix d’efforts dont le coût semble à la plupart d’entre nous démesuré par rapport au risque dont ils nous protègent. Il incite finalement à ne pas se protéger puisque toute protection serait vaine.  
[Clochix, L’intimité n’est pas chose trop compliquée réservée à l’"Élite"](https://clochix.net/marges/160612-securite)

Ce n'est pas parce qu'on ne maîtrise pas tout qu'il ne faut pas conserver une intimité numérique, et Clochix explique très bien cela !