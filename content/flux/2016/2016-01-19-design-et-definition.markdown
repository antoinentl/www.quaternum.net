---
layout: post
title: "Design et définition"
date: "2016-01-19T22:00:00"
comments: true
published: true
categories: 
- flux
---
>Jamais, pourtant, n’ai-je entendu parler de linguistique dans cette quantité monumentale d’articles sur le design. Et ce même dans les articles traitant par exemple des messages système auxquels l’utilisateur a souvent fort à faire…  
Le fait est que je crois sincèrement que nous devrions intégrer la linguistique dans nos approches de conception. Et je vais tenter de vous expliquer pourquoi.  
[Jiminy Panoz, La linguistique comme outil de design](http://jiminy.chapalpanoz.com/linguistique-design/)