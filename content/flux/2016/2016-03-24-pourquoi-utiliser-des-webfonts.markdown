---
layout: post
title: "Pourquoi (ne pas) utiliser de webfonts"
date: "2016-03-24T13:00:00"
comments: true
published: true
categories: 
- flux
slug: pourquoi-utiliser-des-webfonts
---
>System fonts can be beautiful.  
Webfonts are not a requirement for great typography.  
[Adam Morse, Webfonts](http://mrmrs.io/writing/2016/03/17/webfonts/)

Adam Morse présente ses arguments pour modérer l'usage des *web fonts*, j'ajoute quelques éléments :

- les propos d'Anselm Hannemann : ["A lot of projects don’t care about using a proper fallback font that matches the web font — and these days, with content and privacy blockers available for every device, there are a lot of people not seeing any web fonts at all."](https://wdrl.info/archive/129)
- concernant les techniques de chargement de fontes, notamment par [Donny Truong sur Professionnal Web Typography](https://prowebtype.com/delivering-web-fonts/#observer) ou par [Scott de Filament Group](https://www.filamentgroup.com/lab/font-events.html) ;
- le contexte pour faire le meilleur choix.