---
layout: post
title: "Des alternatives pour les contenus non-textuels sur Twitter"
date: "2016-04-21T07:00:00"
comments: true
published: true
categories: 
- flux
slug: alt-twitter
---
>Today, we’re pleased to tell you we’re extending our platform products, both the REST API and Twitter Cards, so that our customers, publishers and third-party apps will be able to publish images to Twitter that are accessible to people who are visually impaired.  
[Todd Kloots, Alt text support for Twitter Cards and the REST API](https://blog.twitter.com/2016/alt-text-support-for-twitter-cards-and-the-rest-api)

2016, Twitter implémente des solutions pour rendre les contenus non-textuels en partie accessibles, via l'utilisation de la balise `alt` et les *Twitter Cards*, bonne nouvelle !