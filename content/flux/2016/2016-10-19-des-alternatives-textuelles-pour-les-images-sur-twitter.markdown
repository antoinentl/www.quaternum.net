---
layout: post
title: "Des alternatives textuelles pour les images sur Twitter"
date: "2016-10-19T17:00:00"
comments: true
published: true
description: "Twitter propose désormais aux utilisateurs de décrire les images partagées : 420 caractères pour compléter l'attribut alt de la balise img, ou pour contourner la limitation des 140 caractères."
categories: 
- flux
tags:
- accessibilité
---
>6 nouveaux services et une annonce majeure pour attaquer la 3e – et dernière – année de notre (modeste) plan de libération du monde… De moins en moins modeste : déjà trente services à ce jour !  
[Twitter, Rendre les images accessibles aux utilisateurs malvoyants de Twitter](https://support.twitter.com/articles/20174697#)

Twitter propose désormais aux utilisateurs -- après [une activation](https://twitter.com/settings/accessibility) dans les paramètres -- de décrire les images partagées : 420 caractères pour compléter l'attribut `alt` de la balise `img`, ou pour contourner la limitation des 140 caractères.