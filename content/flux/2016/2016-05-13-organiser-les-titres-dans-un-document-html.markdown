---
layout: post
title: "Organiser les titres dans un document HTML"
date: "2016-05-13T14:00:00"
comments: true
published: true
description: "Comment organiser les niveaux de titres dans une page/document HTML tout en restant accessible ? Julie Grundy aborde cette question avec beaucoup de pédagogie, c'est clair et efficace !"
categories: 
- flux
---
>People often judge a website by how quickly they can get what they need and leave. Headings are a great way to do just that. They’re visually larger than the rest of the content, so they stand out. They provide a description of what’s in each section, so people can skip ahead or stop when they find the section they need.  
People who use screen readers also use headings to skim and scan the content on a long page.  
[...]
Headings also help people create a mental outline of the content on the page.  
[Julie Grundy, Creating bulletproof headings](http://simplyaccessible.com/article/bulletproof-headings)

Comment organiser les niveaux de titres dans une page/document HTML tout en restant accessible ? Julie Grundy aborde cette question avec beaucoup de pédagogie, c'est clair et efficace !

Les trois étapes :

1. Use real heading elements
2. Create a nested structure to organize the content
3. Give your users useful content

À noter que le troisième point est rarement abordé dans ce type d'article : la structure sémantique doit être accompagnée de contenus compréhensibles et clairs :

>Finally, headings need to be descriptive and useful. There’s no point making a lovely outline structure for our page if we’re going to use generic headings like “Learn more” or “Visit.” What are we going to learn about, and who are we being invited to visit? Let people see at a glance what you’re offering, instead of making them read every word.