---
layout: post
title: "La transparence et ses revers"
date: "2016-04-05T18:00:00"
comments: true
published: true
categories: 
- flux
---
>Plus un instant de nos vies ne peut rester nôtre et si cela complique bien sûr, et tant mieux, la tâche des malfaiteurs, des corrompus et des terroristes, cela facilite tout autant celle des polices dans un monde où les dictatures et autres régimes autoritaires sont autrement plus nombreux que les démocraties.  
[...]  
Il est réjouissant de voir des potentats éclaboussés, tant de turpitudes dénoncées et un nouveau coup porté aux paradis fiscaux mais, à terme, ce n’est pas aux plus faibles mais aux plus forts, pas à la liberté mais à la dictature, que profitera la disparition, l'impossibilité du secret.  
[Bernard Guetta, La transparence et ses revers](http://www.franceinter.fr/emission-geopolitique-la-transparence-et-ses-revers)

Voici la vision de Bernard Guetta, d'une justesse hallucinante. *Let's encrypt* (mais ça ne suffira peut-être pas).