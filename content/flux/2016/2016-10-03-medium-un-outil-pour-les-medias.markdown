---
layout: post
title: "Medium, un outil pour les médias ?"
date: "2016-10-03T17:00:00"
comments: true
published: true
description: "Analyse de Medium avec le regard d'InaGlobal, et pour résumer Medium ne semble pas dépasser l'intention, pour l'instant."
categories: 
- flux
---
>Au-delà d’être une plateforme où distribuer leurs contenus, Medium semble donc surtout pour l’instant être un laboratoire d’engagement et d’échange pour les médias.  
[Guillaume Galpin, Medium : quelles perspectives pour les médias ?](http://www.inaglobal.fr/numerique/article/medium-quelles-perspectives-pour-les-medias-9278)

Analyse de Medium avec le regard d'[InaGlobal](http://www.inaglobal.fr/), et pour résumer Medium ne semble pas dépasser l'intention, *pour l'instant*.