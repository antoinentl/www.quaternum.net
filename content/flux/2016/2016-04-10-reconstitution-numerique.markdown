---
layout: post
title: "Reconstitution numérique"
date: "2016-04-10T17:00:00"
comments: true
published: true
categories: 
- flux
---
>Le médium numérique n'a pas de matérialité sculpturale. Il permet seulement de réaliser une silouhette, avec les traits caractéristiques d'un caractère produit par la gravure et à l'échelle -- exclusive -- à laquelle il sera imprimé, en relief, à l'envers et en métal. Or, au lieu de retirer de la matière pour obtenir la forme voulue, le caractère numérique prend forme à l'écran par la modification de ses contours. Donc à proprement parler, une "reconstitution numérique" n'a simplement pas de sens.  
[John Downer, Call it what it is, Azimuts 43](http://www.esadse.fr/fr/post-diplome-master/171012-la-revue-azimuts)

Je ne suis pas tout à fait d'accord avec cette approche de John Downer -- et l'exemple de [Prototypo](https://www.prototypo.io/) lui prouverait d'ailleurs le contraire, puisque le dessin des caractères se fait par leur centre --, mais cet article est passionnant.

À lire dans le dernier numéro -- double -- d'Azimuts, la revue du *post-diplôme* de l'École supérieure d'art et design de Saint-Étienne.

![Photographies des pages intérieures de la revue Azimuts](/images/2016-04-azimuts.gif)