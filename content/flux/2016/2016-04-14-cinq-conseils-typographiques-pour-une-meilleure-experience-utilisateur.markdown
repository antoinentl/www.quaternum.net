---
layout: post
title: "Cinq conseils typographiques pour une meilleure expérience utilisateur"
date: "2016-04-14T14:00:00"
comments: true
published: true
categories: 
- flux
---
>The average person with a smartphone or laptop and a stable internet connection probably spends more time interacting with typography in a single day than with food, music, and family combined.  
[...]  
Here’s a list of 5 easy ways to improve the typographic quality and effectiveness of any website, email, or digital product design.  
[Studio Function, Typography tips for a better user experience](https://medium.com/studio-function/typography-tips-for-a-better-user-experience-30a1a48371e6#.833ofs98c)

Les cinq points abordés sont les suivants :

1. Be conscious of line and paragraph spacing
2. Avoid awkwardly long lines of text
3. Be authentic with hand lettering
4. Don’t use too many type styles
5. Use proper punctuation when it matters most

>Typography is an invisible craft that permeates almost every aspect of our waking lives. Good typography has a major role to play in the creation of successful, memorable user experiences.