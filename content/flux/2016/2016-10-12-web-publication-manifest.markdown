---
layout: post
title: "Web Publication Manifest"
date: "2016-10-12T17:00:00"
comments: true
published: true
description: "Depuis quelques jours Hadrien Gardeur expérimente différentes applications pour permettre la lecture d'EPUBs (le format du livre numérique), directement dans le navigateur. À tester, à observer de près, pour contribuer à ces projets."
categories: 
- flux
---
>EPUB exists because the web doesn't allow us to easily speak about collections of documents.  
[Hadrien Gardeur, Web Publication Manifest](https://github.com/HadrienGardeur/webpub-manifest)

Depuis quelques jours [Hadrien Gardeur](https://twitter.com/Hadrien/) expérimente différentes applications pour permettre la lecture d'EPUBs (le format du livre numérique), directement dans le navigateur. À tester, à observer de près, pour contribuer à ces projets qui *poussent* le livre numérique dans le web.

Voici un exemple de *Web App* : [https://hadriengardeur.github.io/webpub-manifest/examples/viewer/](https://hadriengardeur.github.io/webpub-manifest/examples/viewer/)