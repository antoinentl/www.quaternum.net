---
title: "L'édition est une économie de prototypes"
date: 2023-04-24
categorie:
- flux
tags:
- publication
description: "Cette dimension de prototypage éditorial est souvent oubliée, ou mise en avant comme une éloge du risque capitaliste, mais c'est surtout une dimension expérimentale et créatrice particulièrement réjouissante !"
---

> L'édition c'est une économie de prototype. Par définition un prototype il peut réussir et gagner les 24h du Mans ou ne jamais gagner la moindre course automobile. Donc il ne suffit pas sur le papier ou devant son écran d'ordinateur d'élaborer une stratégie ou une politique éditoriale, pour qu'elle se traduise immédiatement par des ventes à 10000, 50000, 100000 exemplaires. Jean-Yves Mollier {{< cite de_rocquigny_edition_2023 "28:20" >}}

Cette dimension de prototypage éditorial est souvent oubliée (et surtout récemment comme l'explique Jean-Yves Mollier avec la course aux bestsellers), ou mise en avant comme une éloge du risque capitaliste, mais c'est surtout une dimension expérimentale et créatrice inhérente à toute activité d'édition.
Cette conceptualisation de l'édition comme une économie de prototypes est aussi applicable à la _manière_ de faire des livres — c'est en tout cas une partie de [mon hypothèse](/phd).
