---
title: "Montrer les gabarits"
date: 2023-12-20
categorie:
- flux
tags:
- publication
description: "[Quire](https://quire.getty.edu/), la chaîne de publication multi-formats du Getty, dispose désormais d'un site web de démonstration très complet avec, notamment, la mention du gabarit (ou _template_) utilisé pour chaque page."
---

> A page with `layout: essay`, often used for standalone articles or chapters in a collected volume.  
> Quire, Essay, [https://quire.getty.edu/demo/pages/essay/](https://quire.getty.edu/demo/pages/essay/)

[Quire](https://quire.getty.edu/), la chaîne de publication multi-formats du Getty, dispose désormais d'un site web de démonstration très complet avec, notamment, la mention du gabarit (ou _template_) utilisé pour chaque page ainsi que le comportement éditorial en fonction des sources concernées.
Cette volonté de montrer la modélisation éditoriale est suffisamment rare pour être soulignée.
L'objectif principal est de faciliter la compréhension des différents types de pages, pour pouvoir utiliser, adapter ou modifier Quire.

Je note au passage que ces déclarations de gabarit ne sont pas automatisées — les informations sont indiquées dans l'entête de chaque source au format Markdown —, ce qui aurait pourtant été un mécanisme intéressant (à réaliser avec 11ty, le générateur de site statique désormais utilisé dans Quire).

