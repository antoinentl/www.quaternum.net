---
title: "Le texte et le code"
date: 2023-12-30
categorie:
- flux
tags:
- outils
description: "Alex Schroeder rassemble ici des sites web qui sont autant des initiatives éditoriales personnelles (écrire et diffuser des textes) que des expérimentations techniques pour permettre ces actes éditoriaux."
---

> Sometimes I hear of “opinionated” software. That is, software which has a strong vision of how to do a thing. If you like it, use it. If you don’t, then don’t. But what happens if the software turns out to be something the author of a site wrote for themselves? It’s more than opinionated. It’s personal.  
> This website presents a number of sites and the tools they use to create and update them.  
> Alex Schroeder, The text and the code go hand in hand, [https://transjovian.org/view/web-sites/index](https://transjovian.org/view/web-sites/index)

Alex Schroeder rassemble ici des sites web qui sont autant des initiatives éditoriales personnelles (écrire et diffuser des textes) que des expérimentations techniques pour permettre ces actes éditoriaux.
Des sites ou des blogs écrits à la main, autant les textes que le code permettant leur fonctionnement.
Des micro-[fabriques](/2023/06/02/fabrique-concept/), en quelque sorte.

> I’d love to show the world the unpolished, “just good enough with a number of bugs I know of” solutions.

La diversité des solutions, relativement peu nombreuses, démontre un besoin de maîtriser son outil de production (de textes), d'adapter cet outil quand c'est nécessaire, et de s'amuser (en apprenant).

