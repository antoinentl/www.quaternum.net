---
title: "Vers un hiver numérique réjouissant"
date: 2023-01-12
categorie:
- flux
tags:
- recherche
description: "En novembre 2022 Devine Lu Linvega a présenté beaucoup de choses passionnantes lors de Handmade Seattle, notamment la démarche de son studio Hundred Rabbits (avec Rekka Bellum), le besoin de ne plus utiliser des systèmes complexes ou qui nécessitent une connexion internet quasi-permanente, l'envie d'opérer une forme de décroissance numérique et de tester de nombreux langages de programmation peu connus voir ésotériques, et enfin en creux la question du permacomputing."
---
> Our philosophy is that to make fast software, you need slow computers, and we've tried to espouse this as much as we could.  
> Hundred Rabbits, Weathering Software Winter, [https://100r.co/site/weathering_software_winter.html](https://100r.co/site/weathering_software_winter.html)

En novembre 2022 Devine Lu Linvega a présenté beaucoup de choses passionnantes lors de [Handmade Seattle](https://handmade-seattle.com/), notamment la démarche de son studio Hundred Rabbits (avec Rekka Bellum), le besoin de ne plus utiliser des systèmes complexes ou qui nécessitent une connexion internet quasi-permanente, l'envie d'opérer une forme de décroissance numérique et de tester de nombreux langages de programmation peu connus voir ésotériques, et enfin en creux la question du [permacomputing](https://permacomputing.net/).
Le positionnement de Devine et de Rekka est impressionnant, parce que c'est un positionnement radical, expérimental et peut-être, d'une certaine façon, avant-gardiste (dans son sens littéraire et sa dimension de déconstruction et non d'innovation).

Deux citations extraites du long texte, la vidéo de l'intervention de Devine est [ici](https://guide.handmade-seattle.com/c/2022/weathering-software-winter/) :

> As a disclaimer, all that I am writing now is very naive. I draw, and I make music, when I started doing research I didn't have the vocabulary to find what I was looking for. I didn't know what virtual machines were, I didn't know what compilers were either. I had a vague idea of what programming was. I had written Swift and Objective C, but had no conception of how it actually tied to processors. It seemed like I was learning a service, the same way I was learning "To Photoshop". It wasn't like learning a skill, you don't learn to draw when you use Photoshop, you learn how to operate within the confines of someone else's playground, and when that rug is pulled from underneath you, there's nothing you can say or do, and you never really understood how it worked in the first place so you can't really replicate it.

> One-bit can be a totally evocative, things don't have to be visually busy all the time. I think this maximalism of 'I need all these features' and 'I need to be doing this and that,' is exhausting. Learning to live without float points is actually kind of nice. There is beauty in really simple systems, trying to always scale things to fit everyone's usage is foolish.
