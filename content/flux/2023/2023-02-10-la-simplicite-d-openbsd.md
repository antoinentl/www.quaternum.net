---
title: La simplicité d'OpenBSD
date: 2023-02-10
categorie:
- flux
tags:
- outils
description: "Simplicité n'est pas exacte, mais OpenBSD présente de nombreux avantages par rapport à d'autres distributions Linux."
---

> It’s **uncompromising**. It’s not a people-pleaser or vendor-pleaser. Linux is in everything from Android phones to massive supercomputers, so has to include features for all of them. The OpenBSD developers say no to most things. Instead of trying to make it do more, they keep it focused on doing what it does with more security and reliability.  
> Derek Sivers, OpenBSD : why and how, [https://sive.rs/openbsd](https://sive.rs/openbsd)

"Simplicité" n'est pas exacte, mais [OpenBSD](https://en.wikipedia.org/wiki/OpenBSD) présente de nombreux avantages par rapport à d'autres distributions Linux.
Et une certaine idée de la légèreté.

