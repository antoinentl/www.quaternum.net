---
title: Produire des sites web avec Pandoc
date: 2023-01-19
description: "Pandoc intègre désormais, à partir de la version 3.0, un moyen de produire une série de fichiers HTML liés entre eux, de quoi construire un site web très facilement, et simplement avec Pandoc."
categorie:
- flux
tags:
- publication
---
> New output format: chunkedhtml. This creates a zip file containing multiple HTML files, one for each section, linked with “next,” “previous,” “up,” and “top” links. (If -o is used with an argument without an extension, it is treated as a directory and the zip file is automatically extracted there, unless it already exists.) The top page will contain a table of contents if --toc is used. A sitemap.json file is also included. The option --split-level determines the level at which sections are to be split.  
> pandoc 3.0, Pandoc, [https://pandoc.org/releases.html#pandoc-3.0-2023-01-18](https://pandoc.org/releases.html#pandoc-3.0-2023-01-18)

Pandoc intègre désormais, à partir de la version 3.0, un moyen de produire une série de fichiers HTML liés entre eux, de quoi construire un site web _très_ facilement, et simplement avec Pandoc.
Je trouve cette idée particulièrement réjouissante pour fabriquer des livres web qui ne sont souvent qu'un ensemble de pages, d'autant plus avec ces éléments de navigation (page précédente, page suivante) disponibles par défaut.
Voir le site de démonstration qui permet de prendre la mesure de tout ça : [https://pandoc.org/chunkedhtml-demo/index.html](https://pandoc.org/chunkedhtml-demo/index.html).

Via John Maxwell.
