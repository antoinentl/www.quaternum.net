---
title: "Choisir une modélisation"
date: 2023-12-24
categorie:
- flux
tags:
- publication
description: "John Maxwell, professeur à l'Université Simon Fraser à Vancouver, explique ses choix concernant le _workflow_ de son (nouveau) blog."
---

> And I began to realize that while this was all technically possible, there were architectural reasons why this wasn’t optimal, and that, in trying to reinvent everything (e.g., blog staples like categories, tags, archive pages, RSS feeds) in my bash script, I would in effect be re-treading all the ground that the people who built Jekyll, Hugo, and Eleventy had already trod. And that made me feel somewhat silly.  
> John Maxwell, Imaginary Text, [https://imaginarytext.ca/posts/2023/StaticSiteTrials/](https://imaginarytext.ca/posts/2023/StaticSiteTrials/)

John Maxwell, professeur à l'Université Simon Fraser à Vancouver, explique ses choix concernant le _workflow_ de son (nouveau) blog.
La tension entre la personnalisation d'une modélisation et l'usage d'un modèle préconçu est intéressante : à quel point est-il nécessaire de créer _from scratch_ (ou presque) une chaîne de publication (ou d'édition) ?
Une réflexion similaire est développée par [Thomasaurus](https://thomasorus.com/from-static-site-generator-to-static-site-processor.html), qui fait le choix de [Lume](https://lume.land) pour ne pas dépendre de Node.js (dans le cas de 11ty), et ainsi de disposer d'un _processeur_ de site statique plutôt que d'un _générateur_.

Et il s'agit ici aussi de saluer l'ouverture d'un nouveau carnet, qui est toujours un événement en soit !

> And I think it’s time to start building “the new web” as people are saying these days (I installed an RSS Reader the other day, too, for the first time in a decade).

