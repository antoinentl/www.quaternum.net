---
title: "Une histoire de dépendances"
date: 2023-07-21
categorie:
- flux
tags:
- informatique
description: "Si l'histoire fait sourire, elle révèle un problème assez profond dans la volonté de donner des réponses non souhaitées."
---

> So I decided to never ask non-wizard users to install any dependencies again.  
> VitoVan, Jack's Ass, [https://sdf.org/~vito/jack.html](https://sdf.org/~vito/jack.html)

Si l'histoire fait sourire, elle révèle un problème assez profond dans la volonté de donner des réponses non souhaitées.
Tenter de résoudre des problèmes simples avec des processus complexes n'est pas en soit la question, il s'agit plutôt de rendre tout cela utilisable, et probablement _convivial_ (c'est le mot qui manque dans ce billet de blog).

