---
title: "Typst, une alternative à LaTeX ?"
date: 2023-07-19
categorie:
- flux
tags:
- publication
description: "Si l'émergence d'un nouveau langage de balisage aussi puissant que LaTeX semble une bonne idée, je reste dubitatif sur les efforts déployés pour atteindre la même qualité, la principale raison d'exister étant peut-être l'usage d'un autre langage de programmation."
---

> Typst is a new markup-based typesetting system for the sciences. It is designed to be an alternative both to advanced tools like LaTeX and simpler tools like Word and Google Docs. Our goal with Typst is to build a typesetting tool that is highly capable and a pleasure to use.  
> Typst, [https://typst.app](https://typst.app)

Si l'émergence d'un nouveau langage de balisage aussi puissant que LaTeX semble une bonne idée, je reste dubitatif sur les efforts déployés pour atteindre la même qualité, la principale raison d'exister étant peut-être l'usage d'un autre langage de programmation (comme [le signale Albert Krewinkel](https://tarleb.com/posts/typst-musings/)).
Un coup d'œil à [la syntaxe](https://typst.app/docs/guides/guide-for-latex-users/#elements) montre combien d'autres langages de balisage léger influencent celui-ci et je trouve ça assez intéressant.
Je m'interroge pour savoir si :

1. cette alternative à LaTeX est capable d'assurer conjointement un balisage de composition et un balisage sémantique (ce n'est toutefois pas son but) ?
2. Typst invite à se passer (en partie ou définitivement) de Pandoc ?

Mais je n'ai pas suffisamment testé l'outil pour pouvoir répondre à tout cela.
