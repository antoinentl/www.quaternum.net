---
title: "Manifeste (anticapitaliste) d'OFFDEM"
date: 2023-02-10
categorie:
- flux
tags:
- communauté
description: "L'OFFDEM, c'est la version OFF du FOSDEM, le FOSDEM (pour Free and Open Source Software Developers' European Meeting) c'est la grande messe du logiciel libre qui a lieu à Bruxelles tous les ans et qui rassemble tous ceux et toutes celles qui se sentent impliquées dans le développement du logiciel libre _et_ open source."
---
> Nous trouvons inacceptable que nos communautés soient associées à de telles entreprises [GMAFIA/GAFAM] : au lieu de soutenir les personnes qui partout dans le monde s’opposent à leur domination, nous franchissons leur piquet de grève. Si les producteurs de technologies libres ne s’opposent pas aux sociétés capitalistes de surveillance, qui le fera ?  
> OFFDEM, La menace qui pèse sur le logiciel libre, [https://thx.zoethical.org/pub/presence-solidaire-offdem-oxygene](https://thx.zoethical.org/pub/presence-solidaire-offdem-oxygene)

L'OFFDEM, c'est la version OFF du FOSDEM, le FOSDEM (pour Free and Open Source Software Developers' European Meeting) c'est la grande messe du logiciel libre qui a lieu à Bruxelles tous les ans et qui rassemble tous ceux et toutes celles qui se sentent impliquées dans le développement du logiciel libre _et_ open source.
Donc cela concerne aussi les (très) grandes entreprises qui font (beaucoup) d'argent avec le logiciel libre (et non pour).
L'enjeu n'étant plus de _simplement_ produire, utiliser et diffuser des logiciels libres, mais de constituer des communautés libres, l'OFFDEM apparaît comme une nécessité joyeuse et puissante.

> La proposition à OFFDEM est de faire confiance aux capacités et savoirs de nos réseaux de résistance, seuls capables d’habiter les interstices, de nouer des liens selon d’autres modalités, vivantes, pérennes […].
