---
layout: post
title: "Vous avez dit erreur ?"
date: 2020-11-12
published: true
description: "Abrüpt publie une nouvelle revue, Error, en ligne, sous-titrée Littérature des courts-circuits."
categories:
- flux
tags:
- publication
---
[Abrüpt](https://abrupt.cc/) publie une nouvelle revue, _Error_, [en ligne](https://www.error.re/), sous-titrée "Littérature des courts-circuits" ou "Antirevue des courts-circuits".
Que dire de cette revue (pour l'instant uniquement numérique) ?

- pour chaque article il y a 3 versions : web, PDF format A4, PDF format A5 à plier (pour construire un petit fanzine) ;
- il y a un dépôt ouvert : [https://gitlab.com/404-error/404-error.gitlab.io/](https://gitlab.com/404-error/404-error.gitlab.io/) ;
- le design est très soigné, avec une identité solidement encadrée — peut-être étonnamment pour une revue révolutionnaire ;
- le site est construit méthodiquement, tout y est très structuré mais aussi volontairement caché ;
- les textes sont forts, sans concession ;
- on peut regretter les métadonnées peu verbeuses (ouvrir une _merge request_ bientôt ?).
