---
layout: post
title: "Le code a changé"
date: "2020-03-12T22:00:00"
comments: true
published: true
description: "Le code a changé, c'est le nom du nouveau podcast (et non émission de radio) de Xavier de La Porte. À noter que l'invité d'un des premiers épisodes est Alexandre Laumonier et son livre _4_ !"
categories:
- flux
tags:
- métier
---
> Ce qui m’intéresse, c’est la technologie. Quand elle est appropriée, quand elle cesse d’être de la nouveauté parce qu’elle a été incorporée, parce qu’elle fait partie de nos vies, parce qu’elle est devenue banale.  
[Xavier de La Porte, Le code a changé](https://www.franceinter.fr/emissions/le-code-a-change/prologue)

Le code a changé, c'est le nom du nouveau podcast (et non émission de radio) de Xavier de La Porte.
À noter que l'invité d'un des premiers épisodes est Alexandre Laumonier et son livre _4_ !
