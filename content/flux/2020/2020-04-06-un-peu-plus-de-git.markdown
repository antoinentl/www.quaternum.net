---
layout: post
title: "Un peu plus de Git"
date: "2020-04-06T22:00:00"
comments: true
published: true
description: "Git est un outil – un instrument ? – dont la pratique peut être continuellement affinée, soit par la répétitions de commandes simples (il y en a), soit en apprenant de nouvelles méthodes et de nouvelles commandes (pour faire la même chose ou de nouvelles choses, sans forcément parler d'efficacité). C'est l'avantage avec cet outil aussi riche que souple. Et là j'ai appris plusieurs choses bien pratiques pour rendre l'usage plus agréable."
categories:
- flux
tags:
- outil
---
> The basics of Git can be learned. These techniques will empower your Git usage, help you avoid potential pitfalls, and make the life cycle of your development smoother.  
[James Turnbull, More productive Git](https://increment.com/open-source/more-productive-git/)

Git est un outil – un instrument ? – dont la pratique peut être continuellement affinée, soit par la répétitions de commandes simples (il y en a), soit en apprenant de nouvelles méthodes et de nouvelles commandes (pour faire la même chose ou de nouvelles choses, sans forcément parler d'efficacité).
C'est l'avantage avec cet outil aussi riche que souple.
Et là j'ai appris plusieurs choses bien pratiques pour rendre l'usage plus agréable.
