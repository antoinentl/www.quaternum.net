---
layout: post
title: "Fidus Writer"
date: 2020-12-01
published: true
description: "Marcello me souffle l'existence de Fidus Writer (www.fiduswriter.org), un éditeur de texte collaboratif en ligne pour l'édition scientifique."
categories:
- flux
tags:
- outils
---
> Fidus Writer is an online collaborative editor especially made for academics who need to use citations and/or formulas. The editor focuses on the content rather than the layout, so that with the same text, you can later on publish it in multiple ways: On a website, as a printed book, or as an ebook. In each case, you can choose from a number of layouts that are adequate for the medium of choice.  
> Fidus Writer, [https://www.fiduswriter.org](https://www.fiduswriter.org)

Marcello me souffle l'existence de Fidus Writer ([www.fiduswriter.org](https://www.fiduswriter.org/)), un éditeur de texte collaboratif en ligne pour l'édition scientifique.
Je pense que je l'avais vu passer il y a peut-être un an.
Il manque clairement une documentation, mais certaines fonctionnalités sont intéressantes.
Il s'agit d'un concurrent direct de Stylo, mais avec le (gros) défaut de vouloir offrir une expérience similaire à celle d'outils plus conventionnels.
