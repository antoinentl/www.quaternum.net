---
layout: post
title: "Maintenir"
date: "2020-01-31T22:00:00"
comments: true
published: true
description: "Jeremy Keith réagi à l'initiative de Frank Chimero en matière de documentation du redesign de son site web. Où il est question d'automatisation, de conception de nos outils de production, de design system et de temps qui passe."
categories:
- flux
tags:
- design
---
> Historically, we’ve seen automation in terms of physical labour—dock workers, factory workers, truck drivers. As far as I know, none of those workers participated in the creation of their mechanical successors. But when it comes to our work on the web, we’re positively eager to create the systems to make us redundant.  
The usual response to this is the one given to other examples of automation: you’ll be free to spend your time in a more meaningful way. With a design system in place, you’ll be freed from the drudgery of manual labour. Instead, you can spend your time doing more important work …like maintaining the design system.  
[Jeremy Keith, Architects, gardeners, and design systems](https://adactio.com/journal/16369)

Jeremy Keith réagi à l'initiative de Frank Chimero en matière de [documentation du redesign de son site web](https://frankchimero.com/blog/2019/redesign/).
Où il est question d'automatisation, de conception de nos outils de production, de _design system_ et de temps qui passe.
