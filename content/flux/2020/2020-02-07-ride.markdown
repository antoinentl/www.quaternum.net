---
layout: post
title: "RIDE"
date: "2020-02-07T22:00:00"
comments: true
published: true
description: "Découverte de la revue RIDE, via le message d'Elena Spadini sur la liste de diffusion francophone autour des Digital Humanities. Belle découverte."
categories:
- flux
tags:
- publication
---
> RIDE is an open access review journal dedicated to digital editions and resources.  
[IDE, Review Journal of the Institute for Documentology and Scholarly Editing](https://ride.i-d-e.de/)

Découverte de la revue _RIDE_, via [le message](https://groupes.renater.fr/sympa/arc/dh/2020-01/msg00049.html) d'[Elena Spadini](https://elespdn.github.io/io/) sur la liste de diffusion francophone autour des Digital Humanities.
Belle découverte.
