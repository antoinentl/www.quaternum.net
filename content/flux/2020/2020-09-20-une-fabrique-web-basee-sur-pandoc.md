---
layout: post
title: "Une fabrique web basée sur Pandoc"
date: 2020-09-20
published: true
description: "Arthur Perret a un usage avancé de Pandoc, qui lui permet de générer tous les formats dont il a besoin dans ses activités de publication."
categories:
- flux
tags:
- publication
---
> Or en simplifiant une architecture déjà basique, on peut envisager de se passer d’un CMS classique mais aussi d’un générateur de site statique comme Jekyll ou Hugo : ce dont j’ai besoin peut être réalisé par des scripts très simples, des outils qui n’évoluent pas ou presque (les scripts shell, Make). C’est ce que j’ai choisi d’appliquer à mon site.  
> Arthur Perret, Dr Pandoc & Mr Make, [https://www.arthurperret.fr/dr-pandoc-and-mr-make.html](https://www.arthurperret.fr/dr-pandoc-and-mr-make.html)

Arthur Perret a un usage avancé de Pandoc, qui lui permet de générer tous les formats dont il a besoin dans ses activités de publication.
Se passer d'un générateur de site statique semble effectivement plus cohérent que de tordre Jekyll via un convertisseur comme Pandoc.
Si cela fonctionne bien avec un site simple comme celui d'Arthur, je ne suis pas certain que cela fonctionnerait pour des sites web plus complexes (comme [quaternum.net](https://www.quaternum.net) par exemple).
Je suis assez fan du minimalisme de la solution, qui reste en partie modulaire puisque Make pourrait être remplacé par un autre système.
