---
layout: post
title: "Études critiques du code"
date: 2020-11-06
published: true
description: "Louis-Olivier a pris l'initiative de créer la version française de la page de présentation du champ de recherche Critical code studies."
categories:
- flux
tags:
- design
---
> Les études critiques du code (en anglais critical code studies) constituent un champ émergent de la recherche en sciences humaines et sociales à l'intersection des études logicielles, des humanités numériques, des études culturelles, de l'informatique, des interactions humain-machine et des études littéraires. Elles portent sur le code informatique, l'architecture logicielle et la documentation.  
> Wikipédia, Études critiques du code, [https://fr.wikipedia.org/wiki/%C3%89tudes_critiques_du_code](https://fr.wikipedia.org/wiki/%C3%89tudes_critiques_du_code)

[Louis-Olivier](https://www.loupbrun.ca) a pris l'initiative de créer la version française de la page de présentation du champ de recherche _Critical code studies_.
Voir : Marino, M. (2020). _Critical Code Studies_. MIT Press.