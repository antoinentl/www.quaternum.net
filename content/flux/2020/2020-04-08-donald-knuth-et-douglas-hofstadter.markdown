---
layout: post
title: "Donald Knuth et Douglas Hofstadter"
date: "2020-04-08T22:00:00"
comments: true
published: true
description: "Thomas Huot-Marchand lance un journal par le biais de l'Atelier national de recherche typographique (ANRT), et cette entrée sur Donal Knuth (et Douglas Hofstadter) est passionnante !"
categories:
- flux
tags:
- typographie
---
> En 1982, quelques mois après la publication de l'article de Donald Knuth “The Concept of a Meta-​Font”, la revue Visible Language publie “Meta-​Font, Metamathematics and Metaphysics: Comments on Donald Knuth's ‘The Concept of a Meta-​Font’”, par Douglas Hofstadter, un professeur américain de sciences cognitives. Ses remarques apportent des nuances aux conclusions de Knuth, abordant le problème d’un autre point de vue.  
[Thomas Huot-Marchand, Knuth Vs. Hofstadter](https://anrt-nancy.fr/journal/k-knuth/)

Thomas Huot-Marchand lance [un journal](https://anrt-nancy.fr/fr/journal/) par le biais de l'Atelier national de recherche typographique (ANRT), et cette entrée sur Donal Knuth (et Douglas Hofstadter) est passionnante !
