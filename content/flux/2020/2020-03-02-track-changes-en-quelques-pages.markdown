---
layout: post
title: "Track Changes en quelques pages"
date: "2020-03-02T22:00:00"
comments: true
published: true
description: "Pour les plus pressé·e·s qui s'intéressent néanmoins à l'histoire de l'écriture et de la publication, voici une chronique de Track Changes par Elena Spadini, le livre incontournable de Matthew Kirschenbaum."
categories:
- flux
tags:
- design
---
> With this essay, Kirschenbaum has widened the borders of literary history.  
[Elena Spadini, Review of:Kirschenbaum, Matthew G. Track Changes: A Literary History of Word Processing](https://serval.unil.ch/notice/serval:BIB_D8C95EAABB04)

Pour les plus pressé·e·s qui s'intéressent néanmoins à l'histoire de l'écriture et de la publication, voici une chronique de _Track Changes_ par Elena Spadini, le livre incontournable de Matthew Kirschenbaum.
