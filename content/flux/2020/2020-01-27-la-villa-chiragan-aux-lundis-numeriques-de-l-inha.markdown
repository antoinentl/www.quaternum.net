---
layout: post
title: "La villa Chiragan aux lundis numériques de l'INHA"
date: "2020-01-27T22:00:00"
comments: true
published: true
description: "Lundi 13 janvier 2020 Julie Blanc et Christelle Molinié sont venues présenter toute la mécanique derrière le catalogue Les sculptures de la villa romaine de Chiragan."
categories:
- flux
tags:
- publication
slug: la-villa-chiragan-aux-lundis-numeriques-de-l-inha
---
> Le système de publication devait être modulable, c'est-à-dire que toutes les briques que nous avons utilisées dans ce système pouvaient être changées si jamais on n'en a plus l'utilité ou si elles sont dépassées.  
[Julie Blanc et Christelle Molinié, Catalogue des sculptures de la villa romaine de Chiragan](https://www.youtube.com/watch?v=-bTGBPaJR3o)

Lundi 13 janvier 2020 Julie Blanc et Christelle Molinié sont venues présenter toute la mécanique derrière le catalogue _[Les sculptures de la villa romaine de Chiragan](https://villachiragan.saintraymond.toulouse.fr/)_.
Un an et demi avant, aux mêmes lundis numériques de l'Institut national de l'histoire de l'art, [je venais présenter](https://www.youtube.com/watch?v=-jNEInGuybs) des initiatives qui nous ont permis à Julie et à moi de faire ce catalogue.
La boucle est bouclée.
