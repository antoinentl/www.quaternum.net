---
layout: post
title: "Peritext"
date: "2020-02-12T22:00:00"
comments: true
published: true
description: "Peritext est un outil d'édition initié par Robin de Mourat. Il faut noter les outils cousins qui ont influencé Peritext, dont Scalar, Métopes ou PubPub."
categories:
- flux
tags:
- publication
---
> Peritext vise à équiper des pratiques d’écriture et d'édition savante sensibles et polymorphiques. Il articule des expérimentations situées avec des outils réutilisables pour questionner le rôle de la matérialité dans les pratiques de publication de la recherche universitaire. Ce document présente les différentes composantes du projet.  
[Robin de Mourat, Peritext](https://peritext.github.io/fr/)

Peritext est un outil d'édition initié par Robin de Mourat.
Il faut noter les outils cousins qui ont influencé Peritext, dont Scalar, Métopes ou PubPub.
