---
layout: post
title: "Un podcast autour du design"
date: "2020-03-19T22:00:00"
comments: true
published: true
description: "Un des enregistrements d'un nouveau podcast proposé par Élise Rigot, avec Anthony Masure et Xavier Guchet."
categories:
- flux
tags:
- design
---
> Nous partageons un dialogue entre deux chercheurs qui chacun à leur manière ont contribué à la réflexion du design par rapports aux techniques.  
[Elise Rigot, Dialogue entre Anthony Masure & Xavier Guchet - des designers passeur.se.s](https://podcast.ausha.co/bio-is-the-new-black/passeur-long)

Un des enregistrements d'un nouveau podcast proposé par Élise Rigot, avec Anthony Masure et Xavier Guchet.

(Je retiens notamment cette petite phrase lâchée par Anthony Masure et qui ne résume pas du tout cet épisode : "Une thèse réussie est toujours un déplacement.")
