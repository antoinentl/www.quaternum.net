---
layout: post
title: "Le contenu comme du code"
date: "2020-02-26T22:00:00"
comments: true
published: true
description: "Je tombe sur ce site web (non mis à jour depuis 2016) via un message de Pierre Ozoux. On retrouve beaucoup de points communs avec le doc as code promu par Asciidoctor."
categories:
- flux
tags:
- publication
---
> Content as Code is an approach to develop workflows and technology to improve content re-use and maintainability.  
It aims to make content authoring and management benefit from software engineering collaboration best practices.  
[iilab, Content as Code](https://www.unilim.fr/interfaces-numeriques/3915)

Je tombe sur ce site web (non mis à jour depuis 2016) via [un message de Pierre Ozoux](https://talk.libreho.st/t/future-of-documents/299).
On retrouve beaucoup de points communs avec le "doc as code" promu par Asciidoctor.
