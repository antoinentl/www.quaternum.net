---
layout: post
title: "Puissance et simplicité"
date: "2020-12-08T22:00:00"
published: true
description: "Voici une discussion à propos de la complexité de Hugo et de la difficulté pour de nombreuses personnes de réussir à utiliser toutes ses fonctions."
categories:
- flux
tags:
- design
---
> I’ve been a Hugo aficionado and small-scale evangelist for many years.  
> But a while ago a Hugo update broke my site/theme and I simply couldn’t fix it.  
> Peter_030, Has Hugo become too complex?, [https://discourse.gohugo.io/t/has-hugo-become-too-complex/29609](https://discourse.gohugo.io/t/has-hugo-become-too-complex/29609)

Voici une discussion à propos de la complexité de Hugo et de la difficulté pour de nombreuses personnes de réussir à utiliser toutes ses fonctions.

> It may come as a surprise, but Hugo is not Wordpress. It’s not even a direct competitor to Wordpress. Yes, both can be used to build a personal (or not too personal) website, but that’s all they have in common. Different philosophies, different approaches, different means for different goals.  
> nekrOz, [https://discourse.gohugo.io/t/has-hugo-become-too-complex/29609/26](https://discourse.gohugo.io/t/has-hugo-become-too-complex/29609/26)

> The features I add to Hugo or the bugs I fixes, those are stuff that I need fixed. There are certain things I find interesting on the technical level, but mostly it’s because I need it. That is egoism more than anything.  
> bep, [https://discourse.gohugo.io/t/has-hugo-become-too-complex/29609/35](https://discourse.gohugo.io/t/has-hugo-become-too-complex/29609/35)

Les questions indirectes sont celles du développement des logiciels libres, ici tout repose sur le bep, et je pense qu'il y a une cohérence lorsqu'il dit qu'il veut résoudre en priorité les problèmes qu'il rencontre.

> This thread stinks to coder elitism.  
> Javier_Cabrera, [https://discourse.gohugo.io/t/has-hugo-become-too-complex/29609/27](https://discourse.gohugo.io/t/has-hugo-become-too-complex/29609/27)

Il y a beaucoup de confusions dans les commentaires, et probablement trop d'attentes de la part de certaines personnes.

> I am **not** a programmer, but understanding Hugo is feasible. Hugo’s complexity is rather functionality . All in all I prefer generating a slim website with a super fast single binary SSG over dozens of MB of PHP scripts and plugins or tens of thousands of JS files. But it’s a matter of taste.  
> When I read some GitHub issues I sometimes don’t even understand what the guys are talking about. Programming is just not my profession. But this is not coder elitism – it’s just necessary technical vocabulary.  
> Hugo is a generous offer. If it is not the right instrument for you you don’t have to use it.  
> Grob, [https://discourse.gohugo.io/t/has-hugo-become-too-complex/29609/29](https://discourse.gohugo.io/t/has-hugo-become-too-complex/29609/29)

Comprendre la philosophie d'un outil _avant_ de l'utiliser est primordial, mais pas forcément possible.

Quoi qu'il en soit c'est l'occasion de tester le générateur de site statique [Zola](https://www.getzola.org/) dont il est question dans le message original, et de comprendre [pourquoi](https://mrkaran.dev/posts/migrating-to-zola/) certains fuient le générateur Hugo devenu trop complexe.

Il y aurait aussi un lien à faire avec l'ouvrage de Nadia Eghbal (_Sur quoi reposent nos infrastructures numériques ?_, [http://books.openedition.org/oep/1797](http://books.openedition.org/oep/1797)) : les questions de dépendances à un programme informatique développé par une seule personne ou un tout petit groupe.
L'engouement que peut générer un outil n'est pas proportionnel aux ressources disponibles pour développer et maintenir cet outil.
En d'autres termes : si une communauté d'utilisateurs et d'utilisatrices se forme autour d'un logiciel, d'un programme ou d'un outil, et que ces personnes ne sont pas dans la capacité de _maintenir_ cet outil, nous nous retrouvons dans une situation délicate.
Et il ne s'agit pas seulement de moyens ou de capacités, puisque le développement de certains programmes est volontairement centralisé.
Et cela rejoint aussi la question de la modularité, exemple :

- dans le cas de l'utilisation de Hugo, si le développeur principal arrête de maintenir le programme, il sera possible d'utiliser d'autres outils similaires moyennant des ajustements (parfois lourds) ;
- les alternatives à Hugo peuvent être aussi bien [Pandoc](/analyses/fabriques-de-publication-pandoc/), [Jekyll](/analyses/fabriques-de-publication-jekyll/) ou [Eleventy (11ty)](https://www.11ty.dev/).
