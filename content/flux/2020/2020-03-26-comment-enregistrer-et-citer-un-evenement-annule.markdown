---
layout: post
title: "Comment enregistrer et citer un événement annulé ?"
date: "2020-03-26T22:00:00"
comments: true
published: true
description: "Comment créer une référence (avec le style APA, mais cela peut être facilement appliqué à d'autres styles bibliographiques) pour une conférence ou communication annulée ? Un peu dommage d'avoir attendu cette période pour avoir ce type de solution, mais c'est toujours bon à prendre !"
categories:
- flux
tags:
- publication
---
> **Question**: How should the APA Style reference for an accepted presentation appear on my CV when the conference has been canceled?  
**Answer**: Include the presentation in your CV, as your work was peer reviewed and accepted, but consider which of the following cases is most applicable.  
[Timothy McAdoo, How to Create an APA Style Reference for a Canceled Conference Presentation](https://apastyle.apa.org/blog/canceled-conferences)

Comment créer une référence (avec le style APA, mais cela peut être facilement appliqué à d'autres styles bibliographiques) pour une conférence ou communication annulée ?
Un peu dommage d'avoir attendu cette période pour avoir ce type de solution, mais c'est toujours bon à prendre !
