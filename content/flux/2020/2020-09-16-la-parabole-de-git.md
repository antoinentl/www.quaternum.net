---
layout: post
title: "La parabole de Git"
date: 2020-09-16
published: true
description: "La complexité du fonctionnement de Git ne doit pas faire oublier que son utilisation est simple, il s'agit d'un protocole d'édition de documents texte que d'autres métiers déploient depuis longtemps."
categories:
- flux
tags:
- publication
---
> Git is really very simple underneath, and it is this simplicity that makes it so flexible and powerful.  
> Tom Preston-Werner, The Git Parable, [https://tom.preston-werner.com/2009/05/19/the-git-parable.html](https://tom.preston-werner.com/2009/05/19/the-git-parable.html)

La complexité du fonctionnement de Git ne doit pas faire oublier que son _utilisation_ est simple, il s'agit d'un protocole d'édition de documents texte que d'autres métiers déploient depuis longtemps.
Les éditeurs et les éditrices étaient en quelque sorte en avance sur les pratiques du développement informatique.
