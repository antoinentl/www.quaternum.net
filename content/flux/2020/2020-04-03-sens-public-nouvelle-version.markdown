---
layout: post
title: "Sens public (nouvelle version)"
date: "2020-04-03T22:00:00"
comments: true
published: true
description: "La nouvelle version du site de Sens public est enfin en ligne, après plusieurs années de travaux, entraînant la création d'un éditeur de texte sémantique, Stylo, et la mise en place d'une chaîne de publication modulaire. Bref, un beau projet."
categories:
- flux
tags:
- publication
---
> _Sens public_ est, depuis toujours, un chantier.  
[Sens public, Fêter Sens public](https://sens-public.org/articles/1509/)

La nouvelle version du site de _[Sens public](https://sens-public.org)_ est enfin en ligne, après plusieurs années de travaux, entraînant la création d'un éditeur de texte sémantique, [Stylo](https://stylo.ecrituresnumeriques.ca/), et la mise en place d'une chaîne de publication modulaire.
Bref, un beau projet.
