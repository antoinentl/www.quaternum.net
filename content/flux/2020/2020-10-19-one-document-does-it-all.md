---
layout: post
title: "One Document Does it all"
date: 2020-10-19
published: true
description: "Via Arthur Perret je découvre ce concept One Document Does it all."
categories:
- flux
tags:
- publication
---
> Le principe de l’affichage conditionnel est un premier pas vers le principe du One Document Does it all (ODD), dans lequel chacun est libre de remplacer document, it et all en fonction de sa problématique.  
> [Arthur Perret, Enseignement et automatisation avec Pandoc, https://www.arthurperret.fr/enseignement-automatisation-pandoc.html](https://www.arthurperret.fr/enseignement-automatisation-pandoc.html)

Via Arthur Perret je découvre ce concept de _One Document Does it all_.
Jusqu'ici j'utilisais — peut-être à tort — le concept de Single Source Publishing.

L'idée est de pouvoir produire différentes versions ou artefacts à partir d'une seule et même source, en faisant varier des paramètres (modèles de documents ou _templates_, données d'entête des documents ou _frontmatter_, données à la volée).

L'origine du concept est à chercher [du côté de la TEI](https://wiki.tei-c.org/index.php/ODD), lui-même héritier du concept de "Programmation lettrée" [de Donald Knuth](https://en.wikipedia.org/wiki/Literate_programming).

L'expression permet d'imaginer le recours à plusieurs fichiers, tant que l'ensemble forme un _document_.

Une bonne alternative (plus générique) à l'expression Single Source Publishing.
