---
layout: post
title: "Cyberpoétique"
date: "2020-01-22T22:00:00"
comments: true
published: true
description: "La cyberpoétique rêve sa révolte en dessous de nos mondes électriques."
categories:
- flux
tags:
- publication
---
> La cyberpoétique rêve sa révolte en dessous de nos mondes électriques. Elle chante l’espace de ses métamorphoses, disperse ses échos parmi l’abrüpt, désordonne ses passés, se met à la recherche de son devenir numérique. Elle silence, mais nous demeurons à son écoute.  
[Abrüpt, Cyberpoétique](https://www.cyberpoetique.org/)
