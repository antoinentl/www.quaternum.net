---
layout: post
title: "Paged design"
date: "2020-01-03T22:00:00"
comments: true
published: true
description: "Bonne idée que de pouvoir disposer de modèles préconçus pour fabriquer des livres avec paged.js !"
categories:
- flux
tags:
- design
---
> Are you creating books with HTML and CSS? Stylesheets for books can be long and complicated to write. Our themes give you a head start: pick a theme, add it to your project, and customise it.  
[Electric Book Works, Paged Design](https://paged.design/)

Bonne idée que de pouvoir disposer de modèles préconçus pour fabriquer des livres avec paged.js !
