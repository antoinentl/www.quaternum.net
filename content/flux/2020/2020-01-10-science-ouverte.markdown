---
layout: post
title: "Science ouverte ?"
date: "2020-01-10T22:00:00"
comments: true
published: true
description: "L'expression open science n'est effectivement pas très claire, malgré le fait qu'elle soit beaucoup utilisée. Tal Yarkoni pointe l'ambiguïté du terme et des différentes façons de s'y raccrocher."
categories:
- flux
tags:
- métier
---
> I fundamentally dislike the term open science. For the last few years, I’ve deliberately tried to avoid using it. I don’t call myself an open scientist, I don’t advocate publicly for open science (per se), and when people use the term around me, I often make a point of asking them to clarify what they mean.  
It’s ambiguous  
[Tal Yarkoni, I hate open science](https://www.talyarkoni.org/blog/2019/07/13/i-hate-open-science/)

L'expression _open science_ n'est effectivement pas très claire, malgré le fait qu'elle soit beaucoup utilisée.
Tal Yarkoni pointe l'ambiguïté du terme et des différentes façons de s'y raccrocher.
À rapprocher du problème similaire qu'engendre le mouvement des humanités numériques ?
