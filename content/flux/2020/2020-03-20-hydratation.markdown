---
layout: post
title: "Hydratation"
date: "2020-03-20T22:00:00"
comments: true
published: true
description: "Question récurrente sur laquelle Jemery Keith revient inlassablement et patiemment : le développement progressif et la dépendance des objets numériques à des contraintes techniques – qui sont des choix humains."
categories:
- flux
tags:
- web
---
> The actual process is: Run React on the server; send pre-generated HTML down the wire to the user; then send everything again but this time in JavaScript, bundled with the entire React library.  
[Jeremy Keith, Hydration](https://adactio.com/journal/16404)

Question récurrente sur laquelle Jemery Keith revient inlassablement et patiemment : le développement progressif et la dépendance des objets numériques à des contraintes techniques – qui sont des choix humains.
