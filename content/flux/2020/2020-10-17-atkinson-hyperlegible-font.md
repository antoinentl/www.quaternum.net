---
layout: post
title: "Atkinson Hyperlegible Font"
date: 2020-10-17
published: true
description: "Une fonte élégante pour l'imprimé et l'écran."
categories:
- flux
tags:
- typographie
---
> What makes it different from traditional typography design is that it focuses on letterform distinction to increase character recognition, ultimately improving readability.  
> [Atkinson Hyperlegible Font, Braille Institute, https://brailleinstitute.org/freefont](https://brailleinstitute.org/freefont)

Une fonte élégante pour l'imprimé et l'écran.
L'accessibilité en plus.
Très envie de l'utiliser dès que possible.
