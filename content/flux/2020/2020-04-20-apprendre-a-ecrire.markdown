---
layout: post
title: "Apprendre à écrire"
date: "2020-04-20T22:00:00"
comments: true
published: true
description: "Interrogation légitime et bienvenue de l'usage de GitHub dans les humanités (numériques)."
categories:
- flux
tags:
- publication
---
> [...] de l’abandon de « Word » à la maîtrise réelle des instruments permettant de structurer son écriture (et donc sa pensée), il y a parfois un gouffre.  
[Lisa Spiro, Evaluating GitHub as a Platform of Knowledge for the Humanities](https://dh2016.adho.org/abstracts/225)

Émilien Ruiz aborde la question de l'écriture académique pour les étudiants de façon très claire et très pragmatique.
Plutôt que se focaliser sur les outils les plus cohérents, il propose une approche en nuances tout en conservant un esprit critique.
