---
layout: post
title: "L'internet du futur"
date: "2020-02-19T22:00:00"
comments: true
published: true
description: "Une trentaine d'histoires et d'entretiens réunis sur ce site qui raconte l'histoire d'internet, passé et peut-être à venir. Ces lectures vous rendent optimiste ou pessimiste ?"
categories:
- flux
tags:
- web
slug: l-internet-du-futur
---
> In March 2019, the World Wide Web turned thirty, and October will mark the fiftieth anniversary of the internet itself. These anniversaries offer us an important opportunity to reflect on the internet’s history, but also a chance to ponder its future.  
[Katja Bego, Visions for the future internet](https://findingctrl.nesta.org.uk/)

Une trentaine d'histoires et d'entretiens réunis sur ce site qui raconte l'histoire d'internet, passé et peut-être à venir.
Ces lectures vous rendent optimiste ou pessimiste ?

Il y a une version "texte" plus légère pour celles et ceux qui auraient des contraintes (connexion, machine, handicap), le site original étant très beau mais un peu lourd (poids et interface) : [https://findingctrl.nesta.org.uk/text/](https://findingctrl.nesta.org.uk/text/).
