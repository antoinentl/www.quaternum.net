---
layout: post
title: "Les dessous du web"
date: 2020-11-16
published: true
description: "Un article qui regroupe nombre de ressources _cachées_ — ou tout du moins peu visibles en temps ordinaire, et hors du scope des moteurs de recherche généralistes."
categories:
- flux
tags:
- web
---
> Aujourd’hui on ne navigue plus sur internet, il n’y a plus d’exploration.  
> serveur410, Les passages secrets du web, [https://serveur410.com/les-passages-secrets-du-web/](https://serveur410.com/les-passages-secrets-du-web/)

Un article qui regroupe nombre de ressources _cachées_ — ou tout du moins peu visibles en temps ordinaire, et hors du _scope_ des moteurs de recherche généralistes.
Ce qui est intéressant ici est de constater la façon dont ces initiatives vivent indépendamment, et les choix techniques (site web, indexation, catégorisation).
