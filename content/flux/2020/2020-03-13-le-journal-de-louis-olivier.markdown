---
layout: post
title: "Le journal de Louis-Olivier"
date: "2020-03-13T22:00:00"
comments: true
published: true
description: "Voilà. Si vous hésitez encore à ouvrir un carnet web, juste faites-le !"
categories:
- flux
tags:
- publication
---
> Ce journal est une réaction nécessaire à la concentration massive du pouvoir technique – un pouvoir devenu presque total.  
[Louis-Olivier, Billet zéro : pourquoi ce journal ?](https://journal.loupbrun.ca/n/000/)

Voilà.
Si vous hésitez encore à ouvrir un carnet web, juste faites-le !
