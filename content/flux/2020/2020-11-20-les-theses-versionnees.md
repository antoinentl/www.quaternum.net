---
layout: post
title: "Les thèses versionnées"
date: 2020-11-20
published: true
description: "Nicolas (Sauret) met sa thèse en ligne, et suit les chemins tracés par plusieurs chercheur·e·s comme Anthony Masure, Nolwenn Maudet ou Robin de Mourat."
categories:
- flux
tags:
- publication
---
> Si l’on s’accorde à dire que les outils numériques ont modifié en profondeur nos pratiques d’écriture et de lecture, l’influence que ces nouvelles pratiques exercent sur les contenus d’une part, et sur la structuration de notre pensée d’autre part, reste encore à déterminer.  
> [Nicolas Sauret, De la revue au collectif : la conversation comme dispositif d’éditorialisation des communautés savantes en lettres et sciences humaines](https://these.nicolassauret.net/)

Nicolas (Sauret) met sa thèse en ligne, et suit les chemins tracés par plusieurs chercheur·e·s comme [Anthony Masure](http://www.softphd.com/), [Nolwenn Maudet](https://designing-design-tools.nolwennmaudet.com/) ou [Robin de Mourat](http://www.these.robindemourat.com/).
Sa thèse est également [versionnée](https://framagit.org/laconis/these), ce qui permet de constater une utilisation très spécifique de Git :

- usage individuel de la gestion de versions ;
- l'objectif est à la fois de versionner pour structurer et organiser le travail d'écriture ;
- et de publier ces changements de versions pour _montrer_ l'évolution ;
- et enfin pour garder une trace (pour Nicolas et pour ses lecteurs) des strates ainsi créées.
