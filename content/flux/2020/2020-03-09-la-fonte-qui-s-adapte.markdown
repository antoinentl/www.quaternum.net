---
layout: post
title: "La fonte qui s'adapte"
date: "2020-03-09T22:00:00"
comments: true
published: true
description: "Pour tester la puissance adaptative de cette fonte (ou police typographique), rien de tel que cette page assez hallucinante."
categories:
- flux
tags:
- typographie
slug: la-fonte-qui-s-adapte
---
> Inter is a typeface carefully crafted & designed for computer screens.  
[Rasmus Andersson, Inter font family](https://rsms.me/inter/)

Pour tester la puissance adaptative de cette fonte (ou police typographique), rien de tel que [cette page assez hallucinante](https://rsms.me/inter/lab/).

Via [Vincent Valentin](https://twitter.com/htmlvv/status/1231542315262627840).
