---
layout: post
title: "Des ressources pour Hugo"
date: 2020-12-02
published: true
description: "Une ressource qui en regroupe beaucoup."
categories:
- flux
tags:
- outils
---
> A curated list of awesome things related to Hugo, the world's fastest framework for building websites.  
> Awesome Hugo, [https://github.com/theNewDynamic/awesome-hugo](https://github.com/theNewDynamic/awesome-hugo)

Une ressource qui en regroupe beaucoup.
[Hugo](https://gohugo.io/) nécessite ce type de démarche, tant l'outil permet de faire énormément de choses, encore faut-il le savoir.
