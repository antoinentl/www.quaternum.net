---
layout: post
title: "La création littéraire en ligne"
date: "2020-01-29T22:00:00"
comments: true
published: true
description: "Franck Queyraud a dirigé cet ouvrage collectif qui réunit notamment Alexandra Saemmer, François Bon, Anne Savelli, Benoît Epron ou Guillaume Hatt. Et moi-même pour un chapitre intitulé Origines, formes et supports du livre numérique."
categories:
- flux
tags:
- livre
---
> On ne peut plus écrire aujourd'hui sans prendre en compte l'arrivée du traitement de texte individuel, l'accès facilité aux sources du savoir, les échanges interactifs avec les lecteurs, la possibilité pour un auteur de maîtriser sa publication, la nécessité également d'apprendre le code informatique pour être maître des dispositifs techniques.  
[Franck Queyraud, Connaître et valoriser la création littéraire numérique en bibliothèque](https://books.openedition.org/pressesenssib/9846)

Franck Queyraud a dirigé cet ouvrage collectif édité aux Presses de l'Enssib et qui réunit notamment Alexandra Saemmer, François Bon, Anne Savelli, Benoît Epron ou Guillaume Hatt.
Et moi-même pour un chapitre intitulé "Origines, formes et supports du livre numérique".
Si le livre est accessible en ligne seulement aux personnes dont leur bibliothèque est abonnée à OpenEdition Freemium, il y a également une version papier ainsi qu'une belle sélection de ressources dans ce _padlet_ : [https://padlet.com/lirecrirenumerique/lire_ecrire_web](https://padlet.com/lirecrirenumerique/lire_ecrire_web).
