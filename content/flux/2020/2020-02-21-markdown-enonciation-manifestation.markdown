---
layout: post
title: "Markdown, énonciation, manifestation"
date: "2020-02-21T22:00:00"
comments: true
published: true
description: "Patrick Mpondo-Dicka fait une présentation historique et méticuleuse de Markdown en tant que simplification de l'écriture numérique, comme exemple du texte qui fait le lien entre machine et humain."
categories:
- flux
tags:
- publication
---
> Nous pensons que la popularité du Markdown est paradoxalement due à ces limites : incomplet en tant que langage informatique, il l’est aussi en tant que sociolecte numérique, mais cela favorise son appropriation en ouvrant les possibilités de sa modification ; le caractère flexible du langage facilite son adoption par une frange plus large de la communauté utilisatrice, quitte à ce que cela pose ensuite des problèmes d’interopérabilité.  
[Patrick Mpondo-Dicka, Le Markdown, une praxis énonciative du numérique](https://www.unilim.fr/interfaces-numeriques/3915)

Patrick Mpondo-Dicka fait une présentation historique et méticuleuse de Markdown en tant que simplification de l'écriture numérique, comme exemple du texte qui fait le lien entre machine et humain.
