---
layout: post
title: "Urgence bibliothéconomique"
date: "2020-04-02T22:00:00"
comments: true
published: true
description: "L'ouverture d'une grande bibliothèque."
categories:
- flux
tags:
- livre
---
> To address our unprecedented global and immediate need for access to reading and research materials, as of today, March 24, 2020, the Internet Archive will suspend waitlists for the 1.4 million (and growing) books in our lending library by creating a National Emergency Library to serve the nation’s displaced learners.  
[Chris Freeland, Announcing a National Emergency Library to Provide Digitized Books to Students and the Public](https://blog.archive.org/2020/03/24/announcing-a-national-emergency-library-to-provide-digitized-books-to-students-and-the-public/)

L'ouverture d'une grande bibliothèque.
