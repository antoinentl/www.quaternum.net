---
layout: post
title: "Adaptation contre l'automatisation"
date: "2020-01-08T22:00:00"
comments: true
published: true
description: "Où l'on comprend mieux pourquoi certaines interfaces ne fonctionnent pas très bien, et de la nécessité d'interroger leur conception dans un contexte algorithmico-numérique."
categories:
- flux
tags:
- design
slug: adaptation-contre-l-automatisation
---
> Lorsque l’on observe les plateformes numériques actuelles, c’est comme si les designers avaient d’abord créé les interfaces et les interactions avant d’y ajouter des algorithmes après coup. Les algorithmes forment une couche séparée, sans existence visuelle. À l’inverse, ils réassignent les fonctionnalités existantes pour en extraire des données qu’ils utilisent ensuite pour générer des inférences. Leur invisibilité explique aussi pourquoi ils sont souvent considérés à raison comme des boites noires.  
[Nolwenn Maudet, L'angle mort de la personnalisation](https://carnet.nolwennmaudet.com/home/angle-mort-de-la-personnalisation)

Où l'on comprend mieux pourquoi certaines interfaces ne fonctionnent pas très bien, et de la nécessité d'interroger leur conception dans un contexte algorithmico-numérique.
