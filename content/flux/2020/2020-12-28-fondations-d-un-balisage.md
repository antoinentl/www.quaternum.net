---
layout: post
title: "Fondations d'un balisage"
date: 2020-12-28
published: true
description: "L'histoire de Markdown est instructive à bien des égards, elle nous permet de comprendre pourquoi ce langage de balisage est apparu, et quelle filiation il a avec d'autres langages ou initiatives."
categories:
- flux
tags:
- design
slug: fondations-d-un-balisage
---
> That spirit is what's made Markdown endure. It can be whatever you want it to be. There's no one way to use it; pick the way that's easiest for you.  
> Matthew Guay, The story behind Markdown, [https://capiche.com/e/markdown-history](https://capiche.com/e/markdown-history)

L'histoire de Markdown est instructive à bien des égards, elle nous permet de comprendre pourquoi ce langage de balisage est apparu, et quelle filiation il a avec d'autres langages ou initiatives.
Il faut noter que d'autres systèmes de _sémantisation_ ont précédé Markdown, basés eux aussi sur des signes typographiques minimalistes et du texte brut.

> L’idée que certains caractères puissent « mettre en boîte » du texte a peut-être une signification profonde, ancienne.  
> Arthur Perret, Histoire typographique de la légèreté, [https://www.arthurperret.fr/histoire-typographique-legerete.html](https://www.arthurperret.fr/histoire-typographique-legerete.html)

Arthur Perret explore aussi le sens et l'origine de ces caractères utilisés pour baliser le texte, il en ressort des mécanismes plus complexes qu'il n'y paraît.
Et c'est passionnant.

> Dans le cas du balisage, la légèreté est certainement une histoire de scripturation – qui reste à écrire.  
> Arthur Perret
