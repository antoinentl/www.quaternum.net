---
layout: post
title: "Fabriques faites maison"
date: 2020-12-29
published: true
description: "Voici une thématique fréquente dans le domaine de la programmation, et qui se déplace sur le champ de la publication avec l'apparition et l'usage massif des générateurs de site statique."
categories:
- flux
tags:
- publication
---
> The real reason for all this work is that I think that a personal site should be personal and to make it personal, one should solely be guided by one’s intuition and not by the mental models of available tools and the restrictions they impose on your thoughts.  
> Erik Winter, Why I Built My Own Shitty Static Site Generator, [https://ewintr.nl/shitty-ssg/why-i-built-my-own-shitty-static-site-generator/](https://ewintr.nl/shitty-ssg/why-i-built-my-own-shitty-static-site-generator/)

Voici une thématique fréquente dans le domaine de la programmation, et qui se déplace sur le champ de la publication avec l'apparition et l'usage massif des générateurs de site statique.
Plutôt que d'utiliser des solutions existantes, voir de les détourner — au points parfois de les tordre —, les développeurs et les développeuses fabriquent leurs propres outils, souvent dans un contexte où ils et elles créent pour un usage individuel.

Erik Winter explique bien pourquoi il s'est détourné de générateurs de site statique tels que Hugo ou Jekyll : il ne veut pas être contraint par un outil, avec cette idée que l'outil contraint le geste, et donc que la technique nous contraint — je grossis un peu le trait.
André Leroi-Gourhan parle plutôt de rencontre entre la technique et l'humain, et d'_orientation_.
Est-ce que cela signifie que les "éléments techniques" ont une influence sur les "individus techniques" pour reprendre les termes de Gilbert Simondon ?

Je ne suis pas totalement d'accord avec Erik Winter, qui considère — de manière peut-être un peu binaire — qu'une _fabrique_ influence notre façon de penser ou d'écrire, et que nous nous retrouvons à devoir entrer dans le moule de celui ou celle qui a pensé l'outil.
Il est aussi possible de _se jouer_ des contraintes, d'en prendre conscience et de les utiliser à bon escient.
Naviguer de fabriques en fabriques permet de constituer un ensemble de pratiques pour ensuite pouvoir choisir le meilleur compromis.
Car il s'agit aussi de compromis, compromis qui apparaîtraient même en développant un outil _from scratch_.

Mais est-ce qu'il faut forcément coder son outil pour le maîtriser et ainsi ne pas être influencé dans ses pratiques d'écriture ?
N'est-ce pas là un raccourci puisque la création d'un outil est elle-même influencée par d'autres facteurs ?
Ne faudrait-il pas carrément créer son propre langage de programmation _avant_ de créer l'outil ?

Karl Dubost [parle](https://www.la-grange.net/2020/12/14/becoming-you) d'"outil intime", cette dimension intime définit bien la relation créée avec un programme que l'on code soi-même, et son usage particulier dans un contexte individuel.
Il est possible de développer des programmes et de les mettre à disposition d'une communauté, tout en créant en parallèle d'autres systèmes sans forcément les proposer publiquement.
Créer _pour soi_, c'est quelque chose d'important quelque soit le domaine.
C'est même essentiel pour ne pas toujours _fabriquer_ dans l'objectif de le mettre à disposition, et ainsi formater notre façon de faire.
