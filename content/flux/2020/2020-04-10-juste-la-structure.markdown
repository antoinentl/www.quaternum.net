---
layout: post
title: "Juste la structure"
date: "2020-04-10T22:00:00"
comments: true
published: true
description: "Toutes ces dernières années je suis passé à côté de cette journée au combien importante, le CSS Naked Day : l'occasion de montrer à quoi ressemble le web sans feuille de style, à quoi ressemble un site web avec sa seule structure et la feuille de style par défaut du navigateur. Merci à Laura Kalbag d'expliquer de quoi il s'agit."
categories:
- flux
tags:
- web
---
> You don’t need to make your site naked for all the public to see. Maybe just try it in the privacy of your own home. Use it as an opportunity to find out where you’re using `<div>`s and `<span>`s where you could be using a more descriptive element. Try disabling styles in your developer tools and browsing other sites without CSS for the day.  
[Laura Kalbag, CSS Naked Day 2020](https://laurakalbag.com/css-naked-day-2020/)

Toutes ces dernières années je suis passé à côté de cette journée au combien importante, le [CSS Naked Day](https://css-naked-day.github.io/)&nbsp;: l'occasion de montrer à quoi ressemble le web sans feuille de style, à quoi ressemble un site web avec sa seule structure et la feuille de style par défaut du navigateur.
Merci à Laura Kalbag d'expliquer de quoi il s'agit.
