---
layout: post
title: "La puissance des sites personnels"
date: "2020-01-13T22:00:00"
comments: true
published: true
description: "Matthias Ott liste méthodiquement les différents avantages de disposer de son propre site web (lieu d'expérimentations, liens entre sites, pratiques d'écriture, stratégie de publication, communauté et opportunité de rencontres, etc.)."
categories:
- flux
tags:
- web
---
> Building things for your own site is so worthwhile because you are allowed to make mistakes and learn without pressure. If it doesn’t work today, well, maybe it’ll work tomorrow. It doesn’t matter.  
[Matthias Ott, Into the Personal-Website-Verse](https://matthiasott.com/articles/into-the-personal-website-verse)

Matthias Ott liste méthodiquement les différents avantages de disposer de son propre site web (lieu d'expérimentations, liens entre sites, pratiques d'écriture, stratégie de publication, communauté et opportunité de rencontres, etc.).
