---
layout: post
title: "Fraidycat"
date: "2020-03-30T22:00:00"
comments: true
published: true
description: "Fraidycat est un lecteur de flux RSS minimaliste, qui permet de récupérer les entêtes des items de différents flux (mais sans récupérer le contenu). Simple et efficace."
categories:
- flux
tags:
- web
---
> Fraidycat is a desktop app or browser extension for Firefox or Chrome. I use it to follow people (hundreds) on whatever platform they choose - Twitter, a blog, YouTube, even on a public TiddlyWiki.  
[Fraidycat](https://fraidyc.at/)

Fraidycat est un lecteur de flux RSS minimaliste, qui permet de récupérer les entêtes des items de différents flux (mais sans récupérer le contenu).
Simple et efficace.

Via [Laura](https://laurakalbag.com/how-to-read-rss-in-2020/) via [Nicolas](https://nicolas-hoizey.com/links/2020/03/24/how-to-read-rss-in-2020/).
