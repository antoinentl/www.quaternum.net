---
layout: post
title: "Définir le low-tech"
date: "2020-04-13T22:00:00"
comments: true
published: true
description: "Voici un long article, tout en nuance, sur le concept de low-tech, concept à la mode qui mérite qu'on le critique."
categories:
- flux
tags:
- design
---
> Il semble raisonnable d’affirmer que la binarité du _high_ et du _low_ est improductive. Pourtant la “low-tech” agrège des pratiques et des savoirs techniques pertinents pour décrire un monde en train de se transformer en profondeur. Alors, que faire de ce terme ? Il semble nécessaire de l’utiliser pour ce qu’il est : une porte d’entrée.  
[Gautier Roussilhe, Une erreur de "tech"](http://www.gauthierroussilhe.com/fr/posts/une-erreur-de-tech)

Voici un long article, tout en nuance, sur le concept de low-tech, concept à la mode qui mérite qu'on le critique.

Via [Julie Blanc](https://julie-blanc.fr/).
