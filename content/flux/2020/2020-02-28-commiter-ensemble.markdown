---
layout: post
title: "Commiter ensemble"
date: "2020-02-28T22:00:00"
comments: true
published: true
description: "Comment commiter ensemble ? Techniciens, techniciennes et non-techniciennes et non-techniciens. Une bonne introduction au système de contrôle de versions Git (à lire avec des dessins de chats)."
categories:
- flux
tags:
- outils
---
> En tant que développeuses ou développeurs, nous avons tendance à oublier que nous utilisons un jargon obscur qui nous fait ressembler à d’occultes sorcières ou sorciers. [...] Il n’y a pas beaucoup de ressources disponibles en ligne qui introduisent Git, GitHub, le concept de commits, branches, gestion de version, forks, pull requests et autres termes liés au développement, qui apparaissent comme du jargon auprès d’une audience non-technique. Ce texte est une tentative d’améliorer la situation.  
[Agate_ Berriot, Construire des logiciels, ensemble, avec Git](https://agate.blue/blog/2019/03/08/construire-logiciel-ensemble-git/)

Comment commiter ensemble ?
Techniciens, techniciennes et non-techniciennes et non-techniciens.
Une bonne introduction au système de contrôle de versions Git (à lire avec [des dessins de chats](https://girliemac.com/blog/2017/12/26/git-purr/)).
