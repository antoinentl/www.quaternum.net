---
layout: post
title: "Fabriques personnelles"
date: 2020-12-30
published: true
description: "Intéressante argumentation en faveur d'une webdiversité."
categories:
- flux
tags:
- web
---
> On your personal website, you own your work. You decide what and when to publish. You decide when to delete things. You are in control. Your work, your rules, your freedom.  
> Matthias Ott, Make it Personal, [https://css-tricks.com/make-it-personal/](https://css-tricks.com/make-it-personal/)

Intéressante argumentation en faveur d'une _webdiversité_.
Au-delà de la question de disposer d'un espace personnel maîtrisé, il y a la question de l'expérimentation de la fabrique de publication.
Des fabriques personnelles _et_ en partie ou totalement faites maison.

Le regret ici est de constater que celles et ceux qui expérimentent sont majoritairement des personnes impliquées dans le Web, c'est presque une déformation professionnelle.
Dommage que les chercheurs et chercheuses qui ont pourtant des activités de publication ne soient pas plus impliqués dans ce type d'initiative.

> [Personal websites](https://personalsit.es/) are such a thing. If you don’t have a website yet, come join us on the other side where the web is (still) personal. It’s worth it.
