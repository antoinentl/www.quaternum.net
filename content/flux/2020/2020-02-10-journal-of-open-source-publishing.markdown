---
layout: post
title: "Journal Of Open Source Software"
date: "2020-02-10T22:00:00"
comments: true
published: true
description: "Déjà aperçu il y a quelques mois, JOSS (ou The Journal of Open Source Software) a été présenté pendant la devroom Open Research Tools and Technologies durant le FOSDEM 2020. Cette revue répond à un besoin relativement simple : comment citer un logiciel dans un article scientifique ? Plutôt que de citer le logiciel, pourquoi ne pas écrire un article sur ce logiciel et citer ce papier ?"
categories:
- flux
tags:
- publication
slug: journal-of-open-source-publishing
---
> The Journal of Open Source Software is a developer friendly, open access journal for research software packages.  
[The Journal of Open Source Software](https://joss.theoj.org/)

Déjà aperçu il y a quelques mois, JOSS (ou _The Journal of Open Source Software_) a été présenté pendant [la devroom Open Research Tools and Technologies](https://fosdem.org/2020/schedule/track/open_research_tools_and_technologies/) durant le FOSDEM 2020.
Cette revue répond à un besoin relativement simple : comment citer un logiciel dans un article scientifique ?
Plutôt que de citer le logiciel, pourquoi ne pas écrire un article sur ce logiciel et citer ce papier ?

[JOSS](https://joss.theoj.org/) a également la particularité d'utiliser Git, GitHub et les issues pour réaliser la révision de l'article.
Il y a [une documentation](https://joss.readthedocs.io/en/latest/) qui présente cela.

La présentation de Karthik Ram pendant le FOSDEM [est disponible en ligne](https://inundata.org/talks/fosdem/), ainsi que [la vidéo](https://fosdem.org/2020/schedule/event/open_research_joss/).
