---
layout: post
title: "Une certaine vision de la performance"
date: 2020-11-10
published: true
description: "Comparatif intéressant concernant les générateurs de site statique les plus populaires, le problème étant que ce calcul de performance se base sur le nombre de pages produites, et non sur la complexité des modèles de ces pages."
categories:
- flux
tags:
- web
---
> This is an in-depth comparison of build times across multiple popular SSGs and, more importantly, to analyze _why_ those build times look the way they do. Blindly choosing the fastest or discrediting the slowest would be a mistake. Let’s find out why.  
> Sean C Davis, Comparing Static Site Generator Build Times, [https://css-tricks.com/comparing-static-site-generator-build-times/](https://css-tricks.com/comparing-static-site-generator-build-times/)

([Version française](https://jamstatic.fr/2020/10/31/comparatif-performance-generateurs-de-site-statique/))

Comparatif intéressant concernant les générateurs de site statique les plus populaires, le problème étant que ce calcul de performance se base sur le nombre de pages produites, et non sur la complexité des modèles de ces pages.
C'est pourtant un point essentiel des générateurs de site statique : la façon dont les données sont manipulées.
