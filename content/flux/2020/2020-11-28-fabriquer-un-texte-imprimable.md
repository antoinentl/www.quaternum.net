---
layout: post
title: "Fabriquer un texte imprimable"
date: 2020-11-28
published: true
description: "Timothée Goguely partage son expérience de design d'une publication pour la structure Design commun."
categories:
- flux
tags:
- publication
---
> Ce journal documente le travail de mise en page de l’ouvrage Situer le numérique rédigé par Gauthier Roussihle à l’aide du générateur de site statique Hugo et de la bibliothèque Paged.js.  
> Timothée Goguely, [Design Commun] Situer le numérique, [http://pad.designcommun.fr/s/BJWk-A3QD](http://pad.designcommun.fr/s/BJWk-A3QD)

[Timothée Goguely](https://timothee.goguely.com/) partage son expérience de design d'[une publication](https://designcommun.fr/cahiers/situer-le-numerique/) pour la structure [Design commun](https://designcommun.fr/) : https://situer-le-numerique.netlify.app/.
Plusieurs choses me semblent intéressantes, comme le fait d'utiliser une _fabrique_ uniquement pour produire une version imprimable, sans version web.

Les outils utilisés sont le générateur de site statique Hugo, Forestry pour interagir sur les contenus, le polyfill pagedjs pour paginer le web et Git pour le versionnement.
À noter que Timothée [a documenté sa pratique](http://pad.designcommun.fr/s/BJWk-A3QD), très pratique pour comprendre sa démarche.
