---
layout: post
title: "Markdown + Git + Pandoc"
date: 2020-09-08
published: true
description: "C'est le trio qui fonctionne en tout temps, mais qui manque encore d'interfaçage."
categories:
- flux
tags:
- publication
---
> Le futur de l’écriture collaborative (Nature, mars 2020) • Rédiger et citer en Markdown, gérer avec git, convertir avec pandoc… L’alternative "élégante et complexe" à Word / Docs, née de l'écosystème scientifique texte brut. Lecture rassurante !  
> [Arthur Perret, Twitter](https://twitter.com/arthurperret/status/1303021193566576645)

C'est le trio qui fonctionne en tout temps, mais qui manque encore d'interfaçage.
Comme le dit Arthur les solutions existent mais il faut encore accompagner la prise en main, comme s'il y avait forcément déplacement de l'un (UX) vers l'autre (puissance).

Les deux articles en question :

- Jeffrey M. Perkel, Synchronized editing: the future of collaborative writing, [https://www.nature.com/articles/d41586-020-00916-6](https://www.nature.com/articles/d41586-020-00916-6)
- Jeffrey M. Perkel, Three ways to collaborate on writing, [https://www.natureindex.com/news-blog/three-ways-to-collaborate-on-writing](https://www.natureindex.com/news-blog/three-ways-to-collaborate-on-writing)
