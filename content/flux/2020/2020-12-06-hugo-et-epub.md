---
layout: post
title: "Hugo et EPUB"
date: 2020-12-06
published: true
description: "Je cherchais cela depuis un moment, comment générer un livre numérique au format EPUB avec Hugo."
categories:
- flux
tags:
- ebook
---
> The widely used e-book file format epub is actually just a zip folder with a fixed structure of XHTML files, images and some metadata. That’s why I came up with the idea to develop a HUGO theme that creates an e-book directly from the HUGO content folder without further manual intervention.  
> Erhard Maria Klein, Generate Hugo Website as E-Book (epub), [https://discourse.gohugo.io/t/generate-hugo-website-as-e-book-epub/29559/7](https://discourse.gohugo.io/t/generate-hugo-website-as-e-book-epub/29559/7)

Je cherchais cela depuis un moment, comment générer un livre numérique au format EPUB avec Hugo.
Il y a aussi [un dépôt avec une documentation](https://github.com/weitblick/epub).
L'idée pour moi est de générer un format EPUB aux côtés d'au moins deux autres formats : HTML (site web) et PDF (avec paged.js).

La question se pose de comment bien distinguer les différents formats sans que cela ne devienne une usine à gaz, et ce sera un des enjeux de mes différentes expérimentations des prochains mois.
