---
layout: post
title: "Antilivre"
date: 2020-09-12
published: true
description: "La maison d'édition Abrüpt dédie un site web pour son concept d'Antilivre."
categories:
- flux
tags:
- livre
---
> L’antilivre n’a pas de forme, son impermanence dispose de toutes les formes, il se transforme sans cesse, et son information brute ne connaît aucune fixité, aucune frontière, elle fragmente son essence, distribue le commun, déploie sa liberté au-devant de nos singularités cybernétiques.  
> Abrüpt, Antilivre, [https://www.antilivre.org/](https://www.antilivre.org/)

La maison d'édition Abrüpt dédie un site web pour son concept d'_Antilivre_.
Mais d'ailleurs peut-on parler de concept ?
Il s'agit à la fois d'une démarche éditoriale, d'une forme d'artéfact et d'un geste d'édition.

À mon humble avis il ne faut pas interpréter cela comme une simple volonté de pointer une originalité, mais il s'agit bien d'une reconfiguration de ce que l'on peut appeler _édition_ et _livre_.

À noter qu'Abrüpt va jusqu'à définir une typologie (Antilivre Dynamique, Antilivre Statique, etc.), qui est aussi un moyen de découvrir les antilivres de la maison d'édition.
