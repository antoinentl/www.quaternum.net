---
layout: post
title: "Un podcast sur les livres et leur fabrication"
date: 2020-12-04
published: true
description: "Un podcast d'Arthur Attwell (à l'origine de Electric Book Works) qui semble correspondre à peu près à ce que j'aurais aimé faire, mais en beaucoup mieux."
categories:
- flux
tags:
- livre
---
> How Books Are Made is a podcast about the art and science of making books. It’s for book lovers who believe that details matter, on paper and on screen: from the feel of the paper to the shapes of the ligatures, from hyperlinks to accessibility.  
> Arthur Attwell, How Books Are Made, [https://howbooksaremade.com/](https://howbooksaremade.com/)

Un podcast d'Arthur Attwell (à l'origine de [Electric Book Works](https://electricbookworks.com/)) qui semble correspondre à peu près à ce que j'aurais aimé faire, mais en beaucoup mieux.
