---
layout: post
title: "La souffrance des traitements de texte"
date: "2020-03-24T22:00:00"
comments: true
published: true
description: "Je ne sais pas trop quoi penser de cette étude de 2014, qui semble à la fois sérieuse dans sa méthodologie et en même temps qui comporte quelques œillères : quand bien même un système de publication comme LaTeX pourrait donner quelques difficultés à son utilisateur ou le rendre un peu moins productif, est-ce qu'il ne répond pas à d'autres besoins (la liberté) et est-ce qu'il n'offre pas d'autres satisfactions (la maîtrise) ?"
categories:
- flux
tags:
- publication
---
> The choice of an efficient document preparation system is an important decision for any academic researcher.  
[Markus Knauff et Jelica Nejasmic, An Efficiency Comparison of Document Preparation Systems Used in Academic Research and Development](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0115069)

Je ne sais pas trop quoi penser de cette étude de 2014, qui semble à la fois sérieuse dans sa méthodologie et en même temps qui comporte quelques œillères : quand bien même un système de publication comme LaTeX pourrait donner quelques difficultés à son utilisateur ou le rendre un peu moins productif, est-ce qu'il ne répond pas à d'autres besoins (la liberté) et est-ce qu'il n'offre pas d'autres satisfactions (la maîtrise, la réduction d'une certaine forme de souffrance) ?
Et LaTeX n'est pas qu'un système de préparation de documents, contrairement à Word qui nécessite un retraitement pour pouvoir être publié !
La seule étude métrique d'un outil d'écriture est à mon humble avis une erreur de considération de nos désirs de raconter le monde.
