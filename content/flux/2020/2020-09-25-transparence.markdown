---
layout: post
title: "Transparence"
date: "2020-09-25T22:00:00"
published: true
description: "Le billet de Julie Brillet est le résumé d'une de ses interventions sur le sujet de la réduction de notre empreinte numérique. Je trouver que cette approche d'associer empreinte numérique et logiciel libre et collectif est très stimulante."
categories:
- flux
tags:
- privacy
---
> En synthèse, c’est un modèle qui repose avant tout sur la transparence : on a accès au code-source, c’est-à-dire à la recette de cuisine.  
[Julie Brillet, Réduire son empreinte numérique - mon intervention](https://juliebrillet.fr/blog/2020_09_ndw/)

Le billet de Julie Brillet est le résumé d'une de ses interventions sur le sujet de la réduction de notre empreinte numérique.
Je trouver que cette approche d'associer empreinte numérique et logiciel libre et collectif est très stimulante.
