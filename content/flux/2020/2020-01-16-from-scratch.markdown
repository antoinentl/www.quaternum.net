---
layout: post
title: "From scratch"
date: "2020-01-16T22:00:00"
comments: true
published: true
description: "Frank Chimero a commencé à refaire le design de son site web il y a quelques semaines, et à documenter cela. Très inspirant."
categories:
- flux
tags:
- design
---
> A quick note about the current “naked” design of this site: it is kept intentionally default-ish—no webfonts, no Javascript, and only a dash of vanilla CSS to constrain the body width and aid reading. I don’t intend to have my site’s redesign turn out like this; my thought is that the more boilerplate this blog feels, the easier it will be to see and consider the posted designs and sketches.  
[Frank Chimero, Redesign: On This Design](https://frankchimero.com/blog/2020/redesign-this-design/)

Frank Chimero a commencé à refaire le design de son site web il y a quelques semaines, et à documenter cela.
Très inspirant.
