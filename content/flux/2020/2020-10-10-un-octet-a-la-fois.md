---
layout: post
title: "Un octet à la fois"
date: 2020-10-10
published: true
description: "L'éducation populaire comme moyen d'atteindre une certaine littératie numérique."
categories:
- flux
tags:
- outils
---
> Le logiciel libre : s'approprier les technologies et les mettre au service de la construction d'un être humain autonome.  
> [Stéphane Crozat, Connaître les objets numériques, http://aswemay.fr/co/040010.html](http://aswemay.fr/co/040010.html)

Trois choses intéressantes ici :

- le rapport que nous entretenons avec les machines — ou plutôt cette quasi-absence — avec la mention de Gilbert Simondon ;
- l'éducation populaire comme moyen d'atteindre une certaine littératie numérique ;
- le logiciel libre comme condition de cet apprentissage.
