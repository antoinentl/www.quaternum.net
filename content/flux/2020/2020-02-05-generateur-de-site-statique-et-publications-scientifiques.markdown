---
layout: post
title: "Générateur de site statique et publications scientifiques"
date: "2020-02-05T22:00:00"
comments: true
published: true
description: "Un article sur l'usage des générateurs de site statique dans les domaines de la publication scientifique et des ressources universitaires, dont l'un des points de départ est Quire, la chaîne de publication de Getty Publications."
categories:
- flux
tags:
- publication
---
> Using open source static site generators for digital publishing is one option for avoiding future lock-in scenarios and supporting values of openness to research, education, and technology.  
[Chris Diaz, Using Static Site Generators for Scholarly Publications and Open Educational Resources](https://journal.code4lib.org/articles/13861)

Un article sur l'usage des générateurs de site statique dans les domaines de la publication scientifique et des ressources universitaires, dont l'un des points de départ est Quire, la chaîne de publication de Getty Publications.
Intéressant de voir que les principaux points soulignés sont les mêmes que ceux qui figurent dans [mon mémoire](https://memoire.quaternum.net).
