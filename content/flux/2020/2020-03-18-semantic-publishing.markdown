---
layout: post
title: "Semantic Publishing"
date: "2020-03-18T22:00:00"
comments: true
published: true
description: "Articles de fond très documenté et référencé sur la publication sémantique."
categories:
- flux
tags:
- publication
---
> Cet article analyse les enjeux du semantic publishing en contexte scientifique et examine sous un axe sémiotique les codes sources qui en sont le vecteur de propagation.  
[Gérald Kembellec, Semantic publishing, la sémantique dans la sémiotique des codes sources d’écrits d’écran scientifiques](https://lesenjeux.univ-grenoble-alpes.fr/2019/dossier/04-semantic-publishing-la-semantique-dans-la-semiotique-des-codes-sources-decrits-decran-scientifiques)

Articles de fond très documenté et référencé sur la publication sémantique.
(Que je vais de ce pas relire une seconde fois.)
