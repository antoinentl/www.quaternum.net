---
layout: post
title: "Spatialisation du web"
date: "2020-03-10T22:00:00"
comments: true
published: true
description: "L'espace du web est ce que nous en faisons, voici un rappel nécessaire de Nolwenn Maudet."
categories:
- flux
tags:
- web
---
> C'est assez étrange ces jours-ci de considérer une adresse sur le web comme un espace public.  
[Nolwenn Maudet, un web des espaces ou des individus](https://carnet.nolwennmaudet.com/home/individucentrification)

L'espace du web est ce que nous en faisons, voici un rappel nécessaire de Nolwenn Maudet.

> J'aimerais voir plus de ces espaces libres où nous n'interagissons pas en ligne en tant qu'individu, mais en tant que village, en tant que couple, en tant que groupe anonyme.
