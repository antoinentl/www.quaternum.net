---
layout: post
title: "Larry Tesler"
date: "2020-03-04T22:00:00"
comments: true
published: true
description: "J'avoue que je ne connaissais pas Larry Tesler, il laisse pourtant un héritage énorme dans le champ de l'écriture (numérique ?), bien au-delà du copier-coller comme le démontre Baptiste."
categories:
- flux
tags:
- design
---
> Larry Tesler (avec Peter Deutsch) inventa alors le curseur placé entre les caractères que l’on connaît aujourd’hui.  
[Baptiste, L’héritage de Larry Tesler au-delà du copier-coller](https://toutcequibouge.net/2020/02/lheritage-de-larry-tesler-au-dela-du-copier-coller/)

J'avoue que je ne connaissais pas Larry Tesler, il laisse pourtant un héritage énorme dans le champ de l'écriture (numérique ?), bien au-delà du copier-coller comme le démontre Baptiste.
