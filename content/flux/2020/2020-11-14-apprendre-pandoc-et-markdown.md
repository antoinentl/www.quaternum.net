---
layout: post
title: "Apprendre Pandoc et Markdown"
date: 2020-11-14
published: true
description: "Un tutoriel introductif à Pandoc et Markdown très bien fait, avec une introduction concise, pédagogue et efficace."
categories:
- flux
tags:
- publication
---
> Cette leçon vous apprendra les notions de base de Markdown, une syntaxe de balisage facile à lire et écrire, ainsi que Pandoc, un outil en ligne de commande qui permet de convertir du texte brut en différents types de fichiers bien formatés: PDF, .docx, HTML, LaTeX, diaporama, et plus encore.  
> Dennis Tenen et Grant Wythoff, Rédaction durable avec Pandoc et Markdown, [https://programminghistorian.org/fr/lecons/redaction-durable-avec-pandoc-et-markdown](https://programminghistorian.org/fr/lecons/redaction-durable-avec-pandoc-et-markdown)

Un tutoriel introductif à Pandoc et Markdown très bien fait, avec une introduction concise, pédagogue et efficace.

> Quand vous utilisez MS Word, Google Docs ou Open Office pour écrire des textes, vous ne voyez pas ce qui se passe en coulisses. Sous une surface de mots, phrases et paragraphes visibles à l’écran se cache une autre couche, de code celle-là, que seule la machine peut comprendre. À cause de cette couche de code dissimulée, vos documents en format .docx et .pdf dépendent de logiciels propriétaires pour être affichés correctement.

> Écrire ce cette façon libère l’auteur(e) de son outil. Vous pouvez écrire en Markdown dans n’importe quel éditeur de texte brut, et la syntaxe dispose d’un riche écosystème de logiciels qui peuvent transformer ces textes en de magnifiques documents. C’est pour cette raison que Markdown connaît actuellement une hausse de popularité, non seulement comme outil de rédaction d’articles scientifiques, mais aussi comme _norme_ pour l’édition en général.

(L'emphase est de moi.)

> Il est important de comprendre que Markdown n’est qu’une convention d’écriture. Les fichiers Markdown sont enregistrés en texte brut, ce qui contribue d’autant plus à la flexibilité de ce format. Les fichiers en texte brut existent depuis l’apparition des machines à écrire électroniques. La longévité de cette norme fait du texte brut un format intrinsèquement plus durable et plus stable que les formats propriétaires. Alors que des fichiers créés avec Microsoft Word et Apple Pages il y a à peine dix ans peuvent provoquer des difficultés importantes lorsqu’ils sont affichés dans une version plus récente de ces logiciels,
