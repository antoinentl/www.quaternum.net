---
layout: post
title: "Hors service"
date: "2020-01-06T22:00:00"
comments: true
published: true
description: "Critique argumentée d'un design supposé au service de tous."
categories:
- flux
tags:
- design
---
> Persister à considérer le design comme un pourvoyeur de services, c’est perpétuer l’idée que le designer est hors du monde. C’est entretenir la fausse dualité d’un rapport social caricatural, nécessairement réparti entre offre et demande, et sur laquelle se fonde la marchandisation de tout. Dans un monde de services il n’y a plus de femmes libres, plus d’hommes libres, plus de complexité, seulement des utilisateurs captifs qui attribuent des étoiles à leur expérience aliénée de la vie. On croit servir mais on (s’)asservit.  
[Design non éthique, Le design au service de personne.](https://medium.com/@designonethic/le-design-au-service-de-personne-b55cf6327a7e)

Critique argumentée d'un design supposé au service de tous.
(Mais toujours publié sur Medium, quelle tristesse dans la forme.)
