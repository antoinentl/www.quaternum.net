---
layout: post
title: "Et vint le CSS Print Community Group"
date: "2020-03-16T22:00:00"
comments: true
published: true
description: "C'est une grande nouvelle que l'ouverture de ce groupe autour des spécifications de CSS Print ! Ou comment s'organiser pour avancer sur la question de la fabrication de publications paginées et imprimables depuis un navigateur web."
categories:
- flux
tags:
- publication
---
> We are a community of users of CSS print, working together to gather use cases, help with specifications, and advocate for more and better implementations.  
[W3C Team, Call for Participation in CSS Print Community Group](https://www.w3.org/community/cssprint/2020/02/13/call-for-participation-in-css-print-community-group/)

C'est une grande nouvelle que l'ouverture de ce groupe autour des spécifications de CSS Print !
Ou comment s'organiser pour avancer sur la question de la fabrication de publications paginées et imprimables depuis un navigateur web.
(Et une petite émotion en repensant aux discussions avec Julie, Quentin, Raphaël ou Julien à ce sujet en 2017.)
