---
layout: post
title: "Déployer Eleventy avec GitLab CI"
date: "2020-02-18T22:00:00"
comments: true
published: true
description: "M'étant essayé au générateur de site statique Eleventy pendant quelques semaines, voici une méthode pour déployer un site web sans passer par des services qui assurent déploiement et hébergement."
categories:
- flux
tags:
- web
---
> Here's how to use GitLab CI to easily deploy a static website to a virtual private server.  
[Quentin Delcourt, Deploy an Eleventy website using GitLab CI](https://quentin.delcourt.be/blog/2020-02-10_eleventy-gitlab-ci-pipeline/)

M'étant essayé au générateur de site statique Eleventy pendant quelques semaines, voici une méthode pour déployer un site web sans passer par des services qui assurent déploiement _et_ hébergement.
Parce que même si Netlify ou Zeit sont bien pratiques (notamment avec des CDN), héberger chez soit peut-être pratique ou nécessaire.
