---
layout: post
title: "Les chiffres de la Jamstack"
date: "2020-12-10T22:00:00"
published: true
description: "Quelques chiffres sur les sites web produits avec la Jamstack, c'est-à-dire des générateurs de site statique et des architectures de développement web découplées."
categories:
- flux
tags:
- web
---
> The goals of this chapter are to estimate and analyze the growth of the Jamstack sites, the performance of popular Jamstack frameworks, as well as an analysis of real user experience using the Core Web Vitals metrics.  
> Ahmad Awais, Web Almanac Part III Chapter 17 Jamstack, [https://almanac.httparchive.org/en/2020/jamstack](https://almanac.httparchive.org/en/2020/jamstack)

Quelques chiffres sur les sites web produits avec la Jamstack, c'est-à-dire des générateurs de site statique et des architectures de développement web découplées.
Ces chiffres sont instructifs, et surtout ils sont désormais pris en compte, ce qui semble d'une certaine façon un peu étrange car ce n'est pas forcément mis en avant par les personnes qui travaillent sur ces sites.

Sans regarder les chiffres en détail, cela dit beaucoup de la façon dont le web est conçu, et le changement qui est en train de s'opérer.
