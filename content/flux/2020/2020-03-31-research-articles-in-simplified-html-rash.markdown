---
layout: post
title: "Research Articles in Simplified HTML (RASH)"
date: "2020-03-31T22:00:00"
comments: true
published: true
description: "Je suis toujours dubitatif face aux tentatives de création d'un énième langage tout en étant conscient que tordre l'existant n'est pas non plus forcément une bonne idée. L'idée originelle de RASH est intéressante, et la démarche complète."
categories:
- flux
tags:
- publication
---
> This paper introduces the Research Articles in Simplified HTML (or RASH), which is a Web-first format for writing HTML-based scholarly papers; it is accompanied by the RASH Framework, a set of tools for interacting with RASH-based articles.  
[Silvio Peroni​, Francesco Osborne, Angelo Di Iorio, Andrea Giovanni Nuzzolese, Research Articles in Simplified HTML: a Web-first format for HTML-based scholarly articles](https://peerj.com/articles/cs-132/)

Je suis toujours dubitatif face aux tentatives de création d'un énième langage tout en étant conscient que tordre l'existant n'est pas non plus forcément une bonne idée.
L'idée originelle de RASH est intéressante, et la démarche complète.

Via [Gérald Kembellec](https://twitter.com/kembellec).
