---
layout: post
title: "Asciidoctor + TEI"
date: "2020-01-20T22:00:00"
comments: true
published: true
description: "Asciidoctor est la suite de programmes permettant de transformer tout fichier au format AsciiDoc en d'autres formats plus ou moins classiques ou exotiques. Et porter Asciidoctor vers la TEI (Text Encoding Initiative) est assez exotique, et en même temps très utile pour convaincre de l'intérêt des langages de balisage léger !"
categories:
- flux
tags:
- publication
---
> An Asciidoctor converter for TEI.  
[Guillaume Grossetie et Thomas Parisot, asciidoctor-tei](https://github.com/Mogztter/asciidoctor-tei)

Asciidoctor est la suite de programmes permettant de transformer tout fichier au format AsciiDoc en d'autres formats plus ou moins classiques ou exotiques.
Et porter Asciidoctor vers la TEI (Text Encoding Initiative) est assez exotique, et en même temps très utile pour convaincre de l'intérêt des langages de balisage léger !
Merci à [Guillaume Grossetie](https://blog.yuzutech.fr/blog/) et à [Thomas Parisot](https://oncletom.io) pour l'initiative !
