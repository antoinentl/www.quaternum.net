---
layout: post
title: "Gemini : un protocole et un format"
date: 2020-11-29
published: true
description: "Timothée Goguely partage son expérience de design d'une publication pour la structure Design commun."
categories:
- flux
tags:
- web
---
> Gemini is a new, collaboratively designed internet protocol, which explores the space inbetween gopher and the web, striving to address (perceived) limitations of one while avoiding the (undeniable) pitfalls of the other.  
> Gemini, [https://gemini.circumlunar.space/](https://gemini.circumlunar.space/)

Un nouveau protocole et un nouveau format pour _publier_ des informations sur Internet : Gemini est une version simplifiée du Web, avec un protocole plus basique et un langage de description de l'information inspiré de Markdown (encore plus simple).

> Gemini est donc très simple. Il utilise :
>
> - Un protocole à lui, qui ressemble un peu à la version 0.9 de HTTP (en mieux, quand même),
> - Un format à lui, une sorte de version simplifiée et uniformisée de Markdown,
> - Les URL que nous connaissons,
> - Le TLS que nous connaissons.
>
> Stéphane Bortzmeyer, Le protocole Gemini, revenir à du simple et sûr pour distribuer l'information en ligne ?, [https://www.bortzmeyer.org/gemini.html](https://www.bortzmeyer.org/gemini.html)

Ce qui me semble intéressant, outre la limitation par défaut de ce système de publication, c'est l'articulation entre le protocole de communication et le langage de structuration de l'information.

Et si Gemini peut sembler une invention élitiste pour technophile, il me semble que c'est le type de critique qui a pu être faite envers les débuts du Web.
Les clients existants sont certes un peu limités en terme de rendu graphique, mais ce n'est qu'un début — et surtout cela dépend de ce que l'on veut faire avec, les choix graphiques étant du côté du client.
