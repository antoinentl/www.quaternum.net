---
layout: post
title: "Des spécifications continues"
date: "2020-03-27T22:00:00"
comments: true
published: true
description: "Je trouve cette approche très représentative du projet du web, en tant que chose vivante qui ne peut pas, par définition, être figée – pour le pire et le meilleur."
categories:
- flux
tags:
- web
---
> In other words, CSS as a whole doesn’t have levels anymore. It is instead a family of specifications (the modules), which individually acquire new levels as new features are added to them. At any moment in time, current CSS is defined as the set of all modules that are stable.  
[Bert Bos, ‘CSS X’](https://www.w3.org/blog/2020/03/css-x/)

Je trouve cette approche très représentative du projet du web, en tant que chose vivante qui ne peut pas, par définition, être figée – pour le pire et le meilleur.
Et ce bout de commentaire en réaction :

> CSS has now become a living document.  
[H.E.A.T.](https://www.w3.org/blog/2020/03/css-x/#comment-106014)
