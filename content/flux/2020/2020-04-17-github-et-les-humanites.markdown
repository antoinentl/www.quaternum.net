---
layout: post
title: "GitHub et les humanités"
date: "2020-04-17T22:00:00"
comments: true
published: true
description: "Interrogation légitime et bienvenue de l'usage de GitHub dans les humanités (numériques)."
categories:
- flux
tags:
- privacy
---
> What digital humanities researchers are adopting GitHub, and why? What are the benefits and risks of employing GitHub for digital humanities work?  
[Lisa Spiro, Evaluating GitHub as a Platform of Knowledge for the Humanities](https://dh2016.adho.org/abstracts/225)

Interrogation légitime et bienvenue de l'usage de GitHub dans les humanités (numériques).

La présentation complète suite à ce _draft_ est disponible [ici](https://digitalscholarship.files.wordpress.com/2016/07/spirosmithdh2016githubpresentationfinal.pdf).
