---
layout: post
title: "Simplifier la chaîne"
date: 2020-11-08
published: true
description: "Une lecture utile pour alimenter ma réflexion sur les fabriques simples et bien pensées, même si ce type d'article de billet manque d'appui théorique."
categories:
- flux
tags:
- publication
---
> As much as I like exploring new tools, I’ve come to really appreciate simplicity when it comes to my development workflow. By embracing static site generators, dropping Sass in favor of CSS, leveraging Hugo’s asset pipeline features, deploying with Netlify and augmenting my builds with Netlify Build Plugins, I’ve managed to significantly reduce the decisions needed to spin up a working environment and be productive in each development session. This gives me the ability to focus on delivering the best possible user experience by removing the complexity debt that often times becomes a focus.  
> Jon Yablonski, Simplified Development Workflow, [https://jonyablonski.com/articles/2020/simplified-development-workflow/](https://jonyablonski.com/articles/2020/simplified-development-workflow/)

Une lecture utile pour alimenter ma réflexion sur les fabriques simples et bien pensées, même si ce type ~~d'article~~ de billet manque d'appui théorique.
Préférer un cadre contraint mais compréhensible et puissant, quitte à ne plus suivre l'évolution future de cet outil monolithique qu'est une fabrique comme Hugo ?
(Encore faudrait-il définir précisément _monolithique_.)
