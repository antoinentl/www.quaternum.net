---
layout: post
title: "Déterminisme technologique"
date: 2020-11-02
published: true
description: "Long texte de Christophe Masutti sur les questions de déterminisme technologique à travers le développement de l'informatique, avec de nombreuses références (à creuser)."
categories:
- flux
tags:
- technique
---
> En somme, l’informatisation a été aussi considérée comme un mouvement capitaliste construit sur une concentration des pouvoirs et la diminution des contre-pouvoirs dans les institutions de l’économie.  
> Christophe Masutti, Déterminisme et choix technologiques : tentative de synthèse depuis les débuts de l'informatisation de la société, [https://golb.statium.link/post/20201021determinisme/](https://golb.statium.link/post/20201021determinisme/)

Long texte de Christophe Masutti sur les questions de déterminisme technologique à travers le développement de l'informatique, avec de nombreuses références (à creuser).
Christophe Masutti revient continuellement sur les thèses de Jacques Ellul et ses critiques concernant la technique et la technologie.
Cette analyse (qui est plus qu'un état de l'art) fort bien documentée est quelque peu déprimante, pour qui tente de chercher dans la technique une forme d'outil d'émancipation.
La dernière partie donne un peu d'espoir (même si ce n'est probablement pas l'objectif de ce texte), avec Internet et le logiciel libre comme _preuve_ que la technique peut se faire "réapproprier".

> Alors même que le mot « numérique » est sur toutes les lèvres pour promouvoir ce projet global de soumission à un ordre technologique capitaliste, Internet et le mouvement du logiciel libre démontrent depuis le milieu des années 1980 qu’il est possible de se réapproprier socialement la technique et la libérer des discours déterministes.
