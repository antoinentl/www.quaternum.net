---
layout: post
title: "Lieu de travail"
date: "2020-04-15T22:00:00"
comments: true
published: true
description: "Réflexion intéressante dans ce (un peu trop) beau documentaire, néanmoins le niveau de contrôle des travailleurs sur leur environnement devrait être au point de pouvoir le retourner sans dessus dessous, ce qui n'est pas vraiment ce que l'on peu observer dans ce film, chacun·e se conformant à un modèle malgré tout."
categories:
- flux
tags:
- métier
---
> I mean, some kind of work does involve lots of conversation and collaboration. And a lot of it just involves making phone calls, which no one else needs to hear or wants to or a lot of it just involves putting your head down and concentrating. The only way to find that out is actually by having workers controlling the process to some degree. That's just never been a really high priority for a lot of companies. And I think that's the only way you'd ever get a workspace that really reflects the variety of work and workers.  
[Nikil Saval, Workplace](https://www.ohyouprettythings.com/free)

Réflexion intéressante dans ce (un peu trop) beau documentaire, néanmoins le niveau de contrôle des travailleurs sur leur environnement devrait être au point de pouvoir le retourner sans dessus dessous, ce qui n'est pas vraiment ce que l'on peu observer dans ce film, chacun·e se conformant à un modèle malgré tout.

> I think that the office will be a feature of global capitalism for a long time to come.
