---
layout: post
title: "Impression à la demande"
date: "2020-12-05T22:00:00"
published: true
description: "Une présentation très (trop ?) axée sur la stratégie économico-financière mais qui a le mérite d'être assez complète."
categories:
- flux
tags:
- livre
---
> Le principale objectif [de l'impression à la demande] est de faire exister un livre dans un territoire où il n'y a pas d'inventaire et où les ventes sont généralement insuffisantes pour justifier un inventaire.  
[Stéphane Labbé, L’impression à la demande, un outil d’aide à l’exportation?](https://livrescanadabooks.com/fr/stu_events/limpression-a-la-demande-un-outil-daide-a-lexportation-2/)

Une présentation très (trop ?) axée sur la stratégie économico-financière mais qui a le mérite d'être assez complète.
