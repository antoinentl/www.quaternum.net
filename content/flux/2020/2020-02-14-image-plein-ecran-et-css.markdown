---
layout: post
title: "Image plein écran et CSS"
date: "2020-02-14T22:00:00"
comments: true
published: true
description: "Sylvain Durand propose une solution légère pour afficher des images en grand, en se passant de JavaScript. Parfait !"
categories:
- flux
tags:
- design
---
> Il n’existe pas de solution native au sein des navigateurs pour afficher, au clic, une image en plein écran. Plusieurs bibliothèques Javascript sont venues palier ce manque [...] Ces solutions sont parfois lourdes, alors que si l’on ne recherche qu’un comportement simple, quelques lignes de CSS suffisent, et aucun code Javascript n’est nécessaire.  
[Sylvain Durand, Overlay d’une image en CSS](https://www.sylvaindurand.fr/overlay-image-in-pure-css/)

Sylvain Durand propose une solution légère pour afficher des images en grand, en se passant de JavaScript.
Parfait !
