---
layout: post
title: "Scripter Gutenberg"
date: "2020-03-06T22:00:00"
comments: true
published: true
description: "Julie Blanc a organisé un workshop avec Lucile Haute autour des procédés numériques d'impression (le web to print), et elle donne accès à tous les travaux des étudiants. Une mine d'or ! (Et une initiative qui s'inscrit dans l'esprit de PrePostPrint.)"
categories:
- flux
tags:
- publication
---
> Du papier à l’écran, la dualité s’estompe tandis que les formes de publication migrent d’un support à l’autre et réciproquement. La composition fixe de la page imprimée rencontre l’interactivité et le flux liquide et adaptable des écrans.  
[Julie Blanc, Scripter Gutenberg: des publication de papier et d'écran](https://workshops.julie-blanc.fr/2020-esad-orleans/)

Julie Blanc a organisé un workshop avec Lucile Haute autour des procédés numériques d'impression (le web to print), et elle donne accès à tous les travaux des étudiants.
Une mine d'or !
(Et une initiative qui s'inscrit dans l'esprit de PrePostPrint.)
