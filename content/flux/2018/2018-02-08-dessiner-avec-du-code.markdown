---
layout: post
title: "Dessiner avec du code"
date: "2018-02-08T22:00:00"
comments: true
published: true
description: "Dessiner avec du code c'est possible ! Voici quelques exemples d'icônes en SVG."
categories:
- flux
tags:
- design
---
>One of the great things about scalable vector graphics (aside from their being infinitely scalable without quality loss) is that once you know the fundamentals you can hand-code simple shapes quite easily, without needing to open a graphics application.  
[Kezz Bracey, How to Hand Code SVG](https://webdesign.tutsplus.com/tutorials/how-to-hand-code-svg--cms-30368)

Dessiner avec du code c'est possible ! Voici quelques exemples d'icônes en SVG.
