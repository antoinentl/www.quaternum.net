---
layout: post
title: "Du design à la radio"
date: "2018-05-28T22:00:00"
comments: true
published: true
description: "Pour une fois qu'il est question de design à la radio ! Anthony Masure et Stéphane Buellet ont été les invités de Sylvain Bourmeau, et ça vaut le coup d'être écouté."
categories:
- flux
tags:
- design
---
>Et si la mutation numérique offrait l'occasion pour les sciences humaines et sociales, les sciences dites dures et l'art d'enfin se rencontrer.  
[Anthony Masure et Stéphane Buellet, Quand le design participe des humanités numériques](https://www.franceculture.fr/emissions/la-suite-dans-les-idees/la-suite-dans-les-idees-du-samedi-26-mai-2018)

Pour une fois qu'il est question de design à la radio ! [Anthony Masure](http://www.anthonymasure.com/) et [Stéphane Buellet](https://chevalvert.fr/) ont été les invités de Sylvain Bourmeau, et ça vaut le coup d'être écouté.
