---
layout: post
title: "Lancement de PagedMedia"
date: "2018-01-08T22:00:00"
comments: true
published: true
description: "PagedMedia, débuté il y a plusieurs mois, est donc lancé officiellement&nbsp;! Ce projet collectif de recherche, porté par la Collaborative Knowledge Foundation, vise à produire des publications paginées dans le navigateur et à générer des PDF pour l'impression à partir de ces publications numériques."
categories:
- flux
tags:
- publication
slug: lancement-de-paged-media
---
>PagedMedia is a project to educate and inspire publishers to use HTML, CSS and JavaScript to produce print and paginated electronic content. The project goal is to develop a suite of Javascripts to paginate HTML/CSS in the browser, and to apply PagedMedia controls to paginated content for the purposes of exporting print ready, or display friendly, PDF from the browser. This will be an Open Source initiative, appropriately licensed with the MIT license.  
[Collaborative Knowledge Foundation, PagedMedia Announcement](https://www.pagedmedia.org/book-production-with-css-paged-media-at-fire-and-lion/)

PagedMedia, débuté il y a plusieurs mois, est donc lancé officiellement&nbsp;! Ce projet collectif de recherche, porté par la [Collaborative Knowledge Foundation](https://coko.foundation/), vise à produire des publications paginées dans le navigateur et à générer des PDF pour l'impression à partir de ces publications numériques.
