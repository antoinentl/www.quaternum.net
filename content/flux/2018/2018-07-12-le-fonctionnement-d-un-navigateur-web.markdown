---
layout: post
title: "Le fonctionnement d'un navigateur web"
date: "2018-07-12T22:00:00"
comments: true
published: true
description: "Mozilla produit une série de vidéos pour démystifier le Web et son fonctionnement, en anglais et sous-titrées."
categories:
- flux
tags:
- web
slug: le-fonctionnement-d-un-navigateur-web
---
>Browsers are very complex pieces of software that have drastically evolved since the 90's, from rendering simple text to executing fully fledged applications.  
[Jeremie Patonnier, How do web browsers work?](https://www.youtube.com/watch?v=uE3UPEK26U0)

Mozilla produit une série de vidéos pour "démystifier" le Web et son fonctionnement, en anglais et sous-titrées.
