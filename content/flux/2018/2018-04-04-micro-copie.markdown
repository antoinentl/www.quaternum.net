---
layout: post
title: "Micro-copie"
date: "2018-04-05T22:00:00"
comments: true
published: true
description: "La micro-copie, ou l'art d'écrire des petits textes, ou comment rendre des formulations évidentes dans des situations complexes. Une bonne entrée en matière pour ce nouveau carnet sur l'UX lancé par Anne-Sophie."
categories:
- flux
tags:
- design
---
>Il existe des milliers de façons de présenter une même chose à l’utilisateur. Mais comment la formuler ? Quels mots choisir ? Quel ton utiliser ? Comment présenter le problème ou l’information ?  
[Anne-Sophie, La micro-copie : de l’importance du choix des mots](https://uxways.wordpress.com/2018/03/19/la-micro-copie-de-limportance-du-choix-des-mots/)

La micro-copie, ou "l'art d'écrire des petits textes", ou comment rendre des formulations évidentes dans des situations complexes. Une bonne entrée en matière pour [ce nouveau carnet sur l'UX](https://uxways.wordpress.com) lancé par Anne-Sophie.
