---
layout: post
title: "Publier des brouillons"
date: "2018-01-29T22:00:00"
comments: true
published: true
description: "Publier des articles encore à l'état de brouillon ? Très bonne idée pour faire circuler des idées encore en cours de _formalisation_. Une idée qui rejoint l'initiative de Marcello Vitali-Rosati&nbsp;: rendre public des articles qui ne sont pas encore relus et validés, dans le domaine de la recherche cette fois."
categories:
- flux
tags:
- publication
---
>I understand why print publications need cleaned-up stuff. And if I’m paying for articles, in print or not, I’d like them to be intelligent and edited. But the free blogging we do, who said it had to observe the same standards?  
[Florens Verschelde, Blogging with public drafts](https://fvsch.com/articles/public-drafts/)

Publier des articles encore à l'état de brouillon ? Très bonne idée pour faire circuler des idées encore en cours de _formalisation_. Une idée qui rejoint l'[initiative de Marcello Vitali-Rosati](http://blog.sens-public.org/marcellovitalirosati/en-cours-de-publication/)&nbsp;: rendre public des articles qui ne sont pas encore relus et validés, dans le domaine de la recherche cette fois.
