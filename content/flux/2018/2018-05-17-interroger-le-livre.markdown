---
layout: post
title: "Interroger le livre"
date: "2018-05-17T22:00:00"
comments: true
published: true
description: "Rebond suite au billet de Marc Jahjah ? Beau texte sur le rapport au livre d'un professeur en littérature (entre autre)."
categories:
- flux
tags:
- livre
---
>Je n’aime pas les livres [...].  
[René Audet, Du livre (notamment numérique)](http://carnets.contemporain.info/audet/archives/1412)

Rebond suite [au billet de Marc Jahjah](http://www.marcjahjah.net/2019-livre-pratique-definitionnelle) ? Beau texte sur le rapport au livre d'un professeur en littérature (entre autres).

>On devinera alors ce que je pense du livre numérique — les lieux de l’écrit évoluent, les méthodes de sa consignation et de sa circulation s’adaptent, les besoins liés à l’écologie du savoir se remodèlent constamment. Le livre s’y prête, revêt de nouveaux atours et s’offre en performance. Il importe d’aller voir ce qui advient de cette expérience dans les aires de la culture numérique, d’observer et de s’engager – du travail nous y attend.
