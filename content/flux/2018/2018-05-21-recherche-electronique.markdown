---
layout: post
title: "Recherche électronique"
date: "2018-05-21T22:00:00"
comments: true
published: true
description: "Voici un cas d'usage intéressant, et présenté avec beaucoup de précisions par Damien Petermann : l'usage de la liseuse dans une activité de recherche universitaire."
categories:
- flux
tags:
- ebook
---
>Pour résumer, dès lors que la mise en page n’est pas importante et que tout ce qui vous intéresse c’est lire du texte sans vous abîmer les yeux, je conseille de privilégier la liseuse électronique (encore plus dans le cas de longs documents).  
[Damien Petermann, Doctorant et liseuse électronique : un retour d’expérience](https://imagelyon.hypotheses.org/813)

Voici un cas d'usage intéressant, et présenté avec beaucoup de précisions par Damien Petermann : l'usage de la liseuse dans une activité de recherche universitaire.

Seule interrogation pour moi : comment lire un document sur un dispositif qui ne permet pas facilement d'écrire ou d'annoter ?
