---
layout: post
title: "Wikipédia et pédagogie"
date: "2018-06-12T22:00:00"
comments: true
published: true
description: "Compte rendu d'une conférence qui met en avant le rôle pédagogique que peut jouer Wikipédia dans différents contextes, en creux dans une démarche éditoriale."
categories:
- flux
tags:
- publication
---
>Les bibliothécaires pourraient proposer des cours sur le développement des compétences informationnelles durant lesquels Wikipédia servirait de support pédagogique. L’encyclopédie pourrait par exemple ouvrir une discussion autour de l’importance des sources et de leur référenciation, notamment lors de la rédaction de travaux universitaire. Un échange basé sur une mise en application concrète lors de création d’une page Wikipédia avec les étudiants. Cependant, les enjeux sont plus vastes que cela, car Wikipédia repésente une culture éditoriale unique de par l’horizontalité de ses pratiques.  
[Bruno Milia, Wikipédia, un ami au service d’une approche critique de la bibliothéconomie](https://tribuneci.wordpress.com/2018/05/17/wikipedia-un-ami-au-service-dune-approche-critique-de-la-bibliotheconomie/)

Compte rendu d'une conférence qui met en avant le rôle pédagogique que peut jouer Wikipédia dans différents contextes, en creux dans une démarche éditoriale.
