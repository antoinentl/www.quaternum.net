---
layout: post
title: "Sur l'informatique"
date: "2018-02-02T22:00:00"
comments: true
published: true
description: "Voici un essai critique et philosophie qui semble très engageant, à lire et à approfondir."
categories:
- flux
tags:
- livre
slug: sur-l-informatique
---
>L’information produite en si grande quantité et si bon marché est accaparée et mise à profit par les grands propriétaires du réseau. Les données générées par les masses connectées forment une marchandise spéciale, qui sert à écouler toutes les autres. L’analyse statistique de l’usage des interfaces offre une vue imprenable sur le comportement économique des masses, chose que la concurrence, jusqu’alors, ne permettait pas.  
[Véloce, Thèses sur l’informa­tique](http://www.lisez-veloce.fr/veloce-01/theses-sur-linformatique/)

Voici un essai critique et philosophie qui semble très engageant, à lire et à approfondir.

>À défaut d’avoir fait de toute activité une production de valeur, la grande victoire de l’informatique est d’être déjà parvenue à donner à tout effort humain qui l’emploie _la forme_ d’une production de valeur.
