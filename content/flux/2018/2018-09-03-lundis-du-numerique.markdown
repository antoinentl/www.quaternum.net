---
layout: post
title: "Les lundis du numérique de l'INHA"
date: "2018-09-03T22:00:00"
comments: true
published: true
description: "La nouvelle saison des lundis du numérique est annoncée, avec une petite conférence que je donnerai lundi 17 septembre 2018 à 18h sur le même sujet que mon mémoire, Antoine Courtin me donne une très belle occasion de venir présenter mon travail !"
categories:
- flux
tags:
- publication
slug: lundis-du-numerique
---
> Depuis la numérisation des outils  de conception et de production du livre dans les années 1980, le domaine de la publication connaît sans doute une évolution majeure avec l’influence des méthodes et des outils du développement web. Passant d’une chaîne à un système modulaire, abandonnant traitement de texte et logiciel de publication assisté par ordinateur, et intégrant désormais  le versionnement, des éditeurs pionniers déploient de nouvelles façons de fabriquer des livres, qu’ils soient imprimés,numériques ou web.  
[INHA, Les lundis du numérique 2018-2019](https://calenda.org/468480)

La nouvelle saison des lundis du numérique est annoncée, avec [une petite conférence](https://www.inha.fr/fr/agenda/parcourir-par-annee/en-2018/septembre-2018/les-lundis-numeriques-de-l-inha.html) que je donnerai lundi 17 septembre 2018 à 18h sur le même sujet que [mon mémoire](https://memoire.quaternum.net), Antoine Courtin me donne une très belle occasion de venir présenter mon travail ! Le programme 2018-2019 est très intéressant !
