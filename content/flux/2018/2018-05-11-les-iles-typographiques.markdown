---
layout: post
title: "Les îles typographiques"
date: "2018-05-11T22:00:00"
comments: true
published: true
description: "Un podcast pour le moins original, et néanmoins passionnant : des épisodes de moins de cinq minutes consacrés chacun à une typographie. C'est drôle et instructif."
categories:
- flux
tags:
- typographie
---
>L'Océan des Cent Typos est une chronique radio mensuelle qui nous plonge dans le monde surprenant de la typographie.  
[..]  
À chaque épisode, l’explorateur découvre une nouvelle île, et avec lui l’auditeur une nouvelle police de caractère.  
[Malo Malo, L’Océan des Cent Typos](http://malo-malo.com/PAGES/Ocean_des_cent_Typos.php)

[Un podcast](https://soundcloud.com/studio-malomalo) pour le moins original, et néanmoins passionnant : des épisodes de moins de cinq minutes consacrés chacun à une typographie. C'est drôle et instructif.

Via la lettre d'information du [Musée de l'imprimerie de Lyon](http://www.imprimerie.lyon.fr/imprimerie/).
