---
layout: post
title: "Documenter"
date: "2018-11-21T22:00:00"
comments: true
published: true
description: "Une présentation complète d'Antora, l'outil développé pour publier de la documentation."
categories:
- flux
tags:
- publication
---
> Antora is a static site generator designed to create documentation sites from AsciiDoc documents sourced from multiple git repositories.
In other words, you write content in AsciiDoc, feed it to Antora, and out comes a docs site.
Under the covers, Antora uses Asciidoctor to convert the AsciiDoc to HTML and enrich the content.
Although Asciidoctor plays an important role in the docs pipeline, Antora handles the big picture of publishing the site.  
[Dan Allen, Documentation Sites for Software Teams](https://opendevise-talks-docs-sites-for-software-teams.netlify.com/)

Une présentation complète d'Antora, l'outil développé pour publier de la documentation.
Les sources du support de présentation sont disponibles [en ligne](https://gitlab.com/opendevise/talks/docs-sites-for-software-teams).
