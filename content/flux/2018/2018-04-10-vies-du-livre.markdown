---
layout: post
title: "Vies du livre"
date: "2018-04-10T22:00:00"
comments: true
published: true
description: "Le nouveau livre de C&F Éditions parle de... livres ! Ou plutôt des vies du livre."
categories:
- flux
tags:
- livre
---
>Aimer un livre, c'est le prêter, l'offrir, le recommander, en parler, le commenter sur son blog, le présenter sur YouTube ou dans un cercle de lecture, et puis le poser sur un rayonnage ou le laisser quelque part, en quête d'une autre vie.  
[Mariannig Le Béchec, Dominique Boullier, Maxime Crépel, Vies du livre & pratiques des lecteurs](https://cfeditions.com/livre-echange/)

Le nouveau livre de [C&F Éditions](https://cfeditions.com) parle de... livres ! Ou plutôt des "vies du livre".
