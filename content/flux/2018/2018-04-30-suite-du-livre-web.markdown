---
layout: post
title: "Suite du livre web"
date: "2018-04-30T22:00:00"
comments: true
published: true
description: "Support de l'intervention d'Anthony Masure lors du colloque ÉCRIDIL 2018, et prolongement – en quelque sorte – de ma proposition lors d'ÉCRIDIL 2016, en plus ambitieux et plus abouti."
categories:
- flux
tags:
- publication
---
>[...} en quoi le Web, en tant que media de production et de diffusion, peut-il nous inciter à repenser la façon dont les savoirs sont élaborés et transmis ?  
[Anthony Masure, Mutations du livre Web](http://www.anthonymasure.com/conferences/2018-04-livre-web-ecridil-montreal)

Support de l'intervention d'Anthony Masure lors du colloque [ÉCRIDIL 2018](http://ecridil.ex-situ.info/), et prolongement – en quelque sorte – de [ma proposition](/2016/10/24/le-livre-web-une-autre-forme-du-livre-numerique/) lors d'ÉCRIDIL 2016, en plus ambitieux et plus abouti.
