---
layout: post
title: "Petite histoire de Python"
date: "2018-07-23T22:00:00"
comments: true
published: true
description: "Python, le langage informatique, a un succès relativement important notamment en raison de sa simplicité, et son histoire mérite d'être connue."
categories:
- flux
tags:
- design
---
>No computing language can ever be truly general purpose. Specialisation will necessarily remain important. It is nevertheless true that, in that long-past Yuletide, Mr Van Rossum started something memorable. He isn’t the Messiah, but he was a very clever boy.  
[The Economist, Python has brought computer programming to a vast new audience](https://www.economist.com/science-and-technology/2018/07/19/python-has-brought-computer-programming-to-a-vast-new-audience)

Python, le langage informatique, a un succès relativement important notamment en raison de sa simplicité, et son histoire mérite d'être connue.
