---
layout: post
title: "Documenter un projet avec les messages de Git"
date: "2018-01-22T22:00:00"
comments: true
published: true
description: "Quelques conseils sur le design des messages – des commits – dans Git, une réflexion qui concerne plus la façon dont on peut documenter un projet et communiquer que sur des aspects purement techniques."
categories:
- flux
tags:
- outils
---
>Our commit history has some very special properties which make it particularly useful for documenting intent.  
[...]  
So three principles. 1. Make atomic commits. 2. Write good commit messages and 3. Revise your development history before sharing.  
[Joel Chippindale, Telling stories through your commits](https://www.pagedmedia.org/book-production-with-css-paged-media-at-fire-and-lion/)

Quelques conseils sur le design des messages – des commits – dans Git, une réflexion qui concerne plus la façon dont on peut documenter un projet et communiquer que sur des aspects purement techniques.

Via [David](https://larlet.fr/david/stream/2018/01/19/).
