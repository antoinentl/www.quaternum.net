---
layout: post
title: "AMP Letter"
date: "2018-01-12T22:00:00"
comments: true
published: true
description: "Depuis plusieurs mois (années ?) les critiques envers les Accelerated Mobile Pages, un projet de Google, sont nombreuses."
categories:
- flux
tags:
- web
---
>The Web is not Google, and should not be just Google.  
[AMP Letter, A letter about Google AMP](http://ampletter.org/)

Depuis plusieurs mois (années ?) les critiques envers les "Accelerated Mobile Pages", un projet de Google, sont nombreuses. Cette lettre pointe deux aspects :

1. Tout contenu qui choisit de supporter AMP ainsi que l’hébergement affilié sous le domaine Google acquiert une promotion privilégiée dans les résultats de recherche, y compris (pour les articles) un placement au dessus des autres résultats.
2. Les utilisateurs et utilisatrices naviguant depuis Google vers un contenu recommandé par Google restent, sans le savoir, dans l’enceinte de l’écosystème Google.

Pour signer la lettre il faut soumettre une _Pull Request_ sur GitHub (non ce n'est pas la façon la plus inclusive de participer).
