---
layout: post
title: "Recherche contemporaine"
date: "2018-01-11T22:00:00"
comments: true
published: true
description: "Quelques réflexions de Marcello Vitali-Rosati sur la façon dont la recherche (universitaire) est impactée, en profondeur, par le numérique."
categories:
- flux
tags:
- publication
slug: publication
---
>Qu’est-ce que la recherche aujourd’hui? Comment « le numérique » a-t-il modifié le sens de nos pratiques de chercheurs en sciences humaines et sociales?  
[Marcello Vitali-Rosati, Réinventer la recherche à l’époque du numérique](http://blog.sens-public.org/marcellovitalirosati/reinventer-la-recherche-a-lepoque-du-numerique/)

Quelques réflexions de Marcello Vitali-Rosati sur la façon dont la recherche (universitaire) est impactée, en profondeur, par le _numérique_.

>Ces changements redéfinissent ce qu’est la recherche et donnent de nouveaux défis et de nouvelles responsabilités aux chercheurs. Serons-nous capables de relever ces défis et de prendre en charge ces responsabilités?
