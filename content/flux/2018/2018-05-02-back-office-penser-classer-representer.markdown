---
layout: post
title: "Back Office : Penser, classer, représenter"
date: "2018-05-02T22:00:00"
comments: true
published: true
description: "Le deuxième numéro de la revue Back Office – une revue entre design graphique et pratiques numériques – a donc comme thème Penser, classer, représenter, et une belle couleur saumon. La revue est toujours disponible en version imprimée chez tous les bons libraires, et en version numérique en ligne directement sur le site. La qualité du premier numéro était au rendez-vous, le sommaire semble encore de très bonne facture !"
categories:
- flux
tags:
- design
---
>Back Office #2 aborde les notions d’indexation, de tri et de représentation des données numériques.  
[Back Office, Penser, classer, représenter](http://www.revue-backoffice.com/numeros/02-penser-classer-representer)

Le deuxième numéro de la revue Back Office – une revue entre design graphique et pratiques numériques – a donc comme thème "Penser, classer, représenter", et une belle couleur saumon. La revue est toujours disponible en version imprimée chez tous les bons libraires, et en version numérique en ligne directement sur le site. La qualité du premier numéro était au rendez-vous, le sommaire semble encore de très bonne facture !
