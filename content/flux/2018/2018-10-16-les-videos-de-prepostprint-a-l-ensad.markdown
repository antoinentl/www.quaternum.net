---
layout: post
title: "Les vidéos de PrePostPrint à l'Ensad"
date: "2018-10-16T22:00:00"
comments: true
published: true
description: "Le troisième événement PrePostPrint s'est déroulé en avril 2018 à l'EnsadLab, avec plusieurs conférences et des workshops. Six mois plus tard les vidéos de la dizaine de conférences sont disponibles en ligne, ce qui est une très bonne nouvelle (notamment pour moi, puisque je devais assurer une petite conférence lors de ces deux jours, mais je n'ai pas pu me rendre à Paris) !"
categories:
- flux
tags:
- publication
slug: les-videos-de-prepostprint-a-l-ensad
---
> Dans le domaine de la publication numérique et imprimée, des éditeurs et des designers s’intéressent aux outils alternatifs de création graphique afin d’échapper aux écosystèmes propriétaires de production, distribution et consultation d’objets éditoriaux.  
[EnsadLab, PrePostPrint](http://www.ensadlab.fr/fr/francais-prepostprint/)

Le troisième événement [PrePostPrint](https://prepostprint.org/) s'est déroulé en avril 2018 à l'EnsadLab, avec plusieurs conférences et des workshops.
Six mois plus tard les vidéos de la dizaine de conférences sont disponibles en ligne, ce qui est une très bonne nouvelle (notamment pour moi, puisque je devais assurer une petite conférence lors de ces deux jours, mais je n'ai pas pu me rendre à Paris).
