---
layout: post
title: "L'initiative Paged Media"
date: "2018-06-07T22:00:00"
comments: true
published: true
description: "Paged Media est à la fois un site web regroupant des ressources, lancé il y a plus d'un an, et une initiative collective portée par la Coko Foundation qui a débutée début 2018. Dans ce billet Julie Blanc explique pour cette initiative est nécessaire, et où en sont les avancées."
categories:
- flux
tags:
- design
slug: l-initiative-paged-media
---
>If you have ever tried to lay out a website for printing or to publish a book in HTML, you’ll have experienced the limitations of styling meant for displaying scrolling text on screens. The Paged Media Initiative is developing tools which help make it possible to produce paginated material from your browser.  
So why is this all so difficult?  
[Julie Blanc, What is the Paged Media initiative?](https://www.pagedmedia.org/what-is-the-paged-media-initiative/)

Paged Media est à la fois un site web regroupant des ressources, lancé il y a plus d'un an, et une initiative collective portée par la Coko Foundation qui a débutée début 2018. Dans ce billet Julie Blanc explique pour cette initiative est nécessaire, et où en sont les avancées.
