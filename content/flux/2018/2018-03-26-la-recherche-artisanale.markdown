---
layout: post
title: "La recherche artisanale"
date: "2018-03-26T22:00:00"
comments: true
published: true
description: "Belle réflexion sur la dimension artisanale de la recherche."
categories:
- flux
tags:
- métier
---
>Nous ne savons pas bien comment travaillent nos collègues, lorsqu'ils écrivent un article par exemple : quels instruments (gomme, post-it, logiciels, etc.) et quels supports mobilisent-ils ? Comment rassemblent-ils la matière collectée dans un ensemble épars de documents ? Quelles transformations subissent leurs écrits, du surlignement au texte envoyé à une revue ?  
[Marc Jahjah, Le chercheur comme artisan](http://www.marcjahjah.net/1840-le-chercheur-comme-artisan)

Belle réflexion sur la dimension artisanale de la recherche.
