---
layout: post
title: "GitHub = Microsoft"
date: "2018-06-05T22:00:00"
comments: true
published: true
description: "Le rachat de GitHub Par Microsoft fait grand bruit dans le domaine du numérique, pourtant GitHub était déjà une entreprise privée avec une partie de son code fermé, et une plateforme centralisée pour des dépôts Git. C'est finalement une bonne nouvelle pour GitHub et ses employés, l'équilibre financier semblait précaire. Toutefois cela pose des questions sur le manque de remise en cause de la centralisation des projets open source ou libre : désormais c'est Microsoft qui possède la base la plus riche au monde."
categories:
- flux
tags:
- métier
---
>Now, of course, things are different. Git is far and away the most popular version control system, clouds are mostly computers, and Microsoft is the most active organization on GitHub in the world. Their VS Code project alone is beloved by millions of developers, entirely open source, and built using GitHub’s Electron platform. Beyond that, today major enterprises regularly embrace open source. The world has realized how important happy, productive developers really are. And also, people have smartphones now.  
[Chris Wanstrath, A bright future for GitHub](https://blog.github.com/2018-06-04-github-microsoft/)

Le rachat de GitHub Par Microsoft fait grand bruit dans le domaine du numérique, pourtant GitHub était déjà une entreprise privée avec une partie de son code fermé, et une plateforme centralisée pour des dépôts Git. C'est finalement une bonne nouvelle pour GitHub et ses employés, l'équilibre financier semblait précaire. Toutefois cela pose des questions sur le manque de remise en cause de la centralisation des projets open source ou libre : désormais c'est Microsoft qui possède la base la plus riche au monde. Et même si Microsoft fait aujourd'hui des choses intéressantes, ce n'est pas une bonne nouvelle.

Voir également [le billet sur le site de Microsoft](https://blogs.microsoft.com/blog/2018/06/04/microsoft-github-empowering-developers/), et le mouvement [#movingtogitlab](https://twitter.com/hashtag/movingtogitlab?f=tweets&vertical=default&src=hash) qui a suivi.
