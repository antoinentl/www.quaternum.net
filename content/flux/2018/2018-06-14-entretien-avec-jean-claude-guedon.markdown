---
layout: post
title: "Entretien avec Jean-Claude Guédon"
date: "2018-06-14T22:00:00"
comments: true
published: true
description: "Beaucoup d'inspirations du côté du développement informatique pour penser de nouvelles façons de faire de la science, de permettre la conversation scientifique."
categories:
- flux
tags:
- publication
---
>Repenser la revue scientifique nécessite en fait de penser sa dissolution. L’entretien avec Jean-Claude Guédon dans le cadre du projet de refonte de Sens Public a été l’occasion de filer la métaphore du cristal. Précipitation, dissolution, cristallisation, ces différents passages d’un état solide à un état liquide reflètent bien les états du texte et les processus de stabilisation et déstabilisation du texte à l’œuvre.  
[Nicolas Sauret, Entretien avec Jean-Claude Guédon : on «crystal of knowledge»](http://nicolassauret.net/carnet/2017/04/12/entretien-avec-jean-claude-guedon-on-crystal-of-knowledge/)

Beaucoup d'inspirations du côté du développement informatique pour penser de nouvelles façons de faire de la science, de permettre la conversation scientifique.

>Pour J-C. Guédon, la revue et l’article ne sont plus les bons objets et les bons formats pour nourrir la conversation scientifique. Il faut selon lui remettre au centre la communication et non le texte. Le texte doit rester prétexte à la discussion scientifique.
