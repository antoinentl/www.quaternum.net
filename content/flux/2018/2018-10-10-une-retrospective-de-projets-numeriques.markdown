---
layout: post
title: "Une rétrospective de projets numériques"
date: "2018-10-10T22:00:00"
comments: true
published: true
description: "Nicolas Taffin fait le bilan de six années de designer au sein de l'équipe d'Internet Memory Research : des beaux projets et un recul impressionnant."
categories:
- flux
tags:
- design
---
> J’ai aussi, je crois, compris que pour réussir, il faut commencer tôt, mais surtout pas trop, pas avant qu’une question ne soit actuelle.  
[Nicolas Taffin, C’était demain, ou 6 années dans l’open space](https://polylogue.org/cetait-demain-ou-6-ans-dans-lopen-space/)

Nicolas Taffin fait le bilan de six années de designer au sein de l'équipe d'Internet Memory Research : des beaux projets (vraiment) et un recul éclairé.
