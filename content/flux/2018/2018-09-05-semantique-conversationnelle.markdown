---
layout: post
title: "Sémantique conversationnelle"
date: "2018-09-05T22:00:00"
comments: true
published: true
description: "Comment utiliser la sémantique – structurer un texte en attribuant des attributs spécifiques à chaque fragment – pour qu'une synthèse vocale puisse l'interpréter convenablement ?"
categories:
- flux
tags:
- design
---
> As Alexa, Cortana, Siri, and even customer support chat bots become the norm, we have to start carefully considering not only how our content looks but how it could sound. We can—and should—use HTML and ARIA to make our content structured, sensible, and most importantly, meaningful.  
[Aaron Gustafson, Conversational Semantics](https://alistapart.com/article/conversational-semantics)

Comment utiliser la sémantique – structurer un texte en attribuant des attributs spécifiques à chaque fragment – pour qu'une synthèse vocale puisse l'interpréter convenablement ?
