---
layout: post
title: "Paginer le flux par Julie Blanc"
date: "2018-08-31T22:00:00"
comments: true
published: true
description: "Julie Blanc, avec qui j'ai l'honneur de travailler sur différents projets, a présenté ses travaux pendant les Rencontres internationales de Lure, ainsi que des projets collectifs comme PrePostPrint ou paged.js. À Lire !"
categories:
- flux
tags:
- publication
---
> L'imprimé est le dernier endroit où il manque une évolution pour que ces méthodes d'organisation de la chaîne éditoriale soient effectives (et réellement transformées).  
[Julie Blanc, Paginer le flux](http://recherche.julie-blanc.fr/revealjs/20180820_Lure.html)

Julie Blanc, avec qui j'ai l'honneur de travailler sur différents projets, a présenté ses travaux pendant les Rencontres internationales de Lure, ainsi que des projets collectifs comme PrePostPrint ou paged.js.
À Lire !
