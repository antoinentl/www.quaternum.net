---
layout: post
title: "L'open source dans le domaine de la publication"
date: "2018-10-08T22:00:00"
comments: true
published: true
description: "Bel exemple de ce qu'est et ce que n'est pas l'_open source_, et plus particulièrement dans le domaine de la publication (universitaire), par le co-fondateur de Coko, Adam Hyde."
categories:
- flux
tags:
- publication
slug: l-open-source-dans-le-domaine-de-la-publication
---
> Do I have to have a software team to use open source?  
> No, you don’t. In the publishing sector, there have been suggestions that you need a development team in order to merely use, let alone deploy, open source solutions. This argument is made to (incorrectly) argue for the apparent ease of proprietary vendor offerings that sell the software and hosting in a single bundle as opposed to ‘more difficult’ open source offerings. This is misleading, as open source is not only for the technically gifted.  
[Adam Hyde, Guest Post: Open Source and Scholarly Publishing](https://scholarlykitchen.sspnet.org/2018/09/06/guest-post-open-source-and-scholarly-publishing/)

Bel exemple de ce qu'est et ce que n'est pas l'_open source_, et plus particulièrement dans le domaine de la publication (universitaire), par le co-fondateur de [Coko](https://coko.foundation/), [Adam Hyde](https://www.adamhyde.net/).
