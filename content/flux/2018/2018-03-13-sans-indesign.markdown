---
layout: post
title: "Sans InDesign"
date: "2018-03-13T22:00:00"
comments: true
published: true
description: "J'étais passé à côté de ce court billet d'Arthur Attwell de Fire and Lion."
categories:
- flux
tags:
- publication
---
>We don’t do page-by-page layout and convert to HTML later. In fact, we do exactly the opposite: we make each book as a little website, and then output to PDF.  
[Arthur Attwell, I love you, InDesign, but it’s time to let you go](https://fireandlion.com/thinking/2017/05/15/i-love-you-indesign-but/)

J'étais passé à côté de ce court billet d'Arthur Attwell de Fire and Lion.
