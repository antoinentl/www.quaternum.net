---
layout: post
title: "Comment lire le Web"
date: "2018-01-30T22:00:00"
comments: true
published: true
description: "Robin Rendle livre ici un beau plaidoyer pour les flux RSS : une alternative aux flux biaisés des réseaux sociaux, un îlot d'indépendance dans un Web de plus en plus centralisé, un espace sans publicité, un temps de lecture hors connexion, etc. Bonus : Robin présente son système pour lire sur plusieurs supports et avec confort."
categories:
- flux
tags:
- publication
---
>Before newsletters and social networks there was RSS, a tool that helped us keep up to date with our favorite websites.  
[Robin Rendle, How to Read the Internet](https://robinrendle.com/notes/how-to-read-the-internet/)

Robin Rendle livre ici un beau plaidoyer pour les flux RSS&nbsp;: une alternative aux flux biaisés des réseaux sociaux, un îlot d'indépendance dans un Web de plus en plus centralisé, un espace sans publicité, un temps de lecture hors connexion, etc. Bonus&nbsp;: Robin présente son système pour lire sur plusieurs supports et avec confort.
