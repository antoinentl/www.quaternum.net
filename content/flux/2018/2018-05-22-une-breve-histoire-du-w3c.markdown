---
layout: post
title: "Une brève histoire du W3C"
date: "2018-05-22T22:00:00"
comments: true
published: true
description: "Coralie Mercier et Jean-François Abramatic proposent une plongée dans le fonctionnement du W3C : son origine, son fonctionnement, ses objectifs."
categories:
- flux
tags:
- web
---
>La variété des acteurs du Web rend la recherche de consensus complexe et difficile. W3C n’a aucun pouvoir pour imposer le déploiement des standards issus de ses travaux. Son succès est donc venu de la mise en œuvre volontaire par les acteurs du Web des standards conçus au W3C.
[Jean-François Abramatic et Coralie Mercier, W3C invente la nouvelle manière de concevoir les standards du numérique](http://binaire.blog.lemonde.fr/2018/05/10/w3c-invente-la-nouvelle-maniere-de-concevoir-les-standards-du-numerique/)

Coralie Mercier et Jean-François Abramatic proposent une plongée dans le fonctionnement du W3C : son origine, son fonctionnement, ses objectifs.
