---
layout: post
title: "Vers le hors ligne"
date: "2018-04-16T22:00:00"
comments: true
published: true
description: "Un livre complet sur les techniques permettant de rendre un site web consultable hors ligne – la gestion des Service Workers. Bientôt disponible chez A Book Apart et écrit par Jeremy Keith."
categories:
- flux
tags:
- offline
---
>Design an ideal offline experience with Service Workers.  
[Jeremy Keith, Going Offline](https://abookapart.com/products/going-offline)

Un livre complet sur les techniques permettant de rendre un site web consultable hors ligne – la gestion des Service Workers. Bientôt disponible chez A Book Apart et écrit par Jeremy Keith.
