---
layout: post
title: "Notes sur le livre numérique"
date: "2018-11-07T22:00:00"
comments: true
published: true
description: "Pour le deuxième colloque ÉCRIDIL qui s'est déroulé en avril 2018 à Montréal, un livre a été écrit pendant un booksprint de deux jours. Le livre est disponible en imprimé et en numérique, sous licence libre, et les sources sont déposés sur Framagit."
categories:
- flux
tags:
- livre
---
> Qu’est-ce qu’un livre ? Comment est-il appelé à se transformer en contexte numérique ? En mai 2018, des chercheurs et des praticiens de tous horizons (design, édition, communication, littérature...) se sont réunis à Montréal lors du colloque ÉCRIDIL afin de dresser un état des lieux et proposer des pistes de réponse.  
[René Audet, Version 0. Notes sur le livre numérique](http://www.fabula.org/actualites/version-0-notes-sur-le-livre-numerique_87566.php)

Pour le deuxième colloque ÉCRIDIL qui s'est déroulé en avril 2018 à Montréal, un livre a été écrit pendant un _booksprint_ de deux jours.
Le livre est disponible en imprimé et en numérique, sous licence libre, et les sources sont déposés [sur Framagit](https://framagit.org/ecrinum/ecridil-booksprint).
