---
layout: post
title: "Une chaîne de développement web pour fabriquer un livre"
date: "2018-09-07T22:00:00"
comments: true
published: true
description: "Un nouvel exemple d'utilisation d'outils du développement web pour fabriquer un livre (imprimé) !"
categories:
- flux
tags:
- publication
---
> How we use our web-development workflow to make a book.  
[Friedrich Schultheiß, Headless CMS for a Printed Pizza Book](https://medium.com/buro-int/headless-cms-for-a-printed-pizza-book-54b39827e651)

Un nouvel exemple d'utilisation d'outils du développement web pour fabriquer un livre (imprimé) !

Via [Frank](https://frank.taillandier.me/).
