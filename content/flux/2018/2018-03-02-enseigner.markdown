---
layout: post
title: "Enseigner"
date: "2018-03-02T22:00:00"
comments: true
published: true
description: "Comment passer d'apprenant à enseignant, sans concession (et en privilégiant le sens aux outils)."
categories:
- flux
tags:
- métier
---
>Si apprendre, c’est devenir libre, alors apprendre à d’autres à apprendre, c’est favoriser les conditions d’émergence de cette liberté.  
[Arthur, Épisode VIII : Les Derniers Écriveurs](https://arthurperret.fr/2018/02/16/episode-viii-les-derniers-ecriveurs/)

Comment passer d'apprenant à enseignant, sans concession (et en privilégiant le sens aux outils).
