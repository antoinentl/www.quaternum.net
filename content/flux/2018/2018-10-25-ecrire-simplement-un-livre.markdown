---
layout: post
title: "Écrire (simplement) un livre"
date: "2018-10-25T22:00:00"
comments: true
published: true
description: "Thorsten Ball décrit le processus qu'il a mis en place pour écrire ses livres, à base de Markdown et de Pandoc, entre autres. Où la simplicité est probablement la meilleure solution !"
categories:
- flux
tags:
- publication
---
> In the beginning, there is always a single text file, nothing more.  
[Thorsten Ball, The Tools I Use To Write Books](https://thorstenball.com/blog/2018/09/04/the-tools-i-use-to-write-books/)

Thorsten Ball décrit le processus qu'il a mis en place pour écrire ses livres, à base de Markdown et de Pandoc, entre autres.
Où la simplicité est probablement la meilleure solution !
