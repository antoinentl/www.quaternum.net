---
layout: post
title: "La consommation numérique"
date: "2018-05-29T22:00:00"
comments: true
published: true
description: "Il est nécessaire de rappeler que le numérique – à la fois les infrastructures et les dispositifs – consomment beaucoup d'énergie, et trop. Pourtant il y a des solutions pour réduire cette consommation excessive."
categories:
- flux
tags:
- métier
---
>Environ 30 % de cette consommation électrique est imputable aux équipements terminaux – ordinateurs, téléphones, objets connectés –, 30 % aux data centers qui hébergent nos données et, plus surprenant, 40 % de la consommation est liée aux réseaux, les fameuses « autoroutes de l‘information ».  
[Laure Calloce, Numérique : le grand gâchis énergétique](https://lejournal.cnrs.fr/articles/numerique-le-grand-gachis-energetique)

Il est nécessaire de rappeler que le numérique – à la fois les infrastructures et les dispositifs – consomment beaucoup d'énergie, et trop. Pourtant il y a des solutions pour réduire cette consommation excessive.
