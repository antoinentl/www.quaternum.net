---
layout: post
title: "The Front-End Checklist"
date: "2018-09-10T22:00:00"
comments: true
published: true
description: "Une checklist bien utile pour tout projet web digne de ce nom, et pour tout développeur minutieux."
categories:
- flux
tags:
- web
---
> The Front-End Checklist Application is perfect for modern websites and meticulous developers!  
[David Dias, The Front-End Checklist](https://frontendchecklist.io/)

Une _checklist_ bien utile pour tout projet web digne de ce nom, et pour tout développeur "minutieux".

Probablement via [la Web Development Reading List](https://wdrl.info/).
