---
layout: post
title: "Trop d'interactions"
date: "2018-03-14T22:00:00"
comments: true
published: true
description: "Un billet qui fait du bien ! Le Web ne doit pas nécessairement clignoter et bouger dans tous les sens pour être intéressant, un peu de simplicité est une bonne chose."
categories:
- flux
tags:
- design
slug: trop-d-interactions
---
>All this to say: you journalists, with your fancy interactive stories and animation libraries, are making me sick.  
[Eileen Webb, Your Interactive Makes Me Sick](https://source.opennews.org/articles/motion-sick/)

Un billet qui fait du bien ! Le Web ne doit pas nécessairement clignoter et bouger dans tous les sens pour être intéressant, un peu de simplicité est une bonne chose. Avec des exemples et des contre exemples !

Via [Véronique](https://twitter.com/webetcaetera/status/973936798706540544).
