---
layout: post
title: "L'émergence du sens"
date: "2018-10-29T22:00:00"
comments: true
published: true
description: "Une nouvelle définition de l'éditorialisation par Marcello Vitali-Rosati à l'occasion du colloque du CRIHN."
categories:
- flux
tags:
- publication
slug: l-emergence-du-sens
---
> L’éditorialisation est l’ensemble des dynamiques qui constituent l’espace numérique et qui permettent à partir de cette constitution l’émergence du sens. Ces dynamiques sont le résultat de forces et d’actions différentes qui déterminent après-coup l’apparition et l’identification d’objets particuliers (personnes, communautés, algorithmes, plateformes…).  
[Marcello Vitali-Rosati, Pour une pensée préhumaine](http://blog.sens-public.org/marcellovitalirosati/pour-une-pensee-prehumaine/)

Une nouvelle définition de l'éditorialisation par Marcello Vitali-Rosati à l'occasion du colloque du CRIHN.
