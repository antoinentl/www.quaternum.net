---
layout: post
title: "Les connaissances de GitHub"
date: "2018-02-21T22:00:00"
comments: true
published: true
description: "GitHub est l'espace d'hébergement et de partage de code le plus utilisé, regroupant aussi les problèmes et les bugs liés aux codes source des programmes présents sur cette plateforme. David Humphrey s'inquiète de la domination de GitHub – il y a d'autres plateformes comme GitLab ou Bitbucket –, mais aussi du manque de suggestion de GitHub qui pourrait trouver des solutions à un bug d'un dépôt sur un autre dépôt – là je ne sais pas trop quoi penser d'une telle fonctionnalité."
categories:
- flux
tags:
- outils
---
>Why not suggest some alternatives to balance the load?  
[David Humphrey, GitHub Knows](https://blog.humphd.org/github-knows/)

GitHub est l'espace d'hébergement et de partage de code le plus utilisé, regroupant aussi les problèmes et les bugs liés aux codes source des programmes présents sur cette plateforme. David Humphrey s'inquiète de la domination de GitHub – il y a d'autres plateformes comme GitLab ou Bitbucket –, mais aussi du manque de suggestion de GitHub qui pourrait trouver des solutions à un bug d'un dépôt sur un autre dépôt – là je ne sais pas trop quoi penser d'une telle fonctionnalité.
