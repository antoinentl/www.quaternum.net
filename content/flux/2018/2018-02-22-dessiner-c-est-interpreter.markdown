---
layout: post
title: "Dessiner c'est interpréter"
date: "2018-02-22T22:00:00"
comments: true
published: true
description: "Dessiner c'est interpréter. Le dessin oblige à interpréter, alors que la photographie demande de la maîtrise avant de pouvoir refléter ce type d'interprétation. Je crois que c'est l'une des raisons principales de ma pratique du dessin depuis une dizaine d'années."
categories:
- flux
tags:
- design
slug: dessiner-c-est-interpreter
---
>So when you draw something you are not simply recording the outside world. You are drawing your personal interpretation!  
[Ralph Ammer, Stop taking pictures and start drawing!](https://medium.com/personal-growth/stop-taking-pictures-and-start-drawing-b1642aded2b6)

Dessiner c'est interpréter. Le dessin oblige à interpréter, alors que la photographie demande de la maîtrise avant de pouvoir refléter ce type d'interprétation. Je crois que c'est l'une des raisons principales de ma pratique du dessin depuis une dizaine d'années.

Via [Karl](http://www.la-grange.net/) (et quelques rebonds).
