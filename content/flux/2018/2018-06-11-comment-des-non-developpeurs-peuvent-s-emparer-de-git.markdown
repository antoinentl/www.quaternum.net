---
layout: post
title: "Comment des non-développeurs peuvent s'emparer de Git ?"
date: "2018-06-11T22:00:00"
comments: true
published: true
description: "William Chia travaille chez GitLab, et en tant que développeur il est amené à utiliser Git. Mais il s'est posé une question : comment permettre à des non-développeurs de s'emparer de Git ? Cette présentation, dans le cadre de Git Merge 2018, est très intéressante car elle met en lumière les défauts de certains procédés – notamment la limite des éditeurs en ligne – et la façon de simplifier des procédés techniquement complexes mais conceptuellement accessibles. Et oui c'est aussi de la publicité pour GitLab..."
categories:
- flux
tags:
- outils
slug: comment-des-non-developpeurs-peuvent-s-emparer-de-git
---
>- something to version
>    - Markdown
>- somewhere to publish
>    - Static Site Generator
>- some way to stop explosions
>    - CI/CD
>
>[William Chia, Empowering non-developers to use Git](https://www.youtube.com/watch?v=pY5i0Io86UQ)

William Chia travaille chez GitLab, et en tant que développeur il est amené à utiliser Git. Il s'est posé la question suivante : comment permettre à des non-développeurs de s'emparer de Git ? Cette présentation, dans le cadre de Git Merge 2018, est très intéressante car elle met en lumière les défauts de certains procédés – notamment la limite des éditeurs en ligne – et la façon de simplifier des procédés techniquement complexes mais conceptuellement accessibles. Et oui c'est aussi de la publicité pour GitLab...

Le support de présentation est [disponible en ligne](https://williamchia.gitlab.io/git-merge-2018/) (appuyer sur S pour afficher les notes), ainsi que [le dépôt](https://gitlab.com/williamchia/git-merge-2018/tree/master) de ce support.
