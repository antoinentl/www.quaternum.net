---
layout: post
title: "La bourse ou la vie (dans la recherche)"
date: "2018-09-28T22:00:00"
comments: true
published: true
description: "Ça semble (enfin) bouger."
categories:
- flux
tags:
- recherche
---
> Les conseils de recherche de onze pays européens, dont la France et le Royaume-Uni, prennent les grands moyens pour endiguer la soif de profits des géants de l’édition scientifique, qui siphonnent les budgets des bibliothèques universitaires.  
[...]  
Comme Le Devoir l’a rapporté au cours de l’été, les frais d’abonnement aux magazines scientifiques accaparent désormais 73 % des budgets d’acquisition des bibliothèques universitaires.  
[Marco Fortier, Offensive contre les géants de l’édition scientifique en Europe](https://www.ledevoir.com/societe/education/536595/offensive-europeenne-contre-les-geants-de-l-edition-scientifique)

Ça semble (enfin) bouger.
