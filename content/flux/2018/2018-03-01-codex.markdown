---
layout: post
title: "Codex"
date: "2018-03-01T22:00:00"
comments: true
published: true
description: "Les sources de CodeX - 01: PrePostPrint ont été mises ligne ! Ce journal a été réalisé à l'occasion du  salon de l’édition alternative organisé par PrePostPrint à La Gaîté lyrique en octobre 2017. Si vous souhaitez découvrir les contenus de ce journal – des articles sur les chaînes de publication, des présentations de projet – je vous recommande la version imprimée. Si vous voulez comprendre comment ce journal a été conçu (HTML to print) alors ce dépôt est pour vous !"
categories:
- flux
tags:
- livre
---
>"Code X" est un journal publié à l'occasion du salon de l’édition alternative organisé par PrePostPrint à La Gaîté Lyrique, le 21 octobre 2017.  
Le journal a été réalisé en HTML et CSS et le PDF a été généré depuis la boîte d’impression de Google Chrome.  
[Julie Blanc, CodeX - 01: PrePostPrint](https://gitlab.com/prepostprint/code-x-01)

Les sources de _CodeX - 01: PrePostPrint_ ont été mises ligne ! Ce journal a été réalisé à l'occasion du  salon de l’édition alternative organisé par [PrePostPrint](https://prepostprint.org/) à La Gaîté lyrique en octobre 2017. Si vous souhaitez découvrir les contenus de ce journal – des articles sur les chaînes de publication, des présentations de projet – je vous recommande [la version imprimée](http://www.editions-hyx.com/fr/code-x). Si vous voulez comprendre comment ce journal a été conçu (HTML to print) alors ce dépôt est pour vous ! Bravo à [Julie](http://julie-blanc.fr/) et [Quentin](http://www.juhel-quentin.fr/) pour le travail accompli !
