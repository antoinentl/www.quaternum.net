---
layout: post
title: "Le flux à Lure"
date: "2018-05-07T22:00:00"
comments: true
published: true
description: "La 66e édition des Rencontres internationales de Lure se déroulera à Lurs du dimanche 19 au samedi 25 août 2018, et ce thème du flux est encore plus intriguant que ceux des années précédentes. Le programme sera dévoilé le 15 mai !"
categories:
- flux
tags:
- design
---
>Le plomb a fondu, l’espace de la page s’est liquéfié, on écrit comme on parle. Les yeux dans les écrans, on vit à flux tendu. Il n’y a ni pause, ni forme fixe, ni frontière. Dans les méandres ou les torrents d’informations, nous réapprenons à canaliser les données, l’énergie, l’émotion, sans les fixer. Les designers classent, organisent les circulations et balisent les trajets.  
[Les Rencontres internationales de Lure, À flux détendu](http://delure.org/les-rencontres/rencontres-precedentes/rencontres-2018)

La 66e édition des Rencontres internationales de Lure se déroulera à Lurs du dimanche 19 au samedi 25 août 2018, et ce thème du flux est encore plus intriguant que ceux des années précédentes. Le programme sera dévoilé le 15 mai !
