---
layout: post
title: "Bilan du Web sémantique"
date: "2018-10-12T22:00:00"
comments: true
published: true
description: "Vaste sujet que le Web sémantique. Gautier Poupeau réalise un bilan très complet – et sans complaisance – du Web sémantique, entre projection et réalisation. Impressionnant de lucidité."
categories:
- flux
tags:
- web
---
> Alors, peut-être pour la dernière fois sur ce blog, je vous propose une plongée dans les entrailles des technologies du Web sémantique pour étudier quels en sont finalement les apports et les limites et l’écart entre les promesses et la réalité.  
[Gautier Poupeau, Les technos du Web sémantique ont-elles tenu leurs promesses ?](http://www.lespetitescases.net/les-technos-du-web-semantique-ont-elles-tenu-leurs-promesses)

Vaste sujet que le Web sémantique.
Gautier Poupeau réalise un bilan très complet – et sans complaisance – du Web sémantique en quatre billets, entre projection et réalisation.
Impressionnant de lucidité.

> Le traitement technique de données demande à un moment ou un autre de recentraliser les données pour en permettre l’exploitation quel que soit l’algorithme.
