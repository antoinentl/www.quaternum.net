---
layout: post
title: "Nomenclature des couleurs"
date: "2018-10-05T22:00:00"
comments: true
published: true
description: "Magnifique version numérique de la nomenclature des couleurs de Werner, un livre de P. Syme. Un travail de design incroyable !"
categories:
- flux
tags:
- design
---
> A recreation of the original 1821 color guidebook with new cross references, photographic examples, and posters designed by Nicholas Rougeux.
[Nicholas Rougeux, Werner’s Nomenclature of Colours By P. Syme](https://www.c82.net/werner/)

Magnifique version numérique de la nomenclature des couleurs de Werner, un livre de P. Syme.
Un travail de design incroyable !
