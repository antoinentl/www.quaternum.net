---
layout: post
title: "Aux origines de WordPress"
date: "2018-04-12T22:00:00"
comments: true
published: true
description: "Jay Hoffmann continue sa formidable histoire du Web avec ce chapitre consacré au système de gestion de contenu WordPress, très emblématique de ce que le Web a construit – tant au niveau de la constitution de communautés, que de développement technique ou encore de licence d'utilisation."
categories:
- flux
tags:
- publication
---
>In the meantime, WordPress developers focused on making things easy for users. They set up documentation and forums for users to post questions. They plucked new features straight from user requests, or Valdrighi’s wish list. WordPress was easy to install (in 5 minutes or less, the project promised) and had a unique admin. The goal was to make it as easy as possible to log into your site and post to your blog without ever having to see any code.  
[The History of the Web, The Story of WordPress](https://thehistoryoftheweb.com/the-story-of-wordpress/)

Jay Hoffmann continue sa formidable histoire du Web avec ce chapitre consacré au système de gestion de contenu WordPress, très emblématique de ce que le Web a construit – tant au niveau de la constitution de communautés, que de développement technique ou encore de licence d'utilisation.
