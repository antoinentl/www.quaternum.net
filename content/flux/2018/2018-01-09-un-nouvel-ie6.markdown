---
layout: post
title: "Un nouvel IE6"
date: "2018-01-09T22:00:00"
comments: true
published: true
description: "Chrome serait-il le nouvel Internet Explorer 6 ? Malgré cette comparaison quelque peu maladroite, cet article pointe la position monopolistique de Google comme étant un vrai problème."
categories:
- flux
tags:
- web
---
>Microsoft might have celebrated the death of Internet Explorer 6, but if Google isn’t careful then it might just resurrect an ugly era of the internet where “works best with Chrome” is a modern nightmare.  
[Tom Warren, Chrome is turning into the new Internet Explorer 6](https://www.theverge.com/2018/1/4/16805216/google-chrome-only-sites-internet-explorer-6-web-standards)

Chrome serait-il le nouvel Internet Explorer 6 ? Malgré cette comparaison quelque peu maladroite, cet article pointe la position monopolistique de Google comme étant un vrai problème.
