---
layout: post
title: "Conditions de la recherche"
date: "2018-11-09T22:00:00"
comments: true
published: true
description: "Ce récit en bande dessinée parle du syndrome de l'imposteur dans le domaine de la recherche, et en fond des conditions de la recherche."
categories:
- flux
tags:
- métier
---
> Solitude dans le travail [...], forte concurrence [...], assimilation du chercheur à sa recherche [...]. Bref, autant de manière de vous faire penser que vous êtes un imposteur.  
[LM et NOP, La recherche, c'est aussi avoir le sentiment d'être un imposteur...](http://socio-bd.blogspot.com/2018/11/la-recherche-cest-aussi-avoir-le.html)

Ce récit en bande dessinée parle du syndrome de l'imposteur dans le domaine de la recherche, et en fond des conditions de la recherche.
