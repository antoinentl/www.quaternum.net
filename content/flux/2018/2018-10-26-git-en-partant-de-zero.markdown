---
layout: post
title: "Git en partant de zéro"
date: "2018-10-26T22:00:00"
comments: true
published: true
description: "Un livre (web) sur Git."
categories:
- flux
tags:
- outils
---
> Welcome to the world of Git.  
[John Wiegley, Git from the Bottom Up](https://jwiegley.github.io/git-from-the-bottom-up/)

Un livre (web) sur Git.
