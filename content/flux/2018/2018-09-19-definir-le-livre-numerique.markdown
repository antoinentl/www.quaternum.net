---
layout: post
title: "Définir le livre numérique"
date: "2018-09-19T22:00:00"
comments: true
published: true
description: "Je ne suis pas totalement d'accord avec cette définition, mais elle me semble toutefois très intéressante. Mes lectures imprimées sur papier sont parfois bien plus ouvertes que certaines de mes lectures numériques sur le réseau, cela a plus à voir avec moi qu'avec le support, il est tellement difficile de faire des généralités sur les pratiques de lecture."
categories:
- flux
tags:
- ebook
---
> La technique peut bien rendre possible la mise en ligne de textes numériques faisant l’objet d’un travail d’édition aussi exigeant que celui qui est requis pour le papier ; la lecture d’un texte sur un terminal numérique, quel qu’il soit, ne saurait pourtant être la même que dans un codex. L’écart entre les deux lectures a quelque chose à voir avec la distinction entre la concentration induite par un monde clos et l’état d’alerte permanent généré par un système toujours potentiellement ouvert. Ces deux lectures ne doivent pas être hiérarchisées ou mises en concurrence : elles peuvent très bien être parallèles ou complémentaires.  
[Olivier Desgranges, De l’EPUB "maison" aux éditions Transbordage](http://bbf.enssib.fr/consulter/bbf-2018-15-0024-003)

Je ne suis pas totalement d'accord avec cette définition, mais elle me semble toutefois très intéressante.
Mes lectures imprimées sur papier sont parfois bien plus ouvertes que certaines de mes lectures numériques sur le réseau, cela a plus à voir avec moi qu'avec le support, il est tellement difficile de faire des généralités sur les pratiques de lecture.
