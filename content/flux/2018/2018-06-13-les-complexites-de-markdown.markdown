---
layout: post
title: "Les complexités de Markdown"
date: "2018-06-13T22:00:00"
comments: true
published: true
description: "Un sujet du forum CommonMark qui permet de comprendre les difficultés et les complexités de Markdown, afin de tenter de conserver la simplicité de ce langage."
categories:
- flux
tags:
- publication
---
>What if we weren’t chained to the past? What if we tried to create a light markup syntax that keeps what is good about Markdown, while revising some of the features that have led to bloat and complexity in the CommonMark spec?  
[John MacFarlane, Beyond Markdown](https://talk.commonmark.org/t/beyond-markdown/2787)

Un sujet du forum CommonMark qui permet de comprendre les difficultés et les complexités de Markdown, afin de tenter de conserver la simplicité de ce langage.
