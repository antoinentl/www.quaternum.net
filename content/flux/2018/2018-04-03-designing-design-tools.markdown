---
layout: post
title: "Designing Design Tools"
date: "2018-04-03T22:00:00"
comments: true
published: true
description: "Designing Design Tools est la thèse de Nolween Maudet, elle porte sur les outils utilisés par les designers. Au-delà du sujet passionnant, cette thèse a pour originalité d'être publiée intégralement en ligne sur un site web dédié, dans le prolongement de l'initiative d'Anthony Masure avec Le design des programmes."
categories:
- flux
tags:
- design
---
>Mainstream digital graphic design tools seldom evolved since their creation, more than 25 years ago. In recent years, a growing number of designers started questioning the resulting invisibility of design tools in the design process. In this dissertation, I address the following questions: _How do designers work with design software?_ _And how can we design novel design tools that better support designer practices?_  
[Nolwenn Maudet, Designing Design Tools](http://www.designing-design-tools.nolwennmaudet.com)

_Designing Design Tools_ est la thèse de Nolween Maudet, elle porte sur les outils utilisés par les designers. Au-delà du sujet passionnant, cette thèse a pour originalité d'être publiée intégralement en ligne sur un site web dédié, dans le prolongement de l'initiative d'Anthony Masure avec _Le design des programmes_.
