---
layout: post
title: "Fabriquer un livre avec le Web"
date: "2018-07-26T22:00:00"
comments: true
published: true
description: "Imprimer une page web avec des fonctions avancées et avec un simple fichier JavaScript ? C'est le défi auquel Julien Taquet et l'équipe de la Coko Foundation (et notamment Julie Blanc et Fred Chasen) entendent répondre. Ou comment fabriquer des livres imprimés avec le Web."
categories:
- flux
tags:
- publication
---
>With the first preview of Paged.js coming very soon (!) I thought I would post an example book I made last week with Paged.js, and provide some tips on how I made it.  
[Julien Taquet, Paged.js – sneak peeks](https://www.pagedmedia.org/pagedjs-sneak-peeks/)

Imprimer une page web avec des fonctions avancées et avec un simple fichier JavaScript ? C'est le défi auquel Julien Taquet et l'équipe de la Coko Foundation (et notamment Julie Blanc et Fred Chasen) entendent répondre. Ou comment fabriquer des livres imprimés avec le Web.
