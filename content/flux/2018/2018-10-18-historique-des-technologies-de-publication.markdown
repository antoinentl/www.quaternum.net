---
layout: post
title: "Historique des technologies de publication"
date: "2018-10-18T22:00:00"
comments: true
published: true
description: "Un travail énorme de recension et d'organisation des inventions et des événements qui ont marqué l'histoire des technologies de publication. Un travail remarquable et bien utile ! Le dépôt de la page est disponible sur GitLab : gitlab.com/JulieBlanc/timeline-publishing"
categories:
- flux
tags:
- publication
---
> Timeline of technologies for publishing (1963-2018)  
[Julie Blanc, Timeline of technologies for publishing](http://recherche.julie-blanc.fr/timeline-publishing/)

Un travail énorme de recension et d'organisation des inventions et des événements qui ont marqué l'histoire des technologies de publication.
Un travail remarquable et bien utile !
Le dépôt de la page est disponible sur GitLab : [gitlab.com/JulieBlanc/timeline-publishing](gitlab.com/JulieBlanc/timeline-publishing).
