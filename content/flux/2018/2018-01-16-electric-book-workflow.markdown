---
layout: post
title: "Electric Book Workflow"
date: "2018-01-16T22:00:00"
comments: true
published: true
description: "À l'occasion d'un événement organisé par la Coko Foundation autour de Paged Media, Arthur Attwell est venu présenté les outils utilisés par sa structure Fire and Lion pour produire des livres, autour du concept d'Electric Book : générateur de site statique, Git, CSS, Prince XML, etc. Electronic Book j'en avais parlé ici il y a presque un an) est un bon exemple de chaîne de publication basée sur les technologies du web."
categories:
- flux
tags:
- publication
---
>We make books for publishers and for organisations who need to act like publishers. And we produce many of those books in multiple formats: print, epub, website, and app.  
I’m going to go through some examples of books we’ve produced, and highlight some of the key features we need for printed books in particular.  
[Arthur Attwell, Book production with CSS Paged Media at Fire and Lion](https://www.pagedmedia.org/book-production-with-css-paged-media-at-fire-and-lion/)

À l'occasion d'un événement organisé par la Coko Foundation autour de Paged Media, Arthur Attwell est venu présenté les outils utilisés par sa structure Fire and Lion pour produire des livres, autour du concept d'Electric Book : générateur de site statique, Git, CSS, Prince XML, etc. Electronic Book ([j'en avais parlé ici il y a presque un an](/2017/01/31/the-electric-book-workflow/)) est un bon exemple de chaîne de publication basée sur les technologies du web.
