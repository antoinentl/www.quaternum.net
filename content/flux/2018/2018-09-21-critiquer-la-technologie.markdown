---
layout: post
title: "Critiquer la technologie"
date: "2018-09-21T22:00:00"
comments: true
published: true
description: "Hubert Guillaud propose un long article pour présenter le dernier ouvrage de James Bridle, c'est riche et complet, je n'ai moi-même pas lu le billet entièrement. Les critiques de la technologie qui ne sont pas binaires sont suffisamment rares pour être soulignées !"
categories:
- flux
tags:
- privacy
---
> Reste que la complexité et l’intrication du monde que décrit James Bridle, montrent combien il nous faut, plus que jamais, nous défaire justement d’une vision simple et manichéenne de la technologie.
[Hubert Guillaud, Technologie : l’âge sombre](http://www.internetactu.net/2018/09/10/technologie-lage-sombre/)

Hubert Guillaud propose un long article pour présenter le dernier ouvrage de James Bridle, c'est riche et complet, je n'ai moi-même pas lu le billet entièrement.
Les critiques de la technologie qui ne sont pas binaires sont suffisamment rares pour être soulignées !
