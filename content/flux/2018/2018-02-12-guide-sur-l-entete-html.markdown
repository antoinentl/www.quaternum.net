---
layout: post
title: "Guide sur l'entête HTML"
date: "2018-02-12T22:00:00"
comments: true
published: true
description: "Un guide complet sur l'entête HEAD, élément incontournable mais pourtant souvent mal géré/complété des pages HTML."
categories:
- flux
tags:
- web
slug: guide-sur-l-entete-html
---
>A list of everything that *could* go in the head of your document  
[https://gethead.info/](https://gethead.info/)

Un guide complet sur l'entête HEAD, élément incontournable mais pourtant souvent mal géré/complété des pages HTML. Les sources du projet sont par ailleurs disponibles sur GitHub, notamment pour proposer des modifications ou des ajouts.

Via [Opquast](https://mailchi.mp/opquast/qualit-web-n45-prestataires-clients-utilisateurs-un-triangle-vertueux).
