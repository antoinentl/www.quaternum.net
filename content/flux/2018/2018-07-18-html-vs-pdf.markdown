---
layout: post
title: "HTML VS PDF"
date: "2018-07-18T22:00:00"
comments: true
published: true
description: "Le format HTML est donc à préférer au PDF pour des raisons de recherche, d'utilisation et de maintenance, en plus de pouvoir être plus facilement rendu accessible."
categories:
- flux
tags:
- accessibilité
---
>Compared with HTML content, information published in a PDF is harder to find, use and maintain. More importantly, unless created with sufficient care PDFs can often be bad for accessibility and rarely comply with open standards.  
[Neil Williams, Why GOV.UK content should be published in HTML and not PDF](https://gds.blog.gov.uk/2018/07/16/why-gov-uk-content-should-be-published-in-html-and-not-pdf/)

Le format HTML est donc à préférer au PDF pour des raisons de recherche, d'utilisation et de maintenance, en plus de pouvoir être plus facilement rendu accessible.
