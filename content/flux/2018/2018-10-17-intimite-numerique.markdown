---
layout: post
title: "Intimité numérique"
date: "2018-10-17T22:00:00"
comments: true
published: true
description: "Clochix renverse l'approche habituelle autour de l'intimité numérique : plutôt que de présenter des outils, il aborde les raisons de s'intéresser à cette question. Et c'est ô combien pertinent et particulièrement utile."
categories:
- flux
tags:
- privacy
---
> L’intimité est un besoin vital pour les humains, en être privé nous prive d’une partie de notre humanité. En être privé, être toujours sous la menace du regard et du jugement d’autrui, c’est perdre la capacité de penser par soi même, d’exister, de se comporter en tant qu’individu indépendant, autonome. Priver les citoyen·ne·s d’intimité est une des caractéristiques des régimes totalitaires qui cherchent à nier les individualités pour gérer que des robots déshumanisés.  
[Clochix, Hygiène et écologisme numérique](https://clochix.net/marges/181014-cvp2018-10-14)

Clochix renverse l'approche habituelle autour de l'intimité numérique : plutôt que de présenter des outils (ah, la technique), il aborde les raisons (politiques notamment) de s'intéresser à cette question.
Et c'est ô combien pertinent et particulièrement utile.

> La meilleure des protections, c’est l’éducation. C’est acquérir une compréhension du fonctionnement des outils numériques.
