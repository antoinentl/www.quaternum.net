---
layout: post
title: "Fabriquer un livre avec le workflow Electric Book"
date: "2018-06-08T22:00:00"
comments: true
published: true
description: "J'en avais parlé ici il y a déjà un moment, Fire and Lion a mis au point un workflow pour produire en même temps une version imprimée et une version numérique d'un livre à partir d'une même source. Arthur Attwell revient en détail sur le projet The Economy."
categories:
- flux
tags:
- publication
---
>For most book-makers like me, who make print and digital publications, this has meant creating two versions: the print edition and the digital edition. The print edition is usually the master, and the digital version a laborious, post-production conversion.
[Arthur Attwell, Producing The Economy with the Electric Book workflow: a case study in multi-format book production](https://fireandlion.com/thinking/2017/08/29/producing-the-economy-with-the-ebw-case-study/)

J'en avais parlé [ici](/2017/01/31/the-electric-book-workflow/) il y a déjà un moment, Fire and Lion a mis au point un workflow pour produire en même temps une version imprimée et une version numérique d'un livre à partir d'une même source. Arthur Attwell revient en détail sur le projet [The Economy](https://core-econ.org/the-economy/).
