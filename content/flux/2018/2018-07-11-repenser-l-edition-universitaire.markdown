---
layout: post
title: "Repenser l'édition universitaire"
date: "2018-07-11T22:00:00"
comments: true
published: true
description: "Partant du constat, largement partagé, que la communication scientifique a un problème, Jon Tennant formule quelques propositions pertinentes pour repenser l'édition universitaire, notamment sur le modèle de publication et les fonctionnalités possibles. La dimension politique n'est pas abordée et c'est dommage."
categories:
- flux
tags:
- publication
slug: repenser-l-edition-universitaire
---
>The world of scholarly communication is broken.  
[Jon Tennant, Academic publishing is broken. Here’s how to redesign it](https://www.fastcompany.com/90180552/academic-publishing-is-broken-heres-how-to-redesign-it)

Partant du constat, largement partagé, que la communication scientifique a un problème, Jon Tennant formule quelques propositions pertinentes pour repenser l'édition universitaire, notamment sur le modèle de publication et les fonctionnalités possibles. La dimension politique n'est pas abordée et c'est dommage.

>Use your imagination. What would you want the scholarly communication system to look like? What are all the wonderful features you would include? What can you do to help turn a vision into reality?  
It is feasible to achieve 100% open access in the future while saving around 99% of the global spending budget on publishing. Funds could be better spent instead on research, grants for under-privileged students and minority researchers, improving global research infrastructure, training, support, and education. We can create a networked system, governed by researchers themselves, designed for effective, rapid, low-cost communication and research collaboration.
