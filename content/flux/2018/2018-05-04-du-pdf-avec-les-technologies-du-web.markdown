---
layout: post
title: "Du PDF avec les technologies du Web"
date: "2018-05-04T22:00:00"
comments: true
published: true
description: "Créer des documents PDF avec les technologies du Web, avec une source en Markdown, vraiment un beau projet qui laisse penser que les logiciels de PAO classiques vont pouvoir être abandonnés – en tout cas on s'en approche. [Les exemples](https://github.com/RelaxedJS/ReLaXed-examples) sont impressionnants !"
categories:
- flux
tags:
- publication
---
>ReLaXed creates PDF documents interactively using HTML or Pug (a shorthand for HTML). It allows complex layouts to be defined with CSS and JavaScript, while writing the content in a friendly, minimal syntax close to Markdown or LaTeX.  
[ReLaXed](https://github.com/RelaxedJS/ReLaXed/)

"Créer des documents PDF avec les technologies du Web", avec une source en Markdown, vraiment un beau projet qui laisse penser que les logiciels de PAO classiques vont pouvoir être abandonnés – en tout cas on s'en approche. [Les exemples](https://github.com/RelaxedJS/ReLaXed-examples) sont impressionnants !  
À noter que le principe a été dupliqué pour être utilisé avec des sources en AsciiDoc (et en utilisant Asciidoctor) : [https://github.com/Mogztter/asciidoctor-pdf.js](https://github.com/Mogztter/asciidoctor-pdf.js)

Via [Thomas](https://oncletom.io).
