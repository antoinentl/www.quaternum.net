---
layout: post
title: "Markdown et Zotero"
date: "2018-11-23T22:00:00"
comments: true
published: true
description: "Quelques solutions pour rédiger avec Markdown tout en utilisant le logiciel de référence bibliographique Zotero, bien pratique pour entamer un changement de paradigme et abandonner les traitements de texte classiques en environnement académique."
categories:
- flux
tags:
- publication
---
> Markdown est de plus en plus utilisé non seulement pour documenter du code mais pour d’autres usages : il s’agit ainsi de proposer à ceux qui l’utilisent pour rédiger au sens large des pistes pour assurer la partie bibliographique de l’écriture avec Zotero.  
[Vincent Carlino, Markdown et Zotero](https://zotero.hypotheses.org/2258)

Quelques solutions pour rédiger avec Markdown tout en utilisant le logiciel de référence bibliographique Zotero, bien pratique pour entamer un changement de paradigme et abandonner les traitements de texte classiques en environnement académique.

Via [Igor](https://herds.eu/user/140150).
