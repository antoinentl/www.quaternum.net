---
layout: post
title: "WebBook Level 1"
date: "2018-01-25T22:00:00"
comments: true
published: true
description: "Étant en désaccord avec les directions prises par les EPUB 3 Community Group et Publishing Working Group du W3C, Daniel Glazman propose ses propres spécifications pour une évolution du format EPUB, basée sur les technoglogies du Web."
categories:
- flux
tags:
- ebook
---
>I have then decided to work on a different format for electronic books, called WebBook. A format strictly based on Web technologies and when I say "Web technologies", I mean the most basic ones: html, CSS, JavaScript, SVG and friends; the class of specifications all Web authors use and master on a daily basis. Not all details are decided or even ironed, the proposal is still a work in progress at this point, but I know where I want to go to.  
[Daniel Glazman, Announcing WebBook Level 1, a new Web-based format for electronic books](http://www.glazman.org/weblog/dotclear/index.php?post/2018/01/18/WebBook-Level-1)

Étant en désaccord avec les directions prises par les EPUB 3 Community Group et Publishing Working Group du W3C, Daniel Glazman propose ses propres spécifications pour une évolution du format EPUB, basée sur les technologies du Web.
