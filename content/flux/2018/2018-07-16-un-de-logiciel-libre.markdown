---
layout: post
title: "Un peu de logiciel libre"
date: "2018-07-16T22:00:00"
comments: true
published: true
description: "France Culture, à l'occasion du rachat de GitHub par Microsoft, propose une émission sur le logiciel libre avec Pierre-Yves Gosset (Framasoft), Bernard Ourghanlian (Microsoft) et Amaëlle Guiton (Libération). Le problème c'est que tous ne parle pas le même langage, donc parfois il y a des discours relativement inaudibles (par exemple Bernard Ourghanlian ne semble pas faire la différence entre open source et libre)."
categories:
- flux
tags:
- outils
slug: un-de-logiciel-libre
---
>Dire que l'open source et le libre ont gagné, c'est très trompeur. C'est le cas dans plein d'endroits, parce que les éditeurs et les fabricants se sont rendus compte que c'était rentable, mais dès qu'on arrive à l'utilisateur, en bout de chaîne, c'est là que tout se referme petit à petit.  
[Amaëlle Guiton, Que reste-t-il du logiciel libre ?](https://www.franceculture.fr/emissions/du-grain-a-moudre/que-reste-t-il-du-logiciel-libre)

France Culture, à l'occasion du rachat de GitHub par Microsoft, propose une émission sur le logiciel libre avec Pierre-Yves Gosset (Framasoft), Bernard Ourghanlian (Microsoft) et Amaëlle Guiton (Libération). Le problème c'est que tous ne parle pas le même langage, donc parfois il y a des discours relativement inaudibles (par exemple Bernard Ourghanlian ne semble pas faire la différence entre open source et libre).
