---
layout: post
title: "Combien pour un site web ?"
date: "2018-03-19T22:00:00"
comments: true
published: true
description: "Combien coûte un site web ? David pose plutôt la question de savoir combien peut coûter un projet, et donc tout dépend du projet – définir un projet à également un coût."
categories:
- flux
tags:
- web
---
>Chacune de ces étapes a un coût et il faut le remettre en question avant d’arriver dans le mur auquel cas — même (et surtout !) en ayant le sentiment d’avoir réussi quelque chose — vous allez au devant de grandes frustrations.  
[David Larlet, Coût d’un site](https://larlet.fr/david/blog/2018/cout-site/)

Combien coûte un site web ? David pose plutôt la question de savoir combien peut _coûter_ un projet, et donc tout dépend du projet – définir un projet à également un coût.
