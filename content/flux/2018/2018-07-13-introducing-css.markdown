---
layout: post
title: "Introducing CSS"
date: "2018-07-13T22:00:00"
comments: true
published: true
description: "Heydon Pickering publie un billet assez drôle, imaginant que JavaScript serait utilisé pour mettre en forme des documents numériques et que CSS pourrait faire cela avec beaucoup plus d'efficacité. Un pied de nez à celles et ceux qui souhaitent la mort de CSS (ce qui n'est pas prêt d'arriver)."
categories:
- flux
tags:
- web
---
>Those familiar with the web platform will be well-versed in its two complementary technologies: HTML for documents and their structure, and JavaScript for interaction and styling.  
[Heydon Pickering, CSS: A New Kind Of JavaScript](http://www.heydonworks.com/article/css-a-new-kind-of-javascript)

Heydon Pickering publie un billet assez drôle, imaginant que JavaScript serait utilisé pour mettre en forme des documents numériques et que CSS pourrait faire cela avec beaucoup plus d'efficacité. Un pied de nez à celles et ceux qui souhaitent la mort de CSS (ce qui n'est pas prêt d'arriver).
