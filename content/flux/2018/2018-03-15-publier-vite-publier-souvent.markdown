---
layout: post
title: "Publier vite, publier souvent"
date: "2018-03-15T22:00:00"
comments: true
published: true
description: "Quelques solutions pour palier aux délais souvent très importants entre un événement universitaire et une publication qui en rend compte (spoil : le numérique est une caisse à outils déterminante)."
categories:
- flux
tags:
- publication
---
>Les durées propres à la publication universitaire existent donc à plusieurs niveaux, en asynchronie avec les pratiques pédagogiques qui sont l’horizon du texte de recherche.  
[...]  
Il apparaît que des formes plus fluides et plus légères de protocoles éditoriaux sont à tester, inventer et démocratiser.  
[Anthony Masure, La publication buissonnière : guide tactique pour les pratiques éditoriales universitaires](http://www.anthonymasure.com/articles/2017-09-publication-buissonniere)

Quelques solutions pour palier aux délais souvent très importants entre un événement universitaire et une publication qui en rend compte (_spoil_ : le numérique est une caisse à outils déterminante).
