---
layout: post
title: "À lire hors ligne"
date: "2018-02-09T22:00:00"
comments: true
published: true
description: "The Disconnect est une revue en ligne lisible uniquement hors connexion, il faut donc couper sa connexion internet pour pouvoir consulter les contenus – des récits de fiction, de la poésie, des courts essais. Autre particularité pour un revue en ligne : il n'y a aucun lien qui pointe hors de la revue ! Chris Bolin est à l'origine de cette initiative, j'avais parlé ici d'une page de son site web disponible que hors connexion."
categories:
- flux
tags:
- publication
---
>_The Disconnect_ is an offline-only, digital magazine of commentary, fiction, and poetry. Each issue forces you to disconnect from the internet, giving you a break from constant distractions and relentless advertisements.  
[The Disconnect](https://thedisconnect.co/)

_The Disconnect_ est une revue en ligne lisible uniquement _hors connexion_, il faut donc couper sa connexion internet pour pouvoir consulter les contenus – des récits de fiction, de la poésie, des courts essais. Autre particularité pour un revue en ligne&nbsp;: il n'y a aucun lien qui pointe hors de la revue&nbsp;! Chris Bolin est à l'origine de cette initiative, j'avais parlé [ici](/2017/12/06/deconnexion/) d'une page de son site web disponible uniquement hors connexion.
