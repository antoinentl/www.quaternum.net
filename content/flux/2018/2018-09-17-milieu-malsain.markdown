---
layout: post
title: "Milieu malsain"
date: "2018-09-17T22:00:00"
comments: true
published: true
description: "Le petit milieu du Web ne semble pas échapper aux phénomènes de harcèlement, mais j'aurais aimé croire que certaines valeurs intrinsèques à ce domaine permettent de se sortir autrement de ce type de situation. En combattant et non en baissant la tête, comme le fait Marie et malheureusement trop peu de ses soutiens."
categories:
- flux
tags:
- métiers
---
>Il n’y a rien de normal dans le fait de se faire harceler, quelle qu’en soit la raison.  
[Marie Guillaumet, Deux ans plus tard](https://marieguillaumet.com/deux-ans-plus-tard/)

Le petit milieu du Web ne semble pas échapper aux phénomènes de harcèlement, mais j'aurais aimé croire que certaines valeurs intrinsèques à ce domaine permettent de se sortir autrement de ce type de situation.
En combattant et non en baissant la tête, comme le fait Marie et malheureusement trop peu de ses soutiens.
