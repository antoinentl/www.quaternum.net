---
layout: post
title: "Un livre papier à base de bash"
date: "2018-10-01T22:00:00"
comments: true
published: true
description: "Une riche collection de projets divers, avec comme objectif de la publication basée sur du libre."
categories:
- flux
tags:
- publication
---
> The book collects documentation, ideas, software, graphic design and
experiments made with free software between 2008 and 2016.
> [...]
> Direction means:
>
> - a lightweight markup language that is easy to read and edit in its raw form
> - this simple markup will be translated in the next step into a more powerful and complex markup language (as LaTeX)
> -this more complex markup should be a widespread standard to have access and support for a bigger range of tools.
>
> BUT: I had the ambition for a little more complexity than what seemed possible with standard markdown.  
[Christoph Haag, How I learned to stop and love the Draft](https://freeze.sh/_/2016/london/)

Une riche collection de projets divers, avec comme objectif de la publication basée sur du libre.

Via [Stéphane](https://cafe.des-blogueurs.org/@notabene/100814084930351161).
