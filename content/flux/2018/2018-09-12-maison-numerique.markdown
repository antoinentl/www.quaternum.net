---
layout: post
title: "Maison numérique"
date: "2018-09-12T22:00:00"
comments: true
published: true
description: "Quelques conseils pratiques, y compris pour débutant·e pour créer sa maison numérique."
categories:
- flux
tags:
- privacy
---
> Dans l’optique de réduire l’emprise qu’ont les GAFAM sur moi, j’ai décidé de reprendre le contrôle d’une partie de mes outils.  
[Llu, Chez soi numérique en quelques clics](https://bribesdereel.net/post/2018/08/30/chez-soi-simplement)

Quelques conseils pratiques, y compris pour débutant·e pour créer sa maison numérique.
