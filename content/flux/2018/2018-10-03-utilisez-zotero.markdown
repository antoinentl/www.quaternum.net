---
layout: post
title: "Utilisez Zotero !"
date: "2018-10-03T22:00:00"
comments: true
published: true
description: "La communauté francophone propose cette traduction de la page Why Zotero? : les arguments sont nombreux ! Si vous devez gérer une bibliographie, que vous soyez étudiant·e, jeune chercheur·euse ou rédacteur·trice d'un document, Zotero est fait pour vous et se connecte à la plupart des outils de rédaction-publication."
categories:
- flux
tags:
- publication
---
> Nous voulons créer le meilleur logiciel de recherche disponible, et nous pensons que cela implique non seulement de produire des outils d’une grande puissance, mais aussi de fournir le meilleur support possible et de prendre des décisions qui vous permettent de contrôler vos données.
[Zotero, Pourquoi Zotero ?](https://zotero.hypotheses.org/1998)

La communauté francophone propose cette traduction de la page "Why Zotero?" : les arguments sont nombreux !
Si vous devez gérer une bibliographie, que vous soyez étudiant·e, jeune chercheur·euse ou rédacteur·trice d'un document, Zotero est fait pour vous et se connecte à la plupart des outils de rédaction-publication.
