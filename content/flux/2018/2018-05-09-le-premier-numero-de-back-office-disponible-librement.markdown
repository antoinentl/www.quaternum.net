---
layout: post
title: "Le premier numéro de Back Office disponible librement"
date: "2018-05-09T22:00:00"
comments: true
published: true
description: "L'équipe de Back Office l'avait annoncé dès le début du projet, au bout d'un an chaque numéro est disponible sous licence Creative Commons Paternité, Utilisation non commerciale et Partage à l'identique – donc librement en ligne. Le premier numéro est donc consultable gratuitement en ligne, une bonne nouvelle pour une série de textes de très bonne qualité."
categories:
- flux
tags:
- design
---
>Le premier numéro de _Back Office_ interroge les notions d’outil, d’instrument et d’appareil, dans le contexte du design. En sortant d’un rapport instrumental à la technique, à savoir une façon d’accomplir efficacement des tâches, les designers présents dans ce numéro « font avec » la technique, c’est-à-dire qu’ils la travaillent dans des directions qui échappent à tout déterminisme technologique.  
[Back Office, Faire avec](http://www.revue-backoffice.com/numeros/01-faire-avec/)

L'équipe de Back Office l'avait annoncé dès le début du projet, au bout d'un an chaque numéro est disponible sous licence Creative Commons Paternité, Utilisation non commerciale et Partage à l'identique – donc _librement_ en ligne. Le premier numéro est donc consultable gratuitement en ligne, une bonne nouvelle pour une série de textes de très bonne qualité.
