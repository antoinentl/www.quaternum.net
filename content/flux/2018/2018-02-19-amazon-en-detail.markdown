---
layout: post
title: "Amazon en détail"
date: "2018-02-19T22:00:00"
comments: true
published: true
description: "La stratégie et l'organisation d'Amazon analysées par Stéphane Schultz de 15marches, en deux épisodes, utile et intéressant, même si cela manque quelque peu de regard critique."
categories:
- flux
tags:
- métier
---
>L’ouverture à des "tiers" – vendeurs, marchands, mais aussi développeurs avec AWS –  va devenir le coeur de la stratégie d’Amazon, et la clé de son succès.  
[Stéphane Schultz, Amazon, l’Empire Invisible (1/2)](https://15marches.fr/numerique/amazon-lempire-invisible)

La stratégie et l'organisation d'Amazon analysées par Stéphane Schultz de 15marches, en deux épisodes, utile et intéressant, même si cela manque quelque peu de regard critique.
