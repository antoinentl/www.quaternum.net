---
layout: post
title: "ZoteroBib"
date: "2018-05-25T22:00:00"
comments: true
published: true
description: "Zotero, l'outil de gestion bibliographique incontournable, que l'on soit chercheur, bibliothécaire ou curieux, lance un site web qui permet de générer très facilement des références standardisées. ZoteroBib c'est un peu Zotero pour celles et ceux qui ne veulent pas s'embêter."
categories:
- flux
tags:
- outils
---
>Propulsé par la même technologie que Zotero et utilisant ses capacités inégalées d’extraction de métadonnées, ZoteroBib vous permet avec une grande fluidité, au fil de votre navigation sur le web, d’ajouter des éléments et de générer des bibliographies dans plus de 9 000 styles bibliographiques. ZoteroBib ne requiert ni installation de logiciel ni création de compte, et fonctionne sur tout type d’appareil, y compris les tablettes et les téléphones.  
[Dan Stillman, Lancement de ZoteroBib : créez vos bibliographies en un clin d'œil](https://zotero.hypotheses.org/1795)

Zotero, l'outil de gestion bibliographique incontournable, que l'on soit chercheur, bibliothécaire ou curieux, lance un site web qui permet de générer très facilement des références standardisées. [ZoteroBib](https://zbib.org/) c'est un peu Zotero pour celles et ceux qui ne veulent pas s'embêter. (ZoteroBib utilise la mémoire _cache_ des dispositifs qui l'utilisent, et c'est très intéressant.)
