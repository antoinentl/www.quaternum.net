---
layout: post
title: "Les ateliers [sens public]"
date: "2018-10-22T22:00:00"
comments: true
published: true
description: "La revue Sens Public se dote d'une belle collection. Au-delà de l'intérêt des premiers titres, il s'agit d'une application des principes que j'ai exposé dans mon mémoire. Bravo à l'équipe qui a mené cet ambitieux projet !"
categories:
- flux
tags:
- publication
slug: les-ateliers-de-sens-public
---
> Les ateliers de [sens public] naviguent dans l'archipel Sens public, dont les fragments de sens servent à fabriquer des monographies imprimées et numériques. [...] Nos textes sont d'abord publiés en format numérique sur la revue Sens public, où ils sont soumis à un processus d'évaluation par les pairs rigoureux. Ils sont ensuite restructurés pour une publication monographique en trois formats:
> - Format imprimé (POD)
> - Format epub en accès libre
> - Format pdf en accès libre.
>  
[Les ateliers de [sens public]](http://ateliers.sens-public.org)

La revue Sens Public se dote d'une belle collection.
Au-delà de l'intérêt des premiers titres, il s'agit d'une application des principes que j'ai exposé [dans mon mémoire](https://memoire.quaternum.net/).
Bravo à l'équipe qui a mené cet ambitieux projet !
