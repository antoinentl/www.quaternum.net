---
layout: post
title: "Surveillance généralisée"
date: "2018-11-14T22:00:00"
comments: true
published: true
description: "D’un point de vue humain, technique et économique, seules les grandes plateformes qui appliquent déjà ces mesures depuis qu’elles collaborent avec les polices européennes seront capables de respecter ces obligations : Google, Facebook et Twitter en tête."
categories:
- flux
tags:
- privacy
---
> D’un point de vue humain, technique et économique, seules les grandes plateformes qui appliquent déjà ces mesures depuis qu’elles collaborent avec les polices européennes seront capables de respecter ces obligations : Google, Facebook et Twitter en tête. Les autres acteurs n’auront d’autres choix que de cesser leur activité d’hébergement ou (moins probable, mais tout aussi grave) de sous-traiter aux géants l’exécution de leurs obligations.  
Ce texte consacre l’abandon de pouvoirs régaliens (surveillance et censure) à une poignée d’acteurs privés hégémoniques. Pourtant, la Commission et les États membres, en 146 pages d’analyse d’impact, ne parviennent même pas à expliquer en quoi ces obligations pourraient réellement être efficaces dans la lutte contre le terrorisme.  
[La Quadrature du Net, Censure antiterroriste : Macron se soumet aux géants du Web pour instaurer une surveillance généralisée](https://www.laquadrature.net/2018/11/14/censure-antiterroriste-macron-se-soumet-aux-geants-du-web-pour-instaurer-une-surveillance-generalisee/)
