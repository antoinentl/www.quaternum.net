---
layout: post
title: "Messages automatiques"
date: "2018-03-16T22:00:00"
comments: true
published: true
description: "Un générateur de messages pour les commits dans Git, c'est-à-dire les messages sensés illustrés des changements dans un système de gestion de versions comme Git. Le but ici étant de proposer des messages qui peuvent correspondre à n'importe quel projet, malin et drôle."
categories:
- flux
tags:
- outils
---
>debug suff  
[Nick Gerakines, whatthecommit](http://whatthecommit.com/index.txt)

Un générateur de messages pour les commits dans Git, c'est-à-dire les messages sensés illustrés des changements dans un système de gestion de versions comme Git. Le but ici étant de proposer des messages qui peuvent correspondre à n'importe quel projet, malin et drôle (toutes les possibilités sont dans [ce fichier](https://github.com/ngerakines/commitment/blob/master/commit_messages.txt)).

Via [Some skills](https://twitter.com/_noskill/status/974652191226187776).
