---
layout: post
title: "Comment écrire un message de commit"
date: "2018-03-06T22:00:00"
comments: true
published: true
description: "Quelques conseils très utiles pour écrire les messages de vos commits lorsque vous utilisez Git. Cela fait plusieurs mois que je reviens régulièrement sur ce billet, je le conserve donc ici pour mémoire."
categories:
- flux
tags:
- outils
---
>Why not suggest some alternatives to balance the load?  
[Chris Beams, How to Write a Git Commit Message](https://chris.beams.io/posts/git-commit/)

Quelques conseils très utiles pour écrire les messages de vos commits lorsque vous utilisez Git. Cela fait plusieurs mois que je reviens régulièrement sur ce billet, je le conserve donc ici pour mémoire.
