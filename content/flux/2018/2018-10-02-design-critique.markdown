---
layout: post
title: "Design critique"
date: "2018-10-02T22:00:00"
comments: true
published: true
description: "Initiative intéressante que de remettre en cause le mouvement dit de design éthique, avec une approche historique rafraîchissante. Favoriser le débat quitte à créer des clivages forts est toujours une bonne idée, je suis moins fan de la forme – un billet sur la plateforme Medium..."
categories:
- flux
tags:
- design
---
> [...] dans quelques semaines s’apprête à se tenir la seconde édition de la conférence Ethics by design, se présentant comme _principale conférence française dédiée à la conception numérique responsable et sociale_.  
Par ce texte nous entendons dénoncer l’hypocrisie de ceux qui, prétendant analyser le désastre et lui chercher des solutions, le perpétuent et s’en nourrissent.  
[Design non éthique, Design éthique — mort du design.](https://medium.com/@designonethic/design-%C3%A9thique-mort-du-design-e488650ebc58)

Initiative intéressante que de remettre en cause le mouvement dit de "design éthique", avec une approche historique rafraîchissante.
Favoriser le débat quitte à créer des clivages forts est toujours une bonne idée, je suis moins fan de la forme – un billet sur la plateforme Medium...
