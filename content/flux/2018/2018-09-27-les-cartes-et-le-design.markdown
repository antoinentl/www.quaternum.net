---
layout: post
title: "Les cartes et le design"
date: "2018-09-27T22:00:00"
comments: true
published: true
description: "Distinction très à propos de la part de Nolwenn Maudet : le rôle du design n'est pas d'aplatir mais plutôt de montrer les volumes et de dissocier les différentes strates d'informations."
categories:
- flux
tags:
- design
---
> Et si au contraire, nos cartes tentaient de faire résonner les espaces qu'elles cartographient ? Par exemple en indiquant par un choix de couleur l'animation de certaines rues ou bien, au contraire, le silence des autres. Parce que nos cartes numériques sont interactives, elles n'ont plus nécessairement besoin de forcer une lisibilité parfaite à chaque échelle car on peut tout à fait ajuster soi-même sa lecture par le zoom ou le filtrage. Nos cartes pourrait alors en devenir plus vastes que le territoire.  
[Nolwenn Maudet, les cartes et le territoire](https://carnet.nolwennmaudet.com/les-cartes-et-le-territoire)

Distinction très à propos de la part de Nolwenn Maudet : le rôle du design n'est pas d'aplatir mais plutôt de montrer les volumes et de dissocier les différentes strates d'informations.
