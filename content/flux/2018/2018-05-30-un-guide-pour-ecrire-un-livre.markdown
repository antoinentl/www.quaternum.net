---
layout: post
title: "Un guide pour écrire un livre"
date: "2018-05-30T22:00:00"
comments: true
published: true
description: "Un guide sur l'écriture, et plus particulièrement l'écriture d'un livre, avec beaucoup de citations."
categories:
- flux
tags:
- livre
---
>There is a lot of writing advice out there, but I don’t find much of it especially helpful. I do not mean that it’s “inaccurate”; I only want to note that a lot of it suggests that there are only a few “correct” methods, and that can endanger the process, or at least make it a lot less fruitful. Writing a book is an individual endeavor, an expression of a writer’s unique and thoughtful approach to inspiration, process, and refinement. The way a book is written is part of what makes it so singular. This guide points to a few approaches that have worked for some writers.  
[Naomi Huffman, How to make a book](https://thecreativeindependent.com/guides/how-to-make-a-book/)

Un guide sur l'écriture, et plus particulièrement l'écriture d'un livre, avec beaucoup de citations.
