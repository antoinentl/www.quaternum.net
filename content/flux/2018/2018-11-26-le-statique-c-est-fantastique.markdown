---
layout: post
title: "Le statique c'est fantastique"
date: "2018-11-26T22:00:00"
comments: true
published: true
description: "Une vidéo d'une heure sur les évolutions des technologies du web, un condensé utile et nécessaire pour qui s'intéresse au numérique aujourd'hui, avec comme prétexte la mouvance statique (générateurs de site statique, versionnement, architectures découplées, etc.). L'article correspondant à la vidéo est sur le site de Frank Taillandier."
categories:
- flux
tags:
- web
slug: le-statique-c-est-fantastique
---
> Ce qu’on résume encore trop souvent à l’appellation « site statique » cache en fait l’adoption d’un nouveau paradigme : le séparation du back et du front et le passage d’architectures monolithiques à des architectures plus modulaires, plus performantes, plus sécurisées et redimensionnables par défaut.  
[Frank Taillandier, La hype statique ne fait que commencer !](https://www.youtube.com/watch?v=44YQNMYeo_E)

Une vidéo d'une heure sur les évolutions des technologies du web, un condensé utile et nécessaire pour qui s'intéresse au numérique aujourd'hui, avec comme prétexte la mouvance statique (générateurs de site statique, versionnement, architectures découplées, etc.).
L'article correspondant à la vidéo est [sur le site de Frank Taillandier](https://frank.taillandier.me/2018/11/20/la-hype-statique-ne-fait-que-commencer/).
