---
layout: post
title: "LaTeX en sciences humaines et sociales"
date: "2018-10-11T22:00:00"
comments: true
published: true
description: "C'est suffisamment rare pour être salué, voici un billet qui présente le système de publication LaTeX comme solution accessible pour rédiger un mémoire de Master. Clair et précis, j'espère que cela suscitera un certain intérêt chez des étudiants de domaines divers."
categories:
- flux
tags:
- publication
---
> Ce qu’il faut retenir c’est qu’avec LaTeX, vous vous concentrez sur la mise en sens du texte. Vous n’avez quasiment jamais à faire de travail de mise en forme (même si c’est possible), le langage et le logiciel s’occupe de cela pour vous, en fonction de ce que vous lui avez dit d’interpréter.  
[Valentin Mériaux, LaTeX en master : mission impossible ?](https://encouragr.wordpress.com/2018/05/14/latex-en-master-mission-impossible/)

C'est suffisamment rare pour être salué, voici un billet qui présente le système de publication LaTeX comme solution accessible pour rédiger un mémoire de Master.
Clair et précis, j'espère que cela suscitera un certain intérêt chez des étudiants de domaines divers.
