---
layout: post
title: "Écriture et souplesse en milieu académique"
date: "2018-10-19T22:00:00"
comments: true
published: true
description: "Arthur Perret s'inspire d'un article que j'avais moi-même signalé il y a quelques semaines pour mettre en place une chaîne de publication, personnelle mais puissante. Un bon exemple de l'apport d'un peu de curiosité et d'expérimentation."
categories:
- flux
tags:
- publication
---
> Circuler entre Mardown, docx, HTML et PDF ne fait peut-être pas du chercheur un dieu de la science, mais cela favorise assez bien l’autonomie et l’adaptabilité – une double compétence très utile au quotidien dans l’univers académique.  
[Arthur Perret, Une micro-chaîne éditoriale multicanal pour l’écriture scientifique](https://infologie.hypotheses.org/342)

Arthur Perret s'inspire d'un article que j'avais [moi-même signalé](https://www.quaternum.net/2018/07/24/markdown-et-word-en-milieu-academique/) il y a quelques semaines pour mettre en place une chaîne de publication, personnelle mais puissante.
Un bon exemple de l'apport d'un peu de curiosité et d'expérimentation.
