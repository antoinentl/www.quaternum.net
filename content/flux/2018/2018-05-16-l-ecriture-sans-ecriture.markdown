---
layout: post
title: "L'écriture sans écriture"
date: "2018-05-16T22:00:00"
comments: true
published: true
description: "J'étais passé à côté de ce livre, sa lecture me semble désormais incontournable."
categories:
- flux
tags:
- livre
slug: l-ecriture-sans-ecriture
---
>Internet et l’environnement numérique présentent aux auteurs et aux lecteurs de nouveaux défis et de nouveaux outils pour repenser la créativité, l’autorité de l’auteur et notre relation avec la langue.  
[Jean Boîte Éditions, L'écriture sans écriture by Kenneth Goldsmith](https://www.jean-boite.fr/product/l%C3%A9criture-sans-%C3%A9criture-by-kenneth-goldsmith)

J'étais passé à côté de ce livre, sa lecture me semble désormais incontournable.

Via [Nicolas Tilly](https://twitter.com/nicolastilly/status/996116928480579584).

Mise à jour : Kenneth Goldsmith était [l'invité de Sylvain Bourmeau sur France Culture en juin](https://www.franceculture.fr/emissions/la-suite-dans-les-idees/la-suite-dans-les-idees-du-samedi-02-juin-2018).
