---
layout: post
title: "Fabriquer des livres avec des sites statiques"
date: "2018-11-22T22:00:00"
comments: true
published: true
description: "À l'occasion de la JAMstack Conf 2018 Eric Gardner a présenté les publications numériques de Getty Publications et la chaîne d'édition Quire."
categories:
- flux
tags:
- publication
---
>
[Eric Gardner, Building books from static sites](https://building-books-from-static-sites.netlify.com/)

À l'occasion de la JAMstack Conf 2018 Eric Gardner a présenté les publications numériques de Getty Publications et la chaîne d'édition Quire.
[La vidéo est en ligne](https://www.youtube.com/watch?v=-2hOesSVoRw&index=12&list=PL58Wk5g77lF-UQ39pejLX2Zn5DxQyExBa&t=0s).

Les autres vidéos de cette conférence [sont en ligne](https://www.youtube.com/playlist?list=PL58Wk5g77lF-UQ39pejLX2Zn5DxQyExBa) et quelques notes sont disponibles [par ici](https://www.notion.so/JAMstack-conf-notes-ab6d2d185ae54a669cfa3d4f65f5442e).
