---
layout: post
title: "Engagement"
date: "2018-07-02T22:00:00"
comments: true
published: true
description: "Framasoft a rencontré quota_atypique, chercheuse et militante : un entretien passionnant et motivant !"
categories:
- flux
tags:
- internet
---
>Un jour, je me suis rendu compte que je n’allais pas pouvoir rattraper les ingénieurs qui m’entouraient sur leur terrain. Par contre, la philo, j’en ai fait à haute dose en prépa, et j’ai une solide culture dans le domaine, qui m’autorise à faire une lecture philosophique du monde sans trop me tromper et en étant à peu près légitime à le faire.  
Ces philosophes sont pertinents dans l’analyse parce qu’ils fournissent des cadres de réflexion – pour peu qu’on les utilise rigoureusement, sans faire des enfants dans le dos à l’auteur. Ça permet de voir s’agencer des systèmes.  
Aussi, le numérique est un outil pour faire société, on ne dit pas qu’Internet est le fil dont est tissé la société pour rien : au final les bonnes questions sont des questions de société. Moins de solutions d’ingénieurs qui répondent parfois élégamment à des faux problèmes et plus de : qu’est-ce que ça fait au tissu social ? À la démocratie ? Les philosophes se posent ces questions depuis le Ve siècle avant JC, ils sont de bon conseil.  
[quota_atypique, Engagement_atypique](https://framablog.org/2018/06/30/engagement-atypique/)

Framasoft a rencontré quota_atypique, chercheuse et militante : un entretien passionnant et motivant !
