---
layout: post
title: "Appel à contribution pour le forum des archivistes"
date: "2018-06-22T22:00:00"
comments: true
published: true
description: "Pour le troisième Forum des archivistes un appel à contribution est ouvert jusqu'au vendredi 14 septembre 2018. Les neuf axes répartis en trois grandes questions sont intéressants, allez y jeter un œil !"
categories:
- flux
tags:
- publication
---
>La troisième édition du Forum des archivistes qui se déroulera à Saint-Étienne les 3, 4 et 5 avril 2019 sera l’occasion de dresser le bilan des pratiques archivistiques actuelles, à l’aune de la question de la transparence, et d’analyser les perspectives à venir, tout en favorisant le dialogue avec les utilisateurs des documents et données que les archivistes contribuent à conserver, ainsi qu’avec les autres professions qui participent à la production de l’information.  
[Forum des archivistes, Appel à contribution](https://forum.archivistes.org/programme/appel-a-contribution/)

Pour le troisième Forum des archivistes un appel à contribution est ouvert jusqu'au vendredi 14 septembre 2018. Les neuf axes répartis en trois grandes questions sont intéressants, allez y jeter un œil !
