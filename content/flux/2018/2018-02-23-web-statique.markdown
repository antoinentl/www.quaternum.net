---
layout: post
title: "Web statique"
date: "2018-02-23T22:00:00"
comments: true
published: true
description: "Boris Schapira présente des nouveaux outils et systèmes pour gérer des sites web, loin des CMS traditionnels, avec du statique dedans. Un résumé très clair de ce que les générateurs de site statique et autre CMS headless apportent comme nouvelles approches."
categories:
- flux
tags:
- web
---
>Mais pourquoi diable voudrions-nous séparer l’environnement contributif et l’outil de production? Pour une meilleure séparation des préoccupations.  
L’équipe de développement, libérée du fardeau de la maintenance d’une base de données, peut se concentrer sur l’évolution technique de la plate-forme et le pipeline de production des ressources statiques tandis que l’équipe de contribution, de son côté, peut affiner les contenus.  
Les contributeurs peuvent travailler sur des fichiers plats faciles à stocker et à modifier.  
[Boris Schapira, Meilleure UX, meilleures performances : la nouvelle donne du web statique](https://blog.dareboost.com/fr/2018/02/site-statique-performance-web/)

Boris Schapira présente des nouveaux outils et systèmes pour gérer des sites web, loin des CMS traditionnels, avec du statique dedans. Un résumé très clair de ce que les générateurs de site statique et autre CMS headless apportent comme nouvelles approches.
