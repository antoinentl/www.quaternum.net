---
layout: post
title: "Granularité"
date: "2018-06-26T22:00:00"
comments: true
published: true
description: "Forestry.io, l'éditeur de contenu pour générateurs de site statique, intègre désormais un système de blocs ou de briques, afin de construire facilement une page web. Fonctionnalité intéressante qui permet de penser une granularité plus fine pour un site web, et non plus traditionnellement la page. Bonus : Forestry.io a même développé un thème Jekyll basé sur ce concept."
categories:
- flux
tags:
- publication
---
>Today we introduce Blocks - a powerful Field Type that enables your editors to build entire landing pages from scratch and create rich blog post layouts with a pre-defined code template, we call it a Block Template.  
[Sebastian Engels, Blocks - Give Your Editors the Power to Build Pages](https://forestry.io/blog/blocks-give-your-editors-the-power-to-build-pages/)

[Forestry.io](https://forestry.io), l'éditeur de contenu pour générateurs de site statique, intègre désormais un système de blocs ou de briques, afin de construire facilement une page web. Fonctionnalité intéressante qui permet de penser une granularité plus fine pour un site web, et non plus traditionnellement la page. Bonus : Forestry.io a même développé [un thème Jekyll](https://forestryio.github.io/ubuild-jekyll/) basé sur ce concept.
