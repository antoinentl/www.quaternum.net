---
layout: post
title: "Livre et recherche"
date: "2018-06-25T22:00:00"
comments: true
published: true
description: "Une étude sur l'évolution du recours aux ouvrages dans les travaux universitaires, par rapport aux articles, permet de révéler que le livre serait moins sollicité dans la recherche. En plus des éléments des conclusions assez pertinents, j'ajouterai que le rythme des publications, ou tout du moins le monde dont fait partie le milieu académique, s'accélère, et un article met moins de temps à être publié qu'un ouvrage."
categories:
- flux
tags:
- publication
---
>Dans l’ensemble, les livres connaissent bel et bien un déclin dans l’usage qu’en font les chercheurs. Ils prennent une part de moins en moins importante dans les paysages des références en SHSAL, même si cette tendance est moins marquée en arts et lettres. [...] L’accessibilité des livres savants demeure toutefois cruciale. Car le livre n’est pas qu’un véhicule de communication savante, c’est aussi un véhicule de construction des savoirs . Et c’est aussi par le livre que la recherche sort de l’université et part à la rencontre d’un autre public.  
[Delphine Lobet et Vincent Larivière, La mort des livres dans les sciences humaines et sociales, et en arts et lettres ?](https://www.acfas.ca/publications/decouvrir/2018/06/mort-livres-sciences-humaines-sociales-arts-lettres)

Une étude sur l'évolution du recours aux ouvrages dans les travaux universitaires, par rapport aux articles, permet de révéler que le livre serait moins sollicité dans la recherche. En plus des éléments des conclusions assez pertinents, j'ajouterai que le rythme des publications, ou tout du moins le monde dont fait partie le milieu académique, s'accélère, et un article met moins de temps à être publié qu'un ouvrage.

Via [Louis-Olivier Brassard](https://twitter.com/_loupbrun).
