---
layout: post
title: "Markdown et Word en milieu académique"
date: "2018-07-24T22:00:00"
comments: true
published: true
description: "La recette de Raphael Kabo pour passer d'un document Markdown (accompagné d'une bibliographie au format BibTex gérée avec Zotero) à un document Word, avec l'aide de Pandoc. D'autres solutions existent aujourd'hui (comme Stylo), mais celle-ci a le mérite d'être indépendante (le principal problème provient d'Ulysses, mais c'est une autre question)."
categories:
- flux
tags:
- publication
---
>Over the last few months, as I’ve been working on my PhD, I’ve set up a workflow to convert my writing (in everyone’s favourite simple markup language, Markdown), as if by magic, into Word documents which I can email to my supervisor, without having to explain to her that Markdown is great, really, if you only tried it you’d see.  
[Raphael Kabo, My workflow for transforming academic Markdown into beautiful Word documents](http://raphaelkabo.com/blog/posts/markdown-to-word/)

La recette de Raphael Kabo pour passer d'un document Markdown (accompagné d'une bibliographie au format BibTex gérée avec Zotero) à un document Word, avec l'aide de Pandoc. D'autres solutions existent aujourd'hui (comme [Stylo](https://stylo.ecrituresnumeriques.ca/)), mais celle-ci a le mérite d'être indépendante (le principal problème provient d'Ulysses, mais c'est une autre question).
