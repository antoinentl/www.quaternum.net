---
layout: post
title: "Des couleurs dans le terminal"
date: "2018-01-17T22:00:00"
comments: true
published: true
description: "Pour rendre son terminal plus beau !"
categories:
- flux
tags:
- outils
---
>Color Scheme for Gnome Terminal and Pantheon Terminal  
[Gogh](https://mayccoll.github.io/Gogh/)

Pour rendre son terminal plus beau !
