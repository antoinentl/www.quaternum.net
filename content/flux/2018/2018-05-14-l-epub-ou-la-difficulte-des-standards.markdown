---
layout: post
title: "L'EPUB ou la difficulté des standards"
date: "2018-05-14T22:00:00"
comments: true
published: true
description: "Pour expliquer l'apparition du format EPUB 3.2 Dave Cramer fait un nécessaire historique des dernières versions de l'EPUB. Un billet qui permet de comprendre la difficulté de mettre en place, de diffuser et de maintenir des standards."
categories:
- flux
tags:
- ebook
slug: l-epub-ou-la-difficulte-des-standards
---
>Unlike the web, the ebook ecosystem is highly dependent on formal validation.  
[Dave Cramer, Why Specs Change: EPUB 3.2 and the Evolution of the Ebook Ecosystem](http://epubsecrets.com/why-specs-change-epub-3-2-and-the-evolution-of-the-ebook-ecosystem.php)

Pour expliquer l'apparition du format EPUB 3.2 Dave Cramer fait un nécessaire historique des dernières versions de l'EPUB. Un billet qui permet de comprendre la difficulté de mettre en place, de diffuser et de maintenir des standards.
