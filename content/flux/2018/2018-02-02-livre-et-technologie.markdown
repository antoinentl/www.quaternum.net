---
layout: post
title: "Livre et technologie"
date: "2018-02-02T22:00:00"
comments: true
published: true
description: "Comment dire... Il s'agit d'une vision du livre du point de vue américain."
categories:
- flux
tags:
- livre
---
>Books were overtaken by other media decades ago. The problem isn’t that books don’t have enough television in them, or enough internet in them; it’s that they are just one form of readily available cultural consumption among so many.  
[Alex Shepard, Silicon Valley Won’t Save Books](https://newrepublic.com/article/146430/silicon-valley-wont-save-books)

Comment dire... Il s'agit d'une vision du livre du point de vue américain.
