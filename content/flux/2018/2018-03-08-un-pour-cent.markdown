---
layout: post
title: "1%"
date: "2018-03-08T22:00:00"
comments: true
published: true
description: "Très bonne illustration du faux argument qui consiste à dire qu'il est légitime de ne pas prendre en compte 1% des utilisateurs. En effet il s'agit plutôt de 1% de cas d'utilisation, et chaque utilisateur peut être concerné par ce 1%."
categories:
- flux
tags:
- design
slug: un-pour-cent
---
>It's not 1% of people who always can't see your site and 99% of people who always can. It's 1% of visits.  
[Stuart, Why availability matters](https://kryogenix.org/code/browser/why-availability/)

Très bonne illustration du faux argument qui consiste à dire qu'il est légitime de ne pas prendre en compte 1% des _utilisateurs_. En effet il s'agit plutôt de 1% de _cas d'utilisation_, et chaque utilisateur peut être concerné par ce 1%.

Via [HTeuMeuLeu](https://twitter.com/HTeuMeuLeu/status/966318780711424000).
