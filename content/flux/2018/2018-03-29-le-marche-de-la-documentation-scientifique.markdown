---
layout: post
title: "Le marché de la documentation scientifique"
date: "2018-03-29T22:00:00"
comments: true
published: true
description: "Si même le CNRS décide de stopper les négociations avec l'_éditeur_ Springer, alors une réaction s'opère peut-être enfin face aux pratiques ultra-libérales de certains distributeurs de contenus scientifiques. Enfin..."
categories:
- flux
tags:
- publication
---
>Les prix des abonnements aux ressources électroniques, en hausse constante depuis vingt ans, génèrent des bénéfices importants pour les grands éditeurs scientifiques que sont Springer et Elsevier, alors même qu’une proportion non négligeable et croissante1 des articles publiés dans les revues concernées par les abonnements sont en accès libre. Le coût de cet accès pour ces articles est assumé par les auteurs ou leurs institutions et versé aux éditeurs. Pourquoi dès lors faire payer l’accès via l’abonnement quand l’éditeur a déjà été financé?  
[CNRS, Information aux laboratoires sur les abonnements Springer](http://www.cnrs.fr/inshs/recherche/springer.htm)

Si même le CNRS décide de stopper les négociations avec l'_éditeur_ Springer, alors une réaction s'opère peut-être enfin face aux pratiques ultra-libérales de certains distributeurs de contenus scientifiques. Enfin...

>Plusieurs communautés scientifiques se mobilisent pour enrayer cette spirale inflationniste des budgets de documentation scientifique.
