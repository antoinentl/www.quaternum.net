---
layout: post
title: "Le projet de Paged Media"
date: "2018-03-09T22:00:00"
comments: true
published: true
description: "Le projet Paged Media est présenté dans ses détails par Julie Blanc, le but est donc de mettre en place des outils pour pouvoir produire des documents paginés, depuis le navigateur, avec un format PDF en sortie. Fabriquer des livres – ou des documents – avec les outils du Web, en somme."
categories:
- flux
tags:
- publication
---
>On Tuesday 9 January, some people met at the MIT Press offices in Cambridge to seed the Paged Media Open Source initiative: a suite of JavaScripts to paginate HTML/CSS in the browser for the purposes of exporting print-ready, or display-friendly, PDF from the browser. [...] For this first meeting, I made a presentation about different Paged Media approaches and tools that already exist. This post in two parts is a version of that presentation.  
[Julie Blanc, Paged Media approaches (Part 1 of 2)](https://www.pagedmedia.org/paged-media-approaches-part-1-of-2/)

Le projet Paged Media est présenté dans ses détails par Julie Blanc, le but est donc de mettre en place des outils pour pouvoir produire des documents paginés, depuis le navigateur, avec un format PDF en sortie. Fabriquer des livres – ou des documents – avec les outils du Web, en somme. Julie détaille les différentes options existantes, et l'approche choisie.

>Our goal is to build tools for paged media rendering rather than a particular product which is more adaptable for various workflows and products. We want to use the advantages of browsers: CSS feature implementations, development tools to explore rendering, modularity and interoperability with other tools… [...] It’s a great adventure we have begun!
