---
layout: post
title: "Domination des esprits"
date: "2018-09-24T22:00:00"
comments: true
published: true
description: "J'étais complément passé à côté de ce texte de Stéphane Bortzmeyer et daté de 2001. Je suis toujours impressionné par ses analyses très fines et très justes, et c'est d'autant plus visibles ici avec un sujet qui suscite de nouvelles interrogations."
categories:
- flux
tags:
- publication
---
> MS-Word domine les esprits et une bonne part du marché.  
[Stéphane Bortzmeyer, After Word: l'avenir du traitement de texte](http://www.bortzmeyer.org/afterword.html)

J'étais complément passé à côté de ce texte de Stéphane Bortzmeyer et daté de 2001.
Je suis toujours impressionné par ses analyses très fines et très justes, et c'est d'autant plus visibles ici avec un sujet qui suscite de nouvelles interrogations.

> SGML et son populaire descendant XML sont bâtis sur une idée simple : présentation et structure sont trop importants pour être mêlés. SGML n'ayant pas eu, et de loin, le même succès marketing, je ne parlerai plus que de XML. XML voit le document comme une arborescence d'élements, toute la présentation étant confiée à des programmes extérieurs (utilisant parfois des langages spécialisés comme XSL).
