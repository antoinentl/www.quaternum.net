---
layout: post
title: "Citation.js"
date: "2018-08-29T22:00:00"
comments: true
published: true
description: "Un outil permettant de passer d'un format de citation à un autre (BibTeX, Wikidata JSON, etc.), voir de créer des citations à partir de métadonnées structurées."
categories:
- flux
tags:
- publication
slug: citation-js
---
> Citation.js (GitHub, NPM) converts formats like BibTeX, Wikidata JSON and ContentMine JSON to CSL-JSON to convert to other formats like APA, Vancouver and back to BibTeX.  
[Citation.js](https://citation.js.org/)

Un outil permettant de passer d'un format de citation à un autre (BibTeX, Wikidata JSON, etc.), voir de créer des citations à partir de métadonnées structurées.

Via [Thomas](https://oncletom.io/).
