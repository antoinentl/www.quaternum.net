---
layout: post
title: "Sciences du design"
date: "2018-01-18T22:00:00"
comments: true
published: true
description: "Les deux premiers numéros de la revue _Sciences du design_ sont désormais en accès libre sur Cairn.info, deux nouveaux numéros seront disponibles en accès libre l'année prochaine. L'ensemble des numéros et des articles sont par ailleurs consultables sur la plateforme Cairn.info, en partie via paiement (ou par le biais d'un accès bibliothèque)."
categories:
- flux
tags:
- design
---
>_Sciences du Design_ est une revue internationale en langue française de recherche scientifique en design.  
[Sciences du Design](https://www.cairn.info/revue-sciences-du-design.htm)

Les deux premiers numéros de la revue _Sciences du design_ sont désormais en accès libre sur Cairn.info, deux nouveaux numéros seront disponibles en accès libre l'année prochaine. L'ensemble des numéros et des articles sont par ailleurs consultables sur la plateforme Cairn.info, en partie via paiement (ou par le biais d'un accès bibliothèque).
