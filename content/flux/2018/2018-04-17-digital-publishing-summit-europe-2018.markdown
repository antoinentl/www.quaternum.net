---
layout: post
title: "Digital Publishing Summit Europe 2018"
date: "2018-04-17T22:00:00"
comments: true
published: true
description: "L'EPUB Summit devient Digital Publishing Summit Europe, le rendez-vous à ne pas manquer sur la question du livre et de la publication numériques. Le programme est riche, avec notamment des sujets sur la production et la gestion de contenu (enfin) et moins d'interventions sur le format EPUB (ouf)."
categories:
- flux
tags:
- ebook
---
>With this event, EDRLab aims to strengthen a true spirit of cooperation between professionals and push to the massive adoption of open standards and software by the European publishing industry.  
[EDRLab, Digital Publishing Summit Europe 2018](https://www.edrlab.org/dpub-summit-2018/)

L'EPUB Summit devient Digital Publishing Summit Europe, le rendez-vous à ne pas manquer sur la question du livre et de la publication numériques. [Le programme](https://www.edrlab.org/dpub-summit-2018/program/) est riche, avec notamment des sujets sur la production et la gestion de contenu (enfin) et moins d'interventions sur le format EPUB (ouf).
