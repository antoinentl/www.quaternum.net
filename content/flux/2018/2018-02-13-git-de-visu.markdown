---
layout: post
title: "Git de visu"
date: "2018-02-13T22:00:00"
comments: true
published: true
description: "Une ressource utile (pour moi) pour mieux comprendre certains fonctionnements de Git."
categories:
- flux
tags:
- outils
---
>Cette page donne une brève référence visuelle des principales commandes git.  
[Mark Lodato, Une Référence Visuelle de Git](https://marklodato.github.io/visual-git-guide/index-fr.html)

Une ressource utile (pour moi) pour mieux comprendre certains fonctionnements de Git.
