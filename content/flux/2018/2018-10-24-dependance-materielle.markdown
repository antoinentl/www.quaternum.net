---
layout: post
title: "Dépendance matérielle"
date: "2018-10-24T22:00:00"
comments: true
published: true
description: "Le récit surréaliste de l'introduction d'un composant de surveillance sur la plupart du matériel informatique. Il s'agit d'une bonne illustration de la dépendance dans le domaine de l'informatique."
categories:
- flux
tags:
- privacy
---
> The attack by Chinese spies reached almost 30 U.S. companies, including Amazon and Apple, by compromising America’s technology supply chain, according to extensive interviews with government and corporate sources.  
[Jordan Robertson et Michael Riley, The Big Hack: How China Used a Tiny Chip to Infiltrate U.S. Companies](https://www.bloomberg.com/news/features/2018-10-04/the-big-hack-how-china-used-a-tiny-chip-to-infiltrate-america-s-top-companies)

Le récit surréaliste de l'introduction d'un composant de surveillance sur la plupart du matériel informatique.
Il s'agit d'une bonne illustration de la dépendance dans le domaine de l'informatique.
