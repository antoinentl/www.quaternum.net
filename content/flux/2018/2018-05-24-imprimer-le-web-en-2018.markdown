---
layout: post
title: "Imprimer le web (en 2018)"
date: "2018-05-24T22:00:00"
comments: true
published: true
description: "Rachel Andrew fait un point (nécessaire) sur l'avancement des technologies, des outils, des workflows et des initiatives en matière d'impression du web. Dommage que Paged Media ne soit pas mentionnée."
categories:
- flux
tags:
- publication
---
>In an ideal world, browsers would have implemented more of the Paged Media specification when printing direct from the browser, and fragmentation would be more thoroughly implemented in a consistent way. It is certainly worth raising the bugs that you find when printing from the browser with the browsers concerned. If we don’t request these things are fixed, they will remain low priority to be fixed.  
[Rachel Andrew, A Guide To The State Of Print Stylesheets In 2018](https://www.smashingmagazine.com/2018/05/print-stylesheets-in-2018/#comments-print-stylesheets-in-2018)

Rachel Andrew fait un point (nécessaire) sur l'avancement des technologies, des outils, des workflows et des initiatives en matière d'impression du web. Dommage que [Paged Media](https://www.pagedmedia.org/) ne soit pas mentionnée.
