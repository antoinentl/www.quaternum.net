---
layout: post
title: "Le désenchantement du logiciel"
date: "2018-10-04T22:00:00"
comments: true
published: true
description: "Romain Fallet a traduit le billet de Nikita, Software disenchantment, qui fait le constat qu'il y a comme un problème dans le développement web – ou plus globalement le développement logiciel. Même si Nikita fait parfois l'impasse sur des points de compréhension importants – choix technologiques, prise en compte de spécificités de certains logiciels, contextes de développement – ce texte est bienvenu !"
categories:
- flux
tags:
- design
---
>Les éditeurs de texte modernes ont plus de latence qu’un Emacs vieux de 42 ans.  
[Nikita, Le désenchantement du logiciel](https://blog.romainfallet.fr/desenchantement-logiciel/)

Romain Fallet a traduit le billet de Nikita, [Software disenchantment](http://tonsky.me/blog/disenchantment/), qui fait le constat qu'il y a comme un problème dans le développement web – ou plus globalement le développement logiciel.
Même si Nikita fait parfois l'impasse sur des points de compréhension importants – choix technologiques, prise en compte de spécificités de certains logiciels, contextes de développement – ce texte est bienvenu !
