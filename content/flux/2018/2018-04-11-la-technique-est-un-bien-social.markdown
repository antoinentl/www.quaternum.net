---
layout: post
title: "La technique est un bien social"
date: "2018-04-11T22:00:00"
comments: true
published: true
description: "Quel plaisir de lire ce riche texte d'Antoine Gelgon, publié dans la revue .txt éditée par B42, et faisant le lien entre Gilbert Simondon, Bernard Stiegler, Matthew B. Crawford et Ivan Illich !"
categories:
- flux
tags:
- design
---
>Je commence ce texte par une affirmation : la technique est un bien social à investir d'urgence et à tout prix.  
[...]  
Qu'elle soit un langage, un objet ou un milieu, la technique est un bien social parce qu'elle est l'une des composantes indispensables à l'existence et à l'évolution des liens sociaux.  
[Antoine Gelgon, Un dialogue à réaliser : design et technique](https://editions-b42.com/produit/txt-3/)

Quel plaisir de lire ce riche texte d'Antoine Gelgon, publié dans la revue _.txt_ éditée par B42, et faisant le lien entre Gilbert Simondon, Bernard Stiegler, Matthew B. Crawford et Ivan Illich !
