---
layout: post
title: "La fabrique du livre"
date: "2018-04-13T22:00:00"
comments: true
published: true
description: "Une vidéo de François Bon 30 minutes, qui résume très bien l'écosystème de l'édition aujourd'hui, avec les bouleversements induits par le numérique, Internet, le Web, l'impression à la demande, et la possibilité pour les auteurs de reprendre la main. Incontournable donc – même si on n'est pas d'accord."
categories:
- flux
tags:
- publication
---
>À quel moment cette fabrique technique du livre – [ce moment où] le livre prend sa forme matérielle directement pour le lecteur – ne suppose plus la médiation technique de ces maisons [d'édition] avec pignon sur rue ?  
[François Bon, De l'accès à l'édition](https://www.youtube.com/watch?v=NwvMCpMaR2Y)

Une vidéo de François Bon 30 minutes, qui résume très bien l'écosystème de l'édition aujourd'hui, avec les bouleversements induits par le numérique, Internet, le Web, l'impression à la demande, et la possibilité pour les auteurs de reprendre la main. Incontournable donc – même si on n'est pas d'accord.

Via Neil Jomunsi qui en a fait [un billet](https://page42.org/edition-ecrivains-portes-sorties-voies-garage/).
