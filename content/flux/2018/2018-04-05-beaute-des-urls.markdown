---
layout: post
title: "Beauté des URLs"
date: "2018-04-05T22:00:00"
comments: true
published: true
description: "Les URLs sont beaux, c'est un exemple de design réussi."
categories:
- flux
tags:
- web
---
>It’s beautiful in its simplicity.  
[Brendan Dawes, I Heart URLs](http://brendandawes.com/blog/ihearturls)

Les URLs sont beaux, c'est un exemple de design réussi.
