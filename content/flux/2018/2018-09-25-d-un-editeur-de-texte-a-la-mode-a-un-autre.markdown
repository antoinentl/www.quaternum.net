---
layout: post
title: "D'un éditeur de texte à la mode à un autre"
date: "2018-09-25T22:00:00"
comments: true
published: true
description: "Le problème dans le domaine des éditeurs de code/texte depuis une dizaine d'années c'est l'effet de mode. On se croirait à l'aube des années 2010 avec les navigateurs web. Atom et Visual Studio Code sont deux très bons éditeurs de texte, mais les développeurs web semblent se comporter comme des girouettes en passant vite du premier au second, de la même façon que Sublime Text avait été largement abandonné pour Atom. Construire nos outils requièrent un effort certain, qui ne peut pas dépendre que des effets de mode."
categories:
- flux
tags:
- outils
slug: d-un-editeur-de-texte-a-la-mode-a-un-autre
---
> This is for all people, who want to give a chance to VS Code, but ❤️ Atom too much.  
[Samuel Snopko, How I moved from Atom to VS Code](https://medium.com/@samuells/how-i-moved-from-atom-to-vs-code-7c2a1bb9d08c)

Le problème dans le domaine des éditeurs de code/texte depuis une dizaine d'années c'est l'effet de mode.
On se croirait à l'aube des années 2010 avec les navigateurs web.
Atom et Visual Studio Code sont deux très bons éditeurs de texte, mais les développeurs web semblent se comporter comme des girouettes en passant vite du premier au second, de la même façon que Sublime Text avait été largement abandonné pour Atom.
Construire nos outils requièrent un effort certain, qui ne peut pas dépendre que des effets de mode.
