---
layout: post
title: "S'appartenir"
date: "2018-02-06T22:00:00"
comments: true
published: true
description: "De l'intérêt d'avoir son propre site web : écrire, expérimenter, être indépendant, ne rien faire."
categories:
- flux
tags:
- publication
slug: s-appartenir
---
>But it’s going to happen. Here, on my own site.  
[Tim Kadlec, Owning My Own Content](https://timkadlec.com/2018/01/owning-my-own-content/)

De l'intérêt d'avoir son propre site web : écrire, expérimenter, être indépendant, ne rien faire.

Via [Jeremy Keith](https://adactio.com/links/13280).
