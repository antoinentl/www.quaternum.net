---
layout: post
title: "L'édition à l'ère numérique"
date: "2018-06-01T22:00:00"
comments: true
published: true
description: "Enfin un livre sur l'édition numérique ! Après une décennie de publications sur le livre numérique, voici un ouvrage nécessaire pour toute personne curieuse du bouleversement induit par le numérique sur le processus éditorial, disponible dans toutes les bonnes librairies (physiques ou numériques) et sur plusieurs plateformes (donc Cairn.info)."
categories:
- flux
tags:
- livre
slug: l-edition-a-l-ere-numerique
---
>Le numérique est en train de remodeler l’ensemble du processus de production du savoir, de validation des contenus et de diffusion des connaissances. En cause : l’émergence de nouveaux outils et de nouvelles pratiques d’écriture et de lecture, mais aussi un changement plus global que l’on pourrait qualifier de culturel.  
[Benoît Epron et Marcello Vitali-Rosati, L'édition à l'ère numérique](http://www.collectionreperes.com/catalogue/index-L___dition____l___re_num__rique-9782707199355.html)

Enfin un livre sur l'édition numérique ! Après une décennie de publications sur le "livre numérique", voici un ouvrage nécessaire pour toute personne curieuse du bouleversement induit par le numérique sur le processus éditorial, disponible dans toutes les bonnes librairies (physiques ou numériques) et sur plusieurs plateformes (donc Cairn.info).

Mise à jour : le livre est désormais [disponible en open access sous forme de PDF](https://papyrus.bib.umontreal.ca/xmlui/handle/1866/20642), c'est suffisamment rare pour devoir être souligné !
