---
layout: post
title: "Drawing Curved"
date: "2018-02-16T22:00:00"
comments: true
published: true
description: "Un livre sur les courbes."
categories:
- flux
tags:
- livre
---
>Drawing Curved is a collection of texts and images concerned with digital curvature. It seeks to understand when a curve starts to be tangible, or what might give a sense that they can or can't be handled. What makes a curve smooth? When does it seem appropriate to interrupt it?  
[Pierre Huyghebaert, Colm O'Neill, Femke Snelting, Drawing Curved](http://drawingcurved.osp.kitchen/foreword.xhtml)

Un livre sur les courbes.

>Together, we have been trying to understand the virtue of curves in drawing software. Why is it that when we bend physical plastic or wood, we know that they will yield different results, but when we want to adjust the fluidity of a curved line on a computer screen, we have no notion of origin or space, let alone the properties of the segment ― or even whether it is a material at all.
