---
layout: post
title: "Croyances numériques"
date: "2018-01-23T22:00:00"
comments: true
published: true
description: "Éric Guichard, philosophe et maître de conférences à l'enssib, critique la façon dont le numérique devient un instrument du déterminisme de l'innovation, mais également comment certaines instances (ici l'Agence nationale de la recherche) le défendent à la façon d'une religion."
categories:
- flux
tags:
- métier
---
>[...] ce ne sont pas les algorithmes qui vont transformer la société et la science, mais ses gestionnaires et toutes les personnes qui aiment les solutions simples.  
[...]  
La science n’est pas menacée par le pouvoir des algorithmes, elle est malade du pouvoir de ses gestionnaires pétris de croyances en le déterminisme de l’innovation.  
[Éric Guichard, La science contaminée par les croyances numériques](http://www.liberation.fr/debats/2017/12/29/la-science-contaminee-par-les-croyances-numeriques_1619541)

Éric Guichard, philosophe et maître de conférences à l'enssib, critique la façon dont le numérique devient un instrument du "déterminisme de l'innovation", mais également comment certaines instances (ici l'Agence nationale de la recherche) le défendent à la façon d'une religion.

>L’écriture, vue comme technologie de l’intellect, est désormais inaccessible dans sa version numérique à la majorité de nos collègues. Ce qui réduit drastiquement la possibilité d’une pensée critique, hier caractéristique des sciences sociales.
