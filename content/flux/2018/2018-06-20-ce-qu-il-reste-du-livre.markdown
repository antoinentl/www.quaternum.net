---
layout: post
title: "Ce qu'il reste du livre"
date: "2018-06-20T22:00:00"
comments: true
published: true
description: "Arthur Perret a rédigé un article encore plus complet que son intervention lors du colloque ÉCRIDIL en avril 2018 à Montréal, un sujet passionnant et passionnément traité."
categories:
- flux
tags:
- typographie
slug: ce-qu-il-reste-du-livre
---
>Je pars d’un postulat qui est le suivant : la typographie, c’est la seule chose qui reste du papier dans le livre numérique. Et c’est précisément ce qui en fait un objet d’étude intéressant : c’est un point de passage entre la page et l’écran. Quel sens peut-on produire avec un système de signes changeant ? Je décline cette question en trois courtes parties : caractères, signes et frictions, pour parler plus spécifiquement de sémiotique, d’énonciation et de design.  
[Arthur Perret, Sémiotique des fluides](https://infologie.hypotheses.org/33)

Arthur Perret a rédigé un article encore plus complet que son intervention lors du colloque ÉCRIDIL en avril 2018 à Montréal, un sujet passionnant et passionnément traité.
