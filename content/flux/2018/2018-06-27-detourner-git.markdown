---
layout: post
title: "Détourner Git"
date: "2018-06-27T22:00:00"
comments: true
published: true
description: "Brendan Dawes détourne Git, en quelque sorte, pour créer facilement la documentation d'un projet. C'est une initiative originale qui démontre combien les usages de ce système de gestion de versions peuvent être variés."
categories:
- flux
tags:
- design
---
>All my work is tracked through a Git repository — a way to track code changes over time, complete with comments on why something has changed or what that commit was about. In conjunction with that I take timestamped screenshots. These two things combined — words and image — have the side-effect of creating a document of the making process.  
[Brendan Dawes, Using a Git Repo to create a physical document of the work](http://brendandawes.com/blog/gitbook)

Brendan Dawes détourne Git, en quelque sorte, pour créer facilement la documentation d'un projet. C'est une initiative originale qui démontre combien les usages de ce système de gestion de versions peuvent être variés.
