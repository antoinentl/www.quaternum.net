---
layout: post
title: "Neurocapitalisme"
date: "2018-06-15T22:00:00"
comments: true
published: true
description: "Compte rendu d'une conférence qui met en avant le rôle pédagogique que peut jouer Wikipédia dans différents contextes, en creux dans une démarche éditoriale."
categories:
- flux
tags:
- livre
---
>Giorgio Griziotti brosse la fresque de l'évolution fondamentale du capitalisme, depuis la production des objets jusqu'à celle de l'économie de l'attention, de la connaissance et des affects. Il montre comment, après avoir mis en place la connexion permanente, la numérisation gagne aujourd'hui les corps, sinon le code génétique de la vie même. Avec le biohypermédia, nos vies sont prises dans un réseau dominé par quelques acteurs qui accaparent toute l'énergie collective.  
[C&F, Neurocapitalisme, Pouvoirs numériques et multitudes](https://cfeditions.com/neurocapitalisme/)

C&F, la maison d'édition de qualité qui publie notamment des textes majeurs sur le numérique, propose ce livre de Giorgio Griziotti. Une critique du capitalisme numérique qui semble pertinente.
