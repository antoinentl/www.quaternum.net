---
layout: post
title: "Pourquoi développer un éditeur sémantique pour les humanités"
date: "2018-10-27T22:00:00"
comments: true
published: true
description: "Après la sortie de l'éditeur en ligne Stylo, voici une présentation complète et bienvenue pour comprendre la démarche du laboratoire Écritures numériques. L'intérêt d'un tel projet dépasse largement le domaine de l'édition scientifique."
categories:
- flux
tags:
- publication
---
> Ce bricolage constant est important. [...] Mais ces mains pleines de cambouis, de balises, de commandes bash, de branches fusionnées, sont les témoins de leur capacitation, d'une nouvelle maîtrise qui va bien au-delà de l'édition d'un document.  
[Nicolas Sauret, Stylo : éditeur sémantique pour les humanités](http://nicolassauret.net/s_StyloCRIHN/)

Après la sortie de l'éditeur en ligne [Stylo](https://stylo.ecrituresnumeriques.ca/), voici une présentation complète et bienvenue pour comprendre la démarche du laboratoire Écritures numériques.
L'intérêt d'un tel projet dépasse largement le domaine de l'édition scientifique.
