---
layout: post
title: "Entretien avec Craig Mod"
date: "2018-03-05T22:00:00"
comments: true
published: true
description: "Voici un entretien passionnant de Craig Mod pour la revue Offscreen, où il est question de livre, de technologie, d'écriture, de voyages."
categories:
- flux
tags:
- publication
---
>Seven years ago, the ebook space was invigorated with young energy. Remember Nook, Kobo, or even iBooks on the then newly launched iPad? Flipboard emerged from that era, too. But now that the Kindle owns the majority of the market, these other platforms (Readmill, I gaze in your direction with a tear in my eye) folded or got swallowed by Amazon. And the Kindle turns out to be a good enough solution for enough people — it sells well enough as is — which has, I believe, disincentivized its evolution. It hasn’t changed much since it launched. So here we are.  
[Craig Mod, Offscreen Magazine Interview](https://craigmod.com/essays/offscreen_interview/)

Voici un entretien passionnant avec Craig Mod pour la revue Offscreen, où il est question de livre, de technologie, d'écriture, de voyages.

>But I think books are the perfect disconnected objects. They require no energy. They offer a fully immersive, quiet experience for hours or days. The medium dissolves but never becomes translucent. It’s quiet, but present. An exceptional technology.
