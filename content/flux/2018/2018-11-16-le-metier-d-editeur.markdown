---
layout: post
title: "Le métier d'éditeur"
date: "2018-11-16T22:00:00"
comments: true
published: true
description: "En quatre questions le Bulletin des bibliothèques de France donne la parole à Hervé Le Crosnier, co-fondateur de C&F Éditions. Quatre réponses qui définissent ce qu'est le métier d'éditeur aujourd'hui."
categories:
- flux
tags:
- métier
slug: le-metier-d-editeur
---
> Commençons par ce qui me semble ne pas être un enjeu : il n’y a pas, pour un éditeur, de différence majeure entre l’imprimé et le numérique. Le travail de réalisation reste essentiellement le même.  
[...]  
Le modèle économique du livre reste, quel que soit le support, celui de la délivrance d’une unité de culture pour un lecteur, qui peut ensuite le faire circuler autour de lui, soit comme institution (bibliothèque, école), soit comme lecteur individuel, souvent avide et passionné.  
[Hervé Le Crosnier, Éditer à l’ère numérique](http://bbf.enssib.fr/consulter/bbf-2018-15-0046-006)

En quatre questions le Bulletin des bibliothèques de France donne la parole à Hervé Le Crosnier, co-fondateur de C&F Éditions.
Quatre réponses qui définissent ce qu'est le métier d'éditeur aujourd'hui.
