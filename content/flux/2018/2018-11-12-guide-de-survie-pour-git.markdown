---
layout: post
title: "Guide de survie pour Git"
date: "2018-11-12T22:00:00"
comments: true
published: true
description: "Une ressource pour comprendre les commandes de base dans Git, ou pour se sortir de situations difficiles (par exemple Je veux rechercher une chaîne de caractères dans un commit). Bref un guide incontournable, et en plusieurs langues."
categories:
- flux
tags:
- outils
---
> Un guide pour les astronautes (pour les développeur·euse·s utilisant Git, dans le cas présent) sur quoi faire quand les choses se passent mal.  
[Kate Hudson, Règles de vol pour Git](https://github.com/k88hudson/git-flight-rules/blob/master/README_fr.md)

Une ressource pour comprendre les commandes de base dans Git, ou pour se sortir de situations difficiles (par exemple ["Je veux rechercher une chaîne de caractères dans un commit"](https://github.com/k88hudson/git-flight-rules/blob/master/README_fr.md#je-veux-rechercher-une-cha%C3%AEne-de-caract%C3%A8res-dans-un-commit)).
Bref un guide incontournable, et en plusieurs langues.
