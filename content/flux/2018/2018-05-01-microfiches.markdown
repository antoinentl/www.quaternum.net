---
layout: post
title: "Microfiches"
date: "2018-05-01T22:00:00"
comments: true
published: true
description: "Je découvre une riche collection de cartes illustrées, sous licence Creative Commons BY-NC-SA, et donc réutilisables facilement. Ces microfiches sont idéales pour illustrer des concepts complexes."
categories:
- flux
tags:
- outils
---
>Les livres demeurent des sources importantes.  
[Mathieu Labrecque, Audrey Laplante et Nadine Desrochers, Trouver un livre](http://microfiches.org/collections/competences-informationnelles/)

Je découvre une riche collection de cartes illustrées, sous licence Creative Commons BY-NC-SA, et donc réutilisables facilement. Ces microfiches sont idéales pour illustrer des concepts complexes.

Via [Olivier Charbonneau](http://www.culturelibre.ca/).
