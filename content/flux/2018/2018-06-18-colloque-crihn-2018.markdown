---
layout: post
title: "Repenser les humanités numériques"
date: "2018-06-18T22:00:00"
comments: true
published: true
description: "Voici l'appel à communications pour le prochain colloque du Centre de recherche interuniversitaire sur les humanités numériques de l'Université de Montréal, les axes proposés sont intéressants (dont l'édition numérique) ! Date limite fixée au 13 juillet 2018 pour proposer des interventions."
categories:
- flux
tags:
- métier
slug: colloque-crihn-2018
---
>L’appel à communications pour le colloque « Repenser les humanités numériques / Thinking the Digital Humanities Anew » est basé sur les trois axes de la programmation scientifique du CRIHN construit autour de la question fondamentale de la transformation des sciences humaines par les humanités numériques :  
Axe 1 : Production – Écritures numériques et Éditorialisation  
Axe 2 : Circulation – Passage au numérique et recontextualisation  
Axe 3 : Validation – Légitimation des contenus  
[Écritures numériques, Colloque CRIHN 2018 : Repenser les humanités numériques](http://ecrituresnumeriques.ca/fr/Activites/Evenements/2018/6/2/Colloque-CRIHN-twozerooneeight--Repenser-les-humanites-numeriques)

Voici l'appel à communications pour le prochain colloque du Centre de recherche interuniversitaire sur les humanités numériques de l'Université de Montréal, les axes proposés sont intéressants (dont l'édition numérique) ! Date limite fixée au 13 juillet 2018 pour proposer des interventions.

Mise à jour : l'appel à communications est ouvert jusqu'au 31 août 2018.
