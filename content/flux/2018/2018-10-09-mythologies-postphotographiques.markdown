---
layout: post
title: "Mythologies postphotographiques"
date: "2018-10-09T22:00:00"
comments: true
published: true
description: "Voici un nouveau titre de la collection Parcours numériques des Presses de l'Université de Montréal, Servanne Monjour y explore les mythologies photographiques, les questions de remédiation ou encore la reconfiguration de notre regard contemporain. Un essai que j'ai que feuilleté, à lire en papier ou en numérique."
categories:
- flux
tags:
- livre
---
> La période que nous traversons s’avère passionnante, car elle fait cohabiter, pour quelque temps encore, l’analogique et le numérique.  
[Servanne Monjour, Mythologies postphotographiques](http://www.parcoursnumeriques-pum.ca/introduction-158)

Voici un nouveau titre de la collection [Parcours numériques](http://www.parcoursnumeriques-pum.ca/) des Presses de l'Université de Montréal, Servanne Monjour y explore les mythologies photographiques, les questions de remédiation ou encore la "reconfiguration de notre regard contemporain".
Un essai que j'ai que feuilleté, à lire en papier ou en numérique.
