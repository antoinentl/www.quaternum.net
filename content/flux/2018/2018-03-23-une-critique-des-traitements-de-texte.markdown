---
layout: post
title: "Une critique des traitements de texte"
date: "2018-03-23T22:00:00"
comments: true
published: true
description: "Article stimulant sur l'origine des traitements de texte, leurs fonctionnements ou logiques, et les aternatives permettant d'envisager de s'en passer."
categories:
- flux
tags:
- publication
---
>Les traitements de texte développent cette pensée à partir de présupposés implicites qui apparaissent comme autant d’enthymèmes. En utilisant un traitement de texte, nous admettons un certain nombre de leurs prémisses.  
[Julien Dehut, En finir avec Word ! Pour une analyse des enjeux relatifs aux traitements de texte et à leur utilisation](https://eriac.hypotheses.org/80)

Article stimulant sous forme de critique construite des traitements de texte : leur origine, leurs fonctionnements ou logiques, et les aternatives permettant d'envisager de s'en passer. Une version plus académique de l'article de [iA, Multichanel Text Processing](https://www.quaternum.net/2016/08/31/traitement-de-texte-multicanal/).
