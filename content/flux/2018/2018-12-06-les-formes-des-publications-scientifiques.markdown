---
layout: post
title: "Les formes des publications scientifiques"
date: "2018-12-06T22:00:00"
comments: true
published: true
description: "Arthur Perret continue ses recherches pour déconstruire l'activité de publication, en tant qu'auteur tout en sachant que cela concerne également les éditeurs : se passer d'un traitement de texte classique, distinguer le contenu de sa forme, faciliter la production de documents (avec Pandoc) et augmenter la qualité du design des articles."
categories:
- flux
tags:
- publication
---
>Une fois de plus, répéter à quel point la reprise en main des outils est incontournable.  
[Arthur Perret, Sémantique et mise en forme : ouvrir la boîte de Pandoc ?](https://infologie.hypotheses.org/516)

Arthur Perret continue ses recherches pour déconstruire l'activité de publication, en tant qu'auteur tout en sachant que cela concerne également les éditeurs : se passer d'un traitement de texte classique, distinguer le contenu de sa forme, faciliter la production de documents (avec Pandoc) et augmenter la qualité du design des articles.
