---
layout: post
title: "Empreinte digitale numérique"
date: "2018-11-05T22:00:00"
comments: true
published: true
description: ""
categories:
- flux
tags:
- privacy
---
> Initially developed for security purposes, browser fingerprinting (also known as device fingerprinting) is a tracking technique capable of identifying individual users based on their browser and device settings. In order for websites to display correctly, your browser makes certain information available about your device, including your screen resolution, operating system, location, and language settings. These details essentially make up the ridges of your digital fingerprint.  
[Nick Briz et Clayton d'Arnault, This is Your Digital Fingerprint](https://thedisconnect.co/two/your-digital-fingerprint)

Un article de Nick Briz et Clayton d'Arnault dans le deuxième numéro de The Disconnect (la revue numérique lisible hors connexion uniquement) : nous avons tous une empreinte numérique unique lorsque nous consultons des sites web, liée à la configuration de notre navigateur web, et il est nécessaire d'en prendre conscience.
