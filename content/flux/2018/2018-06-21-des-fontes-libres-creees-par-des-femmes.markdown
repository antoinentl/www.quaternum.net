---
layout: post
title: "Des fontes libres créées par des femmes"
date: "2018-06-21T22:00:00"
comments: true
published: true
description: "Plus de soixante fontes de qualité, créées par des femmes, et disponibles librement (utilisation, modification, partage)."
categories:
- flux
tags:
- typographie
---
>This collection aims at giving visibility to libre fonts drawn by womxn designers, who are often underrepresented in the traditionally conservative field of typography.  
[Loraine Furter, Libre Fonts by Womxn](http://design-research.be/by-womxn/)

Plus de soixante fontes de qualité, créées par des femmes, et disponibles librement (utilisation, modification, partage).
