---
layout: post
title: "The Evergreen"
date: "2018-06-19T22:00:00"
comments: true
published: true
description: "Lecteur assidu de l'incontournable lettre d'information d'Anselm Hannemann, Web Development Reading List, je n'avais pas vu cette page bien utile : une liste de ressources qui sont toujours d'actualité même après plusieurs mois ou années. C'est par ailleurs une dimension intéressante d'une veille hebdomadaire : fixer certaines informations dans le temps long."
categories:
- flux
tags:
- web
---
>Welcome to The Evergreen List! You can find useful links and articles to each category here that stay relevant for a longer time.  
[Anselm Hannemann, The Evergreen](https://wdrl.info/evergreen)

Lecteur assidu de l'incontournable lettre d'information d'Anselm Hannemann, Web Development Reading List, je n'avais pas vu cette page bien utile : une liste de ressources qui sont toujours d'actualité même après plusieurs mois ou années. C'est par ailleurs une dimension intéressante d'une veille hebdomadaire : fixer certaines informations dans le temps long.
