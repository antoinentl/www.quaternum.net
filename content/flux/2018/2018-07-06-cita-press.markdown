---
layout: post
title: "Cita Press"
date: "2018-07-06T22:00:00"
comments: true
published: true
description: "Un projet éditorial porté par Juliana Castro, intéressant pour plusieurs raisons : disponibilité des textes, licence (y compris pour les créations graphiques), versions imprimée et numérique (en ligne), créations graphiques, démarche politique."
categories:
- flux
tags:
- publication
---
>Carefully designed public-domain books written by women in free, contemporary editions for print and web. [...] cita is a collaborative labor of love between designers and writers that relies on public-domain writings and open-source texts, fonts, code, and images. [...] Women authors have historically been underrepresented and underpublicized in the male-dominated, profit-driven publishing industry. We make these women writers’ works accessible to all, in free editions.  
[Juliana Castro, Cita Press](http://citapress.org)

Un projet éditorial porté par Juliana Castro, intéressant pour plusieurs raisons : disponibilité des textes, licence (y compris pour les créations graphiques), versions imprimée et numérique (en ligne), créations graphiques, démarche politique.
