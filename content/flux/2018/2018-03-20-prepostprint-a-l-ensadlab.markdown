---
layout: post
title: "PrePostPrint à l'EnsadLab"
date: "2018-03-20T22:00:00"
comments: true
published: true
description: "L'EnsadLab – c'est-à-dire Julie Blanc, Quentin Juhel et Lucile Haute – organise un événement autour des systèmes de publication, les mardi 3 et mercredi 4 avril 2018 à l'Ensad (Paris), entrée libre sur inscription."
categories:
- flux
tags:
- publication
slug: prepostprint-a-l-ensadlab
---
>Enjeux de systèmes de publication libres et outils alternatifs pour la création graphique.  
[PrePostPrint, EnsadLab invite PrePostPrint](https://prepostprint.org/doku.php/fr/ensadlab)

L'EnsadLab – c'est-à-dire Julie Blanc, Quentin Juhel et Lucile Haute – organise un événement autour des systèmes de publication, les mardi 3 et mercredi 4 avril 2018 à l'Ensad (Paris), [entrée libre sur inscription](https://prepostprint.org/doku.php/fr/ensadlab). J'interviendrai sur certains aspects des chaînes de publication modulaires.
