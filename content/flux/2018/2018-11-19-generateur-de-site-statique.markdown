---
layout: post
title: "Générateur de site statique"
date: "2018-11-19T22:00:00"
comments: true
published: true
description: "Karl définit avec ses mots ce qu'est un générateur de site statique, à travers ce billet et trois autres (ici, là, et là). Une approche qui semble assez éloignée des discours habituels, je ne suis pas d'accord avec tout mais cela fait du bien d'avoir des visions différentes. Courez lire ces réflexions rafraîchissantes !"
categories:
- flux
tags:
- web
---
> Il y a deux mots importants dans le projet initial : générateur et statique. Par générateur, on entend une moulinette, un (ou plusieurs scripts) qui traverse la structure d'un contenu et en génère une forme prête à être traitée par un navigateur Web. Le terme statique recouvre déjà beaucoup plus de subtilités. Il n'a pas la même signification pour nous tous. Donc il faudra peut-être mieux le définir ou changer de termes.  
[Karl Dubost, Le sculpteur de carnet Web](http://www.la-grange.net/2018/11/17/site-statique)

Karl définit avec ses mots ce qu'est un générateur de site statique, à travers ce billet et trois autres ([ici](http://www.la-grange.net/2018/11/18/generateur), [là](http://www.la-grange.net/2018/11/19/statique), et [là](http://www.la-grange.net/2018/11/20/hebergement)).
Une approche qui semble assez éloignée des discours habituels, je ne suis pas d'accord avec tout mais cela fait du bien d'avoir des visions différentes.
Courez lire ces réflexions rafraîchissantes !
