---
layout: post
title: "Un historique lisible"
date: "2018-09-14T22:00:00"
comments: true
published: true
description: "Quelques conseils pour conserver un historique des commits le plus lisible possible dans l'utilisation du système de gestion de versions Git. Comme souvent avec Git, ces principes sont applicables plus globalement pour n'importe quelle gestion de projet, personnelle ou collective."
categories:
- flux
tags:
- métiers
---
> Commits are one of the key parts of a Git repository, and more so, the commit message is a life log for the repository.[...] So it's important that these messages reflect the underlying change in a short, precise manner.  
[Kushal Pandya, How (and why!) to keep your Git commit history clean](https://about.gitlab.com/2018/06/07/keeping-git-commit-history-clean/#situation-4-my-commit-history-doesnt-make-sense-i-need-a-fresh-start)

Quelques conseils pour conserver un historique des _commits_ le plus lisible possible dans l'utilisation du système de gestion de versions Git.
Comme souvent avec Git, ces principes sont applicables plus globalement pour n'importe quelle gestion de projet, personnelle ou collective.
