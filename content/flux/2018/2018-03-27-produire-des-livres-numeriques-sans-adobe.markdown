---
layout: post
title: "Produire des livres (numériques) sans Adobe"
date: "2018-03-27T22:00:00"
comments: true
published: true
description: "Remplacer les outils Adobe par des outils ouverts et d'autres fonctionnements ? C'est possible, Simon Collinson et Nick Barreto présente plusieurs pistes techniques – à base de Pandoc, d'Imagemagick et de terminal."
categories:
- flux
tags:
- publication
---
>In this talk, Simon Collinson and Nick Barreto of Canelo Digital Publishing will walk the audience through their Adobe-free toolbelt, giving tips, tricks, and warning of potholes to avoid. The overriding theme will be the fun/challenge of building a heterogeneous toolchain, rolling your own tools, and the efficiencies you can find if you can engineer your workflow from the ground up.  
[Simon Collinson and Nick Barreto, Life After Adobe](https://www.slideshare.net/booknetcanada/life-after-adobe-nick-barreto-simon-collinson-ebookcraft-2018-91429939)

Remplacer les outils Adobe par des outils ouverts et d'autres fonctionnements&nbsp;? C'est possible, Simon Collinson et Nick Barreto présente plusieurs pistes techniques – à base de Pandoc, d'Imagemagick et de terminal. Cette présentation faisait partie de [ebookcraft](https://www.booknetcanada.ca/ebookcraft/), un événement dédié au livre numérique (d'autres supports de présentation sont disponibles [ici](https://www.slideshare.net/booknetcanada/tag/ebookcraft), en attendant les vidéos).

Via [Jiminy](https://twitter.com/JiminyPan/status/978692476717490176).
