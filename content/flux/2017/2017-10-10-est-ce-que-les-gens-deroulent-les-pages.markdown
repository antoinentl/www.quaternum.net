---
layout: post
title: "Est-ce que les gens déroulent les pages ?"
date: "2017-10-10T17:00:00"
comments: true
published: true
description: "Pour savoir si les gens déroulent les pages, il suffit de scroller..."
categories:
- flux
tags:
- web
---
>Yes they do  
[Erez Eden, Do users scroll?](https://www.peopledontscroll.com/)

Pour savoir si les gens *déroulent* les pages, il suffit de *scroller*...

Via [Laurent](https://twitter.com/lrtrln/status/917707890638671872).