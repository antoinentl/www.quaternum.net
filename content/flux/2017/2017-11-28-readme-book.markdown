---
layout: post
title: "README.book"
date: "2017-11-28T17:00:00"
comments: true
published: true
description: "Avec Thomas Parisot nous avons proposé une conférence autour du livre et du développement à Codeurs en Seine, voici la vidéo de cette présentation qui aborde les questions d'écriture, de relecture, d'interfaces, d'édition, de fabrication et de production, dans le domaine du livre."
categories:
- flux
tags:
- publication
slug: readme-book
---
>On veut vous parler de livre et de développement.  
[Thomas Parisot et Antoine Fauchié, README.book](https://www.youtube.com/watch?v=25wCiZVLNBg)

Avec [Thomas Parisot](https://oncletom.io/) nous avons proposé une conférence autour du livre et du développement à [Codeurs en Seine](http://www.codeursenseine.com/2017/), voici la vidéo de cette présentation qui aborde les questions d'écriture, de relecture, d'interfaces, d'édition, de fabrication et de production, dans le domaine du livre.