---
layout: post
title: "Incident et transparence"
date: "2017-02-03T10:00:00"
comments: true
published: true
description: "GitLab -- la plateforme et le service basés sur Git, concurrent à GitHub -- a eu un incident majeur il y a quelques jours, avec fermeture de gitlab.com pendant plusieurs heures, et perte de données... Ce qui est intéressant ici c'est la façon dont la crise a été gérée : communication très transparente, explications détaillées, et même une retransmission en direct de la récupération de la base de données. De l'humain dans le code."
categories: 
- flux
tags:
- métier
---
>Yesterday we had a serious incident with one of our databases.  
[GitLab, GitLab.com Database Incident](https://about.gitlab.com/2017/02/01/gitlab-dot-com-database-incident/)

GitLab -- la plateforme et le service basés sur Git, concurrent à GitHub -- a eu un incident majeur il y a quelques jours, avec fermeture de gitlab.com pendant plusieurs heures, et perte de données... Ce qui est intéressant ici c'est la façon dont la crise a été gérée : communication très transparente, explications détaillées, et même une retransmission en direct de la récupération de la base de données. De l'humain dans le code, mais aussi une communication bien menée.