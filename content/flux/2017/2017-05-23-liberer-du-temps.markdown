---
layout: post
title: "Libérer du temps"
date: "2017-05-23T22:00:00"
comments: true
published: true
description: "Thomas Parisot a raconté son expérience de temps partiel à la conférence MiXiT 2017 : comment libérer du temps pour faire autre chose que du travail, pour faire beaucoup de choses, et aussi mieux travailler."
categories:
- flux
tags:
- offline
---
>En fait j'ai libéré du temps.  
[Thomas Parisot, Travailler moins pour gagner moins](https://mixitconf.org/2017/travailler-moins-pour-gagner-moins)

[Thomas Parisot](https://oncletom.io/) a raconté son expérience de temps partiel à la conférence MiXiT 2017 : comment libérer du temps pour faire autre chose que du travail, pour faire beaucoup de choses, et aussi mieux travailler.
