---
layout: post
title: "Service Workers compatible avec Safari et Edge"
date: "2017-12-21T22:00:00"
comments: true
published: true
description: "Service Workers est une fonctionnalité désormais activée par défaut dans Safari, ce qui veut dire que les Progressive Web Applications pourront être un peu plus compatibles avec le navigateur d'Apple. Et Edge – le navigateur de Windows – suit le même mouvement !"
categories:
- flux
tags:
- offline
---
>Made Service Workers enabled by default  
[Jonathan Davis, Release Notes for Safari Technology Preview 46](https://webkit.org/blog/8042/release-notes-for-safari-technology-preview-46/)

Service Workers est une fonctionnalité désormais activée par défaut dans Safari, ce qui veut dire que les Progressive Web Applications pourront être un peu plus compatibles avec le navigateur d'Apple. Et [Edge – le navigateur de Windows – suit](https://blogs.windows.com/msedgedev/2017/12/19/service-workers-going-beyond-page/) le même mouvement !

Via [Hubert](https://twitter.com/hsablonniere/status/943545996059070467).
