---
layout: post
title: "Pourquoi un carnet web en 2017"
date: "2017-02-06T19:00:00"
comments: true
published: true
description: "Les reprises de blogs et carnets en ligne sont de plus en plus nombreuses : indépendantes, intimes, ouvertes, curieuses."
categories: 
- flux
tags:
- publication
---
>En ces temps fait d’incertitude avec parfois l’impression d’être devant un immense vide, où les plus grandes certitudes sont ignorées, bafouées, où la haine est attisée de toutes parts alors que pourtant tout pourrait être si clair et si lumineux, consigner aujourd’hui le futur passé demeure, à mes yeux, le meilleur moyen de ne pas se perdre en route.  
[Hoedic, Pourquoi bloguer en 2017](http://mon-ile.net/carnet/2017/01/31/bloguer-2017/)

Les reprises de blogs et carnets en ligne sont de plus en plus nombreuses : indépendantes, intimes, ouvertes, curieuses.