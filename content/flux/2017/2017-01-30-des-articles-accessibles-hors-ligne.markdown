---
layout: post
title: "Des articles accessibles hors ligne"
date: "2017-01-30T22:00:00"
comments: true
published: true
description: "Una Kravets présente la façon dont elle a implémenté des Service Workers pour une lecture hors de ses articles."
categories: 
- flux
tags:
- offline
---
>I recently added an option to save blog posts for offline reading, and this post will detail how I did that so you can too.  
[Una Kravets, Implementing "Save For Offline" with Service Workers](https://una.im/save-offline/)

Una Kravets présente la façon dont elle a implémenté des Service Workers pour une lecture hors ligne de ses articles.