---
layout: post
title: "Conception des chaînes éditoriales"
date: "2017-11-21T22:00:00"
comments: true
published: true
description: "Thèse de Thibaut Arribe sur le sujet des chaînes éditoriales."
categories:
- flux
tags:
- publication
---
>Les travaux présentés dans ce mémoire traitent de la conception des chaînes éditoriales numériques XML : des logiciels de production documentaire qui outillent la rédaction de fragments et l'assemblage de ces fragments pour former des documents. La publication des documents s'opère par transformation de fragments XML en documents numériques aux formats standards. La composition des fragments permet d'instrumenter la rééditorialisation documentaire soit l'usage de contenus existants dans la rédaction de documents originaux.  
[Thibaut Arribe, Conception des chaînes éditoriales](https://ics.utc.fr/~tha/co/Home.html)

Thèse de Thibaut Arribe sur le sujet des chaînes éditoriales.

Via [Julie](http://julie-blanc.fr/).