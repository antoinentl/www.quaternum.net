---
layout: post
title: "Les Rencontres de Lure racontées"
date: "2017-09-11T09:00:00"
comments: true
published: true
description: "L'équipe de l'agence Graphéine raconte les Rencontres de Lure 2017, un bien beau résumé."
categories:
- flux
tags:
- typographie
---
>Au gré de la lune, sous les constellations humaines, parmi les techniques et les univers typographiques, Lurs nous a plongés dans un monde parallèle bienveillant et illuminé.  
[Graphéine, Lurs, constellations graphiques](https://www.grapheine.com/divers/lurs-constellations-graphiques)

L'équipe de l'agence Graphéine raconte [les Rencontres de Lure 2017](http://delure.org/les-rencontres/rencontres-precedentes/rencontres-2017), un bien beau résumé.