---
layout: post
title: "Rester dans les nuages"
date: "2017-04-17T10:00:00"
comments: true
published: true
description: "Il y a quelques semaines l'équipe de GitLab -- le logiciel et le service de gestion de git -- expliquait pourquoi rester dans le cloud était le meilleur choix, toujours dans un effort de transparence très appréciable."
categories: 
- flux
tags:
- web
---
>It's not just the cost of owning and renewing the hardware, it's everything else that comes with it.  
[Sean Packham, Why we are not leaving the cloud](https://about.gitlab.com/2017/03/02/why-we-are-not-leaving-the-cloud/)

Il y a quelques semaines l'équipe de GitLab -- le logiciel et le service de gestion de git -- expliquait pourquoi rester dans le *cloud* était le meilleur choix, toujours dans un effort de transparence très appréciable.