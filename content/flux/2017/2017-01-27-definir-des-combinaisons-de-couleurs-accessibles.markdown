---
layout: post
title: "Définir des combinaisons de couleurs accessibles"
date: "2017-01-27T09:00:00"
comments: true
published: true
description: "Voici un outil qui permet de vérifier que des combinaisons de couleurs -- par exemple les couleurs d'un texte et d'un fond -- respectent un contraste suffisant, et donc restent lisibles par toutes et tous. Un outil bien pratique (utilisable en local)."
categories: 
- flux
tags:
- accessibilité
---
>This is a tool to help designers build color palettes with combinations that conform with accessibility standards.  
[Accessible color palette builder](https://toolness.github.io/accessible-color-matrix/)

Voici un outil qui permet de vérifier que des combinaisons de couleurs -- par exemple les couleurs d'un texte et d'un fond -- respectent un contraste suffisant, et donc restent lisibles par toutes et tous. Un outil bien pratique (utilisable en local).