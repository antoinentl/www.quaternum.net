---
layout: post
title: "La refonte de Smashing Magazine"
date: "2017-03-18T12:00:00"
comments: true
published: true
description: "Smashing Magazine présente sa refonte, basée sur les concepts de JAMstack et d'amélioration progressive, et notamment sur un générateur de site statique, un bel exemple de développement web moderne !"
categories: 
- flux
tags:
- design
---
>What’s different? Well, everything. The website won’t be running on WordPress anymore; in fact, it won’t have a back end at all. We are moving to a JAMstack: articles published directly to Netlify CDNs, with a custom shop based on an open-sourced headless E-Commerce GoCommerce and a job board that’s all just static HTML; content editing with Netlify’s new open-source, Git-Based CMS, real-time search powered by Algolia, full HTTP/2 support, and the whole website running as a progressive web app with a service worker in the background (thanks to the awesome Service Worker Toolbox library). Booo-yah!  
[Vitaly Friedman, A Little Surprise Is Waiting For You Here — Meet The Next Smashing Magazine](https://next.smashingmagazine.com/2017/03/a-little-surprise-is-waiting-for-you-here--meet-the-next-smashing-magazine/)

Smashing Magazine présente sa refonte, basée sur les concepts de JAMstack et d'amélioration progressive, et notamment sur un générateur de site statique, un bel exemple de développement web moderne !