---
layout: post
title: "L'origine du Web"
date: "2017-12-12T10:00:00"
comments: true
published: true
description: "Jeremy Keith fait une distinction très intéressante entre d'un côté le constat que les premières utilisations du Web aient été autour du texte et du document, et le fait que le Web n'a pas pour autant été conçu uniquement pour cela – contrairement à ce qui peut être parfois dit. Les utilisations actuelles sont tournées vers l'applicatif, mais c'est possible grâce à la flexibilité d'Internet (et du Web)."
categories:
- flux
tags:
- internet
slug: l-origine-du-web
---
>I think it’s fairer to say that the first *use case* for the web was document retrieval. And yes, that initial use case certainly influenced the *first* iteration of HTML. But right from the start, the vision for the web wasn’t constrained by what it was being asked to do at the time.  
[Jeremy Keith, Origin story](https://adactio.com/journal/13187)

Jeremy Keith fait une distinction très intéressante entre d'un côté le constat que les premières utilisations du Web aient été autour du texte et du document, et le fait que le Web n'a pas pour autant été conçu uniquement pour cela – contrairement à ce qui peut être parfois dit. Les utilisations actuelles sont tournées vers l'applicatif, mais c'est possible grâce à la *flexibilité* d'Internet (et du Web).

>To say that the web was made for sharing documents is like saying that the internet was made for email. It’s true in the sense that it was the most popular use case, but that never defined the limits of the system.
