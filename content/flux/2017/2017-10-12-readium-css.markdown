---
layout: post
title: "Readium CSS"
date: "2017-10-12T17:00:00"
comments: true
published: true
description: "Jiminy Panoz prend le temps d'expliquer le projet de Readium CSS, la citation ci-dessus résume très bien cette entreprise aussi délicate que nécessaire."
categories:
- flux
tags:
- ebook
---
>Web Publications are coming, it’s time we start designing a more solid Reading Experience for the digital age.  
[Jiminy Panoz, On Readium CSS and digital reading](https://medium.com/@jiminypan/on-readium-css-5a7d87f7b671)

Jiminy Panoz prend le temps d'expliquer le projet [Readium CSS](https://github.com/readium/readium-css), la citation ci-dessus résume très bien cette entreprise aussi délicate que nécessaire.