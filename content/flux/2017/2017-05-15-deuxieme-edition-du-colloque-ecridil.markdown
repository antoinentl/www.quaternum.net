---
layout: post
title: "Deuxième édition du colloque ÉCRiDiL"
date: "2017-05-15T22:00:00"
comments: true
published: true
description: "L'appel de propositions d'interventions pour la deuxième édition du colloque ÉCRiDiL est ouvert jusqu'au 12 juin 2017 !"
categories: 
- flux
tags:
- publication
---
>Au cœur de ce colloque s’inscrit un questionnement fondamental et transversal : qu’est-ce qu’un livre ? Comment est-il appelé à se transformer en contexte numérique ?  
[CRILQ, Appel : Colloque ÉCRiDiL "Le livre, défi de design : l’intersection numérique de la création et de l’édition"](http://www.crilcq.org/actualites/item/appel-colloque-ecridil-ecrire-editer-lire-a-lere-numerique/)

L'appel de propositions d'interventions pour la deuxième édition du colloque ÉCRiDiL est ouvert jusqu'au 12 juin 2017 !