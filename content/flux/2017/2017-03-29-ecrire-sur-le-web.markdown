---
layout: post
title: "Écrire sur le web"
date: "2017-03-29T10:00:00"
comments: true
published: true
description: "Si vous avez quelque chose à dire, écrivez ! Publiez !"
categories: 
- flux
tags:
- publication
---
>It’s a world wide web out there. There’s plenty of room for everyone. And I, for one, love reading the words of others.  
[Jeremy Keith, Writing on the web](https://adactio.com/journal/12059)

Si vous avez quelque chose à dire, écrivez ! Publiez ! Le web est fait pour cela !