---
layout: post
title: "Design graphique et CSS"
date: "2017-11-16T22:00:00"
comments: true
published: true
description: "Jen Simmons est intervenue pendant le W3C Publishing Summit sur la question de la mise en page sur le web, son support de présentation est très parlant, mais l'idéal est d'aller voir son laboratoire en ligne."
categories:
- flux
tags:
- design
---
>This new CSS changes everything in web layout.  
[Jen Simmons, How New CSS Is Changing Everything About Graphic Design on the Web](https://speakerdeck.com/jensimmons/how-new-css-is-changing-everything-about-graphic-design-on-the-web)

Jen Simmons est intervenue pendant le [W3C Publishing Summit 2017](https://www.w3.org/publishing/events/summit2017) sur la question de la mise en page sur le web, son support de présentation est très parlant, mais l'idéal est d'aller voir son laboratoire en ligne ([exemple](http://labs.jensimmons.com/2017/03-004.html)).