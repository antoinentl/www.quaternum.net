---
layout: post
title: "Accessibility Checker for EPUB"
date: "2017-11-02T11:00:00"
comments: true
published: true
description: "Ace, pour Accessibility Checker for EPUB, est un outil qui permet d'automatiser certaines étapes de vérification de l'accessibilité de fichiers EPUB (le format du livre numérique). Le guide d'utilisation est disponible ici : https://daisy.github.io/ace/."
categories:
- flux
tags:
- accessibilité
---
>Ace is a tool to run automated accessibility checks for EPUB Publications, in order to assist in the evaluation of conformance to the EPUB Accessibility specification.  
[Ace, by DAISY](https://bertrandkeller.info/2017/11/06/netflix-react-generateur-site-statique/)

Ace, pour Accessibility Checker for EPUB, est un outil qui permet d'automatiser certaines étapes de vérification de l'accessibilité de fichiers EPUB (le format du livre numérique). Le guide d'utilisation est disponible ici : [https://daisy.github.io/ace/](https://daisy.github.io/ace/).