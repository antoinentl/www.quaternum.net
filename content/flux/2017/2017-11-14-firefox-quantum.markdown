---
layout: post
title: "Firefox Quantum"
date: "2017-11-14T22:00:00"
comments: true
published: true
description: "Une nouvelle version du navigateur web Firefox, majeure, sort ce mardi 14 novembre 2017. Cette cinquante-septième version, nommée Quantum, a demandé un an de travail et de nombreux bouleversements au sein de Mozilla. Deux choses importantes à noter dans ce projet (selon moi) : les méthodes initiées (notamment une équipe dédiée à la performance, Quantum Flow), et les choix stratégiques (abandon de projets comme Firefox OS pour se concentrer sur le navigateur)."
categories:
- flux
tags:
- web
---
>It’s time. The New Firefox is finally here. It’s been a year in the making and our biggest update of code in over 10 years. In fact, almost 75% of the Firefox code was impacted all to give you a browser that is fast, intuitive and modern on all of your devices.  
[Mozilla, The New Firefox Is Here!](https://blog.mozilla.org/firefox/the-new-firefox-is-here/)

Une nouvelle version du navigateur web Firefox, majeure, sort ce mardi 14 novembre 2017. Cette cinquante-septième version, nommée Quantum, a demandé un an de travail et de nombreux bouleversements au sein de Mozilla. Deux choses importantes à noter dans ce projet (selon moi) : les méthodes initiées (notamment une équipe dédiée à la performance, "Quantum Flow"), et les choix stratégiques (abandon de projets comme Firefox OS pour se concentrer sur le navigateur). Si la campagne marketing est criticable en tant que campagne marketing, cette nouvelle version de Firefox semble enfin en mesure de concurrencer Chrome sur de nombreux terrains, et cela est une nécessité (je ne sais pas pour vous mais le navigateur web est l'un de mes outils de travail principal).

À Grenoble Bastien a fait une présentation technique de Firefox 57, [le support de son intervention est disponible en ligne](https://nextcairn.com/quantum/).