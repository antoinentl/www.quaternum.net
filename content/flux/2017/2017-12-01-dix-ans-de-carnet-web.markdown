---
layout: post
title: "Dix ans de carnet web"
date: "2017-12-01T22:00:00"
comments: true
published: true
description: "Et si vous ouvriez aussi votre carnet web, dès aujourd'hui ?"
categories:
- flux
tags:
- publication
---
>Fast forward a decade, and that whimsical purchase has probably been the most influential decision I ever made. What started off as an overly-confident teenager’s foray into front-end development ultimately led to my first job, a speaking career, and a full-time consultancy firm. I struggle to believe it, sometimes.  
[Harry, Ten Years Old](https://csswizardry.com/2017/11/ten-years-old/)

Et si vous ouvriez aussi votre carnet web, dès aujourd'hui ?

Via Nicolas Hoizey.