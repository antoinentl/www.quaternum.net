---
layout: post
title: "Comprendre l'Agile"
date: "2017-04-26T22:00:00"
comments: true
published: true
description: "Frank Taillandier traduit un article de Steve Denning, qui permet de mieux comprendre l'Agile et ses multiples applications."
categories: 
- flux
tags:
- design
slug: comprendre-l-agile
---
>L’Agile est donc régi par trois lois - la loi des petites équipes, la loi du client et la loi du réseau. Ensemble elles génèrent les bases d’une organisation Agile. Ces trois lois nous permettent de déchiffrer la myriade de pratiques agiles qui s’appliquent ou pas dans un contexte particulier. Les pratiques peuvent changer mais l’esprit Agile qui applique les trois lois demeure. Elles offrent un cadre durable pour l’implication d’une organisation qui devient Agile.  
[Steve Denning, Expliquer l'Agile](https://frank.taillandier.me/2017/04/26/exliquer-l-agile/)

[Frank Taillandier](https://frank.taillandier.me/) traduit un article de Steve Denning, qui permet de mieux comprendre l'Agile et ses multiples applications.