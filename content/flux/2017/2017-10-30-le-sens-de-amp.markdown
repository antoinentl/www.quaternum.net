---
layout: post
title: "Le sens de AMP"
date: "2017-10-30T18:00:00"
comments: true
published: true
description: "Le débat autour de AMP (Accelerate Mobile Pages, un projet développé par Google) continue de susciter de vives réactions – totalement justifiée à mon sens."
categories:
- flux
tags:
- web
---
>So, to summarise, here are three statements that Google’s AMP team are currently peddling as being true:
>
>1. AMP is a community project, not a Google project.
>2. AMP pages don’t receive preferential treatment in search results.
>3. AMP pages are hosted on your own domain.
>  
[Jeremy Keith, The meaning of AMP](https://adactio.com/journal/13035)

Le débat autour de AMP (Accelerate Mobile Pages, un projet développé par Google) continue de susciter de vives réactions – totalement justifiée à mon sens.