---
layout: post
title: "À propos de publication numérique"
date: "2017-03-30T12:30:00"
comments: true
published: true
description: "Entretien passionnant entre Stacey Sundar et Nellie McKesson pour Type Thursday, où il est question de publication, et notamment de fabriquer des livres avec les technologies du web."
categories: 
- flux
tags:
- publication
---
>One thing that happened under my watch was the switch from focusing on XML to focusing on HTML. This might seem a little obvious or mundane, but it actually opened a lot of doors when it comes to automation. There’s this “white whale” in the publishing industry called “single source publishing”, which is a term that has come to mean maintaining the book content in a single file, and then “pushing a button” so to speak and magically transforming that file into the finished ebook file, the laid-out print file, and whatever else you need (for example, some publishers are publishing to the web as well now).  [...]
O’Reilly is very techy, so it made sense for them, but it was hard for a lot of the non-tech companies to adopt and implement that kind of tool in a way that would make sense for their staff.  
[Nellie McKesson, Everything About E-Publishing](https://medium.com/type-thursday/everything-about-e-publishing-dee31b059fb)

Entretien passionnant entre Stacey Sundar et Nellie McKesson pour Type Thursday, où il est question de publication, et notamment de fabriquer des livres avec les technologies du web.