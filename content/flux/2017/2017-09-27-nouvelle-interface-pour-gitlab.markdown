---
layout: post
title: "Nouvelle interface pour GitLab"
date: "2017-09-27T17:00:00"
comments: true
published: true
description: "L'évolution de la plateforme GitLab est impressionnante, tant sur le développement technique pur (si tant est qu'il existe), que sur les fonctionnalités et l'expérience utilisateur, et que sur la façon de développer tout cela. Un bel exemple de l'application du concept d'amélioration progressive."
categories:
- flux
tags:
- web
---
>After several rounds of research and testing, we released our redesigned navigation under a feature flag. We chose this method so that we could continue implementing improvements discovered in our original research while gathering real-world feedback from our users.  
[Sarrah Vesselov, Unveiling GitLab's new navigation](https://about.gitlab.com/2017/09/13/unveiling-gitlabs-new-navigation/)

L'évolution de la plateforme GitLab est impressionnante, tant sur le développement technique pur (si tant est qu'il existe), que sur les fonctionnalités et l'expérience utilisateur, et que sur la façon de développer tout cela. Un bel exemple de l'application du concept d'amélioration progressive.