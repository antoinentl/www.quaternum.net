---
layout: post
title: "Vous avez dit productivité ?"
date: "2017-06-12T10:00:00"
comments: true
published: true
description: "Privilégier le sens plutôt que tenter de rentabiliser le temps."
categories:
- flux
tags:
- métier
---
>Productivity won’t make you a better person, a better partner or a better friend. Instead it will drive you crazy by lowering your self worth in times of hardship. So be careful with your to-do list. Make sure each task on it isn’t just a distraction from dealing with your reality.  
[Abby Covert, Fuck Productivity](https://superyesmore.com/fuck-productivity-e87f76c75e7e43fac39bdb36978e219d)

Privilégier le sens plutôt que tenter de rentabiliser le temps.