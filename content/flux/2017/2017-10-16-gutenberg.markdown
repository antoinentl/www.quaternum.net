---
layout: post
title: "Gutenberg"
date: "2017-10-16T17:00:00"
comments: true
published: true
description: "Visible en ligne jusqu'au 13 novembre 2017, ce documentaire-fiction retrace le parcours de ce personnage si particulier, mélange de génie et d'entrepreneur, qui inventa notamment l'imprimerie à caractères mobiles."
categories:
- flux
tags:
- typographie
---
>L'invention de l'imprimerie a révolutionné la diffusion du savoir dans le monde. Mais quelle est sa véritable genèse ? Ce docu-fiction riche d'archives rares révèle la naissance tumultueuse de la presse de Gutenberg.  
[Arte, Gutenberg - L'aventure de l'imprimerie](https://www.arte.tv/fr/videos/064434-000-A/gutenberg-l-aventure-de-l-imprimerie/)

Visible en ligne jusqu'au 13 novembre 2017, ce documentaire-fiction retrace le parcours de ce personnage si particulier, mélange de génie et d'entrepreneur, qui inventa notamment l'imprimerie à caractères mobiles.