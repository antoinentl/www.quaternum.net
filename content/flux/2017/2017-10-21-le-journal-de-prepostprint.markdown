---
layout: post
title: "Le journal de PrePostPrint"
date: "2017-10-21T18:00:00"
comments: true
published: true
description: "Les Éditions HYX publient le journal de PrePostPrint, un recueil d'articles (dont deux de moi-même), des présentations de projet et des informations sur PrePostPrint ! Ce beau journal, produit en HTML2print, a été formidablement mis en page par Julie Blanc et Quentin Juhel !"
categories:
- flux
tags:
- publication
---
>Code X- 01-PrePostPrint offre l’opportunité de partager et de découvrir les questions portées par de multiples groupes de recherche, graphistes, éditeurs, théoriciens, qui interrogent aujourd’hui les nouveaux modes de publications : génératives, contributives, open source…  
[Éditions HYX, Code X](http://editions-hyx.com/fr/code-x)

Les Éditions HYX publient le journal de [PrePostPrint](https://prepostprint.org/), un recueil d'articles (dont deux de moi-même), des présentations de projet et des informations sur PrePostPrint ! Ce beau journal, produit en HTML2print, a été formidablement mis en page par [Julie Blanc](http://julie-blanc.fr/) et [Quentin Juhel](http://www.juhel-quentin.fr/) !