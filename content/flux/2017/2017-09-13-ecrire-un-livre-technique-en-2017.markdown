---
layout: post
title: "Écrire un livre technique en 2017"
date: "2017-09-13T09:00:00"
comments: true
published: true
description: "Voici le support de présentation de Thomas Parisot à l'occasion de Write the Docs, un événement autour de la documentation du code. C'est une version plus aboutie de la rencontre à la Coop que j'avais organisé en février 2017 et à laquelle Thomas avait accepté de participer."
categories:
- flux
tags:
- publication
---
>Publisher: "Where is my ODT file?"  
Me: "Wait, building it in HTML first!"  
[Thomas Parisot, Writing a book in 2017](https://oncletom.io/talks/2017/writethedocs/)

Voici le support de présentation de [Thomas Parisot](https://oncletom.io/) à l'occasion de [Write the Docs](http://www.writethedocs.org/), un événement autour de la documentation du code. C'est une version plus aboutie de [la rencontre à la Coop](https://www.quaternum.net/2017/03/07/ecrire-un-livre-en-2017/) que j'avais organisé en février 2017 et à laquelle Thomas avait accepté de participer.