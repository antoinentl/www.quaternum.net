---
layout: post
title: "Gilbert Simondon et le web"
date: "2017-08-03T17:00:00"
comments: true
published: true
description: "David Larlet lit Gilbert Simondon et fait quelques parallèles avec le web et plus globalement le logiciel libre. Il faut lire Gilbert Simondon !"
categories:
- flux
tags:
- web
---
>Est-ce que le logiciel libre est ce « mode d’existence naturel » ? En un sens oui, car il devient théoriquement entretenu par une communauté qui se renouvelle et le fait évoluer itérativement. En pratique malheureusement on est bien loin du compte [...].  
[David Larlet, Web et technique](https://larlet.fr/david/blog/2017/web-technique/)

David Larlet lit Gilbert Simondon et fait quelques parallèles avec le web et plus globalement le logiciel libre. Il faut lire Gilbert Simondon !