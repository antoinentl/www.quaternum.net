---
layout: post
title: "Nos infrastructures numériques"
date: "2017-09-22T17:00:00"
comments: true
published: true
description: "Open Edition Press vient de publier la traduction du livre de l'américaine Nadia Eghbal, qui interroge la façon dont les logiciels open source peuvent être maintenus."
categories:
- flux
tags:
- livre
---
>L’entretien de notre infrastructure numérique est une idée nouvelle pour beaucoup, et les problèmes que cela pose sont mal cernés. Dans cet ouvrage, Nadia Eghbal met au jour les défis uniques auxquels sont confrontées les infrastructures numériques et comment l’on peut œuvrer pour les relever.  
[Nadia Eghbal, Sur quoi reposent nos infrastructures numériques](https://books.openedition.org/oep/1797)

Open Edition Press vient de publier la traduction du livre de l'américaine [Nadia Eghbal](http://nadiaeghbal.com/), qui interroge la façon dont les logiciels *open source* peuvent être maintenus.