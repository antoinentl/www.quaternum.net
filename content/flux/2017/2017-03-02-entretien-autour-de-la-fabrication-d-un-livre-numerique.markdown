---
layout: post
title: "Entretien autour de la fabrication d'un livre numérique"
date: "2017-03-02T22:00:00"
comments: true
published: true
description: "Entretien avec Jiminy Panoz, qui aborde la conception des fichiers EPUBs, mais également quelques points intéressants comme les chaînes de publication des éditeurs."
categories: 
- flux
tags:
- ebook
slug: entretien-autour-de-la-fabrication-d-un-livre-numerique
---
>C’est surtout et avant tout une problématique culturelle : la conception d’un livre numérique, c’est beaucoup plus qu’une réplique au mieux du livre imprimé, avec, éventuellement, du multimédia et des interactions. Il faut y intégrer les problématiques d’utilisabilité, de performance, de compatibilité avec les réglages utilisateur, etc.  
[Nicolas Gary s'entretient avec Jiminy Panoz, "La conception d’un livre numérique, c’est bien plus qu’une réplique du livre imprimé"](https://www.actualitte.com/article/interviews/la-conception-d-un-livre-numerique-c-est-bien-plus-qu-une-replique-du-livre-imprime/69837)

Entretien avec Jiminy Panoz, qui aborde la conception des fichiers EPUBs, mais également quelques points intéressants comme les chaînes de publication des éditeurs.