---
layout: post
title: "Programming Design Systems"
date: "2017-05-29T22:00:00"
comments: true
published: true
description: "Rune Madsen commence l'écriture d'un livre -- un livre web -- sur une nouvelle approche du design graphique, orientée système plutôt qu'objet, à suivre de près."
categories:
- flux
tags:
- design
---
>No matter where you go, you are surrounded by systems. We use the word ‘system’ to describe a group of interacting parts as a common whole, and these can be simple, like a watch, or incredibly complex, like the web of computer networks we call the internet. In this book, the term ‘design system’ is used to describe a philosophy that encourages designers to define the rules of their designs as a system of instructions that can be used on more than a single product.  
[Rune Madsen, Programming Design Systems](https://programmingdesignsystems.com/what-is-a-design-system/)

Rune Madsen commence l'écriture d'un livre -- [un livre web](https://github.com/antoinentl/web-books-initiatives) -- sur une nouvelle approche du design graphique, orientée *système* plutôt qu'*objet*, à suivre de près.