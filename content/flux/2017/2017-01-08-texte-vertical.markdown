---
layout: post
title: "Texte vertical"
date: "2017-01-08T22:00:00"
comments: true
published: true
description: "Mettre en forme du texte, horizontalement et verticalement."
categories: 
- flux
tags:
- design
---
>Recently, more freedom in web design has meant that now every browser vendor and W3C have started to implement and recommend vertical design for text layouts and formatting for web design.  
As for typesetting, it has now become normal to utilize horizontal and/or vertical layouts for Japanese text, where previously restrictions meant only horizontal were allowed.  
[たてよこWebアワード](https://tategaki.github.io/awards/)

Mettre en forme du texte, horizontalement et verticalement. Une belle démonstration.