---
layout: post
title: "La question du processus"
date: "2017-03-28T10:00:00"
comments: true
published: true
description: "La comparaison entre l'industrie du livre et le web est intéressante, elle permet de révéler une absence de changement du côté du livre, milieu encore attaché ou contraint par des outils sans savoir comment ceux-ci fonctionnent."
categories: 
- flux
tags:
- publication
---
>As I’ve said several times before, the publishing industry as a whole has outsourced their entire workflow and production pipeline to a single vendor—Adobe. It’d be like if the web industry as a whole had decided that websites could only be made with Dreamweaver and only served from a Windows Server.  
[Baldur Bjarnason, The process is the thing](https://www.baldurbjarnason.com/notes/the-process-is-the-thing/)

La comparaison entre l'industrie du livre et le web est intéressante, elle permet de révéler une absence de changement du côté du livre, milieu encore attaché ou contraint par des outils sans savoir comment ceux-ci fonctionnent.

Cela rejoint ce que j'ai exposé dans [Une chaîne de publication inspirée du web](/2017/03/13/une-chaine-de-publication-inspiree-du-web/).