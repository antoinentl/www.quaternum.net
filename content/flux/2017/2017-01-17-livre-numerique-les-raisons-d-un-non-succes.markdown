---
layout: post
title: "Livre numérique, les raisons d'un non-succès"
date: "2017-01-17T22:00:00"
comments: true
published: true
description: "Daniel Glazman livre en 11 points pourquoi le livre numérique ne décolle pas en France. Je partage ses avis, hormis le point 8, je lis sur téléphone mobile et je pense que cela va se développer."
categories: 
- flux
tags:
- ebook
slug: livre-numerique-les-raisons-d-un-non-succes
---
>EPUB est trop compliqué et les chaînes logicielles éditoriales sont du coup trop compliquées et surtout trop rares.  
[Daniel Glazman, Livre électronique, qu'est-ce qu'on a raté ?](http://www.glazman.org/weblog/dotclear/index.php?post/2017/01/16/Livre-electronique-qu-est-ce-qu-on-a-rate)

Daniel Glazman livre en 11 points pourquoi le livre numérique ne décolle pas en France. Je partage ses avis, hormis le point 8, je lis sur téléphone mobile et je pense que cela va se développer.