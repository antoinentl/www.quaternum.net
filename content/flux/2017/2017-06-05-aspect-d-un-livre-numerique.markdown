---
layout: post
title: "Aspect d'un livre numérique"
date: "2017-06-05T22:00:00"
comments: true
published: true
description: "Oui c'est une blague qui concerne celles et ceux qui travaillent dans le livre numérique : un livre numérique, ou ebook, n'a pas besoin d'avoir exactement le même aspect sur tous les systèmes de lecture numérique."
categories:
- flux
tags:
- ebook
slug: aspect-d-un-livre-numerique
---
>Do ebooks need to look exactly the same in every Reading System?  
[Jiminy Panoz, Do ebooks need to look exactly the same in every Reading System?](https://jaypanoz.github.io/doebooksneedtolookexactlythesameineveryreadingsystem/)

Oui c'est une blague qui concerne celles et ceux qui travaillent dans le livre numérique : un livre numérique, ou ebook, n'a pas besoin d'avoir *exactement* le même aspect sur tous les systèmes de lecture numérique.