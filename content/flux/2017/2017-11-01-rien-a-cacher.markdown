---
layout: post
title: "Rien à cacher"
date: "2017-11-01T10:00:00"
comments: true
published: true
description: "Un documentaire disponible librement sur YouTube en version sous-titrée française."
categories:
- flux
tags:
- privacy
---
>C’est la réponse choc à l’argument “je n’ai rien à cacher” quand on parle de la surveillance de masse qui est organisée par les géants du Net et un nombre croissant d’états de par le monde.  
[Tristan Nitot, Pourquoi il faut voir le documentaire 'Nothing to hide'](https://blog.cozycloud.cc/post/2017/09/11/Documentaire-Nothing-to-hide)

Un documentaire disponible *librement* sur [YouTube en version sous-titrée française](https://www.youtube.com/watch?v=djbwzEIv7gE).