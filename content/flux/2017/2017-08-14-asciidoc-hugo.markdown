---
layout: post
title: "Asciidoc + Hugo"
date: "2017-08-14T09:00:00"
comments: true
published: true
description: "Intégrer Asciidoc -- un langage de balisage ou markup plus puissant que Markdown -- dans Hugo -- le générateur de site statique ultra performant -- pour gérer un blog ? Cela donne un workflow de publication intéressant !"
categories:
- flux
tags:
- publication
---
>I use a new branch in Git for each article, which keeps things nice and simple until I am ready to publish. Once I give my article a couple of edits to make sure everything flows well, I add AsciiDoc markup so Hugo can format the article as clean HTML. When the article is ready to publish, I merge it back into my master branch.  
[Andrew Thornton, How to create a blog with AsciiDoc](https://opensource.com/article/17/8/asciidoc-web-development)

Intégrer Asciidoc -- un langage de balisage ou *markup* plus puissant que Markdown -- dans Hugo -- le générateur de site statique ultra performant -- pour gérer un blog ? Cela donne un workflow de publication intéressant !