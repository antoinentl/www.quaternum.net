---
layout: post
title: "Questionner le travail"
date: "2017-03-22T12:00:00"
comments: true
published: true
description: "Dans le numéro 15 de la revue Faces B, Clémentine Hahn expose une vision intéressante du travail, du temps, de l'emploi, de l'activité salariée. Bref cela concerne une bonne partie de nos vies. Des réflexions qui soulèvent de nombreuses questions."
categories: 
- flux
tags:
- métier
---
>J'ai compris que beaucoup confondent emploi et travail. Parfois oui, ils se confondent. Mais parfois l'emploi empêche de travailler tout en te donnant les moyens de continuer. Absurde. Aujourd'hui je n'ai ni l'un ni l'autre mais j'ai une fenêtre : un peu de temps. Alors je le déplie dans tous les sens pour fabriquer mon travail.  
[Clémentine Hahn, Tu fais quoi dans la vie ?](http://www.facesb.fr/mag/facesb-15.pdf)

Dans le numéro 15 de la revue [Faces B](http://www.facesb.fr/), Clémentine Hahn expose une vision intéressante du travail, du temps, de l'emploi, de l'activité salariée. Bref cela concerne une bonne partie de nos vies. Des réflexions qui soulèvent de nombreuses questions.