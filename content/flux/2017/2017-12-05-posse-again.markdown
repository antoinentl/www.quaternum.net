---
layout: post
title: "POSSE (again)"
date: "2017-12-05T22:00:00"
comments: true
published: true
description: "Un article de plus en faveur du concept POSSE — Publish (on your) Own Site, Syndicate Elsewhere —, comme d'autres l'expliquent régulièrement (j'avais écrit Interroger nos pratiques de publication en ligne) il y a presque un an), mais Nicolas Hoizey utilise ici de très bons arguments et souligne les difficultés qui se présentent."
categories:
- flux
tags:
- publication
---
>Your platform is the origin.  
[Nicolas Hoizey, Medium is only an edge server of your POSSE CDN, your own blog is the origin](https://nicolas-hoizey.com/2017/11/medium-is-only-an-edge-server-of-your-posse-cdn-your-own-blog-is-the-origin.html)

Un article de plus en faveur du concept POSSE — Publish (on your) Own Site, Syndicate Elsewhere —, comme d'autres l'expliquent régulièrement (j'avais écrit [Interroger nos pratiques de publication en ligne](https://www.quaternum.net/2017/01/06/interroger-nos-pratiques-de-publication-en-ligne/) il y a presque un an), mais Nicolas Hoizey utilise ici de très bons arguments et souligne les difficultés qui se présentent.
