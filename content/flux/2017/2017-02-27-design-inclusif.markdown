---
layout: post
title: "Design inclusif"
date: "2017-02-27T22:00:00"
comments: true
published: true
description: "Une vision et une approche intéressantes sur l'accessibilité, qui rejoignent le point de vue de Heydon Pickering : concevoir des produits inclusifs plutôt que les rendre accessibles, et considérer que nous nous sommes tous potentiellement des personnes en situation de handicap. Je suis de cet avis, même si cela crée le débat actuellement."
categories: 
- flux
tags:
- accessibilité
---
>Everyone has abilities, and limits to those abilities. Designing for people with permanent disabilities actually results in designs that benefit people universally. Constraints are a beautiful thing.  
[Inclusive Design at Microsoft](https://www.microsoft.com/en-us/design/inclusive)

Une vision et une approche intéressantes sur l'accessibilité, qui rejoignent [le point de vue de Heydon Pickering](/2017/01/02/une-definition-du-design-inclusif/) : concevoir des produits inclusifs plutôt que les *rendre* accessibles, et considérer que nous nous sommes tous potentiellement des personnes en situation de handicap. Je suis de cet avis, même si cela crée le débat actuellement.

Via [Nicolas](https://twitter.com/NT_polylogue/status/832535476376629249).