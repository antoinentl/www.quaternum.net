---
layout: post
title: "Des commentaires avec Webmention"
date: "2017-07-31T17:00:00"
comments: true
published: true
description: "Nicolas Hoizey expérimente Webmention, et supprime complètement les commentaires de son carnet web. Pourquoi ? Pour ne pas dépendre d'un service tiers comme Disqus, mais aussi et surtout pour favoriser la publication des commentateurs : plutôt que de laisser un commentaire à la fin d'un article du carnet de Nicolas, le commentaire sera un billet de blog qui sera ensuite signalé sur l'article de Nicolas."
categories:
- flux
tags:
- web
---
>Reminds you of manual trackbacks and Wordpress’ automated pingbacks? Indeed. Let say it’s a standard way to do almost the same.  
[Nicolas Hoizey, So long Disqus, hello Webmention](https://nicolas-hoizey.com/2017/07/so-long-disqus-hello-webmentions.html)

Nicolas Hoizey expérimente Webmention, et supprime complètement les commentaires de son carnet web. Pourquoi ? Pour ne pas dépendre d'un service tiers comme Disqus, mais aussi et surtout pour favoriser la publication des *commentateurs* : plutôt que de laisser un commentaire à la fin d'un article du carnet de Nicolas, le commentaire sera un billet de blog qui sera ensuite signalé sur l'article de Nicolas.