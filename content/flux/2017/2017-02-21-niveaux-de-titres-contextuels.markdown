---
layout: post
title: "Niveaux de titres contextuels"
date: "2017-02-21T08:00:00"
comments: true
published: true
description: "Ajouter un élément <h> pourrait-il résoudre le problème de hiérarchie des niveaux de titres dans un document HTML, en laissant le navigateur analyser et construire l'arborescence ? Question qui n'est pas si simple, notamment pour prendre en compte l'existant..."
categories: 
- flux
tags:
- accessibilité
---
>There's a proposal to add a new `<h>` element to the HTML spec. It solves a fairly common use-case.  
[Jake Archibald, Do we need a new heading element? We don't know](https://jakearchibald.com/2017/do-we-need-a-new-heading-element/)

Ajouter un élément `<h>` pourrait-il résoudre le problème de hiérarchie des niveaux de titres dans un document HTML, en laissant le navigateur analyser et construire l'arborescence ? Question qui n'est pas si simple, notamment pour prendre en compte l'existant...