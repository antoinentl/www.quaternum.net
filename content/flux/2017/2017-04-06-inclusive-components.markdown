---
layout: post
title: "Inclusive Components"
date: "2017-04-06T10:00:00"
comments: true
published: true
description: "Heydon Pickering débute une initiative autour du design inclusif, et donc de l'accessibilité : un blog rassemblant des bonnes pratiques autour du desgin et de l'accessibilité du web, avec des bouts de code dedans."
categories: 
- flux
tags:
- accessibilité
---
>A blog trying to be a pattern library. All about designing inclusive web interfaces, piece by piece.  
[Heydon Pickering, Inclusive Components](https://inclusive-components.design)

Heydon Pickering débute une initiative autour du design inclusif, et donc de l'accessibilité : un blog rassemblant des bonnes pratiques autour du desgin *et* de l'accessibilité du web, avec des bouts de code dedans.