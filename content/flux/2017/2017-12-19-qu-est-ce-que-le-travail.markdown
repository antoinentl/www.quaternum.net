---
layout: post
title: "Qu'est-ce que le travail ?"
date: "2017-12-19T22:00:00"
comments: true
published: true
description: "Publié originellement dans le quarante-septième numéro de la revue Azimuts, ce texte d'Anthony Masure interroge la notion de travail dans le monde contemporain, et donc en lien avec le numérique."
categories:
- flux
tags:
- métier
slug: qu-est-ce-que-le-travail
---
>[...] nous verrons que les limites du travail n’ont jamais été aussi floues, tant du point des vues d’activités en ligne non rémunérées («&nbsp;labeur numérique&nbsp;») que de la prolifération d’objets supposément «&nbsp;intelligents&nbsp;» (smart) qui brouillent encore plus la distinction entre le temps libre et le temps travaillé.  
[Anthony Masure, Peut-on encore ne pas travailler ?](http://www.anthonymasure.com/articles/2017-06-peut-on-encore-ne-pas-travailler)

Publié originellement dans le quarante-septième numéro de la revue Azimuts, ce texte d'Anthony Masure interroge la notion de travail dans le monde contemporain, et donc en lien avec le *numérique*. (Note : article en cours de lecture.)
