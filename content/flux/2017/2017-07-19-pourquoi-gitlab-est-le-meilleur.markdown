---
layout: post
title: "Pourquoi GitLab est le meilleur"
date: "2017-07-19T12:00:00"
comments: true
published: true
description: "Une courte présentation qui vise à convaincre que GitLab est une option très très intéressante pour faire de la recherche -- notamment avec les fonctions de CI (ou Continuous Integration)."
categories:
- flux
tags:
- outils
---
>GitLab has so much potential but isn't used as heavily as other Git repo hosting sites. Here, I'll showcase why I love it in 5 minutes and why I think more people (particularly researchers, and more particularly, those interested in reproducibility) should use it more.  
[Vicky Steeves, Why GitLab is the best (especially for researchers + reproducibility)](https://vickysteeves.gitlab.io/2017-SciPy-GitLab-Lightning/)

Une courte présentation qui vise à convaincre que GitLab est une option très très intéressante pour faire de la recherche -- notamment avec les fonctions de CI (ou Continuous Integration).