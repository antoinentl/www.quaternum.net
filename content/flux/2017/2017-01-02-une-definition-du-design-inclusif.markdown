---
layout: post
title: "Une définition du design inclusif"
date: "2017-01-02T22:00:00"
comments: true
published: true
description: "Une définition du design inclusif, qui intègre la notion d'accessibilité tout en ne s'y limitant pas."
categories: 
- flux
tags:
- accessibilité
---
>So where have we got to? Access is important, but inclusion is bigger than access. Inclusive design means making something valuable, not just accessible, to as many people as we can.  
[Heydo Pickering, What the Heck Is Inclusive Design?](https://24ways.org/2016/what-the-heck-is-inclusive-design/)

Une définition du *design inclusif*, qui intègre la notion d'accessibilité tout en ne s'y limitant pas.

Via [Marie](http://marieguillaumet.com/).