---
layout: post
title: "Consommation de nos messageries"
date: "2017-05-04T22:00:00"
comments: true
published: true
description: "Prise de conscience de ce que représente nos usages de la messagerie électronique, du mail. Enguerran fait des choix pour réduire sa consommation d'hébergement de données, ici dans le cas du mail."
categories: 
- flux
tags:
- web
---
>Mais si le cloud est l'ordinateur de quelqu'un d'autre, ce dernier sait exactement comment ne pas perdre les données de ses utilisateurs : backup, duplication, réplication, …  
L'équation est simple : **si** on ne supprime plus ses emails, **si** on a un espace de stockage géant, **si** on active par dessus l'IMAP2. **Alors** on utilise vraiment beaucoup d'espace disque et de serveurs.  
[Enguerran, J'ai dépollué internet](https://blog.ticabri.com/2017/05/03/pollution-internet/)

Prise de conscience de ce que représente nos usages de la messagerie électronique, du mail. Enguerran fait des choix pour réduire sa consommation d'hébergement de données, ici dans le cas du mail.