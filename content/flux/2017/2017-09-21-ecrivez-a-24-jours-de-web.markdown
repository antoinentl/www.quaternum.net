---
layout: post
title: "Écrivez à 24 jours de web"
date: "2017-09-21T17:00:00"
comments: true
published: true
description: "C'est reparti pour 24 jours de web (le calendrier de l'avent des gens qui font le web, en gros), il est possible de proposer des articles, d'être relecteur et de soutenir une association."
categories:
- flux
tags:
- web
---
>Alors cette année plus que jamais, on a besoin de vous pour qu'une cinquième édition de 24 jours de web prenne vie. On recherche 24 auteur(e)s, des relecteur(trice)s et une association à soutenir.  
[Rémi, 24 jours de web 2017](http://www.24joursdeweb.fr/2017/lancement/)

C'est reparti pour 24 jours de web (le calendrier de l'avent des gens qui font le web, en gros), il est possible de proposer des articles, d'être relecteur et de soutenir une association.