---
layout: post
title: "Numéro Onze"
date: "2017-01-16T22:00:00"
comments: true
published: true
description: "Un magazine numérique pour une fois original. Chaque numéro prend la forme d'une page web, une couverture façon magazine papier avec les titres qui amènent vers quelques contenus : un entretien sous forme d'échanges qui viennent se superposer à la vouverture, et des liens rubriqués (application, développement, typo, GIF, etc.). Un système simple, efficace. Dommage que l'adaptation aux écrans ne soit pas mieux réussie."
categories: 
- flux
tags:
- média
---
>NUMERO ONZE, magazine numérique de l'Internet  
[NUMERO ONZE](http://numero-onze.com/)

Un magazine *numérique* pour une fois original. Chaque numéro prend la forme d'une page web, une couverture façon magazine papier avec les titres qui amènent vers quelques contenus : un entretien sous forme d'échanges qui viennent se superposer à la vouverture, et des liens rubriqués (application, développement, typo, GIF, etc.). Un système simple, efficace. Dommage que l'adaptation aux écrans ne soit pas mieux réussie.