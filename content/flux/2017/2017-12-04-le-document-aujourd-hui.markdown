---
layout: post
title: "Le document aujourd'hui"
date: "2017-12-04T22:00:00"
comments: true
published: true
description: "Un article sur le document tel qu'il existe aujourd'hui. Un point de départ pour des réflexions (personnelles) en cours, notamment sur les propriétés attendues d'un document au vingt-et-unième siècle."
categories:
- flux
tags:
- publication
slug: le-document-aujourd-hui
---
>En entrant dans l’ère du numérique, le document a connu un grand nombre de mutations qui se sont traduites au cours de ces trente dernières années par une transformation de son rapport à la technique, l’apparition d’enjeux de société inédits et l’explosion de nouveaux métiers sur le marché de l’emploi.  
[Catherine Muller, Qu’est-ce qu’un document numérique au 21è siècle&nbsp;? Exercice de repérages](https://dlis.hypotheses.org/1429)

Un article sur le document tel qu'il *existe* aujourd'hui. Un point de départ pour des réflexions (personnelles) en cours, notamment sur les propriétés attendues d'un document au vingt-et-unième siècle.
