---
layout: post
title: "Le contenu est souverain"
date: "2017-12-11T22:00:00"
comments: true
published: true
description: "Après l'annonce du lancement d'Antora, le générateur de site statique modulaire basé sur AsciiDoc, voici une série d'articles qui explique la raison de sa création et les détails de son fonctionnement. Première partie sur les contenus, leur origine et la façon de les rassembler/gérer."
categories:
- flux
tags:
- publication
---
>One branch, one repository. Sometimes even a single folder. That’s the first major obstacle we encounter when using a typical static site generator.  
[Dan Allen, Content is Sovereign](https://opendevise.com/blog/content-is-sovereign/)

Après l'annonce du lancement d'Antora – le générateur de site statique modulaire basé sur AsciiDoc et porté par OpenDevise –, voici une série d'articles qui explique la raison de sa création et les détails de son fonctionnement. Première partie sur les contenus, leur origine et la façon de les rassembler/gérer.
