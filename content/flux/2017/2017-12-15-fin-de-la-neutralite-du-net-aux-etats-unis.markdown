---
layout: post
title: "Fin de la neutralité du Net aux États-Unis"
date: "2017-12-15T10:00:00"
comments: true
published: true
description: "Aux États-Unis la neutralité du Net c'est fini, pour le moment..."
categories:
- flux
tags:
- internet
---
>The Federal Communications Commission voted on Thursday to dismantle rules regulating the businesses that connect consumers to the internet, granting broadband companies the power to potentially reshape Americans’ online experiences.  
The agency scrapped the so-called net neutrality regulations that prohibited broadband providers from blocking websites or charging for higher-quality service or certain content. The federal government will also no longer regulate high-speed internet delivery as if it were a utility, like phone service.  
[Cecilia Kang, F.C.C. Repeals Net Neutrality Rules](https://www.nytimes.com/2017/12/14/technology/net-neutrality-repeal-vote.html)

Aux États-Unis la neutralité du Net c'est fini, [pour le moment](https://advocacy.mozilla.org/en-us/net-neutrality/petition)...
