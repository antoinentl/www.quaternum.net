---
layout: post
title: "Quelques trucs et astuces pour git"
date: "2017-05-25T01:00:00"
comments: true
published: true
description: "Une série de trucs et astuces pour mieux maîtriser git, bien utiles."
categories:
- flux
tags:
- outils
---
>In this post, we gathered some Git tips and tricks we use at GitLab everyday. Hopefully they will add up to your aha! moment.  
[Achilleas Pipinellis, Git Tips & Tricks](https://about.gitlab.com/2016/12/08/git-tips-and-tricks/)

Une série de trucs et astuces pour mieux maîtriser git, bien utiles.
