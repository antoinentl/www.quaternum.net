---
layout: post
title: "Design graphique et numérique en revue"
date: "2017-05-30T22:00:00"
comments: true
published: true
description: "Loup Cellard présente cinq initiatives, revue ou numéro de revue, qui abordent les questions de design graphique mêlées aux enjeux numériques. Forcément Back Office en fait partie, mais il y a également d'autres belles découvertes."
categories:
- flux
tags:
- design
---
>Passage en revue  
[Loup Cellard, Design graphique & pratiques numériques](http://strabic.fr/Design-graphique-pratiques-numeriques)

Loup Cellard présente cinq initiatives, revue ou numéro de revue, qui abordent les questions de design graphique mêlées aux enjeux numériques. Forcément [Back Office](http://www.revue-backoffice.com/) en fait partie, mais il y a également d'autres belles découvertes.