---
layout: post
title: "Ce que le terme plateforme recouvre"
date: "2017-10-04T17:00:00"
comments: true
published: true
description: "Définir la notion de plateforme est plus que nécessaire aujourd'hui, et cette remise en perspective historique d'Antonio Casilli est plus que bienvenue !"
categories:
- flux
tags:
- web
---
>Bref, l’expression plateforme n’est pas une simple métaphore, mais une dégradation/évolution d’un concept du XVIIe siècle. En tant que telle, elle reste porteuse d’implications et prescriptions politiques implicites qu’il serait nuisible d’égarer—si on abandonnait la notion.  
[Antonio A. Casilli, De quoi une plateforme (numérique) est-elle le nom ?](http://www.casilli.fr/2017/10/01/de-quoi-une-plateforme-est-elle-le-nom/)

Définir la notion de plateforme est plus que nécessaire aujourd'hui, et cette remise en perspective historique d'Antonio Casilli est plus que bienvenue !