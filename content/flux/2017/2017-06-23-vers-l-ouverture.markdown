---
layout: post
title: "Vers l'ouverture"
date: "2017-06-23T10:00:00"
comments: true
published: true
description: "David Dufresne conclue son passage d'un système d'exploitation verrouillé à un OS ouvert, les solutions sont à trouver seul ou avec l'aide des communautés, l'amélioration est continue, l'environnement aussi personnalisé que possible, les évolutions dépendent autant des développeurs que de l'utilisateur."
categories:
- flux
tags:
- outils
slug: vers-l-ouverture
---
>Et, soudain, je me mis à sourire devant l’anarchisme de Linux : options, préférences, configurations, on joue à cache cache avec les possibilités, on tente de se mettre dans l’esprit des développeurs et puis, si on est pas content, on change tout car on peut tout changer sur Linux. Et adapter sa machine à son bon vouloir et à son bon plaisir.  
[David Dufresne, Du Mac à Linux, épisode 4 : démolition totale (et bilan final)](http://www.davduf.net/du-mac-a-linux-episode-4-demolition-totale-et)

David Dufresne conclue son passage d'un système d'exploitation verrouillé à un OS ouvert : les solutions sont à trouver seul ou avec l'aide des communautés, l'amélioration est continue, l'environnement aussi personnalisé que possible, les évolutions dépendent autant des développeurs que de l'utilisateur.