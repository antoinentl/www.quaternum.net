---
layout: post
title: "Couleurs, contraste et accessibilité"
date: "2017-03-10T08:00:00"
comments: true
published: true
description: "Un outil pour déterminer la qualité du contraste entre deux couleurs -- par exemple celle d'un texte et celle du fond --, très pratique et complémentaire de Accessible color palette builder."
categories: 
- flux
tags:
- accessibilité
---
>Contrast is the difference in luminance or color that makes an object (or its representation in an image or display) distinguishable. In visual perception of the real world, contrast is determined by the difference in the color and brightness of the object and other objects within the same field of view.  
[Brent Jackson, Colorable](https://colorable.jxnblk.com/)

Un outil pour déterminer la qualité du contraste entre deux couleurs -- par exemple celle d'un texte et celle du fond --, très pratique et complémentaire de [Accessible color palette builder](https://toolness.github.io/accessible-color-matrix/).