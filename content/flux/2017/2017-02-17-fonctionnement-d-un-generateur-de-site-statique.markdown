---
layout: post
title: "Fonctionnement d'un générateur de site statique"
date: "2017-02-17T10:00:00"
comments: true
published: true
description: "Frank Taillandier traduit un article de Brian Rinaldi, qui décrit très clairement et très précisèment comment fonctionne un générateur de site statique, à lire si le sujet des générateurs de site statique vous intéresse -- et il devrait vous intéresser !"
categories: 
- flux
tags:
- web
slug: fonctionnement-d-un-generateur-de-site-statique
---
>Je parle beaucoup des générateurs de site statique mais je parle toujours de comment utiliser des générateurs de site statique. Ils sont souvent perçus comme une boîte noire. Je crée un modèle, j’écris un peu de Markdown et hop j’obtiens une page entièrement formatée en HTML. C’est magique !  
Mais qu’est-ce vraiment un générateur de site statique ? Qu’y-a-t-il dans cette boîte noire ? De quelle magie Vaudou s’agit-il ?  
[Brian Rinaldi, Qui y'a t-il dans un générateur de site statique ?](https://jamstatic.fr/2017/02/09/y-a-quoi-dans-un-generateur-de-site-statique/)

[Frank Taillandier](https://frank.taillandier.me/) traduit un article de Brian Rinaldi, qui décrit très clairement et très précisément comment fonctionne un générateur de site statique, à lire si le sujet des générateurs de site statique vous intéresse -- et il devrait vous intéresser !