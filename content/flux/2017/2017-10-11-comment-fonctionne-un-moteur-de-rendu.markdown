---
layout: post
title: "Comment fonctionne un moteur de rendu ?"
date: "2017-10-11T17:00:00"
comments: true
published: true
description: "Un article complet et illustré qui explique le fonctionnement d'un moteur de rendu d'un navigateur web, par Lin Clark."
categories:
- flux
tags:
- web
---
>Bref, l’expression plateforme n’est pas une simple métaphore, mais une dégradation/évolution d’un concept du XVIIe siècle. En tant que telle, elle reste porteuse d’implications et prescriptions politiques implicites qu’il serait nuisible d’égarer—si on abandonnait la notion.  
[Lin Clark, The whole web at maximum FPS: How WebRender gets rid of jank](https://hacks.mozilla.org/2017/10/the-whole-web-at-maximum-fps-how-webrender-gets-rid-of-jank/)

Un article complet et illustré qui explique le fonctionnement d'un moteur de rendu d'un navigateur web, par Lin Clark.

Via [HTeuMeuLeu](https://twitter.com/HTeuMeuLeu/status/918006243813396480).