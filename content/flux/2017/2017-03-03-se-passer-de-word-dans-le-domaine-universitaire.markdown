---
layout: post
title: "Se passer de Word dans le domaine universitaire"
date: "2017-03-03T22:00:00"
comments: true
published: true
description: "Comment utiliser Markdown en situation académique ? Se passer de Word dans le domaine de l'édition universitaire est-il possible ? Des réponses à ces questions avec RMarkdown -- mais il existe d'autres solutions basées sur Markdown ou AsciiDoc."
categories: 
- flux
tags:
- publication
---
>L’édition scientifique fait aujourd’hui face à plusieurs transformations : réduction des budgets et des «personnels de renfort» éditoriaux, transition des supports vers le numérique, et crise de la reproductibilité des résultats. Chacune conduit à des évolutions des besoins des éditeurs, donc aussi par contrecoup des auteurs. Alors que Word est l’outil et le format standard pour les textes de sciences sociales depuis les années 1990, RMarkdown apparait mieux adapté à la situation contemporaine.  
[Alexandre Hobeika et Florent Bédécarrats, Une alternative à Word : écrire en RMarkdown](http://data.hypotheses.org/1144)

Comment utiliser Markdown en situation académique ? Se passer de Word dans le domaine de l'édition universitaire est-il possible ? Des réponses à ces questions avec RMarkdown -- mais il existe d'autres solutions basées sur Markdown ou AsciiDoc.