---
layout: post
title: "Redimensionner"
date: "2017-10-03T17:00:00"
comments: true
published: true
description: "C'est un débat récurrent, en plus de créer une interface responsive, qui s'adapte donc aux dimensions de l'écran de l'utilisateur, il faut également prévoir le cas où le redimensionnement est effectué pendant la consultation d'un site."
categories:
- flux
tags:
- accessibilité
---
>[...] how does our code work when a user resizes the browser instead of just on load?  
[Russell Goldenberg, How Many Users Resize Their Browser?](https://pudding.cool/process/resize/)

C'est un débat récurrent, en plus de créer une interface *responsive*, qui s'adapte donc aux dimensions de l'écran de l'utilisateur, il faut également prévoir le cas où le redimensionnement est effectué *pendant* la consultation d'un site.