---
layout: post
title: "Robert Darnton sur France Culture"
date: "2017-09-29T17:00:00"
comments: true
published: true
description: "Un entretien avec Robert Darnton en cinq épisodes, tous passionnants (notamment sur le livre, la connaissance, le numérique)."
categories:
- flux
tags:
- publication
---
>*Élise Gruau : Quelles sont les traces que nous laissons, nous qui écrivons sur des ordinateurs, sur des supports numériques, quelles seront les archives pour les historiens de demain ?*  
Robert Darnton : Ça m'inquiète beaucoup, je me réveille la nuit en me demandant si tel site web va disparaître demain. [...] Il y a un homme tout à fait remarquable qui s'appelle Brewster Kahle qui a créé ce qu'il appelle Internet Archive.  
[À voix nue, Robert Darnton : un historien américain des Lumières](https://www.franceculture.fr/emissions/voix-nue/robert-darnton-un-historien-americain-des-lumieres)

Un entretien avec Robert Darnton en cinq épisodes, tous passionnants (notamment sur le livre, la connaissance, le numérique).