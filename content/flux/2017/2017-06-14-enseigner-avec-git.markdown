---
layout: post
title: "Enseigner avec Git"
date: "2017-06-14T10:00:00"
comments: true
published: true
description: "Utiliser Git et une plateforme comme GitHub pour échanger entre étudiants et enseignants ? L'idée est séduisante, et pas uniquement pour du code. Par contre je comprends mal l'intérêt de GitHub Classroom..."
categories:
- flux
tags:
- outils
---
>Students said it was easier to understand feedback when it was in context.  
[mozzadrella, How to grade programming assignments on GitHub](https://github.com/blog/2376-how-to-grade-programming-assignments-on-github)

Utiliser Git et une plateforme comme GitHub pour échanger entre étudiants et enseignants ? L'idée est séduisante, et pas uniquement pour du code. Par contre je comprends mal l'intérêt de GitHub Classroom...

Via [Boris](https://twitter.com/borisschapira/status/874735560178446336).