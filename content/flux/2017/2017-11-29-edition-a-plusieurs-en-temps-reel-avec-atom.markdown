---
layout: post
title: "Édition à plusieurs en temps réel avec Atom"
date: "2017-11-29T22:00:00"
comments: true
published: true
description: "Atom, l'éditeur de code initié et développé par GitHub, propose désormais l'édition à plusieurs en temps réel. Après avoir intégré certaines fonctionnalités de Git et surtout de GitHub dans ce logiciel, Atom va donc encore plus loin. Il s'agit d'une belle fonctionnalité, mais je ne sais pas si c'est une bonne nouvelle, notamment parce qu'Atom prend une place assez hégémonique."
categories:
- flux
tags:
- outils
---
>Social coding shouldn’t have to be this hard! Today, we’re taking a first step toward making it just as easy to code together as it is to code alone with Teletype for Atom. At the dawn of computing, teletypes were used to create a real-time circuit between two machines so that anything typed on one machine appeared at the other end immediately. Following in these electro-mechanical footsteps, Teletype for Atom wires the keystrokes of remote collaborators directly into your programming environment, enabling conflict-free, low-latency collaborative editing for any file you can open in Atom.  
[Nathan Sobo, Code together in real time with Teletype for Atom](https://blog.atom.io/2017/11/15/code-together-in-real-time-with-teletype-for-atom.html)

Atom, l'éditeur de code initié et développé par GitHub, propose désormais l'édition à plusieurs en temps réel. Après avoir intégré certaines fonctionnalités de Git et surtout de GitHub dans ce logiciel, Atom va donc encore plus loin. Il s'agit d'une belle fonctionnalité, mais je ne sais pas si c'est une bonne nouvelle, notamment parce qu'Atom a désormais une place hégémonique.
