---
layout: post
title: "HTTP1 et HTTP2 illustrés"
date: "2017-05-19T22:00:00"
comments: true
published: true
description: "Comprendre la différence entre HTTP1.x et HTTP2 rapidement et facilement ? Cette série de trois illustrations de Mariko Kosaka sont très réussies !"
categories:
- flux
tags:
- internet
---
>I drew what HTTP is & how HTTP1.x and HTTP2 are different.  
[Mariko Kosaka, HTTP what?](https://twitter.com/kosamari/status/859958929484337152)

Comprendre la différence entre HTTP1.x et HTTP2 rapidement et facilement ? Cette série de trois illustrations de Mariko Kosaka sont très réussies !
