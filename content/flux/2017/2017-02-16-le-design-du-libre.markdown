---
layout: post
title: "Le design du libre"
date: "2017-02-16T10:00:00"
comments: true
published: true
description: "Julien aborde une question délicate, celle du design -- pas uniquement le graphisme, il s'agit bien du design dans sa globalité -- dans le libre. Les raison de ces échecs sont probablement celles de l'absence de place au design dès le début d'un projet, et le manque de confiance accordée au designer lorsqu'il est impliqué dans un projet."
categories: 
- flux
tags:
- design
---
>[...] je vais essayer d’exposer les obstacles qui font que design et open source font rarement bon ménage, essayer de comprendre pourquoi le grand public préfère utiliser des logiciels espions de sales capitalistes plutôt que leurs alternatives libres, et envisager des pistes de réflexion.  
[Julien, Le design dans le libre : pistes de réflexion](http://mariejulien.com/post/2017/02/08/Le-design-dans-le-libre-%3A-pistes-de-r%C3%A9flexion)

[Julien](http://judbd.com/) aborde une question délicate, celle du design -- pas uniquement le graphisme, il s'agit bien du design dans sa globalité -- dans le libre. Les raison de ces échecs sont probablement celles de l'absence de place au design dès le début d'un projet, et le manque de confiance accordée au designer lorsqu'il est impliqué dans un projet. En lisant [ce compte-rendu qui suit l'article](http://mariejulien.com/post/2017/02/13/Table-ronde-design-et-open-source-au-Reset), on comprend qu'il y a aussi souvent un problème de gouvernance.