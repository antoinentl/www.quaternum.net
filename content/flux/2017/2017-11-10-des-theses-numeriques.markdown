---
layout: post
title: "Des thèses numériques"
date: "2017-11-10T22:00:00"
comments: true
published: true
description: "Anthony Masure est intervenu, avec Pierre-Damien Huyghe, lors de la journée Publier la recherche organisée par le Centre national des arts plastiques (Cnap), son support de communication et un texte de présentation sont disponibles sur son site web."
categories:
- flux
tags:
- publication
---
>En quoi ces pratiques de publication, encore peu répandues dans la conduite d’un doctorat, modifient-elles les relations entre la direction et la rédaction de la thèse ? Comment peuvent-elles ouvrir de nouvelles directions de recherche tout en prenant en compte les normes en vigueur ? Quelles perspectives de diffusion et de communication permettent-elles ?  
[Anthony Masure, Les versions numériques des thèses de doctorat](http://www.anthonymasure.com/conferences/2017-11-versions-numeriques-theses-doctorat)

Anthony Masure est intervenu, avec Pierre-Damien Huyghe, lors de la journée "Publier la recherche" organisée par le Centre national des arts plastiques (Cnap), son support de communication et un texte de présentation sont disponibles sur son site web.