---
layout: post
title: "PrePostPrint à la Gaîté Lyrique"
date: "2017-09-23T17:00:00"
comments: true
published: true
description: "Samedi 21 octobre 2017 de 14h à 19h à la Gaîté Lyrique à Paris, retrouvez les expérimentations les plus pointues autour des techniques d'édition et de publication alternatives ! Conférences, rencontres, affiches, tee-shirts et bonne ambiance au menu de ce deuxième événement PrePostPrint organisé par Sarah Garcin, Raphaël Bastide, Julie Blanc, Alexia Foubert, et moi-même ! Journée garantie sans traîtement de texte ni logiciel de publication assistée par ordinateur !"
categories:
- flux
tags:
- publication
---
>Durant un salon de l’édition alternative et libre, nos invités présenteront des productions, ouvrages et outils, réalisés avec des techniques expérimentales et libre (génératives, collaboratives, libre / open source…).  
[PrePostPrint, Un événement à la Gaîté Lyrique](https://prepostprint.org/)

Samedi 21 octobre 2017 de 14h à 19h à la Gaîté Lyrique à Paris, retrouvez les expérimentations les plus pointues autour des techniques d'édition et de publication alternatives ! Conférences, rencontres, affiches, tee-shirts et bonne ambiance au menu de ce deuxième événement PrePostPrint organisé par Sarah Garcin, Raphaël Bastide, Julie Blanc, Alexia Foubert, et moi-même ! Journée garantie sans traîtement de texte ni logiciel de publication assistée par ordinateur !