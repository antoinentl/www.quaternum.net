---
layout: post
title: "Livre et web"
date: "2017-02-14T22:00:00"
comments: true
published: true
description: "Il s'agit de la transcription d'une intervention de Dave Cramer à une conférence de Books in Browsers, qui résume très bien les questions suscitées par l'EPUB -- le format actuel du livre numérique -- et le web : comment réussir à utiliser le web pour raconter une, des histoires ? Comment rapprocher le web et le livre ?"
categories: 
- flux
tags:
- publication
---
>I want to collect, connect, digest, and perhaps then I can create something from what I’ve found. What kind of bag can hold the things I find and make on the web?  
[Dave Cramer, A Bag Full of Stories](https://www.pagedmedia.org/a-bag-full-of-stories/)

Il s'agit de la transcription d'une intervention de [Dave Cramer](https://twitter.com/dauwhe) à une conférence de [Books in Browsers](http://booksinbrowsers.org/), qui résume très bien les questions suscitées par l'EPUB -- le format actuel du livre numérique -- et le web : comment réussir à utiliser le web pour raconter une, des histoires ? Comment rapprocher le web et le livre ?

>Here we are, in 2016, with all this new web technology, and I can easily imagine a feature in your browser where making a book is as easy as making a bookmark. Just click a button to add a new chapter to your story.  
Thus we turn the playlist into the mixtape, and the web becomes a net that catches the story.