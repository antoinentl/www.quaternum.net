---
layout: post
title: "Living Books About History"
date: "2017-04-13T10:00:00"
comments: true
published: true
description: "Une initiative des plus réjouissante, tant en terme de forme que de fond ! Avec notamment Digital Humanities de Tara Andrews ou Histoires de l'Internet et du Web de Valérie Schafer et Alexandre Serres."
categories: 
- flux
tags:
- livre
---
>Les Living Books about History sont une nouvelle forme d'anthologies digitales. Ils présentent des essais courts sur des sujets de recherche actuels, complétés par une sélection raisonnée de contributions librement accessibles en ligne.  
Le projet vise à expérimenter un nouveau format de publication scientifique, à favoriser la découverte et la réutilisation de textes et de sources, et à attirer l'attention sur les avantages de l'open access pour la recherche scientifique.  
[Living Books About History](http://livingbooksabouthistory.ch/)

Une initiative des plus réjouissante, tant en terme de forme que de fond ! Avec notamment *Digital Humanities* de Tara Andrews ou *Histoires de l'Internet et du Web* de Valérie Schafer et Alexandre Serres.

Via [Joël](http://www.lekti.fr/blog/).