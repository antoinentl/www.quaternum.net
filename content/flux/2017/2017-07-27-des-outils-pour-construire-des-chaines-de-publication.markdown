---
layout: post
title: "Des outils pour construire des chaînes de publication"
date: "2017-07-27T17:00:00"
comments: true
published: true
description: "Des outils pour construire des chaînes de publication, c'est ce que propose cette initiative de The Collaborative Knowledge Foundation."
categories:
- flux
tags:
- publication
---
>PubSweet is a free, open source toolkit for building state-of-the-art publishing workflows.  
[PubSweet, the open toolkit for building publishing workflows](https://pubsweet.org/)

Des outils pour construire des chaînes de publication, c'est ce que propose cette initiative de [The Collaborative Knowledge Foundation](https://coko.foundation/).