---
layout: post
title: "Les futurs de la typographie"
date: "2017-01-07T22:00:00"
comments: true
published: true
description: "Nouvel essai de Robin Rendle : après The New Web Typography en février 2016 avec des références à Jan Tschichold, voici un texte qui présente les possibilités, ou plutôt les outils, dont les designers vont pouvoir s'emparer pour offrir de nouvelles expériences, dans le web. Le point de départ s'appuie sur deux figures historiques de la typographie : Johannes Gensfleisch zur Laden zum Gutenberg et Aldus Pius Manutius."
categories: 
- flux
tags:
- typographie
---
>The future of typography is not to be found in new letters alone, but a series of tools, techniques and options combined together.  
[Robin Rendle, The Futures of Typography](https://robinrendle.com/essays/futures-of-typography/)

Nouvel essai de Robin Rendle : après [The New Web Typography](https://robinrendle.com/essays/new-web-typography/) en février 2016 avec des références à Jan Tschichold, voici un texte qui présente les possibilités, ou plutôt les outils, dont les designers vont pouvoir s'emparer pour offrir de nouvelles expériences, dans le web. Le point de départ s'appuie sur deux figures historiques de la typographie : Johannes Gensfleisch zur Laden zum Gutenberg et Aldus Pius Manutius.

>When seen as separate, disparate technologies the few upgrades I’ve mentioned above don’t seem all that impressive. But when the ServiceWorker API, new font formats, and Grid Layout are brought together the web is suddenly very different to the one that we experience today.