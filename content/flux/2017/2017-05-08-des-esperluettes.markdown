---
layout: post
title: "Des esperluettes"
date: "2017-05-08T22:00:00"
comments: true
published: true
description: "Voici les résultats d'un workshop organisé par la fonderie Velvetyne et les éditions Zeug, placé sous le signe de l'esperluette. Plus de 400 esperluettes ont été dessinées et/ou numérisées ! À noter que cette production s'inscrit dans le projet de traduction en français de l'ouvrage Évolutions formelles de l'esperluette de Jan Tschichold."
categories: 
- flux
tags:
- typographie
---
>À ceux qui pensent que la technique a déshumanisé l'homme, Gilbert Simondon répond que c'est l'homme, justement, qui a déshumanisé la technique.  
[Velvetyne's Worldwide ampersand call](http://velvetyne.fr/saintjean/gallery)

Voici les résultats d'un workshop organisé par la fonderie Velvetyne et les éditions Zeug, "placé sous le signe de l'esperluette". Plus de 400 esperluettes ont été dessinées et/ou numérisées ! À noter que cette production s'inscrit dans le projet de traduction en français de l'ouvrage *Évolutions formelles de l'esperluette* de Jan Tschichold.