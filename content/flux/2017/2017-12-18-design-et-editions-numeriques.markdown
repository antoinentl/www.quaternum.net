---
layout: post
title: "Design et Éditions numériques"
date: "2017-12-18T10:00:00"
comments: true
published: true
description: "La revue Sciences du Design lance son appel à articles pour le numéro 08 sur le thème Éditions numériques, les propositions peuvent être transmises jusqu'au 18 janvier 2018 !"
categories:
- flux
tags:
- publication
---
>Passé le constat de l’omniprésence des dispositifs numériques dans la chaîne éditoriale et du bouleversement qu’ils induisent, le numéro 08 de Sciences du Design se propose d’étudier les éditions numériques contemporaines et leurs enjeux de design sous trois prismes : leurs aspects expérimentaux et leurs dispositifs émergents en arts et littérature, leur rôle dans la production et la transmission de connaissance pour les publications savantes, ainsi que leurs enjeux professionnels pour les métiers des éditions numériques.  
[Sciences du Design, 08 — Éditions numériques](http://www.sciences-du-design.org/aac/sciences-du-design-08-editions-numeriques/)

La revue [Sciences du Design](http://www.sciences-du-design.org/) lance son appel à articles pour le numéro 08 sur le thème "Éditions numériques", les propositions peuvent être transmises jusqu'au 18 janvier 2018 !
