---
layout: post
title: "Entretien autour de l'accessibilité d'un livre numérique"
date: "2017-03-16T10:00:00"
comments: true
published: true
description: "Sylvie Duchateau d'Access42 a interviewé Tristan Nitot, Nicolas Taffin et moi-même, autour du livre surveillance:// publié chez C&F Éditions. Un échange très riche, qui a permis de réunir quatre sujets rarement évoqués ensemble : les données personnelles, l'édition, le livre numérique et l'accessibilité ! Sans être un spécialiste de l'accessibilité du livre numérique, cet entretien m'a permis d'expliquer que l'accessibilité est avant tout une question d'approche et de bonnes pratiques."
categories: 
- flux
tags:
- accessibilité
slug: entretien-autour-de-l-accessibilite-d-un-livre-numerique
---
>Lire n’est pas consommer, c’est déjà produire du sens.  
[Nicolas Taffin, Dans les coulisses de la mise en accessibilité du livre « surveillance :// »](http://access42.net/coulisse-accessibilite-surveillance.html)

Sylvie Duchateau d'[Access42](http://access42.net/) a interviewé Tristan Nitot, Nicolas Taffin et moi-même, autour du livre surveillance:// publié chez C&F Éditions. Un échange très riche, qui a permis de réunir quatre sujets rarement évoqués ensemble : les données personnelles, l'édition, le livre numérique et l'accessibilité ! Sans être un spécialiste de l'accessibilité du livre numérique, cet entretien m'a permis d'expliquer que l'accessibilité est avant tout une question d'approche et de bonnes pratiques.