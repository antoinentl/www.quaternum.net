---
layout: post
title: "Exemplaires 2017"
date: "2017-02-24T22:00:00"
comments: true
published: true
description: "Une manifestation et un colloque qui valent le coup, en tout cas l'édition 2015 était de qualité, j'avais d'ailleurs écrit un billet sur OSP : OSP : non-outil."
categories: 
- flux
tags:
- livre
---
>Cette manifestation permet d’illustrer des pratiques qui débordent du seul champ du design graphique et dans lesquelles se mêlent différentes disciplines. Elle laisse apparaître des questions (Quels en sont les acteurs ? Quels sont aujourd’hui les enjeux et les pratiques de l’édition contemporaine ?), esquisse des réponses, ouvre des potentialités.  
[Exemplaires, formes et pratiques de lʼédition](http://exemplaires2017.fr/presentation)

Une manifestation et un colloque qui valent le coup, en tout cas l'édition 2015 était de qualité, j'avais d'ailleurs écrit un billet sur OSP : [OSP : non-outil](/2015/04/14/osp-non-outil/).