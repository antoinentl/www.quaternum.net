---
layout: post
title: "De la page au flux"
date: "2017-06-27T10:00:00"
comments: true
published: true
description: "Bianca Tangaro, étudiante en Master Publication numérique (entre autre), a écrit cet article, contribution importante aux recherches sur la conception de livres numériques, et plus globalement de publications numériques. À lire en urgence !."
categories:
- flux
tags:
- publication
---
>Nous voudrions ici rendre compte du fait que la réalisation d’un livre numérique implique une certaine conception du livre différente de celle historiquement focalisée sur la page destinée à la seule impression sur papier. [...] Ainsi, nous avons pu constater qu’il y a peu de textes ou études permettant de comprendre le changement fondamental qui s’opère lors de la conception d’un livre numérique par rapport à la conception d’un livre destiné à être imprimé.  
[Bianca Tangaro, De la page au flux : la conception du livre numérique](http://dlis.hypotheses.org/1255)

Bianca Tangaro, étudiante en Master Publication numérique (entre autre), a écrit cet article. Contribution importante aux recherches sur la conception de livres numériques, et plus globalement des publications numériques, il faut le lire d'urgence !

>La conception de livres numériques exige donc une conversion du regard par rapport à l’objet livre. Pendant des siècles la pensée a raisonné sur les textes et les a conçues de manière "page-centrée" : autrement dit l’espace de la page était au centre de la conception de l’imprimeur et de l’éditeur. Le concepteur s’adaptait à son espace-page. En développant un livre numérique au contraire, il n’y a plus de centre à partir duquel l’on peut penser et concevoir les textes : les textes ne sont plus "page-centrés" mais "fluides" et recomposables.