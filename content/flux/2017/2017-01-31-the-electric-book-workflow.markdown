---
layout: post
title: "The Electric Book workflow"
date: "2017-01-31T22:00:00"
comments: true
published: true
description: "Dans le même esprit que ce que The Getty Publication a mis en place : une chaîne de publication basée sur des outils issus du web, afin de simplifier les processus, et d'envisager les différentes versions d'un même livre dès l'écriture."
categories: 
- flux
tags:
- publication
---
>The Electric Book workflow is a set of tools and processes for creating high-quality books. [...] It lets you store books in plain text with great version control, and output website versions, ebooks and print editions easily from a single source.  
[Arthur Attwell, The Electric Book workflow](http://electricbook.works/)

Dans le même esprit que ce que [The Getty Publication a mis en place](https://jamstatic.fr/2017/01/23/produire-des-livres-avec-le-statique/) : une chaîne de publication basée sur des outils issus du web, afin de simplifier les processus, et d'envisager les différentes versions d'un même livre dès l'écriture.