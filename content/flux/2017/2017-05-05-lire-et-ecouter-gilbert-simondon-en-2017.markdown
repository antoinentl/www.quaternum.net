---
layout: post
title: "Lire et écouter Gilbert Simondon en 2017"
date: "2017-05-05T22:00:00"
comments: true
published: true
description: "Étant en plein dans la lecture Du mode d'existence des objets techniques de Gilbert Simondon, je ne résiste pas à partager ce podcast. Lire Gilbert Simondon en 2017 m'apparaît essentiel pour qui veut interroger ses pratiques et son rapport au numérique."
categories: 
- flux
tags:
- design
---
>À ceux qui pensent que la technique a déshumanisé l'homme, Gilbert Simondon répond que c'est l'homme, justement, qui a déshumanisé la technique.  
[Les Chemins de la philosophie, Du mode d’existence d’un penseur technique](https://www.franceculture.fr/emissions/les-nouveaux-chemins-de-la-connaissance/gilbert-simondon-14-du-mode-d-existence-d-un)

Étant en plein dans la lecture [*Du mode d'existence des objets techniques* de Gilbert Simondon](https://fr.wikipedia.org/wiki/Du_mode_d%27existence_des_objets_techniques), je ne résiste pas à partager ce podcast. Lire Gilbert Simondon en 2017 m'apparaît essentiel pour qui veut interroger ses pratiques et son rapport au numérique.