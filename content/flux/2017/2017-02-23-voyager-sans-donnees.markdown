---
layout: post
title: "Voyager sans données"
date: "2017-02-23T22:00:00"
comments: true
published: true
description: "Voyager sans données, pour protéger nos intimités personnelles et celles des autres."
categories: 
- flux
tags:
- privacy
---
>Mon prochain voyage aux États-Unis pour le travail, je pense [y aller sans données personnelles](http://www.otsukare.info/2017/01/30/no-data-travels), puisque le simple passage de la frontière est un risque. Le risque a plus d'une ramification. Ce que nous transportons dans nos appareils mobiles et nos ordinateurs sont des tranches de vie, non pas seulement la notre, mais celles de tous nos contacts, de toutes personnes avec qui nous communiquons. Cela me pose déjà un problème pour mes données personnelles, mais c'est encore plus terrifiant pour les données des autres. Une forme de responsabilité supplémentaire surgit.  
[Karl, La folie des frontières](http://www.la-grange.net/2017/02/08/folie)

Voyager sans données, pour protéger nos intimités personnelles et celles des autres.