---
layout: post
title: "Le futur du livre"
date: "2017-01-09T22:00:00"
comments: true
published: true
description: "Les réflexions de l'éditeur Walrus, en ce qui concerne le livre et le texte, sont aussi irrégulières que pertinentes. Cet appel à considérer autrement le texte, le livre, le web et le numérique est réjouissant !"
categories: 
- flux
tags:
- livre
---
>Offrons de nouvelles possibilités narratives aux auteurs en les débarrassant du carcan du livre. Jouons des possibilités graphiques et typographiques de l’écran. Brouillons les frontières entre web et livre — n’envisageons qu’un seul corps pour nos créations, celui de l’écran.  
[Walrus, Livre numérique : ce que nous avons raté, pourquoi nous l’avons raté, et comment nous allons changer les choses](http://www.walrus-books.com/livre-numerique-ce-que-nous-avons-rate-pourquoi-nous-lavons-rate-et-comment-nous-allons-changer-les-choses/)

Les réflexions de l'éditeur [Walrus](http://www.walrus-books.com/), en ce qui concerne le livre et le texte, sont aussi irrégulières que pertinentes. Cet appel à considérer autrement le texte, le livre, le web et le numérique est réjouissant !