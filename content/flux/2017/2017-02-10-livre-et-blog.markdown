---
layout: post
title: "Livre et blog"
date: "2017-02-10T10:00:00"
comments: true
published: true
description: "Depuis plusieurs semaines David s'est lancé dans une forme de correspondance, en ligne. Cette lettre à Marion dessine les différences entre livre et blog, cette lettre parle de pratiques d'écriture d'aujourd'hui."
categories: 
- flux
tags:
- publication
---
>[...] le blog est le fruit d’une évolution personnelle grâce à l’intelligence du collectif qui a pris la peine d’échanger, de proposer d’autres voies. Un journal qui n’est plus un *curriculum vitae* mais un chemin de pensée montrant une progression au fil des années.  
[...]  
Le blog se rapproche de l’*open-source* sur ce plan là qui consiste à exposer son code à la critique collective en vue de le rendre plus pertinent.  
[David Larlet, Lettre à Marion](https://larlet.fr/david/correspondances/2017/02/09/)

Depuis plusieurs semaines [David](https://larlet.fr/david/) s'est lancé dans une forme de correspondance, en ligne. Cette lettre à Marion dessine les différences entre livre et blog, cette lettre parle de pratiques d'écriture d'aujourd'hui.