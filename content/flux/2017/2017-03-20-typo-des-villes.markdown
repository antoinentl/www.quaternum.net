---
layout: post
title: "Typo des villes"
date: "2017-03-20T12:00:00"
comments: true
published: true
description: "Après Sacrés caractères en 2014, Thomas Sipp revient avec la réalisation de 8 épisodes d'une nouvelle série typographique, encore avec Bureau 205 au graphisme."
categories: 
- flux
tags:
- typographie
---
>Plongez en 8 épisodes dans la jungle "typographique" de Paris, Londres, Berlin, Barcelone, Marseille, Amsterdam ou encore Montréal et (re)découvrez ces inscriptions qui influencent notre perception de la ville !  
[arte, Safari Typo !](http://creative.arte.tv/fr/safaritypo)

Après [Sacrés caractères](https://www.franceculture.fr/litterature/sacres-caracteres-une-webserie-de-12-films-courts-sur-des-polices-qui-ont-du-caractere) en 2014, Thomas Sipp revient avec la réalisation de 8 épisodes d'une nouvelle série typographique, encore avec Bureau 205 au graphisme.