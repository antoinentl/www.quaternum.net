---
layout: post
title: "Navigation oblique ?"
date: "2017-01-12T22:00:00"
comments: true
published: true
description: "Dans le numéro 7 de la revue Back Cover des Éditions B42, Anthony Masure et Kevin Donnot présente le site du photographe Enrico Floriddia, réalisé par Raphaël Bastide : slanted.cc."
categories: 
- flux
tags:
- web
---
>Que ce soit sur les supports imprimés et numériques, nous sommes toujours assez liés à l'orthogonalité pour des raisons techniques, pratiques et d'habitude. Et si la navigation était oblique ?  
[Anthony Masure et Kevin Donnot, Web oblique](http://editions-b42.com/books/back-cover-7/)

Dans le numéro 7 de la revue Back Cover des [Éditions B42](http://editions-b42.com/), Anthony Masure et Kevin Donnot présente le site du photographe Enrico Floriddia, réalisé par [Raphaël Bastide](http://raphaelbastide.com/) : [slanted.cc](http://slanted.cc/).