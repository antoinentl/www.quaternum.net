---
layout: post
title: "Compte rendu du W3C Publishing Summit 2017"
date: "2017-11-20T22:00:00"
comments: true
published: true
description: "Teresa Elsey a rédigé une synthèse du premier Publishing Summit du W3C, un très bon moyen de suivre les évolutions des travaux des groupes du W3C et des structures membres."
categories:
- flux
tags:
- publication
---
>Jeff Jaffe, CEO of the W3C, spoke on "frictionless publishing" – the idea that the web standards (HTML or XML) that we use to build ebooks could be used throughout the whole publishing life cycle, thus avoiding the multiple transformations of most publishing processes (e.g., Word > InDesign > EPUB). This idea was not brand new to me, but he added the idea that since open web standards are also working on next things like accessibility, video, and virtual reality, if we adopt tools/processes that use those web standards we will get all those things along with them.  
[Teresa Elsey, W3C Publishing Summit 2017: An Ebook Dev’s View](http://epubsecrets.com/w3c-publishing-summit-2017-an-ebook-devs-view.php)

Teresa Elsey a rédigé une synthèse du premier [Publishing Summit du W3C](https://www.w3.org/publishing/events/summit2017), un très bon moyen de suivre les évolutions des travaux des groupes du W3C et des structures membres.