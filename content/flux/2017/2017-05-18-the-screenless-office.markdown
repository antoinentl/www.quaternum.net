---
layout: post
title: "The Screenless Office"
date: "2017-05-18T22:00:00"
comments: true
published: true
description: "Un projet original et intrigant, à suivre."
categories:
- flux
tags:
- design
---
>The Screenless Office is a system for working with media and networks without using a pixel-based display. It is an artistic operating system.  
[The Screenless Office](http://screenl.es/)

Un projet original et intrigant, à suivre.

Via [Anthony Masure](https://twitter.com/AnthonyMasure/status/863695449835941888).
