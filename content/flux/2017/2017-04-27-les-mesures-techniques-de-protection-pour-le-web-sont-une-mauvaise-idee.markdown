---
layout: post
title: "Les mesures techniques de protection pour le web sont une mauvaise idée"
date: "2017-04-27T22:00:00"
comments: true
published: true
description: "Le débat autour de l'implémentation de EME (Encrypted Media Extensions), du point de vue de archive.org."
categories: 
- flux
tags:
- web
---
>At your request we have assessed what the possible effects of the Encrypted Media Extensions (EME) as a W3C recommendation would be.  
We believe it will be dangerous to the open web unless protections are put in place for those who engage in activities, such as archiving, that are threatened by the legal regime governing the standard.  
[Lila Bailey, DRM for the Web is a Bad Idea](https://blog.archive.org/2017/04/18/drm-for-the-web-is-a-bad-idea/)

Le débat autour de l'implémentation de EME (Encrypted Media Extensions), du point de vue de archive.org.