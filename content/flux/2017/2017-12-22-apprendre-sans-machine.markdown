---
layout: post
title: "Apprendre sans machine"
date: "2017-12-22T22:00:00"
comments: true
published: true
description: "De la présence des machines dans les temps d'apprentissage : pour Susan Dynarski il faut bannir l'électronique des salles de classe !"
categories:
- flux
tags:
- métier
---
>The research is unequivocal: Laptops distract from learning, both for users and for those around them. It’s not much of a leap to expect that electronics also undermine learning in high school classrooms or that they hurt productivity in meetings in all kinds of workplaces.  
[Susan Dynarski, Laptops Are Great. But Not During a Lecture or a Meeting.](https://www.nytimes.com/2017/11/22/business/laptops-not-during-lecture-or-meeting.html)

De la présence des machines dans les temps d'apprentissage : pour Susan Dynarski il faut bannir l'électronique des salles de classe !
