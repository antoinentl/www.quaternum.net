---
layout: post
title: "Muriel Cooper et Theodor Nelson, convergence"
date: "2017-03-24T12:00:00"
comments: true
published: true
description: "Un article sur Muriel Cooper et Theodor Nelson, sur la plateforme de publication Radôme des étudiants de l'ÉESAB de Rennes."
categories: 
- flux
tags:
- design
slug: muriel-cooper-et-theodor-nelson
---
>Il s’agit là de présenter aux étudiant(e)s le design graphique et la communication, en leur donnant accès à des outils et méthodes de reproduction au sein même de la recherche. Muriel Cooper y témoigne, entre autres, de ses préoccupations principales : son intérêt pour les mécanismes de production, et pour le rapprochement, autant que possible, du processus de fabrication et de celui de la production.  
[Adeline Racaud, De Theodor Nelson à Muriel Cooper](http://radome.eesab.fr/de-theodor-h-nelson-a-muriel-cooper/)

Un article sur Muriel Cooper et Theodor Nelson, sur la plateforme de publication [Radôme](http://radome.eesab.fr/a-propos/) des étudiants de l'ÉESAB de Rennes.