---
layout: post
title: "PrePostPrint, un workshop autour des systèmes de publication alternatifs"
date: "2017-03-17T14:00:00"
comments: true
published: true
description: "Sarah Garcin, Raphaël Bastide, Étienne Ozeray et Romain Marula organisent un workshop autour des systèmes de publication alternatifs, les 6 et 7 avril 2017 à Paris, autant dire que le sujet est d'actualité ! J'y serai, et vous ?"
categories: 
- flux
tags:
- publication
---
>PrePostPrint is a two days open workshop dedicated to the experimentation of alternative publishing systems.  
[PrePostPrint](http://prepostprint.org/)

Sarah Garcin, Raphaël Bastide, Étienne Ozeray et Romain Marula organisent un workshop autour des systèmes de publication alternatifs, les 6 et 7 avril 2017 à Paris, autant dire que le sujet est [d'actualité](/2017/03/13/une-chaine-de-publication-inspiree-du-web/) ! J'y serai, et vous ?