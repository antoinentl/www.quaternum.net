---
layout: post
title: "Un livre dans un navigateur"
date: "2017-09-28T17:00:00"
comments: true
published: true
description: "Expérimentation intéressante du design d'un livre dans un navigateur web. On pourrait croire à une démarche de skeuomorphisme, ou à un autre exemple de livre web, mais il s'agit plutôt d'une recherche en design qui ouvre quelques prespectives intéressantes : épaisseur du livre, organisation en chapitres et sous-chapitres, réglages utilisateur, etc."
categories:
- flux
tags:
- publication
slug: un-livre-dans-le-navigateur
---
>Exploring new ways for reading online, the book emphasizes accessibility, offering a range of inputs through which to engage with the text.  
[Filip Visnjic, Poetic Computation: Reader – Code as a form of poetry and aesthetic](http://www.creativeapplications.net/theory/poetic-computation-reader-code-as-a-form-of-poetry-and-aesthetic/)

Expérimentation intéressante du design d'un livre dans un navigateur web. On pourrait croire à une démarche de skeuomorphisme, ou à un autre exemple de [livre web](https://github.com/antoinentl/web-books-initiatives), mais il s'agit plutôt d'une recherche en design qui ouvre quelques prespectives intéressantes : *épaisseur* du livre, organisation en chapitres et sous-chapitres, réglages utilisateur, etc.

Via [Anthony](https://twitter.com/AnthonyMasure/status/913361750812225536) et [Jiminy](https://twitter.com/JiminyPan/status/913349873268924416).