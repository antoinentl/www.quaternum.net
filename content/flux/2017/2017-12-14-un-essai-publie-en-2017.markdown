---
layout: post
title: "Un essai publié en 2017"
date: "2017-12-14T23:00:00"
comments: true
published: true
description: "Sorti début décembre 2017 chez B42, le nouveau livre d'Anthony Masure, Design et humanités numériques, a un pendant numérique : des textes et des analyses de cas qui viennent compléter la version imprimée – ou inversement. Et ce jeudi 14 décembre 2017 Anthony Masure est venu présenter son livre et le making of d'écriture et d'édition à l'enssib."
categories:
- flux
tags:
- livre
---
>En quoi les environnements numériques contemporains actualisent-ils les modes de production et de transmission des savoirs ? Quelle est la place des designers dans des projets relevant des sciences humaines et sociales ?  
[Anthony Masure, Design et humanités numériques](http://esthetique-des-donnees.editions-b42.com/)

Sorti début décembre 2017 chez B42, le nouveau livre d'Anthony Masure, *Design et humanités numériques*, a un pendant numérique : des textes et des analyses de cas qui viennent compléter la version imprimée – ou inversement. Et ce jeudi 14 décembre 2017 Anthony Masure est venu présenter son livre sous forme de making of de l'écriture et de l'édition [à l'enssib](http://www.enssib.fr/conference-anthony-masure).