---
layout: post
title: "Pourquoi tous le monde n'aime pas lire des livres numériques ?"
date: "2017-11-22T22:00:00"
comments: true
published: true
description: "Voici une question dont je peux soupçonner les réponses, le résumé et le sommaire apportent déjà de nombreuses informations intéressantes (fatigue des yeux, confort de lecture, DRMs, distraction, design des livres numériques, etc.). Article à lire, donc."
categories:
- flux
tags:
- ebook
slug: pourquoi-tous-le-monde-n-aime-pas-lire-des-livres-numeriques
---
>Why do many students still prefer paper books to e-books? This article summarizes a number of problems with e-books mentioned in different studies by students of higher education, but it also discusses some of the unexploited possibilities with e-books.  
[Caroline Myrlberg, Why doesn’t everyone love reading e-books?](https://insights.uksg.org/articles/10.1629/uksg.386/)

Voici une question dont je peux soupçonner les réponses, le résumé et le sommaire apportent déjà de nombreuses informations intéressantes (fatigue des yeux, confort de lecture, DRMs, distraction, design des livres numériques, etc.). Article à lire, donc.