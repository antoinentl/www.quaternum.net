---
layout: post
title: "Le numérique en vrai"
date: "2017-01-18T22:00:00"
comments: true
published: true
description: "Belle émission qui replace le numérique dans un contexte plus global, réel, humain, par rapport à ce que nous -- les acculturés du numérique -- avons tendance à poser. Cela fait du bien d'écouter ces témoignages de vie, et cela donne envie de se retrousser les manches, notamment pour un web plus inclusif."
categories: 
- flux
tags:
- web
---
>Si la précarité s'éprouve aussi à travers la fracture numérique, des initiatives solidaires existent pour la contrer.  
[Les Nouvelles vagues, Connexions solidaires](https://www.franceculture.fr/emissions/les-nouvelles-vagues/la-precarite-35-connexions-solidaires)

Belle émission qui replace le numérique dans un contexte plus global, réel, humain, par rapport à ce que nous -- les acculturés du numérique -- avons tendance à poser. Cela fait du bien d'écouter ces témoignages de vie, et cela donne envie de se retrousser les manches, notamment pour un web plus inclusif.