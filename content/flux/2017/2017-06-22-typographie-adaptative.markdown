---
layout: post
title: "Typographie adaptative"
date: "2017-06-22T10:00:00"
comments: true
published: true
description: "Voici une démonstration de la typographie adaptative, ou responsive typography : les paramètres -- épaisseur, largeur, contraste, etc. -- des caractères d'un texte évoluent en fonction de certaines indications."
categories:
- flux
tags:
- typographie
---
>Spectral, designed by our partner Production Type, is the first Google font turned parametric by Prototypo. Opening up to a new era of type design, the parametric font technology allows to work with responsive characters improving creativity and exploring new shapes. Make custom unique fonts, stronger design identities and even better user experience.  
[Prototypo, Spectral, the first parametric Google font by Prototypo](https://spectral.prototypo.io/)

Voici une démonstration de la typographie adaptative, ou *responsive typography* : les paramètres -- épaisseur, largeur, contraste, etc. -- des caractères d'un texte évoluent en fonction de l'environnement et des interactions.