---
layout: post
title: "Bataille pour un web ouvert"
date: "2017-03-13T10:00:00"
comments: true
published: true
description: "Un outil pour déterminer la qualité du contraste entre deux couleurs -- par exemple celle d'un texte et celle du fond --, très pratique et complémentaire de Accessible color palette builder."
categories: 
- flux
tags:
- web
---
>Pour construire le web, il a fallu notre participation à tous, et c’est à nous tous, désormais, de construire le web que nous voulons – pour tous.  
[Tim Berners-Lee, Three challenges for the web, according to its inventor](http://webfoundation.org/2017/03/web-turns-28-letter/#VersionFR)

Pour les 28 ans de l'invention du web, Tim Berners-Lee publie une tribune dans laquelle il dénonce trois tendances qui visent à restreindre l'ouverture du web : perte du contrôle de nos données personnelles, question de la désinformation et de sa diffusion, et propagande politique en ligne.