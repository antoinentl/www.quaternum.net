---
layout: post
title: "Les adresses sont des interfaces"
date: "2017-07-11T17:00:00"
comments: true
published: true
description: "De l'intérêt de disposer d'URLs claires, concises, (résilientes ?), et qui ont du sens !"
categories:
- flux
tags:
- web
---
>So many folks spend time on their CSS and their UX/UI but still come up with URLs that are at best, comically long, and at worst, user hostile.  
[Scott Hanselman, URLs are UI](https://www.hanselman.com/blog/URLsAreUI.aspx)

De l'intérêt de disposer d'URLs claires, concises, (résilientes ?), et qui ont du sens !