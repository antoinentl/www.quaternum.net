---
layout: post
title: "Aaron Swartz, écrits"
date: "2017-03-23T12:00:00"
comments: true
published: true
description: "Les Éditions B42 publient un recueil de textes d'Aaron Swartz, et c'est une très bonne nouvelle !"
categories: 
- flux
tags:
- privacy
---
>Aaron Swartz (1986-2013) était programmeur informatique, essayiste et hacker-activiste. Convaincu que l’accès à la connaissance constitue le meilleur outil d’émancipation et de justice, il consacra sa vie à la défense de la « culture libre ».  
[Éditions B42, Celui qui pourrait changer le monde](http://editions-b42.com/books/celui-qui-pourrait-changer-le-monde/)

Les Éditions B42 publient un recueil de textes d'Aaron Swartz, et c'est une très bonne nouvelle !