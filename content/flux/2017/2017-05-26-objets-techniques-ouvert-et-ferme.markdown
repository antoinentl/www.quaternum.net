---
layout: post
title: "Objets techniques ouvert et fermé"
date: "2017-05-26T01:00:00"
comments: true
published: true
description: "La distinction entre objet technique ouvert et objet technique fermé dans la pensée de Gilbert Simondon, raisonne forcément avec logiciel/code ouvert et fermé en 2017. Il est intéressant de tenter d'appliquer les concepts de Gilbert Simondon dans notre monde contemporain."
categories:
- flux
tags:
- design
---
>Si le réparateur – qui peut être l'utilisateur – peut perpétuellement maintenir une œuvre, les pièces qui s'usent, alors il n'y a pas de date, il n'y a pas de vieillissement. Sur une base qui est une base de pérennité ou tout du moins de grande solidité, on peut installer des pièces qui devront être remplacées, mais en tout cas qui laissent le schème fondamental intact et qui même permettent de l'améliorer. [...] Voilà ce que j'appelle l'objet ouvert.  
[Gilbert Simondon, Gilbert Simondon sur les objets techniques "ouverts" et "fermés" (1967)](https://www.youtube.com/watch?v=ScotWBb6ROo)

La distinction entre "objet technique ouvert" et "objet technique fermé" dans la pensée de Gilbert Simondon, raisonne forcément avec logiciel/code ouvert et fermé en 2017. Il est intéressant de tenter d'appliquer les concepts de Gilbert Simondon dans notre monde contemporain.
