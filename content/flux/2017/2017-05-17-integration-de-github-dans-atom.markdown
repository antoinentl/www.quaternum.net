---
layout: post
title: "Intégration de GitHub dans Atom"
date: "2017-05-17T22:00:00"
comments: true
published: true
description: "L'intégration de GitHub dans l'éditeur de texte Atom est probablement une fausse bonne idée : comment privilégier d'autres éditeurs de texte si vous utilisez GitHub ? Comment se passer de la plateforme GitHub si vous utilisez Atom ? Cette nouvelle fonctionnalité, GitHub breaks out of the browser, au demeurant très prometteuse, risque de donner un peu plus de suprématie à GitHub. Un peu de diversité est pourtant nécessaire."
categories: 
- flux
tags:
- outils
---
>A text editor is at the core of the developer’s toolbox, but many other useful pieces of software coexist along with it, such as Git and GitHub. Starting today, Atom adds Git and GitHub integration directly in Atom via the GitHub package.  
[Michelle Tilley, Git and GitHub Integration comes to Atom](https://blog.atom.io/2017/05/16/git-and-github-integration-comes-to-atom.html)

L'intégration de GitHub dans l'éditeur de texte Atom est probablement une fausse bonne idée : comment privilégier d'autres éditeurs de texte si vous utilisez GitHub ? Comment se passer de la plateforme GitHub si vous utilisez Atom ? Cette nouvelle fonctionnalité, "GitHub breaks out of the browser", au demeurant très prometteuse, risque de donner un peu plus de suprématie à GitHub. Un peu de diversité est pourtant nécessaire.