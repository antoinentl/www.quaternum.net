---
layout: post
title: "Enjeux de la recherche"
date: "2017-04-28T22:00:00"
comments: true
published: true
description: "Ambitieuse entreprise de Pierre-Damien Huyghe dans son nouvel ouvrage qui paraît aux éditions B42."
categories: 
- flux
tags:
- livre
---
>Alors que les perspectives des universités se modifiaient au niveau européen, les écoles d’art en France se sont trouvées obligées de répondre à ce que Pierre-Damien Huyghe présente comme une « injonction à faire de la recherche ». Cette injonction a un contexte : le merchandising des savoirs. Le philosophe dresse un état des conséquences en matière d’éducation et d’enseignement, et examine les possibilités demeurant dans les champs des arts, du design et de l’architecture.  
[B42, *Contre-temps* de Pierre-Damien Huyghe](http://editions-b42.com/books/contre-temps/)

Ambitieuse entreprise de Pierre-Damien Huyghe dans son nouvel ouvrage qui paraît aux éditions B42.