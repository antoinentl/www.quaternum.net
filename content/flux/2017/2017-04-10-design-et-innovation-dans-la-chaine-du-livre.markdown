---
layout: post
title: "Design et innovation dans la chaîne du livre"
date: "2017-04-10T10:00:00"
comments: true
published: true
description: "Suite au colloque ECRiDiL -- Écrire, éditer et lire à l'ère numérique -- qui s'est déroulé en avril 2016, voici un ouvrage qui rassemble les interventions, il est disponible depuis quelques jours. J'avais participé avec Le livre web, une autre forme du livre numérique, il y a beaucoup d'autres contenus très riches à découvrir, commandez-le à votre libraire !"
categories: 
- flux
tags:
- livre
---
>Quels sont les mécanismes et enjeux du geste technique d’écrire ? Qu’est-ce que « publier » dans une société numérique ? Comment le numérique transforme-t-il l’idée et l’acte d’écrire ? Dans quelle mesure les interfaces numériques refaçonnent-elles les activités d’écriture et de lecture à l’écran ?  
[Stéphane Vial et Marie-Julie Catoir-Brisson, Design et innovation dans la chaîne du livre](https://www.puf.com/content/Design_et_innovation_dans_la_cha%C3%AEne_du_livre)

Suite au colloque [ECRiDiL](http://ecridil.hypotheses.org/) -- Écrire, éditer et lire à l'ère numérique -- qui s'est déroulé en avril 2016, voici un ouvrage qui rassemble les interventions, il est disponible depuis quelques jours. J'avais participé avec [Le livre web, une autre forme du livre numérique](/2016/10/24/le-livre-web-une-autre-forme-du-livre-numerique/), il y a beaucoup d'autres contenus très riches à découvrir, commandez-le à votre libraire !