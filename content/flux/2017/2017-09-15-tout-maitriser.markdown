---
layout: post
title: "Tout maîtriser ?"
date: "2017-09-15T17:00:00"
comments: true
published: true
description: "Un débat intéressant non pas sur la technique (faut-il maîtriser JavaScript ?) mais sur la situation d'une communauté qui manque parfois d'empathie ou d'objectivité (on ne peut pas tout maîtriser)."
categories:
- flux
tags:
- métier
---
>I love CSS, I love HTML and I love JavaScript. They are all important and all valuable in the front end space. What I *don’t* love, is the constant infighting and devaluing. Let’s stop that and appreciate all the things. If you want to and have capacity to learn all the things, awesome, but if you don’t that is also okay.  
[Mandy Michael, Is there any value in people who cannot write JavaScript?](https://medium.com/@mandy.michael/is-there-any-value-in-people-who-cannot-write-javascript-d0a66b16de06)

Un débat intéressant non pas sur la technique (faut-il maîtriser JavaScript ?) mais sur la situation d'une communauté qui manque parfois d'empathie ou d'objectivité (on ne peut pas tout maîtriser).