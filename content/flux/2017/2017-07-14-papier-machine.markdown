---
layout: post
title: "Papier Machine"
date: "2017-07-14T17:00:00"
comments: true
published: true
description: "Superbes expérimentations postnumériques à base de papier et d'encre conductrice."
categories:
- flux
tags:
- publication
---
>An expedition in the invisible aesthetics of the electronic.  
[Marion Pinaffo et Raphaël Pluvinage, Papier Machine](https://www.papiermachine.io/)

Superbes expérimentations *postnumériques* à base de papier et d'encre conductrice.

Via [Loup Cellard](https://twitter.com/CellardLoup/status/885840610153910277).