---
layout: post
title: "La déconnexion sera-t-elle encore possible ?"
date: "2017-03-21T12:00:00"
comments: true
published: true
description: "Evgeny Morozov parle de privacy, d'addiction, de centralisation et de futur."
categories: 
- flux
tags:
- privacy
slug: la-deconnexion-sera-t-elle-encore-possible
---
>A-t-on vraiment les moyens de se déconnecter des compagnies d’assurance, des banques et des services d’immigration ? En principe, oui, si l’on peut assumer les coûts sociaux et économiques croissants de la déconnexion et de l’anonymat. Ceux qui souhaitent se déconnecter finiront par devoir payer ce privilège : taux d’emprunt plus élevés, contrats d’assurance plus onéreux et plus de temps perdu à essayer de convaincre les douaniers de leurs bonnes intentions.  
[Evgeny Morozov, Le prix de la déconnexion](https://blog.mondediplo.net/2017-02-23-Le-prix-de-la-deconnexion)

Evgeny Morozov parle de *privacy*, d'addiction, de centralisation et de futur.