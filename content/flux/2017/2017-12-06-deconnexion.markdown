---
layout: post
title: "Déconnexion"
date: "2017-12-06T22:00:00"
comments: true
published: true
description: "Une page web lisible uniquement hors connexion (oui vous devez couper votre connexion pour lire le contenu de cette page), un beau manifeste !"
categories:
- flux
tags:
- offline
---
>Et si les lecteurs avaient accès à cette inestimable attention qui rend la lecture d'un roman si passionnante pendant des heures ? Et si les créateurs pouvaient coupler cela avec la puissance des nouvelles technologies ? Nos téléphones et nos ordinateurs portables sont des outils incroyables pour de nouveaux contenus — si seulement nous pouvions maîtriser notre propre attention.  
[Chris Bolin, Déconnexion](https://chris.bolin.co/offline/)

Une page web lisible uniquement hors connexion (oui vous devez couper votre connexion pour lire le contenu de cette page), un beau manifeste !

Via [Ilan Peer](https://twitter.com/ilan_peer/status/938377409338003458).