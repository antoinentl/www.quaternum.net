---
layout: post
title: "Appel à projets publication numérique"
date: "2017-06-30T10:00:00"
comments: true
published: true
description: "Le Master dans lequel j'interviens ouvre un appel à projets pour des structures qui souhaitent travailler avec des étudiants sur un projet de publication numérique ! L'appel est ouvert jusqu'au 1er septembre 2017."
categories:
- flux
tags:
- publication
---
>Le Master Publication numérique de l’Enssib propose à des maisons d’édition ou à des structures ayant une activité de publication de travailler avec des étudiants autour d’un projet de publication, de document ou de livre numériques. Ouverture des réponses jusqu’au 1er septembre.  
[enssib, Appel à projets livre numérique du Master Publication numérique 2017-2018](http://www.enssib.fr/appel-projets)

Le Master dans lequel j'interviens ouvre un appel à projets pour des structures qui souhaitent travailler avec des étudiants sur un projet de publication numérique ! L'appel est ouvert jusqu'au 1er septembre 2017.