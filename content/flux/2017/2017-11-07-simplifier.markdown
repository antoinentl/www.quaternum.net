---
layout: post
title: "Simplifier"
date: "2017-11-07T11:00:00"
comments: true
published: true
description: "Pourquoi utiliser des technologies dynamiques et des recettes complexes alors que des technologies et des méthodes peuvent faire gagner en performance ?"
categories:
- flux
tags:
- web
---
>Netflix vient d'annoncer leur abandon de React.js sur leurs pages d'interface et ils ont vue les performances de chargement s'améliorer de 50%.  
[Bertrand Keller, Netflix abandonne React.js pour ses landing pages](https://bertrandkeller.info/2017/11/06/netflix-react-generateur-site-statique/)

Pourquoi utiliser des technologies dynamiques et des recettes complexes alors que des technologies et des méthodes peuvent faire gagner en performance ?

À lire également : [Netflix functions without client-side React, and it's a good thing par Jake Archibald](https://jakearchibald.com/2017/netflix-and-react/).