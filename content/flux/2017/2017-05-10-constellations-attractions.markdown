---
layout: post
title: "Constellations - attractions"
date: "2017-05-10T22:00:00"
comments: true
published: true
description: "Le programme de la 65e semaine de culture graphique des Rencontres de Lure est en ligne, et les inscriptions sont ouverte ! Ça se passe à Lurs du 20 au 26 août 2017."
categories: 
- flux
tags:
- design
---
>Une constellation est un tracé primitif qui relie les étoiles pour nous guider et donner sens au monde. De même, dans l’abondance quasi infinie de l’information, le designer graphique repère les grands astres et trace discrètement les sentiers de l’intelligible. Typographes, graphistes, artistes, conçoivent-ils des mondes qui leur ressemblent, ou se fondent-ils dans des univers qui les dépassent ?  
[Les Rencontres internationales de Lure, Constellations – Attractions, liens et tensions graphiques](http://delure.org/les-rencontres/rencontres-precedentes/rencontres-2017)

Le programme de la 65e semaine de culture graphique des Rencontres de Lure est en ligne, et [les inscriptions sont ouvertes](http://delure.org/les-rencontres/rencontres-precedentes/rencontres-2017/inscriptions/inscriptions-2017) ! Ça se passe à Lurs du 20 au 26 août 2017.