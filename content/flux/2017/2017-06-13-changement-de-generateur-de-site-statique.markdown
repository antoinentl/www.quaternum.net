---
layout: post
title: "Changement de générateur de site statique"
date: "2017-06-13T10:00:00"
comments: true
published: true
description: "Sara Soueidan détaille sa migration depuis Jekyll vers Hugo, un changement de générateur de site statique expliqué en détail !"
categories:
- flux
tags:
- web
---
>This is the most effective way to be productive, really: Just do it.  
[Sara Soueidan, Migrating from Jekyll+Github Pages to Hugo+Netlify](https://www.sarasoueidan.com/blog/jekyll-ghpages-to-hugo-netlify/)

Sara Soueidan détaille sa migration depuis Jekyll vers Hugo, un changement de générateur de site statique expliqué en détail !