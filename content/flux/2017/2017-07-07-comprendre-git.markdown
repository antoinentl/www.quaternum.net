---
layout: post
title: "Comprendre Git"
date: "2017-07-07T17:00:00"
comments: true
published: true
description: "Je découvre cet article sur Git de Thibault Jouannic suite à la recherche de Marie, beau billet très bien fichu !"
categories:
- flux
tags:
- outils
---
>Je dois pourtant reconnaître que Git n'est pas forcément l'outil le plus abordable qui soit. Toutes ces commandes bizarres ! Toutes ces options apparemment redondantes ! Cette documentation cryptique ! Et ce worflow de travail, qui nécessite 18 étapes pour pousser un patch sur le serveur. Tel un fier et farouche étalon des steppes sauvages, Git ne se laissera approcher qu'avec cisconspection, et demandera beaucoup de patience avant de s'avouer dompté.  
[Thibault Jouannic, Pour arrêter de galérer avec Git](https://www.miximum.fr/blog/enfin-comprendre-git/)

Je découvre cet article sur Git de Thibault Jouannic suite à la recherche de [Marie](https://twitter.com/kReEsTaL/status/882865146057641985), beau billet très bien fichu !