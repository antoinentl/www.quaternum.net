---
layout: post
title: "Bulletin de santé d’Internet"
date: "2017-01-26T22:00:00"
comments: true
published: true
description: "Mozilla lance une initiative intéressante : Bulletin de santé d’Internet, Éléments utiles et néfastes à notre principale ressource mondiale."
categories: 
- flux
tags:
- internet
slug: bulletin-de-sante-d-internet
---
>Bienvenue sur le site de la nouvelle initiative open source de Mozilla visant à documenter et à analyser l’état de santé d’Internet. En combinant des informations de plusieurs sources, elle rassemble des données relatives à cinq thèmes clés dont elle propose une brève présentation.  
[Mozilla, Comment mesurer l’état de santé d’Internet ?](https://internethealthreport.org/v01/fr/about/)

Mozilla lance une initiative intéressante : "Bulletin de santé d’Internet, Éléments utiles et néfastes à notre principale ressource mondiale."

Via [Clochix](https://clochix.net/).