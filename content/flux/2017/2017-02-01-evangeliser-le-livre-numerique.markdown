---
layout: post
title: "Évangéliser le livre numérique"
date: "2017-02-01T10:00:00"
comments: true
published: true
description: "Jiminy Panoz expose de façon claire et convaincante ce que beaucoup de personnes intéressées et impliquées dans le livre numérique pensent : il faut évangéliser les acteurs du web en ce qui concerne le livre numérique, l'EPUB et les Portable Web Publications."
categories: 
- flux
tags:
- ebook
---
>À un moment, je pense qu’EPUB (ou son successeur) ne pourra pas faire sans évangélistes.  
[Jiminy Panoz, Maintenant, l’évangélisation auprès du web](http://jiminy.chapalpanoz.com/evangelisation/)

Jiminy Panoz expose de façon synthétique et convaincante ce que beaucoup de personnes intéressées et impliquées dans le livre numérique pensent : il faut évangéliser le livre numérique auprès des acteurs du web -- l'EPUB, son "successeur" et les Portable Web Publications.

Une citation bonus, parce que cela me semble aussi être une aberration :

>Quant aux rapports et livres blancs, la très grosse majorité n’est disponible qu’au format PDF — ce qui est pour le moins ironique quand le rapport ou livre blanc a pour sujet le livre numérique.