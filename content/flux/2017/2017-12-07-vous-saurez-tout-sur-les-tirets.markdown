---
layout: post
title: "Vous saurez tout sur les tirets"
date: "2017-12-07T22:00:00"
comments: true
published: true
description: "Toutes les réponses essentielles et très bien expliquées à la question de l'utilisation des tirets ! C'est le retour des 24 jours de web !"
categories:
- flux
tags:
- typographie
---
>Il règne la plus grande des confusions au sujet des tirets et des traits d’union. [...] Sans prétendre à l’exhaustivité ni formuler une version définitive, je propose de partager avec vous mes recherches et conclusions actuelles.  
[Vincent Valentin, Les tirets](https://www.24joursdeweb.fr/2017/les-tirets/)

Toutes les réponses essentielles et très bien expliquées à la question de l'utilisation des tirets ! C'est le retour des [24 jours de web](https://www.24joursdeweb.fr/) !
