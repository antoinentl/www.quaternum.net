---
layout: post
title: "Un guide pour auditer en RGAA 3"
date: "2017-07-06T10:00:00"
comments: true
published: true
description: "Un guide pour faciliter la réalisation d'audits RGAA."
categories:
- flux
tags:
- accessibilité
---
>Ce guide s'adresse aux personnes qui effectuent des audits RGAA : référent accessibilité dans une administration, développeur en charge des audits de suivi ou encore auditeur externe à l'administration.  
Ce guide est destiné à des utilisateurs avertis ayant déjà une connaissance du RGAA 3. Il ne s'agit pas d'un cours à propos de l'utilisation du référentiel général d'accessibilité pour les administrations ou de l'application du RGAA sur un site web.  
Ce document est purement informatif, il vous permet d'appréhender les principales étapes de la mise en place d'un audit à la présentation de ses résultats.  
[DISIC, Guide de l'auditeur RGAA 3](https://disic.github.io/guide-auditeur/)

Un guide pour faciliter la réalisation d'audits RGAA. Le code source est disponible [sur GitHub](https://github.com/DISIC/guide-auditeur/).