---
layout: post
title: "Des livres en HTML"
date: "2017-03-07T08:00:00"
comments: true
published: true
description: "Fabriquer des livres imprimés et numériques avec HTML + CSS, plutôt qu'avec InDesign ou XML ? C'est ce qui a été mis en place chez Hachette Book Group, et ce que nous raconte Dave Cramer."
categories: 
- flux
tags:
- livre
---
>We believe that single-source content in HTML, with styling expressed with CSS, is the best way to make our books.  
[Dave Cramer, Beyond XML: Making Books with HTML](https://www.xml.com/articles/2017/02/20/beyond-xml-making-books-html/#appendix)

Fabriquer des livres imprimés et numériques avec HTML + CSS, plutôt qu'avec InDesign ou XML ? C'est ce qui a été mis en place chez Hachette Book Group, et ce que nous raconte Dave Cramer.

>In InDesign, the content and the presentation are inseparable—two different presentations of the same content means having two independent files. Even embedding XML in InDesign doesn’t change anything; you still have to maintain that content in multiple files. We needed something better.