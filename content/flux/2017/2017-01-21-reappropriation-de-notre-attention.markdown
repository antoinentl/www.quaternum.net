---
layout: post
title: "Réappropriation de notre attention"
date: "2017-01-21T23:50:00"
comments: true
published: true
description: "Dans un long papier Craig Mod explique pourquoi et comment il a voulu se réapproprier son attention, dans un monde hyper-connecté et surchargé en information : à lire absolument."
categories: 
- flux
tags:
- privacy
---
>Maybe I lost my attention because I’m weak, lonely, pathetic. Maybe everyone else has total control; they can resist all the information spun by algorithms—all the delicious dopamine hits in the form of red circles. *Bing!* Maybe it’s just me.  
But … I want my attention back.  
[Craig Mod, How I Got My Attention Back](https://backchannel.com/how-i-got-my-attention-back-c7fc9297d347)

Dans un long papier Craig Mod explique pourquoi et comment il a voulu se réapproprier son attention, dans un monde hyper-connecté et surchargé en information : à lire absolument.

>There are a thousand beautiful ways to start the day that don’t begin with looking at your phone. And yet so few of us choose to do so.