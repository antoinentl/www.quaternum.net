---
layout: post
title: "Les perspectives du web en 2017"
date: "2017-01-06T22:00:00"
comments: true
published: true
description: "De la résilience aux Progressive web apps, en passant par l'accessibilité ou la performance, les experts d'O'Reilly évoquent les sujets de 2017, dans la continuité de 2016."
categories: 
- flux
tags:
- web
---
>Building a better web. Sounds great, doesn't it? But what do we need to do to make it so? And what do we mean by better? Isn't it great as is? This is what we are going to try to unravel and present to you this coming year.  
[O'Reilly, Building a better web in 2017](https://www.oreilly.com/ideas/building-a-better-web-in-2017)

De la résilience aux Progressive web apps, en passant par l'accessibilité ou la performance, les *experts* d'O'Reilly évoquent les sujets de 2017, dans la continuité de 2016.