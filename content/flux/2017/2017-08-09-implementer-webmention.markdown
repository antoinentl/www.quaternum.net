---
layout: post
title: "Implémenter Webmention"
date: "2017-08-09T09:00:00"
comments: true
published: true
description: "Drew McLellan décrit le fonctionnement du protocole Webmention, permettant de gérer les mentions entre site afin de se passer de commentaires, de façon décentralisée."
categories:
- flux
tags:
- web
---
>Webmention is a W3C Recommendation that solves a big part of this. It describes a system for one site to notify another when it links to it. It’s similar in concept to Pingback for those who remember that, just with all the lessons learned from Pingback informing the design.  
[Drew McLellan, Implementing Webmentions](https://allinthehead.com/retro/378/implementing-webmentions)

Drew McLellan décrit le fonctionnement du protocole Webmention, permettant de gérer les mentions entre site afin de se passer de commentaires, de façon décentralisée.