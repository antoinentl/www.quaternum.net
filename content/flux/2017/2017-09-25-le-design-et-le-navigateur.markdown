---
layout: post
title: "Le design et le navigateur"
date: "2017-09-25T17:00:00"
comments: true
published: true
description: "Utiliser le web et ses outils pour expérimenter le design autrement, ou comment réunir le fond et la forme. À noter que for / with / in est également un outil qui permet de composer et de produire un livre sur le modèle HTML2print."
categories:
- flux
tags:
- design
---
>This collection of formal experiments and conversations explore the role of the browser as another possible tool for designers. The website and print-on-demand pocket-sized book are made from the same content, the book treated as one size within a multi-width design logic.  
[for / with / in](http://htmloutput.risd.gd/about-intro/)

Utiliser le web et ses outils pour expérimenter le design autrement, ou comment réunir le fond et la forme. À noter que for / with / in est également un outil qui permet de composer et de produire un livre sur le modèle HTML2print.

Via [PrePostPrint](https://prepostprint.org/).