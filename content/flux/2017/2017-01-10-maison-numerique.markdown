---
layout: post
title: "Maison numérique"
date: "2017-01-10T22:00:00"
comments: true
published: true
description: "Rachel Andrew pose la question de nos maisons numériques que nous pouvons créer et maintenir, indépendamment des plateformes de blogging ou des réseaux sociaux. Dans le même esprit que mon billet Interroger nos pratiques de publication en ligne."
categories: 
- flux
tags:
- publication
---
>As we move our code to CodePen, our writing to Medium, our photographs to Instagram we don’t just run the risk of losing that content and the associated metadata if those services vanish. We also lose our own place to experiment and add personality to that content, in the context of our own home on the web.  
[Rachel Andrew, It's more than just the words](https://www.rachelandrew.co.uk/archives/2017/01/05/its-more-than-just-the-words/)

Rachel Andrew pose la question de nos *maisons numériques* que nous pouvons créer et maintenir, indépendamment des plateformes de blogging ou des réseaux sociaux. Dans le même esprit que mon billet [Interroger nos pratiques de publication en ligne](/2017/01/06/interroger-nos-pratiques-de-publication-en-ligne/).

Je partage l'interrogation de Clochix : "Mais être locataire de sa propre maison Web a-t-il un sens pour qui n'est pas Webeu⋅se ?"

Via [Clochix](https://twitter.com/clochix/status/817866086691508225) via [Karl](http://www.la-grange.net/2017/01/06/assiette).