---
layout: post
title: "Langage non genré accessible"
date: "2017-03-31T12:30:00"
comments: true
published: true
description: "Très bonne synthèse des possibilités offertes pour utiliser, à l'écrit, un langage non genré, et quelles solutions sont les plus accessibles."
categories: 
- flux
tags:
- accessibilité
---
>Afin que tout le monde comprenne le texte, il vaudrait donc mieux tout écrire, comme « lectrices et lecteurs ». Si ce n’est pas faisable, il est possible d’utiliser le point médian, sans en abuser.  
[Sylvie Duchateau, Langage non genré et accessibilité font-ils bon ménage ?](https://access42.net/Langage-non-genre-accessibilite)

Très bonne synthèse des possibilités offertes pour utiliser, à l'écrit, un langage non genré, et quelles solutions sont les plus accessibles.