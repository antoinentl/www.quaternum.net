---
layout: post
title: "Transmettre c'est passer au sens physique du terme"
date: "2017-06-28T10:00:00"
comments: true
published: true
description: "Belle image de l'enseignement et de la transmission dans ce réjouissant feuilleton de France Culture."
categories:
- flux
tags:
- métier
slug: transmettre-c-est-passer-au-sens-physique-du-terme
---
>Je voudrais des idées ovales comme un ballon de rugby. C'est une métaphore qui vaut ce qu'elle vaut, sans doute pas grand chose. Mais j'aime cette idée, l'idée que transmettre c'est passer au sens physique et sportif du terme. Allez hop, attrape ! Et ne garde pas ça pour toi ! Passe à ton tour !  
[...]  
C'est à chaque fois un émerveillement pour moi de constater à quel point la pensée engendre la pensée. L'effet surgénérateur du langage et des idées. La transmission c'est une autre paire de manches.  
[L’apocalypse est notre chance de Sylvie Coquart-Morel et Sophie Maurer (1/15), France Culture](https://www.franceculture.fr/emissions/fictions-le-feuilleton/lapocalypse-est-notre-chance-de-sylvie-coquart-morel-et-sophie-0)

Belle image de l'enseignement et de la transmission dans ce réjouissant feuilleton de France Culture, *L’apocalypse est notre chance*.