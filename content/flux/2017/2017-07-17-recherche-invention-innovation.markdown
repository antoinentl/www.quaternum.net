---
layout: post
title: "Recherche, invention, innovation"
date: "2017-07-17T17:00:00"
comments: true
published: true
description: "Huit cours de Didier Roux au Collège de France, au croisement de la recherche, de l'invention, de la découverte et de l'innovation. Une bonne façon de comprendre des innovations récentes en lien avec des découvertes scientifiques parfois assez anciennes."
categories:
- flux
tags:
- web
---
>Quelle convergence entre la machine à calculer de Pascal et les métiers à tisser de Jacquard? Pourquoi l’intérêt vers la recherche fondamentale après les chocs pétroliers? Comment marchent les micro-émulsions? Didier Roux interroge les interconnexions entre découvertes, inventions et innovations.  
[France Culture, Découverte fondamentale, invention technologique, innovation : un voyage scientifique](https://www.franceculture.fr/emissions/les-cours-du-college-de-france/decouverte-fondamentale-invention-technologique-innovation)

Huit cours de Didier Roux au Collège de France, au croisement de la recherche, de l'invention, de la découverte et de l'innovation. Une bonne façon de comprendre des innovations récentes en lien avec des découvertes scientifiques parfois assez anciennes.