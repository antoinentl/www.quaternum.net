---
layout: post
title: "Liquidité du livre numérique"
date: "2017-05-16T22:00:00"
comments: true
published: true
description: "Toute personne ayant eu à convaincre de l'intérêt de la liquidité du format EPUB sera heureux de trouver ici réunis un nombre important d'arguments, d'exemples, mais aussi de contraintes et d'obstacles. Jiminy Panoz parle de redistribuabilité, il s'agit bien du caractère adaptatif du format EPUB, en fonction de la dimension de l'écran, mais aussi d'autres paramètres."
categories: 
- flux
tags:
- ebook
---
>L’objectif de cette page est par conséquent de donner les clés du livre numérique redistribuable, aussi connu sous le nom d’ebook reflowable text.  
[Jiminy Panoz, L'ebook redistribuable](https://jaypanoz.github.io/reflow/)

Toute personne ayant eu à convaincre de l'intérêt de la *liquidité* du format EPUB sera heureux de trouver ici réunis un nombre important d'arguments, d'exemples, mais aussi de contraintes et d'obstacles. Jiminy Panoz parle de *redistribuabilité*, il s'agit bien du caractère adaptatif du format EPUB, en fonction de la dimension de l'écran, mais aussi d'autres paramètres.

>Le livre numérique, c’est du web