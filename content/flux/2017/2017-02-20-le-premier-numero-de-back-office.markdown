---
layout: post
title: "Le premier numéro de Back Office : faire avec"
date: "2017-02-20T10:00:00"
comments: true
published: true
description: "Le premier numéro de la revue Back Office est sorti, il est disponible en version papier et numérique. Une très belle initiative d'Anthony, Élise et Kévin !"
categories: 
- flux
tags:
- design
slug: le-premier-numero-de-back-office
---
>Le premier numéro de Back Office interroge les notions d’outil, d’instrument et d’appareil, dans le contexte du design.  
[Back Office, Faire avec](http://www.revue-backoffice.com/numeros/01-faire-avec)

Le premier numéro de la revue Back Office est sorti, il est disponible en version papier et numérique. Une très belle initiative d'Anthony, Élise et Kévin, co-éditée par B42 !

>Conserver dans le numérique ce que la main et l’œil ont de spécifique, construire ses propres systèmes et architectures techniques, ouvrir les boîtes noires des programmes pour travailler ensemble, revisiter des projets oubliés par l’histoire pour explorer d’autres directions, autant de façons de faire « avec » le numérique que nous vous proposons d’explorer.