---
layout: post
title: "Repenser le livre numérique"
date: "2017-03-06T08:00:00"
comments: true
published: true
description: "Boris Anthony et Hugh McGuire, les fondateurs de Rebus Foundation, échangent autour des concepts de livre, de livre numérique et d'open textbooks : expériences de lecture, constitution du savoir, construction de soi grâce aux livres, déception liée au livre numérique tel qu'il existe aujourd'hui, modèle du livre numérique à réinventer avec le web, etc. Un entretien passionnant !"
categories: 
- flux
tags:
- livre
---
>Paper is limited as a physical medium; it’s not connected to the network. Ebooks are limited by vendor silos, Digital Rights Management (DRM) lock-in, and what Amazon or Apple will let you do with them. Yet books are a central set of nodes in how I define myself, and I would like to explore my relationship with books using digital tools. But I can’t.  
[Boris Anthony et Hugh McGuire, A Momentary Suspension of Disbelief](https://borisanthony.net/writing/a-momentary-suspension-of-disbelief/)

Boris Anthony et Hugh McGuire, les fondateurs de [Rebus Foundation](https://rebus.foundation/), échangent autour des concepts de livre, de livre numérique et d'*open textbooks* : expériences de lecture, constitution du savoir, construction de soi, déception liée au livre numérique tel qu'il existe aujourd'hui, modèle du livre numérique à réinventer avec le web, etc. Un entretien passionnant qui introduit les travaux de recherche de Rebus Foundation !

>The synthesis has to happen in the reader’s mind, and figuring out the tools to do that effectively and delightfully is an exciting challenge.
Boris Anthony

>If everyone’s interactions with their copies of a book feed back up to their source—their creator and publisher—they can begin to engage a distributed conversation and exploration.  
Hugh McGuire

>It’s simply not good enough to just go and build another reader app, another ebook store or stuff like that. A huge part of the work that needs to be done is adding the necessary infrastructure to the existing web to make books first-class citizens on the network, and beyond that: figuring out what equitable value flows and business models can bring all this to life.  
Boris Anthony