---
layout: post
title: "Une documentation qui rassemble"
date: "2017-10-18T22:00:00"
comments: true
published: true
description: "Voilà une grande nouvelle : une majorité d'éditeurs de navigateurs web se rassemblent autour de la documentation MDN initiée par Mozilla !"
categories:
- flux
tags:
- web
---
>Today, Mozilla is announcing a plan that grows collaboration with Microsoft, Google, and other industry leaders on MDN Web Docs. The goal is to consolidate information about web development for multiple browsers – not just Firefox.  
[Ali Spivak, Mozilla brings Microsoft, Google, the W3C, Samsung together to create cross-browser documentation on MDN](https://blog.mozilla.org/blog/2017/10/18/mozilla-brings-microsoft-google-w3c-samsung-together-create-cross-browser-documentation-mdn/)

Voilà une grande nouvelle : une majorité d'éditeurs de navigateurs web se rassemblent autour de la documentation MDN initiée par Mozilla !