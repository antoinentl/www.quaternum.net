---
layout: post
title: "Le livre numérique vu par les médias"
date: "2017-09-26T17:00:00"
comments: true
published: true
description: "Forcément lorsque les grands médias jouent les prospectivistes et qu'ils se trompent, il faut bien qu'ils expriment leur propre déception. Si le livre numérique est une révolution ce n'est pas celle des chiffres, mais plutôt la partie visible d'un plus grand bouleversement au sein de la chaîne du livre (et notamment dans les moyens et méthodes de conception et de production)."
categories:
- flux
tags:
- ebook
---
>Histoire d'une révolution qui n'aura pas eu lieu.  
[Mathilde Serrell, Le livre numérique a-t-il encore un avenir?](https://www.franceculture.fr/emissions/le-billet-culturel/le-livre-numerique-t-il-encore-un-avenir)

Forcément lorsque les *grands médias* jouent les prospectivistes et qu'ils se trompent, il faut bien qu'ils expriment leur propre déception. Si le livre numérique est une révolution ce n'est pas celle des chiffres, mais plutôt la partie visible d'un plus grand bouleversement au sein de la chaîne du livre (et notamment dans les moyens et méthodes de conception et de production).