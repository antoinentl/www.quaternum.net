---
layout: post
title: "Écrire un livre en 2017"
date: "2017-02-09T19:00:00"
comments: true
published: true
description: "Thomas passant par Grenoble, je me suis dit que c'était l'occasion idéale pour présenter sa démarche d'écriture de son livre sur Node.js, et La Coop est le lieu parfait pour échanger. Cette rencontre aurait pu s'intituler Écrire un livre en 2017."
categories: 
- flux
tags:
- publication
---
>[Thomas Parisot](https://oncletom.io/), développeur web basé à Londres, viendra présenter son expérience d’écriture d’un livre à paraître aux éditions Eyrolles. Ce projet est réalisé en résidence itinérante et collaborative.  
[Écrire à plusieurs avec GitHub?! – Rencontre avec Thomas Parisot](http://www.la-coop.net/evenement/ecrire-avec-github/)

[Thomas](https://oncletom.io/) passant par Grenoble, je me suis dit que c'était l'occasion idéale pour présenter sa démarche d'écriture de [son livre sur Node.js,](https://oncletom.io/node.js) et La Coop est le lieu parfait pour échanger. Cette rencontre aurait pu s'intituler "Écrire un livre en 2017".

Cela se passera jeudi 23 février 2017 à La Coop, 31 rue Gustave Eiffel à Grenoble, l'entrée est libre, [sur inscription](http://www.eventbrite.fr/e/billets-ecrire-avec-github-31962957065?ref=ebtn).