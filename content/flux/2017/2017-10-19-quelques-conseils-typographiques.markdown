---
layout: post
title: "Quelques conseils typographiques"
date: "2017-10-19T18:00:00"
comments: true
published: true
description: "Quelques diapositives pour un discours synthétique sur la typographie."
categories:
- flux
tags:
- typographie
---
>Typography is the technique of arranging type for effective communication and a bit of delight.  
[Pierrick Calvez, A Five Minutes Guide to Better Typography](http://pierrickcalvez.com/journal/a-five-minutes-guide-to-better-typography)

Quelques *diapositives* pour un discours synthétique sur la typographie.