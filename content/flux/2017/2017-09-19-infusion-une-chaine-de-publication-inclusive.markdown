---
layout: post
title: "Infusion : une chaîne de publication inclusive"
date: "2017-09-19T17:00:00"
comments: true
published: true
description: "The Paciello Group met à disposition un outil pour construire de la documentation inclusive, basé notamment sur Hugo (le générateur de site statique à la mode). Infusion est un bel exemple de chaîne de publication inspirée du web dans le domaine de la documentation."
categories:
- flux
tags:
- publication
---
>To manage pattern libraries effectively, we needed a system for building and hosting them. This is why we built Infusion, and we’re happy to share it as a free and open tool with you.  
[Heydon, Infusion: An Inclusive Documentation Builder](https://developer.paciellogroup.com/blog/2017/09/infusion-an-inclusive-documentation-builder/)

The Paciello Group met à disposition un outil pour construire de la documentation inclusive, basé notamment sur Hugo (le générateur de site statique à la mode). Infusion est un bel exemple de chaîne de publication inspirée du web dans le domaine de la documentation.