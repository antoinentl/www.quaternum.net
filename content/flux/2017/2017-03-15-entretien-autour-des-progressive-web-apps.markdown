---
layout: post
title: "Entretien autour des Progressive Web Apps"
date: "2017-03-15T22:00:00"
comments: true
published: true
description: "Jeremy Keith a répondu à quelques questions posées par Colin van Eenige, à propos des Progressive Web Apps. Le point de vue Jeremy Keith est toujours très intéressant, et en rapport avec un web ouvert."
categories: 
- flux
tags:
- design
---
>Embracing minimalism on different aspects of your life has plenty of benefits, like peace of mind, knowing where things are, and being low-maintenance in general.  
[Jeremy Keith, Progressive Web App questions](https://adactio.com/journal/12015)

Jeremy Keith a répondu à quelques questions posées par Colin van Eenige, à propos des Progressive Web Apps. Le point de vue Jeremy Keith est toujours très intéressant, et en rapport avec un web ouvert.

>So there you go—I’m very excited about the capabilities of these technologies, but very worried about how they’re being “sold”. I’m particularly nervous that in the rush to emulate native apps, we end up losing the very thing that makes the web so powerful: [URLs](https://adactio.com/journal/10708).