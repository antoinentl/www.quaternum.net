---
layout: post
title: "Trucs et astuces pour les CSS de livres numériques"
date: "2017-01-04T22:00:00"
comments: true
published: true
description: "Après les checklists sur la performance et le design de livre numérique, voici une liste de trucs pour construire au mieux une feuille CSS pour un EPUB. Un outil pratique, qui par ailleurs s'installe sur votre téléphone puisqu'il d'une web app."
categories: 
- flux
tags:
- outils
---
>A collection of CSS snippets to do progressive enhancement and achieve better typography, layout and UX in eBooks.  
[Blitz eBook Tricks](https://friendsofepub.github.io/eBookTricks/)

Après les checklists sur [la performance](https://friendsofepub.github.io/eBookPerfChecklist/) et [le design](https://friendsofepub.github.io/eBookDesignChecklist/) de livre numérique, voici une liste de *trucs* pour construire au mieux une feuille CSS pour un EPUB. Un outil pratique, qui par ailleurs s'installe sur votre téléphone puisqu'il d'une *web app*.