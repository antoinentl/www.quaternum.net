---
layout: post
title: "La technique et ses usages"
date: "2017-12-25T22:00:00"
comments: true
published: true
description: "Beau retour d'expérience sur le fait que les langages de balisage léger, les générateurs de site statique ou les outils d'intégration continue peuvent être utilisés par d'autres personnes que des informaticiens ou des développeurs. Mais attention, Marie-Stéphanie est malgré tout une spécialiste – de l'histoire de l'art –, et une technicienne – car on peut dire que les documentalistes sont des techniciens."
categories:
- flux
tags:
- outils
---
>Marie-Stéphanie a fait des études d’histoire de l’art et de documentaliste. Pourtant, elle édite des fichiers Markdown en respectant la structure du projet, fait des commits sur Git avec un message permettant de fermer le ticket JIRA correspondant, push sur GitHub, attend le rapport de Jenkins qui contiendra la log Jekyll et lui indiquera si le site de pré-prod est à jour.  
>[...]  
>Pour elle, l’informatique est un outil, un moyen.  
[Cédric Temple, Marie-Stéphanie, Markdown, GIT, Jekyll et Jenkins](https://linuxfr.org/users/ctemple/journaux/marie-stephanie-markdown-git-jekyll-et-jenkins)

Beau retour d'expérience sur le fait que les langages de balisage léger, les générateurs de site statique ou les outils d'intégration continue peuvent être utilisés par d'autres personnes que des informaticiens ou des développeurs. Mais attention, Marie-Stéphanie est malgré tout une spécialiste – de l'histoire de l'art –, et une technicienne – car on peut dire que les documentalistes sont des techniciens.

Via [iGor](https://herds.eu/notice/2937708).
