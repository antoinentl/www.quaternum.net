---
layout: post
title: "Quitter Facebook"
date: "2017-01-20T23:50:00"
comments: true
published: true
description: "Pia présente très clairement les raisons de son départ de Facebook, c'est bien dit, et cela fait réfléchir à nos usages d'autres réseaux sociaux ou plateformes."
categories: 
- flux
tags:
- privacy
---
>Je ne veux plus participer à un mouvement de centralisation de la vie numérique sur quelques mal-nommées *plateformes* [...].  
[PiaP, Quitter Facebook](https://medium.com/france/quitter-facebook-d305a50da455#.o10eca352)

Pia présente très clairement les raisons de son départ de Facebook, c'est bien dit, et cela fait réfléchir à nos usages d'autres réseaux sociaux ou plateformes.