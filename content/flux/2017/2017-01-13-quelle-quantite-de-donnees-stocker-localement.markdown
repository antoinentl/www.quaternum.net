---
layout: post
title: "Quelle quantité de données stocker localement ?"
date: "2017-01-13T22:00:00"
comments: true
published: true
description: "Désormais un site web peut être utilisé hors ligne, après une mise en cache des données (fichiers HTML, CSS et JavaScript, mais aussi images et fontes), et grâce aux Service Workers -- voir mon billet Quatre bonnes raisons de faire du web hors connexion à ce sujet. Nicolas Hoizey pose une question que personne n'osait poser : quelle limite fixer pour la quantité de données stockées localement ?"
categories: 
- flux
tags:
- offline
---
>For a small site/app that takes 2 or 3 Mb, I can accept to download everything, because the convenience to have all this available while offline can be great. But I think 16 Mb is really to much.  
[Nicolas Hoizey, How much data should my Service Worker put upfront in the offline cache?](https://nicolas-hoizey.com/2017/01/how-much-data-should-my-service-worker-put-upfront-in-the-offline-cache.html)

Désormais un site web peut être utilisé hors ligne, après une *mise en cache* des données (fichiers HTML, CSS et JavaScript, mais aussi images et fontes), et grâce aux Service Workers -- voir mon billet [Quatre bonnes raisons de faire du web hors connexion](/2016/06/23/quatre-bonnes-raisons-de-faire-du-web-hors-connexion/) à ce sujet. [Nicolas Hoizey](https://nicolas-hoizey.com/) pose une question que personne n'osait poser : quelle limite fixer pour la quantité de données stockées localement ? L'exemple qu'il prend est le livre web de Jeremy Keith : [Resilient Web Design](https://resilientwebdesign.com/). Question pertinente qui concerne aussi les domaines de l'accessibilité et de [la qualité web](http://qualite-web-lelivre.com/).