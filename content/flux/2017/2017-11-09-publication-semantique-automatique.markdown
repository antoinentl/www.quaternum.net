---
layout: post
title: "Publication sémantique automatique"
date: "2017-11-09T22:00:00"
comments: true
published: true
description: "Comment générer automatiquement une structure sémantique fine pour des articles de recherche afin de faciliter le travail des auteurs ? Voici la proposition de Silvio Peroni pour répondre à cette question (note : je n'ai pas entièrement lu cet article)."
categories:
- flux
tags:
- publication
---
>In this article, I propose a generic approach called compositional and iterative semantic enhancement (CISE) that enables the automatic enhancement of scholarly papers with additional semantic annotations in a way that is independent of the markup used for storing scholarly articles and the natural language used for writing their content.  
[Silvio Peroni, Automating Semantic Publishing](https://essepuntato.github.io/papers/cise-datascience2017.html)

Comment générer automatiquement une structure sémantique fine pour des articles de recherche afin de faciliter le travail des auteurs ? Voici la proposition de Silvio Peroni pour répondre à cette question (note : je n'ai pas entièrement lu cet article).