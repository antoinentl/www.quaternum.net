---
layout: post
title: "Twitter Lite"
date: "2017-04-19T10:00:00"
comments: true
published: true
description: "Twitter a mis en place une Progressive Web App et Nicolas Gallagher donne quelques explications sur la conception et l'architecture. À noter que, pour le moment, il y a bien moins de publicité et l'interface est tout simplement plus réussie que la version desktop ou même l'application... Comme quoi le design n'est pas qu'une question de front, mais également d'architecture technique."
categories: 
- flux
tags:
- offline
---
>We’re excited to introduce you to Twitter Lite, a Progressive Web App that is available at mobile.twitter.com. Twitter Lite is fast and responsive, uses less data, takes up less storage space, and supports push notifications and offline use in modern browsers. The web is becoming a platform for lightweight apps that can be accessed on-demand, installed without friction, and incrementally updated. Over the last year we’ve adopted new, open web APIs and significantly improved the performance and user experience.  
[Nicolas Gallagher, How we built Twitter Lite](https://blog.twitter.com/2017/how-we-built-twitter-lite)

Twitter a mis en place une Progressive Web App et Nicolas Gallagher donne quelques explications sur la conception et l'architecture. À noter que, pour le moment, il y a bien moins de publicité et l'interface est tout simplement plus réussie que la version *desktop* ou même l'application... Comme quoi le design n'est pas qu'une question de *front*, mais également d'architecture technique.