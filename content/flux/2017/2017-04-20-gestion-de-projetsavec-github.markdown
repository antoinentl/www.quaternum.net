---
layout: post
title: "Gestion de projet avec GitHub"
date: "2017-04-20T22:00:00"
comments: true
published: true
description: "Anselm, Tobias, et Holger utilisent GitHub pour travailler à plusieurs, Anselm détaille leurs pratiques de la plateforme -- gestion des issues, communication, etc. À noter que la même chose est faisable avec GitLab."
categories: 
- flux
tags:
- outils
slug: gestion-de-projetsavec-github
---
>During our time working on Colloq we realised that we don’t need a lot of tools to run our project. Instead, we try to focus on our most important tasks. We try to understand our real needs, and we tweak the available options we have to match our needs. We lower the complexity in our product by reducing the tools we use. We believe that this is one of the reasons we currently work very efficiently on our product.   
[Anselm, The Tools We Use To Stay Afloat](https://colloq.io/blog/the-tools-we-use-to-stay-afloat)

Anselm, Tobias, et Holger utilisent GitHub pour travailler à plusieurs, Anselm détaille leurs pratiques de la plateforme -- gestion des *issues*, communication, etc. À noter que la même chose est faisable avec GitLab.