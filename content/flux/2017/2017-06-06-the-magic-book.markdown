---
layout: post
title: "The Magic Book"
date: "2017-06-06T22:00:00"
comments: true
published: true
description: "The Magic Book Project est une solution pour produire différents formats de livre à partir d'une même source en texte brut."
categories:
- flux
tags:
- publication
---
>The Magic Book Project is an open source project funded by New York University's Interactive Telecommunications Program. It aims to be the best free tool for creating print and digital books from a single source.  
[The Magic Book Project](https://github.com/magicbookproject/magicbook)

The Magic Book Project est une solution pour produire différents formats de livre à partir d'une même source en texte brut.