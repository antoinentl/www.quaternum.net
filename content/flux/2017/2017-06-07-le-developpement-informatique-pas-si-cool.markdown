---
layout: post
title: "Le développement informatique pas si cool"
date: "2017-06-07T22:00:00"
comments: true
published: true
description: "Les chroniques de Xavier de La Porte sont toujours d'aussi bonne qualité, celle-ci est particulièrement stimulante !"
categories:
- flux
tags:
- web
---
>J’aime assez l’idée du développeur en scribe à l’envers : une élite qui déguise son pouvoir, non sous le sérieux de sa mission, mais sous sa fausse légèreté. Et voilà que tout à coup, la coolitude de la Silicon Valley prend un tout autre sens. Rendre son sérieux à cette activité est aussi un moyen de lutter contre les abus du pouvoir qu’elle s’octroie. Voilà qui est aussi politique.  
[Xavier de La Porte, Coder, ce n'est ni facile, ni marrant](https://www.franceculture.fr/emissions/la-vie-numerique/coder-ce-nest-ni-facile-ni-marrant)

Les chroniques de Xavier de La Porte sont toujours d'aussi bonne qualité, celle-ci est particulièrement stimulante !