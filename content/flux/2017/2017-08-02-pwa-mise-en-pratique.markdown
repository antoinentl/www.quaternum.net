---
layout: post
title: "PWA : mise en pratique"
date: "2017-08-02T17:00:00"
comments: true
published: true
description: "Un article -- encore un -- sur les Progressive Web Applications (PWA), mais celui-ci a le mérite d'être concis et clair pour une mise en pratique rapide, partant du fait qu'une PWA n'est pas forcément une application complexe."
categories:
- flux
tags:
- web
---
>Turning a basic website into a PWA is not that hard and has a lot of real benefits, so I want to take a look at the three main steps necessary to achieve just that.  
[Max Böck, How to turn your website into a PWA](https://mxb.at/blog/how-to-turn-your-website-into-a-pwa/)

Un article -- encore un -- sur les Progressive Web Applications (PWA), mais celui-ci a le mérite d'être concis et clair pour une mise en pratique rapide, partant du fait qu'une PWA n'est pas forcément une application complexe.