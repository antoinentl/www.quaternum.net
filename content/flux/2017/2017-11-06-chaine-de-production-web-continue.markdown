---
layout: post
title: "Chaîne de production web continue"
date: "2017-11-06T11:00:00"
comments: true
published: true
description: "Les designers de Airbnb ont mis au point une chaîne de production web réellement continue, il est donc possible de passer des maquettes (sous Sketch qui utilise désormais un format standardisé et ouvert) au code, et inversement."
categories:
- flux
tags:
- publication
---
>Today, we’re excited to share a tool we built to help bridge the gap between designers and engineers working on design systems at scale. React-sketchapp is an open-source library that allows you to write React components that render to Sketch documents.  
[Jon Gold, Painting with Code](https://airbnb.design/painting-with-code/)

Les designers de Airbnb ont mis au point une chaîne de production web réellement continue, il est donc possible de passer des maquettes (sous Sketch qui utilise désormais un format standardisé et ouvert) au code, et inversement.

Via [Baptiste](https://twitter.com/Saint_loup/status/927112999902597120).