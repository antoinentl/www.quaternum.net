---
layout: post
title: "Un entretien autour d'Hugo"
date: "2017-10-20T10:00:00"
comments: true
published: true
description: "Frank Taillandier s'entretient avec le principal développeut d'Hugo, le générateur de site statique à la mode, et c'est très instructif !"
categories:
- flux
tags:
- publication
slug: un-entretien-autour-d-hugo
---
>Comme Gutenberg en son temps, Hugo est un générateur de sites web pour de la documentation, des livres, des journaux, des magazines, des blogs, etc. C’est manifeste quand vous voyez les dernières fonctionnalités ajoutées comme les sections imbriquées et les contenus relatifs : structurez bien votre contenu et trouvez-le facilement.  
[Frank Taillandier, Entretien avec Bjørn Erik Pedersen, le développeur principal d'Hugo](https://jamstatic.fr/2017/10/03/interview-hugo-lead-developer/)

Frank Taillandier s'entretient avec le principal développeut d'Hugo, le générateur de site statique à la mode, et c'est très instructif !