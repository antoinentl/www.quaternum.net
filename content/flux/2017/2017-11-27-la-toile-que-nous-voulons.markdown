---
layout: post
title: "La toile que nous voulons"
date: "2017-11-27T22:00:00"
comments: true
published: true
description: "Louis Wiart présente le livre La toile que nous voulons, ouvrage sous la direction de Bernard Stiegler et publié aux éditions Fyp."
categories:
- flux
tags:
- livre
---
>Pourquoi est-il urgent de réinventer le web ? Telle est la question à laquelle une quinzaine d’intellectuels tentent de répondre.  
[...]  
Dressant le constat de technologies numériques évoluant dans un sens globalement défavorable, *La toile que nous voulons* esquisse toutefois quelques pistes de changements.  
[Louis Wiart, Reprendre le pouvoir sur le web](http://www.inaglobal.fr/numerique/note-de-lecture/sous-la-direction-de-bernard-stiegler/la-toile-que-nous-voulons/reprendre-)

Louis Wiart présente le livre *La toile que nous voulons*, ouvrage sous la direction de Bernard Stiegler et publié aux éditions Fyp.