---
layout: post
title: "Les PWAs ne sont pas la solution unique"
date: "2017-07-10T17:00:00"
comments: true
published: true
description: "Les Progressive Web Applications ne sont pas forcément la solution, les applications natives ont encore leur intérêt, tout dépend de la situation, du budget, des contraintes, de la commande initiale, etc. Cet article dénote par rapport au discours quelque peu hétérogène du milieu du web ouvert, et ça fait du bien."
categories:
- flux
tags:
- web
---
>Whether you have doubts or not, get advice from people who have experience, who are knowledgeable and independent of the technologies employed.  
[Sarah Marchand et Boris Schapira, (Web) Apps: There is no silver bullet](https://blog.clever-age.com/en/2017/07/07/web-apps-there-is-no-silver-bullet/)

Les Progressive Web Applications ne sont pas forcément *la* solution, les applications natives ont encore leur intérêt, tout dépend de la situation, du budget, des contraintes, de la commande initiale, etc. Cet article dénote par rapport au discours quelque peu homogène du milieu du web ouvert, et ça fait du bien.