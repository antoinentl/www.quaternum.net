---
layout: post
title: "Confiance"
date: "2017-12-20T23:00:00"
comments: true
published: true
description: "Quand le marketing fait passer la promotion avant tout, bafouant l'un des principes les plus importants de Mozilla et de Firefox : la confiance. Pour fait la publicité de la série Mr. Robot Firefox a installé une extension sans demander l'avis aux utilisateurs. Heureusement Mozilla a fait marche arrière rapidement, mais les équipes et les utilisateurs ne vont pas oublier cette erreur..."
categories:
- flux
tags:
- privacy
---
>But still, if Mozilla is positioning itself as the privacy-conscious alternative to Google, Microsoft and others, then installing an extension without asking users first doesn’t feel right at all.  
[Frederic Lardinois, Mozilla’s Mr. Robot promo backfires after it installs a Firefox extension without permission](https://techcrunch.com/2017/12/15/mozillas-mr-robot-promo-backfires-after-it-installs-firefox-extension-without-permission/)

Quand le marketing fait passer la promotion avant tout, bafouant l'un des principes les plus importants de Mozilla et de Firefox : la confiance. Pour faire la publicité de la série Mr. Robot Firefox a installé automatiquement une extension sans demander l'avis aux utilisateurs du navigateur. Heureusement Mozilla a fait marche arrière rapidement, mais les équipes et les utilisateurs ne vont pas oublier cette erreur...
