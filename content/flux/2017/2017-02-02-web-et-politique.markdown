---
layout: post
title: "Web et politique"
date: "2017-02-02T09:00:00"
comments: true
published: true
description: "Brad Frost pointe quelques énormités constatées ou en perspective suite à l'arrivée de Donald Trump à la présidence des États-Unis, et en rapport avec le web. L'appel à résistance de Brad Frost est clair, sans qu'il ne soit frontalement politique."
categories: 
- flux
tags:
- web
---
>We, the people who work on the web, have seen firsthand the great things can be accomplished when we open up, share, discuss ideas, and collaborate together. We need to stand by our commitment to openness and collaboration, and must unequivocally condemn any efforts to suppress the freedom of speech and ideas. We have a tremendous opportunity to help spread the spirit of the web, as it has woven itself into every industry and aspect of society.  
[Brad Frost, The Web’s Responsibility In The Trump Era](http://bradfrost.com/blog/post/the-webs-responsibility-in-the-trump-era/)

Brad Frost pointe quelques énormités constatées ou en perspective suite à l'arrivée de Donald Trump à la présidence des États-Unis, et en rapport avec le web. L'appel à résistance de Brad Frost est clair, mais est-il politique ?