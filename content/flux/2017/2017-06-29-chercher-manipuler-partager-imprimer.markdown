---
layout: post
title: "Chercher, manipuler, partager, imprimer"
date: "2017-06-29T10:00:00"
comments: true
published: true
description: "Strabic vient de publier ma brève sur le premier workshop de PrePostPrint, l'occasion de découvrir cette (super) initiative autour de la publication imprimée qui utilise les technologies et les outils du web !"
categories:
- flux
tags:
- publication
---
>Ce workshop a permis de détourner les technologies du web pour façonner des documents imprimés, et de privilégier l’expérimentation à l’efficacité.  
[Antoine Fauchié, Workshop PrePostPrint](http://strabic.fr/Workshop-PrePostPrint)

[Strabic](http://strabic.fr/) vient de publier ma brève sur le premier workshop de [PrePostPrint](http://prepostprint.org/), l'occasion de découvrir cette (super) initiative autour de la publication imprimée qui utilise les technologies et les outils du web !