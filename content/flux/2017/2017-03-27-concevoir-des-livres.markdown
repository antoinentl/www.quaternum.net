---
layout: post
title: "Concevoir des livres"
date: "2017-03-27T10:00:00"
comments: true
published: true
description: "Entretien avec Alexandre Laumonier, l'éditeur de Zones sensibles, absolument passionnant, sur : le design dans l'édition, comment un livre peut-il être conçu, la publication papier à l'ère numérique, la chaîne du livre, etc."
categories: 
- flux
tags:
- publication
---
>Le constat était simple : si, d’après certains pleureurs, les livres de sciences humaines ne se vendent plus, c’est moins à mon sens parce que le lectorat manque qu’en raison de la pauvreté graphique de la plupart des livres. Cela n’est pas seulement valable pour les couvertures, dont les choix graphiques peuvent toujours se discuter, ou pour les caractères utilisés pour les textes, mais aussi, plus globalement, pour les objets eux-mêmes – certains livres sont de vraies briques, impossibles à ouvrir correctement. Or le savoir doit s’ouvrir au lecteur, au sens propre comme au sens figuré. D’où le choix de notre brochage.  
[Entretien d'Ulysse Baratin avec Alexandre Laumonier, L’art de publier des essais](https://www.en-attendant-nadeau.fr/2017/03/14/entretien-alexandre-laumonier/)

Entretien avec Alexandre Laumonier, l'éditeur de [Zones sensibles](http://www.zones-sensibles.org/), absolument passionnant, sur : le design dans l'édition, comment un livre peut-il être conçu, la publication papier à l'ère numérique, la chaîne du livre, etc.

Via [Anthony Masure](https://twitter.com/AnthonyMasure/status/843544306644410368).

>Être éditeur, c’est créer un contexte (une maison, des livres, peut-être même des lignes de fuite) où tout livre doit pouvoir être le bienvenu à un certain moment [...].

>C’est à tous les niveaux de la chaîne du livre que se jouent l’indépendance d’esprit et la liberté de pensée : cela demande une attention quotidienne.