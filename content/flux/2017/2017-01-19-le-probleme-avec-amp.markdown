---
layout: post
title: "Le problème avec AMP"
date: "2017-01-19T22:00:00"
comments: true
published: true
description: "Ce n'est pas le premier billet qui vient critiquer AMP, le langage de balisage créé par Google pour accélérer le web : mauvaise expérience utilisateur en raison des URLs et des contenus dupliqués (même contenu avec deux accès différents, dont celui avec AMP peu compréhensible), la dépendance à Google en raison du contrôle du langage et des serveurs qui hébergent les pages, et les questions de données personnelles et de sécurité."
categories: 
- flux
tags:
- web
---
>Google’s goals with the AMP Project are laudable, but there are major security and UX concerns that need to be addressed. In its current form, AMP is bad for the open web and should be changed or eliminated.  
[Kyle Schreiber, The Problem With AMP](https://80x24.net/post/the-problem-with-amp/)

Ce n'est pas le premier billet qui vient critiquer <abbr title="Accelerated Mobile Pages">AMP</abbr>, le langage de balisage créé par Google pour *accélérer* le web : mauvaise expérience utilisateur en raison des URLs et des contenus dupliqués (même contenu avec deux accès différents, dont celui avec AMP peu compréhensible), la dépendance à Google en raison du contrôle du langage et des serveurs qui hébergent les pages, et les questions de données personnelles et de sécurité.

Via [Boris](https://borisschapira.com/) via [François](https://twitter.com/francoisz/status/821722614737629184).