---
layout: post
title: "Visualiser Git"
date: "2017-11-23T23:00:00"
comments: true
published: true
description: "Un outil qui permet de visualiser certaines commandes Git, parfois complexes à comprendre."
categories:
- flux
tags:
- outils
---
>This website is designed to help you understand some basic git concepts visually.  
[Visualizing Git Concepts with D3](https://onlywei.github.io/explain-git-with-d3/)

Un outil qui permet de visualiser certaines commandes Git, parfois complexes à comprendre.

Via Stéphane.