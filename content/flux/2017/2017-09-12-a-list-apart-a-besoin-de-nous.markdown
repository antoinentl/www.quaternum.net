---
layout: post
title: "A List Apart a besoin de nous"
date: "2017-09-12T09:00:00"
comments: true
published: true
description: "A List Apart, la publication qui parle de Web depuis vingt ans dont les articles sont bien souvent incontournables, a besoin de ses lecteurs pour se réinventer. A List Apart milite pour un web ouvert !"
categories:
- flux
tags:
- web
---
>In recent years, we’ve seen our rich universe of diverse, creative blogs and sites implode—leaving fewer and fewer channels available to new voices. As more content centralizes into a handful of all-powerful networks, there’s a dreary sameness in perspective and presentation.  
This creeping monopolization is a sad echo of how media worked in the 20th century. It doesn’t reflect 21st century diversity and empowerment. It’s not the web’s promise. It’s not how it’s supposed to be.  
We have no beef with networks like Twitter or Facebook, or with companies like Apple and Google that currently dominate our communal digital space. We just think diversity is about expanding and speaking up—not consolidating and homogenizing.  
[Jeffrey Zeldman, New A List Apart wants you!](https://alistapart.com/article/new-a-list-apart-wants-you)

A List Apart, la publication qui parle de Web depuis vingt ans dont les articles sont bien souvent incontournables, a besoin de ses lecteurs pour se réinventer. A List Apart milite pour un web ouvert !