---
layout: post
title: "Interaction et design"
date: "2017-10-09T17:00:00"
comments: true
published: true
description: "Un appel à contribution qui pourra, espérons-le, réunir des projets dits interactifs."
categories:
- flux
tags:
- design
---
>Dans le cadre de la revue Interfaces numériques, nous lançons un appel à contribution sur le thème : Design d’œuvres interactives & méthodologies de conception (parution en novembre 2018). [...] L’appel à contribution court jusqu’au 15 décembre 2017.  
[Designers Interactifs, Appel à contribution : Design d’œuvres interactives & méthodologies de conception](http://www.designersinteractifs.org/2017/10/09/appel-a-contribution-design-doeuvres-interactives-methodologies-de-conception/)

Un appel à contribution qui pourra, espérons-le, réunir des projets dits *interactifs*.