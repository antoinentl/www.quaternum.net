---
layout: post
title: "Quatrième Biennale du numérique"
date: "2017-06-16T10:00:00"
comments: true
published: true
description: "Le programme de la quatrième Biennale du numérique de l'École nationale des sciences de l'information et des bibliothèques est en ligne, elle se déroulera les 13 et 14 novembre 2017, et les inscriptions sont ouvertes."
categories:
- flux
tags:
- publication
---
>La 4ème édition de la Biennale du numérique de l'Ecole nationale supérieure des sciences de l'information et des bibliothèques (Enssib) sera consacrée à la place prise aujourd'hui par les plateformes numériques dans l'activité des éditeurs, des libraires et des bibliothécaires (bibliothèques publiques et universitaires confondues). Moment unique d’échanges et de débats interprofessionnels entre bibliothécaires, éditeurs, libraires, auteurs, chercheurs, l’objectif premier de cette Biennale sera de permettre aux professionnels du livre de mieux maîtriser les différents aspects que recouvre la notion de "plateforme numérique".  
[enssib, Biennale du numérique](http://www.enssib.fr/biennale-du-numerique)

Le programme de la quatrième Biennale du numérique de l'École nationale des sciences de l'information et des bibliothèques est en ligne, elle se déroulera les 13 et 14 novembre 2017, et les inscriptions sont ouvertes.