---
layout: post
title: "Deux journées autour de LaTeX"
date: "2017-09-08T17:00:00"
comments: true
published: true
description: "Deux journées organisées par l'enssib (et plus particulièrement par Éric Guichard) autour de LaTeX, un événement autour de la programmation éditoriale."
categories:
- flux
tags:
- publication
---
>[...] les liens se reconstruisent entre designers et typographes d’une part, monde universitaire d’autre part. Parce que l’esthétique et la sémiologie graphique reprennent de l’importance à l’heure du numérique (visualisation, cartographie, sites web, etc.). Et aussi parce que le regain d’intérêt pour la culture historique de la typographie est manifeste.  
[enssib, Journées LaTex : Typographie et édition numériques, épistémologie](http://www.enssib.fr/journees-LATEX)

Deux journées organisées par l'enssib (et plus particulièrement par Éric Guichard) autour de LaTeX, un événement autour de la "programmation éditoriale".