---
layout: post
title: "Jekyll pour les développeurs Wordpress"
date: "2017-04-21T10:00:00"
comments: true
published: true
description: "Un article assez classique sur les générateurs de site statique, et plus particulièrement sur Jekyll. L'approche est intéressante, puisque Mike Neumegen compare continuellement les static site generators à des CMS dynamiques comme Wordpress."
categories: 
- flux
tags:
- web
---
>Because it is difficult to perfectly transfer a thought from one mind to another it is essential that the principal medium through which such transference is accomplished may be as perfect as it is possible to make it.  
[Mike Neumegen, Jekyll For WordPress Developers](https://www.smashingmagazine.com/2017/04/jekyll-wordpress-developers/)

Un article assez classique sur les générateurs de site statique, et plus particulièrement sur Jekyll. L'approche est intéressante, puisque Mike Neumegen compare continuellement les static site generators à des CMS dynamiques comme Wordpress.