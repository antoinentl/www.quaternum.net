---
layout: post
title: "Un service web conversationnel basé sur Kirby"
date: "2017-01-11T14:00:00"
comments: true
published: true
description: "Étude de cas d'un site web de recherche d'emplois : une interface conversationnelle -- un peu comme uxchat.me -- avec le CMS Kirby comme gestionnaire de contenus."
categories: 
- flux
tags:
- design
---
>Ginetta created a concept for their new jobs website, which allows siroop to introduce themselves in a unique way. Through a conversational UI, a dialog between between siroop and potential candidates is established.  
[...]  
We used Kirby CMS as a lightweight file-based backend to manage the content of the site. Kirby is simple, especially for content editors.  
[Ginetta, siroop: Jobs](http://made.ginetta.net/siroop-jobs.html)

Étude de cas d'un site web de recherche d'emplois : une interface conversationnelle -- un peu comme [uxchat.me](http://uxchat.me/) -- avec le CMS [Kirby](https://getkirby.com/) comme gestionnaire de contenus.

Via [Kirby](https://twitter.com/getkirby/status/819152469267718144).