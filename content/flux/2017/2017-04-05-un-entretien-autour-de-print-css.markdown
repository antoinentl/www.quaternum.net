---
layout: post
title: "Un entretien autour de print CSS"
date: "2017-04-05T22:30:00"
comments: true
published: true
description: "Toby Osbourn s'entretient avec Jeremy Keith, celui-ci donne de la profondeur aux questions techniques liées à print CSS -- la mise en forme pour l'impression avec une feuille de style."
categories: 
- flux
tags:
- web
---
>Toby Osbourn: Why do you think in general browser have been slow to support paged media?  
Jeremy Keith: In a nutshell, because it’s invisible.  
[Jeremy Keith Interview](http://tosbourn.com/jeremy-keith-interview/)

Toby Osbourn s'entretient avec Jeremy Keith, celui-ci donne de la profondeur aux questions techniques liées à *print CSS* -- la mise en forme pour l'impression avec une feuille de style.