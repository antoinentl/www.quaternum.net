---
layout: post
title: "Histoire numérique"
date: "2017-01-14T22:00:00"
comments: true
published: true
description: "Long entretien, passionnant, avec Valérie Schafer, historienne d’Internet, du Web et des cultures numériques."
categories: 
- flux
tags:
- web
---
>Pour ma part, j'ai voulu faire une histoire française du Web - ce qui qui n’est pas tout à fait la même chose qu'une histoire du Web français.  
[InaGlobal, Une histoire française du Web…](http://www.inaglobal.fr/numerique/article/une-histoire-francaise-du-web-9490)

Long entretien, passionnant, avec Valérie Schafer, "historienne d’Internet, du Web et des cultures numériques".