---
layout: post
title: "Open Type Checker"
date: "2017-05-11T22:00:00"
comments: true
published: true
description: "Jiminy Panoz est joueur, et il nous propose aussi de jouer, avec cette web app permettant de tester les fonctionnalités d'Open Type."
categories: 
- flux
tags:
- typographie
---
>This is a minimum viable web app to test most latin open type features.  
[Jiminy Panoz, Open Type Checker](https://jaypanoz.github.io/otf-checker/)

Jiminy Panoz est joueur, et il nous propose aussi de jouer, avec cette web app permettant de tester les fonctionnalités d'Open Type.