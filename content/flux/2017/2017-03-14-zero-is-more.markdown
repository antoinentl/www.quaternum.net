---
layout: post
title: "Zero is more"
date: "2017-03-14T22:00:00"
comments: true
published: true
description: "Un manifeste minimaliste, avec des points très pertinents, et du recul à prendre sur certains aspects (en tout cas c'est mon avis)."
categories: 
- flux
tags:
- design
---
>Embracing minimalism on different aspects of your life has plenty of benefits, like peace of mind, knowing where things are, and being low-maintenance in general.  
[Jonathan Verrecchia, Zeromalist – Zero is more](http://verekia.com/zeromalist/)

Un manifeste minimaliste, avec des points très pertinents, et du recul à prendre sur certains aspects (en tout cas c'est mon avis).

Via [Frank](https://frank.taillandier.me/).