---
layout: post
title: "Printing in Relation to Graphic Art"
date: "2017-04-14T10:00:00"
comments: true
published: true
description: "Il s'agit de la version web du livre de George French, Printing in Relation to Graphic Art, publié en 1903. Un bel exemple de livre web."
categories: 
- flux
tags:
- livre
---
>Because it is difficult to perfectly transfer a thought from one mind to another it is essential that the principal medium through which such transference is accomplished may be as perfect as it is possible to make it.  
[George French, Printing in Relation to Graphic Art](http://print.lapels.club/)

Il s'agit de la version web du livre de George French, *Printing in Relation to Graphic Art*, publié en 1903. Un bel exemple de [livre web](https://github.com/antoinentl/web-books-initiatives).

Via [Jiminy](http://jiminy.chapalpanoz.com/).