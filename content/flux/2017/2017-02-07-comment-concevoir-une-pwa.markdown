---
layout: post
title: "Comment concevoir une PWA"
date: "2017-02-07T10:00:00"
comments: true
published: true
description: "L'équipe de 14islands a conçu une application web, une Progressive Web Application, et explique très clairement comment ils ont procédé."
categories: 
- flux
tags:
- design
---
>I think every website from now on should use some of the Progressive Web App features.  
[Hjörtur Hilmarsson, We built a PWA from scratch - This is what we learned](https://14islands.com/blog/2017/01/19/progressive-web-app-from-scratch/)

L'équipe de [14islands](https://14islands.com/) a conçu une application web, une Progressive Web Application, et explique très clairement comment ils ont procédé.