---
layout: post
title: "Une brève histoire de l'hypertexte"
date: "2017-02-28T22:00:00"
comments: true
published: true
description: "Jay Hoffmann raconte l'histoire d'Internet, et cette partie sur l'hypertexte est passionnante. Chouette projet, sous forme de timeline, à suivre."
categories: 
- flux
tags:
- web
slug: une-breve-histoire-de-l-hypertexte
---
>Hypertext, broadly defined, is a document which contains links to other documents. Pretty simple in theory, but it’s a concept that has echoed through the minds of innovators for over a hundred years.  
[Jay Hoffmann, A Brief History of Hypertext](http://thehistoryoftheweb.com/brief-history-hypertext/)

Jay Hoffmann [raconte l'histoire d'Internet](http://thehistoryoftheweb.com/timeline/), et cette partie sur l'hypertexte est passionnante. Chouette projet, sous forme de *timeline*, à suivre.