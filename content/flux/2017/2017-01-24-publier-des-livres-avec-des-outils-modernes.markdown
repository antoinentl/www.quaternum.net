---
layout: post
title: "Publier des livres avec des outils modernes"
date: "2017-01-24T22:00:00"
comments: true
published: true
description: "Eric Gardner a accepté de répondre à mes questions concernant la chaîne de publication qu'il a mis en place pour les éditions de The Getty. De mon point de vue c'est une petite révolution, car il s'agit de passer du couple Word + InDesign, ou d'une chaîne XML complexe, à un ensemble d'outils issus du web : Markdown, YAML, Git et un générateur de site statique. C'est une révolution métier."
categories: 
- flux
tags:
- publication
---
>Je voulais expérimenter une chaîne de production statique pour plusieurs raisons. Premièrement, je voulais m’assurer que notre travail demeure accessible aussi longtemps que possible. [...] Notre deuxième préoccupation était la dépendance à des logiciels propriétaires. [...] Enfin, comme toute personne ayant une formation en design, je me soucie beaucoup du design et de l’expérience que nous proposons à nos utilisateurs.  
[Antoine Fauchié s'entretient avec Eric Gardner, Publier des livres avec un générateur de site statique](https://jamstatic.fr/2017/01/23/produire-des-livres-avec-le-statique/)

Eric Gardner a accepté de répondre à mes questions concernant la chaîne de publication qu'il a mis en place pour les éditions de The Getty. De mon point de vue c'est une petite révolution, car il s'agit de passer du couple Word + InDesign, ou d'une chaîne XML complexe, à un ensemble d'outils issus du web : Markdown, YAML, Git et un générateur de site statique. C'est une révolution métier.

Un grand merci à [Frank](https://frank.taillandier.me/) d'avoir accepté cet entretien sur [Jamstatic.fr](https://jamstatic.fr/), et pour avoir grandement contribué à la traduction.

La version anglaise a ensuite été publiée sur The New Dynamic, merci à [Bud Parr](https://www.budparr.com/) : [Interview with Eric Gardner, Getty Publications](https://www.thenewdynamic.org/article/2017/01/26/interview-with-eric-gardner-getty/)