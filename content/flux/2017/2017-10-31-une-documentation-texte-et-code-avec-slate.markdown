---
layout: post
title: "Une documentation texte et code avec Slate"
date: "2017-10-31T10:00:00"
comments: true
published: true
description: "Slate permet de créer facilement de la documentation pour du code ou plus précisément pour des APIs, l'originalité de cet outil réside dans l'interface qui permet d'afficher du texte et du code sur un même écran."
categories:
- flux
tags:
- publication
---
>Slate helps you create beautiful, intelligent, responsive API documentation.  
[Robert Lorde, Slate](https://github.com/lord/slate)

[Slate](https://lord.github.io/slate/) permet de créer facilement de la documentation pour du code ou plus précisément pour des APIs, l'originalité de cet outil réside dans l'interface qui permet d'afficher du texte et du code sur un même écran.

Via [Eric Hellman](https://go-to-hellman.blogspot.fr/2017/10/turning-page-on-ereader-pagination.html).