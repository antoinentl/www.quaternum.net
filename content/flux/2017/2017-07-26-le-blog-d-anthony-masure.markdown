---
layout: post
title: "Le blog d'Anthony Masure"
date: "2017-07-26T17:00:00"
comments: true
published: true
description: "Anthony Masure, après avoir lancé son site, débute un blog. Rien d'original ? Anthony est enseignant-chercheur en design, ce qui veut dire qu'un format particulier tel que le blog n'est pas forcément habituel dans le milieu académique : une distinction très intéressante est faite entre l'article universitaire -- et son lot de contraintes -- et l'article de blog ou de carnet, qui présente plus de libertés."
categories:
- flux
tags:
- publication
slug: le-blog-d-anthony-masure
---
>Because of the complexity and the long delays in publishing academic papers (peer review, norms, etc.), there is a need to develop some free and fast expression spaces.  
[Anthony Masure, Hello Blog](http://www.anthonymasure.com/blog/2017-07-20-hello-blog)

Anthony Masure, après avoir lancé son site, débute un blog. Rien d'original ? Anthony est "enseignant-chercheur en design", ce qui veut dire qu'un format particulier tel que le blog n'est pas forcément habituel dans le milieu académique : une distinction très intéressante est faite entre l'article universitaire -- et son lot de contraintes -- et l'article de blog ou de carnet, qui présente plus de *libertés*. La réflexion sur la technologie employée -- base de données, language dynamique, génération de fichiers statiques -- est par ailleurs passionnante.