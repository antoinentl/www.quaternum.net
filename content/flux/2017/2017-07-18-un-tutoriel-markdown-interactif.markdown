---
layout: post
title: "Un tutoriel Markdown interactif"
date: "2017-07-18T17:00:00"
comments: true
published: true
description: "Un tutoriel Markdown (en anglais) avec des exercices interactifs, idéal pour apprendre facilement et rapidement Markdown !"
categories:
- flux
tags:
- outils
---
>Markdown is a simple way to format text that looks great on any device. It doesn’t do anything fancy like change the font size, color, or type — just the essentials, using keyboard symbols you already know.  
[CommonMark, Markdown Tutorial](http://commonmark.org/help/tutorial/)

Un tutoriel Markdown (en anglais) avec des exercices *interactifs*, idéal pour apprendre facilement et rapidement Markdown !

Via [Val](https://twitter.com/ValMuetdhiver/status/887290906075951105) via [Jamstatic-fr](https://twitter.com/jamstatic_fr/status/887289968204402688).