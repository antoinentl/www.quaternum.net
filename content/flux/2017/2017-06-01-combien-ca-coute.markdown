---
layout: post
title: "Combien ça coûte"
date: "2017-06-01T22:00:00"
comments: true
published: true
description: "Un rappel très complet de combien vaut un indépendant, et donc combien il doit facturer pour vivre. Indispensable pour toute personne voulant lancer son activité."
categories:
- flux
tags:
- design
---
>Sauf que là où Jason se plante, c’est qu’en fait, la valeur de son travail lorsqu’il était salarié, ce n’était pas son salaire net…  
[Princesse RH, Parce que vous le valez bien](http://libelilou.github.io/2017/05/29/sudweb.html)

Un rappel très complet de combien vaut un indépendant, et donc combien il doit facturer pour vivre. Indispensable pour toute personne voulant lancer son activité.