---
layout: post
title: "Kirby comme générateur de site statique"
date: "2017-02-22T22:00:00"
comments: true
published: true
description: "Une utilisation originale de Kirby -- un flat CMS qui n'utilise pas de base de données, contrairement à Wordpress --, utilisé comme un générateur de site statique."
categories: 
- flux
tags:
- publication
---
>I'm using Kirby as a CMS, but only on my local machine. The output of my Kirby templates and content is converted to a static site, which I push to github. This flow is baked into my gulp file, to make things quick and simple.  
[Nate Steiner, Blogging flow](https://natesteiner.com/blog/blogging-flow/)

Une utilisation originale de [Kirby](https://getkirby.com/) -- un *flat* <abbr title="Content Management System">CMS</abbr> qui n'utilise pas de base de données, contrairement à Wordpress --, utilisé comme un générateur de site statique.