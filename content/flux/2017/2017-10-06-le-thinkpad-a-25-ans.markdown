---
layout: post
title: "Le ThinkPad a 25 ans"
date: "2017-10-06T17:00:00"
comments: true
published: true
description: "Difficile de ne pas saluer un quart de siècle pour l'un des ordinateurs portables professionnels les plus utilisés -- ou populaires, au choix. Si le ThinkPad de Lenovo est aujourd'hui très critiqué, il reste pourtant une très bonne machine, pour peu qu'on y banisse Windows."
categories:
- flux
tags:
- outils
---
>Le ThinkPad avait été dessiné par le designer italien Richard Sapper (disparu en janvier 2016) avec l'aide de Kaz Yamasaki. Son format compact avait été étudié pour un usage en avion et inspiré par les bento japonais.  
[Florian Innocente, Un ThinkPad édition spéciale pour ses 25 ans](https://www.macg.co/materiel/2017/10/un-thinkpad-edition-speciale-pour-ses-25-ans-99963)

Difficile de ne pas saluer un quart de siècle pour l'un des ordinateurs portables professionnels les plus utilisés -- ou populaires, au choix. Si le ThinkPad de Lenovo est aujourd'hui très critiqué, il reste pourtant une très bonne machine, pour peu qu'on y banisse Windows.

Via [Nicolas](https://twitter.com/NT_polylogue/status/916034928286060545).