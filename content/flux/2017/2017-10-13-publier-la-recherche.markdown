---
layout: post
title: "Publier la recherche"
date: "2017-10-13T17:00:00"
comments: true
published: true
description: "Jeudi 9 novembre 2017 à Paris le Centre national des arts plastiques organise une journée d’échanges professionnels sur les relations entre le design graphique et les publications liées à la recherche, journée réservée aux professionnels. Une initiative qui prolonge des réflexions et des échanges déjà amorcés par le Cnap ou certains chercheurs comme Anthony Masure (ou d'autres)."
categories:
- flux
tags:
- publication
---
>Le jeudi 9 novembre 2017, le Centre national des arts plastiques (Cnap) propose, dans le cadre de ses actions de promotion du design graphique, une journée d’échanges professionnels sur les relations entre le design graphique et les publications liées à la recherche : revues, livres, affiches, brochures, sites Web, applications, etc.  
[Graphisme en France, Publier la recherche](http://www.cnap.graphismeenfrance.fr/article/paris-publier-recherche)

Jeudi 9 novembre 2017 à Paris le Centre national des arts plastiques organise "une journée d’échanges professionnels sur les relations entre le design graphique et les publications liées à la recherche", journée réservée aux professionnels. Une initiative qui prolonge des réflexions et des échanges déjà amorcés par le Cnap ou certains chercheurs comme Anthony Masure (ou d'autres).