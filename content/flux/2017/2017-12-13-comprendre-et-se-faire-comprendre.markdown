---
layout: post
title: "Comprendre et se faire comprendre"
date: "2017-12-13T22:00:00"
comments: true
published: true
description: "Pour 24 jours de web Éric Daspet propose de se poser la question des mots et des termes utilisés dans le domaine informatique ou numérique. Plus que l'enjeu du sens, c'est celui de l'interaction avec les autres dont il est question ici : comment rester compréhensible pour toutes les personnes ? Comment adapter notre langue (française dans notre cas) pour intégrer des termes anglophones ?"
categories:
- flux
tags:
- métier
---
>Notre jargon est souvent utilisé comme des formules magiques. Utiliser un terme à consonance anglophone est trop souvent un moyen d’oublier ce qu’il veut dire pour y attacher un sens semi-mystique.  
[Éric Daspet, On dit numérique et pas digital (bordel) !](https://www.24joursdeweb.fr/2017/on-dit-numerique-et-pas-digital-bordel/)

Pour [24 jours de web](https://www.24joursdeweb.fr/) Éric Daspet propose de se poser la question des mots et des termes utilisés dans le domaine informatique ou numérique. Plus que l'enjeu du *sens*, c'est celui de l'interaction avec les autres dont il est question ici : comment rester compréhensible pour toutes les personnes ? Comment adapter notre langue (française dans notre cas) pour intégrer des termes anglophones ?
