---
layout: post
title: "Un autre web est possible"
date: "2017-11-03T10:00:00"
comments: true
published: true
description: "Long article sur les phénomènes de centralisation du web, et d'utilisation massive des données personnelles, avec des propositions concrètes concernant les alternatives à adopter."
categories:
- flux
tags:
- privacy
---
>Do we want the web to be open, accessible, empowering and collaborative? Free, in the spirit of CERN’s decision in 1993 or the open source tools it's built on? Or do we want it to be just another means of endless consumption, where people become eyeballs, targets and profiles? Where companies use your data to control your behaviour and which enables a surveillance society — what do we want?  
For me, the choice is clear. And it's something worth fighting for.  
[Parimal Satyal, Against an Increasingly User-Hostile Web](https://www.neustadt.fr/essays/against-a-user-hostile-web/)

Long article sur les phénomènes de centralisation du web, et d'utilisation massive des données personnelles, avec des propositions concrètes concernant les alternatives à adopter.

Via [Jeremy Keith](https://adactio.com/links/13060).