---
layout: post
title: "Premier épisode de Killed by App"
date: "2017-04-07T20:00:00"
comments: true
published: true
description: "Première épisode d'une série sur le numérique, Killed by App, avec pour sujet la déconnexion."
categories: 
- flux
tags:
- offline
---
>Nous ne sommes pas *hyper* connecté aujourd'hui, mais plutôt *constamment* connecté, même si le terme est moins sexy.  
[Pia Pandelakis et Anthony Masure, Ex0053 Fantômes de la déconnexion](https://cpu.dascritch.net/post/2017/04/06/Ex0053-Fant%C3%B4mes-de-la-d%C3%A9connexion)

Première épisode d'une série sur le numérique, Killed by App, avec pour sujet la déconnexion, à écouter !