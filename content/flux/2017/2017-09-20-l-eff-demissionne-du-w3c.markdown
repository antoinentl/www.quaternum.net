---
layout: post
title: "L'EFF démissionne du W3C"
date: "2017-09-20T17:00:00"
comments: true
published: true
description: "L'Electronic Frontier Foundation (EFF) quitte le W3C, suite au débat sur la standardisation d'un DRM pour le web (EME pour Encrypted Media Extensions). Espérons que cela déclenche des réactions."
categories:
- flux
tags:
- web
slug: l-eff-demissionne-du-w3c
---
>We believe they will regret that choice. Today, the W3C bequeaths a legally unauditable attack-surface to browsers used by billions of people. They give media companies the power to sue or intimidate away those who might re-purpose video for people with disabilities. They side against the archivists who are scrambling to preserve the public record of our era. The W3C process has been abused by companies that made their fortunes by upsetting the established order, and now, thanks to EME, they’ll be able to ensure no one ever subjects them to the same innovative pressures.  
[Cory Doctorow, An open letter to the W3C Director, CEO, team and membership](https://www.eff.org/deeplinks/2017/09/open-letter-w3c-director-ceo-team-and-membership)

L'Electronic Frontier Foundation (EFF) quitte le W3C, suite au débat sur la standardisation d'un DRM pour le web (EME pour Encrypted Media Extensions). Espérons que cela déclenche des réactions.