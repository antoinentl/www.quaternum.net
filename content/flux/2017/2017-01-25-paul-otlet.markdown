---
layout: post
title: "Paul Otlet"
date: "2017-01-25T22:00:00"
comments: true
published: true
description: "Paul Otlet n'a peut-être pas inventé Internet, mais il a imaginé/conceptualisé un système d'information qui s'en rapproche beaucoup !"
categories: 
- flux
tags:
- internet
---
>Pourquoi cette intimité entre les bibliothèques, l’informatique et, au-delà, Internet ? Cette intimité a bien des causes, mais pourrait presque tenir en une personne. Un homme très connu des bibliothécaires, documentalistes, historiens du livre etc. mais dont la notoriété mériterait d’être étendue largement au-delà, tant son oeuvre est fascinante. Cet homme, c’est Paul Otlet.  
[Xavier de La Porte, Le belge qui a rêvé Internet](https://www.franceculture.fr/emissions/la-vie-numerique/le-belge-qui-reve-internet)

Paul Otlet n'a peut-être pas inventé Internet, mais il a imaginé/conceptualisé un système d'information qui s'en rapproche beaucoup !

>En fait, ce qui est fascinant chez Otlet, c’est qu’il avait anticipé que le savoir ne serait pas plus seulement dans les livres, qu’il connaîtrait d’autres supports, d’autres modes de consultation, de classement.