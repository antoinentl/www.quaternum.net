---
layout: post
title: "Bibliographie subjective sur le design"
date: "2017-10-05T17:00:00"
comments: true
published: true
description: "Une belle bibliographie de textes sur le design à (re)découvrir, et quelques références en matière de publication (Alessandro Ludovico et Craig Mod)."
categories:
- flux
tags:
- design
---
>A collection of canonical, recommended, and the personal favorite texts about and around graphic design of Jarrett Fuller.  
[Jarret Fuller, Graphic Design Readings](http://readings.design/index)

Une belle bibliographie de textes sur le design à (re)découvrir, et quelques références en matière de publication (Alessandro Ludovico et Craig Mod).