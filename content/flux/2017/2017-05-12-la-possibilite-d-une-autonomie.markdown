---
layout: post
title: "La possibilité d'une autonomie"
date: "2017-05-12T22:00:00"
comments: true
published: true
description: "Voici la réponse de Karl suite à une question de Tristan Nitot."
categories: 
- flux
tags:
- privacy
slug: la-possibilite-d-une-autonomie
---
>Fournir la possibilité d'une autonomie même si elle n'est pas utilisée par la majorité permet de créer le terrain d'opportunité d'affaires, de communications, de réalisations des individus. Un des gros problèmes de la centralisation des services est inscrite déjà dans l'architecture de connexion du réseau. Si un simple ordinateur à la maison peut devenir un service, nous élargissons le champs des possibles et des initiatives individuelles.  
[Karl, Tristan, Ministre de l'économie numérique](http://www.la-grange.net/2017/05/11/ministre)

Tristan Nitot a posé la question :

>Si je devenais ministre du numérique, quelle priorités choisir ? #libre #standards #crypto #privacy #innovation #empowerment #education  
[@tristan](https://twitter.com/nitot/status/861630235162406913)

Voici la réponse de Karl !