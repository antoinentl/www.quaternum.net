---
layout: post
title: "Design des programmes et des machines"
date: "2017-03-09T08:00:00"
comments: true
published: true
description: "Superbe émission de Marie Richeux -- Les Nouvelles vagues -- avec Nolwenn Maudet et Sophie Fétro, à l'occasion de leurs articles dans le premier numéro de la revue Back Office."
categories: 
- flux
tags:
- design
---
>Ce qui m'intéresse quand je fais des outils pour les designers, c'est comment on permet de donner cette marge de liberté que les designers vont utiliser avec leurs outils traditionnels.  
[Nolwenn Maudet, Les Nouvelles vagues, Les machines : Merveilleuses bécanes](https://www.franceculture.fr/emissions/les-nouvelles-vagues/les-machines-35-merveilleuses-becanes)

Superbe émission de Marie Richeux -- Les Nouvelles vagues -- avec Nolwenn Maudet et Sophie Fétro, à l'occasion de leurs articles dans le premier numéro de la revue [Back Office](http://www.revue-backoffice.com/).