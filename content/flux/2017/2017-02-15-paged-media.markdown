---
layout: post
title: "Paged Media"
date: "2017-02-15T19:00:00"
comments: true
published: true
description: "Grâce à Julien je découvre Paged Media : A blog which advocates for innovative approaches to making books in browsers."
categories: 
- flux
tags:
- publication
---
>At Paged Media, we have a vision for books in browsers, a vision that tries to learn from history, from the art and craft of print. We want them to be as beautiful as print, but flexible, accessible, adaptable. We want to be able to take a book and change the font or screen size, and have it automatically adjust to the new size, but still without widows and orphans and bad breaks. We want to be able to use ideas from the history of print, such as drop caps or footnotes or running heads, as well as links, popups, javascript, forms, and all the clever tools from the age of browsers.  
[Dave Cramer, Our vision for books in browsers](https://www.pagedmedia.org/our-vision-for-books-in-browsers/)

Grâce à [Julien](https://twitter.com/john_tax) je découvre Paged Media : "A blog which advocates for innovative approaches to making books in browsers."