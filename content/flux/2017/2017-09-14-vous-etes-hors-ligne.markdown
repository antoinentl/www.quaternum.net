---
layout: post
title: "Vous êtes hors ligne"
date: "2017-09-14T17:00:00"
comments: true
published: true
description: "Comment gérer le mode hors ligne d'un site web, et comment avertir l'utilisateur ? Quelques conseils (avec du code dedans) clairs et concis."
categories:
- flux
tags:
- web
---
>Offline support is awesome, however your users might not be aware of these capabilites - and they shouldn’t have to be. In some cases they might not even know that they’ve gone offline. That’s why it’s important to communicate what’s going on.  
[Max Böck, You're Offline](https://mxb.at/blog/youre-offline/)

Comment gérer le mode hors ligne d'un site web, et comment avertir l'utilisateur ? Quelques conseils (avec du code dedans) clairs et concis.