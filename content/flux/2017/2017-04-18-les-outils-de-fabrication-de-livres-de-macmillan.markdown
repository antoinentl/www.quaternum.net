---
layout: post
title: "Les outils de fabrication de livres de Macmillan"
date: "2017-04-18T10:00:00"
comments: true
published: true
description: "Macmillan partage les outils de sa chaîne de publication, intéressant de voir que l'on retrouve toujours le même paradigme : le traitement de texte est conservé, pour le moment il s'agit plus de *patchs* que d'un changement de pratiques."
categories: 
- flux
tags:
- publication
---
>Bookmaker is an open-source toolkit to make books out of Word .docx files and HTMLBook HTML files (it can accept either type of file as input). It was developed as part of a pilot project to try to make books with almost no budget, and it is currently in use today at Macmillan.  
[Bookmaker: Macmillan's bookmaking tool](https://macmillanpublishers.github.io/bookmaker/)

Macmillan partage les outils de sa chaîne de publication, intéressant de voir que l'on retrouve toujours le même paradigme : le traitement de texte est conservé, pour le moment il s'agit plus de *patchs* que d'un changement de pratiques.

>Remember--this is a tool we built for ourselves, and we're not a software company!

Le support de présentation de Nellie McKesson de cette chaîne de publication, lors d'ebookcraft 2017, est [disponible en ligne](https://www.slideshare.net/booknetcanada/how-i-built-an-automated-ebook-production-platformand-you-can-too-nellie-mckesson-ebookcraft-2017), ainsi que [la vidéo](https://booknetcanada.wistia.com/medias/a2wwcmwr24).

Via [Jiminy](https://twitter.com/JiminyPan).