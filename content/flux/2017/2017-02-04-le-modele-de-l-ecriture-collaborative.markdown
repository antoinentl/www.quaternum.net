---
layout: post
title: "Le modèle de l'écriture collaborative"
date: "2017-02-04T10:00:00"
comments: true
published: true
description: "L'écriture collaborative comme programme politique ? Si Xavier de La Porte distingue l'usage de Google Docs de celui d'outils plus respectueux comme les pads -- et notamment ceux mis en place par Framasoft -- il aurait été intéressant d'aller regarder comment on écrit avec Git -- que ce soit du code ou du texte."
categories: 
- flux
tags:
- publication
slug: le-modele-de-l-ecriture-collaborative
---
>Accepter que ce qu’on fait (le paragraphe qu’on ajoute, le mot que l‘on corrige, l’idée que l’on apporte) ne nous appartient pas, mais se fond dans un ensemble plus vaste.  
[Xavier de La Porte, Du Google Docs comme modèle politique](https://www.franceculture.fr/emissions/la-vie-numerique/du-google-docs-comme-modele-politique)

L'écriture collaborative comme programme politique ? Si Xavier de La Porte distingue l'usage de Google Docs de celui d'outils plus respectueux comme les pads -- et notamment ceux mis en place par Framasoft -- il aurait été intéressant d'aller regarder comment on écrit avec Git -- que ce soit du code ou du texte.