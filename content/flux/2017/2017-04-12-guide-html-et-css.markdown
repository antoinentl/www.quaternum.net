---
layout: post
title: "Guide HTML et CSS"
date: "2017-04-12T10:00:00"
comments: true
published: true
description: "Louis Wimart a créé une documentation très claire pour débuter en HTML et CSS."
categories: 
- flux
tags:
- outils
---
>Derrière cet effrayant nom (HyperTextMarkupLanguage) se cache un site pour apprendre simplement les bases du HTML et du CSS. Par ailleurs, celui-ci s’adresse plus particulièrement aux graphistes et étudiants. L’objectif étant de partir sur les bonnes pratiques actuelles.  
[hypertextmarkuplanguage : guide html & css pour les graphistes](http://jenseign.com/html/)

[Louis Wimart](http://jaiunsite.com) a créé une documentation très claire pour débuter en HTML et CSS.