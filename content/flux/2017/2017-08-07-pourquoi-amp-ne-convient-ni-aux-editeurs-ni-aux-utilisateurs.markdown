---
layout: post
title: "Pourquoi AMP ne convient ni aux éditeurs ni aux utilisateurs"
date: "2017-08-07T17:00:00"
comments: true
published: true
description: "Une nouvelle critique de AMP -- Accelerated Mobile Pages, un projet créé par Google --, par Alex Kras en tant qu'éditeur de contenu -- utilisant AMP -- et en tant qu'utilisateur -- lecteur du web en quelque sorte."
categories:
- flux
tags:
- web
---
>Back in the day we used to have WAP pages – specific web pages that were presented only to mobile devices. Opting into AMP, for publishers, is kind of like going back to those days. Instead of using responsive design (making sure that one version of the site works well on all devices) publishers are forced to maintained 2 versions of each page – their regular version for larger devices and mobile phones that don’t use Google and the AMP version.  
[Alex Kras, I decided to disable AMP on my site](https://www.alexkras.com/i-decided-to-disable-amp-on-my-site/)

Une nouvelle critique de AMP -- Accelerated Mobile Pages, un projet créé par Google --, par Alex Kras en tant qu'éditeur de contenus -- utilisant AMP -- et en tant qu'utilisateur -- lecteur du Web.

>As far as I am concerned, static content (without JavaScript) is still the king.