---
layout: post
title: "Codeurs en Seine 2017"
date: "2017-10-17T10:00:00"
comments: true
published: true
description: "Le programme de Codeurs en Seine 2017 est en ligne, avec Thomas Parisot nous y parlerons de livres, et de comment la culture web peut influencer l'édition. De quoi rappeler quelques souvenirs ! Si vous n'êtes pas loin de Rouen, inscrivez-vous !"
categories:
- flux
tags:
- web
---
>L'invention de l'imprimerie a révolutionné la diffusion du savoir dans le monde. Mais quelle est sa véritable genèse ? Ce docu-fiction riche d'archives rares révèle la naissance tumultueuse de la presse de Gutenberg.  
[Codeurs en Seine 2017](http://www.codeursenseine.com/2017/)

Le programme de Codeurs en Seine 2017 est en ligne, avec Thomas Parisot nous y parlerons de livres, et de comment la culture web peut influencer l'édition. De quoi rappeler [quelques](/2017/03/07/ecrire-un-livre-en-2017/) [souvenirs](/2017/03/13/une-chaine-de-publication-inspiree-du-web/) ! Si vous n'êtes pas loin de Rouen, [inscrivez-vous](http://www.codeursenseine.com/2017/inscription) c'est gratuit !