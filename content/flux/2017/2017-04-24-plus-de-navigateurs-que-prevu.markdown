---
layout: post
title: "Plus de navigateurs que prévu"
date: "2017-04-24T10:00:00"
comments: true
published: true
description: "Un rappel nécessaire : il y a bien plus de navigateurs que ce que l'on pense lorsque l'on est occidental. Il n'y a pas que Chrome, Firefox, Safari, IE/Edge ou Opera, et cela change pas mal de choses si vous faites du web en dehors de l'Europe et des États-Unis."
categories: 
- flux
tags:
- web
---
>This is an important point. The browser market share can vary a lot depending on location.   
[Peter O'Shaughnessy, Think you know the top web browsers?](https://medium.com/samsung-internet-dev/think-you-know-the-top-web-browsers-458a0a070175)

Un rappel nécessaire : il y a bien plus de navigateurs que ce que l'on pense lorsque l'on est occidental. Il n'y a pas que Chrome, Firefox, Safari, IE/Edge ou Opera, et cela change pas mal de choses si vous faites du web en dehors de l'Europe et des États-Unis.