---
layout: post
title: "Documentation as code"
date: "2017-11-30T22:00:00"
comments: true
published: true
description: "En décembre 2016 Hubert Sablonnière était venu présenter Documentation as code à Web en vert, et donc AsciiDoc et Asciidoctor comme moyen d'écrire du texte comme on écrit du code. J'aurais dû regarder cette vidéo bien plus tôt... Hubert est particulièrement clair et convaincant, je recommande de regarder cette petite conférence très bien faite à toute personne qui voudrait se passer de traitement de texte et produire des documents facilement et puissamment (attention je parle bien de documents et non de livre)."
categories:
- flux
tags:
- publication
---
>Quand j'ai montré [AsciiDoc] à mon père, il a désinstallé Word dans la seconde !  
[Hubert Sablonnière, Documentation as code (expliqué à mon père)](https://www.youtube.com/watch?v=1rKgVF5CEEY)

En décembre 2016 Hubert Sablonnière était venu présenter "Documentation as code" à Web en vert, et donc AsciiDoc et Asciidoctor comme moyen d'écrire du texte comme on écrit du code. J'aurais dû regarder cette vidéo bien plus tôt... Hubert est particulièrement clair et convaincant, je recommande de regarder cette petite conférence très bien faite à toute personne qui voudrait se passer de traitement de texte et produire des documents facilement et puissamment (attention je parle bien de documents et non de livre).