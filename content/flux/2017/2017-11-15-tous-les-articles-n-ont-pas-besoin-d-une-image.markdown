---
layout: post
title: "Tous les articles n'ont pas besoin d'une image"
date: "2017-11-15T22:00:00"
comments: true
published: true
description: "Hanson O'Haver fustige contre l'utilisation systèmatique d'images pour illustrer des articles qui n'en auraient pas forcément besoin. Je partage son avis, même si Hanson O'Haver manque un peu d'argument."
categories:
- flux
tags:
- publication
slug: tous-les-articles-n-ont-pas-besoin-d-une-image
---
>If a picture is worth a thousand words, it’s hard for me to imagine there’ll be much value in the text of an article illustrated by a generic stock image.  
[Hanson O'Haver, Not every article needs a picture](https://theoutline.com/post/2485/not-every-article-needs-a-picture)

Hanson O'Haver fustige contre l'utilisation systèmatique d'images pour illustrer des articles qui n'en auraient pas forcément besoin. Je partage son avis, même si Hanson O'Haver manque un peu d'argument.

>Even the fucking Economist now has a photo on every article on its website.

>Adults do not need pictures to help them read.