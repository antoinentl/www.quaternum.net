---
layout: post
title: "Graphisme en France numéro 23"
date: "2017-06-02T22:00:00"
comments: true
published: true
description: "Le dernier numéro de Graphisme en France vient d'être publié, les thèmes de ce numéro de 2017 sont les logos et les identités visuelles. Une version PDF est disponible en ligne, et la version imprimée est distribuée un peu partout ou disponible sur demande. Il manque une version web, ce serait vraiment un plus !"
categories:
- flux
tags:
- typographie
---
>La vingt-troisième édition de la revue Graphisme en France est parue, publiée par le Centre national des arts plastiques (Cnap). Elle aborde pour la première fois la question des logos et des identités visuelles, objet central dans la pratique du design graphique.  
[CNAP, Graphisme en France n°23](http://cnap.graphismeenfrance.fr/livre/graphisme-france-ndeg23)

Le dernier numéro de Graphisme en France vient d'être publié, les thèmes de ce numéro de 2017 sont les logos et les identités visuelles. Une version PDF [est disponible en ligne](http://www.cnap.fr/sites/default/files/publication/152314_cnap_graphisme-en-france_23_2017-fr.pdf), et la version imprimée est distribuée un peu partout ou disponible sur demande. Il manque une version web, ce serait vraiment un plus !