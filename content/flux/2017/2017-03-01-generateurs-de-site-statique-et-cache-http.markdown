---
layout: post
title: "Générateurs de site statique et cache HTTP"
date: "2017-03-01T08:00:00"
comments: true
published: true
description: "Karl en avait déjà parlé il y a quelques années lors de son utilisation de Pelican, cette critique des générateurs de site statique est intéressante, et je ne sais pas si certains des générateurs ont réussi à résoudre ce problème -- il est tout de même désormais possible de ne générer que les pages créées ou modifiées avec Jekyll par exemple."
categories: 
- flux
tags:
- publication
---
>Mon principal problème avec tous les gestionnaires de soit-disant contenu statique est qu'ils le ne sont pas. Le contenu n'est pas généré à chaud en effet, au moment de la requête HTTP, mais il est tout de même généré au moment de la mise à jour. Mais le problème, le plus important est que presque tous ces gestionnaires regénèrent tout le contenu du site pour la création d'un seul article et de fait la plupart du temps détruisent les informations de cache HTTP.  
[Karl, Le printemps de février](http://www.la-grange.net/2017/02/12/beaute)

Karl en avait déjà parlé il y a quelques années lors de son utilisation de [Pelican](https://blog.getpelican.com/), cette critique des générateurs de site statique est intéressante, et je ne sais pas si certains des générateurs ont réussi à résoudre ce problème -- il est tout de même désormais possible de ne générer que les pages créées ou modifiées avec Jekyll par exemplee.