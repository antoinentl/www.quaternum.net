---
layout: post
title: "Constellations"
date: "2017-05-24T22:00:00"
comments: true
published: true
description: "Louis Éveillard a créé cette page web pour présenter la prochaine édition des Rencontres internationales de Lure, et c'est beau !"
categories:
- flux
tags:
- typographie
---
>Une semaine de culture graphique du 20 au 26 août, à Lurs, Alpes de Haute-Provence.  
[Les Rencontres internationales de Lure, Constellations – Attractions, liens et tensions graphiques](http://delure.org/constellations)

[Louis Éveillard](https://louiseveillard.com/) a créé cette page web pour présenter [la prochaine édition des Rencontres internationales de Lure](http://delure.org/les-rencontres/rencontres-precedentes/rencontres-2017), et c'est beau !
