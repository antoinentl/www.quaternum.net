---
layout: post
title: "Statique et versionné"
date: "2017-02-08T19:00:00"
comments: true
published: true
description: "Rapide, sécurisé, versionné, flexible, limité, tous les arguments en faveur de la combinaison générateur de site statique + fichiers versionnés avec Git + hébergement du code source et des fichiers sur GitHub."
categories: 
- flux
tags:
- web
---
>Our website has been fully open source since we released it around two years ago. We built the site using Jekyll, a popular static website generator.  
The site is hosted on Github. We used Jekyll with Github for several reason:
- It’s fast
- It’s secure
- It’s version controlled
- It’s flexible
- It’s dev friendly
- It’s limited
> 
>[Hjörtur Hilmarsson, Our website is open source](https://14islands.com/blog/2016/08/25/our-website-is-open-source/)

Rapide, sécurisé, versionné, flexible, limité, tous les arguments en faveur de la combinaison `générateur de site statique` + `fichiers versionnés avec Git` + `hébergement du code source et des fichiers sur GitHub`.

(Je découvre 14islands et je suis assez fan de leur démarche, de leur vision et leurs projets.)