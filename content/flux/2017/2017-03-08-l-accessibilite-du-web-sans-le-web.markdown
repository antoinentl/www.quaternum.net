---
layout: post
title: "L'accessibilité du web sans le web"
date: "2017-03-08T08:00:00"
comments: true
published: true
description: "Romy raconte une situation qui semble totalement surréaliste aujourd'hui, en 2017, et qui est pourtant bien réelle."
categories: 
- flux
tags:
- accessibilité
slug: l-accessibilite-du-web-sans-le-web
---
>la plus grande difficulté en accessibilité web, n’est pas tant l’accessibilité elle-même, que le mot accolé, « web », qui est lui abyssalement méconnu. Voilà la réalité : celleux qui sont responsables des sites web, commanditaires, chefs de projets, éditeurs… ne connaissent que les documents bureautiques et plus précisément Microsoft comme horizon indépassable. Ils font tout avec ça. Tout. Y compris leurs sites web.  
[Romy Têtue, Accessibilité bureaucratique](http://romy.tetue.net/accessibilite-bureaucratique)

Romy raconte une situation qui semble totalement surréaliste aujourd'hui, en 2017, et qui est pourtant bien réelle.