---
layout: post
title: "Front-End-Checklist"
date: "2017-10-23T22:00:00"
comments: true
published: true
description: "Une liste bien pratique pour gérér beaucoup de détails déterminants lors de la production d'un site web."
categories:
- flux
tags:
- web
---
>The perfect Front-End Checklist for modern websites and meticulous developers  
[Front-End-Checklist](http://frontendchecklist.com/)

Une liste bien pratique pour gérér beaucoup de détails déterminants lors de la production d'un site web.

Via [Stéphanie Walter](https://twitter.com/WalterStephanie/status/922375814838214656).