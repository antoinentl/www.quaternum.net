---
layout: post
title: "Faciliter l'apprentissage du web"
date: "2017-02-13T10:00:00"
comments: true
published: true
description: "Oliver James a écrit une série de tutoriels, en anglais, HTML & CSS Is Hard, pour apprendre les bases du web en 14 leçons : HTML, CSS, sémantique, mise en forme, formulaire, typographie, etc. Cela semble (très) bien fait !"
categories: 
- flux
tags:
- web
slug: faciliter-l-apprentissage-du-web
---
>Learning to code shouldn’t be hard. We’re making it easier by putting together a comprehensive set of web development tutorials to help transform complete beginners into talented Interneting professionals. We’ve got the curriculum, all you need is the motivation to start reading it.  
[Oliver James, Interneting Is Hard (But it doesn’t have to be)](https://internetingishard.com/)

Oliver James a écrit une série de tutoriels, en anglais, [HTML & CSS Is Hard](https://internetingishard.com/html-and-css/), pour apprendre les bases du web en 14 leçons : HTML, CSS, sémantique, mise en forme, formulaire, typographie, etc. Cela semble (très) bien fait !

Via [Web Development Reading List](https://wdrl.info/archive/169).