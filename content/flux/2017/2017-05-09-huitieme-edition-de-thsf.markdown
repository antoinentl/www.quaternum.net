---
layout: post
title: "Huitième édition de THSF"
date: "2017-05-09T22:00:00"
comments: true
published: true
description: "Au-delà d'une simple programmation, le THSF vous invite, avec l'ensemble des invités et des intervenants, à partager un temps et un espace communs quels que soient les parcours, les compétences de chacun."
categories: 
- flux
tags:
- design
---
>Logiciel et matériel libre, DIY, réappropriation et détournement des technologies, sciences, défense des droits et libertés sur Internet, sécurité informatique, arts, création, culture(s), politique et société... Autant de sujets et de pratiques autour desquels les hackers du Tetalab et les artistes de Mix'Art Myrys vous invitent à venir découvrir, apprendre, questionner, partager et construire.  
[THSF #8](https://www.thsf.net/)

>Au-delà d'une simple programmation, le THSF vous invite, avec l'ensemble des invités et des intervenants, à partager un temps et un espace communs quels que soient les parcours, les compétences de chacun. L'objectif est de générer ensemble un espace critique et expérimental fondé sur les échanges et les recherches menés dans les champs croisés de l'art, des technologies, de la philosophie et bien d‘autres.