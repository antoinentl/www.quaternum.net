---
layout: post
title: "Facebook tue le web ouvert"
date: "2017-06-19T10:00:00"
comments: true
published: true
description: "Facebook tue le web ouvert."
categories:
- flux
tags:
- publication
---
>I complain about Google AMP, but AMP is just a dangerous step toward a Google-owned walled garden — Facebook is designed from the ground up as an all-out attack on the open web.  
[John Gruber, Fuck Facebook](https://daringfireball.net/2017/06/fuck_facebook)

Facebook tue le web ouvert.