---
layout: post
title: "Une revue de recherche sur GitHub"
date: "2017-05-03T22:00:00"
comments: true
published: true
description: "Une revue de recherche qui n'est pas au format PDF, qui intègre des graphes interactifs, au design travaillé, et dont les sources sont accessibles ? Distill propose une forme et un modèle de publication très originaux, en utilisant notamment git et GitHub."
categories: 
- flux
tags:
- publication
---
>A modern medium for presenting research: The web is a powerful medium to share new ways of thinking. Over the last few years we’ve seen many imaginative examples of such work. But traditional academic publishing remains focused on the PDF, which prevents this sort of communication.  
[Distill](http://distill.pub/about/)

Une revue de recherche qui n'est pas au format PDF, qui intègre des graphes interactifs, au design travaillé, et dont les sources sont accessibles ? Distill propose une forme et un modèle de publication très originaux, en utilisant notamment git et GitHub.

David Rosenthal a rédigé [un billet de présentation](http://blog.dshr.org/2017/05/distill-is-this-what-journals-should.html), avec quelques interrogations et critiques concernant l'archivage d'une telle forme de revue.

Via [iGor](https://herds.eu/notice/2242315).