---
title: "Css remis à zéro"
date: 2022-07-15
description: "La remise à zéro des feuilles de style des pages web est souvent utile pour pouvoir dessiner correctement."
categorie:
- flux
tags:
- design
---
> Whenever I start a new project, the first order of business is to sand down some of the rough edges in the CSS language. I do this with a functional set of custom baseline styles.  
> Josh Comeau, My Custom CSS Reset, [https://www.joshwcomeau.com/css/custom-css-reset/](https://www.joshwcomeau.com/css/custom-css-reset/)

La remise à zéro des feuilles de style des pages web est souvent utile pour pouvoir _dessiner_ correctement.
Les quelques lignes et leurs explications sont ici très pratiques.
