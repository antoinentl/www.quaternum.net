---
title: Typographie accessible et inclusive
date: 2022-12-05
description: "Quarto est une suite de commandes Pandoc avec de nombreux paramètres très utiles, des modèles de document et (notamment) une intégration dans des éditeurs de texte."
categorie:
- flux
tags:
- typographie
---
> Le refus de l’inclusion de tous·tes dans l’écriture semble donc ne pas être fondé sur des bases solides, mais sur du validisme, voire de l’eugénisme. En effet, cela laisse sous-entendre que si l’écriture inclusive ne doit pas se développer, c’est à cause des personnes ayant des différences neurologiques.  
> Sophie Vela, Pour enfin faire rimer inclusivité et accessibilité. Recommandations pour les dessinateurices de caractères face à l’argument de l’illisibilité, [https://typo-inclusive.net/accessibiliteinclusive/](https://typo-inclusive.net/accessibiliteinclusive/)

Les recherches de Sophie Vela sont intéressantes pour plusieurs raisons, et notamment parce qu'elle est allée discuter avec les personnes concernées, ne s'arrêtant pas aux idées préconçues loin des besoins ou des pratiques réelles.
Penser une typographie accessible **et** inclusive semble être un combat et une recherche longue et difficile, mais pourtant pas impossible :

> ce qu’il restera de ces expérimentations et de ces différentes formes d’écriture, c’est l’usage que nous en ferons.

