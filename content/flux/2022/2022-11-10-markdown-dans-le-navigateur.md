---
title: Markdown dans le navigateur
date: 2022-11-10
description: "Voilà une préoccupation qui n'est pas nouvelle, à savoir _lire_ le format Markdown directement dans le navigateur sans conversion préalable."
categorie:
- flux
tags:
- publication
---
> Pretty-rendering markdown files would suddenly enable everyone to express themselves broadly and with full browser and device ratio support. Something that today only a commercial service or a professional web developer can offer.
> Tim Daubenschütz, Why We Should Have Markdown Rendered Websites, [https://ipfs.io/ipfs/bafybeid7lt7snuzcvcmfqs5a5hlc5fmk3xmflz4hx2qa7c674vm3rpsdvm/why-we-should-have-markdown-rendered-websites.html](https://ipfs.io/ipfs/bafybeid7lt7snuzcvcmfqs5a5hlc5fmk3xmflz4hx2qa7c674vm3rpsdvm/why-we-should-have-markdown-rendered-websites.html)

Voilà une préoccupation qui n'est pas nouvelle, à savoir _lire_ le format Markdown directement dans le navigateur sans conversion préalable.
De nombreux scripts permettent déjà de réaliser assez facilement cela, avec la dépendance à JavaScript et le fait de devoir disposer d'une page HTML qui embarque tout cela.
Il y a deux contraintes dans ce projet (pertinent malgré tout pour certains usages selon moi) :

1. il n'y a pas de standard pour Markdown et les _saveurs_ sont nombreuses, il me semble difficile d'aboutir à un consensus aujourd'hui ;
2. même pour des documents jugés _simples_ il y aura toujours des exceptions et des cas particuliers auxquels certains convertisseurs répondent bien (comme Pandoc).

"Let's have markdown rendering in all major browsers soon."
