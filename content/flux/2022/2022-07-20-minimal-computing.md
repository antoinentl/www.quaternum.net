---
title: "Minimal computing"
date: 2022-07-20
description: "Alex Gil et d'autres utilisent cette expression de minimal computing depuis plusieurs années, cette approche se traduit par exemple par la création d'outils comme Ed (un outil d'édition minimaliste basé sur Jekyll)."
categorie:
- flux
tags:
- recherche
---
> Minimal computing is less a singular methodology — or even a coherent set of methodologies — than it is a mode of thinking about digital humanities praxis that resists the idea that “innovation” is defined by newness, scale, or scope. Broadly speaking, minimal computing connotes digital humanities work undertaken in the context of some set of constraints. This could include lack of access to hardware or software, network capacity, technical education, or even a reliable power grid.  
> Roopika Risam et Alex Gil, Introduction: The Questions of Minimal Computing, [http://www.digitalhumanities.org//dhq/vol/16/2/000646/000646.html](http://www.digitalhumanities.org//dhq/vol/16/2/000646/000646.html)

Alex Gil et d'autres utilisent cette expression de _minimal computing_ [depuis plusieurs années](https://go-dh.github.io/mincomp/), cette approche se traduit par exemple par la création d'outils comme [Ed](/2020/02/17/ed/) (un outil d'édition minimaliste basé sur Jekyll).
Roopika Risam et Alex Gil donnent plusieurs clés pour comprendre cette méthodologie, sans la réduire à un effet de mode (comme le low-tech).
Il y aurait aussi des liens à faire avec le [permacomputing](https://wiki.xxiivv.com/site/permacomputing.html).
