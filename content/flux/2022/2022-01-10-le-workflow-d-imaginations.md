---
title: "Le workflow d'Imaginations"
date: 2022-01-10
description: "Markus Reisenleitner a organisé un atelier autour du workflow de production d'_Imaginations_, il décrit tous les détails de la chaîne de publication avec les contraintes (nombreuses) et les enjeux du numérique très bien présentés."
categorie:
- flux
tags:
- publication
---
> In this workshop, Markus described the workflow and technologies Imaginations uses to generate article layouts in HTML and PDF formats. There was opportunity for participants to discuss their own workflows and challenges.  
> York University, [Typesetting and galley production workshop](https://digital.library.yorku.ca/yul-1153730/typesetting-and-galley-production-workshop)

[Markus Reisenleitner](http://markusreisenleitner.info/) a organisé un atelier autour du workflow de production d'_Imaginations_, il décrit tous les détails de la chaîne de publication avec les contraintes (nombreuses) et les enjeux du numérique très bien présentés.
Le concept de "single-source typesetting" est très intéressant : envisager non pas la publication mais déjà la composition.