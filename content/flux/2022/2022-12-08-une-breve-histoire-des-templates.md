---
title: Une brève histoire des templates
date: 2022-12-08
description: "Nolwenn Maudet détaille l'histoire passionnante des _templates_, d'abord sous l'angle du graphisme mais aussi en évoquant des questions proches des sciences de l'information."
categorie:
- flux
tags:
- design
---
> À travers l’étude de ses évolutions au gré des développements des technologies numériques, cet article montre que les templates incarnent une rigidification progressive du travail de la composition graphique et qu’ils mettent en jeu une tension constante entre émancipation et contrôle des utilisateurs.  
> Nolwenn Maudet, Une brève histoire des templates, entre autonomisation et contrôle des graphistes amateurs, [https://journal.dampress.org/issues/systemes-logiques-graphies-materialites/une-breve-histoire-des-templates-entre-autonomisation-et-controle-des-graphistes-amateurs](https://journal.dampress.org/issues/systemes-logiques-graphies-materialites/une-breve-histoire-des-templates-entre-autonomisation-et-controle-des-graphistes-amateurs)

Nolwenn Maudet détaille l'histoire passionnante des _templates_, d'abord sous l'angle du graphisme mais aussi en évoquant des questions proches des sciences de l'information.
Ce travail d'archéologie des médias est majeur et donne envie d'en lire encore plus.
