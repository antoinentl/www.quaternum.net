---
title: "Simplifier l'existant"
date: 2022-02-18
description: "Gemini est intéressant en tant qu'alternative à un Web devenu à la fois trop encombrant et trop capitaliste, mais ne serait-il pas pertinent d'envisager un usage plus simple du Web sans réinventer un protocole ?"
categorie:
- flux
tags:
- web
---
> When comparing the most minimal version of an HTTP request with a standard Gemini request, it turns out that the only difference is a single additionally required header (Host) and a few additional characters (GET and HTTP/1.1) in the HTTP request. Hence, Gemini’s argument of being “lighter than the web” doesn’t make that much of a difference at all from a protocol perspective, and it certainly does not justify completely replacing existing infrastructure and standards that humans have mutually agreed upon.  
> Marius, Gemini is Solutionism at its Worst, [https://マリウス.com/gemini-is-solutionism-at-its-worst/](https://マリウス.com/gemini-is-solutionism-at-its-worst/)

Gemini est intéressant en tant qu'alternative à un Web devenu à la fois trop encombrant et trop capitaliste, mais ne serait-il pas pertinent d'envisager un usage plus simple du Web sans réinventer un protocole ?
Je rejoins l'avis de Marius, en revanche en terme de [langage de balisage](/2021/05/31/gemini-ce-que-vous-voyez-est-ce-qui-est-lu/) je trouve (toujours) que Gemini est une super idée, pas très réaliste mais tout de même super.
 
