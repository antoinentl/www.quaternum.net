---
title: "Que se passe-t-il sur un terminal ?"
date: 2022-12-17
description: "Voilà une question des plus passionnantes et dont les réponses sont très intriguantes."
categorie:
- flux
tags:
- informatique
---
> I’ve been confused about what’s going on with terminals for a long time.  
> […]  
> As usual we’ll answer that question by doing some experiments and seeing what happens :)  
> Julia Evans, What happens when you press a key in your terminal?, [https://jvns.ca/blog/2022/07/20/pseudoterminals/](https://jvns.ca/blog/2022/07/20/pseudoterminals/)

Voilà une question des plus passionnantes et dont les réponses sont très intriguantes.
L'usage du terminal implique toutes sortes d'étapes d'écriture plus ou moins visibles.
