---
title: Le choix d'un format simple à manipuler
date: 2022-10-30
description: "Les histoires de conversion depuis [insérer ici un format complexe ou un langage de balisage non léger] à Markdown sont nombreuses, je pointe celle-ci parce qu'elle prend en compte quatre éléments qui me semblent pertinents (en tout cas pour moi) : faciliter le travail des personnes qui écrivent, prendre en charge des spécificités sémantiques (ici décrire du code), convertir des milliers de documents et enfin prévoir la maintenance de ces documents sur un temps long."
categorie:
- flux
tags:
- publication
---
> For technical writers, HTML is hard to write and hard to review. It's easy to make mistakes like missing closing tags or getting the nesting wrong, and it's hard for reviewers to spot these kinds of mistakes.  
> Will Bamberg, MDN => Markdown, [https://openwebdocs.org/content/posts/markdown-conversion/](https://openwebdocs.org/content/posts/markdown-conversion/)

Les histoires de conversion depuis [insérer ici un format complexe ou un langage de balisage non léger] à Markdown sont nombreuses, je pointe celle-ci parce qu'elle prend en compte quatre éléments qui me semblent pertinents (en tout cas pour moi) : faciliter le travail des personnes qui écrivent, prendre en charge des spécificités sémantiques (ici décrire du code), convertir des milliers de documents et enfin prévoir la maintenance de ces documents sur un temps long.

> Overall, writing and reviewing is dramatically easier than it used to be. The beauty of Markdown, to you as a writer, is that it lets you concentrate on expressing concepts and hardly thinking about syntax at all.

À noter que l'enjeu est aussi de pouvoir _réviser_ facilement le texte.
HTML n'est peut-être pas si facilement versionnable, trop verbeux.
