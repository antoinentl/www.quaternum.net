---
title: TOTP pour 2FA
date: 2022-12-13
description: "Sous l'acronyme 2FA se cache le concept de double authentification ou authentification à deux facteurs, principe d'identification désormais de plus en plus répandu pour tout type de service numérique : en plus du duo classique identifiant et mot de passe, un second mot de passe temporaire est demandé."
categorie:
- flux
tags:
- design
---
> In any language, TOTP is just a couple of dozen lines of code even if there isn’t already a library — and there is probably already a library. You don’t have to store temporary SMS codes in the database, you don’t have to worry about phishing, you don’t have to worry about SIM swapping, and you don’t have to sign up for some paid SMS API like Twilio. It’s more secure and it’s trivial to implement — so implement it already! Please!  
> Drew Devault, TOTP for 2FA is incredibly easy to implement. So what's your excuse?, [https://drewdevault.com/2022/10/18/TOTP-is-easy.html](https://drewdevault.com/2022/10/18/TOTP-is-easy.html)

Sous l'acronyme 2FA se cache le concept de double authentification ou authentification à deux facteurs, principe d'identification désormais de plus en plus répandu pour tout type de service numérique : en plus du duo classique identifiant et mot de passe, un second mot de passe temporaire est demandé.
Le problème de la double authentification, c'est que certaines applications sont parfois imposées, notamment du côté des banques, ou qu'un message texte est envoyé sur un téléphone comme deuxième authentification (pas la meilleure façon de sécuriser un accès, notamment en Amérique du Nord où il est possible d'usurper un numéro de téléphone).
TOTP (pour [Time-based one-time password](https://en.wikipedia.org/wiki/Time-based_one-time_password)) est un principe technique très sécuritaire et facile à implémenter.
TOTP partout !
