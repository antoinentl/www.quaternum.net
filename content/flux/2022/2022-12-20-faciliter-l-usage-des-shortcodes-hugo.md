---
title: "Faciliter l'usage des shortcodes Hugo"
date: 2022-12-20
description: "Joost van der Schee pose une question très pertinente pour qui a déjà utilisé des shortcodes avec le générateur de site statique et a voulu les rendre utilisable via une interface graphique (autre qu'un éditeur de texte)."
categorie:
- flux
tags:
- publication
---
> I really liked the idea of adding shortcodes, but I wanted a better implementation. First, I thought of a smart way to set up which shortcodes you can see in the editor. […] Secondly, I had to determine what happens when somebody clicks on the shortcode button in the CMS.  
> Joost van der Schee, A CMS with Hugo shortcode support, [https://www.usecue.com/blog/a-cms-with-hugo-shortcode-support/](https://www.usecue.com/blog/a-cms-with-hugo-shortcode-support/)

Joost van der Schee pose une question très pertinente pour qui a déjà utilisé des shortcodes avec le générateur de site statique et a voulu les rendre utilisable via une interface graphique (autre qu'un éditeur de texte).
Les solutions esquissées ici sont intéressantes parce qu'elles ne cachent pas totalement le code de ces shortcodes.
