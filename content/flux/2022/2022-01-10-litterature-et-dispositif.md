---
title: "Littérature et dispositif"
date: 2022-01-10
description: ""
categorie:
- flux
tags:
- écriture
published: false
---
> Je partirai dans cet article de l’hypothèse que les arts numériques, dont la littérature, ont toujours été des « arts du dispositif ». Ils sont indissociables des structures que le techno-pouvoir fait peser sur eux ; mais ils sont également innervés de savoirs et de potentialités que ce pouvoir met à leur disposition.  
> Alexandra Saemmer, Poétiques du crypto-texte, [https://mei-info.com/revue/50/33/poetiques-du-crypto-texte/](https://mei-info.com/revue/50/33/poetiques-du-crypto-texte/)

