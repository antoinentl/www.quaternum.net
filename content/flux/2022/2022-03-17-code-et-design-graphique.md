---
title: "Code et design graphique"
date: 2022-03-17
description: "Silvio Lorusso signe l'introduction du livre Graphic Design in the Post-Digital Age, un document qui rassemble des entretiens avec des designers graphiques et des analyses des pratiques à l'ère post-numérique."
categorie:
- flux
tags:
- design
---
> The computer is just one of those extensions, but a particularly powerful one, since it is, to use Alan Kay’s term, a metamedium, that is, a medium capable of simulating all others. As such the computer should be a thing that can be shaped and transformed.  
> Silvio Lorusso, Learn to Code vs. Code to Learn: Creative Coding Beyond the Economic Imperative, [https://www.postdigitalgraphicdesign.com/introduction](https://www.postdigitalgraphicdesign.com/introduction)

Silvio Lorusso signe l'introduction du livre _Graphic Design in the Post-Digital Age_, un document qui rassemble des entretiens avec des designers graphiques et des analyses des pratiques à l'ère post-numérique.
Je n'ai lu que de courts passages, mais ces textes semblent d'une richesse incroyable.
