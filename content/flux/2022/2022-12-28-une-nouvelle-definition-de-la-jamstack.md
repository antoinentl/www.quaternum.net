---
title: "Une (nouvelle) définition de la Jamstack"
date: 2022-12-28
description: "Si le concept de Jamstack (terme utilisé pour définir les couches cumulatives de JavaScript, API et Markup) méritait une petite mise à jour, tant les technologies ont évolué depuis presque 10 ans, cette nouvelle définition est avant tout un positionnement de la part de Netlify."
categorie:
- flux
tags:
- web
---
> Jamstack is an architectural approach that decouples the web experience layer from data and business logic, improving flexibility, scalability, performance, and maintainability.  
> Jamstack removes the need for business logic to dictate the web experience. It enables a composable architecture for the web where custom logic and 3rd party services are consumed through APIs.  
> The best practices for building with the Jamstack evolve alongside modern technologies.  
> Phil Hawksworth, The Jamstack definition evolved, [https://www.netlify.com/blog/the-jamstack-definition-evolved/](https://www.netlify.com/blog/the-jamstack-definition-evolved/)

Si le concept de Jamstack (terme utilisé pour définir les couches cumulatives de JavaScript, API et Markup) méritait une petite mise à jour, tant les technologies ont évolué depuis presque 10 ans, cette nouvelle définition est avant tout un positionnement de la part de Netlify.
La dernière phrase signifie d'elle-même que tout cela n'est qu'une question de formulation, et clairement Netlify souhaite se mettre du côté des besoins, quels qu'ils soient…

