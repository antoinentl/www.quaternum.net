---
title: "Pour des infrastructures libres"
date: 2022-03-29
description: "Drew DeVault, l'un des fondateurs de sourcehut, formule des critiques à l'égard des plateformes de code qui ne repose pas sur des infrastructures libres."
categorie:
- flux
tags:
- web
---
> In short, choosing non-free platforms is an individualist, short-term investment which prioritizes your project’s apparent access to popularity over the success of the FOSS ecosystem as a whole. On the other hand, choosing FOSS platforms is a collectivist investment in the long-term success of the FOSS ecosystem as a whole, driving its overall growth. Your choice matters.  
> Drew DeVault, It is important for free software to use free software infrastructure, [https://drewdevault.com/2022/03/29/free-software-free-infrastructure.html](https://drewdevault.com/2022/03/29/free-software-free-infrastructure.html)

Drew DeVault, l'un des fondateurs de [sourcehut](https://sourcehut.org/), formule des critiques à l'égard des plateformes de code qui ne repose pas sur des infrastructures libres.
Quand bien même le code que nous proposons est libre, ce n'est pas forcément le cas de plateformes comme GitHub ou GitLab : certains des composants sont propriétaires.
Drew DeVault nous encourage à nous interroger sur nos pratiques, sur les choix que nous faisons en nous tournant telle ou telle plateformme.
Et ce ne sont pas les alternatives qui manquent, [sourcehut](https://sr.ht/) me semble l'une des plus réjouissante actuellement.

> Many projects choose to prioritize access to the established audience that large commercial platforms provide, in order to maximize their odds of becoming popular, and enjoying some of the knock-on effects of that popularity, such as more contributions. Such projects would prefer to exacerbate the network effects problem rather than risk some of its social capital on a less popular platform.

C'est un problème récurrent pour de nombreux projets, mais peut-être est-il temps de faire des choix plus militants et plus audacieux en se détournant de GitHub ou GitLab.
