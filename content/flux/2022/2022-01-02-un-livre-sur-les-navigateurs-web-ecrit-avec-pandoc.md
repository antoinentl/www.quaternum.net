---
title: "Un livre sur les navigateurs web écrit avec Pandoc"
date: 2022-01-02
description: "Le fonctionnement des navigateurs web est rarement décrit avec autant de précision que dans ce livre à la fois curieux et (probablement) incontournable."
categorie:
- flux
tags:
- web
---
> Web browsers are ubiquitous, but how do they work? This book explains, building a basic but complete web browser, from networking to JavaScript, in a thousand lines of Python.  
> Pavel Panchekha et Chris Harrelson, _Web Browser Engineering_, [https://browser.engineering/](https://browser.engineering/)

Le fonctionnement des navigateurs web est rarement décrit avec autant de précision que dans ce livre à la fois curieux et (probablement) incontournable.

Ce qui attire plus particulièrement mon attention ici c'est la _fabrique_ en place pour écrire et produire ce livre web : Markdown, Pandoc et Make ([lien vers le dépôt](https://github.com/browserengineering/book)).
Le contenu est structuré en Markdown, largement suffisant pour la structuration assez simple de ce livre.
Les fichiers HTML sont produits à partir de Markdown, la conversion est assurée par Pandoc avec des filtres Lua et quelques scripts (dont un script Awk qui compte les mots).
Make facilite la production des fichiers avec les commandes Pandoc adéquates pour les différents types de fichiers, et contient également les scripts rsync pour l'envoi des fichiers sur le serveur du livre web.
À noter enfin qu'il y a un script qui teste toutes les commandes, lancé à chaque `push` sur le dépôt.

Via [Robin Rendle](https://www.robinrendle.com/notes/browser-engineering/).
