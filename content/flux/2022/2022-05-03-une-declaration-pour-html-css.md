---
title: "Une déclaration (d'amour) pour HTML et CSS"
date: 2022-05-03
description: "Une belle et juste déclaration d'amour à ce langage de balisage."
categorie:
- flux
tags:
- web
---
> Markup requires systematic thinking.  
> Ashley Kolodziej, Dear HTML & CSS, [https://css-tricks.com/a-love-letter-to-html-css/](https://css-tricks.com/a-love-letter-to-html-css/)

Une belle et juste déclaration d'amour à ce langage de balisage.
