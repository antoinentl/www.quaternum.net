---
title: "Six conseils pour une meilleure typographie sur le Web"
date: 2022-05-07
description: "Six conseils très facile à implémenter sur un nouveau projet ou à utiliser pour influencer un travail déjà en cours."
categorie:
- flux
tags:
- typographie
---
> How do we avoid the most common mistakes when it comes to setting type on the web? That’s the question that’s been stuck in my head lately as I’ve noticed a lot of typography that’s lackluster, frustrating, and difficult to read. So, how can we improve interfaces so that our content is easy to read at all times and contexts? How do learn from everyone else’s mistakes, too?  
> Robin Rendle, Six tips for better web typography, [https://css-tricks.com/six-tips-for-better-web-typography/](https://css-tricks.com/six-tips-for-better-web-typography/)

Six conseils très facile à implémenter sur un nouveau projet ou à utiliser pour influencer un travail déjà en cours.
