---
title: "Thème de nuit ?"
date: 2022-01-18
description: "Comment créer une version sombre d'une feuille de style CSS sans compliquer les choses inutilement : Brad T. propose une solution très efficace (en partie testée de mon côté), qui fonctionne bien pour des petits sites web."
categorie:
- flux
tags:
- design
---
> So I thought to myself, “There has to be a simpler way…”  
> Brad Taunt, The Lazy Developer's Dark Mode, [https://tdarb.org/lazy-dev-dark-mode/](https://tdarb.org/lazy-dev-dark-mode/)

Comment créer une version _sombre_ d'une feuille de style CSS sans compliquer les choses inutilement : Brad T. propose une solution très efficace (en partie testée de mon côté), qui fonctionne bien pour des petits sites web.