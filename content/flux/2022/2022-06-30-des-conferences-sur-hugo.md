---
title: "Des conférences sur Hugo"
date: 2022-06-30
description: "Hugo, le générateur de site statique très rapide qui ne nécessite pas de dépendances, suscite encore beaucoup d'intérêt au point d'avoir son propre événement avec une série de conférences plus ou moins techniques."
categorie:
- flux
tags:
- web
---
> The free, online conference for everything Hugo  
> Hugo Conf, [https://hugoconf.io/](https://hugoconf.io/)

Hugo, le générateur de site statique très rapide qui ne nécessite pas de dépendances, suscite encore beaucoup d'intérêt au point d'avoir son propre événement avec une série de conférences plus ou moins techniques.
C'est CloudCannon qui organise cela, et certaines interventions ont l'air vraiment intéressantes : "Word to Web with Hugo in 5 Minutes", "Limitless HTTP requests with Hugo: from basic GET to GraphQL", "Custom Shortcodes for the Win", etc.
Ça se passe en ligne les 8 et 9 juillet 2022, et c'est gratuit.
