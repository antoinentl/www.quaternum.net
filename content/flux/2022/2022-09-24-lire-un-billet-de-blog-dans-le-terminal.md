---
title: "Lire un billet de blog dans le terminal"
date: 2022-09-24
description: "Voilà un petit bricolage simple et efficace pour lire du contenu web dans le terminal."
categorie:
- flux
tags:
- web
---
> I realised that my blog and its content, even though very simple and lightweight, are only accessible using a full-fledged web browser. I thought it would be interesting if my blog posts were available to be read using an even simpler interface, cURL!  
> mahdi, You can read my blog posts using curl, [https://mahdi.blog/raw-permalinks-for-accessibility/](https://mahdi.blog/raw-permalinks-for-accessibility/)

Voilà un petit bricolage simple et efficace pour lire du contenu web dans le terminal.
En proposant d'accéder à la source d'une page HTML, ici au format Markdown, via HTTP, mahdi permet tout simplement d'afficher ce fichier dans un terminal avec `curl`.
Pour peu que l'on soit à l'aise avec Markdown, la lecture est possible — avec toute la richesse d'un terminal.
