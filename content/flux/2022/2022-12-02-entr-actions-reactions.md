---
title: "Entr : actions, réactions"
date: 2022-12-02
description: "Entr, pour Event Notify Test Runner, est un petit programme qui permet de déclencher une action dès qu'un fichier est modifié."
categorie:
- flux
tags:
- outils
---
> A utility for running arbitrary commands when files change.  
> entr, [https://github.com/eradman/entr](https://github.com/eradman/entr)

Entr, pour Event Notify Test Runner, est un petit programme qui permet de déclencher une action dès qu'un fichier est modifié.
Autrement dit, dès qu'un fichier est modifié il est possible de déclencher un traitement via un programme sur ce fichier.
Par exemple dès qu'un fichier balisé est modifié il pourrait par exemple être possible de transformer ce fichier avec Pandoc.

Via [David](https://larlet.fr/david/).
