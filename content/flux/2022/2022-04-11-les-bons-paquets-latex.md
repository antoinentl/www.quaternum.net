---
title: "Les bons paquets LaTeX"
date: 2022-04-11
description: "Un des problèmes avec LaTeX, c'est le poids particulièrement conséquent d'une distribution LaTeX, et la gestion des bibliothèques/packages qui va avec."
categorie:
- flux
tags:
- outils
---
> Tectonic automatically downloads support files so you don’t have to install a full LaTeX system in order to start using it. If you start using a new LaTeX package, Tectonic just pulls down the files it needs and continues processing.  
> Tectonic, The Tectonic Typesetting System, [https://tectonic-typesetting.github.io](https://tectonic-typesetting.github.io)

Un des problèmes avec LaTeX, c'est le poids particulièrement conséquent d'une distribution LaTeX, et la gestion des bibliothèques/packages qui va avec.
Tectonic s'occupe de sélectionner uniquement les composants utiles, sans télécharger des distributions complètes de plusieurs gigas octets (si j'ai bien compris).

Via [Igor](https://twitter.com/igor_milhit/status/1513523584341360644).
