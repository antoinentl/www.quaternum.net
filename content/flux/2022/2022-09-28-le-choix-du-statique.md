---
title: Le choix du statique
date: 2022-09-28
description: "En quelques lignes Stéphane Pouyllau présente tous les bénéfices que représente le générateur de site statique Hugo couplé à du déploiement continu sur la plateforme GitLab."
categorie:
- flux
tags:
- publication
---
> Face à l’obsolescence rapide des systèmes de gestion de contenus Web s’appuyant sur les langages de programmation dynamiques traditionnels et les systèmes de gestion de base de données relationnelles […], il est possible d’opter pour des systèmes de publication Web utilisant des pages HTML statiques, dépendant moins des mises à jour sous-jacentes de ces langages et outils dynamiques et s’inscrivant pleinement dans les principes fondamentaux du Web, en utilisant principalement le langage HTML.  
> Stéphane Pouyllau, Déploiement d’un site Web statique sous Hugo avec l’intégration continue de GitLab Pages, [https://hnlab.huma-num.fr/blog/2022/09/08/Web-statique/](https://hnlab.huma-num.fr/blog/2022/09/08/Web-statique/)

En quelques lignes Stéphane Pouyllau présente tous les bénéfices que représente le générateur de site statique Hugo couplé à du déploiement continu sur la plateforme GitLab.
Pouvoir profiter des ressources d'Huma-Num et donc utiliser le service GitLab Pages intégré dans leur instance GitLab est un argument intéressant.
