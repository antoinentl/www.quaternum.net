---
title: "Publier plusieurs formats à partir d'une source unique, avec Pandoc et Make"
date: 2022-07-08
description: "L'initiative est audacieuse : proposer (imposer ?) un modèle de document qui ne repose pas sur des outils classiques et qui prend d'abord la forme d'une page web, le tout dans une école d'art et de design."
categorie:
- flux
tags:
- publication
---
> Je peux bricoler et peaufiner ces aspects en CSS ou en LaTeX sans que cela interfère avec l’écriture ; lorsque je dois me concentrer sur cette dernière, tout le reste s’efface. Grâce à cela, j’ai pu progresser dans mon manuscrit de thèse tout en continuant à travailler sur son design éditorial sans que ces deux tâches ne se cannibalisent l’une l’autre.  
> Arthur Perret, Publication multiformats avec Pandoc et Make, [https://www.arthurperret.fr/blog/2022-06-22-publication-multiformats-pandoc-make.html](https://www.arthurperret.fr/blog/2022-06-22-publication-multiformats-pandoc-make.html)

Produire plusieurs artefacts à partir d'une seule source est un objectif difficile à atteindre, pour des raisons autant éditoriales que techniques.
La solution proposée par Arthur Perret est à la fois simple, puissante et, d'une certaine façon, durable.
Basé sur Pandoc pour la conversion et Make pour l'automatisation des tâches et des commandes, ce workflow est disponible [sur GitHub](https://github.com/infologie/pandoc-ssp), avec également [une _antisèche_](https://www.arthurperret.fr/img/pandoc-ssp.pdf) pour comprendre le fonctionnement global.
Un beau travail.
