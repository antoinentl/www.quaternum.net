---
title: "Fabriquer sa propre intégration continue"
date: 2022-09-15
description: "Christian Ştefănescu décrit assez rapidement comment fabriquer son propre système d'intégration continue (ou comment déclencher des actions à chaque nouveau commit dans un dépôt Git)."
categorie:
- flux
tags:
- outils
---
> This is a little demonstration of how little you need to host your own git repositories and have a modest Continuous Integration system for them.  
> Christian Ştefănescu, A tiny CI system, [https://www.stchris.net/tiny-ci-system.html](https://www.stchris.net/tiny-ci-system.html)

Christian Ştefănescu décrit assez rapidement comment fabriquer son propre système d'intégration continue (ou comment déclencher des actions à chaque nouveau commit dans un dépôt Git).
L'idée est d'être indépendant, et évidemment ce type de configuration se prête difficilement à une infrastructure très sollicitée.
