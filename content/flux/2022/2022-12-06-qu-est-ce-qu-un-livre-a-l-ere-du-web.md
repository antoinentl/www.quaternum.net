---
title: "Qu'est-ce qu'un livre à l'ère du Web ?"
date: 2022-12-06
description: "Hugh McGuire propose un premier texte sur ce qu'est le livre, aujourd'hui, sous forme d'un manifeste en cinq parties."
categorie:
- flux
tags:
- livre
---
> The starting premise is that the format of a book doesn’t change its bookness.  
> Hugh McGuire, What is a “book” in the age of the web? (Part 1 of 5), [https://scribe.rip/3a529701e0df](https://scribe.rip/3a529701e0df)

Hugh McGuire propose un premier texte sur ce qu'est le livre, aujourd'hui, sous forme d'un manifeste en cinq parties.
Cette première étape concerne la définition du livre, et la question du format du livre.
L'approche défendue ici, que l'on pourrait qualifier d'ouverte et de technophile, rejoint en partie celle d'Amaranth Borsuk avec [_The Book_](https://mitpress.mit.edu/9780262535410/).

> For example, print books are better for providing physical cues to help with memory and retention; ebooks are better for convenience, portability, and support for media; web books can be interactive in ways neither ebooks nor print can readily achieve.

