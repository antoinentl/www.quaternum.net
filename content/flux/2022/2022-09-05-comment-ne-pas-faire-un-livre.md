---
title: Comment ne pas faire un livre
date: 2022-09-05
description: "Bonne nouvelle, Robin Rendle lance une lettre d'information qui parlera de son expérience de fabrication d'un livre, hâte de découvrir tout le processus sous sa plume."
categorie:
- flux
tags:
- livre
---
> There’s almost too much to care for; the design, paper stock, binding, proofreading, logistics, then—not to forget and most important of all—the words you need to write inside. But what does that process look like? And how do you publish an independent book in 2022?  
> Robin Rendle, How Not To Write a Book, [https://hownottomakeabook.com/](https://hownottomakeabook.com/)

Bonne nouvelle, [Robin Rendle](https://robinrendle.com/) lance une lettre d'information qui parlera de son expérience de fabrication d'un livre, hâte de découvrir tout le processus sous sa plume.
Robin a déjà évoqué l'utilisation de Word pour l'assemblage de ces textes, c'est intéressant d'observer comment quelqu'un qui est passionné par le Web et qui connaît bien CSS va fabriquer un livre.
Par ailleurs c'est étonnant de ne pas voir plus de lien entre le développement web et la fabrication d'un livre.
Encore une remarque, Robin a commencé par évoqué la question de l'impression/fabrication de l'objet avant celle de l'édition (qui relit ? comment préparer un fichier pour l'impression ? quelles métadonnées pour le livre ? etc.), intéressant comme approche que de remonter le processus.
