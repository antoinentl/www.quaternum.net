---
title: "Graph ta recherche"
date: 2022-03-09
description: "Je parle peu de ce que je fais à la Chaire de recherche du Canada sur les écritures numériques, alors quand une personne le fait, ce serait dommage de ne pas la citer."
categorie:
- flux
tags:
- recherche
---
> En ce sens, notre défense demeure inchangée :  
> L’environnement d’écriture est une forme de pensée  
> Margot Mellet, Graph ta recherche, [https://blank.blue/cherches/graph-ta-recherche/](https://blank.blue/cherches/graph-ta-recherche/)

Je parle peu de ce que je fais à la [Chaire de recherche du Canada sur les écritures numériques](https://ecrituresnumeriques.ca), alors quand une personne le fait, ce serait dommage de ne pas la citer (d'autant plus quand c'est bien dit).
J'ai (un peu) contribué à cette expérimentation dont Margot est l'initiatrice.
Un projet qui donnera probablement lieu à d'autres projets.
