---
title: "Des outils dans le terminal"
date: 2022-04-13
description: "Julia Evans publie une liste d'outils à utiliser dans le terminal, et ces outils sont vraiment cools (pour en avoir testé quelqu'uns)."
categorie:
- flux
tags:
- outils
---
> Today I asked on twitter about newer command line tools, like `ripgrep` and `fd` and `fzf` and `exa` and `bat`.  
> I got a bunch of replies with tools I hadn’t heard of, so I thought I’d make a list here. A lot of people also pointed at the [modern-unix](https://github.com/ibraheemdev/modern-unix) list.  
> Julia Evans, A list of new(ish) command line tools, [https://jvns.ca/blog/2022/04/12/a-list-of-new-ish--command-line-tools/](https://jvns.ca/blog/2022/04/12/a-list-of-new-ish--command-line-tools/)

Julia Evans publie une liste d'outils à utiliser dans le terminal, et ces outils sont vraiment cools (pour en avoir testé quelqu'uns).
De la visualisation de différences dans Git à l'affichage du contenus d'un fichier avec de la coloration syntaxique, beaucoup de nouveaux jouets à tester.

Via [Baldur Bjarnason](https://toot.cafe/@baldur/108124448781145793).
