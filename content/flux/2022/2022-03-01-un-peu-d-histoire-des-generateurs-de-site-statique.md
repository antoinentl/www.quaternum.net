---
title: "Un peu d'histoire des générateurs de site statique"
date: 2022-03-01
description: "La démarche de Mike Neumegen — le CEO de CloudCannon —, est intéressante car elle permet de casser un peu l'image parfois romancée des générateurs de site statique."
categorie:
- flux
tags:
- web
---
> Before Jekyll — when the world was young — static website generators were the sole domain of the mavericks of the web development community. After all, why would you want a static website when you could have the flexibility and power of dynamic generation? Building a static HTML website from scratch was a repetitive process, which most people ‘solved’ with dynamic approaches.  
> Even with all the hype around dynamic generation, a few brave souls still saw the benefits of static websites.  
> Mike Neumegen, The "Before Jekyll" era, [https://cloudcannon.com/blog/ssg-history-1-before-jekyll/](https://cloudcannon.com/blog/ssg-history-1-before-jekyll/)

La démarche de Mike Neumegen — le CEO de CloudCannon —, est intéressante car elle permet de casser un peu l'image parfois romancée des générateurs de site statique.
Bien avant Jekyll des développeuses et des développeurs utilisaient déjà des systèmes de génération de fichiers HTML tout en se passant de _workflows_ dits dynamiques.
La suite de la série ("SSGs through the ages") est passionnante, même si on aimerait en savoir plus sur certains détails ou certains ressorts théoriques sous-jacents (mais ça ça fait partie de mon travail de recherche).
