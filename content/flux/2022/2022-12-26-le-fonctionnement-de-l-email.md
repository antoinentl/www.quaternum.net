---
title: Le fonctionnement de l'email
date: 2022-12-26
description: "Tout ce que vous avez toujours savoir (ou pas) sur le fonctionnement du mail ou courriel."
categorie:
- flux
tags:
- outils
---
> Being one of the oldest services on the Internet, email has been with us for decades and will remain with us for at least another decade. Even though email plays an important role in everyday life, most people know very little about how it works.  
> Kaspar Etter, Email explained from first principles, [https://explained-from-first-principles.com/email/](https://explained-from-first-principles.com/email/)

Tout ce que vous avez toujours savoir (ou pas) sur le fonctionnement du mail ou courriel.
Je n'ai parcouru qu'une partie de ce long document (article ?), c'est très riche et bien documenté !

Via [Arthur](https://www.arthurperret.fr/veille/2021-05-12-tout-sur-email.html).
