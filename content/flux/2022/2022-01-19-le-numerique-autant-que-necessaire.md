---
title: "Le numérique autant que nécessaire"
date: 2022-01-19
description: "Cette phrase résonne en moi depuis sa lecture il y a déjà presque un an."
categorie:
- flux
tags:
- design
---
> In our eyes, better software is software that gets smaller over time, that sheds the superfluous, and that reaches further backward in time for that onto which it can run. SourceHut appealed to us instantly because there are so few examples of this willingness to reduce consumption in the wild.  
> Devine Lu Linvega, An interview with 100 rabbits, [https://sourcehut.org/blog/2021-12-08-100-rabbits-interview/](https://sourcehut.org/blog/2021-12-08-100-rabbits-interview/)

[sourcehut](https://sourcehut.org/) est une forge (un ensemble d'outils comme un gestionnaire Git, un système de tickets ou une mailing list) minimaliste, une alternative radicale aux plateformes comme GitHub ou GitLab.
[100 rabbits](https://100r.co) est un collectif de deux artistes qui travaillent avec le numérique dans un esprit similaire de _low-tech_.
Cet entretien est intéressant car il met en lumière des façons nouvelles de produire des objets (numériques ou non), de concevoir des créations artistiques, et même de vivre.
La place de la technologie n'est pas centrale, mais il y a une véritable synergie entre des envies ou des besoins et l'utilisation de l'informatique ou du numérique.

> […] there’s a general trend in software right now to compete for attention and skew people’s behavior to act in favor of large ecosystems. GitHub is heavily afflicted by that sickness. SourceHut, less so.