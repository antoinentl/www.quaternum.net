---
title: "Arthur Attwell et Adam Hyde parlent d'édition"
date: 2022-05-10
description: "Arthur Attwell et Adam Hyde sont deux figures emblématiques de l'édition numérique (ou disons de l'édition qui se fait avec le numérique)."
categorie:
- flux
tags:
- édition
---
> I should't try to rescue publishing. […] I don't want to _disrupt_ publishing, I hate that word, I'm not interesting to breaking other people businesses  of other people. I have an interesting in building new things […].  
> Arthur Attwell, Building Open Source with Arthur Attwell, [https://openpublishingfest.org/archives.html](https://recordings.openpublishingfest.org/playback/presentation/2.3/4acc4cce8ca3d76c4880fe199b0a69a3307e9b9b-1636530846948)

Arthur Attwell et Adam Hyde sont deux figures emblématiques de l'édition numérique (ou disons de l'édition qui se fait avec le numérique).
Que ce soit [Electric Book Works](https://electricbookworks.com/) ou [Coko](https://coko.foundation/), Arthur et Adam partagent plusieurs points communs en terme d'outils ouverts pour _faire_ des livres, et leur discussion est vraiment passionnante sur ce qui se fait et sur le schéma qu'il reste à faire (pour découvrir de nouvelles façons de faire et de les partager).
Cette entretien était organisé lors de la dernière édition de l'Open Publishing Festival en novembre 2021.
