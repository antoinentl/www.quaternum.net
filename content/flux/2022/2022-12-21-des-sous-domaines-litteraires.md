---
title: Des sous-domaines littéraires
date: 2022-12-21
description: "L'idée est très bonne : construire des sous-domaines (soit un premier mot pour le sous-domaine, un second mot pour le domaine et un troisième mot pour l'extension ou domaine de premier niveau) en extrayant des suites de mots d'œuvres littéraires à partir de la liste des extensions existantes."
categorie:
- flux
tags:
- publication
---
> So I set out to find three-word phrases where the third word is a 4+-letter top-level domain, using as my first source text Moby Dick.  
> Parker Higgins, Public (sub-)domains, [https://parkerhiggins.net/2022/11/public-sub-domains](https://parkerhiggins.net/2022/11/public-sub-domains)

L'idée est très bonne : construire des sous-domaines (soit un premier mot pour le sous-domaine, un second mot pour le domaine et un troisième mot pour l'extension ou domaine de premier niveau) en extrayant des suites de mots d'œuvres littéraires à partir de la liste des extensions existantes.
Les résultats sont étonnants.
