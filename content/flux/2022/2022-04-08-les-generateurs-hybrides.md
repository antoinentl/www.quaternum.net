---
title: "Les générateurs hybrides"
date: 2022-04-08
description: "Dernier billet de la série de Mike Neumegen pour CloudCannon, qui permet de prendre la mesure de la diversité des approches réunies sous l'appellation JAMStack (ou générateurs de site statique)."
categorie:
- flux
tags:
- web
---

> The islands era sees a number of SSGs bringing the advantages of partial hydration to reality.  
> Mike Neumegen, The 'Islands' era, [https://cloudcannon.com/blog/ssg-history-8-islands/](https://cloudcannon.com/blog/ssg-history-8-islands/)

Dernier billet de la série de Mike Neumegen pour CloudCannon, qui permet de prendre la mesure de la diversité des approches réunies sous l'appellation JAMStack (ou générateurs de site statique).
Le concept d'_hydration_, difficilement traduisible en français (hydratation correspond en partie à ce qui consiste en une génération partielle de contenu pour des applications complexes), prend tout son sens avec ces types de générateurs.
Astro est emblématique de ces programmes qui se spécialisent, le fait de disposer de solutions plus adéquates est probablement une bonne nouvelle.
Tordre les outils est une bonne et une mauvaise chose.
