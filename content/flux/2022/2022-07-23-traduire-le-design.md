---
title: "Traduire le design"
date: 2022-07-23
description: "Le champ du design, en francophonie, a besoin de s'approprier des termes utilisés dans d'autres langues."
categorie:
- flux
tags:
- design
---
> Design in Translation part d’un constat partagé par les enseignants-chercheurs qui, dans les universités françaises ou francophones, enseignent le design. Pour ce qui concerne les fondamentaux théoriques du design, l’équivalent des readers anglo-saxons n’existent quasiment pas. Entre les ouvrages jamais traduits en français, traduits mais jamais publiés, publiés en français et épuisés, le champ des lectures accessibles aux étudiants demeure peu étendu.  
> L’idée a donc germé de fonder un réseau de collègues susceptibles de traduire des textes et de les réunir dans des anthologies libres d'accès.  
> Design in Translation, [https://dit.dampress.org/project/scientific](https://dit.dampress.org/project/scientific)

Le champ du design, en francophonie, a besoin de s'approprier des termes utilisés dans d'autres langues.
Le manque de textes théoriques _en français_ (même si de plus en plus d'écrits de qualité sont publiés), donne à cette initiative tout son intérêt, et permet d'avoir quelques propositions synthétiques pour rebondir.
