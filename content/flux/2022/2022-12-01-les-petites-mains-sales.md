---
title: "Les (petites) mains sales"
date: 2022-12-01
description: "Margot travaille depuis plusieurs mois sur la question des petites mains, ici il est question de se salir les mains, avec de l'encre ou du code, notamment."
categorie:
- flux
tags:
- recherche
---
> Et puis les tables du savoir, c’est toujours un peu en pagaille, un peu en foutoir, un peu sale.  
> Margot Mellet, Dirty Little Hands, [https://blank.blue/meditions/dirty-little-hands/](https://blank.blue/meditions/dirty-little-hands/)

Margot travaille depuis plusieurs mois sur la question [des petites mains](https://blank.blue/meditions/manifeste-des-petites-mains/), ici il est question de se salir les mains, avec de l'encre ou du code, notamment.
