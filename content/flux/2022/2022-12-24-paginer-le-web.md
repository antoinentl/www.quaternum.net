---
title: Paginer le Web
date: 2022-12-24
description: "Voilà un article très enthousiaste et synthétique sur les possibilités du Web pour imprimer des documents paginés !"
categorie:
- flux
tags:
- publication
---
> En créant des documents avec HTML/CSS, vous bénéficiez de toutes ces choses spécifiques aux documents paginés déjà prévues par CSS, mais vous avez aussi à votre disposition toute la puissance du CSS que vous utilisez déjà pour vos sites !  
> Lucie Anglade et Guillaume Ayoub, Mettre le web en page(s), [https://www.24joursdeweb.fr/2022/mettre-le-web-en-pages/](https://www.24joursdeweb.fr/2022/mettre-le-web-en-pages/)

Voilà un article très enthousiaste et synthétique sur les possibilités du Web pour imprimer des documents paginés !
C'est intéressant de voir ce type de texte du côté de la communauté du Web, et plus uniquement dans les blogs personnels ou du côté du design graphique.
Le _Web to print_ prend de l'ampleur et c'est une bonne chose !
