---
title: Liquider l'informatique
date: 2022-12-11
description: "Remettre en cause un outil de travail comme l'informatique est une nécessité, et les actions du CLODO des années 1980 résonnent encore fortement aujourd'hui (à condition de les connaître)."
categorie:
- flux
tags:
- informatique
---
> À ce titre, ne pas oublier le CLODO [Comité pour la Liquidation ou le Détournement des Ordinateurs], c’est ne pas ignorer les profonds basculements de l’informatisation, processus passé de machines localisées dans les années 80 à des ordinateurs en réseau, infrastructure permanente de toute une série d’actions et d’activités.  
> lundimatin, Machines en flammes, [https://lundi.am/Machines-en-flammes](https://lundi.am/Machines-en-flammes)

Remettre en cause un outil de travail comme l'informatique est une nécessité, et les actions du CLODO des années 1980 résonnent encore fortement aujourd'hui (à condition de les connaître).
Liquider ou détourner l'informatique ?
