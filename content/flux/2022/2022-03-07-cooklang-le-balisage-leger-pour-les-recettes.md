---
title: "Cooklang : le balisage léger pour les recettes"
date: 2022-03-07
description: "Un langage de balisage léger pour écrire des recettes de cuisine ?"
categorie:
- flux
tags:
- écriture
---
> Cooklang is like markdown for recipes. It lets you write recipes in a human-readable format that a computer can parse to get the ingredient list, steps, etc.  
> Brian Sunter, Cooklang - Managing Recipes in Git, [https://briansunter.com/blog/cooklang/](https://briansunter.com/blog/cooklang/)

Un langage de balisage léger pour écrire des recettes de cuisine ?
C'est l'idée de [Cooklang](https://cooklang.org/), et l'idée est très bonne.
En plus d'une syntaxe qui permet de structurer les contenus et d'indiquer des quantités, Cooklang est un convertisseur permettant de passer des étapes d'une recette à liste des ingrédients ou une liste de courses (à partir de plusieurs recettes).
Des usages similaires pourraient être imaginés dans d'autres domaines.
