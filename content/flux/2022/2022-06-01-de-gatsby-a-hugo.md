---
title: De Gatsby à Hugo
date: 2022-06-01
categorie:
- flux
tags:
- outils
---
> When comparing frameworks, the most significant difference between Hugo and Gatsby – _from a user’s standpoint_ – is the allowable contents of the Markdown files themselves. For example, Gatsby makes heavy use of MDX, allowing developers to author and import React components within their content pages. While this can be effective, MDX unfortunately is not CommonMark-compliant and, in turn, this means that its flavor of Markdown is _required_ to be very flexible and permissive. This posed a problem when migrating to any other non-MDX-based solution, including Hugo, as these frameworks don’t grant the same flexibilities with Markdown syntax. Because of this, the largest technical challenge was converting the existing 1,600 markdown pages from MDX to a stricter, more standard Markdown variant that Hugo (or almost any framework) can interpret.  
>  Kristian Freeman et Luke Edwards, We rebuilt Cloudflare's developer documentation - here's what we learned, [https://blog.cloudflare.com/new-dev-docs/](https://blog.cloudflare.com/new-dev-docs/)

D'habitude les histoires de changement d'outils pour la production de documentation sont loin d'être passionnantes (se limitant souvent à des questions de performance).
Ici un des points de bascule concerne la standardisation du balisage, et le choix est fait d'avoir un langage de balisage (léger) moins extensible mais plus pérenne dans le temps.
Le changement est fait dans l'idée de peut-être à nouveau changer de générateur de site statique, d'anticiper des migrations.
