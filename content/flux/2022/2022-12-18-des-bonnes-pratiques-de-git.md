---
title: Des bonnes pratiques de Git
date: 2022-12-18
description: "Ce qu'il faut faire pour ne pas se perdre dans Git et l'utiliser de façon efficiente, avec quelques exemples utiles."
categorie:
- flux
tags:
- outils
---
> Don’t panic  
> Seth Robertson, Commit Often, Perfect Later, Publish Once: Git Best Practices, [https://sethrobertson.github.io/GitBestPractices/](https://sethrobertson.github.io/GitBestPractices/)

Ce qu'il faut faire pour ne pas se perdre dans Git et l'utiliser de façon efficiente, avec quelques exemples utiles.
