---
title: "La culture du Web"
date: 2022-03-24
description: "La dimension ouverte du Web façonne nos pratiques d'écriture et de publication."
categorie:
- flux
tags:
- web
---
> This principle of open access to information for everyone is part of what we mean when we say something is "of the web".  
> Jim Nielsen, What is the Web?, [https://blog.jim-nielsen.com/2022/what-is-the-web/](https://blog.jim-nielsen.com/2022/what-is-the-web/)

La dimension ouverte du Web façonne nos pratiques d'écriture et de publication.
Je me rends compte à quel point je suis impregné de cette culture.
