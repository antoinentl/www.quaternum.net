---
title: "Un lecteur de dispositifs"
date: 2022-01-13
description: "Cette phrase résonne en moi depuis sa lecture il y a déjà presque un an."
categorie:
- flux
tags:
- recherche
---
> En conclusion : je suis un lecteur de dispositifs, je cherche à lire les programmes et les sous-textes qui conditionnent un texte donné.  
> Louis-Olivier Brassard, Autobiographie de lecteur (2021), [https://journal.loupbrun.ca/e/lecteur/](https://journal.loupbrun.ca/e/lecteur/)

Cette phrase résonne en moi depuis sa lecture il y a déjà presque un an.
Il est parfois bon de ne pas se sentir seul·e dans cette lecture des dispositifs.