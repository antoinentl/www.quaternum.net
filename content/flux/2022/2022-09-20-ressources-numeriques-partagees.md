---
title: "Ressources numériques partagées"
date: 2022-09-20
description: "David partage ses ressources concernant des langages informatiques comme Python, JavaScript, Shell, HTML ou CSS, qui ont toutes en commun d'être des outils pour créer des choses (je ne vois pas comment le dire autrement)."
categorie:
- flux
tags:
- outils
---
> J’avais envie de partager des explorations techniques et des bouts de code que je diffusais auparavant via des gists sur Microsoft Github.  
> David Larlet, Code.larlet.fr, [https://code.larlet.fr](https://code.larlet.fr/)

David partage ses ressources concernant des langages informatiques comme Python, JavaScript, Shell, HTML ou CSS, qui ont toutes en commun d'être des outils pour créer des choses (je ne vois pas comment le dire autrement).
Cet espace personnel — puisqu'il s'agit des trouvailles de David rassemblées dans cet espace d'abord pour lui-même — est très utile pour qui s'intéresse un peu au Web et souhaite gagner un peu de temps (et apprendre beaucoup).
À noter que cette initiative peut être vue comme une démarche d'écriture et de publication qui s'émancipe des grandes plateformes comme GitHub, il est toujours réjouissant de voir apparaître des îlots comme celui-ci.
