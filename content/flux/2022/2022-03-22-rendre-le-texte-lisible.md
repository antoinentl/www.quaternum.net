---
title: "Rendre le texte lisible"
date: 2022-03-22
description: "Cet article présente plusieurs façons de rendre des textes plus lisibles et plus compréhensibles, avec une approche très pédagogique et (je trouve) très convaincante."
categorie:
- flux
tags:
- design
---
> Writing text that can be understood by as many people as possible seems like an obvious best practice. But from news media to legal guidance to academic research, the way we write often creates barriers to who can read it. Plain language—a style of writing that uses simplified sentences, everyday vocabulary, and clear structure—aims to remove those barriers.  
> Rebecca Monteleone, Jamie Brew et Michelle McGhee, What makes writing more readable? [https://pudding.cool/2022/02/plain/](https://pudding.cool/2022/02/plain/)

Cet article présente plusieurs façons de rendre des textes plus lisibles et plus compréhensibles, avec une approche très pédagogique et (je trouve) très convaincante.
La question est de déterminer comment simplifier des tournures de phrase pour exprimer le même sens, rendre un texte plus accessible sans faire de compromis sur le message délivré.
L'expression _plain language_ pour décrire ces textes _simplifiés_ rappelle le _plain text_ : le format texte qui est une autre forme de simplification.
Il est intéressant de noter que dans cet article deux versions sont proposées, et que la version _plain language_ utilise une fonte _monospace_ qui rappelle le code informatique ou le format texte.

Via [David](https://mastodon.social/@dav/107910307482955777)
