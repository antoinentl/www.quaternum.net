---
title: "Un site web géré avec un fichier binaire"
date: 2022-04-25
description: "L'idée est aussi intéressante que radicale : ne plus dépendre de processus complexes ou comportant trop de dépendances, créer un fichier binaire pour gérer un site web complet."
categorie:
- flux
tags:
- publication
---
> one afternoon, i was browsing the internet neurotically - this is typical of me. i had a sudden idea: if i wrote my website entirely in one language, i could contain and deploy it within a single binary.  
> Jes Olson, my website is one binary, [https://j3s.sh/thought/my-website-is-one-binary.html](https://j3s.sh/thought/my-website-is-one-binary.html)

L'idée est aussi intéressante que radicale : ne plus dépendre de processus complexes ou comportant trop de dépendances, créer un fichier binaire pour gérer un site web complet.
D'autres ont adoptés cette pratique, préférant de loin leurs propres contraintes à celles d'outils qui ne conviennent jamais tout à fait.
Le code est, a priori, dans [ce fichier](https://git.j3s.sh/j3s.sh/tree/main/main.go).
