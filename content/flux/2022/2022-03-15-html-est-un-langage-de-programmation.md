---
title: "HTML est un langage de programmation"
date: 2022-03-15
description: "Superbe vidéo de Heydon Pickering qui démontre avec succès que oui, et n'en déplaise à certain·e·s développeurs·euses, HTML est bien un langage de programmation."
categorie:
- flux
tags:
- web
---
> HTML is a declarative language  
> HTML is a domain specific language  
> HTML is a markup language  
> HTML is a Turing incomplete language  
> HTML is _not_ a natural language  
> HTML is programming language  
> Heydon Pickering, Is HTML A Programming Language?, [https://briefs.video/videos/is-html-a-programming-language/](https://briefs.video/videos/is-html-a-programming-language/)

Superbe vidéo de Heydon Pickering qui démontre avec succès que oui, et n'en déplaise à certain·e·s développeurs·euses, HTML est bien un langage de programmation.
À travers cette argumentation, c'est aussi une déconstruction nécessaire des a priori autour du Web, et des critiques actuelles qui visent à promouvoir (notamment) JavaScript plutôt que CSS.
