---
title: "Un mémoire en web2print"
date: 2022-07-10
description: "L'initiative est audacieuse : proposer (imposer ?) un modèle de document qui ne repose pas sur des outils classiques et qui prend d'abord la forme d'une page web, le tout dans une école d'art et de design."
categorie:
- flux
tags:
- publication
---
> PageTypeToPrint est un gabarit destiné à la mise en forme normalisée d’un document écrit de DNA ou d’un mémoire de DNSEP. Il est conçu avec comme hypothèse principale la simplicité de l’édition (contenu textuel au format _markdown_), mais peut être adapté, augmenté et personnalisé.  
> PageTypeToPrint, [https://github.com/esadpyrenees/PageTypeToPrint/](https://github.com/esadpyrenees/PageTypeToPrint/)

L'initiative est audacieuse : proposer (imposer ?) un modèle de document qui ne repose pas sur des outils classiques et qui prend d'abord la forme d'une page web, le tout dans une école d'art et de design.
Les scripts de transformation sont en PHP, avec, semble-t-il, peu de dépendances.
La documentation est courte, les instructions claires, cela devrait donner quelques idées à d'autres.

Via [Julien Bidoret](https://post.lurk.org/@julienbidoret/108491351924503015)
