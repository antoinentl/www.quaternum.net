---
title: Créer une publication à partir de tickets
date: 2022-12-30
description: "Le processus de publication adopté par l'équipe du HN Lab (une composante d'Huma-Num) est très intéressant : des tickets (issues dans GitLab) sont créés, un travail éditorial de modification et de révision est réalisé sur ces tickets, pour enfin extraire certaines informations et les rassembler dans un document classique."
categorie:
- flux
tags:
- publication
---
> Le CCTP du programme HNSO est un document qui retranscrit à ce jour 37 nouvelles fonctionnalités à implémenter dans les plateformes ISIDORE et NAKALA à partir des issues créées dans le GitLab.  
> Mélanie Bunel, Nicolas Sauret, Stéphane Pouyllau, Les nouvelles fonctionnalités de ISIDORE et NAKALA, [https://hnlab.huma-num.fr/blog/2022/03/15/hnso-cctp/](https://hnlab.huma-num.fr/blog/2022/03/15/hnso-cctp/)

Le processus de publication adopté par l'équipe du HN Lab (une composante d'Huma-Num) est très intéressant : des tickets (issues dans GitLab) sont créés, un travail éditorial de modification et de révision est réalisé sur ces tickets, pour enfin extraire certaines informations et les rassembler dans un document classique.
Belle démonstration qu'il est possible de publier à partir de n'importe quelles données structurées.
