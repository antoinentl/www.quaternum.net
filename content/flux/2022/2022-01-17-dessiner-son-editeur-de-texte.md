---
title: "Dessiner son éditeur de texte"
date: 2022-01-17
description: "Cette phrase résonne en moi depuis sa lecture il y a déjà presque un an."
categorie:
- flux
tags:
- écriture
---
> Pour Nicolas, bien peu de choses semblent avoir changé dans le design des éditeurs de code depuis l'invention d'Emacs, d'où l'envie de réinventer, dynamiser (voire dynamiter) leur interface. La dimension esthétique est importante dans cette démarche : si Nicolas se réfère au paradigme du livre, ce n'est pas uniquement sur la base de critères rationnels mais aussi par affection et par sensibilité.  
> Arthur Perret et Nicolas Rougier, Emacs / N Λ N O : Le design des éditeurs de texte, [https://inacheve-dimprimer.net/articles/2021-05-27-nicolas-rougier.html](https://inacheve-dimprimer.net/articles/2021-05-27-nicolas-rougier.html)

La démarche de façonnage d'un éditeur de texte est une activité trop souvent ignorée voire moquée, avec l'argument selon lequel il faut _écrire_ plutôt que régler son dispositif d'écriture : Nicolas Rougier nous convainc qu'écrire c'est aussi paramétrer les outils d'inscription, de structuration et de publication.