---
title: Aide-mémoire pour Vim
date: 2022-12-27
description: "Parmi les nombreux aide-mémoires concernant l'éditeur de texte Vim, je trouve celui-ci bien fait, complet (pour mes usages) et surtout très lisibles."
categorie:
- flux
tags:
- outils
---
> Vim is a very efficient text editor.  
> Rico Sta. Cruz, Vim cheatsheet, [https://devhints.io/vim](https://devhints.io/vim)

Parmi les nombreux aide-mémoires concernant l'éditeur de texte Vim, je trouve celui-ci bien fait, complet (pour mes usages) et surtout très lisibles.
