---
title: "Contre Markdown"
date: 2022-02-19
description: "Knut Melvær propose une critique de Markdown très argumentée, en tentant de faire l'historique de ce langage de balisage léger emblématique et en pointant les limites intrinsèques de cette façon de structurer le texte."
categorie:
- flux
tags:
- publication
---
> What’s common for people like me who challenge the prevalence of markdown, and those who are really into the simple way of expressing text formating is an appreciation of how we transcribe intent into code. That’s where I think we can all meet. But I do think it’s time to look at the landscape and the emerging content formats that try to encompass modern needs, and ask how we can make sure that we build something that truly caters to editorial experience, and that can speak to developer experience as well.  
> Knut Melvær, Thoughts On Markdown, [https://www.smashingmagazine.com/2022/02/thoughts-on-markdown/](https://www.smashingmagazine.com/2022/02/thoughts-on-markdown/)

Knut Melvær propose une critique de Markdown très argumentée, en tentant de faire l'historique de ce langage de balisage léger emblématique et en pointant les limites intrinsèques de cette façon de structurer le texte.
Si les parties sur la contextualisation ou sur les besoins actuels des initiatives de publication sont intéressantes, je reste un peu dubitatif sur les quelques pistes envisagées.
Entre base de données simplifiée et détournement d'un langage de sérialisation pour gérer des modèles de données — ici du texte —, je ne suis pas certain que ces perspectives répondent aux contraintes du texte.
Par ailleurs il est dommage qu'[AsciiDoc](https://gitlab.eclipse.org/eclipse/asciidoc-lang/asciidoc-lang) ne soit pas évoqué — langage de balisage léger bien plus expressif que Markdown, et en voie de standardisation —, de même que la force d'une saveur de Markdown comme [Pandoc's Markdown](https://pandoc.org/MANUAL.html#pandocs-markdown) ou encore des possibilités d'extensions dans des générateurs de site statique comme [Hugo](https://gohugo.io/content-management/shortcodes/) ou Jekyll.
