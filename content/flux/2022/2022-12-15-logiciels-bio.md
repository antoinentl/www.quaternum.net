---
title: Logiciels bio
date: 2022-12-15
description: "Et si certains logiciels étaient développés en respectant des principes qui n'en feraient pas des machines à argent ?"
categorie:
- flux
tags:
- design
---
> So maybe we should have ‘organic’ software as well, made by companies that:
>
> 1. Are not funded in such a way where the primary obligation of the company is to 🎡 chase funding rounds or get acquired (so bootstrapping, crowdfunding, grants, and angel investment are okay)
> 2. Have a clear pricing page
> 3. Disclose their sources of funding and sources of revenue
>
> Pirijan, In Search of Organic Software, [https://pketh.org/organic-software.html](https://pketh.org/organic-software.html)

Et si certains logiciels étaient développés en respectant des principes qui n'en feraient pas des machines à argent ?
C'est un peu un _bon capitalisme_ que définit ici Pirijan (le créateur de [Kinopio](https://kinopio.club/)), avec des idées tout de même intéressantes qui permettent de remettre le contexte de développement de la majorité des logiciels et applications numériques.

