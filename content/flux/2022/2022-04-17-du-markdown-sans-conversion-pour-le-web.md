---
title: "Du Markdown sans conversion pour le Web"
date: 2022-04-17
description: "Lea Verou a écrit un script JavaScript pour pouvoir convertir du Markdown directement dans le navigateur, à la volée."
categorie:
- flux
tags:
- publication
---
> Render styleable Markdown in your HTML.  
> Lea Verou, md-block: Render styleable Markdown in your HTML, [https://md-block.verou.me](https://md-block.verou.me)

Lea Verou a écrit un script JavaScript pour pouvoir convertir du Markdown directement dans le navigateur, _à la volée_.
D'autres scripts existent mais celui-ci a quelques fonctionnalités intéressantes.
