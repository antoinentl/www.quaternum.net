---
title: Les codes de l'invisible
date: 2022-12-16
description: "C'est une idée géniale de lister tous les caractères Unicode qui permettent d'exprimer une espace invisible !"
categorie:
- flux
tags:
- typographie
---
> In Unicode there are a lot of invisible characters: regular white-space characters (e.g. U+0020 SPACE), language specific fillers (e.g. U+3164 HANGUL FILLER of the Korean Hangual alphabet), or special characters (e.g. U+2800 BRAILLE PATTERN BLANK). While all of these have a specific meaning in their natural context, they can be used in various applications that don't allow for regular whitespace characters.  
> Florian Pigorsch, Unicode characters you can not see, [https://invisible-characters.com/](https://invisible-characters.com/)

C'est une idée géniale de lister tous les caractères Unicode qui permettent d'exprimer une espace invisible !
Florian Pigorsch propose une cinquantaine de façons différentes de _coder_ une espace invisible.
