---
title: "LaTeX : un outil de composition, pas un outil d'écriture"
date: 2022-01-05
description: "La question revient régulièrement, à savoir s'il _faut_ écrire avec LaTeX ou ne l'utiliser que comme un processeur PDF."
categorie:
- flux
tags:
- publication
---
> What I call the ‘LaTeX fetish’ is the conviction that there is something about LaTeX that makes it good for writing in. As we shall see, arguments in favour of writing in LaTeX are unpersuasive on a rational level: LaTeX is in fact quite bad for writing in (although it could be worse, i.e. it could be TeX). This doesn’t mean that people shouldn’t use LaTeX at all, but it does mean that people probably ought to stop recommending it as a writing tool.  
[…]  
> LaTeX was invented so that nobody would have to write prose in TeX, which is too hard for ordinary mortals. The above were created so that nobody would have to write prose in LaTeX – which is not too hard for ordinary mortals, but still a fairly bad idea.  
> Daniel Allington, The LaTeX fetish (Or: Don’t write in LaTeX! It’s just for typesetting), [http://www.danielallington.net/2016/09/the-latex-fetish/](http://www.danielallington.net/2016/09/the-latex-fetish/)

Daniel Allington fait une longue critique de LaTeX, qui pourrait être résumé par le fait que LaTeX est un outil qui ne correspond pas à toutes les situations ou tous les usages.
Il tente ainsi de préciser (longuement) dans quels cadres son utilisation est la plus pertinente (peut-être générer des documents lisibles).
Ainsi en tant qu'outil d'écriture LaTeX présente des défauts importants, comme le fait que le balisage est trop complexe, que la mise en forme de l'export PDF par défaut de LaTeX ne correspond pas à tous les usages, ou encore que, même en l'utilisant comme outil de composition, il requiert un niveau technique trop important.
Si Daniel Allington argumente le fait que l'alternative à une utilisation de LaTeX serait de former la communauté académique à des traitements de texte comme Word (ce point est probablement une nécessité en soit, mais je suis dubitatif sur cette position), il propose tout de même quelques solutions intermédiaires avec des langages de balisage plus _humains_ et des convertisseurs au milieu.

J'ai du mal à comprendre les critiques répétées concernant le balisage complexe de LaTeX tout en promouvant les traitements de texte : Daniel Allington admet que la séparation fond-forme permise par un balisage est une très bonne chose en soit, mais il ne mentionne que très peu d'autres balisages plus accessibles et lisibles.

Enfin, le fait de passer du temps sur les réglages et la résolution de problèmes de structuration voir de mise en forme ne doit pas être vu comme un problème.
L'écriture ne peut pas être réduite à _taper du texte_ ou à sélectionner des styles avec des boutons, il y a d'autres processus qui interviennent, et l'auteur du billet semble les laisser de côté en plébiscitant une pseudo-facilité.

Au passage Daniel fait d'autres remarques pertinentes, comme le fait que la mise en forme PDF par défaut de LaTeX est une marque reconnaissable, et même un gage de qualité (on sent un certain sarcasme de l'auteur et je suis bien d'accord avec lui).

Via [Arthur Perret](https://www.arthurperret.fr/blog/2020-05-22-ecrire-et-editer.html).