---
title: Zotero ou rien
date: 2022-12-14
description: "Zotero est un puissant outil de gestion de références bibliographiques, Johanna Daniel le prouve ici avec une pratique universitaire qui mêle dispositif documentaire, outil de lecture et de prise de note, et espace de travail pour la préparation d'articles ou de cours."
categorie:
- flux
tags:
- outils
---
> Alors que, le plus souvent, on présente l’utilité de Zotero à court terme (faciliter la mise en place des notes de bas de page et de la bibliographie lors de la rédaction d’un travail universitaire ou d’une publication scientifique), j’aimerais donner à voir le bénéfice que l’on peut en tirer sur une plus longue période (une décennie, une carrière peut-être ?), dès lors que l’on façonne une stratégie documentaire et que l’on y centralise ses pratiques de lecture.  
> Johanna Daniel, Ode à Zotero, une décennie d’usage de Zotero, [https://ig.hypotheses.org/2789](https://ig.hypotheses.org/2789)

Zotero est un puissant outil de gestion de références bibliographiques, Johanna Daniel le prouve ici avec une pratique universitaire qui mêle dispositif documentaire, outil de lecture et de prise de note, et espace de travail pour la préparation d'articles ou de cours.
Si vous n'étiez pas encore convaincu·e, foncez utiliser Zotero !
(D'ailleurs si vous ne savez pas comment vous y prendre, [contactez-moi](/a-propos/#contact).)
