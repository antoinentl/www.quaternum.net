---
title: L'architecture en îlots
date: 2022-12-09
description: "Il faut reconnaître qu'il est parfois difficile de comprendre les nombreux concepts (marketing) qui émergent dans le développement web, après l'hydration c'est donc l'architecture en îlots."
categorie:
- flux
tags:
- web
---
> The general idea of an “Islands” architecture is deceptively simple: render HTML pages on the server, and inject placeholders or slots around highly dynamic regions. These placeholders/slots contain the server-rendered HTML output from their corresponding widget. They denote regions that can then be "hydrated" on the client into small self-contained widgets, reusing their server-rendered initial HTML.  
> Jason Miller, Islands Architecture, [https://jasonformat.com/islands-architecture/](https://jasonformat.com/islands-architecture/)

Il faut reconnaître qu'il est parfois difficile de comprendre les nombreux concepts (marketing) qui émergent dans le développement web, après l'_hydration_ c'est donc l'architecture en îlots.
Ici l'idée est de considérer la page web comme un ensemble d'éléments indépendants dont certains sont servis de façon _statique_ et d'autres sont modifiés selon le comportement de l'utilisateur·trice.
Pas sûr de comprendre toute la subtilité de ce concept, mais je garde ça ici pour y revenir plus tard si nécessaire.
