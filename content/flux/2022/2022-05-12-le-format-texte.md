---
title: "Le format texte"
date: 2022-05-12
description: "Arthur Perret fait ici un travail très précis et très synthétique de description et d'explication de ce qu'est le format texte, ou plein texte ou texte brut."
categorie:
- flux
tags:
- publication
---
> Cette page explique ce qu’est le format texte et donne des arguments en faveur de son utilisation. Elle ouvre vers d’autres ressources pour explorer l’écosystème du format texte.  
> Arthur Perret, Format texte, [https://www.arthurperret.fr/cours/format-texte.html](https://www.arthurperret.fr/cours/format-texte.html)

Arthur Perret fait ici un travail très précis et très synthétique de description et d'explication de ce qu'est le format texte, ou plein texte ou texte brut.
En plus d'être d'utilité publique, cette initiative démontre l'importance d'expliquer ce qu'est ce (fameux) format texte.

À noter que d'[autres cours sont disponibles](https://www.arthurperret.fr/cours/), avec une même rigueur ainsi qu'une écriture limpide et synthétique.
