---
title: "Forgejo : un fork libre de Gitea"
date: 2022-12-19
aliases:
- /2022/12/26/forgejo-un-fork-libre-de-gitea/
description: "La communauté du logiciel libre, et plus particulièrement les personnes qui s'intéressent aux logiciels de versionnement, a été quelque peu secouée par les annonces des créateurs de Gitea en octobre 2022, qui ont décidé de transférer les noms de domaine et la marque a une structure à but lucratif."
categorie:
- flux
tags:
- outils
---
> The Forgejo project, launched today, is hosted on Codeberg and offers a new home to reunite the Gitea community.  
> Codeberg, Codeberg launches Forgejo, [https://blog.codeberg.org/codeberg-launches-forgejo.html](https://blog.codeberg.org/codeberg-launches-forgejo.html)

La communauté du logiciel libre, et plus particulièrement les personnes qui s'intéressent aux logiciels de versionnement, a été quelque peu secouée par les annonces des créateurs de Gitea en octobre 2022, qui ont décidé de transférer les noms de domaine et la _marque_ a une structure à but lucratif.
La communauté autour de Gitea [a réagi](https://gitea-open-letter.coding.social/) rapidement, et Codeberg a décidé de créer un nouveau logiciel, un fork de Gitea.
Cet événement est très intéressant sur la capacité d'un groupe a réagir et a s'autogérer en quelque sorte.
Désormais le projet [Forgejo](https://forgejo.org/) est donc désormais une version plus libre, dans la philosophie globale, que Gitea.
