---
title: Des pratiques de code pour du design graphique
date: 2022-12-22
description: "Julie Blanc et Nolwenn Maudet proposent un panorama de l'usage de la programmation pour faire du design graphique, dix ans après un premier article sur les relations entre code et design."
categorie:
- flux
tags:
- design
---
> Ces nouvelles pratiques trouvent un écho de plus en plus fort auprès des designers graphiques concernés par la relation à leurs outils et leurs valeurs culturelles intrinsèques. À partir d’un engouement pour des technologies libres et open source, ce sont majoritairement les possibilités d’expérimentation graphiques, multimédias et alternatives qui sont explorées.  
> Julie Blanc et Nolwenn Maudet, Code <–> Design graphique: Dix ans de relations, [https://www.cnap.fr/actualites/graphisme-en-france/revues/ndeg28-graphisme-en-france-creation-outils-recherche-2022](https://www.cnap.fr/actualites/graphisme-en-france/revues/ndeg28-graphisme-en-france-creation-outils-recherche-2022)

Julie Blanc et Nolwenn Maudet proposent un panorama de l'usage de la programmation pour faire du design graphique, dix ans après un premier article sur les relations entre code et design.
Les exemples sont nombreux et les constats précis, avec de nombreuses références qu'il faut aller lire pour prolonger ce formidable travail.

> L’autonomie de production repose donc sur la création de programmes additionnables simples fabriqués avec des langages et des technologies ouvertes facilement interpolables.

> Cette conception de la mise en forme graphique qui oblige à penser en dehors des limites du format de la page a transformé profondément les pratiques récentes, mais dans le même temps renoue avec des manières de travailler la mise en forme antérieures à l’apparition des logiciels de PAO, où les designers graphiques ne pouvaient travailler qu’à partir de gabarits et de feuilles de style.

