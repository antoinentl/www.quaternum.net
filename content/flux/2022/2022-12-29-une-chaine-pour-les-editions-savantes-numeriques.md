---
title: Une chaîne pour les éditions savantes numériques
date: 2022-12-29
description: "Je mets de côté ce dépôt GitHub qui rassemble les outils nécessaires à la production d'une édition numérique, avec entre autres des phases de numérisation, de structuration, d'encodage et de publication."
categorie:
- flux
tags:
- publication
---
> […] the pipeline aims at reaching the following goal: facilitate the digitization of data extracted from archival collections, and their dissemination to the public in the form of digital documents in various formats and/or as an online edition.  
> Digital Scholarly Editions, Pipeline for digital scholarly editions, [https://github.com/DiScholEd/pipeline-digital-scholarly-editions](https://github.com/DiScholEd/pipeline-digital-scholarly-editions)

Je mets de côté ce dépôt GitHub qui rassemble les outils nécessaires à la production d'une édition numérique, avec entre autres des phases de numérisation, de structuration, d'encodage et de publication.
