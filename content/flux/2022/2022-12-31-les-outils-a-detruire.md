---
title: Les outils à détruire
date: 2022-12-31
description: "Ne nous concentrons pas trop sur le maintien de certains outils, au risque de nous enfermer dedans."
categorie:
- flux
tags:
- outils
---
> Plutôt que de vénérer les portes de sortie, les outils, les autorités, il faut tomber en amour avec les pratiques, les processus de recherche, les labyrinthes fabriqués.  
> Margot Mellet, Tool to be Destroyed, [https://blank.blue/fabrique/tool-to-be-destroyed/](https://blank.blue/fabrique/tool-to-be-destroyed/)

Ne nous concentrons pas trop sur le maintien de certains outils, au risque de nous enfermer dedans.
Ce plaidoyer (est-ce un plaidoyer ?) doit nous amener à analyser les outils pour ce qu'ils génèrent (y compris en nous) et moins pour ce qu'ils sont.
