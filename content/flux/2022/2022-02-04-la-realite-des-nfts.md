---
title: "La réalité des NFTs"
date: 2022-02-02
description: "Que ce soit sur tous les blogs estampillés techno, la presse généraliste ou la radio (je déconseille la mauvaise émission de France Culture sur le sujet), les NFTs suscitent un intérêt important."
categorie:
- flux
tags:
- technologie
---
> Les NFT promettent plusieurs choses:
>
>- Déclarer et prouver la propriété d'une œuvre numérique.
>- Système distribué (pas d'autorité centrale)
>- Vente sans intermédiaires.
>
> Je vais vous expliquer pourquoi ces promesses ne sont pas tenues. Aucune d'entre elles.  
> Sebsauvage, Le(s) problème(s) avec les NFT, [https://sebsauvage.net/wiki/doku.php?id=nft](https://sebsauvage.net/wiki/doku.php?id=nft)

Que ce soit sur tous les blogs estampillés "techno", la presse généraliste ou la radio (je déconseille la mauvaise émission de France Culture sur le sujet), les NFTs suscitent un intérêt important.
Entre curiosité saine et opportunisme malsain, heureusement qu'il y a quelques critiques constructivent.
Les arguments de Sebsauvage sont très clairs, et permettent de se faire une bonne idée des NFTs.
