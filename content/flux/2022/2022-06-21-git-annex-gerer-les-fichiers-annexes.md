---
title: "git-annex : gérer les fichiers annexes avec Git"
date: 2022-06-21
categorie:
- flux
tags:
- outils
---
> git-annex allows managing large files with git, without storing the file contents in git. It can sync, backup, and archive your data, offline and online. Checksums and encryption keep your data safe and secure. Bring the power and distributed nature of git to bear on your large files with git-annex.  
> git-annex, [https://git-annex.branchable.com/](https://git-annex.branchable.com/)

Git est pensé pour gérer du texte, et plus spécifiquement des fichiers au format texte, et non des fichiers binaires qui sont parfois très lourds comme des images, des vidéos ou des _dumps_ de bases de données.
Git-LFS est une solution, mais elle ne fait que déplacer le problème (en suivant de façon moins régulière des fichiers lourds).
git-annex semble proposer des options plus poussées, en séparant le suivi et le stockage des fichiers (lourds ou légers).
