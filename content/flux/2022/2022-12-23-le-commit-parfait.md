---
title: Le commit parfait
date: 2022-12-23
description: "Si le titre est probablement un peu exagéré, les méthodes fournies par Simon Willison pour le versionnement du code sont pertinentes et permettent de structurer un travail collectif."
categorie:
- flux
tags:
- outils
---
> For the last few years I’ve been trying to center my work around creating what I consider to be the Perfect Commit. This is a single commit that contains all of the following:
>
>- The implementation: a single, focused change
>- Tests that demonstrate the implementation works
>- Updated documentation reflecting the change
>- A link to an issue thread providing further context
>
> Simon Willison, The Perfect Commit, [https://simonwillison.net/2022/Oct/29/the-perfect-commit/](https://simonwillison.net/2022/Oct/29/the-perfect-commit/)

Si le titre est probablement un peu exagéré, les méthodes fournies par Simon Willison pour le versionnement du code sont pertinentes et permettent de structurer un travail collectif.
Les points évoquées pour écrire le commit parfait peuvent même être dupliquées pour l'écriture ou l'édition, dans le cas d'un versionnement des fichiers source :

- créer des commits atomiques : chaque commit concerne une action qui fait sens, et qui peut être comprise dans un fil temporel ;
- tester chaque modification : difficilement applicable pour l'écriture ou l'édition (à moins d'être très inventif) ;
- documenter : pour comprendre de quoi il s'agit, pour contextualiser ;
- lier chaque commit à une _issue_/ticket : un commit est une tâche qui doit être identifiée avant de la commencer, un ticket permet de décrire clairement cela. Ça peut faire sens aussi pour un projet d'édition ;
- accepter les exceptions : tous les commits ne peuvent pas être parfaits.
