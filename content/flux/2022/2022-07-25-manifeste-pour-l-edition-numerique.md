---
title: "Manifeste pour l'édition numérique"
date: 2022-07-25
description: "Quelle bonne nouvelle que cette initiative de proposer un manifeste pour l'édition numérique !"
categorie:
- flux
tags:
- publication
---
> Digital editions follow a digital paradigm to which all those involved orientate themselves. Digital editions imply a media transformation: They transform a source into a result that not only opens up the source, but also makes it machine-readable and algorithmically processable. The digital edition therefore consists of the combination of data and its processing for presentation and use, it is "more than data".  
> Manifesto for digital editions (version traduite), DHdBlog, [https://dhd-blog.org/?p=17563](https://dhd-blog.org/?p=17563)

Quelle bonne nouvelle que cette initiative de proposer un manifeste pour l'édition numérique !
Au-delà de l'aspect théorique — fixer quelques principes et quelques faits — il y a aussi une recherche de reconnaissance d'une communauté que je trouve très pertinente.
À noter que dans les commentaires il est possible d'atterrir sur un document en cours de relecture et que Michael Kurzmeier également ouvert [un dépôt](https://github.com/mkrzmr/Manifesto-for-digital-editions) avec une première traduction automatique via Deepl (d'où j'ai extrait la citation ci-dessus).
