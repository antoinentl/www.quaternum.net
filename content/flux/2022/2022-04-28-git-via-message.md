---
title: "Git via message"
date: 2022-04-28
description: "Ce tutoriel explique de façon très claire comment contribuer à des projets (versionnés avec Git) sans passer par les fameux _pull requests_ ou _merge requests_ et en utilisant les fonctionnalités de base de Git."
categorie:
- flux
tags:
- outils
---
> Git ships with built-in tools for collaborating over email. With this guide, you'll be contributing to email-driven projects like the Linux kernel, PostgreSQL, or even git itself in no time.  
> [https://git-send-email.io](https://git-send-email.io/)

Ce tutoriel explique de façon très claire comment contribuer à des projets (versionnés avec Git) sans passer par les fameux _pull requests_ ou _merge requests_ et en utilisant les fonctionnalités de base de Git.
Si cela peut sembler très _technique_, c'est en fait très accessible et cela permet de se passer des plateformes très centralisées — centralisées en termes de données et d'usages.
À utiliser avec [ce tutoriel sur Git rebase](https://git-rebase.io/).
