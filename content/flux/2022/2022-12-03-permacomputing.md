---
title: Permacomputing
date: 2022-12-03
description: "L'approche est pour le moins originale, appliquer les principes de la permaculture à l'informatique, ce qui se traduit en plusieurs principes comme le fait de penser des programmes simples et avec peu de dépendances, d'intégrer la flexibilité, de tout garder ouvert ou encore de penser l'intérêt de chaque élément d'un système."
categorie:
- flux
tags:
- design
---
> Permacomputing is a more sustainable approach to computer and network technology inspired by permaculture.  
> Permacomputing, [https://permacomputing.net](https://permacomputing.net)

L'approche est pour le moins originale, appliquer les principes de la permaculture à l'informatique, ce qui se traduit en plusieurs principes comme le fait de penser des programmes simples et avec peu de dépendances, d'intégrer la flexibilité, de tout garder ouvert ou encore de penser l'intérêt de chaque élément d'un système.
L'initiative vient notamment de [Devine Lu Linvega](https://wiki.xxiivv.com/site/home.html], qui fait partie de [Hundredrabbits](https://100r.co).

> Permacomputing asks the question whether it is possible to rethink computing in the same way as permaculture rethinks agriculture.

