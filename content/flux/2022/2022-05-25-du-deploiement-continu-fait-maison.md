---
title: "Du déploiement continu fait maison"
date: 2022-05-25
description: "Le déploiement continu ou l'intégration continue est un processus d'automatisation de tâches pour la vérification du fonctionnement du code, la mise à jour d'une application ou le transfert de données sur un serveur."
categorie:
- flux
tags:
- outils
---
> This is a little demonstration of how little you need to host your own git repositories and have a modest Continuous Integration system for them.  
> Christian Ştefănescu, A tiny CI system, [https://www.0chris.com/tiny-ci-system.html](https://www.0chris.com/tiny-ci-system.html)

Le déploiement continu ou l'intégration continue est un processus d'automatisation de tâches pour la vérification du fonctionnement du code, la mise à jour d'une application ou le transfert de données sur un serveur.
Si la plupart du temps ces processus sont intégrés sur des plateformes de versionnement comme GitHub ou GitLab, ou sur des services dédiés comme Jenkins ou Travis CI, ici Christian Ştefănescu propose de créer son propre service.
Au-delà de l'indépendance, ce sont les questions de la faisabilité technique et de la légèreté de la solution qui sont intéressantes.
