---
title: "Le journal d'Alix"
date: 2022-03-10
description: "La création d'un blog, d'un carnet de recherche, d'un journal, est toujours un moment un peu magique."
categorie:
- flux
tags:
- publication
---
> […] this blog is a dedicated space where a year or two from now, I hope it will be possible to follow the development of my reflexion.  
> Alix, 001 - Motivations, [https://alix-tz.github.io/phd/posts/001/](https://alix-tz.github.io/phd/posts/001/)

La création d'un blog, d'un carnet de recherche, d'un journal, est toujours un moment un peu magique.
Encore un carnet de recherche, mais pas un premier pour Alix Chagué qui tente une expérience sur un espace peut-être plus personnel ou paramétrable que des carnets Hypothèses.
C'est en anglais et ça risque de parler DH, entre autres.
Hâte.
