---
title: "La chaîne de publication comme un système"
date: 2022-01-07
description: "Un nouvel épisode des Mécaniques du livre, toujours aussi passionnant, avec cette fois 45 minutes dédiées au numérique."
categorie:
- flux
tags:
- publication
---
> Dans l'idée d'aller sur du système, on aurait plutôt le contenu, et les différents acteurs qui travaillent dessus, potentiellement en même temps, et à tout moment de ressortir typiquement en PDF pour pouvoir aller chez l'imprimeur, ou en EPUB, en ligne, etc. Cela veut dire qu'on met le contenu au centre, et plus au début avec différents passages.  
> Fabrice Luraine, Les mécaniques du livre : Le numérique, [https://www.editionsducommun.org/blogs/podcasts/saison-2-episode-6-le-numerique](https://www.editionsducommun.org/blogs/podcasts/saison-2-episode-6-le-numerique)

Un nouvel épisode des Mécaniques du livre, toujours aussi passionnant, avec cette fois 45 minutes dédiées au numérique.
Et pour une fois il ne s'agit pas que du _livre numérique_, mais des façons de _faire_, et donc d'écrire, d'éditer, de fabriquer, de produire et de lire.
Fabrice Luraine, le directeur artistique des Éditions du Commun, évoque la notion de système (que j'ai abondamment utilisée dans [mon mémoire](https://memoire.quaternum.net)), et c'est très intéressant d'entendre des gens du livre aborder ce type de concept.