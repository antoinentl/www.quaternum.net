---
title: "Bloguer en SSH"
date: 2022-07-05
description: "prose.sh est une plateforme de blogs accessible en écriture via un terminal et SSH, et donc débarrassée des habituelles interfaces graphiques et autre création de compte complexe."
categorie:
- flux
tags:
- publication
---
> The goal of this blogging platform is to make it simple to use the tools you love to write and publish your thoughts. There is no installation, signup is as easy as SSH'ing into our CMS, and publishing content is as easy as copying files to our server.  
> prose.sh, [https://prose.sh](https://prose.sh/)

prose.sh est une plateforme de blogs accessible en écriture via un terminal et SSH, et donc débarrassée des habituelles interfaces graphiques et autre création de compte complexe.
En deux secondes (littéralement) il est possible de créer un blog et de commencer à écrire.
Certes l'accès via un terminal **et** en SSH n'est pas à la portée de tous le monde (le problème n'est pas le niveau technique requis mais plutôt le premier pas, plus aisé en étant accompagné), mais c'est une initiative rudimentaire dans le bon sens du terme.
Ce serait même un excellent moyen d'expliquer des notions autour du terminal et de SSH.

Via [Louis-Olivier](https://mamot.fr/@loupbrun).
