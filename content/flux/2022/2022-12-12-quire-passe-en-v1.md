---
title: Quire passe en v1
date: 2022-12-12
description: "Quire, la chaîne de publication développée par The Getty pour produire des catalogues, passe en v1, c'est une grande nouvelle pour ce beau projet !"
categorie:
- flux
tags:
- publication
---
> Important differences to note between Quire v0 and v1  
> Quire, [https://quire.getty.edu/docs-v1/key-changes/](https://quire.getty.edu/docs-v1/key-changes/)

Quire, la chaîne de publication développée par The Getty pour produire des catalogues, passe en v1, c'est une grande nouvelle pour ce beau projet !
À noter que c'est l'occasion pour Quire de changer de générateur de site statique : de Hugo à 11ty.
