---
title: "Bring Your Own Client"
date: 2022-05-19
description: "Pour peu que l'on comprenne pas ce que veut dire le mot interopérabilité, alors cette proposition peut sembler presque naïve, pourtant Geoffrey Litt formule un besoin largement partagé et qui devrait être étendu à toute utilisation de l'informatique."
categorie:
- flux
tags:
- outils
---
> It’s delightful to have the freedom to Bring Your Own Client (BYOC): to choose your favorite application to interact with some data.  
> Geoffrey Litt, Bring Your Own Client, [https://www.geoffreylitt.com/2021/03/05/bring-your-own-client.html](https://www.geoffreylitt.com/2021/03/05/bring-your-own-client.html)

Pour peu que l'on comprenne ce que veut dire le mot _interopérabilité_, alors cette proposition peut sembler presque naïve, pourtant Geoffrey Litt formule un besoin largement partagé et qui devrait être étendu à toute utilisation de l'informatique.
Pouvoir utiliser le logiciel de son choix selon le service ou protocole, voilà une condition d'utilisabilité nécessaire aujourd'hui à mon humble avis.
Ce principe peut concerner autant des outils de communications (courriel, messagerie instantanée, etc.) que des processus d'écriture ou d'édition, ou encore des accès à des données.
Les questions posées par Geoffrey Litt sur les conditions de réalisation du BYOC sont pertinentes : compatibilité des formats ou granularité du suivi de versions notamment.
