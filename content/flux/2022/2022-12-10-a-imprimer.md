---
title: À imprimer
date: 2022-12-10
description: "Un corpus et des templates pour comprendre et pratiquer le web to print ou code to print."
categorie:
- flux
tags:
- design
---
> Face à l'hégémonie de quelques logiciels commerciaux, des processus alternatifs soulèvent des enjeux d'ordre à la fois expérimentaux et industriels : l'utilisation des techniques du web (langages html et css) pour produire depuis le navigateur un fichier pdf destiné à l'impression, l'utilisation d'outils libres et open source (FOSS / FLOSS) pour la création graphique, l'utilisation de programmation et de scripts pour mettre en page.  
> 2print, la bibliothèque web to print — préfiguration, [http://2print.org/](http://2print.org/)

Un corpus et des templates pour comprendre et pratiquer le _web to print_ ou _code to print_.
À suivre.
