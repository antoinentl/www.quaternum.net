---
title: "Devenir typographique"
date: 2022-07-02
description: "Chouette initiative qui donne envie de regarder un peu en avant, loin des démarches typographiques très consensuelles et trop pragmatiques."
categorie:
- flux
tags:
- typographie
---
> Il me semble avoir longtemps été circonspect vis-à-vis du décalage apparent entre l’attention donnée au dessin de caractère et la portée théorique qu’il représente. Penser et donner sa forme à l’écriture n’est-il pas un enjeu majeur pour notre civilisation ? Pourtant, force est de constater l’état plutôt passif dans lequel nous baignons. Le dessin de caractère est plutôt considéré comme une pratique et nous nous sommes convaincu que nous ne pouvions que subir et non écrire l’histoire de l’écriture.  
> grifi, Typographia Hermetica, [https://www.grifi.fr/fr/r/typographia-hermetica](https://www.grifi.fr/fr/r/typographia-hermetica)

Chouette initiative qui donne envie de regarder un peu en avant, loin des démarches typographiques très consensuelles et trop pragmatiques.
