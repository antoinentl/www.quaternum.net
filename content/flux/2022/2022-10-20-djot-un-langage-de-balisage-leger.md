---
title: "Djot, un langage de balisage léger"
date: 2022-10-20
description: "John McFarlane, le créateur de Pandoc, propose donc un nouveau langage de balisage léger, Djot, pour dépasser les problèmes d'ambiguïtés de Markdown."
categorie:
- flux
tags:
- publication
---
> Djot is a light markup syntax. It derives most of its features from commonmark, but it fixes a few things that make commonmark's syntax complex and difficult to parse efficiently. It is also much fuller-featured than commonmark, with support for definition lists, footnotes, tables, several new kinds of inline formatting (insert, delete, highlight, superscript, subscript), math, smart punctuation, attributes that can be applied to any element, and generic containers for block-level, inline-level, and raw content.  
> Djot, [https://djot.net/](https://djot.net/)

John McFarlane, le créateur de Pandoc, propose donc un nouveau langage de balisage léger, Djot, pour dépasser les problèmes d'ambiguïtés de Markdown.
Au-delà du [constat habituel du nouveau format/standard](https://xkcd.com/927/), il faut reconnaître que le projet est intéressant.
Surtout aussi parce que Djot n'est pas centré sur HTML, et cette approche permet d'envisager des usages plus larges que le _simple_ web !

Djot est grandement basé sur la spécification CommonMark (l'une des saveurs Markdown la plus utilisée), en ajoutant plusieurs fonctionnalités : plus d'ambiguïtés pour la gestion des emphases, une gestion plus fine des listes et notamment des listes ordonnées, l'intégration de bloques thématiques, une meilleure gestion des tableaux, etc.
Un format de trop ?
Dans la perspective d'une approche avec le convertisseur Pandoc, je ne pense pas.

La création de ce format est une réponse aux constats et aux réflexions de John McFarlane que l'on peut lire dans ce billet : [Beyond Markdown](https://johnmacfarlane.net/beyond-markdown.html).
Djot _existe_ depuis juillet 2022 sur [un dépôt GitHub](https://github.com/jgm/djot) et est déjà intégré à Pandoc via un filtre Lua.
