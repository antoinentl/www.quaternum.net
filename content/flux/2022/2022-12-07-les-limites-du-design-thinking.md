---
title: Les limites du design thinking
date: 2022-12-07
description: "Le concept de design thinking semble avoir encore quelques balbutiements, ce texte de Nick Foster est un plaidoyer pour penser des approches plus complexes qui n'aplatissent pas tous les problèmes."
categorie:
- flux
tags:
- design
---
> The five stage process proposed by Design Thinking has proven itself to be inapplicable to real world problems — ignoring, obfuscating and skipping over huge swathes of important work.  
> Nick Foster, On Design Thinking, [https://scribe.rip/@fosta/on-design-thinking-8426ecf328b3](https://scribe.rip/@fosta/on-design-thinking-8426ecf328b3)

Le concept de _design thinking_ semble avoir encore quelques balbutiements, ce texte de Nick Foster est un plaidoyer pour penser des approches plus complexes qui n'aplatissent pas tous les problèmes.
Je trouve cette réflexion tout à fait pertinente, le mouvement du _design thinking_ ayant laissé pensé que nous pouvions toutes et tous être des designers alors que… non.

Pour finir, la meilleure phrase de l'article :

> If you want good Design work, I suggest hiring Designers.
