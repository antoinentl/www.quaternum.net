---
title: "Setup académique"
date: 2022-01-21
description: "Tomislac Medak décrit l'ensemble des outils qu'il utilise pour faire de la recherche."
categorie:
- flux
tags:
- outils
---
> This document covers digital tools and workflows that I use in my scholarly work, covering a range of actions from digitisation, annotation, referencing, plaintext authorship, storage and backup, to presentation and web presence. It includes workflows based on ScanTailor, OCR tools, Zotero, Diigo, Hypothesis, Markdown, Atom, Pandoc, Git, Reveal.js, reveal-md and Nikola.  
> Tomislav Medak, Digital scholarship workflows, [https://tom.medak.click/en/workflows/](https://tom.medak.click/en/workflows/)

Tomislac Medak décrit l'ensemble des outils qu'il utilise pour faire de la recherche, et je constate que cela correspond assez bien à ceux que j'ai moi-même choisis — ainsi que ceux plébiscités par la [Chaire de recherche du Canada sur les écritures numériques](https://ecrituresnumeriques.ca) et dont [Débugue tes humanités](https://debugue.ecrituresnumeriques.ca/) tente de former des étudiant·e·s et des chercheur·e·s.
À noter que le panorama est large, que ces outils sont pour la très grande majorité libres, que certaines solutions techniques pourraient être mises à jour mais que chacune d'elles a des alternatives.

Via (probablement) [Arthur Perret](https://www.arthurperret.fr/).