---
title: La sémantique après le texte
date: 2022-10-10
description: "L'idée est simple : faire de la sémantique sans interférer dans le texte."
categorie:
- flux
tags:
- design
---
> I needed an alternative where content is separate from markup. I made an experimental minilang I'm calling Aftertext.  
> Breck Yunits, Aftertext, [https://breckyunits.com/aftertext.html](https://breckyunits.com/aftertext.html)

L'idée est simple : faire de la sémantique sans interférer dans le texte.
"Aftertext is an independent addition."
Breck Yunits propose un langage (de balisage ?) sémantique qui est construit en deux parties : une première partie est le texte lui-même, puis chaque fragment de texte qui fait l'objet d'un traitement sémantique est décrit en ajoutant des instructions littérales.
Voici un exemple :

Un texte _en italique_ avec [un lien](/phd)

Voici l'équivalent avec Aftertext :

```
Un texte en italique avec un lien.
italics en italique
link https://www.quaternum.net/phd un lien
```

À défaut d'être concis, ce langage semble sans ambiguïté.
