---
title: Markdoc
date: 2022-09-04
description: "La découverte de nouveaux langages de balisage léger laisse toujours penser que l'on passe notre temps à réinventer la roue."
categorie:
- flux
tags:
- balisage
---
> Markdoc is a Markdown-based document format and a framework for content publishing. It was designed internally at Stripe to meet the needs of our user-facing product documentation. Markdoc extends Markdown with a custom syntax for tags and annotations, providing a way to tailor content to individual users and introduce interactive elements.
> Markdoc, [https://markdoc.dev](https://markdoc.dev)

La découverte de nouveaux langages de balisage léger laisse toujours penser que l'on passe notre temps à réinventer la roue.
J'ai du mal à voir quel est l'apport par rapport à un langage comme [AsciiDoc](https://asciidoc.org/) qui prévoit de nombreux éléments pour compléter un langage comme Markdown.
Peut-être que l'avantage de Markdoc est de s'implémenter dans un écosystème JavaScript ?
