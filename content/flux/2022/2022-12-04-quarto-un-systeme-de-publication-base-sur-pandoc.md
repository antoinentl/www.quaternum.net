---
title: "Quarto : un système de publication basé sur Pandoc"
date: 2022-12-04
description: "Quarto est une suite de commandes Pandoc avec de nombreux paramètres très utiles, des modèles de document et (notamment) une intégration dans des éditeurs de texte."
categorie:
- flux
tags:
- publication
---
> Quarto® is an open-source scientific and technical publishing system built on Pandoc  
> Quarto, [https://quarto.org](https://quarto.org/)

Quarto est une suite de commandes Pandoc avec de nombreux paramètres très utiles, des modèles de document et (notamment) une intégration dans des éditeurs de texte.
Avec Quarto il n'est pas nécessaire de connaître en détail les rouages de Pandoc, la génération de différents formats est grandement facilitée.
[La documentation](https://quarto.org/docs/guide/) est très bien faite, c'est même une autre façon de découvrir Pandoc puisque la grande majorité du fonctionnement de Quarto est basée sur Pandoc.

À noter que c'est la structure qui porte RStudio, Posit, qui développe Quarto et la _marque_ associée (ça rigole pas).

Avant que [David](https://larlet.fr/david/) ou [Arthur](https://www.arthurperret.fr/) ne signale Quarto j'étais tombé dessus il y a plusieurs mois, doutant de l'intérêt de cette solution.
Après quelques mois à observer des personnes l'utilisant je suis maintenant convaincu que c'est un _système_ puissant et facile à prendre en main, notamment pour faire du _single source publishing_, ou via un éditeur de texte comme VSCode/Codium.
