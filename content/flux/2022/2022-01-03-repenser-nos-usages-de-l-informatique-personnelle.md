---
title: "Repenser nos usages de l'informatique personnelle"
date: 2022-01-03
description: "Kris De Decker, l'éditeur de Low Tech Magazine, explique son choix de ne plus acheter et utiliser d'ordinateur neuf."
categorie:
- flux
tags:
- informatique
---
> Although capitalism could provide us with used laptops for decades to come, the strategy outlined above should be considered a hack, not an economical model. It’s a way to deal with or escape from an economic system that tries to force you and me to consume as much as possible. It’s an attempt to break that system, but it’s not a solution in itself.  
> Kris De Decker,  How and Why I Stopped Buying New Laptops, [https://solar.lowtechmagazine.com/2020/12/how-and-why-i-stopped-buying-new-laptops.html](https://solar.lowtechmagazine.com/2020/12/how-and-why-i-stopped-buying-new-laptops.html)

Kris De Decker, l'éditeur de Low Tech Magazine, explique son choix de ne plus acheter et utiliser d'ordinateur neuf.
Sa solution, à base d'ordinateurs d'occasion assez solides (les fameux ThinkPad d'IBM désormais proposés par Lenovo), est longuement décrite sous l'angle de l'économie d'énergie (consommer moins en ne produisant pas de nouvelle machine) tout en présentant les limites d'un tel choix (limitations du matériel et de la puissance de calcul).

Deux remarques (qui ne veulent pas pour autant saper cette initiative très intéressante) : même pour des usages jugés simples il devient de plus en plus difficile d'utiliser du matériel de plus de 10 ans ; il s'agit probablement d'un privilège (bourgeois ?) de pouvoir se procurer et réparer des vieilles machines, une certaine forme de luxe réservée à celles et ceux qui ont les moyens (culturels) de le faire, la question serait de déterminer à quel point ce modèle peut être transmis (cet article contribue fortement à cela) et reproduit dans d'autres sphères.