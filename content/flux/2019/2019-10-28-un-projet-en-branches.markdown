---
layout: post
title: "Un projet en branches"
date: "2019-10-28T22:00:00"
comments: true
published: true
description: "Jean-Yves Gastaud détaille l'organisation de ses projets avec Git, et notamment la répartition des phases de développement selon des branches. Très instructif, y compris pour des projets qui ne sont pas du code."
categories:
- flux
tags:
- outils
---
> Résumé du workflow Git, en ligne de commande, que j'applique de façon quasi-systématique à mes projets.  
[Jean-Yves Gastaud, Mon workflow Git](https://gastaud.io/article/git-workflow/)

Jean-Yves Gastaud détaille l'organisation de ses projets avec Git, et notamment la répartition des phases de développement selon des branches.
Très instructif, y compris pour des projets qui ne sont pas du code.
