---
layout: post
title: "Tufte CSS + Jekyll"
date: "2019-11-07T22:00:00"
comments: true
published: true
description: "Le style Tufte appliqué à Jekyll : beau, fonctionnel et minimaliste."
categories:
- flux
tags:
- typographie
slug: tufte-css-plus-jekyll
---
> After creating the ET-Jekyll theme almost two years ago, I finally got around to revamping the structure and improving a lot of minor performance issues.  
> [Bradley Taunt, Improving Tufte CSS for Jekyll](https://uglyduck.ca/improving-tufte-jekyll.html)

Le style Tufte appliqué à Jekyll : beau, fonctionnel et minimaliste.