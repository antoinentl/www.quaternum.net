---
layout: post
title: "À propos de Donald Knuth"
date: "2019-04-15T22:00:00"
comments: true
published: true
description: "Parce qu'il ne faudrait pas réduire Donald Knuth à l'invention de TeX (qui donnera naissance à LaTeX)."
categories:
- flux
tags:
- typographie
---
> Horrified by what his beloved book looked like on the page with the advent of digital publishing, Dr. Knuth had gone on a mission to create the TeX computer typesetting system, which remains the gold standard for all forms of scientific communication and publication. Some consider it Dr. Knuth’s greatest contribution to the world, and the greatest contribution to typography since Gutenberg.  
[Donald Knuth, The Yoda of Silicon Valley](https://www.nytimes.com/2018/12/17/science/donald-knuth-computers-algorithms-programming.html)

Parce qu'il ne faudrait pas réduire Donald Knuth à l'invention de TeX (qui donnera naissance à LaTeX).
