---
layout: post
title: "Publishing Sphere"
date: "2019-05-30T22:00:00"
comments: true
published: true
description: "Du 23 au 25 mai s'est déroulé à Montréal cet événement entre littérature, publication, écriture numérique, poésie et performance. À voir passer les tweets pendant deux jours, j'aurais aimé y être."
categories:
- flux
tags:
- publication
---
> Sous forme d’atelier, de performance, de co-écriture, de prise de parole, de booksprint, de forum, de wiki, de fish-bowl ou encore de pow-wow, nous souhaitons des propositions explorant l’inscription, l’écriture et la publication comme dynamiques d’ouverture d’un espace. La Publishing Sphere tentera de mettre en dialogue les différentes communautés en présence, dans un geste éditorial (curation) à la rencontre entre réflexion et création.  
[Publishing Sphere 2019](http://publishingsphere.ecrituresnumeriques.ca)

Du 23 au 25 mai s'est déroulé à Montréal cet événement entre littérature, publication, écriture numérique, poésie et performance.
À voir passer [les tweets](https://twitter.com/hashtag/publishingsphere?f=tweets&vertical=default&src=hash) pendant deux jours, j'aurais aimé y être.
