---
layout: post
title: "Paysage des outils et plateformes open source de publication"
date: "2019-09-23T22:00:00"
comments: true
published: true
description: "Un rapport ambitieux qui dresse un portrait des outils ouverts de publication, que ce soit des logiciels ou des plateformes : contextualisation, panorama, présentation des outils et prospective."
categories:
- flux
tags:
- publication
---
> In 2018 the MIT Press secured a grant from the Andrew W. Mellon foundation to conduct a landscape analysis of open source publishing systems, suggest sustainability models that can be adopted to ensure that these systems fully support research communication and provide durable alternatives to complex and costly proprietary services. John Maxwell at Simon Fraser University in Vancouver conducted the environmental scan and compiled this report.  
[John W Maxwell, Mind the Gap, A Landscape Analysis of Open Source Publishing Tools and Platforms](https://mindthegap.pubpub.org/)

Un rapport ambitieux qui dresse un portrait des outils _ouverts_ de publication, que ce soit des logiciels ou des plateformes : contextualisation, panorama, présentation des outils et prospective.
