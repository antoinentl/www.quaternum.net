---
layout: post
title: "Reticulum"
date: "2019-02-20T22:00:00"
comments: true
published: true
description: "Une série d'événements organisés par Florian Harmand et Arthur Perret autour des sciences de l'information et du design. La première journée a lieu lundi 4 mars 2019 à l'Université Bordeaux Montaigne et a pour titre Opérabilité du concept de dispositif."
categories:
- flux
tags:
- design
---
> A collection of valuable design and development resource links for designers and developers in general.  
[Reticulum, Reticulum 1. Opérabilité du concept de dispositif](http://reticulum.info)

Une série d'événements organisés par Florian Harmand et Arthur Perret autour des sciences de l'information et du design.
La première journée a lieu lundi 4 mars 2019 à l'Université Bordeaux Montaigne et a pour titre "Opérabilité du concept de dispositif".
