---
layout: post
title: "Gilbert Simondon et John Dewey"
date: "2019-10-08T22:00:00"
comments: true
published: true
description: "À l'occasion d'une lecture d'un texte de John Dewey, je m'interroge sur le rapprochement avec des concepts développés par Gilbert Simondon. Bonne nouvelle, Elisa Binda a écrit un article sur cette question !"
categories:
- flux
tags:
- design
---
> La convergence entre les deux auteurs s’exprime également dans leur façon de considérer le rapport, que l’on peut qualifier de co-constitution, entre l’organisme et son milieu ; tout comme pour Simondon, qui développe la notion de « milieu associé », il n’est possible, pour Dewey, de concevoir un individu qu’à partir de sa relation à un milieu.  
[Elisa Binda, Techno-esthétiques ou philosophies de l’interaction : les réflexions de Gilbert Simondon et John Dewey](https://journals.openedition.org/appareil/2217)

À l'occasion d'une lecture d'un texte de John Dewey, je m'interroge sur le rapprochement avec des concepts développés par Gilbert Simondon.
Bonne nouvelle, Elisa Binda a écrit un article sur cette question !
