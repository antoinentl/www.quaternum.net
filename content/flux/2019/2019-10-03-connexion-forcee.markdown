---
layout: post
title: "Connexion forcée"
date: "2019-10-03T22:00:00"
comments: true
published: true
description: "Où l'obligation d'utiliser des interfaces informatiques et d'être connecter devient un enfer. Déjà que cela m'effraie alors que je comprends vaguement comment tout cela fonctionne, je n'ose imaginer l'angoisse et le lot de contraintes pour celles et ceux qui sont censés s'adapter..."
categories:
- flux
tags:
- privacy
---
> Une société sans contact se profile, avec des millions de citoyens confrontés de force à des écrans.  
[Julien Brygo, Peut-on encore vivre sans Internet ?](https://www.monde-diplomatique.fr/2019/08/BRYGO/60129)

Où l'obligation d'utiliser des interfaces informatiques et d'être connecter devient un enfer.
Déjà que cela m'effraie alors que je comprends vaguement comment tout cela fonctionne, je n'ose imaginer l'angoisse et le lot de contraintes pour celles et ceux qui sont censés s'adapter...
