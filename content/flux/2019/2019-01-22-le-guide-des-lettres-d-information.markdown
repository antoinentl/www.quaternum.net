---
layout: post
title: "Le guide des lettres d'information"
date: "2019-01-22T22:00:00"
comments: true
published: true
description: "Un guide pour écrire des lettres d'information, avec de nombreux exemples."
categories:
- flux
tags:
- outils
slug: le-guide-des-lettres-d-information
---
> A 201 guide for taking your newsletters to the next level — growing the lists, making money, and more.  
[newsletterguide.org](https://newsletterguide.org/)

Un guide pour écrire des lettres d'information, avec de nombreux exemples.
