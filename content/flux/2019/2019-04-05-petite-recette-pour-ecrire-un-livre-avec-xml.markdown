---
layout: post
title: "Petite recette pour écrire un livre avec XML"
date: "2019-04-05T22:00:00"
comments: true
published: true
description: "Stéphane Bortzmeyer explique quels outils et quelle recette il a utilisé pour écrire son livre Cyberstructure, à la fois pour disposer d'un processus qui lui convient, et aussi pour respecter les contraintes de son éditeur C&F éditions – notamment en permettant l'import d'un fichier XML dans InDesign."
categories:
- flux
tags:
- publication
---
> Ce nouvel article est destiné uniquement aux détails techniques de l'écriture, pour ceux et celles qui se demandent « tu as utilisé quel logiciel pour faire ce livre ? ».  
[Stéphane Bortzmeyer, Détails techniques sur l'écriture de mon livre « Cyberstructure »](https://www.bortzmeyer.org/livre-tech.html)

Stéphane Bortzmeyer explique quels outils et quelle recette il a utilisé pour écrire son livre _Cyberstructure_, à la fois pour disposer d'un processus qui lui convient, et aussi pour respecter les contraintes de son éditeur C&F éditions – notamment en permettant l'import d'un fichier XML dans InDesign.
