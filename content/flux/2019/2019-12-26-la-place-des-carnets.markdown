---
layout: post
title: "La place des carnets"
date: "2019-12-26T22:00:00"
comments: true
published: true
description: "Ou comment utiliser Git pour écrire (et non seulement versionner) une pièce de théâtre. Génie."
categories:
- flux
tags:
- publication
---
> Il convient de réfléchir à nos choix individuels et à leurs conséquences collectives. Faire l’impasse sur l’acquisition d’une culture technique de l’écrit contemporain signifie l’adoption de certaines dépendances plutôt que d’autres. Ce faisant, nous déchargeons à la fois responsabilité et contraintes sur d’autres corps de métiers, largement invisibilisés.  
[Arthur Perret, Pour un autre carnet de recherche numérique](https://arthurperret.fr/2019/11/18/pour-un-autre-carnet-de-recherche-numerique/)

La question des carnets de recherche méritait qu'elle soit doublement posée.
Tout d'abord par Arthur qui décortique avec méthode les différents éléments de l'interrogation, puis un peu plus maladroitement [par moi-même](/2019/12/05/publier-les-carnets-eloge-du-brouillon/) sur les questions de forme et d'identité.
