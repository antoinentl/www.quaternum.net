---
layout: post
title: "Git pour les nuls"
date: "2019-05-22T22:00:00"
comments: true
published: true
description: "Je suis toujours fasciné de constater que Git, le puissant système de gestion de versions, peut être utilisé dans un projet sans forcément que les utilisateurs s'en rendent compte. Et c'est tant mieux, car Git demande du temps pour être maîtrisé."
categories:
- flux
tags:
- outils
---
> Behind the scenes we perform a Git merge into the staging branch.  
[Mike Neumegen, Bringing Git Workflows to Editors](https://cloudcannon.com/features/2019/05/08/git-workflows-to-editors/)

Je suis toujours fasciné de constater que Git, le puissant système de gestion de versions, peut être utilisé dans un projet sans forcément que les utilisateurs s'en rendent compte.
Et c'est tant mieux, car Git demande du temps pour être maîtrisé.
CloudCannon n'est qu'un exemple parmi tant d'autres.
