---
layout: post
title: "Possédez toujours votre plateforme"
date: "2019-04-08T22:00:00"
comments: true
published: true
description: "À rapprocher du mouvement Publish (on your) Own Site, Syndicate Elsewhere d'IndieWeb."
categories:
- flux
tags:
- privacy
---
> Stop giving away your work to people who don't care about it. Host it yourself. Distribute it via methods you control. Build your audience deliberately and on your own terms.  
[Sean Blanda, Always Own Your Platform](https://www.alwaysownyourplatform.com/)

À rapprocher du mouvement Publish (on your) Own Site, Syndicate Elsewhere d'[IndieWeb](https://indieweb.org/).
