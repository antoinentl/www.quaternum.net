---
layout: post
title: "Le design web comme architecture"
date: "2019-04-29T22:00:00"
comments: true
published: true
description: "Un manifeste bien construit."
categories:
- flux
tags:
- design
---
> **Websites are inevitable**. Applying Rem Koolhaas' quip about buildings, a website has to happen in order for a service or content to exist in the digital realm.  
[Malte Müller, Web design as architecture](http://www--arc.com/)

Un manifeste bien construit.
