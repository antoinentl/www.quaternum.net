---
layout: post
title: "Killed by Google"
date: "2019-04-12T22:00:00"
comments: true
published: true
description: "Une liste des applications, services, dispositifs et produits développés (ou récupérés) par Google. Je suis partagé, cette initiative ressemble plus à un catalogue de regrets qu'à une véritable critique de ce que représente Google."
categories:
- flux
tags:
- web
---
> Killed by Google is the Google graveyard; a free and open source list of discontinued Google services, products, devices, and apps. We aim to be a source of factual information about the history surrounding Google's dead projects.  
[Killed by Google](https://killedbygoogle.com/)

Une liste des applications, services, dispositifs et produits développés (ou récupérés) par Google.
Je suis partagé, cette initiative ressemble plus à un catalogue de regrets qu'à une véritable critique de ce que représente Google.
