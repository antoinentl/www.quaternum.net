---
layout: post
title: "Vous avez dit livre numérique accessible ?"
date: "2019-02-13T22:00:00"
comments: true
published: true
description: "Beaucoup d'informations intéressantes suite à cette deuxième édition des Rencontres nationales du livre numérique accessible : depuis des projets comme Ace ou Opaline jusqu'aux retours d'expérience."
categories:
- flux
tags:
- accessibilité
---
> Après le succès de la manifestation Journée nationale sur l’accès au livre et à la lecture: Trouvons des solutions ensemble du 7 décembre 2017, BrailleNet, Auvergne-Rhône-Alpes Livre et lecture et L’École nationale supérieure des sciences de l’information et des bibliothèques (Enssib) ont organisé le 17 janvier la première édition des Rencontres nationales du livre numérique accessible. Plus de 80 personnes travaillant dans l’écosystème du livre se sont déplacées à Villeurbanne à cette occasion.  
[BrailleNet, Compte rendu des Rencontres nationales du livre numérique accessible](https://www.braillenet.org/compte-rendu-rencontres-nationales-livre-numerique-accessible/)

Beaucoup d'informations intéressantes suite à cette deuxième édition des Rencontres nationales du livre numérique accessible : depuis des projets comme [Ace](https://daisy.github.io/ace/) ou [Opaline](https://www.braillenet.org/activites/recherche-developpement/opaline/) jusqu'aux retours d'expérience.
