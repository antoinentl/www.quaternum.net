---
layout: post
title: "Culture numérique"
date: "2019-06-28T22:00:00"
comments: true
published: true
description: "Peppe Cavallari propose un riche compte rendu du livre de Dominique Cardon, Culture numérique, avec une introduction fort utile et une bibliographie additionnelle."
categories:
- flux
tags:
- métier
---
> Dans cet ouvrage, qui est la version rédigée du cours que le sociologue assure depuis plusieurs années, Cardon – se dissociant en cela de la vision pour laquelle la révolution numérique s’inscrit dans le sillage des révolutions industrielles4 – défend que l’impact de la technologie numérique doit être comparé à celui de l’invention de l’écriture puis de celle de l’imprimerie, car en créant un nouveau milieu informationnel et communicationnel, elle engage l’émergence d’un système cognitif et symbolique dans lequel les autorités culturelles et institutionnelles traditionnelles sont mises profondément en question, voire bouleversées.  
[Peppe Cavallari, La culture numérique selon Dominique Cardon](http://sens-public.org/article1417.html)

Peppe Cavallari propose un riche compte rendu du livre de Dominique Cardon, _Culture numérique_, avec une introduction fort utile et une bibliographie additionnelle.
