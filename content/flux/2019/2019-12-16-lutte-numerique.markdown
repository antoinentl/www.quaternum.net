---
layout: post
title: "Lutte numérique"
date: "2019-12-16T22:00:00"
comments: true
published: true
description: "Un appel à la grève des travailleuses et travailleurs du numérique (ou disons de l'informatique au sens large) suffisamment rare pour devoir être souligné. Au-delà de ce texte, fort juste, l'action est encore à déterminer dans sa forme."
categories:
- flux
tags:
- métier
---
> L'automatisation peut et doit servir l'humanité. Elle permettrait de travailler moins, de partir en retraite plus tôt, et dans d'excellentes conditions de vie, de dégager à tous du temps libre pour étudier, expérimenter, pratiquer les sports, les arts, passer du temps en famille ou entre amis ; de vivre.  
[onestlatech, Appel des travailleuses et travailleurs du numérique pour une autre réforme des retraites](https://onestla.tech/)

Un appel à la grève des travailleuses et travailleurs du numérique (ou disons de l'informatique au sens large) suffisamment rare pour devoir être souligné.
Au-delà de ce texte, fort juste, l'action est encore à déterminer dans sa forme.
