---
layout: post
title: "Biens communs et information"
date: "2019-01-08T22:00:00"
comments: true
published: true
description: "Pouvoir Savoir, Le développement face aux biens communs de l'information et à la propriété intellectuelle, le cadeau de C&F éditions pour (bien) commencer l'année 2019."
categories:
- flux
tags:
- livre
---
> Les livres sont des bornes sur un chemin, qui témoignent de l'état des réflexions à un moment donné. Le livre numérique ne peut échapper à cette pesanteur, au risque de quitter le monde du livre pour rejoindre celui des médias, et de l'oubli organisé.  
[C&F éditions, Pouvoir Savoir](https://cfeditions.com/voeux2019/)

_Pouvoir Savoir, Le développement face aux biens communs de l'information et à la propriété intellectuelle_, le cadeau de C&F éditions pour (bien) commencer l'année 2019.
