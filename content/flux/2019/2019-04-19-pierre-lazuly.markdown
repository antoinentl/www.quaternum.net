---
layout: post
title: "Pierre Lazuly"
date: "2019-04-19T22:00:00"
comments: true
published: true
description: "Je découvre le personnage en même temps qu'il disparaît, reconnaissant quelques projets auxquels Pierre Lazuly a participé."
categories:
- flux
tags:
- web
---
> On y croyait, au Web libertaire, à l’espace de démocratie, au non-marchand. Pierre Lazuly n’était pas pour rien dans cette dynamique, lui qui prenait le temps d’écrire, de penser, de partager.  
[Stéphane Deschamps, Le Web indé en deuil](https://nota-bene.org/Le-Web-inde-en-deuil)

Je découvre le personnage en même temps qu'il disparaît, reconnaissant quelques projets auxquels Pierre Lazuly a participé.
