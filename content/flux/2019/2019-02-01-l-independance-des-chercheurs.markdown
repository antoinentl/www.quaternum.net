---
layout: post
title: "L'indépendance des chercheurs"
date: "2019-02-01T22:00:00"
comments: true
published: true
description: "L'indépendance des chercheurs, qui prennent conscience de l'importance de la maîtrise de la circulation de leurs textes. Qu'il s'agisse d'ailleurs de libre accès ou non."
categories:
- flux
tags:
- publication
slug: l-independance-des-chercheurs
---
> Le comité éditorial d’un des magazines les plus prestigieux, publié par le conglomérat Elsevier, vient de démissionner en bloc pour fonder sa propre publication, qui offrira tous ses articles en libre accès, loin des tarifs exorbitants exigés par les revues dites "prédatrices".  
[Marco Fortier, Quand les scientifiques se révoltent contre les géants de l'édition savante](https://www.ledevoir.com/societe/education/546298/rebellion-contre-une-revue-predatrice)

L'indépendance des chercheurs, qui prennent conscience de l'importance de la maîtrise de la circulation de leurs textes.
Qu'il s'agisse d'ailleurs de libre accès ou non.
