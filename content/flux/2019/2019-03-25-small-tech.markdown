---
layout: post
title: "Small tech"
date: "2019-03-25T22:00:00"
comments: true
published: true
description: "Millie Servant s'entretient avec Aral Balkan, et même si certaines réponses mériteraient des précisions, l'approche critique est percutante. Internet et le web vont mal mais il y a de nombreuses solutions."
categories:
- flux
tags:
- web
---
> Si nous voulons une alternative, nous devons la financer alternativement.  
[Aral Balkan, « La small tech sera le poison de la Silicon Valley »](https://usbeketrica.com/article/small-tech-poison-silicon-valley)

Millie Servant s'entretient avec Aral Balkan, et même si certaines réponses mériteraient des précisions, l'approche critique est percutante.
Internet et le web vont mal mais il y a de nombreuses solutions.
