---
layout: post
title: "JAMstack WTF"
date: "2019-02-06T22:00:00"
comments: true
published: true
description: "Le modèle JAMstack – pour JavaScript, API et Markup – reconfigure en profondeur la façon de produire des objets numériques, y compris des publications. Ce modèle est également modulaire, donc il est intéressant d'aller piocher ce qui semble le plus pertinent pour un projet de site web ou de publication en ligne par exemple."
categories:
- flux
tags:
- web
---
> JAMstack is revolutionising the way we think about workflow by providing a simpler developer experience, better performance, lower cost and greater scalability.  
[Pedro Duarte, JAMstack WTF](https://jamstack.wtf)

Le modèle JAMstack – pour JavaScript, API et Markup – reconfigure en profondeur la façon de produire des objets numériques, y compris des publications.
Ce modèle est également modulaire, donc il est intéressant d'aller piocher ce qui semble le plus pertinent pour un projet de site web ou de publication en ligne par exemple.

Via [Frank](https://frank.taillandier.me/).
