---
layout: post
title: "Lire autrement"
date: "2019-12-27T22:00:00"
comments: true
published: true
description: "Un appel à projets motivant, auquel j'aurais aimé répondre si j'avais eu beaucoup plus de temps..."
categories:
- flux
tags:
- design
---
> Les éditeurs québécois et les experts en numérique sont invités à déposer un projet qui propose une nouvelle façon de présenter le récit d’un auteur québécois (les éditeurs sont invités à travailler avec leurs auteurs).  
Toutes les œuvres issues de la littérature québécoise et tous les types de supports numériques sont admissibles (jeu vidéo, application mobile, réalité virtuelle, réalité augmentée, web-série, etc.).  
[Xn Québec, Appel à projets - JAM 360 : Lire autrement](https://www.xnquebec.co/nouvelles-xn/lire-autrement-appel-projets/)

Un appel à projets motivant, auquel j'aurais aimé répondre si j'avais eu beaucoup plus de temps...
