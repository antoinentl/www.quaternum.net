---
layout: post
title: "Écriture numérique"
date: "2019-04-17T22:00:00"
comments: true
published: true
description: "Arthur parvient avec beaucoup de concision et d'élégance à exprimer des choses que je n'avais que pressenties. Si je relaie presque chacun de ses billets c'est parce qu'il touche très exactement aux sujets qui m'animent."
categories:
- flux
tags:
- publication
---
> L’outil d’écriture idéal est-il celui qui comporte toutes les fonctionnalités dont vous avez besoin, ou bien celui qui est suffisamment modulable pour que vous les intégriez vous-même ? Je crois que la réponse à cette question peut conditionner l’usage et éventuellement l’adoption d’un logiciel. Ce n’est pas avant d’y avoir répondu que j’ai pu entrevoir les _conditions de l’écriture_ pour ma thèse.  
[Arthur Perret, Et à la fin, écrire…](https://arthurperret.fr/2019/03/14/et-a-la-fin-ecrire/)

Arthur parvient avec beaucoup de concision et d'élégance à exprimer des choses que je n'avais que pressenties.
Si je relaie presque chacun de ses billets c'est parce qu'il touche très exactement aux sujets qui m'animent.
