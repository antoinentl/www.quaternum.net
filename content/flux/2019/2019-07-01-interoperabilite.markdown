---
layout: post
title: "Interopérabilité"
date: "2019-07-01T22:00:00"
comments: true
published: true
description: "Enfin une explication claire de ce qu'est l'interopérabilité, avec des exemples illustratifs."
categories:
- flux
tags:
- design
---
> Le protocole permet l’interopérabilité. L’interopérabilité est la capacité à communiquer de deux logiciels différents, issus d’équipes de développement différentes.  
[Stéphane Bortzmeyer, C’est quoi, l’interopérabilité, et pourquoi est-ce beau et bien ?](https://framablog.org/2019/06/12/cest-quoi-linteroperabilite-et-pourquoi-est-ce-beau-et-bien/)

Enfin une explication claire de ce qu'est l'interopérabilité, avec des exemples illustratifs.
