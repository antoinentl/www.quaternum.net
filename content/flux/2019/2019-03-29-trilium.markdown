---
layout: post
title: "Trilium"
date: "2019-03-29T22:00:00"
comments: true
published: true
description: "Trilium est un gestionnaire de notes très avancé, qui permet d'organiser des contenus de nature très diverse. (Et c'est ouvert et libre, et apparemment sans base de données.)"
categories:
- flux
tags:
- outils
---
> Trilium Notes is a hierarchical note taking application with focus on building large personal knowledge bases.  
[Trilium](https://github.com/zadam/trilium)

Trilium est un gestionnaire de notes très avancé, qui permet d'organiser des contenus de nature très diverse.
(Et c'est ouvert et libre, et apparemment sans base de données.)

Via [Henri](https://henrisergent.fr/).
