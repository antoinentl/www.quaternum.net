---
layout: post
title: "Mélange des genres"
date: "2019-06-05T22:00:00"
comments: true
published: true
description: "Intégrer du CSS dans du JS n'est pas une très bonne idée, le problème étant que c'est celles et ceux qui écrivent du JS qui décident de changer la donne, alors que les spécialistes d'HTML et de CSS les contredisent. Au moins cet article a le mérite d'être clair, cette pratique semble être adéquate pour des usages spécifiques, si l'on passe outre la dette technique et le détournement malheureux de CSS..."
categories:
- flux
tags:
- design
---
> CSS-in-JS ramène le style au sein du composant, dans son scope. Notre composant contient désormais son markup, son style et son comportement.  
[Matthias Le Brun, Tradeoffs](https://alune.fr/pleasetypewell/)

Intégrer _du CSS dans du JS_ n'est pas une très bonne idée, le problème étant que c'est celles et ceux qui écrivent du JS qui décident de changer la donne, alors que les spécialistes d'HTML et de CSS les contredisent.
Au moins cet article a le mérite d'être clair, cette pratique semble être adéquate pour des usages spécifiques, si l'on passe outre la dette technique et le détournement malheureux de CSS...
