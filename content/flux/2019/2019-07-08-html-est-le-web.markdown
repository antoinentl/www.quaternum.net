---
layout: post
title: "HTML est le Web"
date: "2019-07-08T22:00:00"
comments: true
published: true
description: "Il est bon de rappeler cette évidence, au cœur du web il y a HTML."
categories:
- flux
tags:
- web
---
> My big concern is at the bottom of that technology pyramid. The lowest common denominator of the Web. The foundation. The rhythm section. The ladyfingers in the Web trifle. It’s the HTML.  
[Pete Lambert, HTML is the Web](https://www.petelambert.com/journal/html-is-the-web/)

Il est bon de rappeler cette évidence, au cœur du web il y a HTML.
