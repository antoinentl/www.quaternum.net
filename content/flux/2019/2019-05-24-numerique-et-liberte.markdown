---
layout: post
title: "Numérique et liberté"
date: "2019-05-24T22:00:00"
comments: true
published: true
description: "Le coût de la liberté numérique n'est pas à négliger, mais quel plaisir de pouvoir maîtriser – même un tout petit peu – nos environnements numériques !"
categories:
- flux
tags:
- privacy
---
> [...] il est nécessaire d’avoir une compréhension de base qui demande une étude. Mais cette étude est la condition de la liberté.  
[Marcello Vitali-Rosati, Être libres à l’époque du numérique](http://blog.sens-public.org/marcellovitalirosati/tre-libres-lpoque-du-numrique/)

Le coût de la liberté numérique n'est pas à négliger, mais quel plaisir de pouvoir maîtriser – même un tout petit peu – nos environnements numériques !
