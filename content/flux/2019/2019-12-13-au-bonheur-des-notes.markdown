---
layout: post
title: "Au bonheur des notes"
date: "2019-12-13T22:00:00"
comments: true
published: true
description: "Je suis le premier à être un fervent défenseur de la citation dans le texte, et fervent pourfendeur de la note de bas de page, mais il faut bien reconnaître que ce texte est des plus pertinents."
categories:
- flux
tags:
- publication
---
> D’où vient cette convention graphique de mauvais goût ?  
[Notes de bas de page, Harvard m’a tuer](https://ndbdp.hypotheses.org/88)

Je suis le premier à être un fervent défenseur de la citation dans le texte, et fervent pourfendeur de la note de bas de page, mais il faut bien reconnaître que ce texte est des plus pertinents.
