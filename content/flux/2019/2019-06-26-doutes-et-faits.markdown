---
layout: post
title: "Doutes et faits"
date: "2019-06-26T22:00:00"
comments: true
published: true
description: "Texte cinglant sur notre rapport aux faits et à la façon dont les savants du 18e siècle ont géré les infox."
categories:
- flux
tags:
- philosophie
---
> Le travail du vrai savant est de tout soumettre au doute.  
[Benoît Melançon, Donald Trump et l’Agneau de Scythie](http://sens-public.org/article1415.html)

Texte cinglant sur notre rapport aux faits et à la façon dont les savants du 18e siècle ont géré les _infox_.
