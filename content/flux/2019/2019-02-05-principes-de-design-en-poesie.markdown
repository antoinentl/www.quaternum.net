---
layout: post
title: "Principes de design en poésie"
date: "2019-02-05T22:00:00"
comments: true
published: true
description: "La loi de Pareto, le principe d'itération, le concept de cycle de développement et 13 autres principes présentés en une courte poésie et expliqués en quelques lignes. Utile et beau."
categories:
- flux
tags:
- design
---
> La série "Principes du design" explore en poésie les grandes règles du design et de l’innovation, qui servent à estimer, à expliquer, mais aussi à imaginer le monde qui nous entoure.  
[Stéphane Bataillon, Principes du design – le design et l’innovation en poésie](https://www.stephanebataillon.com/principes-du-design-le-design-et-linnovation-en-poesie/)

La loi de Pareto, le principe d'itération, le concept de cycle de développement et 13 autres principes présentés en une courte poésie et expliqués en quelques lignes.
Utile et beau.

Via [Nolwenn](https://mastodon.social/@nolwenn/101540234071276296).
