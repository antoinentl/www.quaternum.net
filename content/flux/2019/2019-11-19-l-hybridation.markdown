---
layout: post
title: "L'hybridation"
date: "2019-11-19T22:00:00"
comments: true
published: true
description: "Quelques semaines après mon arrivée à Montréal le comité étudiant Figura me fait l'honneur de venir parler d'un morceau de mon sujet de thèse."
categories:
- flux
tags:
- publication
slug: l-hybridation
---
> Cette présentation propose de démonter l'idée de rupture qui accompagne souvent l'avènement du numérique dans l'édition, comme cela a été le cas avec l'imprimerie.  
[Les matinées critiques du comité étudiant Figura](http://figura.uqam.ca/actualites/les-matinees-critiques-du-comite-etudiant-figura-1)

Quelques semaines après mon arrivée à Montréal le comité étudiant Figura me fait l'honneur de venir parler d'un morceau de mon sujet de thèse.
C'est l'occasion rêvée d'évoquer le travail d'Alessandro Ludovico, "L'hybridation comme horizon des nouvelles formes du livre : une critique du livre numérique basée sur le concept d'Alessandro Ludovico" est donc le programme que je me suis fixé.
J'espère pouvoir en faire un billet par la suite.
