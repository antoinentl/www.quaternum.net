---
layout: post
title: "La littérature en régime numérique"
date: "2019-11-13T22:00:00"
comments: true
published: true
description: "Intéressant appel à communications qui n'est pas formulé comme une recherche en littérature numérique."
categories:
- flux
tags:
- publication
---
> Le numérique offre à la littérature de nouveaux environnements et outils d’écriture, de nouvelles modalités d’édition d’œuvres imprimées et de fonds d’archives, ainsi que de nouveaux moyens d’investigation sur les textes et l’histoire littéraire.  
> [thalim, Visualités et visualisations du texte littéraire en régime numérique](http://www.thalim.cnrs.fr/appels-a-contribution/article/visualites-et-visualisations-du-texte-litteraire-en-regime-numerique)

Intéressant appel à communications qui n'est pas formulé comme une recherche en littérature numérique.

> Le but du colloque est de prolonger la réflexion sur le rôle du design dans les humanités numériques en l’appliquant plus spécifiquement au versant littéraire de celles-ci 