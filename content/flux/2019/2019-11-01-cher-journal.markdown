---
layout: post
title: "Cher journal"
date: "2019-11-01T22:00:00"
comments: true
published: true
description: "Écrire, écrire, écrire. Je partage l'avis de Derek Sivers, l'écriture quotidienne est puissante, mais elle peut aussi être envahissante, il faut garder une certaine distance et arrêter quand ça devient une contrainte trop forte."
categories:
- flux
tags:
- publication
---
> Every day at some point, just open up this diary, write today’s date, then start writing. Write what you did today, and how you are feeling, even if it seems boring.  
[Derek Sivers, Benefits of a daily diary and topic journals](https://sivers.org/dj)

Écrire, écrire, écrire.
Je partage l'avis de Derek Sivers, l'écriture quotidienne est puissante, mais elle peut aussi être envahissante, il faut garder une certaine distance et arrêter quand ça devient une contrainte trop forte.
