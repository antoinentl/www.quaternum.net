---
layout: post
title: "Raccourcis"
date: "2019-03-20T22:00:00"
comments: true
published: true
description: "Une ressource bien pratique pour découvrir ou retrouver des raccourcis de beaucoup de logiciels et programmes, et pas forcément dans le domaine du design."
categories:
- flux
tags:
- outils
---
> Every shortcut for designers in one place  
[Shortcuts.design](https://shortcuts.design/)

Une ressource bien pratique pour découvrir ou retrouver des raccourcis de beaucoup de logiciels et programmes, et pas forcément dans le domaine du design.
