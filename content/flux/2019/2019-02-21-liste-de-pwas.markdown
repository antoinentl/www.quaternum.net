---
layout: post
title: "Liste de PWAs"
date: "2019-02-21T22:00:00"
comments: true
published: true
description: "Les PWAs – pour Progressive Web Applications – sont des sites web qui peuvent s'installer comme une application ou qui peuvent s'utiliser sans connexion, c'est un peu le futur du web (même si cela va moins vite que je l'aurais cru il y a deux ans). Voici un site qui les récence et qui semble à jour."
categories:
- flux
tags:
- web
---
> Welcome to Appscope, one of the leading directories for Progressive Web Apps (PWAs). Here you can find hundreds of web-based applications that are compatible with all devices.  
[Appscope](https://appsco.pe/)

Les PWAs – pour Progressive Web Applications – sont des sites web qui peuvent s'installer comme une application ou qui peuvent s'utiliser sans connexion, c'est un peu le futur du web (même si cela va moins vite que je l'aurais cru il y a deux ans).
Voici un site qui les récence et qui semble à jour.

Via [Stéphanie Walter](https://stephaniewalter.design/).
