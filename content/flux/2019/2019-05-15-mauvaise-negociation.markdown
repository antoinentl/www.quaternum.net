---
layout: post
title: "Mauvaise négociation"
date: "2019-05-15T22:00:00"
comments: true
published: true
description: "Couperin (le Consortium unifié des établissements universitaires et de recherche pour l'accès aux publications numériques) semble négocier dans son coin avec Elsevier une licence nationale permettant l'accès aux bibliothèques universitaires à de nombreuses ressources utiles aux étudiants et chercheurs. Le problème c'est que tout cela semble fait dans la plus grande opacité, remettant en cause les grands principes de l'ouverture des publications numériques de la Loi République numérique. C'est moche."
categories:
- flux
tags:
- publication
---
> D’une manière générale, je ne comprends pas bien pourquoi ces négociations de Licence Nationale ne sont pas réalisées de manière publique et ouverte.  
[Daniel Bourrion, Un accord de mauvais principes](http://blog.univ-angers.fr/rj45/2019/04/29/un-accord-de-mauvais-principes/)

Couperin (le Consortium unifié des établissements universitaires et de recherche pour l'accès aux publications numériques) semble négocier dans son coin avec Elsevier une licence nationale permettant l'accès aux bibliothèques universitaires à de nombreuses ressources utiles aux étudiants et chercheurs.
Le problème c'est que tout cela semble fait dans la plus grande opacité, remettant en cause les grands principes de l'ouverture des publications numériques de la Loi République numérique.
C'est moche.

À lire également : [Lettre ouverte au consortium Couperin sur le renouvellement de l'abonnement à Elsevier](https://blog.dissem.in/2019/lettre-ouverte-au-consortium-couperin-sur-le-renouvellement-de-labonnement), [Pourquoi et comment le consortium Couperin s’est-il couché devant Elsevier ?](https://frederichelein.wordpress.com/2019/06/10/pourquoi-et-comment-le-consortium-couperin-sest-il-couche-devant-elsevier/).
