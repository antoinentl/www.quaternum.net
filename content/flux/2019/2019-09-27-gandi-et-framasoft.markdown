---
layout: post
title: "Gandi et Framasoft"
date: "2019-09-27T22:00:00"
comments: true
published: true
description: "Un rapprochement de bon augure."
categories:
- flux
tags:
- web
---
> Gandi rejoint la liste des mécènes de Framasoft le 10 septembre 2019. [...] Avec ce partenariat, Gandi et Framasoft confirment leur souhait d’avancer ensemble dans la même direction.  
[Gandi, Gandi et Framasoft s’associent](https://news.gandi.net/fr/2019/09/gandi-et-framasoft-sassocient/)

Un rapprochement de bon augure.
