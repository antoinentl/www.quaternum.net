---
layout: post
title: "Agir face à Google"
date: "2019-09-05T22:00:00"
comments: true
published: true
description: "Quelques propositions radicales, mais néanmoins séduisantes, pour faire face à Google, et agir."
categories:
- flux
tags:
- web
---
> I think the default attitude of most users is to assume defeat when challenging a powerhouse like Google. It might seem like an uphill battle, but every tiny step away from them helps. We as independent web users and developers can make a small difference.  
[Bradley Taunt, Dear Google, I'm Blocking You From My Website](https://uglyduck.ca/stop-crawling-google/)

Quelques propositions radicales, mais néanmoins séduisantes, pour faire face à Google, et agir.
