---
layout: post
title: "Contrôle VS vitesse"
date: "2019-05-09T22:00:00"
comments: true
published: true
description: "Comment mieux résumer le problème AMP ? (AMP est le moyen inventé par Google pour forcer les éditeurs de contenus à utiliser ses technologies pour apparaître dans les premiers résultats.)"
categories:
- flux
tags:
- web
---
> AMP isn’t about speed. It’s about control.  
[Walid Halabi, Google AMP lowered our page speed, and there's no choice but to use it](https://unlikekinds.com/article/google-amp-page-speed)

Comment mieux résumer le problème AMP ?
(AMP est le moyen inventé par Google pour forcer les éditeurs de contenus à utiliser ses technologies pour apparaître dans les premiers résultats.)
