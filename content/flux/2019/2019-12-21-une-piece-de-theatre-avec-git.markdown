---
layout: post
title: "Une pièce de théâtre avec Git"
date: "2019-12-21T22:00:00"
comments: true
published: true
description: "Ou comment utiliser Git pour écrire (et non seulement versionner) une pièce de théâtre. Génie."
categories:
- flux
tags:
- publication
---
> La nature distribuée des modifications dans Git permet de d’identifier l’auteur de chaque ligne (et dont une signature cryptographique permet de valider l’authenticité). Autrement dit : ce qui est dit par Berthold a réellement été écrit par Berthold (en fait foi la signature) ; et ce qui a été écrit par l’auteur est bien signé par l’auteur. Ce sont donc les personnages qui parlent eux-mêmes lorsqu’ils sont identifiés.  
[Louis-Olivier Brassard, Une pièce pour Louis-Olivier](https://scolaire.loupbrun.ca/piece01/)

Ou comment utiliser Git pour écrire (et non seulement versionner) une pièce de théâtre.
Génie.
En parallèle (ou en écho ?) d'une de mes expérimentations d'écriture avec Git, [Cheminement textuel](/2019/12/09/cheminement-textuel/).
