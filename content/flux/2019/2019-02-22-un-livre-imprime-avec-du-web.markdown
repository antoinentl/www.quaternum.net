---
layout: post
title: "Un livre imprimé avec du web"
date: "2019-02-22T22:00:00"
comments: true
published: true
description: "Voilà, C&F éditions a passé le cap avec ce premier essai d'un livre imprimé fabriqué avec HTML, CSS et paged.js. Nicolas Taffin décrit précisément comment il est parvenu à un résultat imprimable, en décortiquant l'utilisation de paged.js et en pointant les obstacles et les limites."
categories:
- flux
tags:
- publication
---
> [...] les CSS étant très au point, on peut utiliser le code html + css et tenter de générer un PDF, soit au moyen du logiciel propriétaire Prince, qui existe depuis une bonne dizaine d’années, et est assez efficace, soit au moyen du nouveau venu sous licence libre (MIT) : Paged.js. C’est cette toute dernière solution que j’ai choisi de tester pour réaliser ce livre, car j’avais envie de voir ce qu’on peut faire dans un navigateur web.  
[Nicolas Taffin, Making-of d’une collection libérée : Addictions sur ordonnance](https://polylogue.org/addictions-sur-ordonnance-making-of-dune-collection-liberee/)

Voilà, C&F éditions a passé le cap avec ce premier essai d'un livre imprimé fabriqué avec HTML, CSS et paged.js.
Nicolas Taffin décrit précisément comment il est parvenu à un résultat imprimable, en décortiquant l'utilisation de paged.js et en pointant les obstacles et les limites.

(J'ai rencontré un certain nombre de problèmes similaires pour la production de [mon mémoire](https://memoire.quaternum.net).)
