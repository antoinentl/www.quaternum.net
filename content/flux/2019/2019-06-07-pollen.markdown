---
layout: post
title: "Pollen"
date: "2019-06-07T22:00:00"
comments: true
published: true
description: "Pollen est un système de publication numérique qui permet de générer un livre web à partir de fichiers Markdown."
categories:
- flux
tags:
- publication
---
> While I write my blog in markdown it's super nice to be able to mix real code with in your markup language. For example if you want to create a special layout for a specific page or if you want a table with subtly different properties than the rest, it's easy.  
[Lawn, Pollen: the book is a program](https://news.ycombinator.com/item?id=20027116)

[Pollen](https://docs.racket-lang.org/pollen/) est un [système de publication numérique](https://memoire.quaternum.net) qui permet de générer un [livre web](/2016/10/24/le-livre-web-une-autre-forme-du-livre-numerique/) à partir de fichiers Markdown.
