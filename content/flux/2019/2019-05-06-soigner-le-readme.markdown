---
layout: post
title: "Soigner le README"
date: "2019-05-06T22:00:00"
comments: true
published: true
description: "Le README, ce fichier accompagnant tout projet informatique pour permettre aux développeurs ou au curieux de comprendre de quoi il en retourne : l'enjeu est de taille. Le README est un peu la quatrième de couverture de toute initiative numérique."
categories:
- flux
tags:
- web
---
> A README is a reflection of how a repository is maintained. [...] A good README tells you everything you need to know to use the project and get involved. It sells the project — but concurrently respects a visitor's time by letting them know if they need a different solution.  
[Andrew Healey, How to Write an Awesome GitHub README](https://dev.to/healeycodes/how-to-write-an-awesome-github-readme-2ldc)

Le README, ce fichier accompagnant tout projet informatique pour permettre aux développeurs ou au curieux de comprendre de quoi il en retourne : l'enjeu est de taille.
Le README est un peu la quatrième de couverture de toute initiative numérique ou de n'importe quel bout de code.
