---
layout: post
title: "Antilivre dynamique"
date: "2019-01-23T22:00:00"
comments: true
published: true
description: "Un court toot pour résumer cette démarche exploratoire : concevoir un livre numérique qui tiendrait dans un fichier HTML. Ça fonctionne et c'est beau ! Les différentes versions de ce livre sont disponibles ici : https://abrupt.ch/otto-borg/la-social-democratie-est-une-chanson-a-boire/"
categories:
- flux
tags:
- ebook
---
> Idées de prospective éditoriale.  
Le problème du HTML face à l'EPUB est la "portabilité", avec sa multitude de fichiers annexes (CSS, JavaScript, images, etc.).  
Mais avec le Data URI scheme, une solution existe. Faire tenir un livre numérique dans un seul fichier HTML.  
Avec l'avènement des écrans pliables (par ex. de l'encre électronique sous Android),  le HTML permettra peut-être une créativité éditoriale supérieure à l'EPUB.  
[Abrüpt, Antilivre dynamique](https://mamot.fr/@cestabrupt/101465668377263368)

Un court toot pour résumer cette démarche exploratoire : concevoir un livre numérique qui tiendrait dans un fichier HTML.
Ça fonctionne et c'est beau !
Les différentes versions de ce livre sont disponibles ici : [https://abrupt.ch/otto-borg/la-social-democratie-est-une-chanson-a-boire/](https://abrupt.ch/otto-borg/la-social-democratie-est-une-chanson-a-boire/)
