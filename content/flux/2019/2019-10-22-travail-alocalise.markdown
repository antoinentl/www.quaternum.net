---
layout: post
title: "Travail alocalisé"
date: "2019-10-22T22:00:00"
comments: true
published: true
description: "Remise en cause particulièrement pertinente de qui est habituellement considéré comme du remote work ou travail à distance (ou encore télétravail)."
categories:
- flux
tags:
- métier
---
> So let's create a term for it. I prefer "alocalized" instead of remote. Remote too often induces the meaning of a central location, where some of the employees are working as satellites.  
[Karl Dubost, This is not a remote work](http://www.otsukare.info/2019/10/15/not-remote-work)

Remise en cause particulièrement pertinente de qui est habituellement considéré comme du _remote work_ ou travail à distance (ou encore télétravail).
Karl présente également ce qui fonctionne pour lui dans une organisation a-centrée.
