---
layout: post
title: "L'art de la ligne de commande"
date: "2019-11-27T22:00:00"
comments: true
published: true
description: "Ou comment améliorer significativement son usage d'un terminal."
categories:
- flux
tags:
- outils
slug: l-art-de-la-ligne-de-commande
---
> La maîtrise de la ligne de commande est une compétence souvent négligée ou considérée ésotérique, pourtant elle améliore de façon évidente et subtile votre habilité et votre productivité en tant qu'ingénieur. Ceci est une sélection de notes et d'astuces sur l'utilisation de la ligne de commande que nous avons trouvées utiles en travaillant avec Linux. Certaines sont élémentaires, d'autres sont assez spécifiques, complexes ou obscures.  
[Joshua Levy, L'art de la ligne de commande](https://github.com/jlevy/the-art-of-command-line/blob/master/README-fr.md)

Ou comment améliorer significativement son usage d'un terminal.
