---
layout: post
title: "Annotation + conversation"
date: "2019-06-19T22:00:00"
comments: true
published: true
description: "Un regard scientifique sur l'expérimentation d'annotation d'OpenEdition (qui me sera bien utile, prévoyant moi-même la rédaction d'un court billet sur la question)."
categories:
- flux
tags:
- publication
slug: annotation-plus-conversation
---
> Le modèle conversationnel fait l’objet d’une circulation qui ne se fait évidemment pas sans altération : de dispositifs en dispositifs, des micro-modifications sont apportées qui déplacent progressivement la manière dont l’annotation comme conversation est pensée, même s’il existe une pérennisation indéniable.  
[Marc Jahjah, L’annotation comme “conversation” (1/2) : des humanistes aux acteurs du web](http://www.marcjahjah.net/3338-lannotation-comme-conversation-1-2-des-humanistes-aux-acteurs-du-web#Conclusion_partielle)

Un regard scientifique sur l'expérimentation d'annotation d'OpenEdition (qui me sera bien utile, prévoyant moi-même la rédaction d'un court billet sur la question).
