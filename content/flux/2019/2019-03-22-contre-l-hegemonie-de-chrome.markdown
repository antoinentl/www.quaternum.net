---
layout: post
title: "Contre l'hégémonie de Chrome"
date: "2019-03-22T22:00:00"
comments: true
published: true
description: "Le temps est venu de retrouver un paysage plus diversifié en terme de navigateurs."
categories:
- flux
tags:
- web
slug: contre-l-hegemonie-de-chrome
---
> Chrome is effectively everywhere you look. And that’s bad news.  
[Reda Lemeden, We Need Chrome No More](https://redalemeden.com/blog/2019/we-need-chrome-no-more)

Le temps est venu de retrouver un paysage plus diversifié en terme de navigateurs.
