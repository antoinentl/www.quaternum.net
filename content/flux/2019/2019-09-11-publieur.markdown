---
layout: post
title: "Publieur"
date: "2019-09-11T22:00:00"
comments: true
published: true
description: "La distinction entre editor et publisher n'existe pas vraiment dans le paysage francophone, quoi qu'il en soit il est intéressant de constater le positionnement d'un acteur comme Rebus, qui se présente comme un éditeur (au sens français) parce qu'il met à disposition des outils permettant la publication."
categories:
- flux
tags:
- publication
---
> Hello, world. We’re Rebus, a new kind of publisher for a new kind of public. Nice to meet you.  
[David, Publishing is about making things public](https://rebus.foundation/2019/07/25/publishing-is-about-making-things-public/)

La distinction entre _editor_ et _publisher_ n'existe pas vraiment dans le paysage francophone, quoi qu'il en soit il est intéressant de constater le positionnement d'un acteur comme Rebus, qui se présente comme un éditeur (au sens français) parce qu'il met à disposition des outils permettant la _publication_.
