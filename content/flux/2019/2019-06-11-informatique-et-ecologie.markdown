---
layout: post
title: "Informatique et écologie"
date: "2019-06-11T22:00:00"
comments: true
published: true
description: "Il manque peut-être une incitation à concevoir des programmes, des logiciels et des services qui consomment moins. Non pas qu'il faut réduire ce combat à une responsabilité individuelle, mais pointer des mauvaises pratiques et signaler des initiatives vertueuses seraient également un beau projet."
categories:
- flux
tags:
- métier
---
> En tant que professionnel·le·s de l’informatique, nous avons la chance de pouvoir choisir pour qui nous souhaitons travailler. Inspirons-nous donc des étudiant·e·s pour signifier aux entreprises responsables du réchauffement climatique et à celles qui les financent que nous refuserons dorénavant de travailler pour elles.  
[...]  
Choisissons de mettre toute notre énergie au service d’entreprises qui répondent à nos valeurs, pas au profit de celles qui détruisent notre avenir !  
[Manifeste écologique des professionnel·le·s de l’informatique](https://www.climanifeste.net/)

Il manque peut-être une incitation à concevoir des programmes, des logiciels et des services qui consomment moins.
Non pas qu'il faut réduire ce combat à une responsabilité individuelle, mais pointer des mauvaises pratiques et signaler des initiatives vertueuses seraient également un beau projet.
