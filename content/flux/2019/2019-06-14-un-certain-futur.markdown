---
layout: post
title: "Un certain futur"
date: "2019-06-14T22:00:00"
comments: true
published: true
description: "Réaliste, drôle et déprimant."
categories:
- flux
tags:
- privacy
---
> Installé sur une chaise pour reposer mes jambes, je capte une fin de discussion où un type dit que « parfois, la technologie quand c’est trop, ça peut faire peur, on a l’impression de plus maîtriser grand-chose… ».  
[lundimatin, Vivatech 2019 : Une journée à l’asile](https://lundi.am/Vivatech-2019-Une-journee-a-l-asile)

Réaliste, drôle et déprimant.
