---
layout: post
title: "Comprendre Git avec des chats"
date: "2019-09-30T22:00:00"
comments: true
published: true
description: "Comprendre Git n'est pas aisé, l'expliquer clairement encore moins. Même si cela peut sembler simplement mignon d'utiliser des chats pour présenter le fonctionnement de Git, c'est surtout efficace !"
categories:
- flux
tags:
- web
---
> Now I created the git doodles.  
[Tomomi Imura, GIT PURR! Git Commands Explained with Cats!](https://girliemac.com/blog/2017/12/26/git-purr/)

Comprendre Git n'est pas aisé, l'expliquer clairement encore moins.
Même si cela peut sembler simplement mignon d'utiliser des chats pour présenter le fonctionnement de Git, c'est terriblement efficace !
