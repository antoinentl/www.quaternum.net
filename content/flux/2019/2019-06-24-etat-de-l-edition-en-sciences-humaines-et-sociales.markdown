---
layout: post
title: "État de l'édition en sciences humaines et sociales"
date: "2019-06-24T22:00:00"
comments: true
published: true
description: "Voilà un événement nécessaire, cette journée organisée par l'École des hautes études en sciences sociales et les Éditions de l’EHESS a par ailleurs été captée, les vidéos sont en ligne."
categories:
- flux
tags:
- publication
slug: etat-de-l-edition-en-sciences-humaines-et-sociales
---
> Le paysage de l’édition en sciences humaines et sociales a été considérablement bouleversé au cours des dix dernières années. Les transformations du marché du livre, l’essor de la publication et de la lecture en ligne et l’évolution de l’évaluation scientifique ont profondément modifié la production et la diffusion du savoir, tout comme les modèles économiques et l’organisation du travail qui les sous-tendaient. Cette évolution a suscité de nombreux débats au sein du monde savant, mais aussi dans la sphère publique, en France comme au niveau international. En dresser un bilan global paraît aujourd’hui une tâche nécessaire et urgente.  
[EHESS, États généraux de l'édition en sciences humaines et sociales](https://editionshs2019.sciencesconf.org/)

Voilà un événement nécessaire, cette journée organisée par l'École des hautes études en sciences sociales et les Éditions de l’EHESS a par ailleurs été _captée_, les vidéos sont [en ligne](https://editionshs2019.sciencesconf.org/resource/page/id/1).
