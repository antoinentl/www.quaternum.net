---
layout: post
title: "Femmes de l'ESR"
date: "2019-11-15T22:00:00"
comments: true
published: true
description: "Des conseils avisés dont les hommes devraient s'inspirer dans le domaine de l'enseignement supérieur et de la recherche (ESR)."
categories:
- flux
tags:
- métier
slug: femmes-de-l-esr
---
> Ce texte est dérivé d’une série de billets conçue pour Twitter, en réaction à un tweet concernant les stéréotypes pesant sur les femmes dans le monde académique.  
[Charlotte, Femmes de l’ESR: le guide dont vous êtes l’héroïne](https://academia.hypotheses.org/4320)

Des conseils avisés dont les hommes devraient s'inspirer dans le domaine de l'enseignement supérieur et de la recherche (ESR).
