---
layout: post
title: "Les humanités numériques temporaires"
date: "2019-09-06T22:00:00"
comments: true
published: true
description: "Au détour d'échanges sur la liste de diffusion française sur les humanités numériques, Gautier replace la question des humanités numériques sous l'angle du rapport entre le chercheur et l'ingénieur, et pose la nécessaire question de la fin de ce qui ne doit pas devenir une discipline."
categories:
- flux
tags:
- métiers
---
> Un jour, peut-être, arrêterons-nous d'opposer le travail du chercheur et celui de l'ingénieur dans les [sciences humaines et sociales] ? Peut-être qu'un jour l'un et l'autre groupe reconnaîtront le rôle de chacun ?  
[Gautier, Repenser la place du numérique dans les SHS](https://www.lespetitescases.net/repenser-la-place-du-numerique-dans-les-shs)

Au détour d'échanges sur la liste de diffusion française sur les humanités numériques, Gautier replace la question des humanités numériques sous l'angle du rapport entre le chercheur et l'ingénieur, et pose la nécessaire question de la fin de ce qui ne doit pas devenir une discipline.
