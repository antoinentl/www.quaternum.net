---
layout: post
title: "Stratégie du statique"
date: "2019-10-16T22:00:00"
comments: true
published: true
description: "GitHub communique plus massivement sur GitHub Pages, présentant cet ensemble d'outil comme un moyen de publier."
categories:
- flux
tags:
- publication
---
> Build a personal website that shows off your contributions, interests, and development experience. It’s your fully-customizable profile powered by the GitHub API, GitHub Pages, and Jekyll—ready for anyone interested in your work—as soon as you’re ready to share it.  
[GitHub, Personal website generator](https://github.dev/)

GitHub communique plus massivement sur GitHub Pages, présentant cet ensemble d'outil comme un moyen de publier.
