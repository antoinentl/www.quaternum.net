---
layout: post
title: "ELPUB 2019"
date: "2019-05-13T22:00:00"
comments: true
published: true
description: "Dommage que l'appel à communication ait été timidement diffusé, j'aurais aimé proposer un sujet autour de la diversité des modes de conception et de fabrication des livres."
categories:
- flux
tags:
- publication
---
> In 2019, the Electronic Publishing conference will expand your horizons and perceptions! Taking as an inspirational starting point the concept of bibliodiversity, a term coined by Chilean publishers in the 1990s, the forum will revisit its definition and explore what it means today.  
[ELPUB 2019](https://elpub2019.hypotheses.org/)

Dommage que l'appel à communication ait été timidement diffusé, j'aurais aimé proposer un sujet autour de la diversité des modes de conception et de fabrication des livres.

Mise à jour : les supports de présentation (_slides_) ont été mis en ligne : [https://elpub2019.hypotheses.org/slides](https://elpub2019.hypotheses.org/slides).
