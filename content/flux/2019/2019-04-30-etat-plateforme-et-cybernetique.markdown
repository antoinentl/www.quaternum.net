---
layout: post
title: "État, plateforme et cybernétique"
date: "2019-04-30T22:00:00"
comments: true
published: true
description: "Un mémoire qui suscite la curiosité (et qui est publié sous forme de site web)."
categories:
- flux
tags:
- privacy
---
> Qu'est-ce que l'État-Plateforme ? Partants d'une relecture du cours Appareils d'États et Machines de Guerre (1979) de Gilles Deleuze, notre hypothèse est que la « plateformisation du gouvernement » peut-être comprise comme un tissu complexe d'alliances et de mises en compétition d'appareils de captures, où les États et les plateformes, modèlent l'espace d'internet, et agissent désormais comme réseaux de dispositifs entremêlés, dans un champ d'immanence dont la cybernétique est la nouvelle technologie de gouvernement.  
[Meven Marchand Guidevay, État, plateforme et cybernétique](http://www--arc.com/)

Un mémoire de [Meven Marchand Guidevay](http://hindelstark.eu/) qui suscite la curiosité (et qui est publié sous forme de site web).
