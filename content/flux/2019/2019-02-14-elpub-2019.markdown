---
layout: post
title: "ELPUB 2019"
date: "2019-02-14T22:00:00"
comments: true
published: true
description: "L'Electronic Publishing conference, un événement incontournable."
categories:
- flux
tags:
- publication
---
> In 2019, the Electronic Publishing conference will expand your horizons and perceptions! Taking as an inspirational starting point the concept of bibliodiversity, a term coined by Chilean publishers in the 1990s, the forum will revisit its definition and explore what it means today. Being organised five years after the adoption of the International Declaration of Independent Publishers to Promote and Strengthen Bibliodiversity Together, supported in 2014 by 400 publishers from 45 countries, the conference aims to bring together the enquiring academic, professional and publishing industry minds keen to explore the ever evolving nature of the knowledge transmission within human societies.  
[ELPUB, ELPUB 2019](https://elpub2019.hypotheses.org/)

L'Electronic Publishing conference, un événement incontournable, dommage qu'il n'y ait pas eu plus de communication sur l'appel à participation, trop tard pour moi...
