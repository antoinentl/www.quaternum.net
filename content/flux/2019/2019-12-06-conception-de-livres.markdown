---
layout: post
title: "Conception de livres"
date: "2019-12-06T22:00:00"
comments: true
published: true
description: "Une belle sélection (et des découvertes dans mon cas)."
categories:
- flux
tags:
- livre
---
> Books are important. Design books encapsulate works of an artist or a designer in a matter that digital can not do just yet.  
[Anton Repponen, Design Books - A Personal Selection](https://readymag.com/repponen/designbooks/)

Une belle sélection (et des découvertes dans mon cas).
