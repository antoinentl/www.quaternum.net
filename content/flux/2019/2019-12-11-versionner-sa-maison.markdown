---
layout: post
title: "Versionner sa maison"
date: "2019-12-11T22:00:00"
comments: true
published: true
description: "Et pourquoi ne pas versionner le dossier home de son ordinateur ? Antonio Martinović donne quelques conseils utiles pour sauvegarder et versionner nos fichiers de configuration (valable uniquement pour des systèmes Unix)."
categories:
- flux
tags:
- outils
---
> Git is an incredibly useful tool for a programmer, you use it to version the work you do and distribute it easily on other computers. But over the years, I’ve found that a lot of my workflow depends on various configurations and helper scripts I have in my path. So why not version those as well, it does allow you to get started in seconds in a very familiar environment on every new computer or even a server if you spend a lot of your time in SSH sessions.  
[Antonio Martinović, Home .git](https://martinovic.blog/post/home_git/)

Et pourquoi ne pas versionner le dossier `home` de son ordinateur ?
Antonio Martinović donne quelques conseils utiles pour sauvegarder et versionner nos fichiers de configuration (valable uniquement pour des systèmes Unix).
