---
layout: post
title: "L'interstice des jeux de données"
date: "2019-06-17T22:00:00"
comments: true
published: true
description: "Quelques pistes de réflexion sur l'interconnexion des données dans une perspective de web sémantique."
categories:
- flux
tags:
- publication
slug: l-interstice-des-jeux-de-donnees
---
> Il me semble que tout est là lorsque se pose le problème de la diversité au sein des données liées et de leur interconnexion. L’enjeu ne se situe pas nécessairement dans l’expressivité de la description même des données, mais plutôt dans l’interstice de ces jeux de données, à la lisière entre deux écosystèmes, là où les données doivent dialoguer et s’interpréter.  
[Nicolas Sauret, Ça se joue aux interstices](http://nicolassauret.net/carnet/2019/04/26/ca-se-joue-aux-interstices/)

Quelques pistes de réflexion sur l'interconnexion des données dans une perspective de web sémantique.
