---
layout: post
title: "Reconfigurer les processus de publication"
date: "2019-05-27T22:00:00"
comments: true
published: true
description: "Le séminaire IMPEC (Interactions Multimodales Par ECran) m'a donné l'occasion d'évoquer mon mémoire sous une forme quelque peu différente, la vidéo est en ligne, ainsi que le support de présentation."
categories:
- flux
tags:
- privacy
---
> Au-delà du succès mitigé du livre numérique, ce dernier impacte la façon de fabriquer des publications ou des livres, qu'ils soient imprimés ou _dématérialisés_.  
[Antoine Fauchié, L'impact du livre numérique sur les pratiques d'édition : reconfiguration des processus de publication](https://impec.sciencesconf.org/resource/page/id/52)

Le séminaire IMPEC (Interactions Multimodales Par ECran) m'a donné l'occasion d'évoquer mon mémoire sous une forme quelque peu différente, [la vidéo est en ligne](https://impec.sciencesconf.org/resource/page/id/52), ainsi que [le support de présentation](https://presentations.quaternum.net/impact-du-livre-numerique-sur-les-pratiques-d-edition/).
