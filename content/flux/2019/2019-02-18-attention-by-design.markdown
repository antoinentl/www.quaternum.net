---
layout: post
title: "Attention by design"
date: "2019-02-18T22:00:00"
comments: true
published: true
description: "Premiers retours du travail engagé depuis un an par la FING, et plus précisément par Hubert Guillaud et Anthony Masure. Cet entretien permet de découvrir une situation inquiétante et contrastée sur la place du designer dans le design de l'attention."
categories:
- flux
tags:
- design
---
> Donc paradoxalement, votre étude du design de l’attention vous conduit à dire que les designers n’ont pas tant de pouvoir que ça ?  
Oui c’est ça. Et je pense que c’est un problème. Les designers amènent la question du respect de l’usager. Ils ne sont pas là pour fabriquer uniquement un produit marketing, ils imbriquent le rapport aux sciences sociales, l’expérience client et la diversité des utilisateurs. Or si le designer est chassé, l’éthique qu’il apporte l’est en même temps. Il y a là un enjeu important, sur la place de la conception et la façon dont elle est complètement instrumentalisée par le marketing. Et ça pose des problèmes à très court terme.  
[Hubert Guillaud et Anthony Masure, On assiste à une perte de pouvoir des designers face au marketing](https://digital-society-forum.orange.com/fr/les-actus/1189-on-assiste-a-une-perte-de-pouvoir-des-designers-face-au-marketing-)

Premiers retours du travail engagé depuis un an par la FING, et plus précisément par Hubert Guillaud et Anthony Masure.
Cet entretien permet de découvrir une situation inquiétante et contrastée sur la place du designer dans le design de l'attention.
