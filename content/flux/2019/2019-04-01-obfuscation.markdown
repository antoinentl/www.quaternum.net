---
layout: post
title: "Obfuscation"
date: "2019-04-01T22:00:00"
comments: true
published: true
description: "Un livre de Finn Brunton et Helen Nissenbaum."
categories:
- flux
tags:
- livre
---
> Dans ce monde de la sélection par des algorithmes, de la publicité ciblée et du marché des données personnelles, rester maîtres de nos actions, de nos relations, de nos goûts, de nos navigations et de nos requêtes implique d'aller au delà de la longue tradition de l'art du camouflage. Si on peut difficilement échapper à la surveillance numérique, ou effacer ses données, il est toujours possible de noyer nos traces parmi de multiples semblables, de créer nous-mêmes un brouillard d'interactions factices.  
[C&F éditions, Obfuscation](https://cfeditions.com/obfuscation/)

Un livre de Finn Brunton et Helen Nissenbaum.
