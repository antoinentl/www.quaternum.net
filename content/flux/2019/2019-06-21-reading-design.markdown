---
layout: post
title: "Reading Design"
date: "2019-06-21T22:00:00"
comments: true
published: true
description: "Une initiative enthousiasmante (même si on déplorera le recours à Squarespace), même si ce n'est que le début (personnellement je ne retrouve pas des auteurs que j'aimerais lire, de Norman Potter à Robin Kinross en passant par Jan Tschichold). Et sinon il y a toujours Monoskop (au spectre plus large)."
categories:
- flux
tags:
- design
---
> Reading Design is an online archive of critical writing about design. The idea is to embrace the whole of design, from architecture and urbanism to product, fashion, graphics and beyond.  
[Reading Design](https://www.readingdesign.org)

Une initiative enthousiasmante (même si on déplorera le recours à Squarespace), même si ce n'est que le début (personnellement je ne retrouve pas des auteurs que j'aimerais lire, de Norman Potter à Robin Kinross en passant par Jan Tschichold).
Et sinon il y a toujours [Monoskop](https://monoskop.org) (au spectre plus large).
