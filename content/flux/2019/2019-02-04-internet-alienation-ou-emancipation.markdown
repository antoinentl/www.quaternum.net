---
layout: post
title: "Internet, aliénation ou émancipation ?"
date: "2019-02-04T22:00:00"
comments: true
published: true
description: "Un film documentaire très intéressant sur le fond, sur le rapport que nous entretenons avec Internet et le Web, et comment appréhender cet espace sans qu'il perde son intérêt. Les intervenants, un chercheur, des développeurs libristes, des passionnés et des membres d'associations, exposent des problèmes complexes avec beaucoup de pédagogie. Je suis moins fan du titre et de certains aspects de la réalisation, mais c'est un document bien utile !"
categories:
- flux
tags:
- web
---
> Un film documentaire en 10 chapitres, composé d'interviews autour du sujet complexe des données personnelles : nos usages du web, les dérives, les espoirs de la loi RGPD, les solutions locales, etc.
[Big Blue Eyes Productions, Internet, aliénation ou émancipation ?](https://peertube.fr/videos/watch/ea2d5153-4704-4a6e-8a13-d09a411c9760)

Un film documentaire très intéressant sur le fond, sur le rapport que nous entretenons avec Internet et le Web, et comment appréhender cet espace sans qu'il perde son intérêt.
Les intervenants, un chercheur, des développeurs libristes, des passionnés et des membres d'associations, exposent des problèmes complexes avec beaucoup de pédagogie.
Je suis moins fan du titre et de certains aspects de la réalisation, mais c'est un document bien utile !
