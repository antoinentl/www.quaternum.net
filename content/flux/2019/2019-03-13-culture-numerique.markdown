---
layout: post
title: "Culture numérique"
date: "2019-03-13T22:00:00"
comments: true
published: true
description: "Une référence qui semble s'imposer comme incontournable."
categories:
- flux
tags:
- livre
---
> Si nous fabriquons le numérique, il nous fabrique aussi. Voilà pourquoi il est indispensable que nous nous forgions une culture numérique.  
[Dominique Cardon, Culture numérique](http://www.pressesdesciencespo.fr/fr/livre/?GCOI=27246100540390&fa=author&person_id=1201)

Une référence qui semble s'imposer comme incontournable.
[À écouter également sur France Culture](https://www.franceculture.fr/emissions/la-grande-table-2eme-partie/culture-numerique-enjeux-du-xxieme-siecle), ce livre ayant une visibilité médiatique assez forte.
