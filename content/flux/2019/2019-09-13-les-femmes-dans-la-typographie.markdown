---
layout: post
title: "Les femmes dans la typographie"
date: "2019-09-13T22:00:00"
comments: true
published: true
description: "Les femmes dans la typographie"
categories:
- flux
tags:
- typographie
---
> Type design plays a fundamental role in visual communication: it is crucial to the textual representation of languages to afford literacies to global communities. Histories to date have largely overlooked type design’s importance, and concomitantly the key contributors to the type-design and manufacturing processes that developed in the twentieth century. Women were often central to this development, particularly in Britain within the major type-manufacturing companies of Monotype and Linotype.  
[University of Reading, Women in Type](https://research.reading.ac.uk/women-in-type/our-research/)
