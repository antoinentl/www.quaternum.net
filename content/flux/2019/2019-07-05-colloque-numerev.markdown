---
layout: post
title: "Colloque Numerev"
date: "2019-07-05T22:00:00"
comments: true
published: true
description: "J'ai suivi de très loin le colloque Numerev, le programme est pourtant très riche, et les vidéos disponibles en ligne : https://www.youtube.com/watch?v=-WHoTXw6Two."
categories:
- flux
tags:
- publication
---
> Faire dialoguer les disciplines via l’indexation des connaissances: la recherche interdisciplinaire en débats  
[Numerev](https://numerev.com/programme-colloque-numerev.html)

J'ai suivi de très loin le colloque Numerev, le programme est pourtant très riche, et les vidéos disponibles en ligne : [https://www.youtube.com/watch?v=-WHoTXw6Two](https://www.youtube.com/watch?v=-WHoTXw6Two).
