---
layout: post
title: "Produire un article académique à partir de Markdown"
date: "2019-01-14T22:00:00"
comments: true
published: true
description: "Dans la série des recherches et expérimentations visant à se libérer de l'environnement des traitements de texte, dont Arthur Perret contribue largement, mais aussi le projet Stylo du laboratoire québécois Écritures numériques, Raphael Kabo propose un logiciel tout en un, basé sur Ulysses, Zotero et Pandoc. Un beau projet, qu'il faudrait relier à d'autres initiatives (il faudrait que je crée un répertoire pour les recenser)."
categories:
- flux
tags:
- privacy
---
> I wanted to write in Markdown in the amazing MacOS text editor Ulysses, using citation keys automatically generated by Zotero for my footnotes and bibliography. But I (and my supervisor, and eventually my viva panel) also wanted to see my work in easily readable Word documents, with the footnotes embedded, the citations properly formatted, and any necessary styles integrated.  
[Raphael Kabo, Introducing DocDown, the easy way to turn academic Markdown into Word documents](https://raphaelkabo.com/blog/posts/introducing-docdown/)

Dans la série des recherches et expérimentations visant à se libérer de l'environnement des traitements de texte, à laquelle [Arthur Perret](https://arthurperret.fr/2018/12/04/semantique-et-mise-en-forme/) contribue largement, mais aussi le projet [Stylo](https://stylo.ecrituresnumeriques.ca/) du laboratoire québécois Écritures numériques, Raphael Kabo propose un logiciel tout en un, basé sur Ulysses, Zotero et Pandoc.
Un beau projet, qu'il faudrait relier à d'autres initiatives (il faudrait que je crée un répertoire pour les recenser).
