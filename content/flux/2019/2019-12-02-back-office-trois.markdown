---
layout: post
title: "Back Office trois"
date: "2019-12-03T22:00:00"
comments: true
published: true
description: "Le troisième numéro de la revue Back Office, une revue entre design graphique et pratiques numériques, est consacrée à l'écriture."
categories:
- flux
tags:
- livre
---
> Bien que les matrices de pixels rétroéclairées peuplent notre quotidien, il n’existe étrangement que peu de textes traitant ; depuis les pratiques de design ; de leurs enjeux utilitaires, cognitifs, sémantiques et sensibles.  
[Kévin Donnot, Élise Gay et Anthony Masure, Écrire l’écran](http://www.revue-backoffice.com/numeros/03-ecrire-lecran)

Le troisième numéro de la revue _Back Office_, "une revue entre design graphique et pratiques numériques", est consacrée à l'écriture.
