---
layout: post
title: "C'est quoi une startup ?"
date: "2019-01-29T22:00:00"
comments: true
published: true
description: "Hubert Guillaud s'est entretenu avec Michel Grossetti, Jean-François Barthe et Nathalie Chauvac qui ont réalisé une longue enquête sur startups."
categories:
- flux
tags:
- métier
slug: c-est-quoi-une-startup
---
> L’entrepreneur solitaire ayant une vision précise est en effet une figure un peu abstraite et théorique qui correspond rarement à la réalité.  
[Michel Grossetti, Jean-François Barthe et Nathalie Chauvac, Startupeurs, des innovateurs ordinaires](http://www.internetactu.net/2019/01/28/startupeurs-des-innovateurs-ordinaires/)

Hubert Guillaud s'est entretenu avec Michel Grossetti, Jean-François Barthe et Nathalie Chauvac qui ont réalisé une longue enquête sur startups.
