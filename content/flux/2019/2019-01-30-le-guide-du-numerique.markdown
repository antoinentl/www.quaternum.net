---
layout: post
title: "Le guide du numérique"
date: "2019-01-30T22:00:00"
comments: true
published: true
description: "Parce que c'est drôle et souvent très vrai..."
categories:
- flux
tags:
- métier
---
> Brief n.: Funny questions with funny answers about brands, business, clients. Also known as the opposite of the final deliverable.  
[Joana Delbone et Ștefania Man, The digital handbook for agency people](https://fakeit.digital)

Parce que c'est drôle et souvent très vrai...
