---
layout: post
title: "Un numéro sur la documentation"
date: "2019-12-04T22:00:00"
comments: true
published: true
description: "Une revue consacrée à l'ingénierie logicielle d'aussi belle facture est étonnante, un numéro dédié à la documentation dans le domaine de l'informatique l'est d'autant plus !"
categories:
- flux
tags:
- publication
---
> This issue of Increment explores documentation as an approachable, essential, and innovative part of engineering culture.  
[Increment, Documentation](https://www.editionsducommun.org/blogs/podcasts)

Une revue consacrée à l'ingénierie logicielle d'aussi belle facture est étonnante, un numéro dédié à la documentation dans le domaine de l'informatique l'est d'autant plus !
"A primer on documentation content strategy", "Why it’s worth it to invest in internal docs", "What a deploy bot taught us about documentation", autant d'articles parmi d'autres qui parlent de pratiques d'écriture autour de la programmation.
