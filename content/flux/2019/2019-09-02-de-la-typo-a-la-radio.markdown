---
layout: post
title: "De la typo à la radio"
date: "2019-09-02T22:00:00"
comments: true
published: true
description: "Trop peu de temps consacré à la typographie et à celle et ceux qui l'incarnent, dans l'action et le militantisme, ces cinquante deux minutes sont donc précieuses."
categories:
- flux
tags:
- typographie
---
> Metteur en page, imposeur, paquetier, tableautier... Les typographes, en charge de la composition des imprimés, furent longtemps considérés comme les aristocrates de la classe ouvrière, détenant un savoir ancestral, manuel autant qu'intellectuel.  
[France Culture, Les typos sur le carreau](https://www.franceculture.fr/emissions/la-fabrique-de-lhistoire/histoire-des-metiers-44-les-typos-sur-le-carreau)

Trop peu de temps consacré à la typographie et à celle et ceux qui l'incarnent, dans l'action et le militantisme, ces cinquante deux minutes sont donc précieuses.
