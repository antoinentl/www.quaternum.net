---
layout: post
title: "Un gabarit pour livre libre"
date: "2019-02-28T22:00:00"
comments: true
published: true
description: "Abrüpt, la maison d'édition suisse étonnante, documente les outils qu'elle utilise pour fabriquer ses livres, principalement Markdown, Pandoc, Pandoc-citeproc, LaTeX et Makefile. Une chaîne composée de programmes libres, une chaîne modulaire (ou presque)."
categories:
- flux
tags:
- publication
---
> Une boîte à outils pour créer facilement un livre numérique ou papier  
[Abrüpt, Gabarit Abrupt](https://gitlab.com/cestabrupt/gabarit-abrupt)

[Abrüpt](https://abrupt.ch), la maison d'édition suisse étonnante, documente les outils qu'elle utilise pour fabriquer ses livres, principalement Markdown, Pandoc, Pandoc-citeproc, LaTeX et Makefile.
Une chaîne composée de programmes libres, une chaîne modulaire (ou presque).

Via [Linuxfr.org](https://linuxfr.org/users/rodhlann/journaux/gabarit-abrupt-pour-livre-libre).
