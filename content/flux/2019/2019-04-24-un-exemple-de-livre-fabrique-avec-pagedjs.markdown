---
layout: post
title: "Un exemple de livre fabriqué avec paged.js"
date: "2019-04-24T22:00:00"
comments: true
published: true
description: "Voici un exemple de livre produit avec paged.js, le polyfill qui permet de fabriquer des publications avec le web – et la fonction imprimer des navigateurs. Ce livre, Printing in Relation to Graphic Art de George French, regroupe toutes les spécificités de la mise en page d'un livre (il manque les notes de bas de page)."
categories:
- flux
tags:
- publication
slug: un-exemple-de-livre-fabrique-avec-pagedjs
---
> Example of a book designed with paged.js, 102 pages.  
[Julie Blanc, PrintingInRelationToGraphicArt](https://gitlab.pagedmedia.org/samples/printing-in-relation-to-graphic-art)

Voici un exemple de livre produit avec [paged.js](https://www.pagedmedia.org/paged-js/), le _polyfill_ qui permet de fabriquer des publications avec le web – et la fonction "imprimer" des navigateurs.
Ce livre, [_Printing in Relation to Graphic Art_ de George French](https://printing-in-relation-to-graphic-art.glitch.me), regroupe toutes les spécificités de la mise en page d'un livre (il manque les notes de bas de page).
