---
layout: post
title: "Balisages la nouvelle revue de recherche de l'enssib"
date: "2019-01-28T22:00:00"
comments: true
published: true
description: "L'École nationale des sciences de l'information et des bibliothèques annonce sa nouvelle revue de recherche, dans le champ des sciences de l'information et des bibliothèques. Le premier numéro verra le jour à l'automne 2019, voici l'appel à articles : Les  objets nativement numériques : transformations et nouveaux enjeux documentaires ?. À vos claviers !"
categories:
- flux
tags:
- publication
slug: balisages-la-nouvelle-revue-de-recherche-de-l-enssib
---
> La revue en ligne Balisages s’inscrit au croisement des sciences de l’information, de la communication et des bibliothèques et d’une anthropologie ouverte (au sens d’un regard et non d’une discipline) des savoirs et des connaissances. Un champ qu’il convient donc de baliser, entre balises numériques et balises sémiotiques, dans des randonnées intellectuelles balisées ou à baliser afin que des chemins prennent formes, se définissent, pour promouvoir ou susciter une intelligence innovante des questions qui touchent aussi bien au document qu’aux données, au papier qu’au numérique comme aux tensions qui les travaillent. Balisages veut questionner les enjeux actuels qui, dans le monde des bibliothèques et de la documentation, nouent les formes du traitement de l’information à celles de la communication.  
[Enssib, Balisages, la revue de recherche de l’Enssib](https://www.enssib.fr/revue-balisages)

L'École nationale des sciences de l'information et des bibliothèques annonce sa nouvelle revue de recherche, dans le champ des sciences de l'information et des bibliothèques.
Le premier numéro verra le jour à l'automne 2019, voici l'appel à articles : "Les  objets nativement numériques : transformations et nouveaux enjeux documentaires ?".
À vos claviers !
