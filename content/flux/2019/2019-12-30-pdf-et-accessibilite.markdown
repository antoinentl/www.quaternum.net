---
layout: post
title: "PDF et accessibilité"
date: "2019-12-30T22:00:00"
comments: true
published: true
description: "Après la considération selon laquelle une page web vaut mieux qu'un document au format PDF, ces recommandations très précises de Sylvie Duchateau permettent de palier aux problèmes récurrents que peuvent rencontres les personnes en situation de handicap."
categories:
- flux
tags:
- accessibilité
---
> Au total, ce sont donc les trois quart des répondants qui déclarent avoir des problèmes (ponctuels ou récurrents) avec les documents PDF.  
[Sylvie Duchateau, PDF et accessibilité : la fausse bonne idée](https://access42.net/pdf-accessibilite)

Après la considération selon laquelle une page web vaut mieux qu'un document au format PDF, ces recommandations très précises de Sylvie Duchateau permettent de palier aux problèmes récurrents que peuvent rencontres les personnes en situation de handicap.
