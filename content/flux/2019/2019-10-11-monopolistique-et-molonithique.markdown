---
layout: post
title: "Monopolistique et monolithique"
date: "2019-10-11T22:00:00"
comments: true
published: true
description: "Un billet daté de quelques années qui souligne combien les traitements de texte, et Word en particulier, sont monopolistiques et monolithiques."
categories:
- flux
tags:
- publication
aliases: monopolistique-et-molonithique
---
> One early and particularly effective combination was the idea of a text file, containing embedded commands or macros, that could be edited with a programmer's text editor (such as ed or teco or, later, vi or emacs) and subsequently fed to a variety of tools: offline spelling checkers, grammar checkers, and formatters like scribe, troff, and latex that produced a binary page image that could be downloaded to a printer.  
[Charlie Stross, Why Microsoft Word must Die](https://www.antipope.org/charlie/blog-static/2013/10/why-microsoft-word-must-die.html)

Un billet daté de quelques années qui souligne combien les traitements de texte, et Word en particulier, sont monopolistiques et monolithiques.
(En lisant Matthew Kirschenbaum je ne suis toutefois pas d'accord avec le fait qu'une création est impossible avec Word comme le prétend Charlie Stross.)
