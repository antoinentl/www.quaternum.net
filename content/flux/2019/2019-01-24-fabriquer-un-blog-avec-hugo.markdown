---
layout: post
title: "Fabriquer un blog avec Hugo"
date: "2019-01-24T22:00:00"
comments: true
published: true
description: "Une ressource utile pour fabriquer un blog avec le générateur de site statique Hugo."
categories:
- flux
tags:
- web
---
> In my opinion, Hugo’s current quick start is ample. It does just what the name says, gets you started you quickly.  
[Zachary Wade Betz, Make a Hugo blog from scratch](https://zwbetz.com/make-a-hugo-blog-from-scratch/)

Une ressource utile pour fabriquer un blog avec le générateur de site statique Hugo.
