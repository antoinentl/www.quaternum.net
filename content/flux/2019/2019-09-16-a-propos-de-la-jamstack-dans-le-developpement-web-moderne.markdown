---
layout: post
title: "À propos de la JAMstack dans le développement web moderne"
date: "2019-09-16T22:00:00"
comments: true
published: true
description: "Un livre/rapport publié par O'Reilly sur la JAMstack, un condensé de pas mal de choses déjà disponibles ici et là (et surtout en français sur Jamstatic.fr). Une lecture nécessaire dans le domaine du développement web, liée mais à dissocier des ouvrages sur les générateurs de site statique."
categories:
- flux
tags:
- web
---
> The JAMstack was born of the stubborn conviction that there was a better way to build for the web. Around 2014, developers started to envision a new architecture that could make web apps look a lot more like mobile apps: built in advance, distributed, and connected directly to powerful APIs and microservices. It would take full advantage of modern build tools, Git workflows, new frontend frameworks, and the shift from monolithic apps towards decoupled frontends and backends.  
[Mathias Biilmann et Phil Hawksworth, Modern Web Development on the JAMstack](https://www.netlify.com/oreilly-jamstack/)

Un livre/rapport publié par O'Reilly sur la JAMstack, un condensé de pas mal de choses déjà disponibles ici et là (et surtout en français sur [Jamstatic.fr](https://jamstatic.fr/)).
Une lecture nécessaire dans le domaine du développement web, liée mais à dissocier des ouvrages sur les générateurs de site statique.
