---
layout: post
title: "Du clavier"
date: "2019-05-03T22:00:00"
comments: true
published: true
description: "À l'occasion de l'édition 2019 des Journées du logiciel libre, Fabien Cazenave a rassemblé beaucoup de ses recherches et recommandations pour une utilisation adéquate du clavier. Inspirant tout autant qu'effrayant."
categories:
- flux
tags:
- outils
---
> 1. Une posture correcte, tu adopteras.
> 2. La dactylographie, tu apprendras.
> 3. La dactylographie, tu apprendras. Vraiment.
> 4. D’un clavier compact, tu t’équiperas.
> 5. De la souris et des flèches, tu t’abstiendras.
> 6. De Vim, tu t’inspireras.
> 7. De dispositions hasardeuses, nul besoin tu n’auras.
>  
[Fabien Cazenave, Ergonomie holistique du clavier](http://fabi1cazenave.github.io/slides/2019-jdll/)

À l'occasion de l'édition 2019 des Journées du logiciel libre, Fabien Cazenave a rassemblé beaucoup de ses recherches et recommandations pour une utilisation adéquate du clavier.
Inspirant tout autant qu'effrayant.
