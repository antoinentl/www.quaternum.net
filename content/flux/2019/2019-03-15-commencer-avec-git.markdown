---
layout: post
title: "Commencer avec Git"
date: "2019-03-15T22:00:00"
comments: true
published: true
description: "Un guide pour débuter avec Git et faire sa première contribution. Un billet qui semble être le premier d'une série sur ce sujet."
categories:
- flux
tags:
- web
---
> Break Git Down is a super beginner-friendly series to help newer developers learn a few of the most important Git tasks that you will certainly use. Git is an essential part of every day life as a developer and my goal is to help make the transition to using Git (and ultimately the command line), a smooth and hopefully easy learning curve.  
[Tae'lur Alexis, Break Git Down: How To Create a Branch From Master and Make Your First Commit!](https://dev.to/taeluralexis/break-git-down-how-to-create-a-branch-from-master-and-make-your-first-commit-2960)

Un guide pour débuter avec Git et faire sa première contribution.
Un billet qui semble être le premier d'une série sur ce sujet.
