---
layout: post
title: "Introduction à XSLT"
date: "2019-09-09T22:00:00"
comments: true
published: true
description: "Une présentation assez rapide de XSLT avec de précieuses ressources."
categories:
- flux
tags:
- outils
---
> XSLT is a _transformation_ language  
[Ashley Blewer, XSLT](https://training.ashleyblewer.com/presentations/xslt.html)

Une présentation assez rapide de XSLT avec de précieuses ressources.
