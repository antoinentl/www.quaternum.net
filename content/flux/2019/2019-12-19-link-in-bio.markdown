---
layout: post
title: "Link in bio"
date: "2019-12-19T22:00:00"
comments: true
published: true
description: "La lente destruction du web par les plateformes silos n'est pas inéluctable, mais elle est bien réelle."
categories:
- flux
tags:
- web
---
> If anyone on Instagram can just link to any old store on the web, how can Instagram — meaning Facebook, Instagram’s increasingly-overbearing owner — tightly control commerce on its platform? If Instagram users could post links willy-nilly, they might even be able to connect directly to their users, getting their email addresses or finding other ways to communicate with them. Links represent a threat to closed systems.  
[Anil Dash, “Link In Bio” is a slow knife](https://anildash.com/2019/12/10/link-in-bio-is-how-they-tried-to-kill-the-web/)

La lente destruction du web par les plateformes silos n'est pas inéluctable, mais elle est bien réelle.

Via [Robin Rendle](https://www.robinrendle.com/notes/link-in-bio-is-a-slow-knife).
