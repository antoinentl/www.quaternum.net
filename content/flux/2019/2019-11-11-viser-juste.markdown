---
layout: post
title: "Viser juste"
date: "2019-11-11T22:00:00"
comments: true
published: true
description: "Quinn Dombrowski, l'auteure de l'ouvrage _Drupal for Humanists_, revient sur les choix technologiques liés à Drupal, avec un sacrée dose d'humilité."
categories:
- flux
tags:
- publication
---
> The minimal computing advocates were right: database-driven websites with dynamic code are inherently fragile, vulnerable things. They’re easy to hack. While high-profile sites with controversial content actively draw the attention of hackers, sometimes sites get hacked for no reason beyond free access to server resources. In Drupal for Humanists, I talked about all the highly configurable, dynamic features that were so easy to implement with Drupal, but never talked about what you lose.  
[Quinn Dombrowski, Sorry for all the Drupal: Reflections on the 3rd anniversary of "Drupal for Humanists"](http://quinndombrowski.com/?q=blog/2019/11/08/sorry-all-drupal-reflections-3rd-anniversary-drupal-humanists)

Quinn Dombrowski, l'auteure de l'ouvrage _Drupal for Humanists_, revient sur les choix technologiques liés à Drupal, avec une sacrée dose d'humilité.
Le choix de l'outil est terriblement complexe, et Drupal impose des contraintes (en tant que système monolithique reposant sur une base de données).

Via [Marcello](http://blog.sens-public.org/marcellovitalirosati/).
