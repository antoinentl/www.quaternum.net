---
layout: post
title: "La puissance de HTML"
date: "2019-07-03T22:00:00"
comments: true
published: true
description: "Même si je ne suis pas un spécialiste du langage HTML j'ai découvert beaucoup de trucs nouveaux."
categories:
- flux
tags:
- web
---
> It's amazing that you can do so much with _just_ HTML.  
[Ananya Neogi, HTML can do that?](https://dev.to/ananyaneogi/html-can-do-that-c0n)

Même si je ne suis pas un spécialiste du langage HTML j'ai découvert beaucoup de trucs nouveaux.
