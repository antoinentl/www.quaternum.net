---
layout: post
title: "Perspective accessible"
date: "2019-05-02T22:00:00"
comments: true
published: true
description: "Vivre une situation de handicap permet de mieux prendre en compte une réalité que l'on ne connaît pas forcément. (Je suis toujours dubitatif sur les prises de parole de personnes qui ne sont pas en situation de handicap, mais cette fois il s'agit bien d'une telle situation, même si elle est temporaire.)"
categories:
- flux
tags:
- accessibilité
---
> In my case, it was a little over a month. Here’s what I learned while going through it.  
[Facundo Carradini, Accessibility for Vestibular Disorders: How My Temporary Disability Changed My Perspective](https://alistapart.com/article/accessibility-for-vestibular/)

Vivre une situation de handicap permet de mieux prendre en compte une réalité que l'on ne connaît pas forcément.
(Je suis toujours dubitatif sur les prises de parole de personnes qui ne sont pas en situation de handicap, mais cette fois il s'agit bien d'une telle situation, même si elle est temporaire.)
