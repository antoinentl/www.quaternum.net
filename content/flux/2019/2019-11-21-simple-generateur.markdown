---
layout: post
title: "Simple générateur"
date: "2019-11-21T22:00:00"
comments: true
published: true
description: "Cette citation n'est pas si révélatrice de ce billet qui parle surtout de Pelican et de simplicité dans le domaine des générateurs de site statique."
categories:
- flux
tags:
- publication
---
> Les solutions les plus simples étant souvent les meilleures, il faut se rappeler qu'on peut tout simplement faire un site statique avec pandoc.  
[Stéphane Bortzmeyer, Les générateurs de site Web statiques, et mon choix de Pelican](https://www.bortzmeyer.org/generateurs-web-statiques.html)

Cette citation n'est pas si révélatrice de ce billet qui parle surtout de Pelican et de simplicité dans le domaine des générateurs de site statique.
L'approche et le ton dénote agréablement par rapport à la tendance à _vendre_ les derniers générateurs de site statique à la mode.
