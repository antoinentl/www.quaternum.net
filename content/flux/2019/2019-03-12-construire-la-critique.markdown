---
layout: post
title: "Construire la critique"
date: "2019-03-12T22:00:00"
comments: true
published: true
description: "Construire la critique technologique, n'est-ce pas vain ?..."
categories:
- flux
tags:
- design
---
> Les mouvements de gauche encore indécis savent ce qui leur reste à faire : s’ils veulent vraiment s’écarter du dogme néolibéral, qui tient la concurrence pour l’instrument politique et social moderne fondamental, ils doivent résister aux tentations rhétoriques et idéologiques de l’« économisme » et de la « technocratie », pour se rallier au projet de transformation démocratique radicale.  
[Evgeny Morozov, Critique technologique : reprendre l’initiative](https://blog.mondediplo.net/critique-technologique-reprendre-l-initiative)

Construire la critique technologique, n'est-ce pas vain ?...
