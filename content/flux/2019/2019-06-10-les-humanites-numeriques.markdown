---
layout: post
title: "Les humanités numériques"
date: "2019-06-10T22:00:00"
comments: true
published: true
description: "Arthur Perret chronique le livre de Pierre Mounier, Les humanités numériques : une histoire critique."
categories:
- flux
tags:
- métier
---
> Cet ouvrage propose un tour d’horizon synthétique des humanités numériques.  
[Arthur Perret, Les humanités numériques : une histoire critique](https://journals.openedition.org/rfsic/5872)

Arthur Perret chronique le livre de Pierre Mounier, _Les humanités numériques : une histoire critique_.
