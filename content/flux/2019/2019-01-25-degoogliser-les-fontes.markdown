---
layout: post
title: "Dégoogliser les fontes"
date: "2019-01-25T22:00:00"
comments: true
published: true
description: "Voici un script permettant de télécharger les fontes de Google d'habitude utilisées via les serveurs de Google. L'intérêt ici est de ne pas dépendre de cette entreprise, et surtout de ne pas être pisté par ce moyen. Malin et minimaliste."
categories:
- flux
tags:
- privacy
---
> A script to de-googlify CSS files, downloading relevant fonts in the process. Because privacy matters and self-hosting is a thing.  
[Michał Woźniak, Fonts Degooglifier](https://git.occrp.org/libre/fonts-degooglifier)

Voici un script permettant de télécharger les fontes de Google d'habitude utilisées via les serveurs de Google.
L'intérêt ici est de ne pas dépendre de cette entreprise, et surtout de ne pas être pisté par ce moyen.
Malin et minimaliste.
