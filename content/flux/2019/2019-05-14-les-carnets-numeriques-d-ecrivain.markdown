---
layout: post
title: "Les carnets numériques d'écrivain"
date: "2019-05-14T22:00:00"
comments: true
published: true
description: "Je n'ai pas encore lu cet article qui semble aborder bon nombre d'interrogations personnelles."
categories:
- flux
tags:
- publication
slug: les-carnets-numeriques-d-ecrivain
---
> La réévaluation du rôle joué par la publication (autonome et médiée) des textes sur le statut d’écrivain et sur leur lectorat conduit à définir la fonction d’implémentation des oeuvres (notion empruntée à Nelson Goodman) comme une clé interprétative de la publication numérique.  
[René Audet, Penser les carnets numériques d’écrivain : écritures médiatisées et réinvestissement de l’idée de publication](https://www.erudit.org/fr/revues/etudlitt/2019-v48-n1-2-etudlitt04438/1057998ar/)

Je n'ai pas encore lu cet article qui semble aborder bon nombre d'interrogations personnelles.
(L'accès à l'article est réservé mais il est possible de négocier [avec l'auteur](https://twitter.com/reneaudet/status/1110912162946527232).)
