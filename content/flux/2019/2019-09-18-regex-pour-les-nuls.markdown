---
layout: post
title: "Regex pour les nuls"
date: "2019-09-18T22:00:00"
comments: true
published: true
description: "Une introduction illustrée à Regex, ou comment apprendre à créer et gérer des expressions régulières."
categories:
- flux
tags:
- outils
---
> The goal of this blog post was to make regex a bit more approachable by means of an illustrated introduction.  
[Jan Meppe, Regex For Noobs (like me!) - An Illustrated Guide](https://www.janmeppe.com/blog/regex-for-noobs/)

Une introduction illustrée à Regex, ou comment apprendre à créer et gérer des expressions régulières.
