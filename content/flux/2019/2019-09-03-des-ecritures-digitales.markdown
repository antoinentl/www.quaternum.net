---
layout: post
title: "Des Écritures digitales"
date: "2019-09-03T22:00:00"
comments: true
published: true
description: "Une approche des écritures numériques qui semble originale, avec une belle bibliographie mais quelques absences. À lire."
categories:
- flux
tags:
- livre
---
> Cet essai souhaite démontrer que l’écriture digitale contribue à l’émergence « d’un nouveau rapport du corps de l’homme aux machines », tel qu’annoncé par Jacques Derrida lorsqu’il commente les effets à venir des nouvelles technologies. Ces liens contractés par l’écriture à la matière digitale et aux corps convient en retour la tradition judéo-chrétienne à réfléchir à leurs conséquences pour ce corpus textuel désigné souvent comme « les Ecritures », soit la Bible.  
[Claire Clivaz, Ecritures digitales: Digital Writing, Digital Scriptures](https://brill.com/view/title/54748)

Une approche des écritures numériques qui semble originale, avec une belle bibliographie mais quelques absences.
À lire.
