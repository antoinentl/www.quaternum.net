---
layout: post
title: "Le contrat d'édition version enfant"
date: "2019-02-12T22:00:00"
comments: true
published: true
description: "L'objectif de cet exercice est louable : réécrire le contrat d'édition classique pour qu'un enfant de 5 ans puisse le comprendre. Si le résultat est quelque peu caricatural, il révèle le gouffre qui sépare l'éditeur de l'auteur, et marque le fait que l'auteur se retrouve dans une situation très défavorable."
categories:
- flux
tags:
- livre
slug: le-contrat-d-edition-version-enfant
---
> On trouve vraiment que tu fais des jolis dessins et que tu écris des supers poésies ! Tu sais, avec le monsieur et la dame on aimerait bien fabriquer un livre pour mettre touuuutes tes jolies histoires dedans !  
On va faire un échange d'accord ?!  
[Julien, eli5_contrat_edition.md](https://gist.github.com/judbd/b7f01a26765848a67bf94d43166c4e10)

L'objectif de cet exercice est louable : réécrire le contrat d'édition classique pour qu'un enfant de 5 ans puisse le comprendre.
Si le résultat est quelque peu caricatural, il révèle le gouffre qui sépare l'éditeur de l'auteur, et marque le fait que l'auteur se retrouve dans une situation très défavorable.
