---
layout: post
title: "Internaliser"
date: "2019-05-29T22:00:00"
comments: true
published: true
description: "Preuve qu'il est possible pour certaines structures ou collectivités de conserver la maîtrise de leur infrastructure numérique. Dommage qu'il y ait eu un amalgame au moment de l'annonce, pointant des scandales récents alors que la Mairie de Paris a dû faire ce choix il y a déjà bien longtemps."
categories:
- flux
tags:
- privacy
---
> Alors qu’elle externalisait jusque-là le stockage de ses données numériques, la ville a investi 16 millions d’euros dans un centre de données pour en reprendre le contrôle.  
[Vincent Fagot, Paris se dote de sa propre infrastructure pour héberger les données de ses administrés](https://www.lemonde.fr/economie/article/2019/05/28/paris-se-dote-de-sa-propre-infrastructure-pour-heberger-les-donnees-de-ses-administres_5468782_3234.html)

Preuve qu'il est possible pour certaines structures ou collectivités de conserver la maîtrise de leur infrastructure numérique.
Dommage qu'il y ait eu un amalgame au moment de l'annonce, pointant des scandales récents alors que la Mairie de Paris a dû faire ce choix il y a déjà bien longtemps.
