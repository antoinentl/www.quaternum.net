---
layout: post
title: "Une vie numérique sans Google ?"
date: "2019-02-08T22:00:00"
comments: true
published: true
description: "Expérimenter une vie numérique sans Google paraît aujourd'hui compromis, ou en tout cas très compliqué ! Mais ce n'est pas parce que c'est difficile qu'il faut abandonner, et c'est l'occasion de nous interroger sur la dimension centralisée d'Internet."
categories:
- flux
tags:
- internet
---
> As part of an experiment to live without the tech giants, I’m cutting Google from my life both by abandoning its products and by preventing myself, technologically, from interacting with the company in any way. Engineer Dhruv Mehrotra built a virtual private network, or VPN, for me that prevents my phone, computers, and smart devices from communicating with the 8,699,648 IP addresses controlled by Google. This will cause some huge headaches for me: The company has created countless genuinely useful products, some that we use intentionally and some invisibly. The trade-off? Google tracks us everywhere.  
[Kashmir Hill, I Cut Google Out Of My Life. It Screwed Up Everything](https://gizmodo.com/i-cut-google-out-of-my-life-it-screwed-up-everything-1830565500)

Expérimenter une vie numérique sans Google paraît aujourd'hui compromis, ou en tout cas très compliqué !
Mais ce n'est pas parce que c'est difficile qu'il faut abandonner, et c'est l'occasion de nous interroger sur la dimension centralisée d'Internet.
