---
layout: post
title: "Les coulisses de mon mémoire"
date: "2019-04-22T22:00:00"
comments: true
published: true
description: "Cet article pour la revue Digital Libraries & Information Sciences est une tentative de montrer les coulisses de mon mémoire, comment j'ai mis en pratique les différents constats ou propositions. (C'est un bon moyen de découvrir de quoi parle le mémoire sans lire le mémoire.)"
categories:
- flux
tags:
- publication
---
> _Vers un système de publication modulaire : éditer avec le numérique_ est un travail de recherche réalisé dans le cadre d’un Master. Outre la spécificité du parcours universitaire dans lequel il s’inscrit, l’originalité de ce mémoire est d’appliquer les principes qui y sont exposés : interopérabilité, modularité et multiformité.  
[Antoine Fauchié, Interopérable, modulaire et multiforme : l’âge d’or de l’édition numérique ? Démonstration par une démarche performative](https://dlis.hypotheses.org/4381)

Cet article pour la revue Digital Libraries & Information Sciences est une tentative de montrer les coulisses de mon mémoire, comment j'ai mis en pratique les différents constats ou propositions.
(C'est un bon moyen de découvrir de quoi parle le mémoire sans lire le mémoire.)
