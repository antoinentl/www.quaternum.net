---
layout: post
title: "Machine sur mesure"
date: "2019-10-30T22:00:00"
comments: true
published: true
description: "À quoi pourrait ressembler un ordinateur pensé pour un usage précis ? Démonstration par l'exemple avec un administrateur système. Étonnamment le résultat est esthétiquement très intéressant."
categories:
- flux
tags:
- outils
---
> After not finding anything suitable, I thought about how a notebook would turn out if it were developed not with design, but the needs of real users in mind. System administrators, for example. Or people serving telecommunications equipment in hard-to-reach places — on roofs, masts, in the woods, literally in the middle of nowhere.  
The results of my thoughts are presented in this article.  
[sukhe, A small notebook for a system administrator](https://habr.com/en/post/437912/)

À quoi pourrait ressembler un ordinateur pensé pour un usage précis ?
Démonstration par l'exemple avec un administrateur système.
Étonnamment le résultat est esthétiquement très intéressant.
(On notera l'usage maladroit du terme design dans la citation, puisque prendre en compte les besoins de l'utilisateur c'est un peu le sens du design.)
