---
layout: post
title: "La mécanique du livre"
date: "2019-11-29T22:00:00"
comments: true
published: true
description: "Une initiative oh combien intéressante (même si je n'ai pas encore écouter un seul épisode), et trop rare : parler de livres du côté de là où on les crée et on les vend."
categories:
- flux
tags:
- publication
---
> Les podcasts des éditions du commun naissent dans un souci de donner à voir notre travail autrement. Dans La mécanique du livre, nous explorons à chaque épisode un grand thème du monde de l’édition : les contrats d’auteurs, la mise en maquette, la relation aux libraires… autant d’activités méconnues de la chaîne du livre que nous nous efforçons de dévoiler dans cette série. Les Lectures, de leur côté, proposent de donner une forme sonore à certains textes des éditions du commun. Chacune reprend un extrait d’un de nos livres, pour vous proposer de le (re)découvrir autrement.  
[Éditions du commun, La mécanique du livre](https://www.editionsducommun.org/blogs/podcasts)

Une initiative oh combien intéressante (même si je n'ai pas encore écouter un seul épisode), et trop rare : parler de livres du côté de là où on les crée et on les vend.
