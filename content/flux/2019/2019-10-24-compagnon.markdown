---
layout: post
title: "Compagnon"
date: "2019-10-24T22:00:00"
comments: true
published: true
description: "Une fonte créée par des étudiant·e·s de l'École européenne supérieure d'art de Bretagne de Rennes, qui semble pertinente autant pour du texte que du code."
categories:
- flux
tags:
- typographie
---
> La scripte de la famille compagnon est composée avec les contraintes d'un caractères á chasse fixe mais aussi avec des liaisons entre les caractères bas de casse.  
[Juliette Duhé, Chloé Lozano, Valentin Papon, Léa Pradine et Sébastien Riollier, Compagnon](http://compagnon.eesab.fr)

Une fonte créée par des étudiant·e·s de l'École européenne supérieure d'art de Bretagne de Rennes, qui semble pertinente autant pour du texte que du code.
