---
layout: post
title: "Internet ou la révolution du partage"
date: "2019-06-12T22:00:00"
comments: true
published: true
description: "Un documentaire encore visible jusqu'au 4 août 2019, et qui fait un panorama pertinent du libre, comprenant Internet mais surtout les nombreuses initiatives autour du logiciel ou des pratiques libres, en passant par les communs. Un film utile, à diffuser largement, mais qui n'apprendra pas beaucoup à celles et ceux qui s'intéressent déjà au sujet."
categories:
- flux
tags:
- privacy
---
> Internet et le numérique sont au cœur de presque toutes les activités humaines. Ont-ils contribué à faire de nous des citoyens plus autonomes ou des consommateurs captifs ? État des lieux de la liberté informatique en Inde, aux États-Unis et en Europe.  
[Philippe Borrel, Internet ou la révolution du partage](https://www.arte.tv/fr/videos/077346-000-A/internet-ou-la-revolution-du-partage/)

Un documentaire encore visible jusqu'au 4 août 2019, et qui fait un panorama pertinent du libre, comprenant Internet mais surtout les nombreuses initiatives autour du logiciel ou des pratiques libres, en passant par les communs.
Un film utile, à diffuser largement, mais qui n'apprendra pas beaucoup à celles et ceux qui s'intéressent déjà au sujet.
