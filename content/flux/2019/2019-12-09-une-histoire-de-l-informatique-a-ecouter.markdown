---
layout: post
title: "Une histoire de l'informatique (à écouter)"
date: "2019-12-09T22:00:00"
comments: true
published: true
description: "Un podcast de Saron Yitbarek produit par l'entreprise Red Hat, qui surprend un peu par son nom, les héros de la ligne de commande, mais qui intrigue par ses sujets vastes à travers déjà plusieurs saisons.
Une histoire de l'informatique à écouter."
categories:
- flux
tags:
- métier
slug: une-histoire-de-l-informatique-a-ecouter
---
> An original podcast about the people who transform technology from the command line up.  
[Saron Yitbarek, Command Line Heroes](https://www.redhat.com/en/command-line-heroes)

Un podcast de Saron Yitbarek produit par l'entreprise Red Hat, qui surprend un peu par son nom, "les héros de la ligne de commande", mais qui intrigue par ses sujets vastes à travers déjà plusieurs saisons.
Une histoire de l'informatique (et de la technologie contemporaine ?) à écouter.
