---
layout: post
title: "Synchroniser des dépôts"
date: "2019-02-25T22:00:00"
comments: true
published: true
description: "Pour aller plus loin dans la décentralisation permise par Git, il arrive parfois qu'un même dépôt (sur votre machine) soit envoyé vers plusieurs dépôts (en ligne), et donc vers plusieurs plateformes. Voici quelques recommandations pratiques pour synchroniser un dépôt local vers plusieurs plateformes."
categories:
- flux
tags:
- outils
---
> Sharing code online is pretty easy these days. But keeping in sync your repos on multiples places is a bit harder. You will easily find scripts and commands to import/export stuff somewhere. Same thing for read-only mirrors, pretty easy. But having a transparent workflow to be able to push your code on multiple places is not that easy. But hey, it's not hard neither.  
[Maxime Thirouin, Keep in sync your Git repos on GitHub, GitLab & Bitbucket](https://moox.io/blog/keep-in-sync-git-repos-on-github-gitlab-bitbucket/)

Pour aller plus loin dans la décentralisation permise par Git, il arrive parfois qu'un même dépôt (sur votre machine) soit envoyé vers plusieurs dépôts (en ligne), et donc vers plusieurs plateformes.
Voici quelques recommandations pratiques pour synchroniser un dépôt local vers plusieurs plateformes.
