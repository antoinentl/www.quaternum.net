---
layout: post
title: "Digital Publishing Summit 2019"
date: "2019-05-17T22:00:00"
comments: true
published: true
description: "Programme très attrayant que cette nouvelle édition du Digital Publishing Summit 2019, notamment les derniers développements autour de Readium, une présentation de Paged.js ou encore des avancées dans le domaine de l'accessibilité."
categories:
- flux
tags:
- ebook
---
> Here is an introduction to the program of the Digital Publishing Summit 2019.  
[EDRLab, Digital Publishing Summit 2019](http://blog.univ-angers.fr/rj45/2019/04/29/un-accord-de-mauvais-principes/)

Programme très attrayant que cette nouvelle édition du Digital Publishing Summit 2019, notamment les derniers développements autour de Readium, une présentation de Paged.js ou encore des avancées dans le domaine de l'accessibilité.
