---
layout: post
title: "Jekyll et LaTeX"
date: "2019-01-03T22:00:00"
comments: true
published: true
description: "Suite du cheminement d'Arthur Perret, ce jeune et brillant doctorant qui associe le générateur de site statique Jekyll au système de publication LaTeX pour produire un site web de chercheur. Un billet clair, pédagogique et à mettre entre toutes les mains de chercheurs."
categories:
- flux
tags:
- publication
---
>La fragmentation de notre présence éditoriale et le caractère souvent insatisfaisant des plateformes professionnelles conduit certains chercheurs à mettre en place un site à leur façon pour diffuser leurs travaux, ou au moins centraliser l’accès à leur production. C’est ce que j’ai entrepris, en faisant des choix qui me tiennent à cœur et que je souhaitais expliquer en quelques lignes.  
[Arthur Perret, Dr Jekyll & Mr TeX](https://arthurperret.fr/2018/12/17/dr-jekyll-et-mr-tex/)

Suite du cheminement d'Arthur Perret, ce jeune et brillant doctorant qui associe le générateur de site statique Jekyll au système de publication LaTeX pour produire un site web de chercheur.
Un billet clair, pédagogique et à mettre entre toutes les mains de chercheurs.
