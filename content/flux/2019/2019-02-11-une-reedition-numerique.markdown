---
layout: post
title: "Une réédition numérique"
date: "2019-02-11T22:00:00"
comments: true
published: true
description: "Nicholas Rougeux se spécialise dans la réédition numérique de beaux ouvrages, ses réalisations web sont magnifiques, et celle-ci est particulièrement réussie : https://c82.net/euclid/"
categories:
- flux
tags:
- livre
---
> A reproduction of Euclid’s Elements by Oliver Byrne from 1847 that pays tribute to the beautiful original design and includes enhancements such as interactive diagrams, cross references, and posters.  
[Nicholas Rougeux, Byrne’s Euclid](https://c82.net/work/?id=372)

Nicholas Rougeux se spécialise dans la réédition numérique de beaux ouvrages, ses réalisations web sont magnifiques, et celle-ci est particulièrement réussie : [https://c82.net/euclid/](https://c82.net/euclid/).
C'est un très bon exemple de l'intérêt d'une version web d'un livre imprimé classique : design, ergonomie, interactivité.
