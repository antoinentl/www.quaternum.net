---
layout: post
title: "Baliser les présentations"
date: "2019-03-08T22:00:00"
comments: true
published: true
description: "Arthur continue son épopée parmi les solutions de publication ouvertes, explorant cette fois les façon de produire un support de présentation – ou diaporama –, à partir d'un fichier au format Markdown, et en respectant les impératifs de la communication scientifique."
categories:
- flux
tags:
- publication
---
> Un autre type de communication extrêmement répandue en sciences est la présentation (ou diaporama). Je montre ici comment on peut y appliquer le même genre de processus que dans les billets précédents, avec quelques spécificités vraiment enthousiasmantes. Plutôt qu’un tutoriel, il s’agit de partager à chaud quelques lignes de code et les premières questions soulevées par l’expérience.  
[Arthur Perret, Le diaporama au temps du balisage](https://arthurperret.fr/2019/03/07/le-diaporama-au-temps-du-balisage/)

Arthur continue son épopée parmi les solutions de publication ouvertes, explorant cette fois les façon de produire un support de présentation – ou diaporama –, à partir d'un fichier au format Markdown, et en respectant les impératifs de la communication scientifique.
Un billet qui me fait penser [à celui-ci](/2012/11/05/reveal-js-des-presentations-en-html/), écrit maladroitement il y a déjà quelques années.
