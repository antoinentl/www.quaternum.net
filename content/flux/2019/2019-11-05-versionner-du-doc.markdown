---
layout: post
title: "Versionner du .doc"
date: "2019-11-05T22:00:00"
comments: true
published: true
description: "Quelle idée de vouloir versionner un document .doc, un document Word ?! Mais après quelle bonne idée de vouloir versionner ce type de format !"
categories:
- flux
tags:
- outils
slug: versionner-du-doc
---
> For some time now, I have been using Git to keep track of my work - both source code and otherwise. Even when you’re not using it for code, Git is perfect - easy backups to a Git server in the cloud along with the possibility of maintaining multiple working versions of my papers and writing projects is just great.  
> Except - Git treats my Word files as binaries.  
> [rishi Olickel, Tracking Word Documents with Git](https://hrishioa.github.io/tracking-word-documents-with-git/)

Quelle idée de vouloir versionner un document .doc, un document Word ?!
Mais après quelle bonne idée de vouloir versionner ce type de format !