---
layout: post
title: "La circulation du livre"
date: "2019-02-07T22:00:00"
comments: true
published: true
description: "Un billet de présentation du livre Le livre-échange des mêmes auteurs qui met en avant la facilité de circulation du livre imprimé, notamment par rapport au livre numérique – sans grande surprise mais encore fallait-il le démontrer."
categories:
- flux
tags:
- livre
---
> Mais ce qui nous a d’abord frappé, c’est la circulation très active des livres imprimés (et par contraste la faible circulation des livres numériques). Le livre une fois vendu en librairie ou sur une plate-forme en ligne, possède plusieurs vies. Il peut être prêté en effet, offert en cadeau, mais aussi revendu d’occasion, en ligne ou dans les magasins spécialisés. Et il peut faire plusieurs fois la boucle et être revendu encore ; autant de moments de circulation rarement pris en compte dans le bilan général de l’édition.  
[Dominique Boullier, Marianig Le Béchec et Maxime Crépel, Autour du livre, de nouvelles pratiques alimentent l’intelligence collective](https://theconversation.com/autour-du-livre-de-nouvelles-pratiques-alimentent-lintelligence-collective-107571)

Un billet de présentation du livre [Le livre-échange](https://cfeditions.com/livre-echange/) des mêmes auteurs qui met en avant la facilité de circulation du livre imprimé, notamment par rapport au livre numérique – sans grande surprise mais encore fallait-il le démontrer.
