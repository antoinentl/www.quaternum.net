---
layout: post
title: "Des balises"
date: "2019-04-10T22:00:00"
comments: true
published: true
description: "Il y a toujours des balises HTML à découvrir !"
categories:
- flux
tags:
- web
---
> HTML is a powerful markup language which can be used to give our web applications structure and provide powerful accessibility benefits, but only when used appropriately.  
Thus, today we’ll discover ten HTML elements you might not have known existed in the hopes that you can create more accessible, and structurally-sound web applications.  
[Emma Wedekind, 10 HTML Elements You Didn't Know You Needed](https://dev.to/emmawedekind/10-html-element-you-didnt-know-you-needed-3jo4)

Il y a toujours des balises HTML à découvrir !
