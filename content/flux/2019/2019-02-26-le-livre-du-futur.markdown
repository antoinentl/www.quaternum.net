---
layout: post
title: "Le livre du futur ?"
date: "2019-02-26T22:00:00"
comments: true
published: true
description: "Le livre du futur est déjà là, et il est plus _simple_ que nous l'avions imaginé. Je partage l'avis de Craig Mod sur le fait que le livre numérique est singulier pour d'autres raisons que sa forme, et d'ailleurs cela rentre fortement en résonance avec le livre d'Alessandro Ludovico, Post-digital print : la mutation de l'édition depuis 1894. Par contre je pense qu'il ne faudrait pas faire l'économie du comment : quels sont les moyens de production disponibles et quelles en sont les implications ?"
categories:
- flux
tags:
- livre
---
> Our Future Book is composed of email, tweets, YouTube videos, mailing lists, crowdfunding campaigns, PDF to .mobi converters, Amazon warehouses, and a surge of hyper-affordable offset printers in places like Hong Kong.  
[Craig Mod, The 'Future Book' Is Here, but It's Not What We Expected ](https://www.wired.com/story/future-book-is-here-but-not-what-we-expected/)

Le livre du futur est déjà là, et il est plus _simple_ que nous l'avions imaginé.
Je partage l'avis de Craig Mod sur le fait que le livre numérique est singulier pour d'autres raisons que sa forme, et d'ailleurs cela rentre fortement en résonance avec le livre d'Alessandro Ludovico, _[Post-digital print : la mutation de l'édition depuis 1894](https://memoire.quaternum.net/1-mutations-du-livre/1-1-hybridation/)_.
Par contre je pense qu'il ne faudrait pas faire l'économie du _comment_ : quels sont les moyens de production disponibles et quelles en sont les implications ?

Quelques citations supplémentaires :

> For my Kindle Oasis—one of the most svelte, elegant, and expensive digital book containers you can buy in 2018—is about as interactive as a potato.  
> [...]  
> Yet here’s the surprise: We were looking for the Future Book in the wrong place. It’s not the form, necessarily, that needed to evolve—I think we can agree that, in an age of infinite distraction, one of the strongest assets of a “book” as a book is its singular, sustained, distraction-free, blissfully immutable voice. Instead, technology changed everything that enables a book, fomenting a quiet revolution.  
> [...]  
> But the books made today, held in our hands, digital or print, _are_ Future Books, unfuturistic and inert may they seem.  
> [...]  
> It’s easy to take these offerings for granted. Today, anyone with a bit of technological know-how and an internet connection can publish&nbsp;[...].
