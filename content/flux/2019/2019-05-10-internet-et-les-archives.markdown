---
layout: post
title: "Internet et les archives"
date: "2019-05-10T22:00:00"
comments: true
published: true
description: "À noter que les archives aiment également Internet (cela se devait d'être dit)."
categories:
- flux
tags:
- internet
---
> Ce n’est pas toujours beau à voir, mais c’est inestimable pour l’avenir.  
[Xavier de La Porte, Pourquoi Internet aime autant les archives ?](https://www.franceinter.fr/emissions/la-fenetre-de-la-porte/la-fenetre-de-la-porte-23-avril-2019)

À noter que les archives aiment également Internet (cela se devait d'être dit).
