---
layout: post
title: "Nouveau thème pour Quire"
date: "2019-03-04T22:00:00"
comments: true
published: true
description: "Quire, la chaîne de publication de Getty Publications, propose un nouveau thème pour débuter un projet éditorial. Moderne et très lisible, c'est un thème bien pensé pour présenter des œuvres et naviguer facilement dans un catalogue. Quire et ce thème sont accessibles sur demande, ces outils étant encore en version bêta."
categories:
- flux
tags:
- livre
---
> This is a starter theme for Quire, a multiformat digital publishing framework. Quire can be used to generate a web book, EPUB and MOBI e-books, and a PDF optimized for print; all from a single set of text files.  
[Getty Publications, Quire Starter](https://gettypubs.github.io/quire-starter)

[Quire](https://gettypubs.github.io/quire/), la chaîne de publication de [Getty Publications](https://www.getty.edu/publications/digital/index.html), propose un nouveau thème pour débuter un projet éditorial.
Moderne et très lisible, c'est un thème bien pensé pour présenter des œuvres et naviguer facilement dans un catalogue.
Quire et ce thème sont accessibles sur demande, ces outils étant encore en version bêta.
Si vous voulez en savoir plus sur Quire, je vous invite à lire [la section 2.2. de mon mémoire](https://memoire.quaternum.net/2-experimentations/2-2-getty-publications/).
