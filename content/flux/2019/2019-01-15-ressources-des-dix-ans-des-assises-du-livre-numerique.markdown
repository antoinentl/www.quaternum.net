---
layout: post
title: "Ressources des dix ans des Assises du livre numérique"
date: "2019-01-15T22:00:00"
comments: true
published: true
description: "Les 10 ans des Assises du livre numérique, organisées comme d'habitude par le Syndicat national de l'édition, le CFC et la Sofia, ont eu lieu le 3 décembre 2018, et voici quelques ressources utiles, dont des vidéos plus ou moins intéressantes."
categories:
- flux
tags:
- ebook
---
> Le 3 décembre 2018 ont eu lieu les dix ans des Assises du livre numérique.  
[SNE, Les 10 ans des Assises du livre numérique](https://www.sne.fr/evenement_sne/les-10-ans-des-assises-du-livre-numerique/)

Les 10 ans des Assises du livre numérique, organisées comme d'habitude par le Syndicat national de l'édition, le CFC et la Sofia, ont eu lieu le 3 décembre 2018, et voici quelques ressources utiles, dont des vidéos plus ou moins intéressantes.
