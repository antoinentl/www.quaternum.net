---
title: "Publications"
type: pages
---

Liste de mes publications (principalement scientifiques), à retrouver également [sur HAL](https://cv.hal.science/antoine-fauchie) :


## Articles dans une revue

{{< bibliography "data/antoine-fauchie-articles.json" >}}


## Chapitres d'ouvrages

{{< bibliography "data/antoine-fauchie-chapitres.json" >}}


## Communications dans un congrès

{{< bibliography "data/antoine-fauchie-communications.json" >}}


## Autres publications scientifiques

{{< bibliography "data/antoine-fauchie-autres.json" >}}


## Divers (publications non scientifiques)

{{< bibliography "data/antoine-fauchie-divers.json" >}}

