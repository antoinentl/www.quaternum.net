---
layout: page
title: "À propos"
type: pages
---

Je m'appelle **Antoine Fauchié** et je suis l'auteur de ce carnet.

[Une page now](/now) présente mes activités des dernières semaines, [une page publications](/publications) liste les articles et autres textes publiés dans des revues ou à l'occasion de colloques, et [une page projets](/projets) donne un aperçu d'activités plus anciennes.


## Contact

Pour prendre contact avec moi ou réagir aux articles du site le plus simple est de me contacter [par courriel](mailto:antoine@quaternum.net).

Vous pouvez aussi me suivre sur Mastodon ([@antoinentl](https://post.lurk.org/@antoinentl/)), ou découvrir d'autres projets sur GitHub ([@antoinentl](https://github.com/antoinentl)), GitLab ([@antoinentl](https://gitlab.com/antoinentl)), Framagit ([@antoinentl](https://framagit.org/antoinentl)), Huma-Num ([https://gitlab.huma-num.fr/antoinefauchie](https://gitlab.huma-num.fr/antoinefauchie)) ou SourceHut ([https://git.sr.ht/~antoinentl/](https://git.sr.ht/~antoinentl/)).


## Moteur du carnet

D'abord propulsé par Wordpress, ce carnet est désormais un ensemble de pages HTML générées par [Hugo](https://gohugo.io/) (après [Jekyll](http://jekyllrb.com/) et un court passage par [Octopress](http://octopress.org/ "Lien vers Octopress")), je m'en explique [ici](/2012/12/23/pourquoi-quitter-wordpress/), [là](/2014/12/29/un-point-sur-jekyll/) et [ici](/2021/07/21/refonte-chantier-ouvert/).


## Un commentaire ?
Mon carnet ne propose pas la possibilité de laisser un commentaire à la fin des articles.

Au-delà de la contrainte technique de mettre en place un système de publication de commentaires -- qui nécessite soit une base de données soit l'utilisation d'un service tiers --, l'absence d'une telle fonctionnalité est une invitation à créer d'autres publications.

Vous souhaitez réagir à un article de ce carnet ? Publiez un billet anonyme avec [Telegra.ph](http://telegra.ph), écrivez un article sur [Write.as](https://write.as/), créez un blog avec [Wordpress](https://fr.wordpress.com/), ouvrez un carnet sur [GitLab Pages](https://pages.gitlab.io/) ou [GitHub Pages](https://pages.github.com/), échangez sur [une instance](https://post.lurk.org) de Mastodon, ou construisez votre propre site web avec votre propre nom de domaine&nbsp;!


## Licence des contenus

Les contenus sont publiés sous licence [Creative Commons BY-SA](http://creativecommons.org/licenses/by-sa/4.0/), vous pouvez donc reproduire, distribuer et communiquer l’œuvre, ainsi que la "remixer" ou l'adapter, à condition que vous utilisiez la même licence, et que vous mentionniez l'auteur (Antoine Fauchié, www.quaternum.net).
Attention certains contenus peuvent porter une autre licence (_preprint_ ou autres).

Les propos de l'auteur (Antoine) n'engagent que lui-même.

