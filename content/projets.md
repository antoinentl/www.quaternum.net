---
layout: page
title: "Projets"
type: pages
---

Sélection de projets que j'ai menés seul ou en équipe, non mise à jour depuis 2019.


## Livre numérique enrichi Recto-Verso, Anact, 2018
![Couverture du livre numérique Recto-Verso](/images/projet-recto-verso.png)

Accompagnement de l'Agence nationale pour les conditions de travail pour le développement du livre numérique enrichi _Recto-Verso&nbsp;: les mutations du travail controverses et perspectives_&nbsp;: ateliers d'idéation, organisation, stratégie éditoriale, conseils techniques.

_Consultant indépendant en édition numérique. [Aurore Chassé](http://www.aurorechasse.com/) a créé l'identité graphique et [Adrien Pifaretti (Avant-Goût Studios)](https://www.avant-gout.com/) a développé le livre numérique au format EPUB._

[Découvrir le projet](http://mutationsdutravail.com/)

## Plate-forme diBook, 1D Lab, 2017
Suivi de projet pour le développement de la plate-forme diBook (ex 1D touch livre numérique) porté par la Scic [1D Lab](http://1d-lab.eu/)&nbsp;: participation au montage du projet, suivi du développement technique de la plate-forme et du lecteur en ligne, amorçage des prises de contact avec les éditeurs.

_Consultant indépendant en édition numérique au sein de la formidable équipe d'1D&nbsp;Lab._

## Site web de l'Arald, 2015
![Site web de l'Arald](/images/arald-site-web.png)
Refonte complète du site web de l'Agence Rhône-Alpes pour le livre et la documentation, alors que j'étais en charge du numérique à l'Arald. [Le site web de l'Arald](http://www.arald.org/) est un outil d'information et de communication auprès des professionnels du livre de la région Auvergne-Rhône-Alpes : actualités, agenda des rencontres professionnelles, ressources, documents, bases de données.

*Chef de projet à l'Arald.  
[Perluette](http://www.perluette-atelier.com/) a créé l'identité graphique, [Rezo Zero](https://www.rezo-zero.com/) a développé la charte graphique pour le web, la direction artistique et le développement -- basé sur le CMS Roadiz.*

<a href="https://www.rezo-zero.com/fr/projects/arald" class="link-button">Plus d'informations</a>


## Audit et maintenance du site web de l'AddnB, 2016-2018
Audit et maintenance du site web de l'Association pour le développement des démarches numériques en bibliothèques, dans l'objectif d'une refonte à plus long terme : analyse de l'existant, audit de l'infrastructure technique, recommandations pour la mise en sécurité et pour la gestion des contenus, maintenance et documentation.

*Consultant indépendant.*

## Le livre 010101, livre web, 2015
![Le livre 010101](/images/le-livre-010101.png)

"Après avoir découvert le livre web [Professionnal Web Typography](/2015/05/12/un-design-de-livre-web/) de Donny Truong, voici une mise en pratique avec *Le livre 010101 (1971-2015)* de Marie Lebert&nbsp;: création d'[un site web dédié](http://www.010101book.net/) et d'un fichier EPUB, afin de palier au seul format PDF disponible. Les principes&nbsp;: permettre une lecture *en ligne* ou *hors ligne*. Les contraintes&nbsp;: lisibilité, simplicité et légèreté."

*Chef de projet indépendant (conception, graphisme, développement, intégration).*

<a href="https://www.quaternum.net/2015/10/26/creation-d-un-livre-web-le-livre-010101/" class="link-button">Plus d'informations</a>

## Annothèque, une expérience d'annotation numérique, 2014
![Annothèque](/images/annotheque.png)

De nombreux dispositifs d’annotation existent et se développent dans des écosystèmes numériques : applications sur smartphones et tablettes, services en ligne, etc. Mais aujourd’hui, l’expérience d’annotation numérique connectée au livre papier n’existe pas. Proposer une expérience d’annotation aussi simple que des notes aux crayons sur un livre papier et aussi intuitive qu’écrire avec des outils numériques, voilà le projet de l’annothèque ! Conception lors du hackathon UmiX, et présenté lors de Blend Web Mix 2014.

*Conception, scénarisation, présentation.*  
*Projet conçu avec Areg Baghinyan, [Emeline Mercier](http://emelinemercier.com/) et [Konstantin Weiss](http://konstantinweiss.com/)*.

<a href="https://web.archive.org/web/20170515193430/http://annotheque.fr/" class="link-button">Plus d'informations</a>


## Mise en place d'une veille, 2012-2016
![Veille](/images/decompte-veille.png)

Mise en place d'une veille informationnelle et stratégique sur le numérique dans le domaine du livre à l'Arald : informations techniques, économiques et juridiques pour les acteurs du livre (auteurs, éditeurs, libraires, bibliothécaires, médiateurs, etc.). Pendant quatre ans et deux fois par mois, plus de 800 professionnels en France ont été informés de l'évolution de l'écosystème du numérique en lien avec le livre : enjeux techniques, évolution des usages, modification de la législation, bouleversements économiques, etc.

*Chef de projet (stratégie globale, veille, rédaction, mise en place du workflow, suivi) à l'Arald.*

## Mémoire et actualité en Rhône-Alpes, portail du patrimoine écrit, 2012
![Mémoire et actualité en Rhône-Alpes](/images/memoireetactualite.png)

Portail des ressources régionales, [Mémoire et actualité en Rhône-Alpes](http://www.memoireetactualite.org) est un site coopératif destiné à signaler et à valoriser les ressources locales de plus de soixante établissements de la région Rhône-Alpes (bibliothèques et services d’archives) : 36 titres de presse ancienne (plus de 400 000 pages de journaux) interrogeables en texte intégral ; près de 3 000 images numérisées ; 518 notices de fonds ; plus de 270 000 notices bibliographiques.

*Chef de projet (conception, ergonomie, suivi) à l'Arald, en lien avec l'équipe de l'Arald pour la conception, CD-Script pour le développement technique et David Juhel pour le graphisme.*

<a href="http://www.memoireetactualite.org/" class="link-button">Plus d'informations</a>


## Expositions virtuelles pour le portail Lectura, 2007-2012
[Lectura](http://www.lectura.fr/) est "le portail des bibliothèques des villes-centres de Rhône-Alpes", et il propose notamment des *expositions virtuelles* : des sites web destinés à présenter les fonds des bibliothèques avec différentes approches. J'ai assuré le suivi de la création d'une dizaine d'expositions virtuelles, aux côtés de graphistes et de développeurs web. La question du respect des standards du web et de l'accessibilité de ces sites web était centrale dans ces projets.

*Chef de projet (accompagnement, conception, suivi) et expert accessibilité à l'Arald, en lien avec des graphistes et des développeurs web.*

<a href="http://www.lectura.fr/fr/expos/index.cfm" class="link-button">Plus d'informations</a>


## D'autres projets
Chef de projets pour d'autres portails et sites web, enseignement, veille personnelle, gestion de systèmes d'information, opérations de numérisation, réalisation d'audits, d'études et de rapports, participation à des projets nationaux autour de l'accessibilité ou de bibliothèques numériques, [contactez-moi](/a-propos) pour en savoir plus.
