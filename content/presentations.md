---
layout: page
title: "Présentations"
type: pages
---
Voici une sélection d'interventions ou de formations que j'ai réalisées ces derniers mois.

<a href="/profil-antoine/" class="link-button">En savoir plus sur mon profil</a>

## Le fanzinat des gens du web. Sud Web, mai 2016
[![Capture d'écran du support de présentation](/images/2016-le-fanzinat-des-gens-du-web-presentation.png)](http://presentations.quaternum.net/le-fanzinat-des-gens-du-web/)

*Lightning talk* de cinq minutes à l’occasion de Sud Web 2016, sur le thème des blogs et carnets web des acteurs du web, un manifeste pour des espaces de publication indépendants, artisanaux, créatifs et ouverts !

<a href="http://presentations.quaternum.net/le-fanzinat-des-gens-du-web/" target="_blank">Découvrir le support de présentation</a>

## Le livre numérique : théorie, (en)jeux et découvertes. esadse, avril 2016
[![Capture d'écran du support de présentation](/images/2016-04-livre-numerique-esadse.png)](http://presentations.quaternum.net/le-livre-numerique-theorie-enjeux-decouverte/)

Sur l'invitation de [Jérémie Nuel](http://www.i-n-d-e-x.net/), enseignant à l'École supérieure d'art et design de Saint-Étienne, je suis intervenu pendant une journée auprès d'étudiants de troisième, quatrième et cinquième année d'art et de design. J'y ai parlé écosystème du livre, écosystème numérique, standards du web et du livre numérique, distinction structure et mise en forme, outils, etc.

<a href="http://presentations.quaternum.net/le-livre-numerique-theorie-enjeux-decouverte/" class="link-button" target="_blank">Découvrir le support de présentation</a>

## Le livre web comme objet d’édition ? Définition et origines. Colloque internationale ECRiDiL, avril 2016
[![Capture d'écran du support de présentation](/images/2016-04-ecridil.png)](http://presentations.quaternum.net/le-livre-web-comme-objet-d-edition/)

Suite à un appel à communication du colloque internationale [ECRiDiL, "Écrire, éditer, lire à l’ère numérique"](https://ecridil.hypotheses.org/), [ma proposition](https://www.quaternum.net/2016/03/07/le-livre-web-comme-objet-d-edition) a été retenue et j'ai pu présenter quelques pistes de réflexion sur l'objet *livre web*, avec une approche sur le design.

<a href="http://presentations.quaternum.net/le-livre-web-comme-objet-d-edition/" class="link-button" target="_blank">Découvrir le support de présentation</a>


## D'autres formations ou interventions
Enseignant, formateur ou intervenant, je suis disponible pour intervenir sur des questions liées au livre numérique, à la publication en ligne, aux enjeux liés aux usages du numérique, etc.

<a href="/profil-antoine" class="link-button">Découvrir mon profil complet</a>