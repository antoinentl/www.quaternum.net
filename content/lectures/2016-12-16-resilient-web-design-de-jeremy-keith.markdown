---
layout: lectures
title: "Resilient Web Design de Jeremy Keith"
date: "2016-12-16T10:00:00"
comments: true
published: true
description: "Quelques citations et de brèves notes sur le livre (web) de Jeremy Keith, Resilient Web Design."
categories:
- lectures
key_book: keith_resilient_2016
lecture_status: notes
---
Quelques citations et de brèves notes sur le livre (web) de Jeremy Keith, _Resilient Web Design_.

<!-- more -->

## Introduction
> Ideas are more resilient than code.

> I’ve tried to combine the most resilient ideas from the history of web design into an approach for building the websites of the future.

## Chapter 1: Foundations
> The icons in our software interfaces are whispering stories to us from the history of clothing and fashion.

> Instead of creating content in other file formats and using HTML to link them up, people began writing content directly in HTML.

## Chapter 2: Materials
> This liberal attitude to errors allowed the vocabulary of HTML to grow over time from the original 21 elements to the 121 elements in HTML5. Whenever a new element is introduced to HTML, we know exactly how older browsers will treat it; they will ignore the tags and display the content.

> HTML was in danger of becoming a visual instruction language instead of a vocabulary of meaning.

> Designers were treating HTML as a tool for the appearance of content instead of a language for describing the meaning of content. CSS was a solution to this problem, if only designers could be convinced to use it.

## Chapter 3
> The shared illusion of a one‐size‐fits‐all approach to web design began to evaporate. It was gradually replaced by an acceptance of the ever‐changing fluid nature of the web.

> For years, web design was dictated by the designer. The user had no choice but to accommodate the site’s demand for a screen of a certain size or a network connection of a certain speed. Now, web design can be a conversation between the designer and the user. Now, web design can reflect the underlying principles of the web itself.

## Chapter 4: langages
> Even if there are errors in the HTML or CSS, the browser will still attempt to process the information, skipping over any pieces that it can’t parse.

> The term Ajax was coined at the same time that another neologism was in the ascendent. Tim O’Reilly used the phrase Web 2.0 to describe a new wave of web products and services. Unlike Ajax, it was difficult to pin down a definition of Web 2.0. For business people, it meant new business models. For graphic designers, it meant rounded corners and gradients. For developers, it meant JavaScript and Ajax.

> This doesn’t mean that web designers shouldn’t use JavaScript. But it does mean that web designers shouldn’t rely on JavaScript when a simpler solution exists.

> This doesn’t mean that web designers shouldn’t use JavaScript. But it does mean that web designers shouldn’t rely on JavaScript when a simpler solution exists.

> On the face of it, the term “web platform” seems harmless. Describing the web as a platform puts it on par with other software environments. Flash was a platform. Android is a platform. iOS is a platform. But the web is not a platform. The whole point of the web is that it is cross‐platform.

> Platforms are controlled and predictable. The World Wide Web is chaotic and unpredictable.

## Chapter 5: Layers
> Likewise, the expressiveness of CSS and JavaScript is only made possible on a foundation of HTML, which itself requires a URL to be reachable, which in turn depends on the HyperText Transfer Protocol, which sits atop the bedrock of TCP/IP.
Each of the web’s shearing layers can be peeled back to reveal a layer below. Running that process in reverse—applying each layer in turn—is a key principle of resilient web design.

> This layered approach to the web allows the same content to be served up to a wide variety of people. But this doesn’t mean that everyone gets the same experience. Champeon realised that a strong separation of concerns would allow enhancements to be applied according to the capabilities of the end user’s device.
To paraphrase Karl Marx, progressive enhancement allows designers to ask from each browser according to its ability, and to deliver to each device according to its needs.


## Chapter 6:
> Here’s a three‐step approach I take to web design:
>
1. Identify core functionality.
2. Make that functionality available using the simplest possible technology.
3. Enhance!

> There was a time when using software meant installing separate programs on your computer. Today it’s possible to have a machine with nothing more than a web browser installed on it.

> Websites do not need to look exactly the same in every browser.


## Chapter 7:
> If every single feature needed to be available to every browser on every device, that would indeed be an impossibly arduous process. This is why prioritisation is so important.

> Whether it’s WYSIWYG editors, graphic design programs, content management systems, or JavaScript frameworks, tools inevitably influence workflows.

## Notes
Comparaison du design web résilient avec deux évolutions majeures du web : CSS pour la mise en forme, et le responsive web design.
