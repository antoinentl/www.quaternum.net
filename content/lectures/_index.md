---
layout: default
title: Lectures
description: "Notes de lectures diverses et variées souvent en lien avec le livre et le numérique, parfois avec des commentaires de texte ou des ébauches d'articles."
---
Notes de lectures diverses et variées souvent en lien avec le livre et le numérique, parfois avec des commentaires de texte ou des ébauches d'articles.