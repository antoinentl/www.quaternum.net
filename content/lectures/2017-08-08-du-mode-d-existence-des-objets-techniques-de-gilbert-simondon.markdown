---
layout: lectures
title: "Du mode d'existence des objets techniques de Gilbert Simondon"
date: "2017-06-03T10:00:00"
comments: true
published: true
description: "Ouvrage de référence dans le domaine de la philosophie de la technique, Du mode d'existence des objets techniques de Gilbert Simondon est un texte que j'ai étudié dans le cadre d'un mémoire. Ce qui apparaît ci-dessous est un commentaire de texte d'une partie de ce livre majeur et passionnant."
categories:
- lectures
key_book: simondon_du_2012
lecture_status: commentaire et notes
slug: du-mode-d-existence-des-objets-techniques-de-gilbert-simondon
---
Ouvrage de référence dans le domaine de la philosophie de la technique, _Du mode d'existence des objets techniques_ de Gilbert Simondon est un texte que j'ai étudié dans le cadre d'un mémoire (disponible [ici](https://memoire.quaternum.net)).
Le commentaire de texte d'une partie de ce livre majeur et passionnant est déposé ici pour mémoire.

<!-- more -->

## Progrès technique (commentaire de texte)

Gilbert Simondon est un philosophe français né en 1924 et mort en 1989.
Philosophe de la technique, mais également professeur de psychologie et de physique, il a une démarche résolument encyclopédique.
*Du mode d'existence des objets techniques* est sa thèse complémentaire qu'il soutient en 1958 et qui est publiée la même année, dans laquelle il aborde la condition des *objets techniques* au même titre que les objets sacrés ou les objets esthétiques, pour construire une *culture technique* comme mode de rapport au monde de l'homme.
Ce texte est probablement le plus connu du philosophe, même si une vingtaine d'ouvrages ont été publiés, souvent posthume, dont : *L'Individu et sa genèse physico-biologique*, *L'individuation psychique et collective*, *Sur la technique*, *Sur la psychologie*, ou *Sur la philosophie*.

Encore étudiée et citée aujourd'hui, l'œuvre de Gilbert Simondon a marqué notre époque et a influencé des penseurs comme Gilles Deleuze, de nombreux philosophes contemporains comme Bernard Stiegler ou Pierre-Damien Huyghe s'y réfèrent directement.
Gilbert Simondon est en quelque sorte un héritier de Marx, et prolonge les pensées de Gaston Bachelard et Henri Bergson.
*Du mode d'existence des objets techniques*, ou MEOT, représente en soi une révolution philosophique du vingtième siècle, Gilbert Simondon s'opposant à la plupart des penseurs techniques – dont Martin Heidegger qui ne pense pas la technique à partir de la catégorie de l'objet.

MEOT est composé de trois parties : la première est consacrée à l'objet technique sous l'angle de sa genèse et de son évolution ; la seconde partie se concentre sur le rapport de l'homme et de la technique, notamment en traitant des questions d'éducation, de formation et de progrès ; la troisième partie fonde la culture technique, base du rapport de l'homme au monde et à la nature.
Nous nous concentrons ici sur la première partie *Du mode d'existence des objets techniques*, elle ouvre des perspectives passionnantes pour évaluer certaines technologies numériques du vingt-et-unième siècle, et y porter un regard critique.

### Une critique de la technique
La question de la technique est encore abordée d'une façon dichotomique lorsque Gilbert Simondon écrit cette thèse : soit considérée comme purement utilitariste, soit sacralisée ou rejetée, la technique est détachée de la culture.
La première partie *Du mode d'existence des objets techniques* traite de la question de l'objet technique afin de constituer, dans le reste de cet ouvrage, une culture technique comme nouveau socle du rapport de l'homme au monde.
L'aliénation du monde contemporain n'est pas due à la machine, mais à une mauvaise reconnaissance de la technique.
Notre civilisation est ["mal technicienne"](https://www.youtube.com/watch?v=VLkjI8U5PoQ) : elle comprend mal la technique et ne se positionne pas de façon juste par rapport à elle.
Nous ne pouvons que réaffirmer ce que Gilbert Simondon exposait il y a plus de cinquante ans : la technique est mal perçue, et pour mieux la comprendre l'auteur *Du mode d'existence des objets techniques* nous invite à définir l'objet technique en tant qu'être et en tant qu'individu.
L'objet technique est la clé de cette nouvelle compréhension de la technique : il est doté d'une existence et d'une individualisation.
Par ailleurs son analyse nous permet de définir ce qu'est le progrès technique.

Comment repositionner l'homme et la machine ?
À travers un travail de définition et de genèse, Gilbert Simondon présente tout d'abord les conditions d'existence de l'objet technique dans le premier chapitre de la première partie : phénomène de concrétisation et mode d'évolution de l'objet technique.
La question de l'individualisation occupe le deuxième chapitre : concept de milieu associé et forme de l'évolution technique.
En déterminant le mode d'existence des objets techniques, Gilbert Simondon place les bases d'une culture technique qu'il développe dans les parties suivantes, que nous ne commentons pas ici.

Afin d'illustrer la pensée de cette première partie et de la replacer dans un contexte contemporain – un monde numérique –, nous nous attarderons sur un exemple d'objet technique qui nous intéresse particulièrement ici : les chaînes de publication, c'est-à-dire l'ensemble du processus de conception et de production de livres.
Enfin, nous répondrons à deux questions essentielles.
Comment dépasser une conception uniquement utilitariste de la technique, quelle peut être cette perfection interne des machines ?
Le progrès technique ne peut-il pas être autre chose qu'une recherche effrénée vers l'efficacité et émanciper l'homme de son mauvais positionnement par rapport à la machine ?

### La génèse de l'objet technique
Dans le premier chapitre de la première partie *Du mode d'existence des objets techniques*, Gilbert Simondon cherche à décrire l'objet technique suivant son évolution : en cherchant les liens entre un objet technique du passé et un objet technique du présent, nous découvrons une ontogenèse.
Celle-ci s'appuie tout d'abord sur la définition du concept de "concrétisation" (p. 26), concept qui est l'une des conditions d'existence de l'objet technique.
Nous pouvons considérer que l'objet technique a une "forme abstraite" (p. 24) lorsqu'il est dissocié de son environnement de fonctionnement, à l'inverse la concrétisation de l'objet technique se définit par l'imbrication des fonctions qui le composent, celles-ci peuvent former une synergie : les fonctions convergent.

Prenons l'exemple de l'ustensile : en soit il ne fait que répondre à un besoin précis, il est purement fonctionnel, sa ou ses fonctions sont chacune indépendante, son fonctionnement est linéaire – une fonction après l'autre –, il s'agit d'un objet technique abstrait.
Prenons un second exemple : un moteur n'a pas un fonctionnement linéaire, ses éléments sont imbriqués, certaines fonctions se répondent entre elles, il y a une synergie interne au moteur, il s'agit d'un objet concret.
En termes de genèse l'"objet technique abstrait" (p. 29) est primitif, ancien, il y a donc une évolution technique, mais comment s'est-elle déroulée ?
Le progrès technique est brusque, il n'est pas continu, il est composé d'"évolutions mineures" (p. 46) et d'"évolutions majeures" (p. 46).
Les évolutions mineures sont des améliorations : ajout d'une fonction augmentant l'efficience de l'ensemble, comme le fait de compléter une phrase par un appendice entre parenthèses.
Les évolutions majeures s'appuient sur les évolutions mineures, elles se traduisent par de nouveaux objets techniques, comme le fait de reformuler une phrase pour la compléter, plutôt que de lui adjoindre une parenthèse : remplacer l'extension par une refonte.

Ce que nous pouvons en déduire, c'est que la perfection d'un objet technique ne correspond pas à son degré de résolution d'un problème, mais à la cohérence et la performance de son propre fonctionnement.
Nous ne devons pas tomber dans une recherche de l'utilité pure qui nous obligerait à juger d'un degré de progrès technique en fonction de la simple réponse de l'objet technique à un problème que nous aurions posé.
Il y a une forme de beauté interne de l'objet technique, que nous pourrions comparer à celle d'un organisme, même si un objet technique n'est pas vivant.
Si nous parlons ici d'évolution technique, quelle forme prend-elle ?
Le progrès technique est basé sur un double mouvement : les évolutions mineures, correctives, donnent lieu à des évolutions majeures, de nouveaux objets techniques ; il ne s'agit donc pas d'une genèse, mais d'une filiation ; les objets techniques sont liés entre eux et le progrès technique n'est pas une évolution continue, mais un processus discontinu dans lequel l'humain a un rôle clé.

La question soulevée à la fin de ce premier chapitre est la suivante : comment est permise cette évolution ?
Quel est ce cadre inventé par l'homme et qui donne lieu à une concrétisation ?

### L'individualisation de l'objet technique
Le second chapitre de cette première partie débute par la définition de l'"hypertélie" (p. 61) de l'objet technique, qui est un phénomène de spécialisation qui le rend moins adaptable, et moins autonome.
L'hypertélie peut être de plusieurs ordres : spécialisation, division en vue d'une spécialisation, efficience en fonction de l'environnement extérieur.
La plurifonctionnalité des objets techniques n'est possible que sans spécialisation des deux premiers types, elle est toutefois rendue possible dans un certain milieu, le troisième milieu : ni l'objet technique seul, ni son contexte.
Il s'agit donc d'un équilibre entre deux milieux, permis par l'homme : le "milieu associé" (p. 70).

La particularité du milieu associé est qu'il est créé par l'objet technique et que ce milieu permet à l'objet technique d'exister : "Ce milieu à la fois technique et naturel peut être nommé milieu associé.
Il est ce par quoi l'être technique se conditionne lui-même dans son fonctionnement."
Prévoir ce qui se passera dans une situation d'application – l'objet technique immergé dans un contexte particulier – est la condition permise par l'homme, un acte d'invention, nous pouvons considérer la "causalité récurrente" (p. 75) comme son expression visible.
Cette forme d'autonomie, et donc cette individualisation, s'exerce au niveau de l'individu technique, et non au niveau de l'ensemble technique, ce dernier en héritant néanmoins, comme pour la causalité récurrente.
Si Gilbert Simondon insiste sur ce point, nous pouvons noter que le caractère d'individualisation concerne l'individu technique et le niveau inférieur, l'élément technique, mais sans que ce dernier ne possède de milieu associé.
Les éléments techniques ont un caractère d'individualisation uniquement parce qu'ils composent l'individu technique.
Le passage de causalité se fait donc entre élément technique et individu technique, nous devons approfondir l'aspect temporel qu'implique ce passage et quelle différence il y a avec le vivant.

Dans le cas du vivant l'organe ne peut être détaché de l'ensemble, alors que l'élément technique peut être retiré de l'individu technique ; le vivant engendre à l'identique, alors que la technique produit.
Le "temps de relaxation" (p. 83) est l'une des distinctions entre le vivant et la technique : comme nous avons déjà pu le voir le progrès technique est discontinu, en dents de scie.
Gilbert Simondon introduit le concept de "production indirecte" (p. 88), que nous pouvons comprendre comme l'amélioration progressive qui donne lieu à des nouveaux objets techniques, c'est par le jeu de leurs imperfections que les objets techniques évoluent, contrairement aux êtres vivants qui, par leur perfection, engendrent des êtres identiques à eux.
Nous pourrions ajouter une précision aux propos de Gilbert Simondon : les êtres vivants évoluent dans un temps long, ils ne sont pas parfaitement identiques lorsqu'ils s'engendrent, alors que les objets techniques connaissent des bons d'évolution.

La machine et l'homme sont très différents : la machine porte les outils et les dirigent, et l'homme règle et dirige la machine.
Le point commun entre la machine et l'homme réside dans la notion d'individu, qui peut être considérée comme l'auto-régulation, il y a l'idée d'un fonctionnement autonome.
Par exemple la machine-outil ne fait qu'utiliser des outils, alors que l'objet technique est plus indépendant ; Gilbert Simondon fait également la distinction entre technicien et artisan pour appuyer cette précision.
Partant du constat d'une part que les civilisations non-industrielles n'ont pas d'individu technique parce que c'est l'homme qui porte les outils, et d'autre part que les civilisations industrielles ont des individus techniques parce que l'homme devient celui qui permet l'auto-régulation des machines, nous devons alors nous interroger sur le niveau d'intervention de l'homme sur l'objet technique.
L'homme était un individu technique, il ne peut donc intervenir _dans_ l'individu technique, mais _au-dessous_ – l'élément technique – ou _au-dessus_ – l'ensemble technique.

Gilbert Simondon introduit la notion de "culture technique" et prévoit de l'expliciter dans les prochaines parties, en tant qu'elle permet de résoudre la confusion autour de l'individualisation de l'objet technique : il n'est pas humain, mais parce qu'il est individu l'homme croit que la machine a des propriétés humaines.
"Il est nécessaire que l'objet technique soit connu en lui-même pour que la relation de l'homme à la machine devienne stable et valide : d'où la nécessité d'une culture technique."

La pensée de Gilbert Simondon peut être considérée comme novatrice, l'analyse de la première partie nous en révèle déjà plusieurs raisons.
Le statut d'objet technique est une nouvelle approche, contrairement à des courants de pensée moderne comme la philosophie de Martin Heidegger.
Autre opposition avec Martin Heidegger, la conception de la technique présentée dans MEOT n'est pas une dépossession supplémentaire de l'homme, pas plus qu'elle est une simple supériorité de l'homme sur la nature comme le soutient Nietzsche.
Parmi ses contemporains, nous pouvons noter la position de Jacques Ellul qui place dans la technique une dimension politique et morale, en tant qu'elle est la source de nouvelles aliénations, ce que réfute justement Gilbert Simondon.
Ce dernier a également influencé nombre d'auteurs, nous pouvons citer Bernard Stiegler qui a bâti un concept d'"individuation" et de "transindividuation" à partir d'une critique de Gilbert Simondon, ou Pierre-Damien Huyghe qui y fait référence dans plusieurs de ses travaux.

### Un objet technique contemporain : les chaînes de publication
Pour illustrer les idées, les concepts ainsi que la thèse que Gilbert Simondon présente dans la première partie de son livre, nous choisissons un objet technique contemporain : les chaînes de publication.

Une chaîne de publication est l'ensemble des outils et méthodes qui forment le processus de conception et de production des livres, ou plus globalement des *publications*.
Un exemple type est le couple traitement de texte et logiciel de publication assistée par ordinateur.
A contrario les chaînes de publication qui utilisent les technologies et les méthodes du développement web peuvent représenter un progrès technique par rapport aux chaînes de publication classiques.
Ces dernières utilisent des logiciels destinés à accomplir une tâche : d'un côté écrire du texte et éventuellement le structurer avec un traitement de texte, et de l'autre côté mettre en forme et produire un fichier permettant une impression papier avec un logiciel de publication assistée par ordinateur.
Majoritairement propriétaires et fermés, le code de ces logiciels n'est pas accessible, modifiable ou partageable, plaçant ceux qui conçoivent et fabriquent les livres dans une situation d'aliénation imposée.
Par exemple si un logiciel comme InDesign ne prend plus en charge GREP – système d'expressions régulières permettant de faciliter les remplacements en série –, alors l'éditeur qui l'utilise est obligé de trouver une autre solution, ou pire il perd un temps précieux.
Autre exemple : si l'interface d'un traitement de texte est complètement revue, son utilisateur ne peut pas la modifier, et doit se former à nouveau pour être en mesure de la comprendre et de la prendre en main.
Avec des logiciels ouverts, une possibilité de modification par la communauté existe : cela ne veut pas dire que chaque problème a sa solution, mais il y a une *possibilité*.

L'arrivée du livre numérique a obligé les éditeurs à modifier une partie de cette chaîne et son mode de fonctionnement : en détournant l'usage d'un logiciel de PAO, ou en faisant appel à un autre outil – un logiciel dédié à la production de livre numérique –, et en devant ouvrir au moins en partie le code source de ces livres.
L'environnement a donc changé, et la chaîne de publication a connu une correction mineure : elle a été perfectionnée par l'ajout d'un élément, et le mode de production n'a été modifié qu'en partie.
Une chaîne de publication qui abandonne le duo traitement de texte et logiciel de PAO, et qui se base sur des technologies et des méthodes de travail du web, est une évolution majeure, et ce pour plusieurs raisons : il ne s'agit plus de simples perfectionnements fonctionnels ou correctifs – par exemple pour produire un livre numérique au format EPUB –, mais une modification de l'ensemble des éléments pour parvenir à produire un livre en plusieurs formats, dont le format EPUB.

L'un de ses éléments est l'utilisation d'un langage de balisage léger comme Markdown pour écrire et structurer les contenus, cette utilisation ouvre plusieurs perspectives, et assure plusieurs fonctions elles-mêmes imbriquées : pouvoir se passer d'un logiciel spécifique comme un traitement de texte ; s'assurer que le fichier sera lisible dans plusieurs années ou dizaines d'années ; transformer facilement les fichiers dans un format structuré comme le HTML ; et disposer d'un format permettant de structurer un texte tout en étant lisible par un humain.
Une chaîne de publication inspirée du web n'est pas seulement un meilleur moyen de concevoir et produire des livres, ou simplement un système perfectionné, nous constatons que cette chaîne est surtout l'occasion pour celle ou celui qui l'utilise de mieux comprendre la technique et de s'y positionner plus justement.
Mieux comprendre implique une action de documentation qui accompagne l'usage, consciemment ou inconsciemment, et donc de constitution d'une culture technique telle que Gilbert Simondon l'a théorisée.
S'y positionner plus justement : non pas _dedans_ comme avec un logiciel qui fonctionne telle une boîte noire, mais _au-dessous_, dans la création et l'ajustement d'éléments et de briques techniques, et _au-dessus_, dans l'orchestration de l'ensemble technique que forme cette chaîne de publication.

### Un regard critique sur le monde numérique
Gilbert Simondon nous a donné le matériel nécessaire pour que nous conservions un regard critique sur le monde numérique du vingt-et-unième siècle – bien que *Du mode d'existence des objets techniques* est illustré d'exemples mécaniques et électrotechniques du milieu du vingtième siècle –, et pour que nous puissions nous y placer d'une façon juste, sans nous aliéner.
Encore aujourd'hui, la technique est embrassée ou rejetée, nous observons majoritairement une dualité entre postures technophile et technophobe, par manque de connaissance de la technique, mais également par manque de maîtrise de celle-ci.
En observant le fonctionnement même des machines et des systèmes techniques, et leur rapport avec leur environnement, nous pouvons intégrer la technique comme la possibilité d'établir une relation juste avec la nature.

Les chaînes de publication inspirées du web peuvent être considérées comme un objet technique ouvert et comme une occasion de mieux comprendre comment produire des livres : la culture technique est une question de réappropriation par des choix technologiques et des choix de processus.
Nous ne devons pas être naïfs quant au progrès technique que peut représenter cet exemple, celui-ci pouvant être repris au compte du fonctionnalisme ou de la recherche aveugle de l'efficacité.
Le progrès technique en tant qu'il peut être une émancipation de l'homme doit intégrer une dimension d'analyse et d'accompagnement continus : avoir le recul nécessaire sur les usages et les outils produits, et faire œuvre de pédagogie pour que ce recul puisse perdurer.


## Notes
_Les notes très détaillées ci-dessous sont placées ici plus pour des questions d'archivage que de partage._

### Introduction
[Page 9] De la même façon que la philosophie a *permis* l'abolition de l'esclavage ou "l'affirmation de la valeur de la personne humaine", elle doit permettre une certaine reconnaissance de la technique ou de l'objet technique. Gilbert Simondon part donc du principe que la technique n'est pas perçue de façon juste, et qu'elle est bien trop dissociée de l'homme et séparée de la culture – la culture étant considérée en opposition à la nature.

[Page 10] La technique étant créée par l'homme, la machine comporte de l'humain, elle ne doit pas être considérée comme une étrangère que l'on ne voudrait pas connaître, elle doit être approchée et comprise, sans être rejetée. L'aliénation du monde contemporain n'est pas dûe à la machine, mais à une mauvaise connaissance de la machine.

[Page 10] Les objets techniques doivent êre considérés en évitant deux écueils : l'objet technique ne possédant qu'un usage ou une "fonction utile" ; et la sacralisation tendant vers un technicisme et une idôlatrie. Gilbert Simondon va plus loin, il tente d'avertir sur le désir de placer en la machine d'autres propriétés humaines (penser, vouloir, vivre) : cet anthropomorphisme est déplacé et faux, et il est une tentative vaine, pour l'homme, de ne pas affronter la vie telle qu'elle est en plaçant la machine comme *bouclier*.

[Page 11-13] Reprenons les deux écueils contradictoires de conceptualisation des objets techniques dans la culture : l'utilitarisme sans signification et l'anthropomorphisme menaçant/dangereux. Le premier servant à contenir ou cacher le second. Le problème vient de l'"automatisme", et du fantasme suivant : les machines pourraient se suffire à elles-mêmes, se gérer seules en communiquant entre elles. Gilbert Simondon évacue rapidement l'automatisme comme perfectionnement technique, et préfère aborder la question de la sensibilité pour les machines. Cette sensibilité serait le degré d'"indétermination" de la machine, lui permettant de s'adapter à des situations imprévues. Gilbert Simondon parle aussi d'"ouverture", une "machine ouverte" serait donc celle qui peut être orchestrée par l'homme, et non autonome et isolée.  
"[L'homme] est *parmi* les machines qui opèrent avec lui." Les machines n'opèrent pas *pour* l'homme mais *avec* l'homme.

[Page 13] Ce que je crois comprendre : Gilbert Simondon parle de machines comme systèmes ayant des potentialités. La perfection est l'ouverture, la "liberté de fonctionnement" comme potentialité, pensée par l'homme, et *activable* selon les situations. La machine n'est donc pas un automate prédéfinie, mais plutôt un système qui réagit et s'adapte à des situations. Le rôle de l'homme est de "régler la marge d'indétermination", d'être le "chef d'orchestre". Et cela peut également concerner les outils, même si ce n'est pas très clair par la suite.

[Page 14] Si l'on ne considère que l'usage dans notre relation avec la machine, alors on passe à côté de ce qu'est la machine, de la réalité technique. Si on est "attaché" à une machine, tout autant ouvrier que dirigeant d'entreprise ou scientifique, alors il est difficile de prendre le recul nécessaire. Seul un poste pivot peut avoir le regard *juste* pour être "comme le sociologue et le psychologue des machines", il s'agit de l'"ingénieur d'organisation". Pour saisir ces "réalités techniques" il faut également faire attention à ne pas avoir un point de vue spécialisé.

[Page 15] Gilbert Simondon aborde la deuxième partie de sa thèse : la réforme de la culture. "La conscience de la nature des machines" doit permettre de réintroduire la technique dans la culture. Et cela passe par l'enseignement, tant sur la forme que le fond : intégration des enseignements de la technique au même titre que les autres – approche généraliste + nouveaux  enseignements.

[Page 15] "La culture actuelle est la culture ancienne" : les conceptions contemporaines liées à la technique – contemporaine – ne sont pas prises en compte, et cela favorise donc des conceptions anciennes basées sur des outils plutôt que sur des machines. Il y a un décalage entre des réalités techniques et une approche ancienne.

[Page 16] "Il faut que la culture redevienne générale, alors qu'elle s'est spécialisée et appauvrie."

[Page 16] Gilbert Simondon insiste sur cette "prise de conscience" nécessaire : elle peut passer par une définition de l'objet technique en cherchant à démontrer qu'il n'est pas qu'un objet utile, un "ustensile".

[Page 17] Les trois niveaux de l'objet technique : l'élément, l'individu, l'ensemble. Il faut considérer trois temps : l'élément technique correspond à l'arrivée de la technique, temps non dramatique qui correspond à l'outil ; l'individu technique correspond à l'entrée de la machine (18e siècle) et probablement le début de la révolution industrielle, la machine est considérée comme un concurrent de l'homme, elle porte et utilise désormais les outils que l'homme utilisait, et cela crée des frictions et un sentiment d'instabilité ; les ensembles techniques correspondent à l'apparition de "la théorie de l'information" (20e siècle) et probablement la révolution informatique, on parle alors de norme, de régulation et de stabilisation, l'ordre est permis par la production d'information elle-même permise par la machine.

[Page 18] Si l'"être technique" n'a pas pu être intégré dans la culture, tant aux niveaux des éléments que des individus, cela pourra – pourrait – être le cas avec les ensembles, c'est une question de stabilité. + ? [relire le dernier paragraphe]

### Première partie – Genèse et évolution des objets techniques

### Chapitre premier – Genèse de l'objet technique : le processus de concrétisation
#### 1. Objet technique abstrait et objet technique concret
[Page 21] L'objet technique a une genèse, une origine, un processus de création et de production qui l'a fait advenir. Mais cette genèse est difficile à définir si l'on part des objets techniques individuellement, car les objets techniques sont très différents entre eux (comparaison moteurs : un type de moteur peut être plus proche d'un autre objet technique que d'un autre type de moteur). Mais l'objet technique peut être définit en observant sa fonction et son fonctionnement, notamment avec son histoire et son évolution.

[Page 23] Il faut donc analyser un objet technique dans sa fonction et son fonctionnement, en comparaison avec le même type d'objet technique dans un autre contexte ou une autre époque. Gilbert Simondon illustre cela avec des moteurs à deux périodes différentes, les moteurs ne sont pas performants uniquement parce qu'ils sont récents, mais parce qu'ils répondent à des nécessités précises, qui elle-mêmes ne dépendent pas forcément de la période (c'est vrai qu'un *vieux* moteur sera bien plus fonctionnel pour un bâteau de pêche qu'un moteur *récent*).

[Page 23-24] Objet technique "ancien" : les fonctions sont isolées, elles fonctionnent pour elles-mêmes et pas avec les autres fonctions, une interaction entre deux fonctions sera jugée comme une "imperfection". {Il y aura donc trois niveaux : une fonction d'un objet technique, un ensemble de fonctions d'un même objet technique, et un ensemble d'objets techniques.}

[Page 24] L'objet technique a une "forme abstraite" ou primitive : il s'agit d'une conception très théorique de l'objet technique, qui le considère comme opérationnel uniquement en "circuit fermé", c'est-à-dire sans prendre en compte l'interaction avec d'autres systèmes ou objets techniques. Si l'on place la forme abstraite de l'objet technique dans un ensemble, alors les problèmes qui adviendront seront dus à la non-intégration des objets entre eux.

[Page 26] "Le problème technique est donc plutôt celui de la convergence des fonctions dans une unité structurale que celui d'une recherche de compromis entre des exigences en conflit." {Si je tente d'illustrer cela avec une chaîne de publication : le langage Markdown est à la fois léger, compréhensible et résilient, mais aussi facilement transformable en HTML. Les deux sont intrinsèquement liés lorsque l'on regarde la chaîne complète, mais il s'agit pourtant de deux fonctions différentes.}

[Page 26-27] Il y a un procédé, une évolution de l'objet technique vers une convergence de ses fonctions, en partant de l'abstrait vers le concret. {Mais est-ce véritablement une convergence nécessaire ?}

#### 2. Conditions de l'évolution technique
[Page 27] Gilbert Simondon cherche les raisons de cette convergence : soit internes pour que les fonctions d'un objet technique correspondent mieux entre elles, soit par nécessité extérieures. Il ne faut pas confondre l'objectif global permis par l'objet technique, et la performance de l'objet technique qui est la capacité à réaliser son objectif propre.

[Page 27] Comparaison entre artisanat et industrie : stade abstrait de l'objet technique d'un côté, et stade concret de l'objet technique de l'autre. {Je suis assez scéptique sur cette vision, une production artisanale ne me semble pas inéssentielle, car elle répond à des besoins précis, sans pour autant être une production en série. Il faut que je creuse les définitions d'*artisanat* et d'*industrie*.}

[Page 28-29] L'"objet technique abstrait" comparé à l'"objet technique concret" : moins performant, plus "fragile", parce qu'il est pensé comme système autonome, comme une brique qui vient s'ajouter à d'autres briques ou à un système. Cette *brique* sera probablement efficace, mais si elle n'est pas totalement *imbriquée* dans le système alors elle devient un danger pour celui-ci. L'objet concret est un ensemble de fonctions imbriquées et non isolées.

[Page 30] Gilbert Simondon parle de performance sans utiliser ce terme : il s'agit bien de diminuer le poids, d'augmenter la vitesse ou d'assurer la sécurité d'une voiture par exemple.

[Page 30-31] Il y a une convergence économique et une convergence technique, la seconde est plus forte que la première, car elle est moins volatile ou versatile. À noter que la convergence technique peut également être le résultat de démarches commerciales ou marketing : par exemple lorsqu'Apple décide de supprimer les ports USB, les autres fabricants risquent le ridicule en les conservant.

[Page 31-32] L'évolution spécifique des objets techniques peut se faire de différentes façons : soit par une "réorganisation structurale" – qui consiste en une refonte du système –, soit par une évolution progressive, fruit de recherches et d'expérimentations.

[Page 32-33] "ce qui était obstacle doit devenir moyen de réalisation." : si un objet technique présente un problème, la résolution de ce problème pourra occasionner d'autres améliorations, le particulier peut avoir une incidence sur le global.

[Page 36] "L"adjonction d'une structure supplémentaire n'est un véritable progrès pour l'objet technique que si cette structure s'incorpore concrètement à l'ensemble des schèmes dynamiques de fonctionnement [...]" {Si je tente de simplifier : Gilbert Simondon parle de "progrès" ou de "perfection" des objets techniques que si les fonctions de ceux-ci sont les mots d'une même phrase. Par exemple ajouter une parenthèse à la phrase complètera le sens de cette phrase, mais cette parenthèse sera trop indépendante, elle a été pensée comme un complément, là où la phrase aurait pu être modifiée dans sa globalité, pour un même résultat : une meilleure compréhension.}

[Page 36] Pour un même résultat ou un résultat assez proche, un assemblage ou une réunion de deux objets techniques ("complication" de la structure l'objet technique) est à distinguer d'un autre objet technique qui donnerait le même résultat. {mal dit}

[Page 37] "Processus de concrétisation" : "chaque élément structural remplit plusieurs fonctions au lieu d'une seule", {c'est un autre effet de l'interconnexion et de l'imbrication des fonctions}. "Processus de différenciation" : des problèmes sont corrigés par des effets secondaires du fonctionnement général, bien mieux que si l'on avait ajouté des éléments extérieurs – "des palliatifs". Il s'agit donc bien de modifier l'ensemble pour corriger des problèmes, et non d'ajouter des éléments extérieurs supplémentaires qui ne viendraient pas s'insérer totalement dans ce fonctionnement global. {On pourrait prendre l'exemple du *patch* en informatique : ça fonctionne mais ce n'est pas efficient.} Ces deux processus semblent se contredire, et pourtant ils sont liés.

[Page 38] Un objet technique a un caractère abstrait et artisanal lorsque certaines de ses fonctions sont "antagonistes" c'est-à-dire lorsqu'elles se contredisent ou s'annulent, cela nécessite des retouches régulières sur l'objet technique pour qu'il puisse remplir ses fonctions. {On pourrait dire que c'est un objet technique *buggé*, que certaines fonctionnalités perturbent d'autres fonctionnalités.}

[Page 41] "l'objet technique progresse par redistribution intérieure des fonctions en unités compatibles, remplaçant le hasard ou l'antagonisme de la répartition primitive ; la spécialisation ne se fait pas *fonction par fonction*, mais *synergie par synergie*". Gilbert Simondon pose le fait que le "sous-ensemble" de l'objet technique est "constitué" par l'interaction des fonctions entre elles, la "synergie" et non la fonction seule, "unique".  
L'objet technique concret est donc celui qui a des synergies qui ne sont pas perturbées par des effets qui ne sont pas prévues par ses fonctions et ses synergies {mal dit}. L'objet technique concret serait donc un objet technique dont le fonctionnement est parfait : plusieurs fonctions sont réalisées – plutôt qu'une seule dans le cas de l'objet technique abstrait – sans "correctifs", sans *patchs* liés. Il y a une articulation de sous-ensembles, et même les effets non désirées deviennent des fonctions de l'objet technique concret. Tout est cohérent. {Mais ne s'agit-il pas là d'un fantasme ?}

[Page 42] Gilbert Simondon compare un "objet concret" à un système physique : l'objet concret a un fonctionnement propre, avec ses propres lois en lien avec les lois physiques, ses propres mécanismes, à tel point que certains effets sont indépendants de son concepteur. Il faut par ailleurs différencier le domaine de l'objet concret – quel est son application ? – et les domaines liés à son fonctionnement, qui peuvent être différents. Il n'y a pas d'objet totalement concret, c'est un objectif à atteindre plus qu'une réalité, et de toute façon cette perfection est limitée par les lois physiques qui concernent l'objet technique, elles-mêmes imparfaites.

[Page 43] Le degré de concrétisation est lié au rapprochement entre sciences et techniques : faible dans l'artisanat, et important dans l'industrie.

[Page 43-44] La concrétisation de l'objet technique est donc quelque chose à atteindre, une sorte d'horizon dont les limites sont toujours mouvantes et liées à la science. Plus la technique se rapproche de la science, plus l'objet utilisant cette technique se concrétise.

#### 3. Rythme et progrès technique ; perfectionnement continu et mineur, perfectionnement discontinu et majeur
[Page 44] "C'est donc essentiellement la découverte des synergies fonctionnelles qui caractérise le progrès dans le développement de l'objet technique." Et cette découverte n'est pas continue, mais "brusque", puisqu'il s'agit à chaque fois de découvertes techniques ou scientifiques, il s'agit donc d'étapes ponctuelles successives.

[Page 46] Par contre les perfectionnements peuvent être de deux types : *fonctionnels* au sens où les synergies entre fonctions sont améliorées, ou *correctives* au sens où les problèmes résiduels notamment dûs aux antagonismes entre fonctions sont réduits ou résolus.

[Page 47] Attention aux "perfectionnements mineurs" : ils peuvent être une entrave aux "perfectionnements majeurs", parce qu'ils ne font que cacher les problèmes de l'objet technique.

[Page 48] Donc il faut parler de genèse en tant qu'évolution de l'abstrait au concret, mais également de progression discontinue, "brusque". Il y a passage d'un objet technique modifié par des évolutions mineures vers un nouvel objet technique qui intégrera ces fonctionnalités avec plus de synergies. On peut donc considérer que l'évolution se fait en deux étapes parallèles : des évolutions mineures qui donneront lieu à une évolutions majeures avec un *nouvel* objet technique. Il peut également y avoir une transmission entre différents domaines techniques, des évolutions abandonnées pourraient devenir la base de nouveaux perfectionnements dans d'autres domaines que ceux concernés initialement.

{Comparaison avec les chaînes de publication : l'amélioration des logiciels (traitements de texte ou logiciels de PAO) par le biais de l'utilisation de systèmes comme XML est une succession d'évolutions mineures. L'évolution majeure serait un nouveau système, qui prend en compte ces nouveaux schèmes, mais de façon plus cohérente, ce qui voudrait dire sans passer par un traitement de texte par exemple. Et ouais !}

{Gilbert Simondon ne parle pas de l'objet technique en tant qu'il répond à un besoin : il parle de la perfection d'un objet technique en soit, partant du principe que les besoins peuvent évoluer sans cohérence pour l'objet technique. Certes, mais il faut tout de même analyser le contexte dans lequel l'objet technique évolue.}


#### 4. Origines absolues d'une lignée technique
[Page 49-50] Gilbert Simondon s'interroge sur l'origine d'une lignée technique, jusqu'où peut-on *remonter* ? Si l'on pourrait croire qu'il n'y a pas de point de départ, avec de la méthode on trouve bien un "commencement absolu", lié à des questions techniques ou de technologie.

[Page 51-52] {Je n'ai pas compris le concept de saturation progressive.}

[Page 52] En lieu et place de genèse, Gilbert Simondon parle cette fois de filiation, d'évolution dans le temps ("primitif", "ancêtre"), de liens temporels et structurels entre des objets techniques plus ou moins perfectionnés, d'inventions successives. Et il reprend son argumentation concernant la concrétisation : des éléments extérieurs permettant le perfectionnement d'un objet technique – nous sommes dans le cas de perfectionnements mineurs – seront intégrés, interiorisés, dans un *nouvel* objet technique. L'exemple pris est celui de la machine à vapeur (chaudière à l'extérieur du cylindre) puis du moteur à gaz (chaudière et cylindre qui ne font qu'un).

[Page 56] L'objet technique abstrait ou primitif n'est pas un système "naturel", il s'agit de l'application de théories, il est "artificiel". L'objet technique concret ou évolué "se rapproche du mode d'existence des objets naturels" : par leur fonctionnement évolué, évolutif, Gilbert Simondon pose ici une partie importante de sa thèse : les objets techniques concrets sont complexes, ils contiennent des fonctions synergiques, ils s'améliorent, ils n'ont plus besoin de l'homme pour maintenir leur fonctionnement, ils se connectent entre eux. {Voilà peut-être une belle définition de l'intelligence artificielle, mais Gilbert Simondon n'en avait peut-être pas encore conscience.}

[Page 58-59] Le mode d'existence des objets techniques est similaire à celui des objets naturels, mais cette similitude concerne le mode d'existence et non les objets en eux-mêmes : il ne faut pas les considérer comme équivalents, pas plus qu'il ne faut faire d'analogies ou de ressemblances extérieures entre les deux, les éléments extérieurs ne reflétant pas l'interiorité des objets techniques. {Car on parle d'objets techniques, et plus particulièrement d'objets techniques concrets, lorsqu'il y a des synergies internes, lorsque le système a un *beau* fonctionnement interne, et non pas lorsque l'objet technique répond à une demande précise et/ou particulière. On ne peut parler d'objet technique comme un objet de la culture que si on considère cette complexité, cette cohérence interne des objets techniques ou entre eux.}  
Gilbert Simondon critique également la "méditation sur les automates" ou la cybernétique, qui se limitent à l'observation d'événements extérieurs, ou qui sont trop spécialisées.

{Est-ce qu'une Progressive Web App (PWA) ne serait pas la concrétisation à la fois du site web et de l'application ? L'application n'est qu'une tentative de faire du web avec un logiciel, avec beaucoup d'antagonismes (utilisation du web en terme de réseau, mais sans le langage du web que sont HTML et CSS), là où la PWA intègre les composants du web et des usages hors ligne et de notifications, en synergie avec les fonctionnalités d'un site web.}

[Page 59-60] Les objets techniques *tendent* vers une concrétisation à travers un processus, alors que les objets naturels *sont* concrets par nature, c'est là l'erreur de la cybernétique. Il ne faut pas confondre une tendance et un état. Il y a donc toujours des résidus d'artificialité dans les objets techniques, plus ou moins importants, il ne faut pas oublier cela.

[Page 60] Cette fameuse "concrétisation" présentée par Gilbert Simondon ne peut pas être uniquement le fruit de la physique et d'un milieu naturel, l'homme et ses contraintes sont nécessaires dans l'immense majorité des cas pour révéler la concrétisation, sauf peut-être dans un cas, celui de l'apparition de l'origine de la vie. {Rien que cela... Mais cette thèse est très intéressante ! D'autant que Gilbert Simondon ne parle pas d'une autre intelligence extérieure dans le cas de l'apparition de la vie !}


### Chapitre 2 – Évolution de la réalité technique ; élément, individu, ensemble

#### 1. Hypertélie et auto-conditionnement dans l'évolution technique
[Page 61-62] La spécialisation rend les objets techniques moins efficients en terme d'adaptation : un objet technique *ancien* pouvait être plus polyvalent qu'un objet technique *récent*, parce que l'une des fonctions de ce dernier est trop importante et nuit aux autres fonctions. Cette évolution peut avoir plusieurs causes selon Gilbert Simondon : éléments extérieurs ou nécessités liées aux objectifs de l'objet technique. {Un site intégrant beaucoup d'interactions, et donc utilisant JavaScript, sera moins interopérable. Cet objet technique répond à des objectifs plus *précis*, mais perd de son *universalité*.}

[Page 62-63] "Il y a deux types d'hypertélie" : la première est de l'ordre de la spécialisation, la seconde correspond à une division de l'objet technique en vue d'une spécialisation (exemple du remorqué et du remorqueur). Gilbert Simondon aborde une troisième hypertélie, celle liée au milieu : l'efficience du fonctionnement d'un objet technique est liée au milieu. {Pour reprendre l'exemple précédent, un site web utilisant des technologies récentes fonctionnera mieux sur certains navigateurs, voir même sur certains systèmes d'exploitation.}

[Page 64-65] L'être humain doit réaliser ou permettre un équilibre de l'objet technique entre deux "milieux". {Je cherche un exemple : une application web doit *fonctionner* sur ordinateur et sur téléphone portable, ces deux milieux sont à la fois proches (informatique + navigateur) et éloignés (clavier VS tactile, systèmes d'exploitation différents, etc.).}

[Page 65-66] Gilbert Simondon porte plus loin sa thèse concernant la concrétisation des objets techniques (le fait que les fonctions d'un système portent l'efficience en ayant plusieurs interactions au lieu d'une seule tâche définie) : l'équilibre entre deux milieux oblige l'objet technique à une "autonomie" en ne s'adaptant pas totalement. Encore une fois il s'agit bien d'un certain inconfort, d'une certaine instabilité, comme si cela était la condition pour garder l'équilibre (exemple du funambule qui doit en permanence équilibrer son poids avec des forces diverses, si il ne bouge plus, il tombe).

{Une chaîne de publication représente un progrès technique (cohérence notamment avec le livre numérique) si elle est en capacité de gérer un même fichier et un même format tout au long de ses étapes. Cela peut être le cas avec une chaîne utilisant des outils web, ces derniers ayant plusieurs fonctions qui ne sont pas que des étapes unilatérales (logique de flux).}

[Page 66-67] Gilbert Simondon parle bien de caractère "plurifonctionnel" : une fonction ou un élément mais également le milieu ou une partie de celui-ci peut avoir plusieurs fonctions, *en même temps*.

[Page 68] "c'est en effet​ grâce aux conditions nouvelles créées par la concrétisation que cette concrétisation est possible".
"L'adaptation-concrétisation est un processus qui conditionne la naissance d'un milieu au lieu d'être conditionné par un milieu déjà donné"
Si je tente de comprendre les propos de Gilbert Simondon, le progrès technique n'est possible que lorsqu'il y a mise en application en milieu réel, physique.
"[...] l'invention concrétisante réalise un milieu techno-géographique [...] qui est une condition de possibilité du fonctionnement de l'objet technique."

[Page 69] Le "troisième milieu" est donc la réunion de l'objet technique et du contexte : les éléments et fonctions s'autoadaptent par le biais d'une invention, cette dernière est une intervention humaine (ni nature, ni objet technique).

#### 2. L'invention technique : fond et forme chez le vivant et dans la pensée inventive
[Page 70] "On peut donc affirmer que l'individualisation des êtres techniques est la condition du progrès technique." Et pour cela il faut plusieurs conditions, tout d'abord l'individualisation se définit par les relations qui s'opèrent dans un milieu que l'"être technique" crée lui-même, qui le conditionne et qui est conditionné par cet être technique {balèse}. Gilbert Simondon parle de "milieu associé", car c'est l'association du naturel et de la technique : le contexte global qui est naturel (lois physiques, eau, air, etc.), et la technique (mécanismes, fonctionnements) crée le milieu associé. Il faut bien prendre tous ces éléments *ensemble*, et pas séparèment, Gilbert Simondon part donc du principe que le milieu naturel *et* technique est *mixte*, et non pas distingué entre la nature et la technique.  
"C'est ce milieu associé qui est la condition d'existence de l'objet technique inventé."

[Page 71] Il y a une dimension de projection pour *penser* le milieu associé dans lequel l'objet technique pourra se réaliser, ou plutôt "exister". Il faut pouvoir *prévoir* ce milieu associé, et c'est une action complexe.

[Page 71-72] La différence entre un être vivant et un "être technique" – un objet technique existant –, réside dans le fait que l'être vivant "porte avec lui son milieu associé qu'il peut inventer". L'objet technique a besoin de l'être vivant pour que ce milieu associé, qui est la condition de son existence, soit inventé. Ou en tout cas l'objet technique a besoin de l'être vivant pour qu'ensuite il se conditionne lui-même.

[Page 72] Gilbert Simondon définit le concept d'*invention* : c'est la forme, "le système de l'actualité", qui prend en charge le fond, "le système des virtualités".

[Page 73] La distinction du milieu et du milieu associé est la suivante : le milieu sert de vecteur d'information, alors que le milieu associé est "homéostatique", c'est-à-dire que les informations qui circulent n'ont pas forcément d'incidences, les structures restent indépendantes.  
L'aliénation est la rupture entre le fond et la forme : "Le milieu associé n'effectue plus la régulation du dynamisme des formes."

{Dans cette partie Gilbert Simondon pose donc plusieurs éléments de sa thèse :

1. l'objet technique est une *invention* ;
2. pour exister, l'objet technique a besoin du milieu associé ;
3. l'invention est un acte humain ;
4. le milieu associé est porté et inventé par l'être vivant, l'objet technique a besoin d'une intervention humaine, l'invention, pour conditionner son milieu associé et être conditionné par son milieu associé}

[Page 74] Le fond apporte une cohérence d'ensemble, un schéma global, une certaine stabilité. La forme, seule, dans le cas de la pensée, est une série de "représentations discontinues", un agglomérat d'éléments sans relations, un ensemble – peut-on encore parler d'ensemble ? – sans logique ou sans objectif.

[Page 74-75] L'objet en tant qu'il est *simplement* "associé" à la vie n'est pas un objet technique, mais quelque chose qui est utile, dont on se sert, qui a un fonction que l'on peut activer. L'objet technique connaît une relation entre son fond et sa forme : il y a une série d'événements qui, ensemble, ont une cohérence selon un schéma général.


#### 3. L'individualisation technique
[Page 75] Gilbert Simondon distingue "individu technique" et ensemble d'individus. Il y a "individu technique" que si le milieu associé est la condition du fonctionnement (de l'objet technique). {On pourrait parler ici d'échanges d'information : dans le cas de l'individu technique, il y a échange d'informations (avec l'extérieur) pour qu'il y ait fonctionnement. Ce n'est pas le cas si le fonctionnement est permis sans relation, échange ou dépendance avec l'extérieur. On revient sur la logique de Gilbert Simondon concernant les causalités récurrentes entre éléments.}

[Page 76-77] La distinction ci-dessus peut être subtile, il s'agit d'observer, dans le fonctionnement de l'objet technique, s'il y a plus qu'échange d'informations, mais réellement "causalité réciproque" : un élément réagira après reçu l'information. {Je comprends le sens d'individualisation utilisé par Gilbert Simondon comme ceci : l'objet technique individué, ou l'un de ses éléments, *réagira*, dans le cas d'un échange d'informations.}

{Nous pouvons imaginer des cas où l'objet technique ne devra pas être individué, où la réception d'information ne devra pas provoquer de réaction ou de perturbation. **Lesquels ?**}

[Page 77] Gilbert Simondon répond à ma question ci-dessus : "on peut poser l'existence de niveaux relatifs d'individualisation des objets techniques." Le niveau de cohérence d'un ensemble technique dépendera du fait que les sous-ensembles aient le même niveau d'"individualisation relative".

[Page 78] Il faut introduire des niveaux : il y a des ensembles et des sous-ensembles. Le "laboratoire" sera le deuxième niveau.

[Page 79] La "causalité récurrente" n'étant pas présente au "niveau supérieur" qu'est le "laboratoire", l'individualité est au niveau du sous-ensemble, là où il y a un milieu associé.

[Page 80] {Attention les yeux} Gilbert Simondon pose la question de savoir s'il peut y avoir un niveau inférieur d'individualité : il s'agit d'"éléments techniques", ils ne possèdent pas de milieu associé mais "peuvent s'intégrer dans un individu".


#### 4. Enchaînements évolutifs et conservation de la technicité. Loi de relaxation
[Page 81-82] Il semble logique que les "éléments techniques" aient une influence sur les "individus techniques". Il y a ce que Gilbert Simondon nomme "un passage de causalité" entre différents niveaux : des éléments aux individus et enfin aux ensembles. Et il peut également y avoir une *circulation* qui *redescend*, des ensembles aux individus puis aux élements.

[Page 82-83] Gilbert Simondon introduit une dimension de temporalité pour l'évolution technique, en plus de la dimension spatiale : la "solidarité" des êtres techniques est dans le temps et pas uniquement présente.  
Gilbert Simondon distingue également l'organe de l'élément technique par le fait que ce dernier est "détachable de l'ensemble qui l'a produit", on peut donc considérer l'"engendré" pour le cas du vivant et le "produit" dans le cas de la technique.  
Par ailleurs, l'évolution se fait de façon non linéaire, en "dents de scie" : il s'agit du rythme de relaxation.

[Page 83] Il faut noter que ce "temps de relaxation" est unique, il ne correspond pas aux temps humains ou géographiques. Le temps de relaxation connaît des périodes plus rapides, il n'est pas continue, il y a parfois des soubresauts, des "jaillissement".

[Page 86] Il faut bien comprendre que différentes applications d'un même système/concept connaîtront une phase de synchronisation – qui n'aura pas pour effet de rendre ces applications identiques –, c'est-à-dire une mise à niveau en terme d'évolution technique. Exemple 1 de synchronisation : énergie électrique et énergie retirée de la combustion de l'essence ; exemple 2 de synchronisation : énergie d'origine nucléaire et énergie d'origine photoélectrique.


#### 5. Technicité et évolution des techniques : la technicité comme instrument de l'évolution technique
[Page 87] "Les différentes aspects de l'individualisation de l'être technique constituent le centre d'une évolution qui procède par étapes successives [...]." La négativité n'est pas "le moteur du progrès", elle est simplement ce qui permet à l'homme d'envisager un changement, et ce changement ne sera lui-même pas forcément moteur également – cela peut même être le contraire.

[Page 87-88] Gilbert Simondon entre à fond dans la notion de "progrès technique" : ce sont les produits des individus ou des ensembles qui vont *passer* d'une époque à une autre, contrairement aux êtres vivants qui se reproduisent *à l'identique*. Du fait de son imperfection par rapport au vivant, l'être technique a "une plus grande liberté" que ce celui-ci, et peut produire des éléments qui, lorsqu'ils atteignent une perfection technique, et une fois réunis, permettront la "constitution d'êtres techniques nouveaux" ! Il s'agit de "production indirecte". {Bingo !}

[Page 89] "La technicité de l'objet est donc plus qu'une qualité d'usage". Il faut distinguer d'un côté le rapport forme-matière et de l'autre la façon dont un objet est conçu, et donc ce qui a été mis en place pour le produire. Il faut également plutôt considérer la technicité comme "l'intermédiaire entre forme et matière".  
"La technicité est le degré de concrétisation de l'objet."

[Page 90] La technicité ne réside donc pas tant dans l'individu technique en tant qu'il est un assemblage d'éléments, mais parce que ses éléments présentent un haut degré de perfection.

{Il est intéressant de regarder les Progressive Web Applications sous la lumière de la pensée de Gilbert Simondon : les PWAs ne sont possible qu'avec l'existence d'un réseau internet, et la propagation de standards et de navigateurs respectant ces standards. En soit les PWAs, si l'on ne regarde pas les éléments techniques ou l'ensemble, ne présentent pas de progrès technique. La technicité des PWAs réside autant dans l'assemblage intelligent – mais simple – que dans la perfection de certains éléments – HTML5, Service Workers. Allons plus loin : les PWAs ne produiront pas de nouveaux individus techniques, mais les éléments qui composent les PWAs oui ! Cela a été le cas avec HTML5 par exemple !}

[Page 91] Les éléments sont ce par quoi la technicité peut être portée, ce que les individus ou les ensembles ne peuvent pas faire, ces derniers la portent mais ne peuvent la transmettre.

[Page 92] Notion d'"imagination technique" par Gilbert Simondon : pas uniquement invention ou représentation, mais "imagination" en tant que perception et construction de "schèmes", assemblage de pièces qui ont des fonctions et de savoir-faire à l'origine de ces pièces. {C'est très maladroit ce que je dis.}

[Page 93] Les technicités sont des puissances, des capacités : selon son emploi – l'utilisation dans un contexte particulier –, il sera plus ou moins efficient.

[Page 95] Gilbert Simondon pousse plus loin les notions de progrès et d'époques. L'objet technique consiste en individus techniques, mais ce sont les éléments techniques de ces individus techniques qui sont "dépositaires" de la technicité, car les éléments techniques survivront à un changement d'époque, et non les individus techniques.

[Page 95-96] Les civilisations industrielles ont des individus techniques, alors que les civilisations non-industrielles non, cette fonction n'est assurée que par des individus humains : ces derniers utilisent des éléments techniques, des outils, les individus humains deviennent le milieu associé des outils.

{Une chaîne de publication industrielle, un objet technique, repose sur le fait qu'il y a existence d'un milieu associé : les interactions entre le fonctionnement de la chaîne et le cadre extérieur de cette chaîne ne repose pas sur des interventions humaines, mais sur une causalité récurrente entre les fonctions ou les éléments de la chaîne. Plutôt que de composer une chaîne de publication avec un traitement de texte, un logiciel de PAO, un outil de synchronisation de fichiers, il faut trouver un standard permettant de conserver un format pivot, un outil de versionnement qui remplacera des interventions humaines (échanges par mail, numérotation des fichiers pour les versions), etc.}

[Page 96-97] La machine et l'homme sont très différents : la machine porte les outils et les dirigent, et l'homme règle et dirige la machine. Il n'y a que la notion d'*individu* qui peut être commune.

[Page 98] Gilbert Simondon fait une distinction intéressante entre le technicien et l'artisan : le technicien est dans un environnement industriel, il a un rôle d'auxiliaire ou d'organisateur de la machine ; alors que l'artisan est l'individu technique, il est un niveau technique. "Pour cette raison, un technicien adhère moins à sa spécialisation professionnelle qu'un artisan."

[Page 99] La machine-outil n'est pas un objet technique, parce qu'elle porte les outils et que l'homme l'a fait fonctionner, l'homme est l'auto-régulation.

[Page 101-102] La conclusion de cette partie est assez claire : Gilbert Simondon résume le problème, la raison du problème, pose des éléments de réponse et ouvre vers un nouveau problème. L'homme ne peut plus être un individu technique dans le cas où l'objet technique existe : l'homme *était* un individu technique lorsque les objets techniques n'avait pas atteint un niveau suffisant de concrétisation, l'homme avait alors le rôle de porteur d'outils. Nouveau problème : comment l'homme va désormais se positionner ? En prenant en compte qu'il y a trois niveaux – l'élément technique, l'individu technique et l'ensemble technique –, l'homme doit désormais intervenir au-dessous et au-dessus, mais pas *dedans*, il s'agit donc uniquement des éléments et des ensembles techniques, l'objet technique est individualisé. Gilbert Simondon introduit le concept de "culture technique" comme permettant de résoudre la confusion autour de l'individualisation de l'objet technique : l'objet technique n'est pas humain, mais parce qu'elle est individu, l'homme croit que la machine a des propriétés humaines. La culture technique devra résoudre ce problème, en découvrant ce qu'est l'objet technique en lui-même.


### Deuxième partie – L'homme et l'objet technique

### Chapitre premier – Les deux modes fondamentaux de relation de l'homme au donné technique
#### 1. Majorité et minorité sociale des techniques
[Page 123] Gilbert Simondon distingue deux statuts qui relient l'homme et l'objet technique : le statut de minorité correspond à un contact de proximité – physique et temporelle – de l'homme enfant avec l'objet technique, l'homme est alors inférieur à l'objet technique ; alors que le statut de majorité correspond à une relation réflexive de l'homme adulte avec l'objet technique – connaissance, maîtrise –, l'homme est alors supérieur à l'objet technique.

[Page 124] Le "manque de cohérence" entre ces deux statuts est à l'origine de la mauvaise perception de la technique.

[Page 125] Dans l'histoire des civilisations, et dans la façon dont elle est rapportée, le statut de minorité est grandement privilégié : "la pensée humaine doit instituer un rapport égal, sans privilège, entre les techniques et l'homme".

[Page 126] "Pour que la culture puisse incorporer les objets techniques, il faudrait découvrir une voie moyenne entre le statut de majorité et le statut de minorité des objets techniques." Il faut trouver un équilibre entre la répresentation de l'artisan – "dominée par son objet" –, et celle de l'ingénieur – elle fait de l'objet un produit.

[Page 127] Plutôt que de juger des technologies incompatibles, notamment parce qu'elles ne proviennent pas de la même époque, il faut les analyser pour ce qu'elles sont. {Je crois comprendre que Gilbert Simondon cherche à écarter les statuts pour mieux comprendre les objets techniques en soit. Régulièrement, il semble que Gilbert Simondon cherche à éloigner l'homme des objets techniques, en tant que l'homme perturbe la perception des objets techniques.}


#### 2. Technique apprise par l'enfant et technique pensée par l'adulte
[Page 127-128] Apprendre tôt une technique implique pour le sujet de rester au niveau du sensible, même s'il est en adéquation avec cette technique, il s'agit d'une "capacité" plus que d'un "savoir". C'est le cas de l'artisan qui a une connaissance pratique, "opératoire", il n'a pas de *réflexion* sur ses connaissances : ce qu'il fait est autant mystérieux – Gilbert Simondon parle de *magie* – pour les autres que pour lui-même.

[Page 128-129] Il y a une connaissance qui est de l'ordre de l'instinct : par proximité et par affinité avec un milieu, un savoir et une pratique très poussés existent. Cette formation est très *poussée*, mais elle a cependant plusieurs défauts, donc celui de n'être pas évolutive, "elle ne peut être modifiée par un symbolisme intellectuel oral ou écrit."

{Est-ce que l'on pourrait considérer les éditeurs comme des artisans, et donc ils pourraient intégrer des fonctions de l'ingénieur, trouver un équilibre entre l'artisan et l'ingénieur, pour être en capacité d'adapter leurs pratiques, leur métier.}

[Page 130] La formation technique sous cette forme est "rigide" : elle est linéaire, et non inclusive.

[Page 130-131] La forme de technique qui repose sur l'instinct et la pratique est également un moyen pour l'enfant de devenir adulte, par une maîtrise de la nature. Cela pose problème, car ce passage est binaire : "une loi de tout ou rien".

[Page 132] Le technicien ou l'artisan rechigne à partager sa connaissance, à montrer sa technique, notamment parce qu'il forme un "couple" avec ce qu'il maîtrise.  
**Mention des imprimeur, éditeur et auteur !**  
Aspect de communauté lié à ce type de connaissance technique : groupes fermés, et d'une certaine façon exclusifs.

[Page 132-133] L'encyclopédie, par son statut de documentation, a bouleversé le type de connaissance technique non rationnelle, non théorique et non scientifique. Notamment parce qu'elle ouvre le groupe : "faire commencer sa recherche au point où s'achève celle des hommes qui l'ont précédé."

[Page 133-134] "Enseignement" qui est "rationnel" et "universel" : réfléxion objective,  connaissances profondes et disponibles pour tous.  
"Une société d'autodidactes ne peut accepter la tutelle et la minorité spirituelle. Elle aspire à se conduire toute seule, à se gérer elle-même."
