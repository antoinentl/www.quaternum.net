---
layout: lectures
title: "Architectures de mémoire"
date: "2020-03-20T10:00:00"
comments: true
published: true
description: "Lecture en cours du livre Architectures de mémoire, sous la direction de Jean-Marie Dallet et Bertrand Gervais."
categories:
- lectures
key_book: dallet_architectures_2019
lecture_status: notes
---
Lecture en cours du livre Architectures de mémoire, sous la direction de Jean-Marie Dallet et Bertrand Gervais.

<!-- more -->

## Notes

### À propos du titre
Avant de commencer la lecture il est nécessaire de s'attarder sur le titre, _Architectures de mémoire_, qui n'est ni _Architecture de mémoire_, ni _Architecture de mémoires_ et non plus _Architectures de mémoires_.
La façon dont est composé le titre peut manifestement marquer plusieurs éléments intéressants :

- il s'agit d'_une_ mémoire, et non de plusieurs mémoires. Ce singulier peut renvoyer à une mémoire commune au sein d'une communauté ou d'une société ;
- le pluriel d'architectures nuance ce singulier, probablement pour souligner deux points :
  - cette mémoire est construire selon des structures/architectures diverses ;
  - la constitution multiple souligne le fait que cette mémoire n'est pas forcément homogène ou monolithique.

### Introduction - "Architectures et palais de mémoire"
(p. 11) Les "nouveaux palais de mémoire" sont autant physiques comme les bibliothèques ou les archives, ou _dématérialisés_ comme les bases de données.
En tant qu'êtres de culture nous faisons désormais appel, continuellement, à des informations _numériques_, comme une seconde carapace.

> Et nous sommes maintenant, semble-t-il, à la lisière d'une culture numérique, une culture qui repose tout entière sur l'efficacité des architectures de mémoire. (p. 10)

(p. 11-12) Il y a une circulation entre les données et ce qu'une société en fait, cela forme des architectures de mémoire.

> Sans mise en relation, sans balisage, il ne peut y avoir de rappel. (p. 11)

(p. 12) La question est donc de savoir comment nous organisons notre mémoire, les informations que nous stockons, et comment nous l'organisons ailleurs qu'en nous-même, et alors qui l'organise ?
La mémoire est une "construction" et non un "acquis".

(p. 12-13) La mémoire serait plus une sélection, et donc un choix de rejet de certaines informations, plutôt qu'un stockage.
(p. 13) Et les données de cette mémoire doivent être interroger, sans interrogation pas de mémoire.

(p. 14) Du livre à l'écran, de la raison graphique de Jack Goody à la raison computationnelle de Bruno Bachimont.
Là où je ne suis pas totalement d'accord c'est que Bertrand Gervais et Jean-Maris Dallet précise que l'écran est ouvert sur un réseau.
Mais c'est également le cas du livre qui est lui aussi ouvert sur un réseau avec son système de citation et de bibliographie – même s'il n'y a pas la même supposée instantanéité qu'avec le réseau Internet.
De la même façon la distinction entre calcul et écriture me paraît critiquable : le calcul est une écriture, le calcul est compris dans l'écriture.

> Parler d'architecture, c'est nécessairement parler de construction, d'une structure qui a été montée, qui existe et qui sert. (p. 15)

(p. 15-16) Les deux auteurs insistent à deux reprises sur cette dimension vivante de l'architecture : "imaginée", "construite", "utilisée", "exploitée", "mise à l'épreuve", "analysée".
Il y a donc à la fois une origine de l'architecture, c'est-à-dire la façon dont elle a été conçue, et un usage de cette architecture.
Le troisième "mouvement" est l'analyse.
Le livre reprend ces trois parties : "Éléments d'une architecture", "Visites guidées" et "L'occupation des sols".

(p. 18) Fin de l'introduction qui souligne le fait que la dimension du temps a son importance pour la constitution d'une architecture qu'est ce livre : les textes ont été à l'origine des communications (de l'énoncé oral à l'inscription numérique puis imprimée).

### Archéologie des médias et tracéologie des datas, pour une nouvelle épistémé numérique, Vincent Puig
> [...] il nous semble intéressant ici [...] d'explorer comment il est possible de repenser les conditions contemporaines de production du savoir. (p. 23)

Vincent Puig s'astreint à un ambitieux programme, en invoquant Friedrich Kittler et des projets menés à l'Iri.

(p. 23-26) Vincent Puig fait une sorte de résumé très condensé de la pensée des médias de Friedrich Kittler, soulignant ses liens (ou ses détachements) avec McLuhan, Lacan, Foucault, Turing ou Shannon.
Une question revient plusieurs fois : les "effets ambigus" des médias, comme la photographie qui remet en cause le texte, le magnétoscope qui vient de la vidéosurveillance, la couleur des films de Goebbels, etc.
Sans pour autant considérer la pharmacologie (chère à Bernard Stiegler).

> Kittler est particulièrement conscient que l'histoire des médias, c'est avant tout l'histoire des normes et des standards qui _métastabilisent_ l'évolution technique au sens de Simondon et en fondent les catégories indispensables à une archéologie qui serait sinon sans repères. (p. 25)

(p. 25-26) Réflexion intéressante sur les normes et les formats : "Nous voyons avec Kittler à quel point notre capacité à comprendre la norme de production de l'image est capitale pour la perception." (p. 25)
Pour comprendre il faut pouvoir remonter la chaîne, c'est-à-dire découvrir comment est construit un format, de quoi est composé un algorithme.
Il faut comprendre la médiation technique à l'œuvre, condition du nouveau médium (je paraphrase).

(p. 26-27) Le signal remplace l'image, la télévision en est un exemple emblématique.
Pour aller plus Vincent Puig faire référence aux interfaces désormais dirigées par des algorithmes.
C'est ainsi que nous passons de média à data.

(p. 27) Vincent Puig introduit la notion de "tracéologie des datas", qui serait le moyen de retrouver une forme de maîtrise ou de compréhension des "systèmes fermés" que nous subissons.

> Ici la tracéologie nous semble une méthode intéressante pour comprendre les algorithmes en comprenant les datas qu'ils produisent. (p. 28)

(p. 28) La condition de cette tracéologie est l'ouverture des programmes et des systèmes qui contiennent les données, malheureusement ce n'est pas toujours le cas, et Vincent Puig propose une autre méthode : la "traçabilité", c'est-à-dire remonter le processus pour comprendre le cheminement (celui-ci étant caché).

(p. 29) Résumé de l'origine du Web en tant que structure de contenus, donc la compréhension humaine a été peu à peu remplacée par la calculabilité automatique : de l'arbre au graphe, "ce processus d'automatisation du sens".

(p. 29-31) Vincent Puig s'attarde sur la standardisation des annotations, avec la Web Annotation.
Intéressant rappel sur le fait que le modèle d'annotation imaginé par le créateur de l'hypertexte Ted Nelson n'est plus possible avec la "logique économique" du web.
Celle-ci étant basée sur une logique des traces et non plus du document : "L'hyperlien comme nouvelle forme d'écriture humaine est aujourd'hui considérablement menacé : on s'en remet aux machines pour faire les liens à notre place."
[La disparition de l'URL dans le navigateur Chrome, annoncée depuis un moment, en serait une preuve de plus.]

(p. 31-32) La "redécentralisation" est nécessaire, et la confiance est sa condition de réussite.

(p. 33-34) Dans la partie "Technologies contributives et nouvelle _épistémé_" Vincent Puig nous offre une perspective assez réjouissante de l'hyperconnexion : la valeur sera dans l'échange entre humains, et non plus dans une capitalisation des données.
Plusieurs exemples sont présentés en quelques paragraphes : Polemic Tweet puis des dispositifs d'annotation (dont Hypothesis).

(p. 36) Vincent Puig nous avertit de pratiques de schématisation qui pourraient être désastreuses : le risque est d'aplanir la richesse de notre pensée et de ses liens.

(p. 37) Les graphes produits seraient un nouveau médium, c'est ainsi que ce chapitre est conclu.
À noter que la construction du texte est la suivante : première partie théorique et historique assez dense, seconde partie qui est une articulation avec des cas pratiques, et enfin troisième partie uniquement consacrée à des exemples concrets mais peu de recul théorique.

### L'architecture de mémoire de Woody Vasulka et le "nouvel espace épistémique", Larisa Dryansky
> Qui dit architecture de mémoire dit d'abord spatialisation de la mémoire. (p. 41)

(p. 41) Cette première phrase de ce chapitre de Larisa Dryansky précise un point crucial, déjà évoqué dans l'introduction, et qui est illustré par l'approche artistique de Woody Vasulka avec sa vidéo _Art of Memory_.
Ce rapport entre temps et espace est également illustré par les choix technique et médiatiques.

> Les objets produits informatiquement de _Art of Memory_ manifestent ainsi une suspension du temps proche de la pétrification, faisant écho à celle du paysage minéral qui leur sert de toile de fond. (p. 42-43)

(p. 45-46) À travers une décortication de cette vidéo et en faisant appel à des références extérieures, Larisa Dryansky démontre la "spatialisation de la fonction mnésique".
La vidéo de Woody Vasulka en est un bon exemple : des souvenirs sont invoqués, chacun trouvant sa place dans un espace.

> Pour désigner ce "découpage" en anglais, Viola s'est servi du terme "carving out" qui signifie plutôt "tailler" comme le fait un sculpteur dans la pierre ou le marbre. (p. 49)

Cet exemple est intéressant sur le fait que Viola a une vision spatialisée du montage.

(p. 52) Larisa Dryansky évoque la différence entre la "dimension spatiale de l'image vidéo" et celle du film : défilement horizontal dans un cas, vertical dans l'autre.
[C'est sans doute lié à l'aspect technique : le montage de film héritant de la bande qui défile verticalement avec un projecteur, contrairement aux interfaces informatiques qui permettent de naviguer horizontalement.]

(p. 55) Après une nouvelle analyse de la vidéo, quelque peu éloignée du sujet direct de la mémoire ou de son architecture, Larisa Dryansky revient sur la thématique de l'ouvrage collectif pour terminer sur un environnement technologique dont lequel il serait difficile de "s'orienter", et "dont il reste toujours à découvrir l'architecture".
Fin un peu étonnante puisque l'auteure nous a fait découvrir en partie cette architecture via l'analyse de la vidéo "Art of Memory".

### Changement de paradigme pour la photographie numérique et l'esthétique du bruit, George Legrady
(p. 57) George Legrady pose tout de suite le cadre de son texte : ses pratiques artistiques sont passées de l'analogique au numérique, et on pressent que cela a un impact sur la conservation de ses œuvres.

> Créer des images à partir d'un code informatique était une découverte majeure et semblait une étape naturelle pour un photographe qui avait commencé à mettre en scène ses images en face de l'objectif et les retravaillait au tirage. (p. 58)

(p. 58-61) George Legrady va expérimenter dès les début de l'informatique, avec des ressources très limitées à l'époque.
La relation entre le "bruit" et l'"image" vient de la manipulation informatique, le fait de pouvoir jouer avec les données d'un fichier.

(p. 61-67) Les différentes œuvres présentées ont toutes en commun le fait de disposer d'une double dimension spatiale et temporelle, ainsi qu'une interactivité avec les visiteurs, interactivité qui prend en compte ces deux dimensions.
Le son traduit la dimension temporelle, alors que l'image traduit la dimension spatiale.

Notons que le terme "mémoire" n'est pas utilisé dans ce chapitre.
