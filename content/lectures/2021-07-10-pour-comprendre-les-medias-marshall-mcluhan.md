---
layout: lectures
title: Pour comprendre les médias, Marshall McLuhan
date: 2021-07-10
bibfile: data/lectures.json
categories:
- lectures
key_book: mcluhan_pour_1968
lecture_status: notes
---
Lecture incontournable {{< cite mcluhan_pour_1968 >}} pour comprendre des écrits plus contemporains.
Ces notes sont déposées ici à titre indicatif, elles sont brutes et contiennent probablement des approximations, des erreurs ou des incompréhensions.

{{< bibliography cited >}}

## Avant-propos
> [...] et l'hèmisphère droit le message, c'est-à-dire le lieu de perception des effets et des formes (gestalt) à travers leur changement. (p. 7)

> Avec l'invention de l'imprimerie, Gutenberg renforça encore la suprématie de l'hémisphère gauche, entraînant la rationalisation de l'organisation du travail, des marchés et des institutions. (p. 8)

Marshall McLuhan fonctionne — ou plutôt son raisonnement fonctionne — par opposition.
Une fois l'opposition binaire établie, il ajoute des nuances, (mais) dans chacune des parties.

## Introduction à la deuxième édition

> "Le message, c'est le médium", cela signifie, à l'âge électronique, qu'un milieu totalement nouveau a été créé. (p. 13)

Vision peut-être trop binaire : "totalement nouveau" est un peu fort, ce "milieu" ne s'est pas fait d'un coup. l'évolution a été plus _lente_.

> À mesure que la prolifération de nos technologies créait toute une série de nouveaux milieux, les hommes se sont rendus compte que les arts sont des "contre-milieux" ou des antidotes qui nous donnent les moyens de percevoir le milieu lui-même. (p. 14)

Voici comment j'interprète cela : ce n'est que par la création (quelle qu'elle soit), la _fabrication_, que nous pouvons construire un environnement de perception et de compréhension.
En d'autres termes : il faut s'accaparer chaque nouvelle technologie médiatique pour pouvoir la comprendre, cette appropriation étant forcément une mise en application — quitte à ce que ce soit un détournement.
C'est clairement la raison d'être des humanités numériques, enfin telles que je les perçois (et non telles qu'elles sont).

> Si l'art est un système "d'alerte préalable", comme on appelait le radar, qui était encore une nouveauté, pendant la guerre de 1939, il est extrêmement pertinent non seulement à l'étude des média. mais aussi à la création de moyens de les dominer. (p. 16)

## Première partie
### 1. Le message c'est le médium
> […] le vrai message, c'est le médium lui-même, c'est-à-dire, tout simplement, que les effets d'un médium sur l'individu ou sur la société dépendent du changement d'échelle que produit chaque nouvelle technologie, chaque prolongement de nous-mêmes, dans notre vie. (p. 25)

Dès cette première partie Marshall McLuhan fait une distinction entre _technologie mécanique_ et _automation_, la première engendrant une perception en terme d'usage, alors que la seconde est 

> En effet, le "message" d'un médium ou d'une technologie, c'est le changement d'échelle, de rythme ou de modèles qu'il provoque dans les affaires humaines. (p. 26)

Il y a là quelque chose qui pourrait être interprété comme une course à l'innovation : est-ce qu'il y a _message_ uniquement si l'effet d'un médium ou d'une technologie est massif ?
Par ailleurs il serait intéressant d'étudier des briques technologiques comme Pandoc pour déterminer si l'effet de son utilisation, étant dans un domaine assez confidentiel, peut être suffisant pour le considérer comme un médium. 

> Les contenus ou les usages des  média sont divers et sans effet sur la nature des relations humaines. (p. 27)

Voilà qui est clair quand à la distinction initiale de cette partie.

> Le message de la lumière électrique, comme celui de l'énergie électrique pour l'industrie, est absolument radical, décentralisé et enveloppant. La lumière et l'énergie électriques, en effet, sont distinctes des usages qu'on en fait. (p. 27)

Notre grille d'interprétation des technologies numériques contemporaines n'est-elle pas dysfonctionnelle ?
Ne faut-il pas reconsidérer, en profondeur, notre façon de comprendre les technologies qui nous entourent, comme étant reliées entre elles plutôt que par le prisme de leurs _usages_ ?

(p. 29-30) Marshall McLuhan déplore que l'on puisse défendre une position selon laquelle c'est l'usage d'une technologie qui détermine cette technologie. "Voilà bien la voix du somnambulisme courant." (p. 29) Une technologie n'est pas un _ajout_.

(p. 30) Marshall McLuhan propose une sorte de critique éclaire de la linéarité, à partir de sa comparaison entre mécanisation et automation, "il n'y a pas de principe de causalité dans une simple succession", l'électricité permet d'envisager une véritable de causalité et une "simple succession".

> Le message du médium cinéma, c'est le passage des connexions linéaires à la configuration. (p. 30)

Cette phrase résonne très fortement avec des critiques que j'ai pu émettre sur la dimension linéaire des chaînes de publication _classiques_, par rapport à des systèmes modulaires où plusieurs tâches peuvent s'accomplir simultanément et d'une certaine façon indépendamment.

(p. 31-32) Marshall McLuhan explique les différences entre représentation et vision globale : d'un côté la recherche d'un message qui n'est pas le médium, et de l'autre quelque chose qui s'apparente à une interrelation entre la technologie et ses effets sur celles et ceux qui l'utilisent.
Et Marshall McLuhan d'introduire déjà la distinction entre médium froid (interaction) et chaud (consommation unilatérale).

(p. 33) Avec cette phrase Marshall McLuhan semble se contredire par rapport à ses propos de début de partie : "Ce n'est qu'à cette condition de se garder hors de toute structure et de tout médium qu'il est possible d'en deviner les principes et les lignes de force." (p. 33)
Est-ce que cela signifie qu'il ne faut pas _pratiquer_ pour comprendre ?

> Nous sommes aussi paralysés dans notre univers électrique tout neuf que l'indigène est empêtré dans notre culture mécanique et alphabétique. (p. 35)

Passons sur cette vision raciste et coloniale, Marshall McLuhan fait le constat que tout cela est nouveau, et qu'ainsi il faut réapprendre, ou plutôt il faut changer de paradigme, ou encore nous changeons de paradigme : abandons de la tribalité au profit de l'individualité.

> Au contraire, dans une culture qui assigne des rôles plutôt que des emplois, les nains, les infirmes et les enfants créent leur propre espace. (p. 35)

Marshal McLuhan défend un monde d'individus non uniformes, ne prônant non pas l'inclusivité comme quelque chose qui vient d'en haut, mais plutôt un monde que chacun et chacune construit, loin des soit-disant standards.
Il faudrait abôlir la "continuité" (référence à la linéarité ci-dessus ?) et l'"uniformité".

(p. 36-37) La plus grande critique faite ici à l'impression à caractères mobiles est la mécanisation.
Si Marshall McLuhan a pu connaître les effets de l'impression automatisée (en partie), je suis curieux de savoir comment il a ou aurait interprété ou critiqué à la fois les technologies d'édition (traitement de texte et logiciels de publication assisté par ordinateur, d'ailleurs Matthew Kirchenbaum en fait mention {{< cite kirschenbaum_track_2016 >}}), mais aussi des chaînes de publication plus contemporaine et moins linéaire (voir aussi Lev Manovich à ce sujet {{< cite manovich_logiciel_2017 >}}).

> Ce n'est pas au niveau des idées et des concepts que la technologie a ses effets; ce sont les rapports des sens et les modèles de perception qu'elle change petit à petit et sans rencontrer la moindre résistance. (p. 37)

### 2. Les média chauds et froids
> Un médium est chaud lorsqu'il prolonge un seul des sens et lui donne une "haute définition". (p. 41)

Par haute définition il faut comprendre une "grande quantité d'information".

> La parole est un médium froid de faible définition parce que l'auditeur reçoit peu et doit beaucoup compléter. (42)

D'un côté le médium froid qui favorise la "participation", et de l'autre le médium chaud qui "décourage l'achèvement".
On pourrait aussi parler d'ouverture et de fermeture.
Cette vision très binaire peut aussi être associée à la distinction que Marshall McLuhan fait entre mécanisation et automation.

(p. 42) Marshall McLuhan s'intéresse aux différents effets que peuvent avoir les médiums chauds et froids, considérant dans le cadre des études sur les médias, que les technologies ne peuvent pas avoir toutes les mêmes conséquences, et surtout qu'elle s'influencent entre elles.
L'exemple récurrent est celui de l'imprimerie : il s'agit d'une évolution de la typographie, elle-même évolution de l'écriture idéographique, elle-même évolution de l'écriture, et dans ces évolutions il y a un échauffement, une intensification.
D'un médium froid, l'écriture, nous passons à un médium chaud, l'imprimerie.
La critique principale ici est de constater que cet échauffement semble contextuel, et ainsi nous pourrions dire qu'aujourd'hui l'imprimerie est un médium froid (ou tiéde ?…) car le livre numérique est un médium chaud (quoique cela ne serait pas totalement le cas, le livre numérique relevant de l'automation, bref…).

(p. 42-43) Le chaud délaisse un potentiel de participation au profit d'un faisceau d'intérêt plus concentré : l'"intensité" est intéressant mais elle engendre une fermeture :

> L'intensité, ou la haute définition, est génératrice de spécialisation et de fragmentation, dans la vie comme dans le divertissement, ce qui explique pourquoi il faut "oublier", "censurer" et refroidir toutes les expériences intenses avant de pouvoir les "apprendre" ou les assimiler. (p. 43)

(Une lecture rapide pourrait laisser penser que Marshall McLuhan a peur de se brûler les doigts, qu'il a des excès de pudibonderie et de réaction bourgeoise de retrait.)

> Les technologies de spécialisation détribalisent. la technologie électrique, non spécialisante, elle, retribalise. (p. 43)

> Le souci de l'_effet_ plutôt que du sens est un changement fondamental à notre époque électrique, car l'effet touche la totalité d'une situation et non plus un seul plan du flux de l'information. (p. 45)

> Il serait alors possible de programmer des sociétés tout entières pour assurer la stabilité du climat émotif, de la même façon que nous commençons à savoir maintenir une certaine stabilité de l'économie mondiale. (p. 47)

Ce passage et ce qui lui précède me laisse un peu interrogateur sur la vision du monde et des sociétés selon Marshall McLuhan, il semble quelque peu déconnecté.

(p. 47-49) Marshall McLuhan fait un (long) développement sur des questions sociales, politiques et urbaines, mais aussi culturelles et musicales.
Pour résumer : tout est vu d'un point de vue binaire entre le chaud et le froid.

(p. 50) Marshall McLuhan introduit une nouvelle dimension : il n'y a pas que le médium qui peut être chaud ou froid, c'est également le cas du monde ou de la culture dans lequel le médium est placé. Il y a soit "des réactions violentes" pour un médium chaud dans une culture froide, voir une mauvaise compréhension.

> En vérité, pour ces littéraires qui ne sont pas conscients de la nature profondément abstraite du médium typographique, ce sont les formes artistiques les plus exubérantes et les plus prenantes qui semblent "chaudes" et la forme littéraire abstraite qui semble froide. (p.52)

### 4. L'amour des gadgets : Narcisse la Narcose
(p. 61-63) Marshall McLuhan présente le mythe de Narcisse et le compare à notre rapport avec la technologie, en faisant le constat que nous cherchons à nous compléter, à nous "amplifier".

> Ce qu'il y a d'intéressant dans ce mythe, c'est qu'il montre que les hommes sont immédiatement fascinés par une extension d'eux-mêmes faites d'un autre matériau qu'eux. (p. 62)

> Avec l'avènement de la technologie électrique, l'homme a projeté ou installé hors de lui-même un modèle réduit et en ordre de marche de son système nerveux central. […] Il se pourrait fort bien que les mécanisations successives des divers organes, depuis l'intervention de l'imprimerie, aient constitué une expérience sociale trop violente et trop excitante pour que le système nerveux central le supporte. (p. 63)

(p. 63-64) Marshall McLuhan fait une analyse des effets des technologies sur les corps, et plus spécifiquement sur les sens, faisant des comparaisons entre des chocs physiques et des chocs _médiatiques_ (le terme est de moi).
Le constat est le suivant : certaines technologies qui favorisent un seul sens opèrent une certaine saturation sans laisser de place [à développer].

(p.65-66) Le rapport entre technologies médiatiques et nos sens doit être pris au sérieux, afin de comprendre qu'une nouvelle dimension apparaît, des effets d'accélération se faisant ressentir notamment.

> Voir, percevoir ou utiliser un prolongement de soi-même sous une forme technologique, c'est nécessairement s'y soumettre. Écouter la radio, lire une page imprimée, c'est laisser pénétrer ces prolongements de nous-mêmes dans notre système personnel et subir la structuration ou le déplacement de perception qui en découle inévitablement. (p. 66)

Dans ce concept de "prolongement", il faut considérer que notre propre fonctionnement est affecté, nous devenons des "servo-mécanismes" (comme si nous n'en étions pas déjà…), et nous alimentons la machine elle-même générant ainsi une sorte de course ou de boucle.
L'innovation serait le résultat de ce processus : créer de nouvelles technologies à partir de l'effet de nouvelles technologies [au secours].

### 5. L'énergie hybride
> Comme la fission ou la fusion, les croisements ou hybridations des média libèrent une énergie et une puissance immense. (p. 69)

C'est le niveau supplémentaire introduit par Marshall McLuhan :

1. "les média, ou prolongements de l'homme, sont des agents qui provoquent des phénomènes"
2. ces phénomènes provoquent eux-mêmes, indirectement, des nouvelles technologies
3. l'"hybridation" ou la "combinaison" permet de mieux comprendre la constitution des média
4. cette hybridation "engendre une nouvelle progéniture"

> Le monde intérieur de l'homme oral est un fouillis d'émotions et de sentiments complexes que le spécialiste occidental a depuis longtemps étouffés ou supprimés en lui au nom de l'efficacité et du sens pratique. (p. 71)

(p. 71-72) Pour Marshall McLuhan l'ère de l'électricité permet de sortir d'un rapport individualiste au monde, rapport créé par la mécanisation.
Cela ne peut pas se faire automatiquement, il s'agit d'une volonté et non d'un processus automatique.

> Le présent livre, en cherchant à comprendre plusieurs média, les conflits d'où ils ont surgi et ceux, plus grands encore, auxquels ils donnent naissance, veut entretenir l'espoir de les atténuer en augmentant l'autonomie de l'homme. Le temps est venu de signaler quelques-uns des effets de l'hybridation ou de la compénétration des média. (p. 72)

> Nos vies, personnelles et collectives, sont devenues des processus d'information, parce que nous avons projeté hors de nous, dans la technologie électrique, nos systèmes nerveux centraux. (p. 73)

> Sauf la lumière, tous les média viennent par paires, l'un comme contenu de l'autre et cachant l'opération des deux. (p. 73)

(p. 73-74) Il ne faut pas tant s'intéresser au "programme" ou au contenu des médias, mais plutôt "aux média comme tels".
Pourquoi ?
Parce que pour Marshall McLuhan ce qui est intéressant ce n'est pas tant l'effet des médias sur nous, mais l'effet des médias les uns sur les autres, entre eux.
Les artistes eux-mêmes composent des œuvres avec plusieurs médias, non pas comme œuvre totale, mais en profitant de l'influence d'un média sur un autre (théâtre et roman par exemple) :

> Les artistes de diverses disciplines sont toujours les premiers à découvrir comment permettre à un médium d'utiliser ou de libérer l'énergie d'un autre médium. (p. 75)

(p. 75-76) Marshall McLuhan s'attarde (un peu trop ?) sur l'exemple de l'adaptation cinématographique, avec le passage du roman au cinéma.
L'exemple devient intéressant quand McLuhan pointe le double mouvement à l'œuvre : roman, puis adaptation du roman au cinéma, puis légitimation du roman gràce au film, puis le livre est décrété comme supérieur au film.

> La recherche opérationnelle fait du principe d'hybridation une technique de créativité. (p. 76)

(p. 76-77) La "rencontre" ou l'"hybridation" de deux média permet d'une part de créer des "formes nouvelles", c'est-à-dire des œuvres qu'un seul média n'aurait pu permettre l'existence, et d'autre part cela nous donne suffisamment de recul pour comprendre les différents médias, "L'instant de leur rencontre nous libère".

> L'hybridation ou la rencontre de deux média est un moment de vérité et de découverte qui engendre des formes nouvelles. Le parallèle entre deux média, en effet, nous retient à une frontière de formes et nous arrache à la narcose narcissique. L'instant de leur rencontre nous libère et nous délivre de la torpeur et de la transe dans lesquelles ils tiennent habituellement nos sens plongés. (p. 77)

Cette idée d'une hybridation serait peut-être une forme de prélude à la réflexion d'Alessandro Ludovico.

Est-ce que l'hybridation des _formes_ des artefacts du livre est une bonne piste de réflexion ?

### 18. L'imprimé
> L'imprimerie par caractères mobiles a été la première expérience de mécanisation d'un métier complexe, et elle devint l'archétype de la mécanisation dans toutes ses applications. (p. 199)

(p. 199-200) Clairement la "typographie" a été un moyen de diffuser la pensée bien au-delà des territoires et des périodes concernées par, par exemple, le livre manuscrit, permettant l'arrivée du "monde moderne".
Ce monde moderne se constitue par un double mouvement : le premier est l'individualisation et la "dé-tribalisation" avec la mécanisation (et donc l'imprimerie), le deuxième est la connexion (terme de moi) la _re-tribalisation_ avec l'automation (et donc l'électricité et les nombreux médias permis par l'électricité).

> La linéarité, la précision et l'uniformité de l'ordonnance des caractères mobiles sont inséparables de ces importantes inventions et formes culturelles de l'expérience de la Renaissance. (p. 201)

L'imprimerie ne vient pas de nul part, il serait intéressant d'imaginer son apparition un siècle plus tôt.

> L'imprimerie […] était un exemple de précision reproductible qui inspira des façons totalement nouvelles de prolonger l'énergie sociale. (p. 201)

Ou comment un outil de diffusion du savoir devient une formidable preuve de concept de l'industrialisation galopante, juste avant l'apparition du capitalisme.
Ou comment "individualiser" pour mieux "additionner".

> Comme la "voiture à traction mécanique", la typographie a été mal comprise et mal utilisée pendant quelques décennies : il n'était pas rare que l'acheteur d'un livre le porte chez un scribes pour le faire recopier et le faire illustrer. (p. 202)

Cet exemple rappelle des pratiques numériques qui miment les pratiques analogiques, comme par exemple le traitement de texte (mal utilisé).

> Le livre fut la première machine à enseigner ; il fut aussi le premier article produit en série. (p. 202-203)

Cette affirmation ne semble pas tout à fait juste, mais on peut tout de même noter l'idée selon laquelle l'imprimerie aurait été un déclencheur.

> Un nouveau médium ne s'ajoute jamais aux média antérieurs et ne les laisse jamais intacts. (p. 203)

Marshall McLuhan (re)boucle ici avec son concept d'hybridation, comme étant le niveau supplémentaire du fonctionnement des technologies médiatiques : elles s'influent entre elles mais sans jamais se remplacer. "Ceci ne tuera pas cela" pour reprendre les mots de Louise Merzeau.

> [À propos de l'imprimé] Il s'agit du principe d'extension par homogénéisation qui constitue la clé de la compréhension de la puissance de l'Occident. La société ouverte est ouverte grâce à un processus typographique uniforme d'éducation qui permet à un groupe social de s'étendre indéfiniment par addition. Le livre imprimé basé sur l'uniformité typographique et la répétition dans l'ordre visuel a été la première machine à enseigner, tout comme la typographie a été la première expérience de mécanisation d'un métier. (p. 203)

Marshall McLuhan tempère certains de ces propos, notamment sur le fait que l'imprimerie n'est pas un bloc monolithique mais le rassemblement de plusieurs médiums, le résultat d'une certaine forme d'hybridation : "le livre imprimé représente une riche synthèse d'inventions culturelles antérieures."

(p. 204-205) L'imprimé remet en cause des modèles sociaux et politiques passés, tout comme l'électricité remet en cause, d'une certaine façon, le modèle engendré par l'imprimé.

> Une technologie qui surgit dans un milieu social l'imprègne tant que toutes les institutions de ce milieu ne sont pas saturées. […] Il serait facile de citer des exemples des processus par lesquels les principes de continuité, d'uniformité et de répétition sont devenus les principes de base du calcul infinitésimal, de l'organisation des marchés, de la production industrielle, de la science et des divertissements. (p. 206)

Ce sont là des arguments entendables, 

### 26. La machine à écrire
Sous-titre : "L'âge de la volonté de faire".

(p. 297-298) Marshall McLuhan aborde un point important : le médium a une influence sur les pratiques.
Que ce soit des poètes ou des dactylographes, le fait d'utiliser une machine à écrire modifie la façon d'écrire ou de percevoir des pratiques d'écriture (j'extrapole un peu).
Par contre il ne faudrait pas sous-entendre que l'apprentissage d'une telle technologie puisse venir remplacer d'autres techniques pédagogiques, ce serait un peu tout mélanger (même chose avec l'ordinateur d'ailleurs).

> Cette espèce d'autonomie et d'indépendance que la machine à écrire confère, selon Charles Olson, à la voix du poète, les femmes d'il y a cinquante ans prétendaient que la machine à écrire les leur donnait. (p. 298)

(p. 299) Marshall McLuhan contraste un peu ses propos en signalant que les 

> La machine à écrire fusionne la composition et la publication et suscite une attitude nouvelle devant le texte écrit et imprimé. (p. 299)

Alors là attention, puisqu'en fait ici Marshall McLuhan ne parle pas de la machine à écrire comme étant un outil de l'écrivain·e, car la machine à écrire vient avec la dactylographe, ce qui signifie que dans la plupart des exemples les auteurs _parlent_ et ne tapent eux-mêmes…

> Devant sa machine à écrire le poète est un exécutant autant qu'un compositeur, tout comme le musicien de jazz. (p. 299)

La machine à écrire est autant un outil d'_inscription_ que de _composition_.

> Composer à la machine à écrire, c'est faire voler un cerf-volant. (p. 300)

Écrire avec une machine à écrire procurerait un plaisir similaire à celui du jeu ?
C'est peut-être ce qu'il faut comprendre ici, en tout cas il y a quelque chose de très positif et joyeux dans ce passage, en espérant que Marshall McLuhan ne défend pas cette position uniquement parce qu'il parle de poésie.

> La machine à écrire est un accélérateur qui a étroitement rapproché l'écriture, la parole et l'édition. (p. 301)

Il y aurait tant à dire sur cette simple phrase :

- les limites typographiques des débuts de la machine à écrire ont tout de même permis d'exprimer, avec des compositions minimales, ce que l'on peut dire à l'oral ;
- le fait de pouvoir produire des objets, des _artéfacts_, publiables, est une avancée énorme. La machine à écrire est un moyen très économique de fabriquer une publication — jusque là limité à la presse typographique notamment ;
- le rapport entre écriture et parole semble limité à la disposition machine à écrire + dactylographe, ce qui devait certes être une réalité dans les années 1960, mais qui manque cruellement de recul critique de la part de Marshall McLuhan (enfin vu les accents racistes ou colonialistes, rien de surprenant).

(p. 302-304) Ce passage concerne les organisations du monde du travail, en lien avec les médias et la machine à écrire, et notamment les questions d'accroissement des équipes ou d'autorité.

> Les circuits électriques permettent d'accomplir les mêmes opérations sans chaîne de montage et sans délégation d'autorité. Particulièrement depuis l'apparition de l'ordinateur, l'essentiel du travail s'accomplit au niveau de la "programmation" et ce travail en est un d'information et de connaissance. […] La machine à écrire, qui comprime ou unifie les diverses tâches de la composition poétique et de la publication, est pour le poète ou le romancier, à peu de chose près, ce qu'est la musique électronique pour le compositeur. (p. 303)

Ces deux affirmations, même nuancées, me paraissent bien naïves.

## 33. L'automation
Cette partie a comme sous-titre "L'école permanente", et elle n'est pas sans rappelée les réflexions de Vilèm Flusser à propos de la fabrique.

> L'automation est information […]. (p. 391)

(p. 391-392) La technologie électrique permet d'abandonner la fragmentation propre à la mécanisation : il ne s'agit plus de composer avec des spécialités, mais de permettre un apprentissage plus transversal et plus global.
Tout est travail et tout est loisir, c'est un peu l'idée que nous produisons sans cesse, en brassant de l'information, et finalement peu importe le but.

(p. 392) En passant de la mécanisation à l'automation ou "cybernation", c'est le processus qui prime par rapport à transformation.
Comme dit précédemment, c'est la formulation des instructions plus que l'application de ces instructions qui importe.
C'est l'ère de la programmation.
Ce ne sont plus les compétences qui sont importantes, mais la façon de les acquérir ou de les organiser.
Marschall McLuhan cible clairement l'éducation ici.

(p. 393) Il y a un passage de la fragmentation à l'instantanéité, comme une sorte de flux global :

> L'automation n'est pas une extension des principes mécaniques de fragmentation et de séparation des opérations. Elle est plutôt une invasion du monde mécanique par l'instantanéité de l'électricité. C'est pour cette raison que les spécialistes de l'automation la décrivent comme une façon de penser tout autant qu'une façon de faire. La synchronisation instantanée de nombreuses opérations a éliminé l'ancien modèle mécanique fait d'opérations en succession linéaire. (p. 394)

Ce qui est intéressant c'est à la fois la filiation entre _mécanisation_ et _automation_, mais aussi cette dimension de linéarité de la mécanisation, dont il manque le pendant pour l'automation.
On avait compris que Marshall McLuhan s'oppose très clairement à cette idée d'une extension quand on parle de technologies médiatiques.

> Et il n'y a pas que l'aspect linéaire et séquentiel de l'analyse mécanique qui ait été éliminé par l'accélération électrique et la synchronisation exacte de l'information que l'on appelle automation. (p. 394)

Cette "synchronisation exacte" est donc le fait de pouvoir traiter simultanément différentes informations, donc de _programmer_.
Il y a, pour Marshall McLuhan, un dépassement de la mécanisation introduite par l'imprimerie à caractères mobiles.
Ce dépassement n'est pas une accélération, mais un changement de paradigme, puisque l'information circule largement et "instantanément".

(p. 394) Contrairement aux "systèmes mécaniques", "la production et la transmission de l'énergie sont complètement séparées de l'organisation de travail qui l'utilise".
En d'autres termes, l'organe organisationnel est indépendant de celui qui produit.

(p. 395-397) Marshall McLuhan, à partir des éléments précédents, proposent (avec force) l'hypothèse selon laquelle le "processus d'automation" va engendrer une pratique de formation plutôt que de production (de produits).
C'est en étant connectés de façon instantanée que les humains peuvent produire de l'information, et envisager tout type de projet.

> L'automation prend la forme du servomécanisme et de l'ordinateur. Cela signifie qu'elle utilise l'électricité comme moyen de conservation et d'accélération de l'information. (p. 396)

> Nous n'avons désormais qu'à nommer ou à programmer un processus ou un produit pour les voir s'accomplir. (p. 397)

Cette vision est un peu réductrice, surtout en regardant le monde de 2021 et ses armées d'ouvriers codeurs.

(p. 397) La vitesse électrique offre aussi une nouvelle vision, il y a des choses qui se révèlent avec cette vitesse et qui était impossible à voir ou à comprendre avec des processus mécaniques.

> La mécanisation repose sur le fractionnement des processus en parcelles homogènes mais sans rapport les unes aux autres. L'électricité rassemble ces fragments parce que sa vitesse d'opération exige un très haut degré d'interdépendance de toutes les phases d'une opération. (p. 398)

La façon dont nous fabriquons (conception et production) des livres encore aujourd'hui et largement inspirée de la mécanisation, les initiatives qui sont basées sur un processus d'automation sont finalement assez rares.

(p. 398-399) Ce changement opéré d'abord dans les dispositifs/appareils, doit se faire aussi dans les organisations humaines : et là il n'est pas possible de le faire _à moitié_, "dans tous les domaines et dans tous les instants".

(p. 400) Marshall McLuhan introduit le concept de _boucle_, ou "feed-back" dans le texte — "dialogue entre un mécanisme et son environnement —, comme nouvel élément par rapport à la mécanisation : c'est le fait de disposer d'un "circuit" où l'information fait un parcours cyclique.
Alors que la mécanisation agissait de façon linéaire et unilatérale ("sens unique" (p. 402)), l'automation oblige à prévoir un parcours logique fermé, au sens où les phases sont liées entre elles de façon globale et croisée.

(p. 400-401) Ce "circuit" global nécessite aussi d'évoluer continuellement : il faut se former, il faut être souple voir malléable, bref Marshall McLuhan annonce une situation que nous connaissons en 2021, et qui ne fait pas rêver (sauf les actionnaires) :

> Avec l'interaction instantanée et complexe des processus, l'industrie automatisée emprunte à la forme organique sa capacité de s'adapter à des usages multiples. […] La machine automatique travaille de façon spécialisée, mais elle n'est pas limitée à un seul type d'articles. (p. 401)

Mais avons-nous réellement eu besoin d'attendre l'électricité pour faire ou voir cela ? Pas si sûr.

> L'invention de l'écriture et celle de l'imprimerie ont été les étapes principales de cette évolution [spécialisation]. (p. 403)

Juste plus bas Marshall McLuhan parle aussi de séparation entre "le savoir" et "l'action.

> Voir dans l'automation la menace d'une uniformisation à l'échelle mondiale, c'est projeter dans l'avenir la standardisation et la spécialisation mécaniques, qui sont désormais choses du passée. (p. 404)
