---
layout: lectures
title: "Du papyrus à l'hypertexte de Christian Vandendorpe"
date: "2019-04-10T10:00:00"
comments: true
published: true
description: "Lecture en cours du livre de référence de Christian Vandendorpe, Du papyrus à l’hypertexte: essai sur les mutations du texte et de la lecture."
categories:
- lectures
key_book: vandendorpe_du_1999
lecture_status: notes
slug: du-papyrus-a-l-hypertexte-de-christian-vandendorpe
---
Notes brutes du livre de référence de Christian Vandendorpe, _Du papyrus à l’hypertexte: essai sur les mutations du texte et de la lecture_.

<!-- more -->

En lien avec cet ouvrage lire [L'écriture du papyrus à l'hypertexte](/2019/12/10/l-ecriture-du-papyrus-a-l-hypertexte), un commentaire de texte très libre.

## Citations et notes
_Les titres ci-dessous correspondent aux titres des fragments de l'ouvrage._

### Présentation
"Par honnêteté intellectuelle, autant que par esprit de recherche, l'essentiel de la présente réflexion a donc été d'abord rédigé à l'aide d'un outil d'édition hypertextuelle élaboré à cette fin et dont les fonctions se sont raffinées au fur et à mesure que se précisaient les besoins. Ce n'est qu'à l'étape finale de la rédaction que les pages ainsi créées ont été intégrées dans un traitement de texte et retravaillée en vue d'une publication imprimée." (p. 10)
Il s'agit donc d'une démarche performative, même si le résultat (le livre édité et imprimé) n'en garde aucune trace.
C'est dommage, et en même temps cette édition date de 1999.

"Alors que la lecture du livre est placée sous le signe de la durée et d'une certaine continuité, celle de l'hypertexte est caractérisée par un sentiment d'urgence, de discontinuité et de choix à effectuer constamment." (p. 11)

### Au commencement était l'écoute
(p. 15-16) L'écriture serait une forme de libération des 3 contraintes de l'oralité (moment, débit, temps).
Mais l'écriture fait perdre des nuances et une identité, elle (l'oralité) revient pour donner une identité et pour _habiller_ les textes désormais froids.

(p. 17) L'écriture n'était que support de l'oralité, puis&nbsp;:

- 4e siècle&nbsp;: première lecture sans parler (Augustin) ;
- 7e siècle&nbsp;: arrivée de la ponctuation ;
- 11e siècle&nbsp;: généralisation de la ponctuation ;
- 12e siècle&nbsp;: les textes sont écrits pour être lus et non plus parlés.

### Écrit et fixation de la pensée
"Pour une part importante des échanges, désormais, le texte tiendra lieu de contexte." (p. 19)
Contrairement à l'oralité qui était le contexte même.

"Ce qui était fluide et mouvant peut devenir précis et organisé comme le cristal, la confusion peut céder la place au système." (p. 20)
(À propos de l'oralité et de l'écriture.)

### Puissance du signe écrit
(p. 22-23) Christian Vandendorpe passe peut-être à côté de quelque chose&nbsp;: l'écriture, en tant que possibilité de transcription de l'oralité, apporte aussi la dimension d'inscription _au moment_ de la lecture.
En d'autres termes l'écriture permet de transcrire l'oralité et donc de _lire_, mais aussi d'écrire au moment de la lecture, et donc d'inscrire de nouveau.

### Écriture et oralité
"Même avec des moyens d'enregistrement moderne, l'oral reste essentiellement prisonnier du fil temporel et installe son auditeur sans la dépendance de celui-ci." (p. 27)
Il est intéressant de noter que depuis des formats ou des standards sont apparus, introduisant une structure dans le son, comme le format [DAISY](https://fr.wikipedia.org/wiki/Daisy_(livre_audio)).

"[...] l'écrit apparaît comme la face idéalisée du langage, le lieu où celui-ci peut prétendre à la perfection." (p. 28)

### Normes de lisibilité
(p. 30) De la même façon que l'écriture était une recherche de perfection par rapport à l'oralité, l'impression a permis l'application de normes pour atteindre une certaine perfection et maîtrise.
Le numérique serait alors, dans une certaine mesure/dimension, le prolongement de cette recherche effréné, inexorable (maniaque ?).

"Chaque microseconde ainsi gagnée se traduit pour le lecteur par une efficacité accrue." (p. 34)
Il y a une véritable recherche de performance&nbsp;: ne pas perdre de temps, disposer d'un possibilité de transmission d'information sans équivoque (programmatique ?).

(p. 40) Le [XML TEI](https://books.openedition.org/oep/1298) serait une des formes les plus aboutie ou avancée d'un langage écrit normalisé, décrit et (peut-être) neutre.
Avec tout ce que peut induire de _jusqu'au-boutisme_ le XML TEI...

(p. 40) Par ailleurs l'usage de dialogues dans l'écriture est un moyen pour son auteur de "charger en émotions" un texte.

### Linéarité et tabularité
(p. 41-42) L'écriture serait linéaire (et non tabulaire), mais Christian Vandendorpe prétend le contraire&nbsp;: "L'œil peut embrasser la page d'un seul regard."
Encore faut-il que le texte soit structuré (paragraphes, titres, etc.) ou que le lecteur ait les compétences pour lire une page (et non des lignes).

(p. 43) Il y a 3 types de linéarité, 3 plans&nbsp;: du contenu, du matériau langagier et du média.

(p. 44-45) La culture orale a mis en place des systèmes de mémorisation, systèmes qui n'ont plus d'intérêt à l'écrit.
La "tabularité visuelle" va prendre le pas sur le "domaine auditif", ce qui est peut-être une perte énorme, puisqu'alors on s'appuie systématiquement sur un support et non sur la mémoire (voir Platon sur le problème que pose l'écriture).

(p. 49-50) Étonnamment le lien hypertexte peut induire une lecture linéaire, contrairement au livre dont l'on peut prendre la mesure de l'épaisseur.
C'est là l'un des problèmes majeur du livre numérique, encore non résolu.

### Vers la tabularité du texte
(p. 51-52) Le volumen était une véritable transcription de l'oralité, avec cette dimension de linéarité&nbsp;: lecture continue du début à la fin.

"Pire encore, comme nous l'apprend Martial, le lecteur devait souvent s'aider du menton pour réenrouler le _volumen_, ce qui avait pour effet de laisser sur la tranche des marques assez malvenues pour les usagers d'une bibliothèque."

(p. 52) Le premier codex était en fait un cahier de note, ce qui est assez étonnant&nbsp;: il a d'abord été utilisé pour écrire et non pour transmettre directement !

"En bref, la page permettra au texte d'échapper à la continuité et à la linéarité du rouleau&nbsp;: elle le fera entrer dans l'ordre de la tabularité." (p. 53)

"Aussi le codex est-il le livre par excellence, sans lequel notre civilisation n'aurait pu atteindre son plein développement dans la quête du savoir et la diffusion de la connaissance." (p. 53)
Voir le livre d'Albert Labarre&nbsp;: _L'histoire du livre_ (http://www.sudoc.fr/074103253, https://www.cairn.info/histoire-du-livre--9782130519928.htm)

(p. 53-54) La dimension de page est essentielle dans la constitution d'une lecture qui n'est plus linéaire.
La lecture peut ainsi devenir sélective grâce à l'autonomie de la page et à sa structuration.

"[...] le potentiel de la page comme espace sémiotique discret [...]" (p. 55)
Des débuts de l'imprimerie à (presque) aujourd'hui, il y a une recherche continue de _mise en tabularité_ du texte&nbsp;: colonnes, gloses, intertitres, blocs liés, emphases, etc.
Que ce soit les premiers livres imprimés ou les sites des journaux.

(p. 56) Exemple de la pagination qui est un outil pour le lecteur qui "favorise aussi la discussion sur les textes."
C'est le point de départ vers d'autres moyens de plus en plus _sophistiqués_.

(p. 56) Ce ne sont pas que des moyens de lire le texte mais aussi d'y accéder, des "modes d'accès au texte" comme des index ou des tables des matières.

(p. 58) Le texte devient surface et non plus "fil"&nbsp;: la linéarité n'est plus un passage obligé.

(p. 58-60) L'histoire du livre oscille entre traduction sémantique et recherche formelle&nbsp;: je ne suis pas d'accord avec cette acception de Christian Vandendorpe, dans la mesure où il ne s'agit pas que de plaisir visuel.
Il serait pertinent d'invoquer des théories du champ du design.

"En somme, le défi du texte imprimé est d'établir un équilibre entre les exigences du sémantique et celles du visuel, l'idéal étant évidemment de combiner ces deux modes d'accès au texte sous un axe cohérent." (p. 61)

(p. 61) Cela prend du temps mais les aspects visuels et les aspects sémantiques tendent à se réunir&nbsp;: par exemple un titre de livre sur une couverture ne sera plus coupé pour privilégier le fond à la forme, et la mise en page justifiée est même parfois abandonnée pour ne pas couper les mots !

(p. 64) À la logique du discours (linéarité temporelle) vient se substituer une logique spatiale (tabularité sémiotique ?), dans le cas de la presse, afin de lier les "unités informationnelles" avec des moyens graphiques.

"Un ouvrage est donc dit tabulaire quand il permet le déploiement dans l’espace et la mise en évidence simultanée de divers éléments susceptibles d’aider le lecteur à en identifier les articulations et à trouver aussi rapidement que possible les informations qui l’intéressent." (p. 65)
Il y a donc deux "réalités différentes" pour la notion de tabularité&nbsp;:

- _tabularité fonctionnelle_&nbsp;: navigation dans l'ouvrage ;
- _tabularité visuelle_&nbsp;: navigation dans la page.

(p. 68-69) Distinction nécessaire entre différents types d'ouvrages qui nécessitent différents types de tabulation&nbsp;: livre savant, roman, livre pour la jeunesse.

### Contexte, sens et effet
"Loin d’être un donné, même cognitif, le sens est le produit de notre activité de compréhension ou d’expression et n’a d’existence que dans le processus même qui le fait naître." (p. 72)

"La caractéristique essentielle de l’écrit, et qui le distingue profondément de l’image, réside dans le fait qu’il fonctionne de manière codée et régulière, à la fois pour la production de significations et pour la compréhension de celles-ci." (p. 76)
La lecture permet au lecteur "d'avance en terrain connu"&nbsp;: le contexte et la donnée sont fusionnés, le texte acquire une certaine indépendance.

(p. 77) La lecture a un fonctionnement mécanique&nbsp;: "Grâce à l’opération de lecture, un contexte est sélectionné parmi les réseaux cognitifs du sujet et mis en relation avec les données proposées par le texte lu, engendrant ainsi des effets de compréhension qui vont en se répétant dans une chaîne continue et, en principe, sans faille."

(p. 78) La mécanisation de la lecture a une limité&nbsp;: le lecteur peut "vagabonder" et ainsi _décrocher_ d'un texte. C'est pour cela que certains auteurs cadrent leurs récits, pour garder le lecteur captif.

"L’espace de la page, avec ses jeux de marge, de couleur et de typographie, produit des effets d’ordre visuel qui peuvent certes solliciter l’attention du lecteur, mais aussi le détourner de la lecture. Selon la nature des opérations sémantiques exigées du lecteur, on pourra avoir intérêt à neutraliser l’effet visuel par le recours à une typographie régulière et la sobriété de la mise en pages." (p. 81)

"Les signes qui produisent un effet ne sont pas nécessairement d’une nature différente de ceux qui produisent du sens." (p. 82)

### Filtres de lecture
(p. 83-86) Un même texte brut peut avoir une interprétation différente en fonction du lecteur et du contexte dans lequel il le lit.

### Textualité&nbsp;: forme et substance
(p. 90) Christian Vandendorpe pose un postulat qui me questionne&nbsp;: l'hypertexte recouvre pour lui les fonctionnalités permises par le texte et pas uniquement par les liens entre des documents. Il semble que par _hypertexte_ Christian Vandendorpe parle en fait d'HTML dans ses possibilités sémantiques et interactives, et non pas du seul lien entre deux pages web.

"Avec l’hypertexte, la part du visuel dans le texte et la dimension iconique sont en voie d’expansion, du fait que l’auteur peut se réapproprier la totalité des outils d’édition dont l’invention de l’imprimerie l’avait dépossédé." (p. 91)
L'auteur peut avoir la légitimité d'intervenir sur le livre comme matériel, sur sa forme, alors que ce n'était pas/plus le cas avec l'imprimé.
Deux questions surgissent ici&nbsp;:

- est-ce que l'auteur peut le faire ? Il s'agit là de savoir s'il a les compétences mais pas que ;
- est-ce que l'auteur doit le faire ? l'éditeur est l'expert du livre et le gardien d'une exigence et d'une cohérence pour sa maison ou sa collection.

### Articulations textuelles
"Un texte ne peut produire des effets de sens que s’il met en place des articulations repérables par l’usager. Cela implique que l’auteur détermine au préalable, d’une part, les masses textuelles proposées au lecteur et, d’autre part, les types d’articulations à établir entre elles." (p. 95)
La structuration du texte est donc primordiale, et c'est en partie à l'auteur de s'en charger !
Christian Vandendorpe souligne le fait que la lecture de fiction met le lecteur dans une position très différente de la _non-fiction_.

### Instances énonciatives
(p. 98) Christian Vandendorpe revient sur la question du contexte, à l'oral beaucoup d'éléments de contexte sont implicites ("je" est finalement identifiable), cela peut être le cas pour un texte sur un support fixe ("ici" gravé sur une pierre), alors qu'un texte imprimé est mouvant.
"le réseau Internet, notre civilisation est entrée dans un nouvel âge, où la "technologisation du mot" est poussée à l’extrême et où la référence se fait encore bien plus mouvante et plus aléatoire que sur le papier. Perdant en stabilité ce qu’il a gagné en fluidité, le texte est devenu une pure configuration immatérielle sans aucune attache avec un lieu d’origine, voire une culture déterminée, accélérant l’obsolescence des appareils d’État."

(p. 99) Il est intéressant de noter que les alias (renvois d'une URL à une autre pour masquer l'usage d'un serveur web en particulier) étaient très utilisés en 1999 mais moins aujourd'hui.
Et on peut aller plus loin que Christian Vandendorpe avec la volonté de Google Chrome de supprimer les adresses&nbsp;: s'agit-il d'un retour en arrière ?

"En outre, la notion de page étant moins fermement établie dans un document hypertextuel que dans le livre papier, et n’étant pas tributaire d’un ordre fixe, comme dans un ouvrage relié, l’instance du discours est amputée de nombreux éléments qui la caractérisent normalement sous le régime imprimé." (p. 99)

(p. 99) La question de la référence et de la façon de l'afficher est intéressante&nbsp;: d'explicite (clairement énoncée avec des mots) la référence devient implicite pour les lecteurs (un simple lien), mais pas pour les robots qui peuvent récolter bien plus d'informations.

(p. 100-101) On constate une certaine homogénéisation avec l'anonymat, l'absence de contexte, la disparition de l'ancrage géographique et l'omniprésence de l'anglais. Si cela représente des avantages dans beaucoup de situations, c'est aussi une perte immense.

### De l'interactivité au langage hors jeu
(p. 103-112) Depuis l'interpellation du lecteur (Diderot, Balzac, Hugo, plus ou moins subtile) jusqu'au jeu utilisant le "pseudo-texte" en passant par les livres dont vous êtes le héros, autant de moyens d'intégrer le lecteur dans le texte ou le récit.

### Variétés de l'hypertexte
"En informatique, la notion d’hypertexte désigne une façon de relier directement entre elles des informations diverses, d’ordre textuel ou non, situées ou non dans un même fichier (ou une même "page"), à l’aide de liens sous-jacents." (p. 113)

(p. 113-114) Il y a un lien entre l'"intertexte" et l'hypertexte, à la différence près que l'hypertexte est informatique (et non littéraire, voir Kristella).

(p. 114-115) Le terme _hypertexte_ créé par Ted Nelson était avant tout un moyen de décrire une "façon nouvelle d'écrire sur ordinateur"&nbsp;: il s'agit d'une "structure non linéaire des idées" – sur la base de Vannevar Bush.
L'invention du web est un "aboutissement sous une forme libre" du système imaginé par Ted Nelson.

(p. 115) Il y a 3 accès (combinables) déterminés par l'auteur d'un hypertexte&nbsp;:

1. Sélection&nbsp;: choix de l'utilisateur. Exemple&nbsp;: catalogue ou dictionnaire (entrées sans lien).
2. Sélection et association&nbsp;: association d'idées. Exemple&nbsp;: encyclopédie.
3. Sélection, association et contiguïté&nbsp;: "les blocs d'information sont accessibles de façon séquentielle". Exemple&nbsp;: livre.
4. Sélection, association, contiguïté et stratification&nbsp;: "les éléments d'information peuvent être distribués en 2 ou 3 niveaux hiérarchisés". Exemple&nbsp;: "plusieurs livres en un seul".

Le niveau/accès 4 correspondrait à un _hypertexte_ avec des éléments déroulables voir à un même texte écrit de plusieurs façons (niveaux de lecture) différentes (de profane à expert).

"Pour être efficace, la lecture doit reposer sur des conventions stables afin de permettre une concentration maximale du lecteur sur le contenu." (p. 120)
Est-ce là une dimension UX (expérience utilisateur) ?

(p. 120-122) Il y a une dimension de contrôle introduite avec l'hypertexte&nbsp;: le lecteur peut faire des choix guidés.

### Contexte et hypertexte
(p. 123-124) L'hypertexte déplace _quelque chose_&nbsp;: une responsabilité ou une mise en contexte, peut-être le choix d'une lecture, une liberté qui laisse peut-être trop de perspective au lecteur et trop peu de responsabilité à l'auteur.

"À quoi bon continuer à cliquer sur des mots quand on ignore absolument le type de texte sur lequel on va déboucher ?" (p. 124)

(p. 124-125) L'enjeu est de retenir et de guider l'attention du lecteur&nbsp;: non pas comme un récit linéaire dont les fragments se suivent, mais plutôt comme un design typographique.
(20 ans après les recommandations de Christian Vandendorpe sont quelque peu touchant&nbsp;: des "événements" comme du son, des effet typographie à chaque changement, de la vidéo.)

"C'est dans cette spectacularisation du texte que réside l'apport le plus révolutionnaire de l'ordinateur comme "machine à lire"." (p. 126)
Je ne suis pas d'accord, je trouve même dommage de réduire l'ordinateur à cela, c'est oublier les facultés d'adaptation de l'écran qui sont pour moi un atout bien plus grand (en terme de responsive et d'accessibilité).
Il est aussi un outil d'écriture et d'interaction.
La spectacularisation évoquée par Christian Vandendorpe se limite souvent à un artifice sans fond, largement repris par des besoins marketing notamment.

### Les limites de la liste
(p. 127-129) La liste a des avantages indéniables, en plus d'être visible sur écran, même si elle se limite à une juxtaposition&nbsp;: "La liste ne peut donc accueillir que des éléments de même nature et situés sur un axe d'équivalence paradigmatique."
La liste ne comporte pas les subtilités permises par les connecteurs, et est ainsi confinée au plan informatif.

### Vers une syntaxe de l'hyperfiction
(p. 131-135) Christian Vandendorpe liste des récits hypertextes avec des fonctionnalités plus ou moins originales, apportant dans tous les cas une dimension nouvelle.

(p. 136) Attention l'hyperfiction peut parfois se limiter à un spectacle que le lecteur _visionne_ (sans être lecteur et sans choisir son _parcours_).

(p. 136-137) Christian Vandendorpe parle d'hybridation à la manière d'Alessandro Ludovico, mais sur les techniques de narration et moins sur la forme ou la diffusion.
Il est intéressant de noter que le roman imprimé s'est d'ailleurs inspiré de l'hyperfiction (il me manque une référence pour appuyer mes propos).

(p. 137-138) Résumé grossier en deux points&nbsp;:

- l'intérêt de l'hypertexte est de créer un nouvel _espace_ non linéaire ;
- l'enjeu de l'hyperfiction est de "garder l'intérêt du lecteur".

"La lecture devient alors une quête – et dans certains cas un parcours initiatique –, où le lecteur recueille des indices qui lui permettent de passer à divers niveaux et d’atteindre finalement à la connaissance attendue." (p. 138)

### Lecture de l'image
(p. 139-140) Définition de la lecture en deux phases&nbsp;: perception et traitement cognitif, par le visuel ou le toucher.
L'ouïe est différent car le son "n'existe que dans la durée de sa production".

"La vue est le sens par excellence de l'intellection car elle permet d'analyser à volonté les données considérées." (p. 140)

(p. 141) Feuilleter ou lire un ouvrage, voir ou regarder&nbsp;: spectateur passif ou actif.

(p. 143-144) Comparaison entre la "lecture" d'un tableau et d'un hypertexte&nbsp;: "Dans le tableau, tous les éléments sont coprésents, alors que dans l'hypertexte ils sont placés dans un rapport de substitution réciproque."

"Surtout, l'image n'appelle pas obligatoirement un décodage." (p. 145)

(p. 146) On retrouve ce que défend Kenneth Goldsmith&nbsp;: le texte peut être comme l'image, se composer et prendre sens selon le contexte.
"Le langage produit du sens (ou du non-sens) et accessoirement un effet; l’image produit un effet (ou un non-effet) et accessoirement du sens."

(p. 147) L'hypertexte introduit une nouvelle dimension en deux points&nbsp;:

- des liens (qui peuvent être explicites) entre des éléments ;
- le contrôle par le _lecteur_ (par le biais d'une lecture _tabulaire_).

### L'écrivain et les images
(p. 151) "Même le titre d'un tableau a la priorité sur le tableau lui-même, comme d'ailleurs tout titre par rapport au texte qu'il charpente."
On constate aujourd'hui une disparition du texte structuré, jusqu'aux titres des publications (exemple de Twitter où des contenus sont publiés directement, sans titre) ou aux légendes des images (exemple d'Instagram qui permet de diffuser des images sans forcément de légende).
Ainsi le lecteur se construit lui-même sa légende ou son titre – ce qui représente un gain en liberté et une perte de contextualisation et amener de la confusion.

"Il n’est pas sûr du tout que les prochaines générations, placées devant des environnements mixtes, liront d’abord le texte comme nous avons si souvent tendance à le faire nous-mêmes. Au contraire, les boucles de rétroaction entre le texte et l’image vont se multiplier à l’infini, ainsi que les zones de mouvance entre la spectacularisation du texte-fragment et la textualisation du tableau." (p. 152)

### Du point et des soupirs
"De tous les signes, c’est sans doute la virgule qui est le témoin le plus révélateur de l’évolution du rapport au texte. Elle pose en effet la question du système global de référence auquel le texte devrait être inféodé." (p. 161)

(p. 162-163) "L'écriture hypertextuelle" inventerait sa propre ponctuation, par exemple le signalement de liens ou les émoticônes.

### Op. Cit.
"Dans un contexte de lecture extensive comme le nôtre, cette vénérable méthode de renvoi revient à contrôler le lecteur en lui imposant un parcours linéaire et d’injustifiables pertes de temps. Résidu fossile du discours oral, ce procédé est donc beaucoup plus proche de la culture du volumen que de celle du codex." (p. 165)
Peut-être que le codex aura eu besoin de l'hypertexte pour se détacher totalement du volumen.
D'autres systèmes ont ensuite été mis en place pour rendre compréhensible une référence quel que soit le passage lu !

(p. 166-167) L'usage de la note est problématique&nbsp;: elle perturbe alors qu'elle pourrait être intégrée au texte.
"Ces difficultés et ces réticences que le livre éprouve à mener deux textes en parallèle sont révélatrices de la prédilection ancienne de celui-ci pour le fil continu, longtemps considéré comme la condition incontournable d’un fonctionnement optimal de la machine textuelle."
Je ne partage pas la position de Christian Vandendorpe sur le fait que cela ne soit qu'une question de gain de temps, il s'agit plutôt d'une question de cohérence et de simplification – le gain de temps est une conséquence.
L'écriture hypertextuelle pourrait tout de même faciliter la lecture en _cachant_ des passages jugés moins prioritaires.

(p. 168) Si l'hypertexte peut devenir un gigantesque système de notes, il est possible de designer les choses autrement pour faciliter la lecture du _paratexte_.

### Lecture intensive et extensive ou les droits du lecteur
(p. 170) La tabularisation favorise une lecture d'écrémage, alimentant une lecture extensive, au détriment d'une lecture intensive ?
C'est surtout la place du lecteur qui change&nbsp;: il choisit ce qu'il lit.

(p. 173) "[...] trois raisons au moins incitent à une lecture fébrile et placée sous le signe de l’urgence."&nbsp;:

1. le confort de lecture est désormais possible (liseuses, téléphones intelligents, design responsif ou adaptatif), contrairement à l'époque où Christian Vandendorpe a écrit ce livre ;
2. Il est vrai que les occasions de cliquer et de partir ailleurs sont nombreuses (autant que les livres imprimés dans l'étagère à côté de moi me sortent de ma lecture) ;
3. enfin, aujourd'hui commenter et annoter est bien plus simple.

### Représentations du livre
(p. 175-179) Si le livre a perdu en force évocatrice au 20e siècle, pour être supplanté par l'ordinateur à la fin des années 1990, il connaît un retour comme argument marketing que Christian Vandendorpe n'avait pas prévu...

### Stabilité de l'écrit
"La permanence texte est désormais chose du passé." (p. 181)
L'informatique permet de modifier un texte à l'infini.

"En remettant son manuscrit à un éditeur, l’auteur lui confie le soin de faire passer une production symbolique dans la sphère des échanges commerciaux, ce qui implique la prise en charge de la normalisation du texte, de la typographie, de la mise en page, de l’impression et de la diffusion. Ces diverses instances, qui mobilisent de nombreux spécialistes, ont pour effet de transformer le manuscrit en un objet socialisé, propre à entrer dans les circuits de consommation et à trouver son public le plus plausible." (p. 181-182)

"Télescopant toutes ces barrières, un texte électronique peut, en quelques heures, être rendu accessible sur Internet, dès que son auteur le considère comme terminé." (p. 182)
Pour Christian Vandendorpe Internet se résumerait donc à un moyen de _publier_ plus vite.

"Surtout, une structure éditoriale [...] fournit la garantie implicite que les textes proposés méritent qu'on s'y intéresse." (p. 182)

### Spatialité de l'écrit et contrôle du lecteur
"Une fois fixées par écrit et mises à plat sur la tablette d’argile ou sur la page, les données verbales ne relèvent plus d’abord de l’ordre du séquentiel. Elles ont troqué l’ordre de la temporalité contre celui de l’espace, échappant ainsi radicalement à leur auteur pour appartenir à l’esprit qui s’en saisit." (p. 184)

"Sous hypertexte, en effet, l’écrit n’est plus régi par des rapports de surface mais de profondeur." (p. 184)

(p. 184-185) Il faut donc noter que l'hypertexte redonne une dimension de contrôle à l'auteur, qu'il avait perdu lors du passage de l'oral à l'écrit.
Il n'y a donc pas que le lecteur qui (re)trouve une forme de maîtrise ou de contrôle.

(p. 187-188) Christian Vandendorpe propose que le texte sous forme hypertexte soit présenté avec des avertissements sur sa longueur et sa forme (par exemple s'il comporte des images).
L'_épaisseur_ du livre est plus subtile, il manque encore un moyen adéquat pour formuler la quantité.
"Bref, l’écran de l’ordinateur doit pouvoir communiquer visuellement quantité d’informations qui, dans le monde physique, sont fournies par les dimensions spatiales des lieux parcourus ou, dans l’univers du livre, par des sensations tactiles ou visuelles périphériques, telle l’épaisseur du volume qu’on a entre les mains."

(p. 188-189) Christian Vandendorpe mentionne "des opérations de lecture et d'écriture", il est assez réjouissant de lire de façon claire qu'il ne s'agit pas que de lecture.

### Le CD-ROM&nbsp;: un nouveau papyrus ?
"Un lecteur d’aujourd’hui qui verrait soudainement ses livres familiers réédités en rouleaux de papyrus ne les reconnaîtrait pas. Il aurait beaucoup de mal à retrouver une section précise dans le cœur du texte, faute de table des matières; à localiser un chapitre en particulier, faute de titre courant; à identifier l’emplacement d’une citation, faute de pagination; à repérer les passages portant sur une question donnée, faute d’index; et à déterminer les sources utilisées par un ouvrage scientifique, faute de bibliographie. Et il ne lui serait pas facile de savoir où s’arrêter dans sa lecture, faute de division en paragraphes." (p. 192)

(p. 192-193) Christian Vandendorpe donne de nombreux arguments pour démonter la thèse selon laquelle le CD-ROM serait plus lié au volumen qu'au codex.

"La lecture sur écran ne pourra séduire durablement les usagers que si elle prend appui sur les acquis de la culture de l’imprimé tout en se libérant des limites inhérentes à un support matériel." (p. 193)

### Retour à la page
"Transposée sur ordinateur, la notion de page possède surtout une valeur métaphorique ou indicielle." (p. 195)
Christian Vandendorpe fait un examen méticuleux du concept de page pour l'imprimé, quelque peu différent pour la "page-écran"&nbsp;:
"La principale caractéristique de la page-écran, et qui la distingue du codex, est qu’elle n’est pas limitée à une dimension fixe, car la fenêtre principale peut être munie d’une flèche de défilement vers le bas ou vers la droite." (p. 197)

(p. 198-201) Christian Vandendorpe explore différentes possibilités pour donner/indiquer l'épaisseur d'un texte, une lisibilité sur écran apporte autant de clarté qu'avec un codex.

### Nouvelles dimensions du texte
(p. 203-205) Christian Vandendorpe revient sur les "événements" que devraient, selon lui, contenir tout hypertexte&nbsp;: à la fois pour compenser la froideur d'une lecture numérique (ordinateur), pour retenir l'attention du lecteur, et pour proposer une nouvelle dimension par rapport à l'imprimé.
Dans mon cas personnel je préfère le contexte physique de mon ordinateur portable à celui de ce livre aux pages trop blanches et au dos trop collé...
Il est intéressant de noter que presque 20 ans plus tard presqu'aucun des artifices imaginés par Christian Vandendorpe n'a été retenu (exemples de Wikipédia et d'autres lecture _enrichies_).

### Métaphores de la lecture
(p. 207-208) Les métaphores qui définissent la lecture d'un livre et d'un hypertexte/hypermédia divergent&nbsp;: recueillir tel le glaneur d'un côté, naviguer en terre inconnue de l'autre.
"Mais peut-on dire du hardi navigateur qu’il lit encore? Certes, il est obligé de lire pour se rendre d’un nœud à un autre. Mais, dans la mesure où il navigue, sa lecture sera hachée, rapide, instrumentale et entièrement orientée vers l’action."

"Sur le Web, le lecteur est devenu son propre navigateur parce qu’il n’y a pas ici de texte unique et que, pour avancer, le lecteur a besoin de prendre des décisions constantes, au gré des nœuds qui s’offrent à sa vue et qu’il parcourt d’un œil rapide sans jamais s’y établir." (p. 209)

(p. 210-211) L'hypertexte apporte des outils plus précis pour la lecture _recherche_, sans avoir d'incidence a priori sur les lectures _précises_ ou rapides.

### Mieux gérer les hyperliens
"Aussi n’est-il pas excessif de dire, sans jeu de mots, que la problématique des liens constitue le maillon faible de la nouvelle organisation textuelle proposée par l’hypertexte." (p. 213)
Les propositions de Christian Vandendorpe sont intéressantes&nbsp;: recontextualiser les hyperliens pour avoir quelques informations _avant_ de cliquer.

### Frontières du livre
"Ce jugement oblige ainsi à prendre une certaine hauteur par rapport à la question initiale et nous rappelle qu’un livre renferme de l’écrit dans une intention donnée. Cette intention répond à deux impératifs majeurs: portabilité et pérennité." (p. 216)

(p. 216-217) Un livre est donc portable et pérenne.
Un livre "électronique" est portable (ou presque en 1999) en revanche sa pérennité est plus complexe.
Quoique celle de l'imprimé étant déterminée par sa conservation matérielle ou sa reproductibilité, l'hypertexte offre une bien plus grande propension à être reproduit (archives du web).

"Aussi le livre papier continuera-t-il longtemps à exister en parallèle, comme moyen de re-connaissance sociale et culturelle." (p. 217)

"Un hypertexte n'est jamais clos." (p. 218)

"Dans de nombreux cas, donc, le projet de lecture ne sera plus déterminé par un auteur ni par une structure éditoriale, mais par des choix personnels organisés autour d’une thématique et menés à terme avec l’aide d’agents informatiques." (p. 220)

### Lecteur, usager ou consommateur de signes ?
"La lecture consiste à recueillir systématiquement des indices convergents participant d'un même univers de sens." (p. 221)

"Le lecteur est, par essence, quelqu’un qui se consacre, pour une durée déterminée, à la perception, à la compréhension et à l’interprétation de signes organisés en forme de message." (p. 222)

(p. 224-225) L'addiction à Internet est du même ordre que celle des livres&nbsp;: un désir de lire ou de communiquer.
Il peut y avoir un effet zapping et consommation du même ordre entre les deux, décuplé avec le web.

### Je clique, donc je lis
"En ce sens, [le zapping] va fondamentalement à l'encontre du projet même qui a guidé l'écriture traditionnelle, qui vise à développer un sujet de façon exhaustive, dans le but d’en proposer une synthèse nouvelle." (p. 229)

"Le lecteur qui aborde un livre est sans cesse porté au-delà de ce qu’il est en train de lire par la promesse d’un dévoilement essentiel – qu'il s'agisse du dénouement d'une intrigue romanesque, de l'appréhension du mystère d'une vie ou d’une compréhension plus large de l’univers." (p. 230)

"Aussi la lecture convient-elle idéalement à un travail de sédimentation cognitive et permet-elle d’inscrire un énoncé dans la durée en lui donnant épaisseur et densité, alors que le monde de l’oralité primaire ne permet d’approcher la densité que par la répétition." (p. 232)

### Entre codex et hypertexte
(p. 235-236) Il faut envisager une modification du lecteur, par le fait d'une lecture personnalisable&nbsp;: le lecteur choisit ce qu'il lit et comment il le lit, dans un _livre_ (électronique).

"On savait déjà que deux personnes qui avaient lu le même livre pouvaient ne pas en avoir fait une lecture identique. Désormais, elles n’auront pas lu le même livre!" (p. 236)

"En fait, plus que par l’oralité, c’est par la séduction de l’image que le texte et la littérature sont aujourd’hui le plus concurrencés. En même temps, l’écrit, devenu omniprésent, subsistera au moins pour une de ses fonctions, qui est de permettre la fixation d’une pensée et d’en faciliter la communication et l’élaboration, d’une façon illimitée." (p. 237)

"Aujourd’hui que le traitement de texte et les correcteurs orthographiques donnent à tout un chacun la possibilité de produire, voire de publier sur papier ou sur le Web un texte d’allure professionnelle, on peut s’attendre à ce que l’écrit fasse plus étroitement partie de l’expérience individuelle que jamais auparavant. En fait, le texte dispose maintenant, pour le soutenir, d’un média de plus." (p. 238)

"L'ordinateur est un média qui s'ajoute à la panoplie des moyens existants." (p. 238)
Ceci ne tuera donc pas cela.

(p. 238-239) La fragmentation est propre à l'hypertexte, mais elle n'est pas nouvelle et déjà présente dans le livre imprimé.

(p. 240-241) Le fragment, la forme qui semble être celle de l'hypertexte, peut apporter des avantages, une nouvelle manière d'écrire et de lire, mais le livre continu est un plus dans la dimension de cohérence intellectuelle.

"Il en va tout autrement de la lecture hypertextuelle sur écran où, faute du concept de livre ou à tout le moins d’une entité unifiée par une même maquette et une même intention, chaque fragment est isolé, pur atoll de sens auquel le lecteur accède au hasard des liens qu’il a activés." (p. 242)
"Alors que le texte imprimé consacre par excellence le triomphe de l’idée fixe et de la cohérence, l’hypertexte donne au lecteur – et à son auteur – la liberté dont jouit le causeur."

(p. 242-243) L'absence d'unité proposée par l'hypertexte, à l'inverse du livre, pourrait amener à une lecture partielle et à du _zapping_, tout autant que susciter et exacerber une lecture active et réfléchie, en réseau.

"Mais il ne faut pas se cacher non plus le fait que l’écriture hypertextuelle destinée à la publication entraîne des contraintes qui excèdent de loin celles de l’écriture traditionnelle. Pour faciliter la tâche du lecteur et lui donner un certain contrôle sur la gestion de l’information qui lui est présentée, le producteur du texte devra consacrer un temps considérable à la mise en forme de la matière textuelle, en la segmentant et en établissant des liens d’un fragment à un autre." (p. 243-244)
Comment structurer ou découper un texte dans le cas de l'hypertexte ?
Où s'arrêter ? Comment éviter les contradictions ? Quel titre donner à tel fragment ? L'auteur fait une analyse fine du problème.
Il est étonnant que Christian Vandendorpe pose cette question car il a lui-même réussi à segmenter son livre en fragments lisibles dans n'importe quel ordre.
Je me demande s'il y a déjà eu des tentatives de rédaction de thèses sous cette _forme_.

"Un texte n'est pas un empilement de modules, pas plus que nos pensées ne sont assimilables à des blocs Lego standardisés susceptibles de se combiner indifféremment les uns des autres." (p. 246)

"Considérée en soi, la spécificité de la lecture réside précisément en ceci qu’elle fait échapper l’activité de compréhension et de construction du sens à la linéarité du temps de la parole." (p. 247)

"L'ordinateur annonce une révolution radicale en autorisant tout un chacun à devenir producteur et distributeur de textes." (p. 248)
Oui effectivement c'est ce qui s'est passé au début des années 2000, les blogs ont probablement été une première expérimentation.
Toutefois la légitimité et la reconnaissance des auteurs ne sont pas facilitées (ou faussement).

"À terme, le lecteur tendra à exiger que chaque œuvre apparaisse sur le support le plus adéquat et le plus fonctionnel, compte tenu des usages de lecture prévus." (p. 248-249)

"Mais le codex va certainement aussi se trouver une nouvelle vie, indépendante du support papier, dans le livre numérique." (p. 249)
Christian Vandendorpe marque une distinction importante&nbsp;: le livre numérique n'est pas l'hypertexte.
Il s'agit d'une annonce du format EPUB, sans le nommer.

"La conclusion la plus sûre est que nous allons vers une hybridation et une diversification des supports de lecture, en même temps que des genres de textes et des types de lecture." (p. 249)
