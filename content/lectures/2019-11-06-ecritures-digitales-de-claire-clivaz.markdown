---
layout: lectures
title: "Ecritures digitales de Claire Clivaz"
date: "2019-11-06T10:00:00"
comments: true
published: true
description: "Lecture en cours du livre de claire Clivaz, Ecritures digitales."
categories:
- lectures
key_book: clivaz_ecritures_2019
lecture_status: résumé et notes
---
Résumé et notes du livre de Claire Clivaz, _Ecritures digitales_.

<!-- more -->
## Résumé

### Présentation de l'ouvrage : Un texte qui s'inscrit dans le contexte des humanités numériques
Claire Clivaz est une chercheuse en humanités numériques (HN) et en théologie, elle dirige la section "Digital Humanities +" à l'Institut suisse de bioinformatique.
Sa recherche est orientée vers l'étude des textes bibliques et les HN.
Les humanités numériques sont ici à considérer comme l'application de nouvelles méthodes pour les sciences humaines et sociales (SHS), sur la manière qu'a ce domaine scientifique de prendre en compte le numérique.
Il faut noter que l'ouvrage _Ecritures digitales_, sous titré "Digital Writing, Digital Scriptures", s'inscrit dans le mouvement des HN, mais aussi dans le domaine de la théologie.

_Ecritures digitales_ est le quatrième livre de la collection "Digital Biblical Studies" de l'éditeur Brill, ce dernier est spécialisé en SHS, et son catalogue comprend plus de 37&nbsp;000 titres.
En plus d'être disponible en version imprimée l'ouvrage est proposé en accès libre sous forme de site web ([https://brill.com/view/title/54748](https://brill.com/view/title/54748)) ou de fichier PDF à télécharger.
Notons ici que cette mise à disposition correspond aux engagements de la communauté des humanités numériques : ouvrir la science.

Nous devons indiquer que l'ouvrage semble respecter des règles typographiques suisses, ce qui expliquerait l'absence de majuscules accentuées.
Aussi les numéros de pages qui figurent ci-dessous font référence au fichier PDF du livre téléchargeable sur la page de présentation.

### Résumé du livre : De l'écriture, des écritures, du corps et des humanités numériques
La tâche que se donne Claire Clivaz semble explicite, elle questionne l'écriture dans un environnement désormais numérique, en incluant _les_ Écritures au sens religieux : "Ce livre s’attache donc à scruter le devenir de l’écriture digitale et des Ecritures digitales, deux parcours rassemblés en français en une seule expression qui sert de titre : _Ecritures digitales_." (p. 1)
Il s'agit donc d'étudier l'écriture au prisme du numérique dans les sciences humaines et plus particulièrement en théologie, ce qui explique la présence d'importantes parties sur les humanités numériques d'un côté et sur l'étude des textes bibliques d'un autre côté.
L'autrice part de la proposition de Jacques Derrida selon laquelle l'écriture "digitale" permettrait d'entrevoir "un nouveau rapport du corps de l’homme aux machines" (p. 1, Claire Clivaz citant Jacques Derrida).
Les quatre parties s'entrelacent autour des concepts d'écriture – l'"écriture digitale" (chapitres 2 et 3) et les "Ecritures digitales" (chapitres 1 et 4) – et se suivent en partant d'une analyse globale pour approfondir ces notions et concepts.
En raison d'une forme d’hétérogénéité entre les chapitres, nous devons les résumer succinctement afin de comprendre la construction des propositions de l'autrice.

Le chapitre 1 aborde la question de la dualité entre lecture et écriture, en prenant comme cas principal les textes de la _Bible_ : le message dépasse désormais le _Livre_, en tant que modèle et format, et cela se traduit avec le numérique par des applications qui permettent d'écouter plutôt que de lire les textes religieux.
Et de conclure sur la question de l'unicité d'un texte dans cette perspective multimodale.
Notons que ce premier chapitre est aussi l'occasion pour l'autrice de souligner l'origine religieuse de certains termes du numérique, _cloud_ et ordinateur notamment.

L'objet du chapitre 2 est d'interroger l'expression "humanités numériques", en se concentrant sur le terme "humanités".
Dans cette seconde section il est peu question d'écriture ou d'Écritures.
Claire Clivaz propose une nouvelle généalogie pour le mouvement des HN : le texte d'Alan Turing sur l'informatique, "Computing Machinery and Intelligence", serait plus fondateur que les initiatives de Roberto Busa autour de l'analyse textuelle de l'œuvre de Saint Augustin.
Et de souligner l'importance du concept d'"esprit" à travers cet article d'Alan Turing, et la dimension corporelle présente dans l'étymologie d'"humanités", longuement commentée.

Le troisième moment de l'ouvrage est consacré à l'écriture, et ambitionne de dépasser "le binôme écriture numérique traditionnelle / computationnelle" (p. 92) : l'horizon peut-il être le corps – pour reprendre la proposition initiale de Jacques Derrida ?
Pour répondre plusieurs approches sont proposées, allant de la difficulté du passage de l'oralité au texte avec l'exemple d'une transcription d'une interview de Jacques Derrida, jusqu'à l'introduction de l'oralité dans le texte avec le cas des commentaires dans le code informatique.
Nous inscrivons dans l'écriture numérique, et celle-ci inscrit en nous, elle dispose d'une dimension corporelle.

Le chapitre 4 prolonge l'hypothèse de la partie précédente – l'écriture numérique développerait "un nouveau rapport du corps humains aux machines" (p. 166) – en l'appliquant aux Écritures bibliques.
L'étude critique de textes – qu'ils soient religieux ou non – dans un contexte numérique doit désormais s'interroger sur la fixité, la _réinscriptibilité_, l'autorité, la pérennité.
L'autrice détaille ces questionnements avec de nombreux exemples.
Les corpus sont aussi profondément bouleversés, tendant vers la "collection digitale", fragmentée et extensible, connectée et liée.
Enrichi de médias autres que le texte, le corpus numérique peut recouvrir une dimension corporelle.

_Ecritures digitales_ se clôt sur un résumé du parcours du livre et propose une ouverture sur la possible appropriation de "la matérialité multimodale de l’écriture et des Ecritures digitales", comme une invitation à observer dans le futur les bouleversements induits par la _corporéité_ des écritures numériques.


## Notes
_La version numérique ayant été utilisée pour la prise de note, seules les parties et sous-parties sont indiquées ici. Charge au lecteur de mes notes de faire une recherche dans la page web (ou le PDF) pour retrouver le passage cité._

En préambule il faut indiquer que la lecture de ce livre est facilitée si l'on dispose d'une culture religieuse, chrétienne et protestante, ce qui n'est pas mon cas.
Même si Claire Clivaz fait un effort important pour contextualiser continuellement, il est parfois difficile d'appréhender totalement la complexité {à compléter}.

### Introduction

#### 1. Préambule
> Ce livre s’attache donc à scruter le devenir de l’écriture digitale et des Ecritures digitales, deux parcours rassemblés en français en une seule expression qui sert de titre : _Ecritures digitales_.

Le premier étonnement est l'absence d'accent aiguë sur le "E" majuscule, les règles typographiques semblent être suisses, il faudra m'habituer.

La tâche que s'est donnée Claire Clivaz semble explicite : questionner l'écriture dans un environnement/monde désormais numérique (ou digital), en incluant _les_ Écritures au sens religieux.
Le plan est intéressant dans sa forme puisque les quatre parties s'entrelacent autour des concepts d'écriture – l'"écriture digitale" (parties 2 et 3) et les "Ecritures digitales" (parties 1 et 4) – et s'enchaînent en partant d'une analyse globale pour ensuite "mener l'enquête".

#### 2. Ecritures en jeu
La question est de savoir ce que devient la culture livresque et religieuse avec l'essor du numérique : "ce qu’il advient des « Ecritures » sacrées pour la tradition judéo-chrétienne dans la culture digitale".

L'avènement du numérique, ou tout du moins sa présence désormais quasi-systématique, peut-il se faire sans notre corps ?
Que ce soit le rapport à la lecture et donc au livre, ou le rapport à l'écriture et donc au geste manuel, il faut prendre en compte cette relation entre numérique et physique.
Claire Rivaz fait appel à Umberto Eco, Jacques Derrida et Robert Darnton pour appuyer ses propos, naviguant entre défense de l'imprimé, angoisse de la disparition du manuel et étonnement du numérique.
Ainsi est introduit le concept d'_humanités_, son étymologie ayant un fort rapport avec le physique, il sera également question d'_humanités numériques_ dans les parties suivantes.

#### 3. La langue et les mots
> Le propos du présent ouvrage est donc de s’interroger à la fois sur l’_écriture_, ce savoir-faire profondément ancré au cœur des sciences humaines, et sur _les Ecritures_ bibliques.

Il s'agit donc, pour Claire Clivaz, d'étudier l'écriture autant dans les sciences humaines que dans la religion.
Une question à ce niveau : la religion est-elle considérée elle-même _dans_ les sciences humaines, comme par exemple avec l'étude des religions ou la théologie ?

L'autrice s'explique sur le choix du français et de l'anglais pour ce livre, comme une nécessité d'attirer un public anglophone tout en défendant la langue française, dans le contexte des sciences humaines et plus particulièrement des humanités numériques.

> Dans cette culture [multimodale digitale], le texte devient un vecteur d’information parmi d’autres [...].

C'est une des affirmations sur laquelle je me suis interrogé pendant toute ma lecture : avec le numérique le texte est justement présent partout, quelque soit le vecteur ou la forme de l'information.
Par exemple même un enregistrement sonore diffusé sur le web sera nécessairement accompagné de données textuelles, quand bien même l'information sera entendue.
Toute information numérique est du texte : une série de langages informatiques, jusqu'aux zéros et aux uns, les atomes du numérique.

#### 4. La Bible au creuset de la culture digitale
De la même façon que le grec ancien n'est plus enseigné, la théologie chrétienne a perdu une place importante dans le domaine universitaire, c'est ce que déplore Claire Clivaz, et je ne comprends pas totalement cette comparaison.
S'il s'agit effectivement de deux socles (parmi d'autres) de nos cultures, il est important de distinguer une civilisation (grecque) d'une religion (chrétienne), qui pour moi ne peuvent pas être placées sur le même plan, quel que soit leur influence.

> je propose de situer ce qu’il advient des Ecritures en lisant d’abord ce qu’il advient de l’écriture en général

Et c'est là une précision très importante et utile, l'entreprise de Claire Clivaz n'est pas d'étudier l'écriture, mais de l'étudier pour comprendre l'évolution des Écritures.

Nous devons noter que dès l'introduction Claire Clivaz étaye régulièrement ses analyses et ses propositions de cas concrets et illustratifs qui facilitent la compréhension du lecteur.


### Chapitre 1 : Les Ecritures hors du Livre
Claire Clivaz annonce les objectifs de ce chapitre au fil du texte : comment est formulée la dualité de la lecture et de l'écriture (point 1) ? "Quel est le rôle de la culture biblique dans l’émergence des digital humanities et comment lire la matérialité digitale depuis la longue histoire du support matériel des Ecritures (point 2)? Qu’en est-il de la question de l’édition même du Nouveau Testament et de l’inscription de ces Ecritures dans une littératie multimodale (point 3) ?"

#### 1. L’identité réformée entre livre et lecture
Le titre de ce premier chapitre s'explique par le fait que les Écritures ont cette spécificité d'être premières dans l'accès à la religion, mais secondes dans les faits.

Claire Clivaz recense des études, éditions, formations et autres courants qui marquent une nette évolution des études des religions avec l'intégration du numérique.

L'ouvrage "Sola Lectura ?" est commenté, en cela que son titre propose l'émancipation de l'écriture par rapport au livre (dans son acception allemande) ou la dissociation des deux (dans sa traduction française).
Il s'agit de la même dichotomie (relative) pour le numérique : entre optimisme et enthousiasme, ou angoisse et regret.

> Sans que l’on puisse, bien sûr, mettre ces doctes opinions en équation avec la « dissociation entre l’écrit et le livre » d’un côté, et l’_Emanzipation der Schrift vom Buch_ de l’autre, il est utile de prendre conscience en entrée d’ouvrage que les acteurs de la vie intellectuelle peuvent réagir de manières diverses au changement culturel en cours. Tout un chacun peut, suivant les cadres et les moments, passer d’un sentiment de dissociation à une conviction d’émancipation, ou inversement, dans son interrogation sur le phénomène des écritures digitales. Ce phénomène invite en tout cas à mesurer ses forces et faiblesses en regard des nouvelles exigences qu’il pose.

#### 2. Les Ecritures et la culture digitale : histoire et matérialité
Cette partie débute sur la question de l'archive et de l'archivage comme outil et support d'une culture.
Sans archive, et donc inscription et écriture, il ne peut y avoir de message, quand bien même c'est celui-ci qui prévaut.
Cette question revient régulièrement dans le début du livre de Claire Clivaz, et c'est le point commun entre écriture et Écritures, d'autant plus avec le numérique : l'inscription précède.
Cela rejoint l'approche défendue par (par exemple) Marcello Vitali-Rosati : l'écriture est là avant toute chose, cet _avant_ n'étant pas temporel.

> [...] Olender valide on ne peut plus fortement le lien entre support matériel d’écriture, archive et christianisme, cette archive « forme de la civilisation chrétienne ».

L'archive ou la question de la mémoire, question qui se décline en recherche de vérité, lieu de la mémoire (interne ou externe avec l'écriture), et lien avec la technique :

> Un dialogue interdisciplinaire est ici indispensable, qui m’amènera au chapitre 3 à fortement militer pour considérer le code comme une écriture digitale à part entière, comme un langage que les chercheurs en sciences humaines feraient bien de découvrir et de s’approprier s’ils souhaitent rester les spécialistes des langues.

Claire Clivaz parle à plusieurs reprises de "sortie du papier", et considère une rupture avec le numérique.
C'est peut-être une erreur d'entrevoir un césure là où il y a peut-être un glissement qui s'opère.
Pour prolonger cette question l'ouvrage d'Alessandro Ludovico, _Post-Digital Print: la mutation de l'édition depuis 1894_ ([voir mes notes et commentaires](/2017/11/21/post-digital-print-d-alessandro-ludovico/)) est d'une grande aide : l'hybridation des supports et des dispositifs est probablement la marque de l'ère numérique, plus que le passage du tout imprimé au tout numérique.

La religion a une place importante dans le nommage des termes du numérique : _cloud_ ou ordinateur font référence à Dieu, même si c'est d'une façon indirecte.
De la même façon, pourrait-on dire, les humanités numériques ont été influencées, sinon créées, par une figure religieuse : le jésuite Roberto Busa.
Claire Clivaz souligne le fait que John W. Ellison a été oublié dans cette filiation des _digital humanities_ (DH), "le premier chercheur à croiser informatique et Bible".
L'autrice conclut cette partie en défendant la thèse de la création des DH suite à l'article du père fondateur de l'informatique, Alan Turing : "Computing Machinery and Intelligence".

#### 3. Vers les Ecritures digitales : édition et littératie multimodale
Claire Clivaz interroge la question de la version dans l'étude de la Bible, internet reconfigurant profondément l'accès au Livre : des versions non _validées_ circulent, certaines sont publiées en libre accès, ou d'autres proposent même des contributions des lecteurs.

> Fidèle à mon choix de posture fait dès 2011, je ne m’enthousiasme sur, ni ne déplore ce qu’il advient via le Web, mais tente par mes travaux de faire la lumière sur les enjeux de ces situations nouvelles et de les expliciter, pour nous permettre d’être acteurs dans ce monde tel qu’il se présente.

Il faut noter cette position de l'autrice, qui me semble être celle d'un·e chercheur·e : ne pas s'attarder sur notre point de vue subjectif par rapport à une évolution, mais observer et analyser ses conséquences.

En évoquant "un retour de l’oralité" Claire Clivaz semble omettre le fait que les textes religieux disponibles en version audio disposent d'un texte : leur code, que ce soit celui du fichier binaire ou celui d'une structuration permettant de naviguer dedans (par exemple le format DAISY).
Par ailleurs l'oralité ne peut pas se résumer à une écoute – ici d'un fichier –, l'oralité est un processus de transmission entre deux humains, intégrant une dimension d'interactivité, ce qui n'est peut-être pas le cas des applications mentionnées par l'autrice.

Ce premier chapitre se referme sur la question de l'unicité d'un texte dans cette perspective multimodale.

### Chapitre 2 : Sauf le nom : nommer les Digital Humanities

#### 1. Préambule
L'objet du chapitre est d'interroger l'expression "humanités numériques", et ses composants "humanités" et "numériques", pour arriver jusqu'au terme "esprit".
Tout cela dans l'objectif de reconsidérer l'origine des humanités numériques.

> [Ce chapitre] portera son attention en particulier à ces deux noms, humanités et esprit.

#### 2. Première partie : Lost in translation? L’odyssée de digital humanities en français

##### 2.1. Introduction
> Autrement dit, en passant d’une langue à l’autre, digital humanities peut muter au point de signifier quelque chose de partiellement différent, ou d’amener à d’autres potentialités de sens.

Plusieurs questionnements se forment autour de l'expression "digital humanities".
En français le sens d'"humanités" n'est pas le même que "humanities" en anglais.
"Digital" est traduit en "numérique" en français, ce qui est remis en cause – pour diverses raisons – par une partie de la communauté scientifique.
En anglais l'abandon de "computing" en faveur de "digital", qui s'explique par la prise en compte des usages plus généraux autant que de la calculabilité à l'œuvre dans le traitement de données dans le champ universitaire.
Sur ce dernier point il faut noter que la prise en considération d'une ère _disruptive_ est critiquée par Bernard Stiegler comme le souligne Claire Clivaz.

Faut-il conserver l'expression anglaise pour éviter des déconvenues linguistiques ?
Ou préférer "digitales" à "numériques" comme le défend Olivier Le Deuff ?

Quoi qu'il en soit la traduction de cette expression pose nombre de problèmes qu'il ne faut pas écarter.

##### 2.2. L’odyssée francophone d’humanités digitales versus humanités numériques
Dans le monde francophone il y a une "hésitation" entre humanités _digitales_ et humanités _numériques_, Claire Clivaz présente les différents arguments, dont celui d'Olivier Le Deuff qui préfère digital en raison de l'origine des DH avec l'usage de l'index.
Une recherche étymologique permet de découvrir que ce n'est pas tant l'index au sens du doigt, mais plutôt du petit caillou {à compléter}.

> Les éléments qui sont mis en évidence dans les pages qui suivent montrent qu’on est loin d’un phénomène anecdotique, et qu’il faut souhaiter que les chercheurs continuent à explorer ce fait langagier en tant que manifestation culturelle de profonds déplacements dans notre rapport au savoir, à l’humain, au corps.

En lisant les arguments de Claire Clivaz et d'autres chercheurs, je me demande si dans les humanités numériques (ou digitales) le rapport au corps serait, dans les usages actuels, à chercher du côté du tactile plutôt que de l'index.
L'index est plutôt un moyen (que ce soit un doigt ou un caillou), et le corps est à chercher ailleurs (ce que fait ensuite l'autrice de ce livre).

Le calcul d'un côté (la _computabilité_), et le corps de l'autre (les humanités, les sciences humaines) : immatériel et matériel.
À cela je dirais que le calcul est bien matériel, puisqu'il nécessite des machines et des connecteurs bien physiques ; et le corps n'est pas si matériel ici, puisqu'il est à peine défini, comme cité mais absent, entité diffuse.

#### 2.3. Le terme « humanités » et ses significations oubliées
> Le pluriel d’« humanités » a été presque oublié en français dans les dernières décennies jusqu’à son récent succès dans le monde des humanités numériques.

Claire Clivaz recense les usages du terme "humanités", pointant sa dimension corporelle, oubliée :

> Est-il possible de comprendre davantage cet oubli contemporain presque total du sens charnel et corporel d’humanité ?

> Il faut à ce stade tenter de considérer ensemble ces trois phénomènes : la presque disparition du sens charnel et corporel d’_humanité(s)_, la fascination pour ce même élément incarné que d’aucuns perçoivent dans _digital(e)_, et la présence persistante et tenace d’_humanités digitales_ en parallèle à _humanités numériques_, malgré toutes les critiques essuyées, provincialisme inclus.

Le langage évolue, supprimant des adjectifs lorsqu'un mot est largement accepté :

> A quel moment a-t-on cessé de parler de digital computer pour ne plus parler que d’ordinateur ?

#### 3. Deuxième partie : Lire Alan Turing et considérer le rôle de l’esprit dans les digital humanities

##### 3.1. Introduction
Le projet de cette partie, qui est en fait un article déjà publié originellement en anglais, est d'explorer une autre piste pour l'origine des DH (autre que la généalogie basée sur Busa) : Alan Turing.

À ce stade il semble que l'ouvrage de Claire Clivaz soit essentiellement un assemblage de textes divers, ne répondant pas tous à la problématique des écritures, pourtant annoncée au début du livre.
Deux options se présentent à nous : l'entreprise ambitieuse d'étudier les écritures numériques avec la nécessité d'embrasser en même temps les DH ; le patchwork de textes pertinents pris isolément, un peu moins rassemblés.

##### 3.2. (Re)lire Ada Lovelace en 1950 et aujourd’hui
Je passe sur cette partie, certes très intéressante, mais assez éloignée du reste de l'ouvrage.
Si ce n'est la question de l'étude de textes contemporains et de la prise en compte de la complexité des contextes de création ou d'étude : il vaut mieux aller aux sources et aux propos originaux des auteurs plutôt que de s'appuyer sur des commentaires indirects.

##### 3.3. Mind, brain, spirit, unthought : décliner les noms de l’esprit
Pour Claire Clivaz Alan Turing donne un bon exemple de la difficulté de nommer l'esprit.

_Fin de la lecture détaillée, faute de temps et d'intérêt._
