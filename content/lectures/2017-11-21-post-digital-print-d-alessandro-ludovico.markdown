---
layout: lectures
title: "Post-Digital Print d'Alessandro Ludovico"
date: "2017-11-21T10:00:00"
comments: true
published: true
description: "Ouvrage de référence dans le domaine de la philosophie de la technique, Du mode d'existence des objets techniques de Gilbert Simondon est un texte que j'ai étudié dans le cadre d'un mémoire. Ce qui apparaît ci-dessous est un commentaire de texte d'une partie de ce livre majeur et passionnant."
categories:
- lectures
key_book: ludovico_post-digital_2016
lecture_status: commentaire et notes
slug: post-digital-print-d-alessandro-ludovico
---
Livre remarquable à bien des égards, _Post-digital print: la mutation de l'édition depuis 1894_ d'Alessandro Ludovico est un texte que j'ai étudié dans le cadre d'un mémoire (disponible [ici](https://memoire.quaternum.net)).
Le commentaire de texte qui précède les notes a été plus spécifiquement rédigé pour une revue en ligne, puis abandonné (parce que le temps passe et toute cette sorte de choses).

<!-- more -->

## Post-Digital Print : prospective de la publication

Les ouvrages abordant l'édition et le numérique sont nombreux, bien souvent concentrés sur des questions économiques et focalisés sur la *grande* édition. La littérature sur ce sujet oublie parfois que les pratiques les plus enrichissantes proviennent d'initiatives alternatives voir expérimentales. Dans ce paysage éditorial *Post-Digital Print : la mutation de l'édition depuis 1894* est un sursaut bienvenu doublé d'un travail de recherche remarquable : cet essai présente et analyse l'évolution historique de l'*imprimé* au contact du numérique.

Alessandro Ludovico est un chercheur, un enseignant, un artiste et un éditeur dont les intérêts et les écrits sont tournés vers le numérique et l'édition alternative depuis plus de vingt ans. Il est le créateur et rédacteur en chef de la revue *Neural* – fondée en 1993 et consacrée aux arts numériques. *Post-Digital Print* est publié en anglais en 2012, suite à un travail de recherche mené à la Hogeschool de Rotterdam, et disponible sous différents formats : un fichier PDF pour une diffusion numérique, et une version imprimée à la fois en offset et en impression à la demande pour une distribution physique. Cette multi-disponibilité reflète ce qu'Alessandro Ludovico expose dans son livre : le papier et le numérique sont désormais complémentaires après plus d'un siècle de cohabitation, de rencontre et parfois de fusion. La traduction française de Marie-Mathilde Bortolotti est permise par les éditions B42 en 2016[^post-digital-print-01].

> Alors l'impression est-elle vraiment morte ? Ou va-t-elle mourir bientôt ? (p. 8)

### Une rétrospective et une prospective de l'édition
*Post-Digital Print* est divisé en six parties, formant à la fois un parcours historique de l'édition et un examen précis de projets éditoriaux et alternatifs depuis 1894. Pourquoi 1894 ? Il s'agit de l'année de publication d'un ouvrage qui imagine l'édition comme intégrant la voix sous forme directe (via le téléphone) et enregistrée (sur cylindre) : *La Fin des livres* d'Octave Uzanne et Albert Robida.
*Post-Digital Print* analyse tout d'abord la supposée "mort du papier" à partir de sept cas illustratifs, pour ensuite examiner l'édition alternative au vingtième siècle, principalement sur la période 1950-1980 qui a vu naître nombre d'initiatives éditoriales liées aux évolutions technologiques, en parallèle d'une édition classique.
Si les bouleversements induits par le *numérique* ont une conséquence sur le papier, l'impression à la demande en est un exemple emblématique qu'Alessandro Ludovico présente à plusieurs reprises.

> L'impression à la demande modifie une des règles fondamentales de l'édition – plus besoin d'investir un (petit) capital de départ. (p. 81)

La suite est une exploration d'expérimentations, de projets et d'entreprises d'édition numérique sous différents axes : que ce soit des plateformes de vente en ligne, la distribution, les bibliothèques, mais également les artistes.
La question de l'archivage, essentielle dans la problématique du papier en lien avec le numérique, est également abordée : de l'interrogation du support à la constitution d'archives, en passant par la diffusion et la visibilité de celles-ci, passées ou futures. Enfin le concept de *réseau* interroge le lecteur : en quoi transforme-t-il l'édition ? Et n'est-il pas le moyen pour l'édition de dépasser la combinaison papier-numérique et de tendre vers une hybridation ?

> la meilleure manière pour les éditeurs de remplir leur rôle spécifique au sein de n'importe quel réseau culturel (ou même du réseau entier de la culture humaine) est de se connecter de manière ouverte et libre à d'autres "nœuds" appartenant au même réseau. Voilà comment nous pouvons produire du sens, et apporter de fait un peu de lumière, au sein de ce réseau mondial dont nous faisons désormais tous partie. (p. 173)

### Vers une hybridation
Dans la conclusion de son ouvrage Alessandro Ludovico revient sur l'évolution des medias, et il note que la situation de l'imprimé est bien distincte de celles de la musique ou de la vidéo : "L'impression, néanmoins, est un cas très différent, puisque le medium – la page imprimée – est plus qu'un simple vecteur de matériel destiné à être montré sur un système d'affichage distinct ; c'est aussi le système d'affichage lui-même." Ainsi, passer au numérique impose une modification de nos usages et de notre rapport avec le livre. L'*hybridation* serait la façon d'aller au-delà des nouveaux modes de lecture ou de consommation, pour atteindre un niveau "processuel" : des nouvelles formes d'accès à la culture et au savoir, le développement de nouvelles formes artistiques, des incidences sur l'environnement social et politique, etc.

L'"impression postnumérique", concept clé de ce livre, serait donc la combinaison de plusieurs éléments : depuis le modèle de la souscription via Internet jusqu'à l'hybridation de l'imprimé et du numérique en passant par l'utilisation d'un ordinateur pour concevoir et produire les objets imprimés et numériques. Voici quelques exemples utilisés par Alessandro Ludovico : l'auteur américain Cory Doctorow propose des formats numériques gratuits (PDF, EPUB ou audio) et imagine plusieurs formes de livres imprimés – du format poche imprimé à la demande via la plateforme Lulu.com à la version plus luxueuse fabriquée par des artisans du livre en nombre limité –, et des façons originales d'y accéder. Le livre numérique peut devenir un produit d'appel, gratuit, pour déclencher des ventes de livres imprimés.
Le projet de Tim Devin d'impression de courriers électroniques, distribués sur les pare-brise des voitures dans différentes villes des États-Unis, est un exemple de la matérialisation d'objets numériques : un simple mail peut retrouver une figure d'autorité dès qu'il y a *impression*.
Enfin, certaines publications imprimées ont évolué vers le numérique, c'est le cas de *Boing Boing* : ce fanzine (imprimé) connaît un succès important à la fin des années 1980 avant de cesser sa publication faute de distributeur ; une version web est alors créée quelques années plus tard, considérées comme un véritable pionnier du phénomène des blogs.

*Post-Digital Print* se referme sur la condition de l'hybridation évoquée plus haut : pour Alessandro Ludovico la métamorphose de l'édition est possible par le dépassement de toute "affiliation idéologique" liée aux medias imprimés ou numériques. Comment ce dépassement est-il possible ? Par la mise en place d'outils, de *workflows* et de communautés autour de la réappropriation des moyens de conception et de production du livre en particulier ou de la publication en général ?

*Post-Digital Print* est un cas à part dans le domaine des ouvrages sur l'édition ou la publication numérique, cas qui pourrait se rapprocher de *6\|5* d'Alexandre Laumonier[^laumonier] – consacré au trading à haute fréquence sous le prisme du développement des moyens de communication. Pour mieux comprendre et étudier un sujet il faut bien évidemment analyser et interroger son progrès historique, ainsi Alessandro Ludovico retrace l'évolution technique de l'édition en illustrant ses propos de descriptions d'expérimentations connues et moins connues : des procédés comme la machine miméographique facilitant la reproduction d'imprimés clandestins ou des premières revues de science-fiction ; la mise en réseau permettant une distribution plus large avec le projet *Whole Earth Catalog* ; la technique d'impression offset avec l'apparition de revues comme *Village Voice* ou *San Francisco Oracle* aux mises en page audacieuses ; la photocopieuse amorçant le mouvement des fanzines ; ou le passage au tout numérique pour certains journaux à partir des années 2008.

Parmi les évolutions que nous pouvons observer depuis 2012 à la suite de l'*écriture* de cet essai, il est difficile de résister à la présentation de quelques exemples qui sont comme la matérialisation ou la confirmation des propos d'Alessandro Ludovico. En voici quelques-uns : la réédition de *Flatland* d'Edwin A. Abbott par Epilogue[^flatland] combinant un objet imprimé de grande qualité et un programme générant des formes en rapport avec le livre, tout cela via une campagne de financement participatif ; *Zeitgeist* de Frank Adebiaye[^adebiaye] offrant deux lectures complémentaires, dont une numérique et aléatoire ; *La dette technique* de Bastien Jaillot et *Design d'icônes : le manuel* de Sébastien Desbenoit chez Le train de 13h37[^le-train] associant tous les deux la souscription *avant* publication et des versions imprimée et numérique ; *Atomic Design* de Brad Frost[^brad-frost] donnant la possibilité de suivre l'écriture du livre et d'y contribuer ; la revue *From–To* de l’ÉSAD Grenoble Valence[^from-to] avec un site web librement accessible et mis à jour régulièrement, et un livre imprimé au format et au prix réduits.

*Post-Digital Print* est une synthèse de ce qui a été entrepris et un aperçu du territoire à explorer. Cet essai parvient à articuler plusieurs axes déterminants pour l'édition : cette volonté d'embrasser la multitude des mutations de l'édition est relativement rare dans les ouvrages théoriques de ce domaine, généralement orientés vers une problématique principale – que ce soit *Après le livre* de François Bon, *Le livre à l'heure numérique* de Françoise Benhamou, *Read/Write Book : Le livre inscriptible*, ou encore *Pratiques de l'édition numérique* de Michael Sinatra et Marcello Vitali-Rosati.
Alessandro Ludovico considère autant des aspects de design et d'usage, des enjeux de diffusion et de distribution, la mise en réseau des acteurs de l'édition, la recherche de stratégies économiques originales, et enfin des démarches d'archivage et d'accès.
Lire et partager *Post-Digital Print* ouvre des perspectives pour la publication d'aujourd'hui et de demain.


## Notes

### Introduction
[Page 7] Alessandro Ludovico commence son ouvrage avec une phrase volontairement provocatrice : "Nous le savons tous : le texte imprimé est mort." Cette courte phrase permet de placer l'objet d'étude de ce livre, qui est l'édition au sens large : l'édition de livre, mais surtout l'édition de presse – journaux, magazines, revues.

[Page 7-8] Cette introduction pose un contexte complexe : du côté de la presse le papier est dans une situation complexe, les versions numériques des journaux se développent, mais les objets éditoriaux physiques continuent d'exister et les librairies et kiosques également. "Alors l'impression est-elle vraiment morte ? Ou va-t-elle mourir bientôt ?" C'est ce que veut étudier Alessandro Ludovico à travers des références et des analyses sur ce contexte particulier, avec une approche historique.

[Page 8-9] Alessandro Ludovico fait plusieurs constats : l'édition cherche un "équilibre parfait" entre le numérique et le physique, ce qui n'est pas toujours bien compris ou perçu ; le papier lui-même doit suivre cette frénésie d'innovations et ce système de "mise à jour en temps réel", tout en restant un support pérenne dans une société du flux ; enfin, si le numérique a "adopté" des caractéristiques propres au papier, le papier cherche encore a intégrer des propriétés du numérique.

[Page 9] Annonce du plan. La première partie est consacrée à l'analyse de la supposée "mort du papier", pour cela Alessandro Ludovico va utiliser sept "cas particulièrement représentatifs".  
"L'Histoire nous apprend que la nature immuable et statique du medium papier a toujours été l'argument principal de ceux qui le déclaraient obsolète." Ce caractère "immuable" est probablement ce qui devient l'avantage le plus fort du papier dans une société en mouvement continu.

[Page 10] La deuxième partie observe et analyse l'édition alternative au vingtième siècle, principalement sur la période 1950-1980 qui a vu naître nombre d'initiatives éditoriales liées aux évolutions technologiques, en parallèle d'une édition que l'on pourrait qualifier de *mainstream*.

[Page 10-11] La troisième partie étudie "la mutation actuelle du rôle du papier imprimé en tant que medium" : comment le papier s'est transformé au contact de l'arrivée du numérique ? L'impression à la demande en est un exemple emblématique.

[Page 11] Le quatrième chapitre examine les expérimentations, projets et entreprises d'édition numérique : que ce soit des plateformes de vente en ligne, la distribution, les bibliothèques, et comment les créateurs s'en emparent.

[Page 12-13] Le cinquième chapitre aborde la question de l'archivage, essentielle dans la problématique du papier en lien avec le numérique : de l'interrogation du support à celle de la constitution d'archives, en passant par la diffusion et la visibilité de celles-ci, passées ou futures.

[Page 13-14] Le sixième et dernier chapitre s'attarde sur la question du réseau : en quoi transforme-t-il l'édition ? Alessandro Ludovico s'intéresse plus particulièrement aux initiatives alternatives, comme bien souvent dans cet ouvrage.

[Page 14] Enfin, le projet d'Alessandro Ludovico synthétise les "différences" et les "similitudes" du papier et du numérique, et fait le point sur son travail de recherche entrepris, qui n'est qu'un point de départ.

### Chapitre 1 : la mort du papier (qui n'a jamais eu lieu)

#### 1.1. Le medium imprimé face aux menaces de la première heure
[Page 15] Malgré l'annonce de la "mort du papier", nous observons des changements profonds et de nouveaux modes de lecture – comprenant "mobilité, fonctions de recherche et d'édition de contenus" –, après ceux d'autres media, mais pas encore la disparition de ce medium. Une analyse historique est nécessaire pour comprendre ce phénomène qui n'est pas nouveau.

[Page 16-17] Les inventions successives et l'arrivée de nouveaux media a pu laisser penser, principalement par la voix de théoriciens, que le papier se ferait dépasser en quelque sorte. Le télégraphe a été le premier moment où cela a pu être observé, suivi par le téléphone puis beaucoup plus tard la radio.

#### 1.2. Les câbles vont étrangler le papier inerte
[Page 17-18] 1894 – qui apparaît dans le sous-titre de ce livre – correspond à l'année de publication d'un ouvrage qui imagine l'édition comme intégrant la voix sous forme directe (via le téléphone) et enregistrée (sur cylindre) : *La Fin des livres* d'Octave Uzanne et Albert Robida. Cette "vision futuriste" et très imaginative est en fait une préfiguration de ce que nous connaissons : de la miniaturisation d'un support de lecture et d'écoute (iPod ou smartphone) à l'autonomie de production des livres (impression à la demande). Le papier ou support imprimé sera voué à disparaître.

[Page 19-20] Nous pouvons noter un point de vue intéressant développé par Octave Uzanne : la lecture est une "tyrannie", car elle demande un effort, d'autant plus à la fin du dix-neuvième siècle où l'électricité est encore très jeune. Malgré cette vision, le livre n'est pas remis en cause.

#### 1.3. *The Readie* : une machine qui lit des mots sortis de leurs pages
[Page 20-21] Au début du vingtième siècle la forme du texte est jugée comme pouvant être améliorée, ainsi Bob Brown imagina un système plus performant – consistant à faire défiler un texte plutôt qu'à parcourir une page –, voir même un nouveau medium qui modifiait aussi la façon d'écrire, sorte de précurseur du microfilm.

#### 1.4. H. G. Wells annonce la mort des journaux : à l'avenir, les dernières nouvelles seront transmises par téléphone
[Page 21-22] Wells dénonçait la presse et le format du livre, se rapprochant d'Uzanne concernant la diffusion par téléphone.

#### 1.5. La radio tente de voler leurs fidèles consommateurs aux journaux
[Page 22] La radio suit le télégraphe puis le téléphone, et est un nouveau moyen de diffusion beaucoup plus large que la presse. Encore une fois la fin du papier est annoncée, mais n'arrivera pas.

[Page 23] Alessandro Ludovico analyse un fait social et historique révélateur du rapport entre l'imprimé et d'autres media comme la radio : suite à une grève des conducteurs-livreurs de journaux en 1945 à New York, la radio ne perça pas tant que ça, le flux ne pris par le dessus sur le support asynchrone que le journal. "Les gens, semblait-il, préféraient le plaisir physique et retardé du mot imprimé au signal radio instantané."

[Page 24] Il faut noter un point très important lié à l'imprimé : c'est le fait que le lecteur conserve la maîtrise de sa consommation de l'information (rythme, relecture, etc.).

#### 1.6. Le pouvoir visuel et "froid" de la télévision *vs* le livre "mort" et la "mosaïque" du journal
[Page 24-25] Alessandro Ludovico continue cette traversée de l'évolution des media, avec cette fois la télévision, et l'analyse que Marshall McLuhan en a fait : cette fois il semble que ce soit ce media qui va supplanter le livre – et non la presse –, notamment parce que la diffusion est extrêmement large, et que le spectateur est actif {ce qui est totalement faux en comparaison de l'activité que nécessite la lecture.}

[Page 26] "Cette nouvelle évolution forcée du texte imprimée, ainsi que son rôle de réflecteur de l'incertitude d'un monde en rapide mutation [...], fut en réalité la raison de sa survie." D'un côté le livre clos et stable, et de l'autre la presse (journal et magazine) "dynamique et en mosaïque".

#### 1.7. Les ordinateurs transposent le papier dans le virtuel : la propagande du "sans papier"
[Page 26-27] Suite logique de l'évolution des media : l'informatique et la dématérialisation qui l'accompagne. Cela a été une recherche de longue haleine, depuis Edison jusqu'à Xerox en passant par Vannevar Bush et son Memex, pour tenter de supprimer le papier des bureaux. Cette recherche s'est transformée en propagande avec l'arrivée de l'informatique.

[Page 28-29] En dehors de l'imperfection du tout informatique dûe au risque de perte de données, le problème est que tout a été imaginé du point de vue de la production, et non de l'utilisation : le papier étant tout simplement plus familier de la majorité, et présentant des propriétés que le numérique ne peut proposer. {Je ne suis pas tout à fait d'accord avec Alessandro Ludovico, le bureau sans papier existe désormais, alors qu'en 2012 on pouvait encore en douter.}  
"Paradoxalement, le papier a même contribué de manière significative à diffuser la culture des nouveaux media et la conscience de leur existence."

#### 1.8. L'hypertexte, ou ce que le papier ne peut pas être
[Page 29-30] Suite : l'hypertexte hors ligne puis connecté. Plus que l'informatique, pour Alessandro Ludovico c'est bien l'hypertexte qui a permis une nouvelle configuration du texte et de sa "linéarité", impossible avec l'imprimé, et ceci est réellement nouveau – contrairement aux autres media, puisque l'hypertextualisation du texte n'est pas réversible (impossible de passer de l'hypertexte au papier).

[Page 30-31] Nombreux sont ceux qui ont imaginé ou annoncé une évolution sans précédent du texte grâce à l'hypertexte. Et pourtant, l'hypertexte introduit plutôt des nouvelles formes, particulièrement riches, qu'un remplacement de formats déjà connus – du livre à la presse.

#### 1.9. La mort du papier... est encore à venir
[Page 32-33] C'est "le rôle du document imprimé" qui a profondément changé, et non son existence. "Le document imprimé est devenu une chose plus précieuse et moins accessoire." Ce qui va faire changer les modes d'édition c'est principalement le réseau, bien plus que le télégraphe, le téléphone, la radio, la télévision, l'informatique ou l'Internet mondial. Et Alessandro Ludovico d'avancer que le papier et le numérique sont désormais complémentaires : le "matériel imprimé" représente la stabilité et une certaine accessibilité, comparé au numérique qui nécessite un dispositif particulier et une source d'énergie.

{Résumé du chapitre 1 :  
À travers une histoire des media, depuis le télégraphe jusqu'à internet, Alessandro Ludovico analyse leurs relations et impacts avec les formats du livre et de la presse. Différentes inventions, imaginées, expérimentées ou adoptées, ont permis au texte imprimé d'évoluer, sans pour autant mettre en cause son existence – même si les prédicateurs de la fin du livre furent nombreux. Malgré des limites d'usage – majoritairement techniques –, l'hypertexte, internet et la mise en réseau des acteurs du texte sont peut-être les seuls bouleversements en mesure de nous faire envisager une modification profonde de l'imprimé.}


### Chapitre 2 : une histoire de l'édition alternative illustrant l'évolution de l'impression

#### 2.1. L'impression est vecteur de libération
[Pages 34-36] L'édition alternative consiste, pour Alessandro Ludovico, en une diffusion d'idées au sein d'"individus de même sensibilité", et ce mouvement a commencé avant Gutenberg, au moment de la gravure sur bois. La révolution de l'imprimerie ne fera que faciliter la diffusion de ces publications alternatives, majoritairement politiques jusqu'au vingtième siècle.

#### 2.2. L'utilisation de l'impression par les avant-gardes du XXe siècle
[Pages 36-39] Alessandro Ludovico présente plusieurs exemples de revues issues de mouvement artistiques, qui ont pu émerger grâce aux techniques d'impression et à la démocratisation de l'électricité, et qui ont expérimenté des formes graphiques du texte, préfigurant la publication assistée par ordinateur : du futurisme italien à El Lissitzky en passant par le mouvement Dada ou le surréalisme.

#### 2.3. La machine miméographique, ou duplicateur à pochoir, ouvre la voie à l'édition alternative
[Pages 40-42] Ce qui va faire suite aux avant-gardes, ce sont des appareils permettant une reproduction beaucoup plus accessible – coût moins important, facilité d'utilisation et de transport. La machine miméographique, ou miméographe ou miméo, en est un exemple emblématique : "Le miméographe se révélait l'outil d'impression clandestine idéal." Ce dispositif sera également utilisé pour les premiers fanzines, que ce soit pour des revues de science-fiction aux États-Unis, ou pour de la littérature subversive en URSS.

#### 2.4. Fluxus, des documents imprimés circulant au sein d'un "réseau éternel"
[Pages 42-44] Alessandro Ludovico aborde le mouvement Fluxus initié par George Maciunas, exemple de structure qui a produit des objets imprimés originaux, avec une distribution et une diffusion originales, allant jusqu'à "formuler explicitement le concept majeur de réseau".

#### 2.5. L'explosion de la presse underground, le *dripping* de couleurs, l'impression offset et le réseau (encore et toujours)
[Pages 45-47] De nouveaux exemples de revues alternatives : Village Voice, San Francisco Oracle, etc. Nous pouvons noter ici le passage de la sérigraphie à l'offset, devenu plus accessible ; des expérimentations graphiques plus originales ; ou la constitution de réseaux permettant une diffusion plus large et rapide notamment via un système de réimpression. Le *Whole Earth Catalog* est l'exemple de la publication qui a su utiliser ces nouvelles techniques de diffusion, autant pour sa production que pour sa distribution – via une interaction très forte entre les auteurs et les lecteurs.

#### 2.6. Photocopier le monde, se réapproprier la culture
[Pages 47-49] L'arrivée de la photocopie – ou xérographie, dont la technique avait été conceptualisée dès le dix-huitième siècle – au milieu des années 1970 permit une démocratisation immense de la publication, notamment via le mouvement des fanzines avec le punk, l'attitude *do it yourself* ou encore le Mail art. Nous pouvons retrouver ici, à chaque fois, la dimension de réseau – que ce soit pour la diffusion, la création de projets ou la recensement de contacts utiles à certains mouvements.

[Pages 50-52] Alessandro Ludovico présente plusieurs expérimentations à la fois sur le plan graphique ou sur l'utilisation du réseau pour alimenter ou diffuser des revues artistiques ou des idées politiques.

#### 2.7. La révolution digitale, l'apogée et le déclin des fanzines
[Page 52] "Mais l'événement décisif qui révolutionna l'impression fut l'explosion de la technologie numérique." Ce n'est non pas les moyens de reproduction, de distribution et de diffusion qui furent "décisifs", mais les moyens de conception et de production : l'arrivée des logiciels de Publication assistée par ordinateur (PAO). {Alessandro Ludovico ne s'attarde pas plus sur cela, mais c'est un point essentiel : disposer de moyens de production accessibles !}

[Page 52-53] Certains fanzines ont commencé à intégrer des versions numériques, avec la disquette puis le CD-ROM, des bonus audio ou vidéo aux supports imprimés : c'est l'arrivée du multimédia, qui trouve parfois un intérêt pour ce qu'il est en lui-même, et non uniquement comme *supplément*.

[Pages 54-56] Le point culminant des fanzines – en nombre, en visibilité et en reconnaissance –, est aussi le moment ou internet arrive, à la fin des années 1990.

#### 2.8. Media entremêlés, un regard sur le futur proche
[Page 56-57] "Que peut bien signifier que de créer une "publication alternative" dans ce nouvel environnement ?" Le mouvement semble dirigé dans le sens de ce qu'a entrepris Fluxus, c'est-à-dire une multiplicité de formats pour un même media. Ou dans le détournement de media existants comme la presse, profitant de la confiance que celle-ci suscite malgré son retard par rapport au numérique.

[Page 59] La contrefaçon militante ou détournement artistique des Yes Men doit être analysée également à la lumière des évolutions de la technologie numérique : "Cette mutation, on peut le présumer, ne sera ni simple ni sans détours." Cette analyse est l'objet du troisième chapitre.

{Résumé du chapitre 2 :  
Alessandro Ludovico explore les évolutions des techniques d'impression en analysant différentes initiatives de publication : activistes, politiques ou artistiques. Du miméographe à la photocopie en passant par l'offset, des courants comme le futurisme italien, le dadaïsme ou le fanzine punk ont su s'approprier les technologies de reproduction, et qui plus est expérimenter des formes graphiques inédites. L'édition alternative a été un véritable laboratoire de l'impression, jusqu'au *multimédia* – représenté par le CD-ROM – et au numérique – dont internet est la figure emblématique.}


### Chapitre 3 : la mutation du papier : papier matériel en des temps immatériels
[Page 60] Pour Alessandro Ludovico le papier est dans une phase de mutation et de transition, parce que le numérique prend progressivement des fonctions jusque-là uniquement portées par l'imprimé. Le papier n'est pas obsolète, mais son usage se modifie et vite, et également l'environnement qui l'entoure, mais parfois avec un certain retard.

#### 3.1. Le massacre en règle des journaux papiers
[Pages 61-63] Les transformations sont plus rapides que les *anciens* media ne le pensent, et c'est d'autant plus le cas dans la presse. Alessandro Ludovico prend quelques exemples, notamment avec la crise de 2008 qui a conduit certains journaux à repenser en profondeur leurs modèles. Ce que nous pouvons déduire en lisant Alessandro Ludovico, c'est que le numérique a influencé les usages, pour forcer un media comme la presse à modifier la façon dont l'information est conçue et transmise, appuyé par les acteurs du numérique qui n'attendent que ces transformations...

[Page 64-65] La presse doit se réinventer, repenser sa façon de diffuser de l'information.

#### 3.1.1. Le carnage des périodiques imprimés
[Page 65-66] Repenser la façon dont la presse peut se diffuser passe par l'observation des usages, par de l'expérimentation, et ainsi de suite. Le problème étant que les modèles commerciaux imaginés sont rarement en adéquation avec un univers numérique fondamentalement ouvert.

[Page 66-67] Parmi les magazines underground, ceux qui s'en sont le mieux sortis sont ceux qui ont réagit vite, même si peu ont perduré sur un temps long.

#### 3.2. Atomisation du contenu : la mise en œuvre du paradigme Apple/iTunes
[Page 67-68] La presse a permis de constater qu'avec le numérique l'"unité atomique" de l'information a changé : du journal entier (papier) à l'article (web). Et c'est ce qu'a très bien compris Apple/iTunes avec la musique, privilégiant ce qui est le plus populaire dans un magazine, plutôt que soutenir un ensemble cohérent.

[Page 68] En prolongeant cette logique de l'atomisation, nous pouvons imaginer deux processus : la personnalisation – choix d'articles pour composer son magazine, y compris les publicités qui s'adaptent ; et la rémunération des journalistes à l'article, suivant la même voie populiste d'Apple/iTunes, et remettant en cause le rôle de l'éditeur.

#### 3.2.1. Contenu automatisé : les informations suivent le lecteur
[Pages 68-70] La suite logique est donc l'automatisation : soit en terme de diffusion (quel article me sera proposé), soit en terme de création/écriture (rédaction par des algorithmes). Ces types d'automatisation peuvent aller encore plus loin en observant (pistant) et en analysant les comportements des lecteurs.

#### 3.3. L'actualité préventive : la lutte pour le marché de l'attention en ligne
[Page 71-72] Alessandro Ludovico avance le concept d'"actualité préventive", sorte de prémisse grossière d'une *vraie* actualité, précisée dans les minutes et heures qui suivent. "La transfiguration de l'information au sein de l'environnement numérique modifie la nature même de l'édition de diverses façons." Et il y a ici un véritable enjeu autour du design de l'information.

#### 3.4. L'espace, la dimension physique et la répétabilité de l'impression
[Page 74-75] Déjà en 2012 Alessandro Ludovico pose la question du sens de l'impression dans nos sociétés désormais numériques – des écrans connectés. La question de l'espace et de notre appréhension de celui-ci est essentielle {et c'est là une question de design}, pour une civilisation qui a mis plusieurs siècles à se constituer sur l'imprimé en rapport avec nos corps et nos sens. Il faut donc remettre de l'espace dans les environnements numériques.

[Page 75] La répétition est, selon Marshall McLuhan, le moyen d'appréhender l'information et que la presse (au sens de Gutenberg) a réalisé, alors que le numérique peine à disposer de normes aussi fortes que l'imprimé.

#### 3.4.1. Le geste éditorial
[Page 75-76] La pérennité du "document imprimé" est une autre caractéristique que le numérique ne parvient pas à supplanter, quand bien même le tactile permet de mieux appréhender le flux d'information. {Je suis un peu perdu dans l'articulation d'Alessandro Ludovico.}

[Pages 76-78] Analyse du geste éditorial : pour prendre conscience de la démarche éditoriale, plusieurs expérimentations visent à montrer comment des contenus peuvent être conçus, produits et diffusés.

#### 3.5. L'impression à la demande, l'équilibre des pouvoirs entre papier et pixel
[Pages 78-80] L'"objet imprimé" représente une expérience encore unique, mais pour Alessandro Ludovico l'équilibre avec le numérique dépend désormais d'une technologie d'impression qu'est l'impression à la demande – ou POD pour Print On Demand –, introduite via l'impression numérique, remplaçant l'offset pour les petits ou très petits tirages. La particularité de l'impression à la demande étant qu'un seul exemplaire peut être produit, tout en restant accessible. La POD peut comprendre aussi des services, comme le fait d'imprimer partout dans le monde, ou d'expédier l'exemplaire directement au client sans passer par l'éditeur.

[Page 80-81] Nous devons noter que la POD n'est possible que parce qu'internet existe, sans réseau il ne serait pas possible de relier les éditeurs et les "imprimeurs 2.0". Alessandro Ludovico cite l'Espresso Book Machine, comme le principe de la POD poussé à l'extrême : il s'agit de la même évolution que celle de la PAO, mais encore peu abordable.

#### 3.5.1. Édition à compte d'auteur, liberté d'expression et autogratification
[Page 81-82] "L'impression à la demande modifie une des règles fondamentales de l'édition – plus besoin d'investir un (petit) capital de départ." Si l'édition à compte d'auteur n'est pas un phénomène nouveau, il est fortement perturbé par ce nouveau procédé d'impression.

[Page 83] Mais c'est également l'occasion d'expérimenter de nouvelles formes, ou d'imprimer des contenus qui ne s'y prêtaient pas – tweets et profils de réseaux sociaux par exemple.

#### 3.5.2. Les frontières de la POD : customisation et open source
[Pages 84-86] Deux phénomènes sont permis par le biais de la POD, comme le numérique l'avait déjà fait plus tôt : l'accès à des contenus rares ou épuisés, et la personnalisation des contenus. Cette dernière peut être poussée jusqu'à la production d'exemplaires d'un même contenu tous différents.

[Page 87-88] Le principe de pouvoir produire des exemplaires uniques peut être poussé encore plus loin, comme avec Philip M. Parker qui a créé un logiciel capable d'assembler des contenus et de produire des livres, 200 000 au total, et tous différents.  
"En fait, le processus de la POD permet de mettre continuellement à jour le contenu – restituant ainsi au medium imprimé un des aspects définitionnels de l'édition en ligne." Les FLOSS Manuals en sont un exemple parfait, ajoutant en plus une dimension d'ouverture permettant de faire évoluer les contenus sans contrainte. Alessandro Ludovico cite également OSP qui a utilisé la plateforme FLOSS. La POD serait la photocopie d'aujourd'hui.

#### 3.6. Les stratégies des magazines indépendants : l'union fait la force
[Pages 88-89] Les magazines sont sans doute, avec les journaux, "les objets imprimés les plus menacés de disparition", l'enjeu étant de trouver des espaces de visibilité : rassemblements entre magazines, événements, nouvelles façons de donner accès à ces objets, curation, etc.

#### 3.7. Le piège de la production numérique automatisée
[Pages 91-93] Nous sommes proches d'une "automatisation totale" de la publication : pour l'instant nous avons l'ordinateur personnel, des logiciels de PAO, des imprimantes laser, des scanners. Il manque l'automatisation de la création graphique, incluant des générations aléatoires ou des procédés visant à créer des résultats imprévisibles.

#### 3.8. L'impression et le numérique se marient – et c'est alors que les vrais problèmes commencent
[Page 93] Les expérimentations dans le domaine des magazines sont nombreuses, à la fois sur le plan de la production ou de la vente, et cela est permis grâce au numérique. L'impression et le numérique sont désormais fortement liés, et pour Alessandro Ludovico "c'est alors que les vrais problèmes commencent".

{Résumé du chapitre 3 :  
Les journaux et les magazines ont été les "objets imprimés" qui ont le plus vivement ressentis les mutations de l'édition au contact du numérique. Avec l'influence du web et de ses usages, l'unité atomique est passée du journal à l'article, remettant en cause le rôle de l'éditeur via des possibilités de personnalisation des contenus. L'impression à la demande, photocopie du début du vingt-et-unième siècle, est la matérialisation des processus en œuvre dans l'édition en ligne, notamment avec la facilité et la rapidité de production et de diffusion. Si d'un côté les magazines recherchent de nouveaux moyens pour résister – notamment via des démarches développant leur visibilité ou par la réintroduction du geste éditorial –, l'"automatisation totale" de la publication est désormais accessible, et est le signe sinon la preuve d'un "mariage" entre l'impression et le numérique.}

### Chapitre 4 : la fin du papier : quelque chose peut-il réellement remplacer le document imprimé ?
[Page 94] Alessandro Ludovico reprend les propos d'Octave Uzanne pour aborder la question du livre numérique, les mêmes arguments de la surproduction et de l'universalité sont utilisés pour promouvoir l'ebook, alors même que celui-ci est en passe d'atteindre le même niveau de confort de lecture – à travers l'intuitivité et la convivialité.

[page 94-95] L'argument de la place gagnée avec l'ebook est en fait un parfait mécanisme de consommation sans fin, mais est-ce que le livre imprimé survivra-t-il à la façon du vinyle pour la musique ?

#### 4.1. Papier électronique, la base de l'édition électronique
[Pages 95-97] Le livre numérique est né d'une intention de partage, le numérique facilitant la diffusion d'un contenu – exemple avec Michael Hart et la Déclaration d'Indépendance des États-Unis. Cette intention s'est transformée en une nouvelle forme de lecture avec un dispositif de lecture numérique intuitif et convivial, la liseuse à encre électronique. Enfin, les différentes démarches de numérisation – Google Books en tête – permettent d'envisager une offre de livres numériques.

#### 4.1.1. La vision Bezos : la Kindle-isation du monde
[Page 97-98] Amazon a entrepris d'imaginer un écosystème dématérialisé pour le livre, avec Jeff Bezos comme figure imposant ce nouveau mode d'accès aux contenus.

#### 4.1.2. Résister au paradigme du e-book
[Page 98-99] Si certains lecteurs peuvent continuer de "résister" au livre numérique – ou "e-book" comme l'écrit Alessandro Ludovico – en tant que dispositif de lecture, c'est notamment en raison de ses limites, par exemple la nécessité de disposer d'une source de lumière {le livre est écrit en 2012, il n'y a pas encore de liseuse avec système de rétroéclairage}, le temps de chargement de l'encre électronique, le noir et blanc ou encore l'impossibilité de signer une liseuse.

[Page 99-100] En dehors de ces limites, il y a de sérieuses raisons de s'inquiéter, pour reprendre les mots d'Alessandro Ludovico : l'utilisateur devient dépendant de l'éditeur ou de la plateforme, pour des raisons de formats (AZW ou EPUB), d'accès aux fichiers ou de DRM, voire même de profilage (au moment de l'achat et après).

#### 4.1.3. Le paradigme iPad : éditer pour le iClient
[Pages 101-103] L'iPad représente à la fois un objet très complet, un ensemble de logiciels ou applications, et surtout la possibilité de vendre des contenus sous forme numérique. Cela avait été tenté par quelques projections ou expérimentations depuis les années 1980, sans succès.

[Page 104-105] L'enjeu n'est plus tant la reproduction numérique des objets imprimés, mais l'intégration d'autres media comme le son et la vidéo, ou de contenus hybrides.

[Page 105] Selon Alessandro Ludovico, les objets imprimés les plus *simples* et périmables – journaux, livres de poche, etc. – seront bientôt sous forme numérique, et les livres resteront imprimés pour être préservés, car leurs contenus auront une valeur...  
"Mais cela représnete aussi la lutte entre statique et dynamique, entre concentration et fascination, entre être absorbé en soi-même et bombardé d'informations venant de l'extérieur – la lutte, en d'autres terme, entre l'écran et l'imprimé."

#### 4.1.4. Le tournant décisif : un contenu électronique offrant la même apparence et les mêmes sensations que l'imprimé
[Page 106-107] L'enjeu des interfaces de lecture est déterminant, et contrairement à ce que nous aurions pu croire, c'est la copie de l'imprimé vers le numérique qui semble rassurer le plus les utilisateurs : du *skeuomorphisme* nous sommes désormais arrivés à un design très classique, emprunté au livre de poche par exemple. Ce principe a même été repris pour détourner le design des pages web via un "bookmarklet".

#### 4.1.5. Les journaux sur écran et sur papier
[Pages 108-110] Alessandro Ludovico aborde cette même question du design de texte avec la presse, en effet les journaux représentent un paradigme reconnaissable très facilement, inspirant des dispositifs d'impression de contenus numériques qui reprennent ce style, ou des interfaces numériques. L'objectif est de donner une certaine consistance aux contenus.

#### 4.2. "Reformatage" : un cycle mobile de lecture et d'écriture
[Pages 110-112] Pour Alessandro Ludovico, le "reformatage" est le fait de permettre une reconfiguration graphique du texte numérique en fonction du dispositif de lecture, et c'est ce que permet le format EPUB. L'éditeur devant "renoncer" à cette maîtrise de la forme. D'un côté perte d'une valeur ajoutée sur le texte, de l'autre gain d'une "universalité" – une lecture sur tout support.

#### 4.3. Fournir du contenu, des disques aux réseaux sans fil
[Pages 112-114] L'évolution des formats numériques des magazines est allée du CD-ROM au PDF, en passant par les sites web, dupliquant finalement et totalement les normes du "medium papier", développant toutefois un marché – les "kiosques en ligne" –, et développant un marché parallèle, pirate.

[Page 114-115] La prolifération des formats proches du PDF n'a finalement de sens que pour l'industrie, mais pas pour les utilisateurs.

[Page 115-116] La tendance de la numérisation de tout objet imprimé est permise par de nombreuses solutions, *hardware*, *software* ou des services dédiés, et il est même possible de numériser l'écriture manuscrite, pendant ce temps "l'information devient une denrée toujours moins chère et plus éphémère".

[Page 116-117] En plus d'être hyper disponible, le contenu vient désormais à nous, engendrant une surcharge d'informations et une fatigue visuelle dûe à la lecture sur écran.

#### 4.4. Passage au numérique : la dématérialisation des bibliothèques
[Page 117] Le numérique semble être une opportunité pour les bibliothèques : gain de place et gain financier {ce qui est totalement faux pour les questions de budget, le numérique étant en fait beaucoup plus cher, dû à un modèle très différent}.

[Pages 117-120] d'El Lissitzky à l'Optigraph de Agostino Ramelli en passant par Fremont Rider, nombreux ont été les théories prospectives autour des bibliothèques : amélioration de l'archivage, nouvelles modalités d'accès, réduction des supports de stockage, services de portage de contenus numériques ou d'impression à la demande, "prêt numérique", visites à distance, etc.

#### 4.5. L'imprimé devient un objet en édition limité
[Page 121-122] Comme nous pouvions le prévoir, le développement du livre numérique va engendrer une modification de la figure du livre imprimé, devenant un objet "rare" et "précieux", en y ajoutant des éléments difficilement reproduisibles mécaniquement. Alessandro Ludovico cite quelques exemples très originaux, à la limite du "kitsch", mais qui ont tous le point commun d'être physiquement appréhendables.  
{Je ne suis pas d'accord avec Alessandro Ludovico lorsqu'il dit que l'imprimé implique une lecture "tranquille" et posée, cela peut être également le cas avec le numérique !}

#### 4.5.1. Stratégies intermédiaires : utiliser des media numériques pour vendre des documents imprimés
[Pages 123-125] Alessandro Ludovico présente l'une des expérimentations de Cory Doctorow : ce dernier propose des formats numériques gratuits, mais imagine plusieurs formes de livres imprimés plus ou moins coûteux, et des façons originales d'y accéder. Le livre numérique peut devenir un produit d'appel gratuit pour déclencher des ventes des livres imprimés.

#### 4.6. L'impression postnumérique
[Pages 125-127] Alessandro Ludovico analyse le texte et l'imprimé dans leur relation d'autorité, et présente plusieurs projets artistiques qui tentent de montrer comment le numérique peut retrouver une figure d'autorité dès qu'il y a impression – même un mail imprimé et déposé sur des pare-brises. "Quoi qu'il en soit l'avenir du modèle éditorial/imprimé traditionnel [...] semble plus incertain que jamais."

{Le principal défaut de ce livre est de vouloir apposer des réflexions théoriques parfois floues sur des analyses de cas très pertinentes, ces dernières se suffiraient à elles-mêmes.}

#### 4.7. L'impression et le "en ligne" : amis ou ennemis ? Différences et similarités conceptuelles entre documents imprimés et blogs
[Pages 127-129] Cette partie est consacrée à une comparaison entre les blogs et les objets imprimés : coûts de production, de distribution et de diffusion ; accès ; esthétique ; fonctionnalité. {Il est particulièrement dommage de constater qu'Alessandro Ludovico use de clichés...}

[Page 129-130] L'objet imprimé véhicule plus d'informations en dehors du texte : toucher et odeur permettent de déterminer quel est le type de contenu, et le rendu graphique sera toujours le même contrairement au numérique. Tout cela pour dire que la mémorisation d'un texte imprimé est plus efficace, grâce à ses propriétés physiques. Il est plus complexe de se repérer dans un texte numérique, l'espace étant toujours le même, l'écran n'ayant pas d'épaisseur notamment.

[Page 130-131] Alessandro Ludovico prend l'exemple de Boing Boing, revue qui a su se transformer en numérique et même poser les normes du blogging.

[Page 132] Si le blog a "probablement" modifié les pratiques éditoriales en profondeur, l'imprimé conserve certaines spécificités – un "espace d'intimité entre auteur et lecteur".

#### 4.8. Lequel des deux est le plus respectueux de l'environnement : l'imprimé ou le numérique ?
[page 132-133] L'argument de l'écologie pour promouvoir le numérique n'est peut-être pas si pertinent : d'une part l'impression de contenus numériques est très massive – même si cela peut paraître étonnant –, et d'autre part le numérique consomme beaucoup d'énergie. Cela donne l'occasion à des artistes ou des expérimentateurs de tester des dispositifs pour limiter notre consommation de papier et d'encre, ou d'énergie.

[Page 134] Alessandro Ludovico va jusqu'à avancer que le papier est un bien meilleur choix à partir du moment où nous passons beaucoup de temps devant un écran. {Alessandro Ludovico manque de subtilité, ses opinions ou positionnements sont parfois binaires, alors que les cas qu'il analyse ne le sont pas autant.}

#### 4.9. La papier, c'est la chair, et l'écran le métal
[Page 134-135] Pour Alessandro Ludovico il faut dépasser cette "dualité" papier et numérique, pour trouver à la fois un support/media avec plus de possibilités que la papier, et à la fois plus *tangible* et humain que le numérique.  
{Nous pouvons noter ici qu'Alessandro Ludovico mélange le media – le numérique – et son support – fait de métal, de plastique et de verre –, mais il faudrait dépasser cela, puisque le media numérique pourrait prendre bien d'autres formes, voir même une absence de forme. Alessandro Ludovico est trop attaché aux supports physiques, alors que le numérique bouleverse potentiellement tout cela !}

{Résumé du chapitre 4 :  
Le livre numérique est né d'une intention de partage, intention qui a pu se transformer en une nouvelle forme de lecture grâce à un dispositif de lecture numérique intuitif et convivial : la liseuse à encre électronique. Cette nouvelle perspective, permise aussi par des sociétés qui usent de renforts marketing très puissants et dont les intentions ne sont pas forcément louables, implique des enjeux de design très profonds : forme du texte, interface de lecture, rapport entre le corps et la page, reproduction du volume du livre, etc. Le "reformatage" du texte, comme l'appelle Alessandro Ludovico, est une condition du développement et du déploiement de ces nouveaux usages, offerte par le format EPUB. Comme nous pouvions le prévoir le développement du livre numérique va engendrer une modification de la figure du livre imprimé, devenant un objet "rare" et "précieux", en y ajoutant par exemple des éléments difficilement reproduisibles mécaniquement, le numérique devenant un moyen de faire connaître des productions imprimées – un outil de diffusion. Alessandro Ludovico rappelle, à juste titre, que le dématérialisé à un coût écologique, et qu'il ne propose pas encore un confort de lecture similaire aux objets imprimés. Cette dualité, pourtant largement exposée par l'auteur de *Post-Digital Print*, doit, selon celui-ci, être dépassée, et c'est l'objet du chapitre suivant.  
}

### Chapitre 5 : archives distribuées : contenu papier appartenant au passé, contenu papier pour l'avenir
[Page 136] Alessandro Ludovico analyse internet de la même façon qu'Evgeny Morozov : il est trop concentré sur lui-même et pas assez sur ce qui l'a précédé, l'histoire passée est moins présente que celle qui le suit. La numérisation des documents imprimés est une forme de rattrapage de ce défaut, mais peut-on parler pour autant d'archivage ?

#### 5.1. Les "géants d'Internet" et leur façon d'envisager l'"archivage" des documents imprimés
[Page 137] Plus que pour faire gagner internet en *pertinence*, la numérisation représente une manne financière pour les géants du net. Exemple avec Amazon et la possibilité de rechercher dans un livre, sans pour autant accéder à tout le contenu ou même à la version numérique du livre, pour l'instant.

[Page 138-139] L'entreprise de numérisation massive de Google est une autre stratégie, tant en terme de contenus sur lesquels poser de la publicité, qu'en terme d'image avec cette action qui semble être de l'archivage. Les opérations de numérisation peuvent être en partie financées, avec des participations de bibliothèques par exemple, et cela est un cheval de Troie pour les éditeurs contemporains – faire venir leur livre sous droits sur Google Books.  
"À terme, il sera possible d'effectuer une recherche à travers cette collection centralisée d'informations et de culture produite sur plusieurs générations à travers l'interface propriétaire de Google. Le contenu pertinent à l'échelle mondiale sera vendu à l'échelle mondiale (par le biais de publicités), mais géré et contrôlé de façon centralisée."

[Page 139-140] Alessandro Ludovico soulève deux problèmes liés à Google Books : le fait que ce soit une entreprise commerciale qui numérise et possède ces numérisations, et Google favorise les cultures les plus "populaires" laissant de côté un pan considérable de la culture, quand bien même minoritaire.

[Pages 140-143] Un autre modèle totalement différent permet d'envisager une alternative à Google Books : l'Internet Archive et ses différents projets, de la conservation physique à la numérisation en passant par l'archivage de contenus nativement numériques. Nous pouvons aussi observer des initiatives similaires – mais d'envergures plus réduites – du côté des bibliothèques.

[Pages 143-145] Mais peut-on parler d'"archivage" ? Le numérique a un défaut de pérennité par rapport au papier ou à des supports physiques comme l'argile. {Je suis en désaccord avec Alessandro Ludovico, le numérique permet d'envisager des archives dynamiques, mouvantes, reproduisibles, etc. Même si nous n'y parvenons pas encore, nous savons que cela est possible.} Alessandro Ludovico préfère parler d'accès que d'archive.  
"En fait, l'utopie d'une bibliothèque des bibliothèques ressemble de manière inquiétante à la vision utopique propagée par les "géants d'Internet" mentionnés ci-avant. Est-il possible (ou même souhaitable) d'organiser de façon définitive la totalité du savoir humaine (dans sa forme la plus traditionnelle et la plus étendue : le mot imprimé) sous une forme ordonnée, dynamique et indexée ?"  
Peut-être sommes-nous en train de parvenir à la situation de la bibliothèque de Babel : nous avons tous les contenus mais nous ne parvenons pas à nous y retrouver.

#### 5.2. Conserver les revues indépendantes, une lutte controversée
[Pages 146-148] Alessandro Ludovico fait une analyse de ce désir d'archivage à partir de Marshall McLuhan : "Le papier a toujours été perçu comme une extension stable de la mémoire humaine, "indépendant de toute plateforme" bien que limité d'un point de vue physique." Les techniques d'archivage, de conservation, de "résurrection" ou de réédition sont "significatives" : que ce soit les techniques de numérisation, l'OCR ou le format PDF.  
Il y a autant l'archivage *pur* que l'intention de créer des versions plus exploitables que le papier à travers ces différentes initiatives, et il faudrait peut-être que les éditeurs se concentrent sur cette deuxième option, bien plus riche.

#### 5.3. Archives distribuées, le modèle d'archivage peer to peer
[Page 148-149] Mais comment agréger les très nombreuses entreprises individuelles d'archivage, souvent très isolées mais néanmoins riches et utiles ? Le rôle des collectionneurs est ici décisif.

[Page 149-150] L'initiative *ouverte* de communautés utilisant des méthodes et des logiciels open source permet d'envisager une "accélération" de la numérisation du patrimoine écrit, constituant des bases de données "dynamiques", et une "mémoire collective" basée sur une "modèle ascendant" plutôt que "descendant" – comme celle des institutions.

{Ces initiatives ouvertes sont une preuve que la constitution d'une collection – de livres, de contenus, ou d'outils – est beaucoup plus puissante ou accessible que lorsque tout cela est fermé et verrouillé. Moins de moyens centralisés, mais plus de possibilités distribuées.}

#### 5.3.1. La nécessité d'une forma d'archive "fluide"
[Page 150-151] Wikipédia ou les archives du New York Times représentent cet exemple type d'archives ouvertes, accessibles et gratuites dans une certaine mesure.

[Page 151] Le défaut des "petits éditeurs" – puisqu'Alessandro Ludovico revient systématiquement à l'échelle des publications alternatives – est d'avoir considéré le web comme un vecteur de leur démarche commerciale, et non sous l'angle de la diffusion culturelle, de la visibilité de leurs activités.

[Page 152] Mais de quoi le numérique est-il le nom dans le cas de l'archivage ? La question de l'accès à des contenus ne peut se limiter à une recherche aussi perfectionnée soit-elle, ou

#### 5.4. L'art de l'archive : créer une version papier de la base de données numérique
[Page 153-154] Aborder l'archive d'un point de vue artistique permet de prendre du recul sur l'acte même d'archiver : donner une spatialité à des ressources numériques nous donnent du recul et une autre vision.

[Pages 155-157] Alessandro Ludovico présente un projet auquel il a participé, "Amazon Noir" : il s'agit de détourner la possibilité de feuilleter quelques pages de presque tous les livres vendus par Amazon, pour reconstituer des livres numériques et les imprimer. "[...] nous étions des voleurs de mémoire (au sens de McLuhan), volant pour le droit de se souvenir, le droit de fabriquer notre propre mémoire physique librement et de façon indépendante."

#### 5.5. Les scrapbooks, l'approche populaire de l'archivage comme nouvelle méthodologie
[Page 157-158] "Le *scrapbooking* est une méthode consistant à conserver son histoire personnelle en collectant des coupres de presse imprimées, des photos, des souvenirs précieux et d'autres types d'artefacts dans des livres vierges ou des carnets." Une façon de constituer une archive éditée, sans le sens où il y a un vrai choix dans la sélection des contenus conservés.  
"On pourrait aussi considérer ce phénomène comme une première version (analogique) de la tendance actuelle à l'"édition personnelle" rendue possible par Internet [...]"

[Page 159] Les "scrapbooks numériques" seraient alors une forme plus pérenne des contenus numériques, plus pérenne que les marque-pages de sites web, et qui pourraient par ailleurs être imprimés via la POD. Nous pourrions alors disposer d'un puissant moyen de constituer des archives personnelles ou des archives de projets particuliers, puissant outil pour comprendre comme une personne ou un projet s'est constituée.

{Résumé chapitre 5 :  
De la même façon que le livre numérique a été une opportunité commerciale, la numérisation rétrospective de l'imprimé portée par des sociétés privées représente une manne financière immense, il ne s'agit donc pas que d'une entreprise patrimoniale. Entre le placement de publicités sur une masse de contenus considérable, et le phénomène d'attraction pour les ouvrages sous droits, peu d'initiatives permettent de concurrencer le projet Google Books, et d'envisager un véritable archivage ouvert aux cultures et disponible librement pour tous. En dehors de ces tentatives d'archivage pur, il y a également l'intention de créer des versions plus exploitables que le papier, avec les techniques de reconnaissance de caractères (OCR) ou le format PDF. Pour Alessandro Ludovico le défaut des "petits éditeurs" est d'avoir considéré le web comme un vecteur de leur démarche commerciale, et non comme l'occasion de repenser une diffusion culturelle ou une nouvelle démarche de visibilité de leurs activités, deux options bien plus riches. Ce cinquième chapitre se clôture sur le concept de *scrapbooking* qui, à l'ère numérique, est la possibilité de constituer des archives éditorialisées et pérennes. Un élément clé permet d'envisager cela : le réseau.  
}


### Chapitre 6 : le réseau transforme la culture et l'édition
[Page 160] En dehors de la littérature les livres sont des objets conceptuellement connectés entre eux, via des citations ou des bibliographies par exemple. Ainsi, l'hypertexte ne fait que faciliter la profusion des liens et la dimension d'un réseau déjà constitué. Le numérique vient renforcer cette nécessité d'une mise en réseau entre éditeurs.

#### 6.1. Le magazine comme nœud de réseau
[Page 160-161] Pour Alessandro Ludovico le réseau est une dimension réelle et incontournable d'un milieu *intellectuel* dont les revues ou magazines représentent des nœuds visibles et tangibles. Le numérique permet de dépasser la dimension asynchrone de l'imprimé pour se rapprocher de l'immédiateté des relations humaines.

[Page 162] "En d'autres termes, l'essor d'Internet a profondément et définitivement bouleversé l'"écosystème qualité"."

#### 6.1.1. Le réseau, c'est de la distribution, et la distribution profite largement au réseau
[Page 162-163] La distribution est une question centrale dans une activité de publication, et elle dépend du contexte et de l'environnement – notamment en terme de conditions politiques ou d'infrastructures. La "réputation" y joue un rôle important, permettant des connexions autrement impossibles.  
"Et si Internet nous a appris quelque chose, c'est bien que la puissance combinée des nœuds individuels (et se renforçant mutuellement) est illimitée."

#### 6.1.2. Le réseau comme infrastructure : agences, syndication/mise en commun/partage et annuaires
[Pages 163-165] Ce qu'explique Alessandro Ludovico à grand renfort d'exemples de distributions alternatives – par ailleurs passionnants –, c'est que le fait d'appartenir à un réseau permet d'exister, sans cela toute initiative purement individuelle est vouée à l'échec.

#### 6.1.3. Le réseau comme moyen de soutien politique et d'entreprise durable : le projet Punti Rossi ("Points Rouges")
[Page 165-166] La mise en réseau peut aller plus loin que la simple distribution {ou diffusion}, avec la constitution de lieux de distribution ou de vente par exemple, ajoutant une "fonction sociale" aux activités de publication.

#### 6.2. Mieux vaut collaborer que se faire concurrence : le réseau Mag.net
[Pages 166-168] Alessandro Ludovico prend l'exemple du réseau Mag.net comme échec relatif de cette constitution d'un réseau, l'expérience aura au moins permis aux différents acteurs d'échanger sur des pratiques, des outils, etc.

#### 6.3. Le réseau comme expérience à grande échelle : le projet *Documenta 12 Magazines*
[Pages 168-170] Autre exemple (d'échec) de réseau de magazines.

#### 6.4. Réseaux de soutien extérieurs – aider les gestes éditoriaux à (grande) distance
[Page 170-171] "L'édition n'existe pas sans la distribution." Réussir à distribuer des contenus dans "des endroits reculés" est la différence la plus importante entre l'édition "commerciale" et l'édition "indépendante". L'édition alternative peut construire des modes de distribution eux aussi alternatifs, comme le fait de profiter d'espaces libres dans les bagages, ou une distribution aléatoire.

[Page 172] Le détournement peut également être une stratégie de distribution, profitant de l'apparente banalité d'un ouvrage ou d'un magazine pour utiliser des canaux classiques.

[Page 172-173] Dans certains cas un réseau peut être comparer à un "organisme complexe" pour Alessandro Ludovico : plusieurs "nœuds" sont disponibles en fonction de la configuration de l'environnement.

#### 6.5. Le réseau : ici commence l'avenir
[Page 173-] Internet est plus qu'un nouveau canal de diffusion, c'est un aussi une "infrastructure" et un moyen d'imaginer de nouvelles modalités de distribution.  
Le réseau est le meilleur moyen d'appréhender la question de la distribution pour une activité de distribution.  
"En d'autres termes, la meilleure manière pour les éditeurs de remplir leur rôle spécifique au sein de n'importe quel réseau culturel (ou même du réseau entier de la culture humaine) est de se connecter de manière ouverte et libre à d'autres "nœuds" appartenant au même réseau. Voilà comment nous pouvons produire du sens, et apporter de fait un peu de lumière, au sein de ce réseau mondial dont nous faisons désormais tous partie."

{Résumé du chapitre 6 :  
En dehors de la littérature les livres sont des objets conceptuellement connectés entre eux, via des citations ou des bibliographies par exemple. Ainsi l'hypertexte ne fait que faciliter la profusion des liens et augmenter la dimension d'un réseau déjà constitué. Le numérique vient renforcer cette nécessité d'une mise en réseau entre éditeurs, nœuds d'une structure distribuée. La distribution est justement une question centrale dans une activité de publication, "Et si Internet nous a appris quelque chose, c'est bien que la puissance combinée des nœuds individuels (et se renforçant mutuellement) est illimitée" (page 163). Internet est plus qu'un nouveau canal de diffusion – comme l'ont été le télégraphe, la radio ou la télévision –, c'est aussi une "infrastructure" et un moyen d'imaginer de nouvelles modalités de distribution.  
}

### Conclusion : impression postnumérique : un scénario pour l'avenir
[Page 175] Pour Alessandro Ludovico il ne faut pas considérer l'évolution des media comme unilatérale, le mouvement n'est pas uniquement de l'analogique vers le numérique, le medium est un "vecteur" qui peut évoluer au fur et à mesure (de la VHS au DVD). Le cas de l'impression est "très différent", puisque la page imprimée est autant le vecteur que l'interface, "le système d'affichage" : passer au numérique impose une modification en profondeur de nos usages et de notre rapport avec le livre.

[Page 175-176] Nous pouvons faire malgré tout le constat que le passage de l'imprimé au numérique est de plus en plus massif, pour deux raisons principales : d'une part les interfaces et usages évoluent, d'autre part la mise en réseau permet cette large diffusion. Et la mise en réseau devient nécessité de connectivité, avec la contrainte d'une interopérabilité.

[Page 176] Pendant ce temps, les documents imprimés deviennent "précieux", mais devront encore se "réinventer".

[Page 176-177] Pendant ce temps, du côté des initiatives alternatives ou underground, la tendance est à la création de réseaux ne nécessitant pas de connexion numérique.

[Page 177] Alessandro Ludovico défend le fait qu'il y a quelque chose à dépasser dans le modèle actuel, c'est ce qui a pu être observé avec "l'activisme imprimé" : développement de nouvelles formes artistiques, incidences sur l'environnement social et politique, etc. D'un côté l'imprimé peut se servir du numérique pour évoluer ("impression à distance", "distribution en temps réel"), et de l'autre le numérique pourrait mieux intégrer l'imprimé, ou tout du moins ne pas le dénigrer. Le résultat pourrait être des pratiques personnelles d'impression plus fortes.

[Page 178] L'hybridation peut encore connaître des améliorations, et malheureusement la plupart des initiatives sont majoritairement industrielles. {Mais les espérances d'Alessandro Ludovico sont probablement assez éloignées des usages, et cela se vérifier quelques années plus tard, certaines de ces projections ne s'étant pas du tout vérifiées.}

[Page 178-179] L'"impression postnumérique" serait donc la "combinaison" de plusieurs éléments, depuis le modèle de la souscription via internet jusqu'à "l'hybridation de l'imprimé et du numérique" {mais laquelle ?...} en passant par l'utilisation d'un ordinateur pour publier le contenu (même sous forme imprimée).

[Page 179] Alessandro Ludovico pense que l'hybridation sera atteinte lorsque d'un côté nous ne ferons plus de distinction ou de niveau de numérique, et de l'autre nous aurons la possibilité de mixer des contenus provenant de diverses sources – "de nouvelles pratiques processuelles".

[Page 179-180] Dans le dernier paragraphe de son livre, Alessandro Ludovico imagine que l'évolution de l'édition est l'hybridation, et cela est possible par le dépassement de toute "affiliation idéologique" liée aux medias imprimés ou numériques. Il s'agit donc bien d'un dépassement à réaliser.

{Résumé conclusion :  
Dans la conclusion de son ouvrage, Alessandro Ludovico revient sur l'évolution des medias, et il note que le cas de l'impression est "très différent" de la musique ou de la vidéo, en effet la page imprimée est autant le vecteur que l'interface, "le système d'affichage" : passer au numérique impose une modification en profondeur de nos usages et de notre rapport avec le livre. L'hybridation serait le moyen de dépasser ce qui se limite à des nouveaux modes de lecture ou de consommation, d'atteindre un niveau "processuel" (page 177) : développement de nouvelles formes artistiques, incidences sur l'environnement social et politique, etc. L'"impression postnumérique" (page 178) serait donc la combinaison de plusieurs éléments, depuis le modèle de la souscription via internet jusqu'à "l'hybridation de l'imprimé et du numérique" en passant par l'utilisation d'un ordinateur pour concevoir et produire les objets imprimés et numériques. *Post-Digital Print* se referme sur la condition de cette hybridation : pour Alessandro Ludovico cette évolution de l'édition est possible par le dépassement de toute "affiliation idéologique" liée aux medias imprimés ou numériques.  
}

[^post-digital-print-01]: LUDOVICO, Alessandro. *Post-Digital Print : la mutation de l'édition depuis 1894*. Paris : B42, 2016. 206 p.
[^ocr]: Reconnaissance optique des caractères, ou OCR pour Optic Caracter Recognize.
[^flatland]: Epilogue. FLATLAND - Ep1. *Kickstarter* [en ligne]. 14 avril 2015. [Consulté le 8 août 2017]. Disponible à l'adresse : https://www.kickstarter.com/projects/epiloguepress/ep1-flatland
[^adebiaye]: FAUCHIÉ, Antoine. Zeitgeist : le fond, la forme, et le reste. *quaternum.net* [en ligne]. 29 mars 2013. [Consulté le 8 août 2017]. Disponible à l'adresse : https://www.quaternum.net/2013/03/29/zeitgeist/
[^le-train]: Le train de 13h37. Design d'icônes et La dette technique : de l'ebook au papier. *Kickstarter* [en ligne]. 28 septembre 2015. [Consulté le 8 août 2017]. Disponible à l'adresse : https://www.kickstarter.com/projects/t13h37/design-dicones-et-la-dette-technique-de-lebook-au
[^brad-frost]: FROST, Brad. *Atomic Design* [en ligne]. 2016. [Consulté le 8 août 2017]. Disponible à l'adresse : http://atomicdesign.bradfrost.com
[^from-to]: FAUCHIÉ, Antoine. From—To : une publication contemporaine. *quaternum.net* [en ligne]. 7 juillet 2015. [Consulté le 8 août 2017]. Disponible à l'adresse : https://www.quaternum.net/2015/07/07/from-to/
[^laumonier]: LAUMONIER, Alexandre. *6|5*. Bruxelles : Zones sensibles, 2014.
