---
layout: lectures
title: "L'écriture sans écriture de Kenneth Goldsmith"
date: "2019-01-21T10:00:00"
comments: true
published: true
description: "Lecture en cours du livre de Kenneth Goldsmith, L'écriture sans écriture: du langage à l'âge numérique."
categories:
- lectures
key_book: goldsmith_lecriture_2018
lecture_status: en cours
slug: l-ecriture-sans-ecriture-de-kenneth-goldsmith
---
Lecture en cours du livre de Kenneth Goldsmith, _L'écriture sans écriture: du langage à l'âge numérique_.

<!-- more -->

## Citations et notes
En feuilletant _L'écriture sans écriture_ de Kenneth Goldsmith je découvre une écriture par composition de textes relativement indépendants, comme si le travail de rédaction avait volontairement fragmenté, sans pour autant qu'il y ait une impression de manque d'unité à la lecture.
La méthode d'écriture, utilisée dans bien d'autres ouvrages, semble être par parties plus manipulables qu'un livre entier.
Quelles sont les limites de ce procédé ?
Peut-être que ce n'est pas si original que cela...

"De l'apparition de Napster ... une authenticité et une unité sans changement." (p. 15)

"Même lorsque l'on fait quelque chose d'aussi mécanique que recopier quelques pages, c'est nous-mêmes que nous exprimons de façons différentes." (p. 16)

"Alors que ce langage [le code] a d'abord pour tâche de passer d'un état ... désormais à l'écriture." (p. 30)

"Avec les médias numériques nous voilà entrés de plein pied dans un monde de manipulation textuelle, qui n'est plus du tout l'apanage exclusif de 'l'écriture' et de la 'littérature'." (p. 31)

"Avant le langage numérique, les mots qu'on rencontrait étaient déjà quasi emprisonnés dans une page." (p. 33)

(p. 40) Le concept de flux est rapproché de celui de liquidité du texte, il y a là une analogie liquide/aquatique.

"Dans l'écriture dans écriture, on crée de nouvelles significations en reconfigurant des textes préexistants." (p. 45)

(p. 79-80) Différentes formes de livres existent non pas uniquement par le support (imprimé ou web par exemple) mais par le contexte : quelles informations sont disponibles _autour_ du livre ?
Est-ce que dans les deux cas (imprimé et web) le texte _existe_ ?

(p. 85) Si l'œuvre est dénudée ou rhabillée, est-elle encore "l'œuvre" ?

(p. 85-86) Le numérique ouvrirait-il une perspective de _work in progress_ pour la littérature ?
Non, il la met en lumière plus clairement !
