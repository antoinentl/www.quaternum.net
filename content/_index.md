---
title: Carnet web d'Antoine
---
Je suis [Antoine](/a-propos), quaternum.net est mon carnet web.
Je publie régulièrement des [articles](/carnet) sur les questions de publication numérique, ainsi qu'une [veille](/flux) irrégulière.
Depuis 2019 ce carnet est aussi un [carnet de recherche](/phd) dans le cadre d'une thèse à l'Université de Montréal.

Ce site est actuellement en [refonte](/2021/07/21/refonte-chantier-ouvert/), c'est un chantier à ciel ouvert.