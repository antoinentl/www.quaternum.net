---
layout: post
title: "Fabriques de publication : Zettlr"
date: "2020-08-28T23:00:00"
comments: true
published: true
description: "Neuvième analyse d'une série sur les fabriques de publication : Zettlr."
categories:
- carnet
- phd
classement:
- phd
status: brouillon
bibfile: data/analyses-zettlr.json
---
Neuvième analyse d'une série sur les fabriques de publication&nbsp;: Zettlr.

<!-- more -->

[Introduction et liste des fabriques](/fabriques)

Zettlr est un éditeur de texte, un logiciel d'écriture pensé pour les documents scientifiques.

## Description
À la fois éditeur de texte, gestionnaire de fichiers et convertisseur, [Zettlr](https://zettlr.com/) est un logiciel [libre](https://github.com/Zettlr/Zettlr#license) qui vise à remplacer tout traitement de texte en situation de rédaction académique.
Basé sur des composants déjà présentés et étudiés ici, Zettlr est une application développée avec Electron – le _framework_ qui permet de créer des logiciels avec les outils du web.
Zettlr, créé et maintenu par Hendrik Erz, est disponible pour les trois systèmes d'exploitation principaux, c'est-à-dire Linux, MacOS et Windows.

Zettlr se présente avec l'intitulé suivant&nbsp;: "The Power of Words. Right at your Fingertips." (Le pouvoir des mots. Au bout de vos doigts.)
Et effectivement ses fonctionnalités permettent une liberté et une puissance pour l'écriture&nbsp;: la structuration du texte avec le langage de balisage léger Markdown, pour une séparation et une distinction claires du fond et de la forme&nbsp;; la gestion des citations bibliographiques et des bibliographies à partir d'un fichier BibTeX ou CSL JSON&nbsp;; une organisation des fichiers facilitée par des liens _entre_ des dossiers ou des fichiers&nbsp;; une recherche dans les fichiers&nbsp;; la génération de différents formats d'export, du PDF paginé à la présentation HTML avec Reveal.js, en passant par du DOC ou de l'ORG&nbsp;; la création de schémas avec Marmaid&nbsp;; une gestion du temps avec le système Podomoro&nbsp;; etc.
Zettlr est un outil qui répond à beaucoup d'exigences de la rédaction scientifique, tout en permettant une édition loin des mauvaises pratiques induites par des traitements de texte classique comme Microsoft Word ou LibreOffice Writer {{< cite "dehut_en_2018" >}}.

Zettlr est un logiciel qui offre une grande capacité de personnalisation, notamment sur les aspects graphiques de l'interface, le choix des fonctions affichées, ou encore sur les possibilités d'export.
C'est loin d'être un détail, surtout pour des personnes amenées à passer **beaucoup** de temps sur un tel outil d'écriture.

Zettlr intègre ou utilise des programmes ou langages comme Pandoc, CSL, YAML, Marmaid, LaTeX, Zotero, BetterBibTeX ou Reveal.js.
Ainsi le balisage des références bibliographiques respecte les pratiques de Pandoc, et il est possible d'intégrer des commandes LaTeX.
Pour peu que vous soyez coutumier ou coutumière d'un langage de balisage léger et d'un processeur pour générer des formats de sortie, Zettlr vous apportera quelques fonctionnalités supplémentaires mais ne changera pas radicalement votre façon de travailler.

Zettlr est un outil d'écriture individuel – loin d'éditeurs de texte collaboratifs en ligne –, il offre toutefois la possibilité de travailler facilement avec d'autres personnes qui utilisent ce logiciel, ou qui ont l'habitude de manipuler des fichiers Markdown avec Pandoc par exemple.
S'agit-il pour autant d'un outil d'_édition_&nbsp;?
Nous verrons un peu plus loin les limites de cette application.

Enfin Zettlr dispose d'[une documentation très fournie](https://docs.zettlr.com/), disponible en plusieurs langues, ainsi que de vidéos explicatives.
Tout est fait pour que les personnes qui avaient encore des doutes utilisent Zettlr.
Je reviendrai plus loin sur cette stratégie.

## Histoire
Sur son site web Zettlr dispose d'[une page sur sa propre histoire](https://zettlr.com/history), c'est suffisamment rare pour être souligné.
Le projet a démarré en 2017 lorsque [Hendrik Erz](https://twitter.com/sahiralsaid) a fait le constat suivant&nbsp;: Markdown n'est pas _facilement_ utilisable dans certains cas d'usages avancés, comme l'écriture scientifique ou le journalisme par exemple.
Les éditeurs de texte basés sur Markdown ne proposent pas de fonctionnalités abouties pour l'export, pas de gestion de références bibliographiques, ou encore pas d'organisation avancée de fichiers.
Hendrik Erz a décidé de rendre accessible un certain nombre de fonctionnalités permises par des programmes jusque-là réservés à des personnes à l'aise avec un terminal – et avec des programmes comme Pandoc, LaTeX, CSL.
D'une certaine façon, Hendrik Erz a concrétisé ce que beaucoup d'autres personnes imaginaient&nbsp;: assembler des composants dans une application complète.

Depuis 2019 Zettlr évolue vite, notamment via la constitution d'une communauté autour de moyens adéquats&nbsp;: le partage du code [sur GitHub](https://github.com/Zettlr/Zettlr), [un site web dédié](https://zettlr.com/), [un compte Twitter actif et suivi](https://twitter.com/zettlr), [un forum](https://forum.zettlr.com/) ainsi qu'[une section Reddit](https://www.reddit.com/r/zettlr).
Hendrik Erz anime, si l'on peut dire, cette petite communauté, ou tout du moins lui donne les ressources nécessaires pour qu'elle existe.

## Spécimen
Comme souvent avec les fabriques présentées ici, le plus simple pour comprendre ce que fait Zettlr, c'est d'utiliser ce logiciel.
[Les vidéos](https://www.youtube.com/c/Zettlr/videos) sont également un bon moyen de _voir_ comment Zettlr fonctionne.
La gestion de fichiers peut perturber au début, mais il faut admettre qu'un gros travail sur l'interface a été réalisé.

## Critique
Il y a beaucoup à dire sur Zettlr, notamment parce qu'il s'agit d'une solution très aboutie parmi d'autres qui utilisent des programmes similaires.
La comparaison avec [Stylo](https://stylo.huma-num.fr) est pertinente, il est très intéressant de comprendre comment et pourquoi les deux éditeurs de texte se distinguent.

Tout d'abord il faut noter le choix d'un logiciel, et non d'un programme ou d'une application web.
Les programmes, présentés dans cette série sur les fabriques de publication, comme Pandoc ou Jekyll, imposent souvent l'utilisation d'un terminal ou d'un éditeur de code.
Ils demandent une gymnastique informatique à laquelle tous les utilisateurs et toutes les utilisatrices ne sont pas forcément habituées.
Une application web, elle, a l'avantage d'éviter l'installation d'un logiciel, seul un navigateur est nécessaire, quelque soit la machine utilisée.
L'application web réduit fortement les temps et les coûts de développement, mais difficile de se passer d'une connexion internet pour son usage (même si désormais les [Progressive Web Applications](https://blog.ionicframework.com/what-is-a-progressive-web-app/) sont de plus en plus fréquentes).
Zettlr est pensé pour un usage individuel, et qui plus est intensif – nous sommes bien dans une situation de travail en milieu académique –, le logiciel installable sur un ordinateur individuel répond à cette contrainte.
La limite ici est la possibilité de travailler à plusieurs sur un document, que ce soit de façon synchrone ou asynchrone – quoique entre des personnes habitées de Markdown et de Pandoc c'est parfait.

Le choix d'[Electron](https://fr.wikipedia.org/wiki/Electron_(framework)) pour développer Zettlr s'explique facilement&nbsp;: il s'agit d'un _framework_ souple et multi-OS.
C'est-à-dire qu'avec Electron un seul développement permet de porter facilement le logiciel pour plusieurs systèmes d'exploitation ou OS (Linux, MacOS et Windows).
Dans le développement informatique c'est d'habitude ce qui pose le plus de problèmes, cette capacité à rendre un outil disponible pour plusieurs OS.
Le choix d'Electron implique toutefois un problème de performance, ce _framework_ rendant particulièrement gourmand en ressources les logiciels développés – les utilisatrices et utilisateurs d'[Atom (l'IDE)](https://atom.io/) connaissent bien ces ralentissements.

Depuis plus d'un an le succès de Zettlr grandit au sein de la communauté scientifique, et plus spécifiquement auprès de deux groupes distincts.
Tout d'abord auprès des personnes habituées de LaTeX ou Markdown, et ensuite auprès d'autres plus classiquement utilisatrices de traitements de texte.
Selon moi ce succès est tout à fait justifié, un effort considérable a été fait de la part d'Hendrik Erz et de la communauté pour permettre une démocratisation de cet outil – et avec lui Markdown ou Pandoc –, grâce notamment à la traduction de l'interface et de la documentation en plusieurs langues (y compris en français).
Attention, la mesure de ce supposé succès est une évaluation sans fondement scientifique sérieux, il s'agit simplement de ce que je vois passer les réseaux, et aussi de ce que Zettlr communique.

D'abord initiative relativement timide, Zettlr prend de l'ampleur, et cela s'explique assez facilement.
Le développeur principal, Hendrik Erz, travaillait jusqu'à maintenant bénévolement.
Il a mis en place depuis peu un système de don via Patreon, permettant de récupérer un peu d'argent afin de financer son temps de travail sur le logiciel.
Le nombre de téléchargements de Zettlr augmente régulièrement.

Zettlr semble être une fabrique exemplaire (par rapport aux critères exposés ici, qui s'affinent d'analyse en analyses)&nbsp;: système modulaire, utilisation de composants ayant fait leur preuve, ouverture du code, interopérabilité, originalité.
Pourtant je ne suis pas à l'aise avec cette solution _tout-en-un_, voici un exemple pour expliquer mon propos&nbsp;: il est possible de personnaliser l'export PDF, ou plus spécifiquement les détails de la commande Pandoc et le modèle LaTeX, mais cela s'applique pour le logiciel dans son ensemble.
Si vous devez gérer des exports multiples (diffèrents modèles selon les travaux, et différents modèles selon les revues), alors il devient vite ennuyeux d'aller modifier à chaque fois ces paramètres.
C'est une dimension modulaire qui manque à l'outil, pouvoir séparer les modèles formels des artéfacts produits, il s'agit de l'étape qui suit celle de la séparation du fond et de la forme.

## Vers d'autres fabriques
Zettlr est un éditeur de texte basé sur Markdown, on peut donc considérer Zettlr comme une version aboutie de nombreux outils existants, ces derniers sont peu satisfaisants pour certaines fonctionnalités (comme des exports PDF paginés, la gestion de citations bibliographiques ou l'organisation de fichiers).

Zettlr et [Stylo](https://stylo.huma-num.fr) ont beaucoup de points communs, les objectifs (remplacer l'usage d'un traitement de texte par un système efficace et accessible) et les composants (Markdown, Pandoc, LaTeX) sont très similaires, et les cas d'usage proches (même si Stylo est pensé pour l'édition scientifique là où Zettlr peut être utilisé dans bien d'autres cas).
En terme d'utilisation Zettlr est un logiciel installable sur un ordinateur alors que Stylo est une application web.
Et surtout Stylo [est porté par un laboratoire de recherche scientifique](https://ecrituresnumeriques.ca/fr/Activites/Projets/2016/1/14/Stylo), là où Zettlr est une initiative personnelle (qui dispose d'une communauté active).
Enfin, Stylo est imaginé comme un composant d'autres systèmes, mais ça c'est une autre histoire.

Pour les personnes sont dans le domaine académique et qui hésitent encore à plonger dans les langages de balisage léger comme Markdown, Zettlr est une belle opportunité.

## Références

{{< bibliography >}}
