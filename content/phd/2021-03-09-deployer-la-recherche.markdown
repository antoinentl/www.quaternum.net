---
layout: post
title: "Déployer la recherche"
date: "2021-03-09T23:55:00"
comments: true
published: true
description: "Je débute une expérimentation&nbsp;: pendant une quinzaine de jours je vais écrire au fur et à mesure une communication dans le cadre du colloque Études du livre au XXIe siècle, sur deployer.quaternum.net."
categories:
- carnet
- phd
classement:
- phd
---
Je débute une expérimentation&nbsp;: pendant une quinzaine de jours je vais écrire au fur et à mesure une communication dans le cadre du colloque Études du livre au XXI<sup>e</sup> siècle, sur [deployer.quaternum.net](https://deployer.quaternum.net). <!-- more -->Intitulé "Déployer le livre", ce texte va présenter ce que signifie et ce qu'implique utiliser le développement continu pour produire des publications comme les livres.

## Une performance opportuniste
J'aime faire coïncider la forme des publications de mes recherches avec leur sujet, même si j'ai moins l'occasion que je le souhaiterais de donner à voir des performances éditoriales.
[Mon mémoire](https://memoire.quaternum.net/) avait déjà pris cette forme, bien qu'imparfaite cette initiative m'avait semblé cohérente — et surtout je m'étais [amusé](https://gitlab.com/antoinentl/systeme-modulaire-de-publication/-/network/master).
Après tout, observer et analyser des formes expérimentales requiert peut-être d'adopter aussi une approche ou une pratique expérimentale.

C'est aussi une contrainte que je me pose en réaction à un emploi du temps surchargé dans lequel il est difficile de caler plusieurs jours de préparation d'une communication pour un colloque.
Placer des jalons sur un temps relativement court permet d'appréhender une démarche de recherche scientifique avec un peu plus de tranquillité et de pragmatisme, enfin pour moi.
Cela fait plus de deux ans que je prends des notes pour aborder le sujet du déploiement, mais dépasser le temps de l'infusion demande une concentration — c'est le cas de le dire — particulière qui nécessite un temps court.

## Une nouvelle rencontre du numérique et du livre
Plutôt que _numérique_, il faudrait parler de la rencontre entre des pratiques du développement informatique et le processus éditorial à l'origine du livre.
Le développement continu et l'intégration continue sont deux concepts et deux pratiques qui ont beaucoup bouleversé les pratiques du développement informatique, dans l'optique d'ajouter de l'automatisation et de structurer les étapes de conception, de test et de mise en production du code — des programmes, des logiciels, des applications, des sites web ou des jeux vidéo notamment.
Mais qu'en est-il quand le domaine du livre s'empare de ces questions ?
Je m'arrête là, le reste est à découvrir directement sur le site dédié à cette petite expérimentation scientifique (voir ci-dessous).

## Le dispositif performatif
Pour _montrer_ un texte en train de se faire, j'ai mis en place un petit dispositif auquel je pensais depuis plusieurs années (le temps d'infusion, tout ça).
Il s'agit simplement de fragmenter un texte et de le publier graduellement, sur une même page.
Rien d'extraordinaire en soit, sauf que l'idée est de proposer un flux RSS pour suivre les mises à jour de cette page&nbsp;: plutôt que des pages web successives c'est une même page qui est produite, le flux RSS ne renvoyant que vers des ancres.

Les fichiers sources sont versionnés avec Git, et chaque _commit_ déclenche une ou plusieurs actions, dont la production du mini-site web de cette publication.
J'applique donc ce que j'étudie pour la production et la diffusion de ce texte.

Pour tout savoir :

- le site du colloque Études du livre au XXI<sup>e</sup> siècle&nbsp;: [https://projets.ex-situ.info/etudesdulivre21/](https://projets.ex-situ.info/etudesdulivre21/)&nbsp;;
- la page de présentation de mon texte sur le site du colloque, avec la possibilité de commenter&nbsp;: [https://projets.ex-situ.info/etudesdulivre21/liv1/fauchie/](https://projets.ex-situ.info/etudesdulivre21/liv1/fauchie/)&nbsp;;
- le site web dédié : [deployer.quaternum.net](https://deployer.quaternum.net)&nbsp;;
- le flux RSS : [deployer.quaternum.net/atom.xml](https://deployer.quaternum.net/atom.xml)&nbsp;;
- le mot-dièse [#deployerlelivre sur Twitter](https://twitter.com/search?q=%23deployerlelivre&src=typed_query&f=live) ou sur [Mastodon](https://mamot.fr/tags/deployerlelivre)&nbsp;;
- le dépôt du projet : [https://gitlab.com/antoinentl/deployer-le-livre/](https://gitlab.com/antoinentl/deployer-le-livre/).
