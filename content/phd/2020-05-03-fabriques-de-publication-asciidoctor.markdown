---
layout: post
title: "Fabriques de publication : Asciidoctor"
date: "2020-05-03T22:00:00"
comments: true
published: true
description: "Troisième analyse d'une série sur les fabriques de publication : Asciidoctor"
categories:
- carnet
- phd
classement:
- phd
status: brouillon
bibfile: data/analyses-asciidoctor.json
---
Troisième analyse d'une série sur les fabriques de publication&nbsp;: Asciidoctor.

<!-- more -->

[Introduction et liste des fabriques](/fabriques)

Asciidoctor est un processeur de texte et une chaîne de publication qui convertit le format de balisage léger AsciiDoc en différents formats de sortie comme HTML, PDF ou DocBook (un schéma XML).

## Description
Le fonctionnement d'Asciidoctor est très bien expliqué, y compris en français, sur le dépôt accueillant le code de ce programme écrit en Ruby : [https://github.com/asciidoctor/asciidoctor/](https://github.com/asciidoctor/asciidoctor/).
Pour faire une comparaison avec deux fabriques de publication [déjà présentées ici](/fabriques), l'association d'Asciidoctor et d'AsciiDoc est assez proche de celle de Pandoc et de Markdown.
D'un côté un langage de balisage léger, ici AsciiDoc, plus riche que Markdown et qui permet de structurer des textes complexes, et pensé plus spécifiquement pour de la documentation.
De l'autre Asciidoctor, un processeur conçu pour transformer ce balisage léger en d'autres formats, principalement HTML et DocBook, mais également PDF.

Asciidoctor est bien plus qu'un processeur, puisqu'il est accompagné de plusieurs modules ou bibliothèques qui viennent augmenter les fonctionnalités initiales.
Nous pouvons mentionner [asciidoctor-bibtex](https://github.com/asciidoctor/asciidoctor-bibtex/), un _package_ qui ajoute la possibilité d'intégrer des citations bibliographiques ou des bibliographies dans un document AsciiDoc, via une source de références en BibTeX.
Citons également [asciidoctor-reveal.js](https://github.com/asciidoctor/asciidoctor-reveal.js), destiné à générer des présentations [Reveal.js](https://github.com/hakimel/reveal.js) à partir d'une source AsciiDoc.
Il y a beaucoup d'autres possibilités d'export (notamment le format du livre numérique EPUB), les seules contraintes sont des informations d'entête à ajouter dans le document source, ou parfois des balisages spécifiques.
Ces _extensions_ peuvent être gérées avec [Bundle](https://bundler.io/), un moyen assez simple d'installer différents programmes en Ruby, de les garder à jour et de pouvoir déplacer tout cela au besoin.

Précisons qu'en plus d'être plus riche que Markdown, AsciiDoc (associé à Asciidoctor) introduit une dimension supplémentaire : le processeur reconstruit un arbre sémantique lorsqu'il parse la source en AsciiDoc, ce qui permet d'intégrer des fonctions intéressantes comme l'affichage de données pour un format d'export spécifique (typiquement ne pas intégrer une vidéo dans un export PDF, contrairement à un export HTML), ou l'utilisation de variables dans l'entête du document AsciiDoc.
Markdown peut permettre ce type de manipulation, mais il faut alors l'associer à un format de description de données comme [YAML](https://yaml.org/), voir ajouter des scripts spécifiques.
AsciiDoc et Asciidoctor le proposent par défaut, cet avantage peut devenir une contrainte – nous le verrons plus bas.

Pour utiliser AsciiDoc et Asciidoctor depuis plusieurs années, je peux témoigner de sa facilité d'usage et de sa puissance.
Pendant deux années j'ai créé tous mes supports de cours (une quinzaine de cours différents, probablement un cinquantaine de supports au total) avec Asciidoctor, produisant deux ou trois sorties différentes à partir de la même source : PDF pour imprimer les supports pour les étudiants (format A4 paginé), Reveal.js (HTML) pour les projections pendant les séances, et parfois HTML pour consulter les contenus dans un navigateur (j'ai toujours en projet d'expliquer plus précisément cette chaîne).

## Histoire
À l'origine AsciiDoc est un format de balisage **et** un processeur, d'abord pensé pour produire le format DocBook {{< cite "hamilton_docbook_2010" >}}, un schéma XML utilisé pour produire des livres techniques.
O'Reilly a utilisé ce format pendant longtemps, et d'ailleurs sa plateforme Atlas est basée sur AsciiDoc.
J'en ai parlé dans mon mémoire dans l'étude de cas consacrée à O'Reilly {{< cite "fauchie_vers_2018" >}}.
DocBook est un schéma XML qui correspond à des usages dans le domaine de la documentation technique, et qui fonctionne très bien pour des livres sur l'informatique par exemple.
L'avantage avec le XML c'est que potentiellement il peut être transformer en presque n'importe quoi, à condition de passer par des règles de conversion complexes en XSL.
AsciiDoc, en tant que processeur, n'est plus mis à jour depuis 2013 (http://asciidoc.org/CHANGELOG.html), et Asciidoctor semble l'avoir remplacé.

Aujourd'hui Asciidoctor est un véritable moteur pour le format de balisage AsciiDoc, avec une communauté très active – menée par l'infatigable [Dan Allen](https://github.com/mojavelinux).

## Specimen
Le projet [Antora](https://antora.org/) donne une bonne vision de ce que peuvent permettre AsciiDoc et Asciidoctor, j'y reviens plus bas.

Pour un exemple dans un domaine relativement classique d'édition de livre, l'ouvrage de Thomas Parisot {{< cite "parisot_nodejs_2018" >}} illustre parfaitement la puissance d'Asciidoctor :

- une seule source : des chapitres au format AsciiDoc ;
- plusieurs formats de sortie : ODT pour l'éditeur qui retravaille le document avant de l'éditer dans InDesign, HTML pour la version web qui intègre des exemples de code exécutables ;
- du déploiement continu pour la version web consultable en ligne : [https://oncletom.io/node.js/](https://oncletom.io/node.js/).

## Critique
La communauté d'Asciidoctor promeut une gestion de la documentation proche de celle du code avec le slogan "Doc as Code" : fichiers texte, versionnement, _single source publishing_, déploiement continu.
Et nous ne pouvons que faire le constat que cela fonctionne, la question étant de savoir si cela peut correspondre à des usages en dehors du domaine de l'informatique ou de la communauté des développeurs et des développeuses, Hubert Sablonnière en parle très bien {{< cite "web_en_vert_documentation_2016" >}}.
Il est possible d'envisager quelque chose comme _book as code_, mais pour cela il faut disposer d'interfaces intermédiaires facilitant la compréhension et la manipulation.
J'en reparlerai dans le cas d'autres fabriques.

Contrairement à Markdown qui a été l'occasion de nombreuses créations d'interfaces d'écriture – de la simple coloration syntaxique à l'environnement d'édition en passant par quelques _boutons_ aidant le balisage –, AsciiDoc et Asciidoctor n'ont pas encore fait l'objet d'implémentation facilitant la rédaction, et dépassant le simple éditeur de texte.
C'est dommage car cela permettrait une meilleure diffusion du langage de balisage léger AsciiDoc.
C'est d'autant plus regrettable que ce langage est standardisé.
Peut-être qu'un outil comme Atlas (voir plus bas) est pourvu de telles fonctionnalités.

Asciidoctor est dépendant du format de balisage AsciiDoc – plus étendu et standardisé que Markdown –, ce qui signifie que ce programme ne prend pas en charge d'autres langages de balisage léger.
Par ailleurs Asciidoctor est principalement pensé pour un usage, la documentation, même s'il est possible le détourner pour écrire tout type de document.
En revanche il faut noter que Asciidoctor, le programme en Ruby, est porté dans un autre langage de programmation, JavaScript, par l'intermédiaire de Guillaume Grossetie, ce qui donne [Asciidoctor.js](https://github.com/asciidoctor/asciidoctor.js).
Asciidoctor s'interconnecte avec d'autres outils ou environnements, permettant de contourner certaines contraintes comme la génération de PDF à la mise en forme avancée {{< cite "grossetie_awesome_2020" >}}.
Signalons aussi que AsciiDoc peut par exemple être utilisé avec [le générateur de site statique Hugo](https://gohugo.io/content-management/formats/)), à condition de disposer d'Asciidoctor.

Comme Markdown, AsciiDoc est détourné pour des usages intéressants.
Le dernier exemple, qui donne matière à réflexion sur la nécessité d'envisager sérieusement l'utilisation de langages de balisage léger dans l'édition scientifique, c'est [asciidoctor-tei](https://github.com/Mogztter/asciidoctor-tei), développé par Guillaume Grossetie et Thomas Parisot.
Il s'agit d'une preuve de concept, l'objectif étant de générer, à partir d'un document balisé en AsciiDoc, un document XML respectant le schéma TEI d'OpenEdition.

## Vers d'autres fabriques
Deux projets ont déjà été cités ici : [Atlas](https://atlas.oreilly.com/), la chaîne de publication de l'éditeur O'Reilly, et Antora.
Je vais uniquement m'attarder sur [Antora](https://antora.org/) : il s'agit d'un générateur de site statique, basé sur AsciiDoc et Asciidoctor, et pensé pour produire de la documentation.
Antora répond à des besoins spécifiques, comme le fait de pouvoir disposer de plusieurs versions d'une même documentation adaptée à différentes versions d'un même logiciel, ou le fait d'intégrer des sources provenant de différents dépôts Git.

## Références

{{< bibliography >}}
