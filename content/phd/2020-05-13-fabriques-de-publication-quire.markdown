---
layout: post
title: "Fabriques de publication : Quire"
date: "2020-05-13T23:00:00"
comments: true
published: true
description: "Sixième analyse d'une série sur les fabriques de publication : Quire."
categories:
- carnet
- phd
classement:
- phd
status: brouillon
bibfile: data/analyses-quire.json
---
Sixième analyse d'une série sur les fabriques de publication&nbsp;: Quire.

<!-- more -->

[Introduction et liste des fabriques](/fabriques)

Quire est un système de publication conçu pour générer différents formats de livres à partir d'une même source.
Créée et maintenue par Getty Publications, Quire est une fabrique modulaire.

## Description
[Quire](https://gettypubs.github.io/quire/) est une solution logicielle, sous-titrée _Multiformat Book Publishing_, pensée pour produire différents formats de livre à partir d'une même source.
Développé par Getty Publications, la structure d'édition au sein du célèbre Getty Museums, Quire est plus spécifiquement conçu pour éditer et publier des catalogues.
Soit un type de livre qui contient des textes, du matériel critique, des photographies, des bibliographies et des fiches de présentation de pièces muséales.
Cette précision est importante car elle a quelques incidences sur la façon dont est imaginé le système.

Quire est en fait l'assemblage de plusieurs programmes, dont Hugo, un générateur de site statique comparable à Jekyll, et Prince XML, un processeur de PDF, en plus d'une série de scripts créés pour automatiser le fonctionnement.
L'originalité de Quire est de proposer une configuration par défaut qui permet de générer assez facilement différents formats (HTML, PDF et EPUB) à partir d'un ensemble de fichiers aux formats Markdown, YAML et CSS.

Quire est en version bêta, et l'accès à cette version est possible après en avoir fait la demande.
[La documentation](https://gettypubs.github.io/quire/), en revanche, est librement accessible.

Cela fait plusieurs années que je m'intéresse à Quire, cet _objet numérique_ est au centre de mon mémoire de master {{< cite "fauchie_vers_2018" >}}, je l'ai présenté lors [d'un Lundi du numérique de l'INHA](https://www.youtube.com/watch?v=-jNEInGuybs), je l'utilise dans un article récent {{< cite "fauchie_les_2020" >}} et j'avais fait un entretien avec l'ancien développeur en charge du projet {{< cite "gardner_publier_2017" >}}.
Malgré tout je ne suis jamais rentré dans les détails du fonctionnement de Quire, et c'est peut-être l'occasion d'expliquer un peu plus comment les composants de ce système s'agencent et s'interconnectent – sous le regard plus connaisseur de [Louis-Olivier Brassard](https://www.loupbrun.ca/).

Quire consiste en deux éléments principaux : quire-cli, une CLI (_Command-line interface_ pour interface en ligne de commande en français) développée en JavaScript qui comporte une série de scripts et qui permet de lancer les processus de génération des différents formats ; quire-theme, un thème Hugo qui est la structuration et la mise en forme des différents éléments d'un livre.
L'interface en ligne de commande, quire-cli, s'utilise dans un terminal, et permet de faire différentes actions comme de générer les formats de sortie HTML, PDF et EPUB.
Des options sont également disponibles pour ces actions.
Le thème Hugo, quire-theme, est un ensemble de gabarits pour les versions web et PDF, il s'agit donc de fichiers HTML et CSS.
Concrètement quire-cli lance plusieurs scripts, qui eux-même déclenchent des programmes pour générer les versions selon plusieurs paramètres.
Hugo est au centre, puisque c'est la _brique_ chargée de générer les fichiers HTML et CSS à partir des données en Markdown (textes) et YAML (métadonnées).
Et ce sont ces fichiers HTML/CSS qui vont permettre ensuite la génération des formats PDF et EPUB.
Pour le format PDF Quire utilise Prince XML, un processeur qui transforme une série de fichiers HTML en PDF paginé à partir de feuilles de style CSS.

Il faut ajouter à cela le fait que puisqu'il ne s'agit que de fichiers plein texte, le versionnement est possible.
C'est ce que fait Getty Publications, et cela permet d'intégrer un processus de gestion et de révision très intéressant.

## Histoire
Quire a été développé au sein de [Getty Publications](https://www.getty.edu/publications/), et plus particulièrement de [l'équipe numérique](https://www.getty.edu/publications/digital/index.html), après plusieurs expériences plus ou moins réussies d'édition numérique.
Faisant le constat que des solutions monolithiques créées et maintenues par des prestataires extérieurs mettaient Getty Publications dans une situation délicate, la décision a été prise de prototyper un système basé sur des technologies du web, ouvertes.
Eric Gardner explique bien les objectifs sous-jacents, notamment la pérennité, la résilience et la qualité.

Après des premiers tests avec le générateur de site statique Middleman {{< cite "gardner_building_2015" >}}, Eric Gardner et Greg Albers ont mis en place Quire comme modèle reproductible, en travaillant avec l'éditrice Ruth Evans Lane {{< cite "evan_lan_editors_2016" >}}.
Depuis plusieurs années, une communauté se fédère autour de cet outil, principalement des musées qui publient des catalogues en versions imprimée et numérique, et qui cherchent comme Getty Publications à gagner en indépendance et en compétences.
La version bêta de Quire est ouverte à condition d'en faire la demande, les informations figurent [sur la documentation](https://gettypubs.github.io/quire/).

## Spécimens
Une dizaine de livres de Getty Publications ont été édités et publiés avec Quire, tous apparaissent [sur la page du site de The Getty](http://www.getty.edu/publications/digital/digitalpubs.html) ou parmi les dépôts GitHub de [Getty Publications](https://github.com/gettypubs/) ou de [The Getty](https://github.com/thegetty).

Nous pouvons nous attarder sur [Artistry in Bronze](https://www.getty.edu/publications/artistryinbronze/) {{< cite "mdaehner_artistry_2017" >}}, un catalogue qui présente plusieurs dizaines de bronzes conservés au J. Paul Getty Museum.
La version web accompagne une version imprimée, une version PDF légère, une version EPUB et un format MOBI pour les liseuses Amazon.
Tous ces formats sont générés par Quire, et les sources sont versionnées sur GitHub, [sur ce dépôt](https://github.com/gettypubs/artistryinbronze).
La navigation dans le livre web est relativement classique, avec un chapitrage similaire à celui du livre papier.
Le site propose un moteur de recherche interne, et certaines images peuvent être zoomées.

## Critique
Quire est un très bon exemple de chaîne de publication modulaire et multicanale.
Modulaire car chaque fonction est traduite par un programme ou une brique logicielle, ce qui signifie qu'un programme peut être remplacé par un autre {{< cite "fauchie_vers_2018" >}}.
Actuellement [Prince XML](https://www.princexml.com/) est utilisé pour fabriquer les PDF à partir de fichiers HTML et CSS, mais cette solution propriétaire pourrait être remplacée par [Paged.js](https://www.pagedjs.org/).
Muticanale car cette chaîne produit plusieurs formats différents, pour certains catalogues il y a même des jeux de données (des notices complètes d'œuvres).

La modularité envisagée ici est indépendante de l'outillage choisi, c'est-à-dire que la chaîne est en partie indépendante des programmes qui font fonctionner Quire – ou sur lesquels Quire est basé pour le dire autrement.
C'est d'ailleurs pour cela que je préfère le terme de _système_ à celui de _chaîne_.
Pour illustrer ce point particulier nous pouvons noter que les premiers tests de Getty Publications ont été réalisés avec un autre générateur de site statique que celui utilisé actuellement, [Middleman](https://middlemanapp.com/).
Ce changement a été possible – cela ne veut pas dire que c'est forcément rapide ou simple – parce que la plupart des générateurs de site statique sont eux-mêmes modulaires : ils dépendent d'un langage de balisage léger (Markdown dans la majorité des cas), et de formats de description de métadonnées.
Quire pourrait, à moyen ou long terme, abandonné Hugo pour un autre générateur de site statique – nous pourrions d'ailleurs imaginé 11ty avec la richesse de l'environnement natif JavaScript.

Quire est un exemple parmi d'autres d'utilisations d'un générateur de site statique dans le domaine académique {{< cite "diaz_using_2018" >}}, puisque les catalogues produits par Getty Publications restent des ouvrages qui peuvent être catégorisés dans le même spectre que l'édition scientifique.
La spécificité de Quire réside dans l'expérience collective qui est envisagée, il ne s'agit pas d'une utilisation ponctuelle.
La documentation du projet le prouve, Greg Albers – le responsable du projet – et son équipe ont l'ambition de bâtir une communauté.

Quire présente quelques limites qu'il ne faudrait pas minimiser.
Tout d'abord l'utilisabilité, puisque Quire s'utilise avec un terminal.
Être capable d'installer quire-cli réclame quelques compétences qui peuvent être réservées à un profil technique.
En revanche l'édition requiert simplement un éditeur de texte.
Pour simplifier encore ce fonctionnement il pourrait être possible de _brancher_ une interface – comme un [CMS headless](https://en.wikipedia.org/wiki/Headless_content_management_system) – sur Quire pour éviter de passer par un éditeur de texte.
Ensuite la gestion bibliographique : Quire ne permet pas d'utiliser une bibliographie dynamique, par exemple un format BibTeX ou CSL Json exporté depuis un logiciel de gestion de références bibliographiques comme Zotero.
Enfin Quire est basé sur un générateur de site statique, et forcément cet outil a une influence sur le système global.
Sincèrement actuellement je ne vois pas les contraintes qu'impliquent l'utilisation d'un générateur de site statique, qui me semble relativement agnostique techniquement.
Mais c'est un point qu'il faudrait approfondir.

## Vers d'autres fabriques
Je ne suis pas très objectif en prenant l'exemple de la fabrique que nous avons développée avec Julie Blanc, mais cela me semble être un bon moyen de montrer l'influence de Quire sur d'autres initiatives.
Nous avons mis en place [une chaîne](https://gitlab.com/musee-saint-raymond/villa-chiragan/) similaire à celle de Quire pour produire le catalogue _[Les sculptures de la villa romaine de Chiragan](https://villachiragan.saintraymond.toulouse.fr)_ {{< cite "capus_les_2019" >}}, basée sur Jekyll et Paged.js, et non sur Hugo et Prince XML.
Pourquoi ne pas avoir profité de Quire ?
Principalement parce que nous avions plus de 500 références bibliographiques à manipuler dans plus de 100 notices, et donc le recours à Zotero était nécessaire.
J'ai préféré disposer d'une intégration _native_ de BibTeX, si c'était à refaire aujourd'hui je pense que je tenterai plutôt d'adapter Quire à mes besoins, et ainsi contribuer à l'évolution de l'outil.

## Références

{{< bibliography >}}
