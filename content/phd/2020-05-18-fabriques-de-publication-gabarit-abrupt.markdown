---
layout: post
title: "Fabriques de publication : Gabarit Abrupt"
date: "2020-05-18T23:00:00"
comments: true
published: true
description: "Septième analyse d'une série sur les fabriques de publication : Gabarit Abrupt."
categories:
- carnet
- phd
classement:
- phd
status: brouillon
bibfile: data/analyses-gabarit-abrupt.json
---
Septième analyse d'une série sur les fabriques de publication&nbsp;: Gabarit Abrupt.

<!-- more -->

[Introduction et liste des fabriques](/fabriques)

Gabarit Abrupt est une fabrique de livres minimaliste basée sur Pandoc et Make.
Gabarit Abrupt est une recette pour produire des livres aux formats PDF, HTML ou EPUB.

## Description
[Gabarit Abrupt](https://gitlab.com/cestabrupt/gabarit-abrupt/) repose en grande partie sur l'utilisation de Pandoc et de LaTeX, c'est pourquoi j'aurais pu présenter cette fabrique comme spécimen ou application, plutôt que comme fabrique.
La qualité et le soin apportés à ce _gabarit_ méritent d'y passer un peu plus de temps.

À travers ce gabarit, la maison d'édition [Abrüpt](https://abrupt.ch/) propose un ensemble de commandes Pandoc préconfigurées et des modèles de livres (structuration et mise en forme).
En soit il s'agit d'une utilisation assez classique, quoique très méticuleuse, de Pandoc.
Une des particularités réside dans l'automatisation de l'utilisation de ce convertisseur via le recours au programme Make et au fichier de configuration Makefile qui va avec.

[Make](https://fr.wikipedia.org/wiki/Make) est un programme similaire à un script shell, en plus précis et plus configurable, il est notamment possible de spécifier des conditions pour l'ordre d'exécution de scripts ainsi que des variables.
Make est toutefois très exigent et n'accepte aucune erreur.
Dans [le fichier Makefile](https://gitlab.com/cestabrupt/gabarit-abrupt/-/blob/master/Makefile) de Gabarit Abrupt il y a ainsi plusieurs commandes Pandoc préconfigurées, qui font appel à des variables également indiquées dans le fichier – par exemple le format de papier, le gabarit TeX, le titre de la table des matières ou le fichier CSL pour le style bibliographique utilisé.
Les contenus sont organisés dans des fichiers en plein texte, au format `.txt` balisé en Markdown.
Il suffit de lancer une commande `make` pour générer un format de sortie, par exemple `make html` produit la version web du livre.

Ce fonctionnement très minimaliste est décrit avec beaucoup de précision sur le dépôt du Gabarit Abrupt&nbsp;: [https://gitlab.com/cestabrupt/gabarit-abrupt](https://gitlab.com/cestabrupt/gabarit-abrupt).

## Histoire
Abrüpt a mis à disposition ce gabarit il y a un peu plus d'un an, et c'est un outil adopté par la maison d'édition.
C'est donc un processus éprouvé.
Le peu de mises à jour prouve sans doute que ce fonctionnement est suffisamment robuste pour ne pas avoir besoin de corrections particulières.
Pour l'avoir testé c'est assez simple d'utilisation.

## Specimen
Une navigation dans la diversité [du catalogue des éditions Abrüpt](https://abrupt.ch/livres/) permet de constater l'efficacité de cette fabrique.

## Critique
J'ai utilisé le qualificatif _minimaliste_ pour définir cette fabrique, nous pourrions être tentés de recourir au terme à la mode _low-tech_ {{< cite "roussilhe_erreur_2020" >}}.
Il faut faire preuve de prudence ici avec ce concept bancal d'un point de vue théorique, même si plusieurs chercheurs ont tentés de le définir {{< cite "grimaud_low_2017" >}} ou de le replacer dans un contexte de critique de la technologie {{< cite "fourmentraux_disnovation_2017" >}}.
Nous pouvons tout de même constater le peu de composants logiciels nécessaires pour faire fonctionner ce processus, et surtout la solidité des programmes invoqués.
Pandoc, LaTeX et Make sont trois logiciels qui disposent d'une certaine ancienneté et de communautés actives.

Il est intéressant de noter qu'Abrüpt semble disposer de compétences techniques avancées, et qu'un tout autre choix que ce gabarit aurait pu être fait : construire un programme sur mesure, un _framework_ {{< cite "smith_brandon_2020" >}}.
Mais non, cette structure a décidé d'utiliser des composants existants, ce qui permet au moins quatre choses&nbsp;: réduire le temps de création de cette fabrique ; apprendre à utiliser des programmes dont la philosophie sous-jacente n'est pas dénuée de sens ; faciliter la réutilisation par d'autres personnes.

Nous pourrions évoquer le degré de technicité que requiert Gabarit Abrupt, en effet il n'y a ici aucune interface graphique, le terminal et la ligne de commande sont incontournables.
Nous pouvons toutefois imaginer le développement de quelques écrans facilitant l'utilisation de Make, j'ai déjà pu constater quelques initiatives dans ce sens, je reviendrai peut-être sur ces projets dans d'autres fabriques.

Enfin cette fabrique de publication est créée par une maison d'édition : cette structure a conçu son outil de production.
La plupart des éditeurs utilisent des logiciels développés par d'autres, pour plusieurs raisons – temps et compétences principalement –, et cela a de fortes implications sur la façon de considérer des textes, de les travailler, de les éditer.

## Vers d'autres fabriques
Par la suite nous découvrirons d'autres expérimentations qui font un usage de Pandoc et de LaTeX, j'espère pouvoir prendre le temps de présenter d'autres initiatives du même type que cette mini fabrique (une [anti](https://abrupt.ch/antilivre/)-fabrique peut-être ?).

## Références

{{< bibliography >}}
