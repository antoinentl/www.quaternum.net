---
title: "Une semaine au(x) labo(s)"
date: 2023-03-25
description: "Avec ce billet je raconte un peu ce qu'il se passe dans un labo, pour montrer à quel point le travail en équipe est une composante importante, un moteur de la recherche en général et de la thèse en particulier, et aussi une perturbation de la rédaction. Plongée dans une semaine typique quelque part à l'automne 2022."
classement:
- phd
published: true
categories:
- carnet
- phd
---
La vie de doctorant c'est aussi une vie dans des laboratoires, y compris en sciences humaines.
C'est aussi ça la thèse.
Avec ce billet je raconte un peu ce qu'il se passe dans ces lieux de recherche, pour montrer à quel point le travail en équipe est une composante importante, un moteur de la recherche en général et de la thèse en particulier, et aussi une perturbation de la rédaction.
Plongée dans une semaine typique quelque part à l'automne 2022.


## Des laboratoires ?

Je fais des recherches et je travaille pour plusieurs laboratoires.
Si physiquement je passe mon temps à la Chaire de recherche du Canada sur les écritures numériques ou [CRCEN](https://ecrituresnumeriques.ca) (dirigée par Marcello Vitali-Rosati), j'assure aussi le suivi de plusieurs projets pour le Centre de recherche interuniversitaire sur les humanités numériques ([CRIHN](https://www.crihn.org)) et le Groupe de recherche sur les éditions critiques en contexte numérique ou [GREN](https://gren.openum.ca) (dirigés tous les deux par Michael Sinatra).


## Lundi : imprimer un livre et se relire

Entre septembre et décembre 2022 normalement le lundi est un jour dédié à la thèse, et plus particulièrement à la rédaction de la thèse.
Ce lundi c'est une exception puisque la version imprimée du [_Novendécaméron_](https://novendecameron.ramures.org/) doit être produite au plus vite.
C'est un projet du GREN, dirigé par Jean-François Vallée en co-édition avec Chantal Ringuet.
Avec [Louis-Olivier Brassard](https://www.loupbrun.ca/) nous sommes chargés d'accompagner cet éditeur et cette éditrice dans la constitution d'un recueil de textes écrits pendant le premier confinement de la Covid-19 au printemps 2020.
Si [la version web](https://novendecameron.ramures.org/) a été mise en ligne progressivement pendant plusieurs mois au printemps 2021, il restait une version imprimable à produire.

Il est donc 9h quand je me mets sur les dernières relectures de cette version, ensuite avec Louis-Olivier et Jean-François nous passons 4h sur les derniers réglages pour valider l'impression à la demande, afin de recevoir les premiers exemplaires à temps pour le lancement mercredi 30 novembre.
Cette dernière phase d'édition est délicate, forcément on découvre encore des coquilles même après plusieurs dizaines de relectures.
On se démène avec Paged.js mais le résultat en vaut la peine !

14h, après une pause déjeuner je prends deux heures pour trier et répondre aux messages de la fin de semaine (dernière), lire la note pour la réunion d'équipe hebdomadaire de la CRCEN du lendemain, et organiser les réunions sur les développements de Stylo.
Stylo c'est un peu le yoyo émotionnel ces temps-ci : des super fonctionnalités sont en préparation et ça avance bien, et en même temps quelques bugs imprévus (oui c'est le principe d'un bug…) viennent compliquer certains acquis.

16h, l'après-midi est déjà bien avancée, mais je parviens à relire un passage de ma thèse, c'est la phase de relecture de ce fragment (avant de nombreuses autres étapes qui jalonneront ce morceau de texte).
Je complète quelques références bibliographiques, notamment autour du fanzine (aspect relativement marginal dans ma thèse).
Je me replonge dans [des BBF](https://bbf.enssib.fr/consulter/bbf-2015-06-0038-005).

17h, fin de la première journée.
20h30, je reprends ma thèse, avec la fin de la relecture mentionnée juste avant, et la finalisation d'un autre passage que j'avais laissé de côté.
Je parle de livre et de technique, ou plutôt de comment les livres sont produits et des évolutions technologiques concomitantes.
C'est l'occasion d'évoquer le beau livre d'Olivier Deloignon, Jean-Marc Chatelain et Jean-Yves Mollier, [_D'encre et de papier: une histoire du livre imprimé_](https://www.actes-sud.fr/catalogue/arts/dencre-et-de-papier-une-histoire-du-livre-imprime).
Si l'approche est relativement classique (et historique), il y a beaucoup d'exemples bien illustrés sur l'évolution du livre depuis les débuts de l'imprimerie.
L'ouvrage est luxueux, d'ailleurs c'est presque un crime de ne pas avoir mis les textes en libre accès quelque part (90€ c'est très cher pour un livre, même si ça les vaut, et je ne traduis même pas le prix en dollars canadiens…).

22h, avant de m'arrêter je commence à préparer quelques éléments de la chaîne de publication de ma thèse (je commence avec Pandoc et quelques automatisations, je passerai sur d'autres outils en 2023).
Comme souvent ces recherches ne vont pas me servir qu'à moi, l'idée est de pouvoir améliorer plusieurs chaînes de publication utilisées à la CRCEN (principalement pour produire des documents).


## Mardi : la journée tunnel au labo

Le mardi est une journée spéciale au labo, d'habitude c'est la journée où les réunions s'enchaînent, principalement pour la Chaire de recherche du Canada sur les écritures numériques.
9h c'est la fameuse _réunion stratégique_ hebdomadaire, l'occasion de faire le point sur les projets en cours, les événements à venir, les demandes de subvention à prévoir, la gestion administrative, les répartitions de tâches, etc.
J'arrive juste à l'heure après 25 minutes intenses de vélo à -5°C (près de -10°C ressentis), une bonne température avec des pistes cyclables déneigées.
Il n'y a pas grand chose à dire si ce n'est que beaucoup de sujets sont évoqués, on s'attarde sur les problèmes de gestion administrative ou de problèmes techniques sur certains sujets.

10h30, l'équipe (une dizaine de personnes) se sépare : d'un côté pour la rédaction d'une demande de subvention (je ne peux pas en dire grand chose à ce stade), et d'un autre pour les gestions diverses des projets.
Pour ma part je dois préparer une autre réunion, autour des activités du Groupe de recherche sur les éditions critiques en contexte numérique (GREN).
Je prévois de présenter les travaux autour des chaînes d'édition mises en place pour [Parcours numériques](https://www.parcoursnumeriques-pum.ca/) et les [Ateliers de \[sens public\]](https://ateliers.sens-public.org/) à Victoria en janvier 2023 avec Michael Sinatra (mon co-directeur de thèse) dans le cadre de [INKE](https://inke.ca/).
C'est une belle occasion de faire le point sur les chantiers menés depuis 3 ans, de prendre le recul nécessaire sur des projets d'édition numérique.
Je travaille sur le plan détaillé du _papier_ avec la constitution d'une bibliographie — il faudra d'ailleurs que je commente cette bibliographie.
Je dois aussi régler plusieurs questions logistiques pour le voyage entre Montréal (Québec, côte est) et Victoria (Colombie Britannique, côte ouest).

12h, j'ai terminé mon plan détaillé et réglé les principaux détails logistiques.
J'en ai aussi profité pour préparer d'autres points concernant le GREN suite à des ateliers qui se sont déroulés en septembre, sur le recrutement d'étudiant·e·s qui viendront (peut-être) en stage ou d'un site web en préparation pour présenter les activités d'un projet.
C'est donc l'heure du repas du midi, moment collectif où l'on se sert dans un des deux bureaux et pendant lequel on parle trop du travail…

13h, je fais un point avec un stagiaire de la CRCEN qui travaille justement sur un prochain livre aux Ateliers de \[sens public\].
On regarde ensemble comment il utilise le workflow en place, ce qu'il ne comprend pas et ce qu'il pourra faire pour avancer.
C'est un temps précieux d'échange qui me/nous permet de clarifier des fonctionnements, de simplifier des conversions ou d'imaginer des chantiers pour refondre la chaîne d'édition.

13h30, réunion avec Michael durant laquelle beaucoup de points sont abordés.
On passe indistinctement de l'après thèse à la présentation pour INKE en passant par le recrutement des étudiant·e·s.
Le temps passe vite et sans s'en rendre compte on règle beaucoup de points en suspens.

16h, je fais un point rapide avec plusieurs personnes pour répondre à des questions techniques sur différents projets ou pour aller chercher des informations sur des chantiers à venir.

17h, ma deuxième journée commence avec ce que j'appelle une _nocturne_.
Normalement c'est un temps entièrement dédié à la thèse, où le labo se vide — s'il reste des personnes elles sont souvent également en rédaction, ce qui donne une ambiance studieuse où on se retrouve pas tout seul pour faire des pauses.
J'avance sur la rédaction de plusieurs parties, mais pas assez vite à mon goût : l'avantage de la _nocturne_ c'est de ne pas être dérangé et d'avoir 4h de temps sans interruptions extérieures, l'inconvénient c'est qu'après une journée de près de 8h l'énergie manque…


## Mercredi : écrire entre les tâches

Pendant l'automne 2022 cette journée est également censée être réservée à la thèse, dans les faits c'est plus compliqué…
9h, je commence par trier trop de messages, je réalise quelques tâches trop diverses, et je fais un peu de veille.

10h30, je passe sur un petit projet d'édition qui n'est pas lié aux labos, j'en parlerai peut-être plus tard par ici.

12h30, pause déjeuner.

13h30, je passe à la rédaction… d'un chapitre pour un ouvrage collectif, pas encore sûr qu'il soit publié.
Je n'y parle pas directement des projets des labos mais je sais que je pourrai réutiliser des passages pour ma thèse.
C'est une pratique que j'essaye d'adopter : les communications, articles ou chapitres doivent aussi faire avancer la thèse, même si ça ne marche pas à chaque fois.
Je devais y passer 1h mais j'écris toute l'après-midi.

17h, fin de journée, beaucoup de frustration concernant la thèse, mais les projets avancent.

21h, je prépare les derniers détails de la séance du cours du lendemain, jusqu'à 22h30.


## Jeudi : enseigner et écrire

Pendant cette session d'automne 2022 je donne le cours "HNU2000 Humanités numériques : technologies", tous les jeudis de 8h30 à 11h30.
Donner un cours à 8h30 ça veut dire partir tôt (je suis à 35 minutes en vélo de l'Université de Montréal, plus le temps de me changer et de préparer le cours).
La douzaine de séances de ce cours est aussi l'occasion d'inviter quelques intervenants, ce jeudi c'est Roch Delannay, collègue du labo (CRCEN) qui vient présenter [Gephi](https://gephi.org/) après mon introduction sur la théorie des graphes et sur le logiciel libre.

13h, après un rapide déjeuner avec Roch et Marcello on enchaîne avec une réunion autour de Stylo et des transformations pour la production des XMLs pour Métopes.
Il s'agit de créer des règles précises de transformation qui se traduisent par des filtres dans certains cas.
La session s'étire sur des tests de templates complexes et sur la définition de besoins.

15h30, je prépare les éléments de la prochaine séance du cours, que ce soit mon support ou les lectures pour les étudiant·e·s.
J'essaye d'anticiper au maximum même si je me retrouve toujours à finaliser mes supports la veille du cours…
Les lectures imposées aux étudiant·e·s m'obligent aussi à replonger dans des textes que je n'ai pas lu depuis un moment ou que je n'avais que survolé, c'est donc aussi l'occasion d'approfondir des lectures pour ma thèse.

16h30, j'ai à peine le temps d'ouvrir un texte pour ma thèse que je dois finir cette journée au labo.
Pas de travail ce soir, il est nécessaire de prendre un peu de repos (après différentes tâches domestiques).


## Vendredi : respirer

Depuis quelques mois j'ai adopté une organisation particulière pour passer du temps avec la personne avec qui je vis, sans pour autant être à trois avec l'enfant.
C'est un moment important, et ça se passe forcément un jour de semaine (= d'école).
Souvent c'est le vendredi après-midi (au moins un vendredi sur deux), jour plus calme aux labos, ce qui me laisse le vendredi matin pour écrire.
Ce jour-là je prends 3h le matin pour faire des recherches bibliographiques et quelques tests avec Pandoc ou Hugo pour construire les _artefacts_ de ma thèse.
C'est un peu un temps suspendu où je ne me mets pas de pression et pendant lequel j'explore des pistes théoriques et techniques intéressantes, souvent abandonnées par la suite.
Cette session de recherche n'est pas très fructueuse cette fois-là, mais la perspective d'une après-midi libre me le fait vite oublier.


## Samedi et dimanche

J'essaye de ne pas travailler en fin de semaine, autant dire que c'est presque impossible en faisant une thèse (et en la finançant en travaillant dans des labos).
Je me suis tout de même imposé de ne pas faire de travail _salarié_ pendant ces deux jours.
Donc sauf urgence je ne touche pas aux activités des labos.
Je prends quelques heures pour faire de la veille et trier des références bibliographiques, mais aussi pour gérer les tâches administratives universitaires (trop chronophages).


## Et une semaine en 2023

Pour rassurer celles ou ceux qui se demanderaient comment la rédaction d'une thèse peut avancer dans ces conditions, mais aussi mes directeurs qui attendent la livraison des chapitres (en même temps que mon implication sur les projets, c'est le jeu), mon organisation a changé depuis le tout début de l'année 2023.
Je consacre la majorité de mon temps à la thèse, et je délaisse un peu les activités des labos.
Aussi, le fait de ne plus enseigner me fait gagner un temps précieux.

Je ne suis pas certain que cet exercice soit pertinent pour montrer ce qu'il se passe sur une semaine de rédaction de thèse (à moins de trouver une contrainte un peu plus intéressante), mais je trouve que ce court récit donne une bonne idée d'une semaine typique dans un labo !

