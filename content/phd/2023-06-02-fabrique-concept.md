---
title: "Fabrique : concept"
date: 2023-06-02
bibfile: "data/fabrique-concept.json"
---
Le terme _fabrique_ définit une approche spécifique dans la création et dans la production d'artefact, y compris dans le domaine de l'édition.
Pour débuter un échange _épistolaire_, par carnets [interposés](https://blank.blue/fabrique/la-fabrique-a-faire/), je me lance enfin dans la définition de ce concept.
À ma manière, le pong n'étant pas le ping.
La recherche d'une complémentarité n'étant pas l'objet de ce texte, mais plutôt de faire coup double : définir un concept central dans mes recherches actuels, engager une conversation à laquelle je tiens.
À ma manière, peut-être trop méthodique.
Un avertissement est toutefois nécessaire : il s'agit d'un premier texte volontairement incomplet, ce carnet de recherche étant l'occasion de publier des travaux en cours et donc loin d'être finalisés.

En préambule je dois également préciser que j'ai entrepris d'analyser ce que j'ai nommé des "fabriques de publication" [depuis quelques années](/fabriques).
"Fabrique" avait alors été choisi comme alternative à plusieurs autres dénominations : outil, instrument, dispositif, machine ; technologie, technique, méthode ; chaîne, système, fabrique, forge.
Ce choix s'est fait par défaut, mais il s'est ensuite révélé particulièrement fécond.

> Fabrique.  
> Action de fabriquer.  
> [CNRTL](https://www.cnrtl.fr/definition/fabrique)

Le terme "fabrique" est utilisé très souvent depuis plusieurs années, que ce soit pour des projets en sciences humaines, des titres d'essais, d'ouvrages ou d'articles scientifiques, ou de thèses.
Une maison d'édition française en a même fait son nom.
Le mot est partout (une rapide recherche vous le prouvera, c'est vertigineux !).
À tel point que son usage en devient suspect.
D'autant plus que le terme est rarement défini, comme si son sens allait de soit, qu'il ne nécessitait pas de précision.
Pourtant son positionnement lexicographique, entre _exécution_, _faire_, _produire_ ou _créer_, suscite la curiosité.
En effet ce mot est bien pratique, il permet de s'extraire d'une logique purement industrielle dans une activité de production, et de reconsidérer la dimension artisanale de certaines initiatives créatives, encore plus dans le vaste champ littéraire.
Plus qu'un terme, _fabrique_ peut être considéré comme un concept, et c'est l'entreprise que je me propose de débuter ici.

Commençons par une définition du contexte de la _fabrique_, la fabrique _produit_ quelque chose, mais cela passe par d'autres étapes qu'il convient de définir : créer, fabriquer, manufacturer ou produire ont des objectifs divers.
Ensuite, et aussi pour répondre à l'invitation de [La fabrique à faire](https://blank.blue/fabrique/la-fabrique-a-faire/), la fabrique se concentre sur un lieu ou un espace, l'acte se réalise _quelque part_.
Et ce _quelque part_ peut être ouvert, à la fois aux collaborations mais aussi aux modifications tel un travail continu.
Enfin, c'est le lien avec l'artefact que je souhaite explorer, et proposer comme prochain échange.


## Construire, fabriquer, manufacturer, produire

Précisons que le terme "fabrication" est défini pour le domaine du livre {{< cite "eyrolles_les_2009" 72-73 >}}, mais cette définition se limite à préciser qu'il s'agit de plusieurs sous-processus.
[La définition](https://www.cnrtl.fr/definition/fabrique) du Trésor de la langue Française informatisé, donnée par l'intermédiaire du Centre national de ressources textuelles et lexicales, précise dans chacune de ses subdivisions que la fabrique donne un résultat.
La fabrique contient plusieurs dimensions ou étapes, ce qui permet tout de suite de comprendre qu'il ne s'agit pas d'une activité d'un seul bloc, c'est un _processus_.
De la fabrique _résulte_ ce que nous pouvons qualifier d'artefact — en tant que production humaine.

Sans entrer dans le détail de ce processus, plusieurs actions sont récurrentes : construire, produire, manufacturer, etc.
Il y a donc une conception, une création, qui est ensuite réalisée afin d'obtenir un ou plusieurs exemplaire d'un artefact.
La production est cette action d'élaborer puis de former un objet à partir d'une idée.
Dans le cas de la fabrique, la production est particulière, il ne s'agit pas d'une démarche dite industrielle qui vise à produire très rapidement en de très grandes quantités.
Il ne s'agit pas non plus de produire totalement à la main dans le sens où la technique n'aurait que peu de place ici.
La réalisation de l'artefact ou des artefacts est donc permise avec des techniques, celles-ci peuvent être réalisées par l'intermédiaire de machines.
C'est pourquoi _manufacturer_ permet de mieux comprendre de quoi il s'agit : une transformation est opérée via un processus technique qui peut être réalisé avec des instruments ou des machines, à une échelle qui oscille entre l'artisanat et l'industriel.
Enfin, ce qui est produit doit permettre une "application pratique" :

> Fabriquer, cela signifie d’abord manipuler et détourner quelque chose qui fait partie du donné, le changer en artefact et le tourner vers l’application pratique. {{< cite "flusser_petite_2002" 58 >}}

Cette dimension de la fabrique est importante, elle permet ensuite d'envisager un mouvement interne qui consiste à modifier la fabrique en même temps que la fabrique produit un objet.


## L'acte et le lieu

La fabrique est un processus, une suite d'opérations, un acte.
Pour le dire en quelques mots, l'acte a ceci d'intéressant qu'il intègre une dimension technique (là où _geste_ entretient un flou sur les détails de l'action) qu'on ne peut pas ignorer.
Cette technique concerne autant des activités manuelles que des activités mécanisées ou automatisées.
Historiquement la fabrique apparaît en effet dans une perspective de mécanisation, elle suit la manufacture et elle précède l'usine.
C'est, en quelque sorte, un espace où l'on produit en se posant la question de la _façon_ dont on produit.
La question n'est alors plus tant celui du résultat que de la manière dont ce résultat est obtenu.

Est-ce que la fabrique est un lieu ?
Un espace ?
Dans les définitions classiques du terme c'est une acception proposée.
Je souhaite répondre à cette question en la détournant : à mon sens la fabrique, telle que définit en tant que concept, est plus proche du processus que du lieu.
Pourtant il est nécessaire de considérer que cette activité humaine ne peut se déployer que dans un espace (à défaut de lieu).
Un espace purement technique ?
Numérique ?
Je laisse ici ces questions.


## Ouvrir les dispositifs

Le terme étant désormais balisé (production, artefact, technique, espace), ou presque, je convoque désormais Vilèm Flusser pour déterminer la dimension conceptuelle de la fabrique.
Dans un petit livre très intelligent, _Petite philosophie du design_ (bien plus éloquent en anglais, _Shape of Things: a Philosophy of Design_), Vilèm Flusser consacre un chapitre à "La fabrique" (_The Factory_ en anglais).
Sur une douzaine de pages l'auteur déroule une réflexion sur la définition de l'humain et son rapport à la technique.
L'humain ne se distingue pas par une supposée sagesse mais par sa capacité à _fabriquer_, "peu importe quoi" {{< cite "flusser_petite_2002" 57 >}}.

> […] Factories are places in which new kinds of human beings are always being produced: first the hand-man, then the tool-man, then the machine-man, and finally the robot-man. To repeat: This is the story of humankind. {{< cite "flusser_shape_2013" 44-45 >}}

> […] les fabriques sont de lieux où sont sans cesse produites de nouvelles variétés d'hommes : d'abord l'homme-main, puis l'homme-outil, puis l'homme-machine et enfin l'homme-appareil. On l'a déjà dit : l'histoire de l'humanité, c'est cela. {{< cite "flusser_petite_2002" 58-59 >}}

Cette évolution (nous nous retenons volontairement de parler de _progression_) va vers plus d'automatisation.
Alors qu'avec l'outil la personne se plaçait au centre de l'atelier, la fabrique modifie cette disposition avec la machine, "c’est elle la constante et l’homme la variable" {{< cite "flusser_petite_2002" 59 >}}.
À tel point que la fabrique n'est plus seulement un agent de la production, elle a aussi une influence sur celui ou celle qui l'utilise et la met en place.
Avec ce que Vilèm Flusser considère comme la troisième révolution industrielle (avec l'introduction de l'_appareil_), nous avons une occasion de reconsidérer notre rapport aux machines, et donc aussi à la technique.
Un peu comme Gilbert Simondon, Vilèm Flusser considère qu'une nouvelle synergie peut être trouvée.
Après la main, l'outil et la machine, l'appareil nécessite un apprentissage constant, comme la détermination continue d'une adéquation.

C'est cette notion d'_apprentissage_ qu'il faudrait interroger plus longuement, à quel point le fait de devoir apprendre, réapprendre, à chaque expérience éditoriale et pédagogique, nous engage dans une fabrique ?
Pourrait-il en être autrement ?

> The factory will have to be the place in which human beings altogether will learn by means of robots: what, why and how to turn things to use. {{< cite "flusser_shape_2013" 50 >}}

## L'artefact, avant/après

Déplaçons-nous du côté de l'anthropologie.
Dans _Faire: anthropologie, archéologie, art et architecture_, Tim Ingold analyse longuement la _fabrication_ d'un biface pour déterminer la façon dont il est produit et dont il est utilisé {{< cite "ingold_faire_2017" 83-109 >}}.
Si Tim Ingold parle beaucoup de fabrication, à aucun moment il n'est question de _fabrique_.
L'hypothèse proposée par l'auteur est la suivante : le _faire_ est autant l’application d’une conception intellectuelle que l'acte qui forme cette conception.
Autrement dit, est-il possible de suivre un parcours linéaire pour _fabriquer_ un artefact ?
Tim Ingold critique fortement le modèle hylémorphique.
Dans le cas de la production d'un livre, il faudrait définir ce qu'il se passe.
Quel rôle joue la fabrique ici ?
Je laisse également cette question ouverte, et je ne prolonge pas plus, pour le moment, l'apport de Tim Ingold sur ces questions.


## Le mouvement de la fabrique

En guise de conclusion, ou d'ouverture vers d'autres explorations de ce concept, notons que la fabrique peut être l'occasion d'un mouvement circulaire, double, ou réflexif.
En produisant un artefact avec une fabrique, la fabrique est elle-même modifiée, complétée, reconfigurée, autour de la production d'un artefact.

Cette définition, qui tient plus de la prémisse d'une recherche en cours, permet d'identifier les enjeux et les tensions qui existent lorsque l'on parle de _fabrique_, et qui mérite d'être prolongée, dans un nouvel échange (j'espère).


## Références

{{< bibliography >}}

