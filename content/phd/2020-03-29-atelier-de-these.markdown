---
layout: post
title: "Atelier de thèse"
date: "2020-03-29T22:00:00"
comments: true
published: true
description: "Qu'est-ce qu'un atelier de thèse ? J'esquisse mes différents espaces de travail depuis la prise de note jusqu'à la rédaction, en passant par un robot littéraire et la définition d'une stratégie de publication."
image: "https://www.quaternum.net/images/2020-atelier-phd-m.jpg"
categories:
- carnet
- phd
classement:
- phd
status:
---
Qu'est-ce qu'un atelier de thèse ?
J'esquisse mes différents espaces de travail depuis la prise de note jusqu'à la rédaction, en passant par un robot littéraire et la définition d'une stratégie de publication.

<!-- more -->

Vous vous trouvez actuellement dans une partie de [mon atelier de thèse (ou atelier PhD)](/phd), partie visible et publique parmi les différents espaces que j'ai mis en place dans le cadre d'un doctorat et d'une thèse.
Mon objet d'études étant la publication – dans le vaste champ littéraire –, le minimum me semble d'adopter une partie des pratiques que j'observe, au moins pour les éprouver.
J'ai mis quelques mois avant de réussir à envisager cet atelier, quelques mois pour accepter aussi une filiation entre ce carnet qui a plus de huit ans, et une pratique inédite pour moi qui est le doctorat et la perspective de la thèse.

![](/images/2020-atelier-phd.svg)  
_Schéma global des différents espaces et des relations avec d'autres écosystèmes_

## Les espaces de l'atelier
C'est d'une certaine façon la méthodologie de ma recherche, la structure de mon travail de doctorant.
Parler d'espaces est simplement un moyen pour signifier différents types de textes que je suis amené à produire à différentes étapes&nbsp;:

- **notes**&nbsp;: comme son nom l'indique, l'inscription d'une idée, des informations sur un livre, les bribes d'une discussion, bref tout ce qui peut nourrir mon sujet. Ces notes sont rédigées de façon informelle et rapide, avec un rythme fréquent&nbsp;;
- **concepts**&nbsp;: la définition de concepts que je manipule ou auxquels je me réfère, en tentant de respecter un certain formalisme (clarté, bibliographie, argumentation)&nbsp;;
- **analyses**&nbsp;: l'observation des objets de mon corpus (livre et systèmes de publication)&nbsp;;
- **lectures**&nbsp;: études de passages, chapitres ou livres qui alimentent ma recherche&nbsp;;
- **entretiens**&nbsp;: avec les acteurs de mes objets d'études. Je ne sais pas encore quelle méthodologie précise appliquer ici&nbsp;;
- **weeknotes**&nbsp;: des notes d'impression sur le processus, le temps qui passe (trop vite) et mon ressenti pendant le doctorat. Sur le modèle des [weeknotes initiées par Thomas](https://détour.studio/#weeknotes).

![](/images/2020-atelier-phd-focus-1.svg)  
_Focus sur les deux premières phases d'inscription_

Ces différentes catégories sont liées entre elles.
Le lien se fait principalement entre les notes et tout le reste&nbsp;: les notes sont de brefs points de départ, qui engagent des chemins plus longs et sinueux pour construire des textes qui se tiennent – notamment scientifiquement.
Il est possible que de nouveaux espaces viennent s'ajouter, ou que certains disparaissent, au gré du parcours.
Rien n'est figé.

![](/images/2020-atelier-phd-focus-2.svg)  
_Focus sur les phases de rédaction qui mènent à la thèse_

## Un robot fait de commits littéraires
Ayant une pratique d'écriture publique régulière depuis plusieurs années (avec ce site [quaternum.net](https://www.quaternum.net)), mettre en place un tel atelier sans rien pouvoir montrer est pour moi assez frustrant.
En effet cet atelier n'est pas public et ne doit pas l'être.
Certaines personnes vont y avoir accès ponctuellement, comme mon directeur de recherche ou des lecteurs ou lectrices avisées.
Il me fallait donc trouver un stade intermédiaire pour dévoiler une partie du travail en cours.

Chaque ajout – nouveau texte ou modification quelconque – est versionné.
J'utilise Git, un système de gestion de versions très répandu.
Sans entrer dans les détails, le cœur de Git est constitué de _commits_.
Chaque commit est une session de modifications (plus ou moins nombreuses, sur un ou plusieurs fichiers), chaque commit est accompagné d'un message informatif qui décrit cette intervention.

![](/images/2020-atelier-phd-focus-3.png)  
_Focus sur le versionnement et les commits littéraires_

Dans mon cas chaque message de commit s'inspire du texte réellement rédigé pour lui donner une dimension littéraire synthétique en moins de 80 caractères.
Ces commits constitue une forme de journal, la partie émergée de tous les écrits produits dans cet atelier.
Voici quelques exemples&nbsp;:

Le littéraire résiste-t-il au numérique, par nature ? (Le peut-il ?)  
Un paysage de solutions ouvertes, vaste.  
Des systèmes de publication agnostiques.  
L'informatique révèle la vraie nature du texte.  
Dévoiler pour mieux chercher.  
La page papier s'est-elle déjà suffit à elle-même ?

Chaque commit fait automatiquement l'objet d'un tweet sur un compte Twitter créé pour l'occasion&nbsp;: [@qtrnmnet](https://twitter.com/qtrnmnet).
Voici ci-dessous le premier tweet du 22 janvier 2020&nbsp;:

> Écrire entre les lignes, dans les creux de Git.  
[@qtrnmnet](https://twitter.com/qtrnmnet)

## Stratégie de publication
Cet atelier est connecté avec d'autres espaces de publication, comme le schéma tente de le montrer.
Tout d'abord chaque texte écrit dans l'atelier a pour objectif de nourrir, à un moment ou à un autre, la thèse.
Ces textes seront réécrits, mais le fait de les inscrire très tôt donne une impulsion vitale.

![](/images/2020-atelier-phd-focus-4.png)  
_Focus sur la relation entre l'atelier et mon site personnel_

Certains des textes ont également vocation à être présentés sur [quaternum.net](https://www.quaternum.net), pour donner à voir une recherche en train de se faire.
Probablement imparfaite et en cours d'amélioration.
Deux exemples extraits de la rubrique intitulée [Carnet de thèse (PhD)](/phd/)&nbsp;:

- [Version : concept](/2020/01/09/version-concept/)&nbsp;: il s'agit du premier concept que j'ai défini, il me semblait pertinent de le partager. À la fois parce que d'autres peuvent se servir de certaines informations, et aussi pour tester la validité de ce texte. C'est aussi un rendu pour un séminaire de doctorat&nbsp;;
- [L'écriture du papyrus à l'hypertexte](/2019/12/12/l-ecriture-du-papyrus-a-l-hypertexte/)&nbsp;: c'est un rendu pour un autre séminaire, qui m'a été utile pour formuler certaines hypothèses. En l'occurrence ce texte est très imparfait et il sera modifié.

Enfin l'atelier de thèse est un point de départ vers d'autres publications dans des circuits plus traditionnels&nbsp;: communications pour des colloques et articles pour des revues.

## Moteur de l'atelier
C'est un point important sur lequel je ne vais pourtant pas m'attarder, cela fera l'objet d'autres billets.
Mon premier postulat est le suivant&nbsp;: cet atelier doit prendre la forme d'un site web.
J'ai besoin, lorsque j'écris, de pouvoir manipuler la forme produite.
Un site web se prête bien à ce type de pratique, notamment en ayant un site fonctionnel _en local_ sans pour autant l'héberger sur un espace public.

Après un court passage par [11ty](https://www.11ty.dev/), un chouette générateur de site statique qui impose peu de contraintes, j'ai choisi [Hugo](https://gohugo.io/), notamment parce qu'il a très peu de dépendances.
Ne pas choisir [Jekyll](https://jekyllrb.com/) est pour un moi une sacrée astreinte, l'objectif étant d'apprendre et d'acquérir de nouveaux gestes.

Je change régulièrement l'aspect du site, parce qu'il s'agit de mon espace.
Je peux en modifier la forme au gré de mes envies ou de mes humeurs.
Pour finir cette visite de mon atelier voici à quoi il ressemble au moment où j'écris ces lignes (j'ai supprimer quelques notes pour la capture d'écran)&nbsp;:

![](/images/2020-atelier-capture.png)
