---
layout: post
title: "Mes outils"
date: "2019-09-21T10:00:00"
comments: true
published: true
description: "À une question simple, quels outils numériques j'utilise, j'ai tenté de répondre de façon pragmatique et synthétique. Ce billet est avant tout un marqueur à un moment précis dans mes pratiques d'apprenti chercheur."
categories:
- carnet
- phd
classement:
- phd
---
À une question simple, quels outils numériques j'utilise, j'ai tenté de répondre de façon pragmatique et synthétique.
Ce billet est avant tout un marqueur à un moment précis dans mes pratiques d'apprenti chercheur.

<!-- more -->

Pour circonscrire quelque peu cette question qui pour moi est immense, je me suis demandé quels processus, dispositifs et outils j'invoquais à chaque projet.
Pour être encore plus pragmatique, j'appelle _projet_ tout type de travail universitaire, que ce soit une communication ou un article pour donner deux exemples concrets.
Si, dans mon cas, les outils choisis sont éprouvés depuis plusieurs années, j'espère les remettre en cause et les faire évoluer au fil du temps.

_Ce billet est le premier d'une série accompagnant [mon doctorat](/phd). Depuis septembre 2019 je débute une thèse sous la direction de Marcello Vitali-Rosati dans le Département des littératures de langue française à l'Université de Montréal, je suis également intégré à la formidable équipe de la Chaire de recherche du Canada en écritures numériques. Le texte ci-dessous est la formalisation d'un exercice non formel dans le cadre du séminaire collectif de doctorat de [Benoît Melançon](http://oreilletendue.com/)._

Cette courte présentation est divisée en six parties :

1. [Écrire](#1-écrire)
2. [Enregistrer](#2-enregistrer)
3. [Référencer](#3-référencer)
4. [Communiquer](#4-communiquer)
5. [Naviguer](#5-naviguer)
6. [Résumé : liste des outils utilisés](#6-résumé--liste-des-outils-utilisés)

## 1. Écrire
Pour écrire je me passe de logiciel de traitement de texte, principalement pour une question de confort, dans mon cas.
Le traitement de texte m'impose une gestuelle fastidieuse, me place dans une dépendance non choisie, en plus de poser un problème de confusion entre la structuration d'un texte et sa mise en forme.
Il faut considérer LibreOffice Writer comme la duplication libre du trop connu Microsoft Word, la question n'est pas seulement celle de la liberté du logiciel, mais aussi [du process imposé](https://www.quaternum.net/2016/08/31/traitement-de-texte-multicanal/) par de tels outils.

J'utilise donc un langage de balisage léger, me permettant de structurer mon texte (niveaux de titre, listes, emphases, liens, images, références bibliographiques) : [Markdown](https://fr.wikipedia.org/wiki/Markdown).
Pour écrire _en Markdown_, j'utilise souvent un éditeur de code, [Atom](https://atom.io/) en l'occurrence, et parfois un éditeur de texte, souvent [Typora](https://typora.io/) ou [Zettlr](https://www.zettlr.com/) – ce dernier étant clairement orienté pratiques académiques.
Je ne m'étends pas sur l'usage de ces logiciels, leur configuration et leurs contraintes, mais je ne reviendrais en arrière pour rien au monde.

## 2. Enregistrer
L'enregistrement n'est pas un acte anodin, il ne s'agit pas simplement de trouver le lieu adéquat dans l'espace de mon ordinateur, mais aussi de pouvoir suivre l'enregistrement de mon ou mes textes.
M'inspirant des méthodes du développement web, j'utilise un système de versionnement.
Le versionnement consiste à suivre l'évolution de plusieurs fichiers : chaque session de travail est suivi d'un enregistrement d'un ou plusieurs fichiers, cet enregistrement est appelé un _commit_ et est accompagné d'un message explicatif.
Il est ainsi possible de garder trace de l'évolution d'un projet, voir de revenir en arrière en cas de problème.
Par ailleurs il est possible de travailler sur plusieurs _branches_ : par exemple pour tester une nouvelle direction dans un texte sans perdre d'autres pistes, le projet sera divisé en plusieurs branches qui pourront être fusionnées par la suite.

[Git](https://git-scm.com/book/fr/v2/) est le système de gestion de versions que j'utilise.
Git est actuellement le système de gestion de versions le plus utilisé dans le monde du développement informatique, et son usage est même détourné dans d'autres domaines – GitBook en est un exemple emblématique.
Pour en savoir plus sur Git il y a un livre de vulgarisation très accessible : [_Git for Humans_ de David Demaree](https://abookapart.com/products/git-for-humans).

Git peut être utilisé avec une plateforme, afin de centraliser les différentes contributions, ce qui apporte d'autres avantages : visualisation graphique plus facile d'accès, gestion de _tickets_ (ou _issues_) afin de définir des tâches à effectuer, partage de fichier, automatisation de déploiement (voir point 4).
[GitHub](https://github.com/) est la plus connue, mais il en existe d'autres comme [GitLab](https://gitlab.com/) – basée sur un logiciel libre qui connaît d'autres instances comme [Framagit](https://framagit.org/).

## 3. Référencer
Un travail universitaire implique de gérer des références bibliographiques (parfois beaucoup), et il serait dommage de tout faire _à la main_ : à la fois parce que cela est fastidieux (même si vous connaissez par cœur votre style bibliographique préféré), et aussi en raison d'une norme bibliographique qui pourrait vous être imposée (et modifiée au dernier moment), auquel cas il devient simple d'intégrer une liste de références.

[Zotero](https://www.zotero.org/) est un logiciel de gestion de références bibliographiques, la prise en main est rapide (quelques heures) et les avantages nombreux :

- gestion des références dans un logiciel (disponible pour tous les systèmes d'exploitation) : enregistrement, classement (dossiers, marqueurs, mots-clés) ;
- création de bibliographies avec les quelques milliers de styles/normes disponibles ;
- sauvegarde et partage, via un compte Zotero en ligne et une gestion fine de ce qui est public, privé ou partagé ;
- gestion de groupes, très pratiques pour travailler à plusieurs.

Zotero devrait être le compagnon indispensable de tout chercheur et apprenti chercheur.

## 4. Communiquer
Produire une communication ou un article implique de communiquer avec les partenaires et les commanditaires.
A minima il s'agit d'échanger par courriel, ce qui signifie définir une identité numérique (quelle adresse utilisée ?) et un lieu de regroupement des messages (un dossier).

Mais la collaboration peut parfois nécessiter d'autres _protocoles_, comme des communications synchrones avec une messagerie instantanée ou des commentaires dans un texte.
Dans ce dernier cas les plateformes utilisant Git proposent des fonctions d'annotation qui peuvent s'avérer utiles, d'autant que les commentaires peuvent être intégrées _dans_ projet sans interférer sur le texte lui-même ou sa mise en forme.

## 5. Naviguer
Pour accéder aux documents en cours d'écritures – les fichiers Markdown – et pour pouvoir manipuler Git, j'utilise un terminal.
Cet usage n'est pas obligatoire, ni pour naviguer dans des répertoires, ni pour Git, mais une fois que l'on a passé un court apprentissage quelque peu fastidieux, impossible de s'en passer.

Pour faciliter l'accès au répertoire du projet, je crée tout simplement un _alias_, grâce à lui je n'ai plus qu'à taper une courte commande qui me mènera au bon _endroit_.
Ce qui peut sembler être un détail facilite grandement les choses et ouvre la perspective d'un véritable _espace de travail_.

## 6. Résumé : liste des outils utilisés

- pour écrire : **Markdown** (langage de balisage léger) et **Atom** ou **Zettlr** (éditeur de texte/code) ;
- pour enregistrer : **Git** (système de versionnement) et **GitLab** ou **Framagit** (plateformes de code) ;
- pour référencer : **Zotero** (logiciel de gestion de références bibliographiques) ;
- pour communiquer : **courriel** (adresse et logiciel de messagerie) et **GitLab** ou **Framagit** (plateformes de code).

## Et le système d'exploitation ?
L'avantage des différents outils présentés ici est qu'ils sont tous disponibles sur n'importe quel système d'exploitation : Windows, Mac et évidemment Linux – puisqu'il s'agit de logiciels libres.
Et ils peuvent être l'occasion de choisir plus fortement cette voie du libre, et de libérer votre ordinateur.
