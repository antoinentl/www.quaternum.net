---
title: "Vim : éditer par déclarations"
date: 2022-02-20T09:17:31-05:00
categories:
- carnet
- phd
groupe:
- vim
tags:
accroche: "Vim est un éditeur de texte libre pour les machines sous Unix. Un outil d'écriture où le paratexte prend une importance déterminante."
published: true
bibfile: "data/analyses-vim.json"
---
Je teste Vim depuis une dizaine de jours, l'occasion de noter quelques observations qui me semblent importantes dans mes/nos pratiques d'écriture et d'édition.
Après avoir considérer que Vim est une [écriture dans l'écriture](/analyses/vim-lecriture-dans-lecriture/), Vim est aussi une pratique d'écriture par déclaration.

L'écriture dans l'écriture, que j'ai décrit naïvement comme un _paratexte_, est aussi une écriture par instructions.
Ces instructions sont des déclarations, a priori clairement formulées, qui déclenchent des actions : il se passe quelque chose quand j'écris `:w` — en l'occurrence j'écris dans le fichier, `w` signifiant `write`.
Vim fonctionne à la manière d'autres écritures programmatiques, comparable à LaTeX {{< cite guichard_lecriture_2008 >}} d'une certaine façon, mais en conservant une séparation stricte entre le contenu et mon intervention sur celui-ci.

Mon texte qui est _écrit_ dans un fichier est le résultat d'un autre texte, celui que je dois _taper_ dans Vim.
Sans les instructions écrites dans Vim, qui sont donc des déclarations, mon texte ne peut pas exister.
Les commandes suivantes permettent au texte que vous lisez d'être inscrit, enregistré, modifié, etc. Ce ne sont que quelques exemples de ces instructions dictées dans un langage compréhensible autant par la machine que par moi (je ne suis pas une machine) :

- `vim content/phd/2022-02-20-vim-editer-par-declaration.md` : pour ouvrir le fichier à l'origine de la page que vous consultez ;
- `:w` : pour enregistrer les modifications ;
- `CTRL+w` puis `t` : aller dans l'arborescence des fichiers et ouvrir un autre fichier dans un nouvel onglet (`t` pour `tab` qui veut dire onglet en anglais) ;
- `/déclaration` : pour chercher le nombre d'occurrences de ce terme dans mon fichier ;
- `gt` : pour changer d'onglet ;
- `:wq` : pour enregistrer et fermer mon fichier.

Plutôt que le paratexte évoqué précédemment, il faut envisager l'architexte : 

> Initialement défini comme une "écriture d'écriture" puis comme un "dispositif d'écriture écrit", l'_architexte_ s'avère être un point de passage obligé pour toute activité numérique. {{< cite souchier_numerique_2019 302 >}}

Je déclare des instructions pour modifier mon texte, pour interagir avec lui.
Ces commandes sont la condition d'existence de mon écriture — ici un fichier Markdown qui sera convertit en un fichier HTML lisible dans votre navigateur web.
Je pourrais même inscrire ces commandes dans un fichier pour les afficher en regard de mon texte finalisé, pour visualiser l'origine, la naissance de mon billet.

Quelles différences y a-t-il avec un traitement de texte ou un éditeur de texte comme Atom ou VSCode ?
D'une part ces instructions sont elles aussi du texte, que j'écris dans une interface (la même qui permet d'afficher le texte inscrit et les instructions) — ici le terminal, Vim fait partie de cette famille d'éditeur _dans_ un terminal.
D'autre part je n'ai pas recours à une interface graphique qui cache un certain nombre des rouages (pour le meilleur et pour le pire), non plus à une série de fonctions hiérarchisée dans des menus : je fais appel à des commandes qui sont documentées.

Ces commandes sont des déclarations, formulées avec des mots qui font partie de mon langage — certes en anglais.
Je rentre en communication avec l'outil d'écriture, j'apprends à parler une langue commune : je suis en situation d'apprentissage afin de maîtriser mon environnement d'écriture et d'édition.

{{< bibliography cited >}}
