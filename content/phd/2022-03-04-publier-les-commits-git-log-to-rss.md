---
title: "Publier les commits : git log to RSS"
date: 2022-03-04T20:17:31-05:00
categories:
- carnet
- phd
tags:
accroche: "Julien me demande comment les commits de mon atelier sont publiés automatiquement sur mon bot qtrnmnet, voici une explication."
published: true
---
Julien me demande comment les commits de mon atelier sont publiés automatiquement sur mon bot qtrnmnet ([Twitter](https://twitter.com/qtrnmnet)/[Mastodon](https://botsin.space/@qtrnmnet)), voici une explication.

## Un peu de contexte : c'est quoi ce bot ?
Je fais une thèse à l'Université de Montréal sur les processus de publication, sous la direction de Marcello Vitali-Rosati et de Michael E. Sinatra.
J'ai débuté cette expérience merveilleuse en septembre 2019, mais j'ai mis en place mon atelier de thèse en janvier 2020, [je m'en suis déjà expliqué](/2020/03/29/atelier-de-these/).
Avec cet atelier, qui est un espace non-public, j'expérimente aussi une entreprise pseudo-littéraire : les contenus de l'atelier sont versionnés avec Git, et à chaque intervention/enregistrement/action, j'effectue un _commit_ dont le texte est une phrase en lien avec le contenu édité.
Cette phrase est, elle, rendue publique : via le bot/robot précédemment cité, et de façon irrégulière sur mon carnet de recherche (carnet qui est public, à l'inverse de l'atelier, vous suivez ?), voir [ce dernier billet par exemple](/2022/01/15/apercu-02/).
Ces messages de commits forment des idées parfois floues, parfois percutantes, des propos décalés ou hors sujet, des bribes de mes recherches.

## Transformer des commits en flux RSS/Atom
Pour pouvoir publier ces messages de commits, il faut d'abord disposer d'un flux de contenu, et RSS ou Atom sont deux standards très proches qui répondent exactement à cet objectif.
J'ai donc dû trouver un moyen de transformer des commits en flux RSS, ce qui est en théorie faisable car les commits sont eux-mêmes déjà un flux de contenu.
Je me suis probablement inspiré d'une réponse sur Stack Overflow mais je ne retrouve pas ce qui pourrait correspondre à ma solution, la voici :

```
#!/bin/bash
                                               
echo '<?xml version="1.0" encoding="utf-8" standalone="yes"?><rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom"><channel><title>[titre] - log</title><link>[url]</link><description>Git Log</description><language>fr-fr</language><atom:link href="[url]/log.xml" rel="self" type="application/rss+xml" />' > log.xml
git log --date=rfc-local --format=format:"<item>%n <title>%h</title>%n <link>[url]/%h</link>%n <guid>[url]/%h</guid>%n <description>%s</description>%n <author>%ae (%an)</author>%n <pubDate>%ad</pubDate>%n</item>" >>log.xml
echo '</channel></rss>' >> log.xml
```

Avec ce script Bash je réalise 3 actions :

1. je crée l'entête du document XML, ici il faut remplacer `[url]` par l'URL du site web en question ;
2. je génère les entrées de chaque commit grâce à `git log`, une commande de Git très puissante ([voir la documentation](https://git-scm.com/docs/git-log)), en construisant mon flux RSS ;
3. j'écris la fin du fichier XML.

C'est assez simple et je ne dépends de rien d'autre que de Git et de Bash, ce qui sous Linux reste très minimaliste.

## Automatiser la diffusion sur Twitter/Mastodon
Il reste à diffuser ce flux sur [Twitter](https://twitter.com/qtrnmnet) et [Mastodon](https://botsin.space/@qtrnmnet), pour cela et vu que je suis assez faignant, j'utilise le service [IFTTT](https://ifttt.com/) et je crée une _recette_ RSS to Twitter.
Pour Mastodon j'utilise ce service me permettant créer automatiquement un pouêt à chaque tweet : [https://crossposter.masto.donte.com.br/](https://crossposter.masto.donte.com.br/).

Et voilà !
