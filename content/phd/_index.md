---
title: "Carnet de thèse (PhD)"
description: "Carnet de thèse."
---
Carnet de thèse qui regroupe des billets et articles en lien avec mon doctorat au Département de littératures et de langues du monde à l'Université de Montréal, débuté en 2019. Le sujet de la thèse, dirigée par Marcello Vitali-Rosati et co-dirigée par Michael E. Sinatra, est la reconfiguration des processus de publication et l'évolution des pratiques d'écriture et d'édition dans le champ littéraire.