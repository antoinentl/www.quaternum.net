---
layout: post
title: "Version : concept"
date: "2020-01-09T06:00:00"
comments: true
published: true
description: "Que désigne le concept de version dans le champ littéraire et dans une perspective d'étude des pratiques d'écriture ?"
categories:
- carnet
- phd
classement:
- phd
status:
bibfile: data/version-concept.json
---
Que désigne le concept de _version_ dans le champ littéraire et dans une perspective d'étude des pratiques d'écriture ?

<!-- more -->

Les acceptions courantes du mot « version » se rapportent à la manière de raconter un fait, aux enjeux de traduction ou aux états d'un texte.
Sans que cela soit conscient dans nos usages quotidiens, il demeure une forte dimension littéraire dans le concept de version ; il s'agit de récit et de texte.
Penser ce concept pourrait revenir à poser les questions suivantes concernant les lignes que vous lisez : existe-il une déclinaison anglaise ?
Son brouillon est-il disponible ?
Pouvons-nous avoir accès à d'autres variantes de ce texte ?
Notre analyse se compose tout d'abord d'une définition du terme "version" concernant l'état et la disponibilité du texte, puis nous abordons son ou ses existences dans une dimension d'archive, enfin la stabilité sur la question du travail autour du texte.
Nous inscrivons cette réflexion dans une approche littéraire avec l'édition et l'étude littéraire, et dans une approche numérique.

Dans le champ littéraire, le concept de version s'applique à l'état du texte à des fins d'étude et de comparaison.
L'étude littéraire est consacrée à un document dont la version comporte une forte importance.
L'édition qui est examinée doit être spécifiée, des écarts peuvent exister entre celle-ci et une autre.
Une révision de l'auteur ou de l'éditeur peut en être la cause – ces intervalles portent sur le contenu, mais la forme peut également être concernée.
Les différences peuvent être le reflet d'une évolution intellectuelle ou artistique de l'auteur, la marque d'une censure, ou révéler un choix de l'éditeur – par exemple le passage d'une édition grand format à une anthologie.
La version est alors fixée par l'acte de publication, point de départ d'un processus éditorial complexe allant de l'auteur à la bibliothèque en passant par la maison d'édition et la librairie.
L'éditeur décide de figer l'état du texte en publiant l'ouvrage, en l'inscrivant dans une dimension économique et documentaire : en plus d'être commercialisé, le livre est enregistré dans une bibliothèque nationale via le dépôt légal – nous prenons ici les cas francophones de la France et du Québec.
Pour la _version_ imprimée ce processus est physique et aujourd'hui difficilement modifiable.
Un texte disponible en librairie, acheté par plusieurs milliers de personnes et conservé dans plusieurs bibliothèques ne pourra pas être remplacé ou supprimé facilement.
Notons qu'à une époque où seules quelques versions d'un texte étaient conservées, sa disparition totale était envisageable {{< cite "chartier_inscrire_2005" >}}.

Comparer deux versions d'un texte nécessite des protocoles et des outils ; nous pouvons évoquer le travail de Jerome McGann qui a dirigé un projet de logiciel {{< cite "davis_mcgann_2018" >}} pour confronter deux variantes d'un même texte.
Le numérique propose des outils de travail sur plusieurs versions d'une même œuvre, ce qui représente à la fois un gain de temps considérable et un nouvel horizon.
Avec l'avènement du numérique, plusieurs questions, déjà présentes avec l'existence imprimée de textes, se cristallisent autour de ce concept.
Reprenons notre exemple d'une tentative de suppression ou de modification de la version d'un ouvrage.
Un fichier numérique est consigné sur des serveurs matériels, mais dont la mémoire peut être aisément modifiée.
En 2009 Amazon avait prouvé cela de façon inquiétante : deux ouvrages de Georges Orwell, _1984_ et _La ferme des animaux_, avaient été effacés des liseuses de nombreux lecteurs {{< cite "stone_amazon_2009" >}}.
Les versions numériques commercialisées par Amazon de ces deux ouvrages n'existaient tout simplement plus.
De la question de l'accès, nous passons à celle de l'existence.
En écho avec cette thématique, Alessandro Ludovico aborde la pérennité et l'archivage {{< cite "ludovico_post-digital_2016" 143 >}}, qui sont fortement liés au concept de version, en posant la question de qui en est chargé et à quelles fins.
Sans nous éloigner de notre objet d'étude, nous abordons ici la problématique des acteurs qui ont la mission ou la maîtrise des moyens nécessaires à la conservation de différentes versions de textes.
Amazon ou Google peuvent décider de ne plus permettre l'accès à un document, voire de supprimer toute trace de son existence.
Imaginons un scénario encore plus dramatique : ces géants du numérique souhaitant disposer d'une place hégémonique, certains textes pourraient être réduits à n'avoir plus qu'une seule version.

En parallèle de cette question de pérennité se trouve celle de stabilité.
L'existence imprimée d'ouvrages impose une temporalité longue, au moins avec certaines techniques d'impression qui contraignent à des tirages importants – et donc à un investissement, à un stockage et à une diffusion en conséquence –, contraintes remises en cause ces vingt dernières années avec des procédés comme l'impression numérique et l'impression à la demande {{< cite "andre_compte_2009" >}}.
Le numérique accélère fortement le processus de fabrication et de diffusion d'un livre, qu'il soit numérique ou imprimé.
Si une version différente peut être fabriquée _à la demande_, pour chaque lecteur d'un livre, le travail de génétique du texte devient vertigineux – ou passionnant.
Qu'en est-il des fichiers numériques qui peuvent être mis à jour en quelques heures sur plusieurs dizaines de plateformes différentes à travers le monde ?

> La permanence du texte est désormais chose du passé.  
> {{< cite "vandendorpe_du_1999" 181 >}}

Le numérique ne fait qu'accentuer un phénomène déjà observable avant son apparition.
Dans le domaine de la publication scientifique, cela se traduit par un problème majeur : comment citer un article scientifique qui peut être modifié à tout moment ?
Si la majorité des moyens de diffusion fige un état d'un _papier_, il existe des revues qui choisissent d'embrasser le flux numérique {{< cite "fauchie_les_2020" >}}.
La question de la réinscriptibilité est présente depuis l'arrivée du livre numérique au début des années 2000 {{< cite "dacos_read-write_2009" >}}, Wikipédia étant le spécimen emblématique en la matière.
Chaque article de l'encyclopédie libre est _versionné_, l'historique est mis à disposition ainsi qu'un outil de comparaison pour visualiser les différences d'un état à un autre, ces deux dispositifs étant inspirés du monde de l'informatique.

Quittons un moment la littérature.
Nous avons omis une signification du mot version, cette fois dans le domaine de l'informatique.
Nous sommes encore dans le vaste champ du texte, mais un texte pour le moins spécifique : le code.
Les programmes sont écrits avec du texte, rédigé pour d'autres programmes via des instructions qui prennent la forme de langages spécifiques.
Un logiciel est composé de code, et défini selon un numéro de version, incrémental.
Composé de plusieurs chiffres séparés par des points et parfois précédés d'un "V" – par exemple 5.0.80 –, ce numéro de version implique une temporalité, les versions se suivant dans le temps.
Pour permettre aux développeurs de travailler ensemble sur ces textes bien particuliers, la gestion de versions est devenue une nécessité.
Les fichiers qui composent un programme sont versionnés, chaque intervention – une série de modifications – est identifiée, le projet est ainsi documenté.
En plus de créer une série d'états, un système de gestion de version comme Git {{< cite "demaree_git_2017" >}} crée des versions parallèles du projet – des _branches_ – qui peuvent ensuite fusionner avec la version principale de travail.
Les états du texte et le type de modification qu'il a subi sont clairement désignés.

Le terme de « version » a une consonance toute littéraire, jusque dans le domaine de l'informatique.
Le numérique apporte des questionnements sur la manière d'aborder les versions d'un livre ou de tout autre support de la littérature – au sens large.
Cette complexité induite par le numérique, présente dès l'apparition de l'écriture, est aussi une occasion de repenser notre rapport au concept de version.
Nous sommes désormais placés dans un environnement non linéaire, dans lequel des moyens comme le versionnement apportent quelques clés de compréhension et de maîtrise de cette matière textuelle en mouvement.

## Bibliographie

{{< bibliography >}}

---

_Définition établie dans le cadre du séminaire collectif de doctorat de Benoît Melançon, Département des littératures de langue française, Universion de Montréal, janvier 2020._
