---
title: "Fabriques de publication : Hugo"
date: 2021-08-06T08:30:00-04:00
description: "Dixième analyse d'une série sur les fabriques de publication : Hugo."
categories:
- carnet
- phd
classement:
- phd
similar: hugo
bibfile: data/analyses-hugo.json
---
Dixième analyse d'une série sur les fabriques de publication : Hugo.

[Introduction et liste des fabriques](/fabriques)

Hugo est un générateur de site statique, un outil pour fabriquer et produire des sites web, et dont l'une des particularités est sa rapidité.

## Description
Un générateur de site statique est un programme informatique qui convertit des fichiers et les organise en site web.
La conversion se fait le plus souvent depuis le format de balisage léger Markdown vers le format HTML, et l'organisation repose sur plusieurs fichiers de configuration : paramètres généraux du site web, modèles de page, gestion de données, etc.
Les générateurs de site statique s'utilisent majoritairement via une interface en ligne de commande, et peuvent aussi être intégrés dans un système de déploiement continu.
[Hugo](https://gohugo.io/) est un programme écrit en Go, Go est un langage de programmation rapide et puissant.

J'ai déjà présenté un générateur de site statique emblématique, [Jekyll](/2020/05/05/fabriques-de-publication-jekyll/), dans le cadre de cette série sur les fabriques de publication.
Alors pourquoi faire une analyse d'un nouveau _static site generator_ en sachant qu'il en existe [des centaines](https://jamstack.org/generators/) ?
Hugo a plusieurs particularités qui en font un outil d'écriture et de publication intéressant, j'en liste quatre principales :

1. l'absence de dépendances : l'installation de Hugo se fait via un seul fichier binaire, aucun autre programme n'est nécessaire. Cela signifie que l'installation du langage Go n'est pas requis (contrairement à Jekyll qui a besoin du langage Ruby), et que Hugo ne dépend d'aucune dépendance en dehors de ce fichier binaire (contrairement au générateur de site statique Eleventy, dans l'environnement JavaScript) ;
2. la rapidité de génération : Hugo met moins d'une seconde pour générer un site complexe — c'est-à-dire avec des modèles de pages avancés et une gestion de données qui dépasse le simple blog — de plusieurs centaines de pages ;
3. la multimodalité : Hugo est pensé pour convertir une même source en plusieurs formats de sortie. Par exemple plusieurs versions HTML différentes avec des modèles de page spécifiques, ou d'autres formats comme XML, TXT, JSON ou même les formats PDF ou [EPUB](https://github.com/weitblick/epub) via des étapes intermédiaires ;
4. l'injection de codes personnalisés : Hugo, comme beaucoup d'autres générateurs de site statique, intègre un système de blocs sémantiques personnalisés. En d'autres termes il est possible de faciliter l'introduction de balises HTML sans avoir à écrire du code HTML _dans_ les documents sources, permettant une structuration sémantique plus précise. Dans le cas d'Hugo cela s'appelle des _shortcodes_, et le fonctionnement est assez ingénieux.

Hugo a la particularité d'être rigide, c'est un outil qui oblige à une certaine rigueur, rigueur qui peut devenir un frein ou une lourdeur de travail.
Cette rigidité est nécessaire pour avoir de bonnes performances et pour disposer de fonctionnalités comme celles présentées ci-dessus.
Et, d'une certaine façon, c'est une série de contraintes qui peuvent susciter des opportunités ou des espaces de création enthousiasmants.

Malgré l'apparition de générateurs de site statique plus flexibles et plus extensibles, Hugo reste un programme très utilisé avec une communauté très active — voir par exemple [le forum dédié](https://discourse.gohugo.io/).
[La documentation](https://gohugo.io/documentation/) est très riche, mais manque cruellement d'exemples ou de cas d'usage, limitant les possibilités pour les personnes qui n'ont pas un profil dans le développement informatique.
Comme beaucoup de générateurs de sites statiques, Hugo peut être connecté à un gestionnaire de contenus, via des API notamment basées sur Git, sa rapidité laissant la possibilité d'activer facilement des possibilités de prévisualisation comme nous le verrons plus loin.

## Histoire
Hugo a été créé par Steve Francia en 2013, en réaction aux générateurs de site statique alors jugés trop lents, comme Jekyll.
Rapidement Bjørn Erik Pedersen, alors contributeur très régulier, devient le développeur principal, toujours accompagné d'une communauté active de développeurs — à ma connaissance ce ne sont que des hommes.

Dès le début l'objectif de Hugo est d'être rapide, il faut comprendre par là un temps de génération très court pour _produire_ un site web depuis des fichiers Markdown (pour les contenus), YAML ou TOML (pour la configuration), HTML (pour les _templates_), etc., vers des fichiers HTML.
Le langage Go n'est pas la seule raison de sa rapidité, des choix inhérents au développement du programme permettent cette performance, choix que je suis bien incapable d'expliquer.
De nombreux sites comportant beaucoup de contenus utilisent Hugo pour cette raison, une liste est notamment disponible sur le site de ressources [Awesome Hugo](https://www.awesome-hugo.dev/#projects-using-hugo).

Hugo est d'abord très bien accueilli par la petite communauté des développeurs intéressés par le mouvement JAMStack {{< cite "markovic_understanding_2021" >}} puis critiqué pour sa rigidité et son manque de connectivité à d'autres environnements ou langages.
Parmi les nombreuses discussions et débats au sein de la communauté de Hugo, nous pouvons retenir [cette discussion](https://discourse.gohugo.io/t/has-hugo-become-too-complex/29609) qui aborde la complexité de Hugo, avec l'enjeu de développer un outil offrant une rétrocompatibilité.
En creux il y a la question de l'évolution et du maintien des logiciels libres {{< cite eghbal_sur_2017 >}}, Hugo ne bénéficie pas d'un soutien financier pour le développement, les choix opérés par Bjørn Erik Pedersen sont à prendre ou à laisser.
Cela a permis à un autre générateur de site statique d'émerger, [Zola](https://www.getzola.org/), en raison — selon le créateur de Zola — de cette complexité {{< cite sharma_migrating_2020 >}}.
Quoi qu'il en soit ce générateur de site statique connaît un développement toujours très actif.

## Spécimens
Nombreux sont les sites web utilisant Hugo, que ce soit des bases de connaissance, des sites institutionnels, des documentations, des publications ou des blogs.
[Quelques références emblématiques](https://gohugo.io/showcase/) sont présentées sur le site de Hugo.
Deux exemples m'intéressent plus particulièrement ici : un livre numérique et une chaîne d'édition.

_Le Novendécaméron_ {{< cite vallee_novendecameron_2021 >}} est un ouvrage collectif publié pendant le printemps 2021, c'est un recueil de textes écrits durant le début de la pandémie en 2020.
C'est un projet auquel je participe, aux côtés de [Louis-Olivier Brassard](https://www.loupbrun.ca/) (je ne suis donc pas objectif).
L'utilisation de Hugo se justifie pour plusieurs raisons :

- l'avantage de travailler avec un générateur de site statique : puisque le résultat est un ensemble de fichiers faciles à déplacer, sans base de données ;
- la portabilité du programme : le fait de pouvoir générer la publication dans plusieurs environnements différents. En effet en plus de sa rapidité Hugo offre la possibilité de configurer des environnements différents, en conservant une seule et même source. Par exemple certains contenus en mode brouillon ne sont générés que pour une version de développement, invisibles dans une version de production publique. Ce procédé a été très utile puisque la publication s'est faite de façon successive au rythme de deux livraisons par semaine ;
- la génération de formats multiples : à partir d'une même source plusieurs fichiers sont produits, par exemple deux versions HTML différentes, utilisant deux modèles distincts — l'une faisant appel à un script [Paged.js](http://pagedjs.org/) permettant l'impression d'une page web ;
- les _shortcodes_ ont permis d'intégrer facilement des spécificités pour chaque texte, tout en rendant lisible le balisage pour l'éditeur et l'éditrice et en anticipant des scénarios différents selon les formats de sortie (un bloc permettant de faire appel à un fichier son MP3 est remplacé par un message dans la version imprimable).

Deuxième spécimen : [Quire](https://quire.getty.edu), une chaîne de publication créée par Getty Publications.
Quire est une chaîne de publication basée sur Hugo, permettant de faire de la publication multimodale, c'est-à-dire produire plusieurs artefacts différents à partir d'une même source.
Ici ce sont des formats HTML, PDF, EPUB ou CSV qui sont générés à partir de fichiers aux formats Markdown, YAML ou JSON.
Cette chaîne de publication génère plus spécifiquement des catalogues, c'est un système de publication pour les musées — voir [cette page pour quelques exemples](https://quire.getty.edu/community/community-showcase/).
Pourquoi recourir à Hugo dans ce cas ?
En raison de la puissance et de la rapidité de l'outil, mais aussi pour certaines fonctionnalités comme les _shortcodes_ qui permettent d'injecter facilement des blocs sémantiques.
Quire est en fait un ensemble de scripts qui orchestrent Hugo et d'autres logiciels, comme Prince XML pour la conversion de pages HTML en fichiers PDF paginés.
Je n'en dis pas plus sur Quire, plusieurs textes en parlent déjà {{< cite fauchie_les_2020 >}}, et il y a de [nombreuses ressources sur le site dédié](https://quire.getty.edu/learn/).

## Critique
Dans le domaine du développement web, Hugo se démarque par sa vitesse.
Il présente aussi d'autres atouts comme ceux évoqués précédemment, je reviens sur plusieurs d'entre eux en tentant de les lier à quelques concepts théoriques.

L'injection de structures sémantiques complexes par le biais de _[shortcodes](https://gohugo.io/content-management/shortcodes/)_ est un excellent compromis pour _étendre_ le balisage de Markdown.
Par exemple le balisage `{{</* image source="/chemin/fichier.jpg" legende="Figure 1 - Une image" description="Cette image représente quelque chose" */>}}` peut devenir un bloc HTML comme celui-ci :

```
<figure>
<img src="chemin/fichier.jpg" alt="Cette image représente quelque chose" />
<figcaption>
    Figure 1 - Une image
</figcaption>
</figure>
```

Un _shortcode_ est un _template_ ou modèle HTML dans lequel des données sont injectées.
Il est possible d'écrire n'importe quel type de bloc sémantique, en utilisant le système d'appel de variables de Hugo.
Voici par exemple à quoi ressemble la configuration du _shortcode_ précédent :

```
<figure>
<img src="{{ with .Get "source" }}{{ . }}{{ end }}" alt="{{ with .Get "description" }}{{ . }}{{ end }}" />
{{- with .Get "legende" }}
<figcaption>
    {{ . | markdownify }}
</figcaption>
{{ end -}}
</figure>
```

Ce n'est pas nouveau et d'autres outils permettent cela, que ce soit des générateurs de site statique comme Jekyll ou des convertisseurs comme Pandoc.
Mais la simplicité du système adopté par Hugo, couplée avec des fonctions de programmation, en fait une bonne alternative à des systèmes de balisage complexe comme certains schémas XML.
Il est possible d'introduire une sémantisation plus fine, tout en restant dans un environnement compréhensible et compatible avec de nombreux éditeurs de texte.
La question se pose de la dépendance à ce _langage_, mélange de Markdown et de balises propres à Hugo.
Est-ce encore du texte {{< cite derose_what_1990 >}} ?

Le fait de produire plusieurs formats de sortie à partir d'une même source, et sans avoir à tordre l'outil, est un avantage dans le domaine de l'édition numérique.
C'est même une prérogative pour qui s'inscrit dans une démarche d'édition résolument numérique {{< cite epron_ledition_2018 >}}.
Cette fonctionnalité est activable facilement, il suffit ensuite de créer les modèles correspondant, quel que soit le format de sortie — même si ces formats sont tout de même [limités](https://gohugo.io/templates/output-formats/).
HTML, XML, JSON, CSV, YAML, TXT : autant de formats utiles dans le domaine de l'édition numérique, et dont certains peuvent être convertis dans d'autres formats — notamment PDF via l'utilisation de Prince XML ou de Paged.js.
Cette fonctionnalité peut être associée aux _shortcodes_ : un _shortcode_ peut être décliné pour chaque format de sortie.
En effet il est possible de prévoir un comportement différent à la fois pour un contenu en fonction du _template_ associé au format de sortie, mais également pour les _blocs sémantiques_ à l'intérieur de ces pages.
Dans le cas d'une version HTML web et d'une version XML, un bloc de contenus pourra se comporter différemment, par exemple en remplaçant un bloc embarquant une vidéo par sa légende dans la version XML.

Comme d'autres générateurs de site statique, Hugo est un outil accessible pour des personnes qui souhaitent intervenir à plusieurs étapes de la conception d'une publication numérique sans pour autant devoir maîtriser la programmation.
Même si la documentation n'est pas facile à lire, la prise en main de Hugo est possible pour qui dispose de quelques rudiments de HTML et de quelques orientations pour savoir par où commencer {{< cite soueidan_migrating_2017 >}}.
Cette question de la maîtrise des outils de production numérique est désormais primordiale.
Hugo serait, d'une certaine façon, un outil pertinent dans le cadre de l'approche des _Software Studies_, qui privilégie autant l'étude que la pratique des dispositifs numériques {{< cite manovich_logiciel_2017 >}}.

La relative accessibilité de Hugo, associée à sa portabilité et à sa rapidité, en fait un outil de prototypage puissant.
C'est-à-dire qu'avec Hugo il est possible de concevoir, fabriquer et déployer {{< cite fauchie_deployer_2021 >}} un objet numérique très rapidement.
La vitesse de génération à chaque changement permet de travailler avec un éditeur de texte d'un côté (pour écrire et structurer), et un navigateur de l'autre (pour prévisualiser le résultat).
Cela peut sembler être un détail, mais ne pas avoir à _attendre_ qu'un prototype d'une publication s'affiche après une modification constitue un atout dans une méthode de travail.
Et cela vaut également pour la génération de prévisualisations sur une machine autre que la nôtre : la génération étant rapide, la consommation de ressources est d'autant plus faible.

Si Hugo semble être une fabrique de publication pertinente pour des projets d'édition numérique, il faut conserver un regard critique.
Les problèmes potentiels sont réels : tout d'abord la rigidité de Hugo déjà mentionnée, rigidité qui va de pair avec une documentation souvent ardue à comprendre.
Ensuite le développeur principal mentionne très clairement qu'il dirige les évolutions selon ses besoins, même s'il prend en compte beaucoup des suggestions proposées par la communauté — et qu'il répond à certaines remarques agressives avec une patience incroyable.
La direction prise suit une logique, le projet reste cohérent (ce qui est assez fou après des années de développement) et on peut penser à certains débats au sein de la communauté comme le fait de gérer ou non le format SASS, et ainsi de transformer du SCSS en CSS — ce qui explique qu'il y a désormais deux versions de Hugo, une _étendue_ avec le support de SASS et l'autre non.

Enfin, la portabilité de Hugo — le fait de n'avoir besoin que d'un fichier binaire quelque soit le système d'exploitation, sans autres dépendances — en ferait une fabrique monolithique, un outil qui ne peut pas évoluer à moins de modifier le code même du programme.
Monolithique est un terme qu'il faut utiliser avec précaution mais qu'il est intéressant d'analyser plus en détail ici.
Effectivement Hugo rassemble en un seul _bloc_ toutes ses fonctionnalités, mais cela signifie-t-il que Hugo ne peut pas être _étendu_, comme c'est le cas par exemple avec Jekyll et ses extensions écrites en Ruby ?
Ce n'est pas parce que Hugo est formé d'un seul tenant qu'il ne peut pas être _modulaire_, la fonctionnalité [Hugo Modules](https://gohugo.io/hugo-modules/) est pensée pour rendre Hugo plus souple et ouvert.
Les thèmes, ces modèles de site web permettant de personnaliser la structure et le rendu graphique d'un site produit avec Hugo, peuvent d'ailleurs être installés en tant que module.
Deux exemples intéressants viennent étayer cette perspective de modularité : un mode d'affichage des références bibliographiques avec [Hugo Cite par Louis-Olivier Brassard](https://labs.loupbrun.ca/hugo-cite/) construit comme un thème, et un outil de corrections typographiques avec [Hugo Microtypo](https://github.com/jygastaud/hugo-microtypo) de Jean-Yves Gastaud.
Aussi, pour terminer sur cette réflexion, il faut prendre en compte que Hugo peut s'inscrire dans un processus de publication plus large, et à ce titre Hugo intègre déjà d'autres outils (Regex, SASS, etc.), et surtout il est possible d'intégrer Hugo à d'autres processus — [Quire](https://quire.getty.edu/) en est un bon exemple.

## Vers d'autres fabriques
Difficile d'analyser Hugo sans le comparer à d'autres générateurs de site statique comme [Jekyll](/2020/05/05/fabriques-de-publication-jekyll/), [Eleventy](https://www.11ty.dev/) ou même le plus récent [Astro](https://astro.build/).
On peut envisager que ces différentes fabriques s'influencent entre elles.
Hugo, avec sa rapidité de génération et quelques fonctionnalités originales, poussent sans doute d'autres programmes à évoluer.
Il est aussi pertinent de regarder du côté de [Pandoc](/2020/04/30/fabriques-de-publication-pandoc/) en tant que convertisseur qui peut se transformer en _static site generator_ [moyennant quelques scripts](https://ybad.name/log/2021-01-27.html).

Quire, déjà mentionné plus haut, est un bon exemple de détournement ou plutôt d'utilisation de Hugo pour faciliter nombre de manipulations dans le domaine de l'édition.

## Références
{{< bibliography >}}