---
layout: post
title: "Fabriques de publication : LaTeX"
date: "2020-04-29T22:00:00"
comments: true
published: true
description: "Première analyse d'une série sur les fabriques de publication : LaTeX."
categories:
- carnet
- phd
classement:
- phd
status: brouillon
bibfile: data/analyses-latex.json
---
Première analyse d'une série sur les fabriques de publication&nbsp;: LaTeX.

<!-- more -->

[Introduction et liste des fabriques](/fabriques)

LaTeX est un langage de balisage et un système de composition qui met en forme des documents.
LaTeX est l'une des premières tentatives de séparation du fond et de la forme, ainsi qu'un outil de composition typographique informatique.
Respectant des normes typographiques strictes tout en étant basé sur des fichiers en texte brut et des commandes, LaTeX est un puissant système de programmation éditoriale {{< cite "guichard_lecriture_2008" >}}.

## Description
Dans LaTeX il faut distinguer le langage de balisage, le système de composition et la _distribution_.

Le langage de balisage permet de structurer un texte, dans l'objectif de générer un document mis en page – principalement au format PDF, imprimable.
Ce langage de balisage comprend des commandes, parfois complexes, qui permettent de gérer de nombreux détails typographiques.
À ce titre il ne s'agit pas d'un balisage de structuration uniquement sémantique, LaTeX est à cette intersection entre l'imprimé et le numérique – le balisage est converti en un rendu graphique.
Il faut donc un système de composition pour assurer cette conversion, il s'agit du Tex de LaTeX.
Enfin LaTeX requiert une distribution TeX, c'est-à-dire un programme complexe qui comprend le _compilateur_ LaTeX ainsi que des _paquets_ – des modules qui ont une fonction spécifique.

LaTeX est souvent connu pour sa formidable gestion des formules mathématiques, ce qui explique son usage dans les domaines de l'informatique ou des mathématiques – que ce soit pour des articles, des thèses ou parfois des livres.
Quoique certains chercheurs en sciences humaines l'utilisent également.
La communauté LaTeX est très importante, ses utilisatrices et utilisateurs sont nombreux, les sites d'aide et d'échange également.

L'usage de LaTeX demande un apprentissage, tant en ce qui concerne le balisage que les commandes pour générer les formats de sortie.
Le balisage est à la fois verbeux et peu lisible pour un œil non habitué.
En revanche, une fois un certain apprentissage atteint, LaTeX est d'une efficacité redoutable.
Le fait qu'il soit toujours possible d'apprendre de nouvelles méthodes avec LaTeX, même après des années de pratique, explique peut-être aussi l'addiction de ses adeptes.

En pratique l'utilisation de LaTeX se fait en plusieurs étapes&nbsp;:

- la rédaction via un éditeur de texte ou de code, chaque balisage est écrit en texte brut, de façon lisible pour un humain ou un programme&nbsp;;
- ce code source est ensuite compilé par LaTeX pour produire un fichier au format PDF (moyennant un passage par d'autres formats intermédiaires)&nbsp;;
- pour modifier le document il faut modifier le code source.

LaTeX nécessitant un temps de prise en main (même si la courbe d'apprentissage est assez rapide), des services en ligne sont apparus pour faciliter la rédaction et la génération des formats de sortie.
En plus de proposer un environnement de travail _nuagique_ pour LaTeX, des services comme [Overleaf](https://www.overleaf.com/) résolvent du même coup les problèmes de compatibilité (question de la distribution LaTeX choisie) et permettent de faire de l'édition collaborative, y compris en temps réel.

## Histoire
L'histoire de LaTeX est intéressante et a influencé nombre de personnes intéressées par les questions de typographie, de livre, de publication et d'informatique.
TeX a été créé par Donald Knuth afin de palier à des défauts de mise en forme d'un de ses livres, à la fin des années 1970.
Regrettant la pauvreté de la qualité de la composition typographique de la deuxième édition du second volume de son ouvrage _The Art of Computer Programming_, il se met en tête de créer un système informatique d'édition de document.
Précision importante&nbsp;: nous sommes à la fin des années 1970, les systèmes de composition et d'impression photographiques remplacent petit à petit les systèmes d'impression mécanique par pression.
Ce qui devait prendre quelques mois à Donald Knuth lui prendra finalement plusieurs années, déclenchant au passage le beau projet Metafont {{< cite "vallance_decrire_2015" >}}, de la typographie générative.

Dans les années 1980 Leslie Lamport crée LaTeX (d'où le "La" de LaTeX) pour faciliter l'utilisation de TeX, il s'agit d'un ensemble de macro-commandes.
Sans Leslie Lamport l'utilisation de TeX serait délicate voir impossible.

## Specimen
Difficile de donner un exemple particulier de document réalisé avec LaTeX, tant le nombre de specimen est gigantesque – une bonne part des articles scientifiques au format PDF dans le domaine des mathématiques, de la physique, de l'informatique, etc.

## Critique
LaTeX est un système de composition de documents basé sur un langage de balisage spécifique.
L'objectif de LaTeX est de produire des documents respectant une exigence typographique.
Parfois LaTeX est utilisé à d'autres fins, notamment pour générer des fichiers HTML (parfois avec l'aide de Pandoc, un convertisseur), ce qui est une erreur.
LaTeX, en tant que format de balisage, ne peut pas être un format pivot – par format pivot j'entends un intermédiaire pérenne pour produire différents types de format.
Aujourd'hui LaTeX permet de fabriquer un format PDF, et il le fait très bien.

Par ailleurs LaTeX ne sépare pas totalement fond et forme – ou structure et mise en forme.
Pour réaliser certaines opérations de composition, des commandes doivent être intégrées _dans_ le fichier.
Nous pouvons prendre le contre-exemple de HTML pour préciser ce point&nbsp;: le balisage HTML structure un texte sans indication de mise en forme&nbsp;; une balise HTML peut comprendre un attribut et une valeur qui renvoient à une feuille de styles qui dicte la composition du document&nbsp;; structure et mise en forme sont distincts.
Avec LaTeX, ce qui ressemble parfois à du balisage sémantique est la plupart du temps une indication de composition graphique.

LaTeX est utilisé dans de nombreuses chaînes de publication dans lesquelles c'est l'élément qui permet de produire un fichier PDF convenablement composé, LaTeX devient ainsi en quelque sorte un intermédiaire – en tant que format de balisage et comme programme.
Des outils comme Prince XML ou paged.js ont un fonctionnement similaire à LaTeX (je suis en train de grossir à très très gros traits)&nbsp;: transformer un fichier balisé en un document PDF – au détail prêt que le balisage n'est pas du LaTeX.

Il faut noter que LaTeX est conçu et maintenu de telle façon qu'il est particulièrement robuste, à tel point que cette stabilité est préférée au développement de nouvelles fonctionnalités.

## Vers d'autres fabriques
LaTeX a influencé énormément d'entreprises de publication, directement ou indirectement.
Cette idée (géniale) de transformer des instructions balisées en composition graphique grâce à un ensemble de programmes est à la base de nombreux systèmes de publication que nous explorerons par la suite.
En 2014 j'avais écrit [un billet maladroit à ce sujet](https://www.quaternum.net/2014/04/26/latex-et-jekyll/).

L'apparition de langages de balisage _léger_ est une réaction à la complexité de LaTeX, Markdown et AsciiDoc en sont des bons exemples.
Il nous faut toutefois finir en insistant sur un point important, LaTeX fait très bien ce pour quoi il a été pensé&nbsp;: composer des documents.
C'est pourquoi nous retrouvons LaTeX comme format et comme processus intermédiaire, et non pas format pivot.

## Références

{{< bibliography >}}
