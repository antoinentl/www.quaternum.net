---
title: "Pour un partage de nos historiques de commandes"
date: 2023-04-04
categories:
- carnet
- phd
accroche: "Mon expérience d'écriture avec Vim continue — et d'ailleurs je comprends que la dimension expérience ne va pas s'arrêter. Je m'interroge cette fois sur le fait qu'avec cet éditeur de texte j'apprends à maîtriser mon environnement, un environnement d'écriture, d'édition et de pensée."
published: false
bibfile: "data/historiques-commandes.json"
---
À l'occasion d'une communication sur les actes d'édition (en ligne temporairement [ici](https://txt.quaternum.net/ma)), une discussion à émerger pendant le colloque et après, autour du partage de nos historiques de commandes textuelles.
Voici une ébauche des problématiques autour de cette question.


## Qu'est-ce qu'un historique de commandes ?

Lorsque vous utilisez un terminal, ou plus spécifiquement un interpréteur en ligne de commande, comme Bash ou zsh, chaque commande est enregistrée dans un historique.
Souvent sous la simple forme d'un fichier texte (comme `.bash_history` ou `.zsh_history`), cet historique est bien pratique pour retrouver une ancienne commande (par exemple une commande [Pandoc]() avec de nombreux paramètres).
Il existe même des outils pour chercher dans cet historique, comme `CTRL + R` sur un shell Unix (un terminal en langage commun).

Voici quelques lignes de mon historique :

```
: 1680555752:0;cd .vim
: 1680555805:0;vim LICENSE
: 1680555847:0;vim .gitignore
: 1680555903:0;git add LICENSE .gitignore && git commit -m"admin: legal information"
: 1680555917:0;vim vimrc
: 1680555965:0;git add vimrc && git commit -m"edit: Grammalecte, goyo and disable NERDTree"
: 1680555968:0;git push
: 1680556522:0;git add content/phd/2023-04-03-vim-quelques-commandes-pour-ecrire-et-editer.md && git commit -m"edit: quelques commandes Vim"
: 1680556817:0;vim content/phd/2023-04-04-pour-un-partage-de-nos-historiques-de-commande.md
: 1680557922:0;vim .zsh_history
```

Et sous une forme plus lisible :

```
10105  2023-04-03 17:02  cd .vim
10106  2023-04-03 17:03  vim LICENSE
10107  2023-04-03 17:04  vim .gitignore
10108  2023-04-03 17:05  git add LICENSE .gitignore && git commit -m"admin: legal information"
10109  2023-04-03 17:05  vim vimrc
10110  2023-04-03 17:06  git add vimrc && git commit -m"edit: Grammalecte, goyo and disable NERDTree"
10111  2023-04-03 17:06  git push
10112  2023-04-03 17:15  git add content/phd/2023-04-03-vim-quelques-commandes-pour-ecrire-et-editer.md && git commit -m"edit: quelques commandes Vim"
10113  2023-04-03 17:20  vim content/phd/2023-04-04-pour-un-partage-de-nos-historiques-de-commande.md
10114  2023-04-03 17:38  vim .zsh_history
```

(Oui 10114 c'est le nombre de commandes dans mon terminal depuis environ 3 ans.)


## Organiser, classer, catégoriser ?


## Pourquoi partager ces historiques ?


