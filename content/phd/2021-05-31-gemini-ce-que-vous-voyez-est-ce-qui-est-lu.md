---
layout: post
title: "Gemini : ce que vous voyez est ce qui est lu"
date: "2021-05-31T08:00:00"
comments: true
published: true
description: "Gemini est un protocole et un format pour publier et consulter des documents sur Internet, c'est une réaction à la complexification du Web, inspirée de Gopher. Le format, proche du langage de balisage léger Markdown, propose une autre façon de lire un texte structuré."
categories:
- carnet
- phd
classement:
- phd
bibfile: data/analyses-gemini.json
---
Gemini est un protocole et un format pour publier et consulter des documents sur Internet, c'est une réaction à la complexification du Web, inspirée de Gopher.
Le format, proche du langage de balisage léger Markdown, propose une autre façon de lire un texte structuré.

<!-- more -->

## Gemini&nbsp;: vers la simplicité
> Gemini is a new, collaboratively designed internet protocol, which explores the space inbetween gopher and the web, striving to address (perceived) limitations of one while avoiding the (undeniable) pitfalls of the other.  
> Gemini, [https://gemini.circumlunar.space/](https://gemini.circumlunar.space/)

Gemini a été créé comme alternative au Web, avec une forte influence de [Gopher](https://fr.wikipedia.org/wiki/Gopher).
Une façon plus simple, plus respectueuse des données personnelles et plus sécurisée de publier sur Internet.
Par publier il faut comprendre le fait de mettre à disposition des documents, c'est-à-dire essentiellement du texte structuré.

Gemini est **un protocole de communication**, qui permet d'effectuer des requêtes depuis un client vers un serveur pour recevoir des informations structurées.
Le protocole Gemini comprend la définition d'**un format**, et plus spécifiquement un format de balisage léger — une version allégée de Markdown.
Gemini est un projet en parallèle du Web&nbsp;: le protocole de communication est beaucoup plus simple que HTTP1 ou HTTP2, et le format est un langage sémantique très limité contrairement à HTML.

Si le projet de Gemini n'est pas de remplacer le Web, c'est une façon très inspirante de concevoir un modèle de publication sur Internet, privilégiant l'économie d'interactions et de balisage.
S'il n'est pas possible d'étendre Gemini — par exemple en ajoutant des balises, en intégrant d'autres langages comme JavaScript, etc. —, les clients qui affichent les pages créées avec Gemini peuvent eux faire preuve de souplesse.
En d'autres termes il y a un déplacement du contrôle de l'affichage depuis le document lui-même vers le dispositif qui affiche ce document.

Pour en savoir plus voici quelques ressources choisies subjectivement&nbsp;:

- Stéphane Bortzmeyer, Le protocole Gemini, revenir à du simple et sûr pour distribuer l'information en ligne ?, [https://www.bortzmeyer.org/gemini.html](https://www.bortzmeyer.org/gemini.html)&nbsp;;
- Sylvain Durand, Discovering the Gemini protocol, [https://sylvaindurand.org/discovering-the-gemini-protocol/](https://sylvaindurand.org/discovering-the-gemini-protocol/)&nbsp;;
- Ploum, Gemini, le protocole du slow web, [https://ploum.net/gemini-le-protocole-du-slow-web/](https://ploum.net/gemini-le-protocole-du-slow-web/)&nbsp;;
- des ressources pour _démarrer_ avec Gemini&nbsp;: [https://geminiquickst.art](https://geminiquickst.art/)&nbsp;;
- des ressources (très) diverses comme des clients (navigateurs)&nbsp;: [https://github.com/kr1sp1n/awesome-gemini](https://github.com/kr1sp1n/awesome-gemini)&nbsp;;
- Gemlog, une plateforme de publication Gemini en ligne et accessible via le Web&nbsp;: [https://gemlog.blue/](https://gemlog.blue/)&nbsp;;
- générer un site web compatible pour Gemini avec le générateur de site statique [Hugo](https://gohugo.io/)&nbsp;: [https://sylvaindurand.org/gemini-and-hugo/](https://sylvaindurand.org/gemini-and-hugo/).

Si vous voulez tester Gemini et afficher des sites _web_ conçus avec ce protocole et ce format, il y a de nombreux clients, il est aussi possible de passer par un proxy&nbsp;: [https://proxy.vulpes.one/](https://proxy.vulpes.one/) (qui existe aussi [sous forme d'extension](https://addons.mozilla.org/fr/firefox/addon/geminize/) pour Firefox).

## Ce que vous voyez est ce qui est lu
Je ne m'intéresse pas au protocole de communication de Gemini, mais au format retenu pour structurer les pages.
Pour les personnes utilisatrices de langages de balisage léger, Gemini est un format encore plus simple que Markdown.
La structuration sémantique se fait avec une dizaine de balises ([voir la documentation](gemini://gemini.circumlunar.space/docs/gemtext.gmi))&nbsp;:

- trois niveaux de titre avec `#`, `##` et `###`&nbsp;;
- des liens qui sont écrits en toutes lettres, précédés de `=>` et suivi du texte qui sera clicable&nbsp;: `=> https://www.quaternum.net Le carnet d'Antoine`&nbsp;;
- des listes non ordonnées composées de `*`&nbsp;;
- des blocs de citation précédés de `>`&nbsp;;
- et du texte dont les sauts de ligne créent les paragraphes.

Vous avez bien lu, impossible d'intégrer des images, c'est un langage tourné vers le texte — et le texte en tant qu'un modèle de données, une matière sémantique {{< cite "derose_what_1990" >}}.

Contrairement à l'usage habituel des langages de balisage léger, comme Markdown, Textile ou AsciiDoc, ici le format Gemini est celui qui est lu par les applications de lecture — les clients, ou navigateurs.
Dans le cas du Web c'est le format HTML qui est interprété par le navigateur, il n'est pas possible d'afficher un document Markdown dans Firefox ou Chromium sans quelque artifice.
Les formats de balisage léger habituels sont donc des formats _intermédiaires_&nbsp;: des formats d'écriture la plupart du temps, parfois des formats pivot, peut-être des normes d'écriture {{< cite "fauchie_markdown_2018" >}}, mais toujours transformés au format HTML pour pouvoir être lus dans un navigateur web.
Écrire des documents directement en HTML est rare, au mieux nous recourons à un balisage plus lisible pour les humains que nous sommes, au pire nous passons par une interface qui cache le balisage.

Gemini est à la fois un format d'écriture et un format de lecture.
Ce que vous écrivez **est** ce qui est affiché, ou plutôt ce qui est lu&nbsp;: la différence entre ce qui est exprimé et ce qui est interprété est réduite {{< cite "bolter_writing_2001" >}}.
Il n'y a pas de transformation du format d'écriture pour que le document original puisse être interprété puis affiché par le navigateur.
C'est une dimension nouvelle du principe du What You See Is What You Mean ([WYSIWYM](https://fr.wikipedia.org/wiki/WYSIWYM)) — Ce que vous voyez est ce que vous signifiez.
D'autant plus que la plupart des clients Gemini conserve les balises à l'affichage, comme si les lecteurs et les lectrices devaient avoir la même proximité avec le langage que ceux aui écrivent et qui publient.
Le protocole Gemini montre les dessous de la structuration du texte, le geste d'écriture apparaît plus clairement — en tout cas en apparence.
Ce qui est habituellement un format intermédiaire devient un format de diffusion.

La question ici ne concerne pas l'adoption ou le succès de Gemini (en tant que protocole ou format), mais le fait d'observer une initiative originale et cohérente d'un système de publication, et de l'influence qu'il peut avoir sur les pratiques d'écriture ou de lecture {{< cite "mcluhan_pour_1968" >}}.
Quoi qu'il en soit je dois avouer que lorsque j'ai affiché ma première page Gemini j'ai eu quelques frissons à la découverte d'un tel dispositif.

## Références citées
{{< bibliography >}}
