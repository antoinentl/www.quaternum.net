---
title: "Énéide, chant trois, lignes cent-soixante-douze à deux-cent-neuf"
date: 2023-03-22
aliases:
- /2023/03/22/eneide-livre-trois-lignes-cent-soixante-douze-a-deux-cent-neuf/
categories:
- carnet
- phd
accroche: "Lignes cent soixante-douze à deux-cent-neuf du livre trois de l'Énéide de Virgile, version remaniée de l'édition de 2015 des Belles Lettres (texte établi par Jacques Perret et traduction d'Olivier Sers), mis en ligne à l'occasion de la lecture publique coorganisée par le Centre d'études classiques de l'Université de Montréal et Mathilde Verstraete, jeudi 23 mars 2023."
published: true
---
Foudroyé par l'apparition, la voix des dieux, (ne dormant plus, je croyais voir encore leurs traits, leurs cheveux, leurs bandeaux, leurs visages présents, une sueur glacée coulait de tout mon corps), sautant du lit, j'élève au ciel avec mes paumes ma prière, au foyer répands en libation du vin pur, puis, joyeux d'avoir rempli ces rites, j'informe Anchise et point par point narre l'affaire.

Admettant l'équivoque issue des deux aïeux et sa nouvelle erreur sur notre terre antique, il dit : fils éprouvé par les destins de Troie, seule Cassandre a su m'annoncer ce futur et le prédire, il m'en souvient, pour notre race, souvent disant les noms d'Hespérie, d'Italie, mais qui eût cru que les Troyens aborderaient l'Hespérie ?
Qui alors Cassandre eût-elle ému ?
Phébus parle, cédons, suivons sa voie meilleure.
Il dit, nous tous l'applaudissons, obéissons, quittons encore la place, y laissant quelques hommes, et nos nefs creuses, voile haute, au large courent.

Sitôt en haute mer, hors de vue de la terre, partout cernés des cieux, partout cernés des flots, sur nos têtes s'arrête en noir nuage, gros de tempête et de nuit, l'eau sombre se hérisse, les vents roulent la mer, dressant de grandes houles, nous sommes ballotés, épars sur le grand gouffre, la nue voile le jour, la nuit noie d'eau les cieux, des éclairs redoublés déchirent les nuées.
Nous perdons notre route, errons à l'aveuglette, Palinure au ciel nie discerner jour ou nuit et retrouver son cap au milieu de ces vagues.

Après trois jours _douteux_ d'errance sur les flots dans l'aveugle brouillard et trois nuits sans étoiles, le quatrième un sol semble émerger, montrant des montagnes au loin, des fumées en volutes.
Voile à bas, sur leurs rames dressés, les marins brassent l'écume à force et balaient le flot sombre.

_Lignes cent soixante-douze à deux-cent-neuf du livre trois de l'Énéide de Virgile, version remaniée de l'édition de 2015 des Belles Lettres (texte établi par Jacques Perret et traduction d'Olivier Sers), mis en ligne à l'occasion de la lecture publique coorganisée par le Centre d'études classiques de l'Université de Montréal et Mathilde Verstraete, jeudi 23 mars 2023._
