---
title: "Vim : quelques commandes pour écrire et éditer"
date: 2023-04-21
categories:
- carnet
- phd
groupe:
- vim
tags:
accroche: "Après plus d'un an d'utilisation de Vim, voici la liste des commandes que j'utilise quotidiennement à l'occasion de l'écriture de textes. Loin d'être un usage poussé ou original, ces commandes constituent mes gestes (au sens du mouvement de mes doigts sur le clavier) d'écriture et d'édition."
published: true
bibfile: "data/analyses-vim.json"
---
Après plus d'un an d'utilisation de Vim, voici la liste des commandes que j'utilise quotidiennement pour l'écriture et l'édition de textes.
Loin d'être un usage poussé ou original, ces commandes constituent mes gestes (au sens du mouvement de mes doigts sur le clavier) d'écriture et d'édition.
J'ai présenté ces commandes à l'occasion d'une communication lors [du premier colloque des étudiants du CRIHN](https://colloque-crihn.ecrituresnumeriques.ca), communication intitulée "Mémoire des actes d'édition : les interfaces textuelles comme pharmakon ?" et temporairement accessible sour forme textuelle [ici](https://txt.quaternum.net/ma).

L'idée est aussi de documenter ma propre pratique, pratique qui évolue au fil de mon usage et de mes expérimentations.


## Quelques commandes

Ces commandes sont usuelles, et ne se veulent pas un mode d'emploi de Vim.
D'autres aide-mémoires bien plus pertinents ou complets existent, notamment [https://devhints.io/vim](https://devhints.io/vim).
Voici les principales instructions que je tape au quotidien :

- les modes : afficher, voir `v`, écrire `i`, écrire avec le curseur en fin de ligne `A` ;
- j'ai configuré un raccourci pour remplacer `ECHAP` : `;;` (voir ma configuration plus bas) ;
- écrire dans un fichier : enregistrer `:w`, fermer `:q` ;
- afficher plusieurs fichiers : si un fichier est déjà ouvert, séparer le terminal en plusieurs zones avec `:split mon-fichier.txt` ; ouvrir plusieurs fichiers avec `vim mon-dossier/*`, puis `:n` pour passer au fichier suivant ;
- supprimer une ligne `dd` ;
- chercher `/un mot` ou remplacer des occurrences `:%s/un-mot/un-meilleur-mot` ;
- afficher des statistiques (nombre de colonnes, lignes, mots, caractères et octets) : `g` puis `CTRL g` ;
- pour conserver un environnement d'écriture (taille et position de l'éditeur, fichiers ouverts, placement du curseur) ou, dit autrement, enregistrer une session : `:mksession nom-de-la-session.vim`. Et `:source nom-de-la-session.vim`.


## Fichier de configuration vimrc

Les commandes que j'utilise le plus ne reflètent pas totalement mon usage de Vim, l'accès au fichier de configuration est nécessaire pour comprendre les ajouts et les adaptations.
Plusieurs façons de gérer la configuration de Vim sont possibles, moi je mets tout dans le fichier `vimrc` (`~/.vim/vimrc` sur ma machine), et je publie ce fichier sur un dépôt Git : [https://git.sr.ht/~antoinentl/vimrc](https://git.sr.ht/~antoinentl/vimrc).

