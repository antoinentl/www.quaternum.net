---
layout: post
title: "Le trouble de l'écriture numérique"
date: "2019-11-13T10:00:00"
comments: true
published: true
description: "Écrire sur Wikipédia semblait si simple et évident pour quelqu'un comme moi qui pense avoir une littératie numérique, et pourtant j'ai été troublé par cette expérience de contribution."
categories:
- carnet
- phd
classement:
- phd
slug: le-trouble-de-l-ecriture-numerique
---
Écrire sur Wikipédia semblait si simple et évident pour quelqu'un comme moi qui pense avoir une littératie numérique, et pourtant j'ai été troublé par cette expérience de contribution.

<!-- more -->

_Ce billet est le texte rendu pour le séminaire "Littérature et culture numérique" de Marcello Vitali-Rosati, suivi à l'Université de Montréal à l'automne 2019, et lié à une pratique de contribution sur Wikipédia._

À lire également sur Wikipédia : [https://fr.wikipedia.org/wiki/Utilisateur:Antoinentl/Le_trouble_de_l%27%C3%A9criture_num%C3%A9rique](https://fr.wikipedia.org/wiki/Utilisateur:Antoinentl/Le_trouble_de_l%27%C3%A9criture_num%C3%A9rique)

## Tout est publié
Je suis encore ébahi par le fait qu'apparemment tout est publié, la notion de brouillon n'existe que dans la non-indexation de ces textes.
Je pensais naïvement pouvoir créer une page, l'éditer et la modifier longuement, puis enfin la rendre publique, cachant de ce fait les différentes évolutions maladroites de ma prose.
Il n'en est rien, tout est publié, éventuellement sans être trop visible, mais la notion de document privé – au sens où personne ne peut l'atteindre sans y être expressément invité – n'existe pas sur Wikipédia.
Et c'est une très bonne chose, car alors la dimension d'horizontalité prend tout son sens.
Je me rappelle de la définition du _livre numérique_ que donnait Pierre Mounier : est numérique un livre totalement et perpétuellement réinscriptible.
Alors seule Wikipédia peut répondre à cette définition, mais qui plus est la réinscriptibilité devient aussi celle des protocoles d'écriture.
L'écriture de l'écriture : chaque intention, chaque intervention, chaque discussion est disponible, liable (_linkable_), et est un atome de la matière Wikipédienne.

## Solitude
Nous sommes seuls sur Wikipédia, cette solitude a deux dimensions.
La première est une solitude collective : peu de personnes collaborent à Wikipédia.
Ce constat est réaliste, peut-être un peu sombre, et certainement le reflet d'une société qui ne peut être réellement collaborative – dans le sens où tous le monde collaborerait.
La seconde solitude est plus spécifiquement littérale : nous sommes seuls face à nos contributions.
Il y a certes de nombreuses et de nombreux relecteurs et relectrices, voir même des robots qui feront quelques vérifications automatiques.
Mais certains articles, de ceux qui ne sont pas les plus vus, n'attireront pas ou peu d'autres _wikipédiens_.
Et alors nous devons assumer ce que nous écrivons, le relire cent fois, être le plus juste.

## Addiction et itérations
Intervenir sur Wikipédia – en effet il s'agit bien plutôt d'interventions, en tant que double mouvement de lecture et d'écriture, et donc d'_inscription_ dans la base de données, et pas seulement de _contributions_ – revêt un caractère addictif une fois passée la contrainte de l'exercice.
Réinscrire continuellement, et créer de nouvelles entrées, tout cela de façon libre.
Chaque écriture fait potentiellement l'objet de révisions de la part de wikipédiens plus aguerris, ou de robots faisant le ménage et facilitant la vie des humains œuvrant sans fin à cette tâche belle et quelque peu ingrate.
Ainsi un jeu s'opère entre l'envie d'ajouter un fragment, une information, une page, et les contraintes de l’encyclopédie : ce passage est-il sourcé ? est-il bien structuré ? ne faudrait-il pas mieux modifier cette autre page ?
Et puis aussi parfois, le silence, aucune réaction, et nous écrivons sur d'autres pages.
Encore, continuellement.

## De belles adresses et de beaux documents
Un aspect peut-être invisible lorsque l'on lit Wikipédia, c'est la structuration de l'information et la mise en forme basée sur cette structuration.
Chaque page est structurée avec des modèles précis pour deux raisons : pour un _confort_ de lecture, pour faciliter le parcours dans les nombreuses informations ; pour que les machines et les programmes puissent identifier les informations et leur valeur.
Plusieurs initiatives ont été lancées pour repenser le _design graphique_ de l'encyclopédie, mais aucune n'a abouti.
Si l'aspect actuel semble quelque peu aride – cette analyse mériterait d'être largement approfondie –, il possède une forme de résilience.
Enfin, la constitution des adresses, des URLs, est particulièrement lisible.
Et c'est un problème encore [trop récurrent](https://www.bortzmeyer.org/beaux-urls.html) sur le Web.

## Versionner, archiver
Le _versionnement_ de documents est souvent un processus caché ou réservé à certains domaines comme le développement informatique.
Wikipédia intègre, pour manipuler le texte, le versionnement de chaque article, il est possible de retrouver une version ancienne et de la comparer avec une version plus récente.
C'est un outil indispensable dès que l'on contribue un tant soit peu dans l'encyclopédie libre, pour comprendre comment à évoluer une page, ou pourquoi une proposition a été rejetée.
Peut-être que grâce à Wikipédia cet outil d'édition, le versionnement, pourrait être utilisé dans [d'autres situations d'écriture et de publication](/2018/06/11/pourquoi-gerer-du-contenu-avec-git/).
Pour un utilisateur de Git comme moi, l'historique des articles a donné une autre dimension à mon écriture numérique : revenir sur d'anciennes versions, visualiser l'évolution.

## Le coût de la réinscriptibilité
Comme je l'ai déjà cité plus haut, Pierre Mounier définissait Wikipédia comme l'exemple du livre numérique emblématique du fait de sa réinscriptibilité.
Cette dernière à un coût non négligeable, au delà du temps nécessaire à l'apprentissage du fonctionnement et des outils de l'encyclopédie libre : accepter que tout contenu soit perpétuellement modifié, que nos propres ajouts soient immédiatement supprimés ou amendés.
Contribuer régulièrement à Wikipédia permet d'envisager cette réinscriptibilité, d'appréhender l'écriture numérique.
