---
title: "Vim : l'écriture dans l'écriture"
date: 2022-02-12T10:51:29-05:00
categories:
- carnet
- phd
groupe:
- vim
tags:
accroche: "Vim est un éditeur de texte libre pour les machines sous Unix. Un outil d'écriture où le paratexte prend une importance déterminante."
published: true
bibfile: "data/analyses-vim.json"
---
[Vim](https://www.vim.org/) est un éditeur de texte libre pour les machines sous Unix.
Très différent des éditeurs de texte _modernes_ comme Sublime Text, Atom, VSCode ou d'autres, c'est un environnement d'écriture qui demande un certain temps d'apprentissage. 
C'est aussi un outil d'édition qui est particulièrement configurable et personnalisable.

La première chose étonnante après quelques minutes à utiliser Vim, c'est le fait qu'écrire — inscrire, structurer, modifier, déplacer, signifier — nécessite une autre écriture.
Par exemple pour enregistrer un fichier, il ne faut pas utiliser les options du logiciel comme un bouton "Enregistrer" ou un raccourci `CTRL+S`, mais écrire une commande : `:w`.
Mais pourquoi ne pas passer par une action a priori plus simple ?
Chaque option (enregistrer, quitter, ouvrir un fichier, copier un fragment, supprimer une ligne, etc.) passe par une commande _écrite_.

Le texte tapé dans Vim est donc forcément accompagné d'un paratexte {{< cite genette_seuils_2002 >}}, ici numérique {{< cite vitali-rosati_paratexte_2015 >}}, qui n'est pas limité à l'espace de la page ou à un objet clos comme le livre.
Un texte _à côté_ du texte mais nécessaire à son existence.
Un texte néanmoins invisible, qui, une fois inscrit, est voué à disparaître de l'écran et du fichier.
Mais la commande reste inscrite dans deux _endroits_ : dans la mémoire de la personne qui écrit — c'est aussi une écriture —, et dans l'historique des commandes de Vim — dans lesquelles il est possible de naviguer.
L'écriture dans l'écriture ?
Non, c'est une écriture globale.

Je comprends mieux pourquoi j'apprécie d'_écrire_ les commandes Git lorsque je travaille sur un projet d'écriture versionné avec Git : je double mon écriture, et j'apporte autant d'importance au texte qui sera le résultat final lisible par moi ou d'autres, et ce paratexte qui accompagne ce texte, qui est même sa condition d'existence.

{{< bibliography cited >}}
