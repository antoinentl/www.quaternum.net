---
layout: post
title: "Fabriques de publication : Stylo"
date: "2020-05-21T23:00:00"
comments: true
published: true
description: "Huitième analyse d'une série sur les fabriques de publication : Stylo."
categories:
- carnet
- phd
classement:
- phd
status: brouillon
bibfile: data/analyses-stylo.json
---
Huitième analyse d'une série sur les fabriques de publication&nbsp;: Stylo.

<!-- more -->

[Introduction et liste des fabriques](/fabriques)

Stylo est un éditeur de texte sémantique, une application d'écriture pour l'édition scientifique.

## Description
Avant de débuter cette présentation je dois préciser que je participe à ce projet.
Malgré tous mes efforts je ne peux pas garantir mon objectivité – même si, d'une certaine manière, j'ai tous les éléments pour être d'autant plus critique.

[Stylo](https://stylo.ecrituresnumeriques.ca/) est un éditeur de texte sémantique utilisé pour créer des documents dans le domaine académique, et principalement des articles.
Stylo est la mise en pratique du principe du WYSIWYM – _What You See Is What You Mean_ pour Ce que vous voyez est ce que vous signifiez –, en opposition au WYSIWYG des traitements de texte.
Application web plutôt que logiciel, Stylo propose une expérience d'écriture pour les chercheurs et un outil d'édition pour les structures d'édition.

Stylo, après avoir été une preuve de concept, est un prototype très avancé.
Il s'agit d'une interface relativement simple à utiliser, qui ne demande pas de prérequis technique – contrairement à toutes les chaînes présentées jusqu'ici qui nécessite la manipulation d'un terminal.
Cet outil d'écriture permet de gérer trois choses&nbsp;: la structuration d'un texte avec l'usage du langage de balisage léger Markdown, la description des métadonnées avec le format YAML via des champs préformatés, et la gestion d'une bibliographie dynamique avec le format BibTeX et avec l'aide de clés de citation.
Une prévisualisation est disponible en version web – l'annotation est permise avec le service [Hypothesis](https://web.hypothes.is/) –, il est possible d'exporter un document dans de multiples formats, du HTML au PDF en passant par du DOCX ou du XML.
Le principe de Stylo est de redonner à l'utilisateur ou l'utilisatrice la maîtrise de l'écriture, c'est pour cela que l'interface ne masque pas toutes les couches techniques.

Stylo utilise des programmes déjà présentés ici, notamment Pandoc pour la génération de la prévisualisation et des formats exportés, et LaTeX pour la génération du format PDF.
Stylo est un assemblage de composants, sa plus-value réside dans l'interface et dans les scripts d'exports.
Si vous utilisez déjà Markdown, Pandoc ou LaTeX, Stylo vous sera utile simplement pour gagner un peu de temps, ou pour partager un document avec d'autres personnes.
Il est possible de travailler de façon asynchrone sur des textes – ce qui signifie qu'il n'est pas possible d'éditer à plusieurs en temps réel –, c'est la fonctionnalité qui en fait un outil d'édition, parmi d'autres&nbsp;: l'organisation des documents via des _tags_, un versionnement minimaliste, la création de documents plus complexes (livre, mémoire et thèse) ou la gestion du profil.

Comme dit précédemment Stylo intègre l'outil et service d'annotation [Hypothesis](https://web.hypothes.is), qui permet de mettre en évidence un passage ou de le commenter.
Il est possible d'engager une conversation sur un texte, publiquement ou via des groupes privés.
Cette fonctionnalité permet d'envisager un véritable travail d'édition, c'est-à-dire basé sur des protocoles et réunissant plusieurs personnes autour d'un même texte, ici de façon asynchrone.

Stylo dispose d'[une documentation détaillée](https://stylo-doc.ecrituresnumeriques.ca), pour des usages basiques ou avancés.
Le plus simple si vous avez des questions est probablement [de prendre contact avec moi](/a-propos).

## Histoire
Stylo est l'aboutissement de plusieurs années d'expérimentations de [la Chaire de recherche du Canada sur les écritures numériques](https://ecrituresnumeriques.ca/), et plus particulièrement Marcello Vitali-Rosati, Servanne Monjour et Nicolas Sauret.

La première étape a été la mise en place d'une chaîne d'édition permettant de contourner les contraintes d'un traitement de texte {{< cite "dehut_en_2018" >}} – "contraintes" étant un euphémisme tant ce type d'outil peut provoquer des catastrophes en terme de pratiques d'écriture.
Tout en expérimentant avec la revue _Sens public_ ([https://sens-public.org](https://sens-public.org/)), l'équipe de Marcello Vitali-Rosati a défini un ensemble de protocoles, et composé un parcours de l'information tout en conservant une exigence éditoriale.
Le résultat de ces recherches est [encore visible en ligne](https://github.com/EcrituresNumeriques/chaineEditorialeSP).

Stylo est une mise en pratique, un _possible_ de réflexions sur l'écriture et l'édition en régime numérique {{< cite "epron_ledition_2018" >}}.
Les travaux ne s'arrêtent pas là, la Chaire de recherche du Canada sur les écritures numériques coordonne notamment un projet avec des revues de sciences humaines qui testent différents dispositifs d'édition avec Stylo autour de [Revue 2.0](http://revue20.org/).

## Spécimen
Pour comprendre comment Stylo fonctionne et voir à quoi ressemble un résultat, le plus simple est encore de [vous créer un compte](https://stylo.ecrituresnumeriques.ca/).

La mise en application la plus poussée de Stylo est probablement la revue _Sens public_, [une revue en sciences humaines et sociales](https://sens-public.org) qui utilise désormais Stylo comme outil d'édition.
Le protocole d'édition est expliqué sur la page [Publier](https://sens-public.org/publier/) du site de _Sens public_.

## Critique
Stylo est modulaire.
Ce n'est pas forcément visible puisque les personnes qui l'utilisent accèdent à une interface, seul l'export révèle la présence de Pandoc et de LaTeX avec les commandes correspondantes.
Si Stylo est modulaire, cela signifie qu'il peut troquer Markdown pour AsciiDoc, Pandoc pour Jekyll (certes ce serait un peu _touchy_), LaTeX pour paged.js, etc.
L'éditeur de texte sémantique a mis en pratique un modèle théorique, d'autres outils disponibles pourraient être invoqués pour réaliser des tâches similaires.
Stylo n'est une création originale qu'à travers l'assemblage des logiciels ou programmes qui le composent, et avec l'interface web créée pour l'occasion.

Stylo est une solution d'édition qui se situe entre le logiciel à l'interface _user-friendly_ et la ligne de commande via un terminal.
Entre le facile et le technique pourrait-on penser.
L'objectif de Stylo n'est pas seulement d'être une solution concrète pour écrire et éditer, mais aussi une opportunité de nous interroger sur notre rapport aux outils et aux interfaces.
Stylo est loin d'être abouti, la rugosité de cet éditeur de texte nous oblige à comprendre le but de certaines de nos actions.
Ses défauts sont en quelque sorte intéressants.

Ce qui est difficilement perceptible dans ce projet c'est que ça priorité n'est pas encore de proposer à tout type d'utilisateurs et utilisatrices un outil d'écriture.
Stylo a d'abord été mis en place pour rédiger et gérer des articles scientifiques dans le cadre éditorial d'une revue.
Ce point est plus compréhensible quand on sait que le développement de Stylo s'est fait en parallèle de la revue _Sens public_ et de son site web – voir la partie spécimen ci-dessus.

Parmi les pistes d'amélioration, il y a notamment l'interface d'écriture qui doit laisser moins de flou sur certaines actions – traduction des libellés, clarification des interactions, _boutons_ pour la structuration Markdown, etc.
Les exports devraient proposés plus de modèles, notamment PDF, en plus de ceux déjà disponibles pour les revues qui utilisent Stylo.
La question de la synchronisation avec Git est à l'étude.
Plusieurs tests de connexion ont été réalisés avec des systèmes de gestion de contenu (CMS), même s'il y a encore des problèmes de synchronisation il serait envisageable d'utiliser Stylo pour _écrire_ avec WordPress ou Drupal.

Stylo est un cas intéressant d'outil créé par des chercheurs et qui a plusieurs implications – il s'agit ici de points de vue très personnels.
La première est de ne pas envisager l'industrialisation de cet éditeur de texte sémantique, ou alors ce ne serait plus à une équipe de recherche de diriger un tel projet.
L'utilisabilité d'un outil devient, à un certain moment, une démarche qui dépasse celle de la recherche.
Je dois avouer que je ne sais pas quoi penser des démarches qui visent à présenter un outil de recherche comme un produit totalement fini, à mon humble avis c'est à ce moment que des discussions peuvent s'amorcer avec d'autres types de structure, commerciales ou non-commerciales.
La seconde implication est le constat qu'en tant que doctorants et doctorantes du laboratoire nous utilisons _beaucoup_ Stylo, sans pour autant nous focaliser dessus.
Personnellement je préfère jouer avec un générateur de site statique, d'autres utilisent Pandoc et LaTeX, ou encore Zettlr.
Bref ce n'est pas parce que nous mettons beaucoup de temps et d'énergie dans Stylo que nous n'envisageons pas d'autres fabriques.

## Vers d'autres fabriques
Stylo est un outil pensé pour les chercheurs mais aussi pour les éditeurs.
C'est probablement ce qui le différencie d'autres fabriques basées sur les mêmes principes, comme [Zettlr](https://zettlr.com/) que nous verrons prochainement.

## Références

{{< bibliography >}}
