---
layout: post
title: "Aperçu #03"
date: "2022-10-04"
comments: true
published: true
description: "Comment imprimer le code source de nos livres ?"
categories:
- carnet
- phd
classement:
- phd
slug: apercu-03
---  
Comme le rouleau, le codex ou l'imprimé avant lui, le numérique redéfinit le rapport entre le texte, la vision du monde qu'il porte et la personne qui le lit.  
Le choix de la contrainte, pour sûr.  
Comment imprimer le code source de nos livres ?  
Composer sémantiquement est désormais un rêve réalisable.  
Nos ateliers sont des jardins ouverts, mais fermés.  
La précision sémantique est un jeu de compromis.  
La performance ou l'ennui.  
La rigidité est parfois bien accueillie.  
Quelle est la version de la version ?  
La recherche est tout de même plus rigolote à mener lorsqu'elle est performative.  
Il n'y a pas de modèle éditorial unique.  
Écrire est un acte complet, qui relève autant du design que d'autres choses.  
Prototyper n'est pas vain.  
Et soudain le geste d'écriture s'emballa, s'extirpant des lignes de code.  
Les réponses technologiques ne sont pas des réponses (bien souvent).  
Ce n'est pas le plat et la soupe dedans disait-il.  
Ce qui est monolithique ne l'est plus forcément si l'on dézoome.  
Reproduire des vieux schémas éculés, mais avec classe.  
D'un seul tenant.  
Le XML génère une pensée inutilement complexe, dans certains cas, mais dans trop de cas.  
Des petites balises dans des petits fichiers pour des grandes publications.  
Le module du module du module.  
Formuler c'est déjà un peu tuer.  
Il y a un nom pour tout.  
Les fabriques auraient-elles cette faculté de se définir elle-même ?  
Dériver le long des concepts, à l'infini.  
Le dialogue entre le texte et sa machine, entre la machine et son texte.  
La gentille fabrique au secours du méchant dispositif.  
Une fabrique de publication est une incarnation d'un rapport particulier aux contenus, à la technique et au monde.  
L'hypothèse est en fait un constat, la thèse une esbroufe de ce qui est devant nous.  
Tout d'un coup le dispositif ne semble plus vraiment un dispositif, tout cela étant un peu vain.  
La littérature se nourrit de ruptures technologiques.  
Éditer avec les fabriques.  
Écrire et dérouler.  
Le mouvement hybride de production de contenus et de conception de la chaîne d'édition.  
Performer l'analyse, ou analyser la performance.  
Les spécimens supportent bien plus que les sources.  
Ne pas rendre générique est un luxe, une liberté.  
Les formats sont à la fois une manifestation des changements dans l'édition et l'origine de ces changements.  
Sans format, pas de média.  
Les détails de l'appropriation constituent les visions du monde.  
Les critères des fabriques, en partie communs aux principes des nouveaux médias.  
Pivoter : le tourbillon des fichiers était si beau.  
Le lien entre l'idée et sa réalisation tient dans la matière du papier.  
Plutôt que les logiciels produisent les formats, les formats informatiques guident eux-mêmes les logiciels.  
Analyser les formats informatiques aujourd'hui est un moyen de comprendre l'évolution de l'édition.  
Un workflow intéressant est un workflow en cours de fabrication.  
Le livre se doit-il d'être robuste, portable, commode et durable ?  
L'édition a mimé la mécanisation trop longtemps.  
Si c'est figé c'est pas numérique.  
Comment définir les trois niveaux d'édition, entre linéarité et modularité.  
XML XML XML.  
Simplifier l'écriture pour enrichir la structure.  
199 occurrences de format.  
L'écriture numérique est une écriture expérimentale et scientifique, elle le sera toujours.  
Avec les standards viennent les communautés, l'informatique est un truc d'humains.  
Artefacts artefacts artefacts.  
La légitimité d'un format doit se faire en oubliant les logiciels permettant de le manipuler.  
Un nouveau geste d'édition, celui qui conçoit des livres tout autant que leurs fabriques.  
Les bouts de texte qui prennent une autre valeur.  
L'hérédité des processus d'édition, sans blague.  
Formater les formats.  
Les formats, entre fondements de l'édition et nouveau cheminement littéraire.  
Désaccorder son workflow pour mieux écrire.  
Dans la fabrique du livre, la chaine éditoriale est la main qui apprend.  
Étudier les détails du texte pour penser les textes.  
La condition de la réussite du single source publishing est de construire la fabrique en fonction des contenus.  
Des outils simples pour écrire des choses sur des sujets compliqués.  
La page n'est pas que l'expression d'évolutions de pratiques d'écriture et d'édition, c'est aussi un nœud technique.  
L'écriture séquentielle est un mode de pensée.  
Les textes foutraques sont-ils possibles avec des méthodes et des outils conventionnels ?  
Abandonner la chaîne pour le système ne serait peut-être pas une si bonne chose.  
Les mauvais choix sont les bons choix, tant qu'on écrit.  
Publier autrement, comme une porte de secours de ce monde cassé.  
Les chaines de dépendances : si une brique ne peut s'enlever, alors il faut tout abandonner.  
Purifier, sublimer, purger ou affiner devient une nécessité pour conserver une autonomie et une indépendance.  
Pas de source unique sans une refonte de la légitimation.  
Déconstruire la chaîne de publication en appliquant le single source publishing.  
Dévoiler le texte en tant que structure peut-il etre autre chose qu'une exaltation ?  
Glisser les brouillons entre les lignes de code.  
L'écriture dans l'écriture.  
Vim ou comment radicaliser ses pratiques d'écriture.  
Le texte peut-il s'abstraire d'une structure hiérarchique, ordonnée et imbriquée ? Espérons-le, pour le bien de la littérature.  
Un outil réellement puissant peut-il être autre chose qu'exigent ?  
L'institutionnalisation c'est la mort.  
Différents espaces dans une même dimension : que la complexité soit possible.  
Où Richard Dawkins nous aide à comprendre le design des interfaces.  
Les fabriques sont inscrites dans un écosystème plus large qui leur permet de se déployer, ou au contraire de rester dans l'ombre.  
L'imprimé comme le balisage sont des réponses technologiques, toutes les deux techniques et complexes.  
Personnaliser à outrance, se fabriquer son environnement, chercher une forme de confort.  
Servir un propos, baliser le terrain, recommencer.  
L'ennui des dispositifs, l'envie de les détruire s'étouffe.  
Les valeurs des valeurs deviennt les moyens des moyens.  
Écrire les programmes pour permettre à nos intuitions scientifiques d'exister et d'être confrontées.  
La concordance des occurrences, la limitation des intuitions.  
Pour des outils post-libres : se débarrasser de l'injonction de créer du nouveau.  
Les effets de la décentralisation sont inespérés (enfin presque).  
La maîtrise, la vraie, est lente, fastidieuse, complexe et presque douloureuse.  
L'action ultime est la non-action : ne pas avoir à suivre un comportement plus ou moins prévu est un privilège.  
Apprendre à écrire, toujours.  
Produire des artefacts editoriaux depuis une source unique est une démarche d'inclusion.  
Le texte sous les doigts, sentir les mots sans quitter le clavier.  
Le texte est vivant si on lui donne les moyens de l'être.  
Malgré nous, en fabriquant des livres, nous imposons des comportements.  
À bas les systèmes (éditoriaux).  
La preuve de concept génère des preuves de concept.  
Publishing tools/workflows studies.  
Le sens de l'édition est aussi de créer des environnements d'écriture et de publication découvrables.  
Le principe de modularité n'est valable qu'à condition de l'épuiser.  
Les changements du présent sont un peu les mauvais choix du futur.  
Pas d'étincelles sans friction, pourrait-on dire naïvement.  
Faire beaucoup avec peu, trouver des solutions simples dans un contexte complexe.  
C'est en fabricant qu'on devient forgeron·ne.  
Entre potentialités et adoption, rien à prouver mais tout à construire.  
Nos écritures modulaires ne sont pas des Lego.  
L'hybridité comme condition de l'édition.  
Entre la complexité et la difficulté, un océan, un mur.  
Du concept au concept, de l'objet au projet.  
Tout tient dans le fil conducteur, c'est l'histoire dans l'histoire, jusqu'à en perdre le réel.  
En séparant les usages nous pouvons nous affranchir de certaines contraintes  
Le déploiement de la pensée nécessite des espaces d'écriture, dans mon cas il y en a cinq.  
Nous écrivons continuellement dans des cadres plus ou moins ouverts, plus ou moins définis.  
Identifier chaque portion pertinente, découper le texte jusqu'à épuisement.  
Éditer est un acte qui englobe la mise en place d'une fabrique.  
Qu'en est-il de la partie technique ?  
Plus qu'une simplification, manipuler des objets textuels dans un terminal est une libération.  
Comment nommer cette action de fabriquer et produire de multiples artefacts à partir d'une source unique ?  
Institutionnaliser nos workflows n'est une nécessité mais une étape.  
Les positionnements politiques et poétiques s'expriment à l'intérieur et autour du livre.  
La chaîne d'édition est le reflet d'une pratique éditoriale.  
L'adéquation entre choix techniques et geste éditorial n'est pas toujours là.  
Le savoir-faire technique et éditorial n'est pas toujours chez les maisons d'édition, et c'est dommage.  
Comment démontrer qu'il y a une technique éditoriale ?  
Repenser l'édition (ou la publication) demande quelques sacrifices.  
Il faut imaginer comment aller quelque part avant de savoir où aller.  
La technique éditoriale s'exprime avec des mots.  
Le balisage est toujours plus vert ailleurs.  
Détourner n'est pas tricher.  
L'apprentissage est plus fun quand on le décrit.  
Des cadres, des pratiques, des processus et des outils.  
Et la littérature ?  
Dans l'écriture balisée, faire se traduit par dire.  
Définir une modélisation de la littérature, voilà une tâche difficile.  
Pour faire disparaître les monopoles la fédération est peut-être la solution.  
L'écriture requiert un certain niveau d'immersion et de praticité.  
Le geste est aussi à l'origine de la pensée.  
Pour une histoire (non balisée) des balisages.  
Parlons des formats, tuons Word, construisons nos espaces d'écriture.  
Luttons contre l'influence des logiciels sur nos pratiques d'édition.  
Imaginer une chaîne d'édition à partir d'un artefact éditorial serait une erreur.  
Quelle était la question ?

_Troisième livraison de l'aperçu de mon [atelier de thèse](/2020/03/29/atelier-de-these/), du 301 au 451e [commits littéraires](/2020/03/29/atelier-de-these/#un-robot-fait-de-commits-littéraires) écrits dans les creux de l'atelier._
