---
layout: post
title: "L'écriture du papyrus à l'hypertexte"
date: "2019-12-12T01:00:00"
comments: true
published: true
description: "Une relecture de l'ouvrage de Christian Vandendorpe, Du papyrus à l'hypertexte: essai sur les mutations du texte et de la lecture."
categories:
- carnet
- phd
classement:
- phd
status:
- brouillon
slug: l-ecriture-du-papyrus-a-l-hypertexte
bibfile: data/l-ecriture-du-papyrus-a-l-hypertexte.json
---
Une relecture de l'ouvrage de Christian Vandendorpe, _Du papyrus à l'hypertexte: essai sur les mutations du texte et de la lecture_.

<!-- more -->

En lisant un article sur un site web il nous est difficile d'imaginer comment il pourrait être écrit avec autre chose qu'un clavier.
Pourrions-nous revenir en arrière pour inscrire, structurer puis diffuser un texte&nbsp;?
Il ne s'agit pas seulement d'une dématérialisation du stylo et de la feuille, mais de tous ces enrichissements permis par des applications diverses et surtout par le web en tant que support.
Qu'est-ce que le numérique fait à l'écriture&nbsp;?
Cette vaste et naïve question peut être abordée, sinon résolue, en invoquant un ouvrage publié il y a désormais vingt ans, _Du papyrus à l'hypertexte: essai sur les mutations du texte et de la lecture_ {{< cite "vandendorpe_du_1999" >}}.

Le texte de Christian Vandendorpe publié en 1999, est d'un apport majeur dans l'étude des médias et l'histoire du document.
Cet ouvrage représente une avant-garde théorique notable, au moment où l'hypertexte, Internet et le web étaient encore peu étudiés dans le domaine des sciences humaines.
À travers une cinquantaine de concepts ou notions et avec comme point d'ancrage le langage hypertextuel, l'auteur parvient à définir avec précision toutes les possibilités mais aussi tous les questionnements qu'impose l'hypertexte dans une perspective d'inscription, d'écriture, et de lecture.
Textualité, interactivité, images, livre, page, etc.
Les sujets abordés par l'auteur sont nombreux et souvent accompagnés d'exemples illustratifs.
_Du papyrus à l'hypertexte_ a également permis d'identifier, en creux, des points essentiels dans le domaine de _l'écriture numérique_.

Ce livre est une source pour analyser des éléments précis de l'écriture numérique, nous allons extraire trois notions&nbsp;: la tabularité (qui peut être considérée comme la structuration d'un contenu ou d'une page permettant d'y naviguer), la note ou _Op. Cit._ (la difficile tâche de faire usage de notes en déconstruisant le modèle d'une lecture linéaire et paginée), et enfin la stabilité (comment figer un texte qui peut sans cesse être modifié&nbsp;?).
Pour chacune de ces trois parties un cas sera analysé, confirmant ou critiquant nos propositions&nbsp;: livres numériques, plateformes, etc.
Enfin nous éclairerons ces trois notions à partir de travaux divers, allant de Platon à Kenneth Goldsmith, en passant par François Bon ou Samuel Archibald.

## 1. Tabularité
La tabularité représente une des évolutions majeures apportées par le codex, permettant dans le même temps une transition marquée entre oralité à écriture.
Pour définir ce concept une comparaison est nécessaire&nbsp;: le rouleau correspond à une transcription fidèle de l'oralité, alors que le codex apporte une nouvelle _structure_ avec la page.
D'un côté le rouleau reprend la dimension de linéarité propre à l'oralité, il n'est pas possible de naviguer dans un rouleau, le texte est écrit aux kilomètres.
De l'autre le codex est construit à partir de la page, cette structure sera plus tard affinée avec le chapitrage, puis l'identification des paragraphes.
Avec la pagination il est possible de circuler dans un texte, avec les sections le repérage est facilité.
Le texte s'émancipe du temps, l'individu peut maîtriser les contenus qu'il lit.

> [...] l’écrit nous permet d’échapper à la linéarité, car l’œil peut embrasser la page d’un seul regard, tout comme il peut se poser successivement sur divers points, choisis chaque fois en fonction de critères différents. (p. 40)

Le langage HTML traduit en langage logique cette structuration sémantique introduite par le codex, et l'hypertexte permet de parcourir les pages et les sections.
Si Christian Vandendorpe aborde les questions de balisage du texte – SGML, XML et HTML –, en 1999 il est encore tôt pour parler de _web sémantique_.
La dimension tabulaire ne concerne pas que les humains, mais aussi les machines.
Structurer finement du texte est utile pour une lecture par l'œil, c'est le rôle des interfaces d'interpréter le balisage et de lui donner un rendu graphique – qui peut fortement changer en fonction du contexte de lecture.
Au-delà de sa seule structuration le texte peut comporter des enrichissements qui seront utiles pour d'autres usages&nbsp;: encore des interfaces pour lier ou mettre en valeur des éléments précis, ou du traitement automatisé sur ces éléments pour référencer ou moissonner les contenus.

Plongeons dans l'édition augmentée du livre de David R. Lankes, _Exigeons de meilleures bibliothèques_ {{< cite "lankes_exigeons_2018" >}}.
Cet ouvrage a été publié en français par les éditions Les ateliers de [sens public], en versions imprimée et numérique.
Les PDF et EPUB sont homothétiques, ces formats portent les mêmes propriétés que le livre _papier_ (lecture a priori linéaire, pagination, chapitrage).
Une édition augmentée vient compléter cette initiative éditoriale.
Pourquoi _augmentée_&nbsp;?
Par un enrichissement global avec des contenus supplémentaires, comme des articles, des vidéos ou des références qui s'ajoutent au texte initial.
La seconde est l'enrichissement du texte&nbsp;: des mots sont balisés, sélectionnés selon plusieurs thématiques.
Ici des personnes, des lieux, des organismes.
C'est ce que propose depuis longtemps la TEI {{< cite "burnard_quest-ce_2015" >}}, sans qu'il n'y ait d'implémentation dans des interfaces de lecture.
En balisant des termes et non plus des sections ou des paragraphes, la tabularité est étendue.
Les robots peuvent parcourir le texte et identifier ces termes mis en évidence et liés à d'autres informations, les humains peuvent découvrir des compléments ou accéder aux contenus par ces nouvelles entrées.

La finesse de la tabularité peut être considérée comme une tentative d'échapper aux lettres mortes, pour dépasser la critique que Platon fait de l'écriture.
En balisant le texte il ne s'agit plus seulement d'un enregistrement figé, mais de quelque chose de vivant, empli de sens.
L'écriture, avec le numérique, n'est plus seulement une fixation mais elle se lie à d'autres.
La trace issue de l'écriture dépasse la parole initiale.
Avant d'analyser un second sujet, nous devons souligner qu'une question est soulevée dans cette nouvelle pratique d'écriture.
Quelle granularité choisir pour la balisage&nbsp;?
Si le mot ou le bloc de texte sont celles qui sont majoritairement choisis, faut-il aller jusqu'à la lettre&nbsp;?

## 2. La note
La note est un héritage de la glose, para-texte incontournable, semble-t-il, des livres savants, pour reprendre l'expression de Christian Vandendorpe.
_Op. cit._ renvoie à une certaine pratique de la note qui consiste à décrire de façon complète une source puis à y faire référence les fois suivantes.
L'idée étant de gagner de la place – moins de texte, moins d'encre, moins de papier – et du temps puisque le lecteur peut se souvenir de la première occurrence sans avoir à la relire plusieurs fois.
Avec une lecture non linéaire ce procédé devient un obstacle, obligeant le lecteur à aller chercher une source indiquée dans un passage qu'il n'avait pas lu.
De nouvelles techniques sont apparues pour faciliter la vie des auteurs, des lecteurs et des éditeurs&nbsp;: des appels composés du nom de l'auteur et de l'année d'édition de la référence bibliographique correspondent à une bibliographie complète en fin de texte.
Cela ne résout pas le problème de la note de contenu, qui est amené à disparaître ou à se fondre dans l'écrit plus structuré.
Christian Vandendorpe voit dans l'hypertexte une solution à la fois intéressante et problématique&nbsp;: si la note est à une portée de clic, comment l'afficher dans le même espace de lecture&nbsp;?

> Ces difficultés et ces réticences que le livre éprouve à mener deux textes en parallèle sont révélatrices de la prédilection ancienne de celui-ci pour le fil continu, longtemps considéré comme la condition incontournable d’un fonctionnement optimal de la machine textuelle. (p. 165)

La passage de l'imprimé au web – de la page à un retour à une forme de rouleau – remet donc en cause le principe de la note classique.
D'une part en raison des liens hypertextes, d'autre part grâce aux travail de design graphique.
Il est possible de lier deux pages ou deux passages, et depuis un appel de note de renvoyer vers la note complète.
L'affichage peut également se faire de façon plus subtile&nbsp;: la note apparaît en surimpression au survol de la souris ou au toucher, ou encore elle s'insère _dans_ le texte à l'activation par le lecteur.
Sur ce dernier point on peut aisément comprendre que la note perd un peu de son intérêt en tant que contenu parallèle, puisqu'elle rejoint alors le flux du texte principal.

Attardons-nous un peu plus sur la note, sur ce qui peut sembler être un détail, mais qui révèle pourtant une forme de dissociation entre la transcription et l'écriture en s'autonomisant du flux.
Le simple fait de pouvoir choisir le comportement de la note donne à celui qui écrit une forme de maîtrise du contexte de lecture.
L'exemple que nous pouvons prendre est un modèle de mise en forme créé par Edward Tufte.
À l'origine créée pour LaTeX, un workflow de publication, cette mise en forme a été déclinée pour le web ([voir en ligne](https://edwardtufte.github.io/tufte-css/)).
Le comportement des notes est d'une finesse remarquable&nbsp;: sur un écran large (ordinateur) la note s'affiche dans le volet latéral&nbsp;; sur un écran plus petit (tablette, téléphone intelligent) la note activée par un clic apparaît sous la ligne, décalant verticalement le texte.
Toutes les situations de lecture sont envisagées, et une "note de marge" vient compléter la note de contenu.
Comme pour l'imprimé, l'écriture est une question de design.
Le numérique prolonge cette recherche de l'interface de lecture.

Christian Vandendorpe évoque l'intérêt que peut représenter la note, paratexte qui complète ou qui donne à voir un autre regard.
Mais il y a d'autres paratextes qui apparaissent avec l'usage du numérique.
Prenons l'exemple du message accompagnant un _commit_, cela nous permettra d'introduire la dernière partie de cet exposé.
Le _commit_ est une suite de modifications dans le processus de gestion de versions Git {{< cite "demaree_git_2017" >}}, un message facilite la compréhension de ces modifications.
Ce _méta-texte_ peut être lu comme une indication quelconque, ou comme un texte en parallèle, une écriture dans l'écriture.
Cela pourrait correspondre au journal qui est produit à côté de l'œuvre, mais le _commit_ est bien plus entremêlé et dépendant du texte.
Le versionnement est, comme nous allons le voir ensuite, une solution aux question de stabilité.

## 3. Stabilité
La stabilité est une question rapidement réglée avec l'impression, l'écriture éditée connaît un état déclaré et peu sujet au mouvement grâce à plusieurs protocoles instaurés dès le 16e siècle avec l'apparition du dépôt légal en France.
Si le texte est modifié une nouvelle édition est créée.
Ce modèle est dupliqué encore aujourd'hui avec le système d'édition scientifique, des articles publié sur des plateformes numériques portent le numéro de la page du numéro de la revue dont ils sont extraits, alors que le dit numéro ne dispose pas de version imprimée.
Dans _Du papyrus à l'hypertexte_ Christian Vandendorpe pointe la limite de l'édition comme modèle de publication dans un environnement numérique.
Si chacun·e peut publier immédiatement et partout, via le web et Internet, l'édition peut retrouver un intérêt comme dernier bastion de légitimation.

> Mais, avec l’avènement du support électronique, le texte est devenu une matière éminemment labile&nbsp;: il peut être effacé en une fraction de seconde, modifié, transformé, corrigé indéfiniment et sans effort. La permanence du texte est aujourd’hui chose du passé. (p. 179)

Nous souhaitons prolonger cette réflexion pour envisager des solutions permettant d'arpenter plusieurs versions d'un texte.
Car quand bien même la fonction éditoriale connaîtrait un renouveau avec le numérique, il est désormais possible pour un texte de disposer de plusieurs versions et il convient de définir comment cela interroge l'écriture.
Dans le domaine académique la co-existence de plusieurs versions parallèles est possible mais rare, en revanche la succession de versions est plus fréquente&nbsp;: d'un brouillon à un article soumis jusqu'à une publication en passant par une révision par les pairs.
Il est possible de retracer la vie d'un texte, cela est facilité si le processus est ouvert.
À une plus large échelle il s'agit du libre accès, et de la _cristallisation_ du savoir {{< cite "stern_crystals_2015" >}}.
Un exemple concret est celui de Wikipédia, l'encyclopédie comporte un système de gestion de versions, chaque série de modifications est enregistrée, il est possible de revenir en arrière ou de comparer deux versions.

S'inspirer d'une écriture du code peut nous apprendre à intégrer cette dimension de versions, autant comme processus d'écriture que comme variantes d'un même texte.
La stabilité n'est alors plus une condition mais un état, un protocole défini.
Le logiciel de gestion de versions Git, très utilisé en informatique, répond à cette exigence.
Plusieurs initiatives font usage de Git comme outil d'édition, prenons celui de la revue _Distill_ ([https://distill.pub](https://distill.pub)).
Chaque article a son propre dépôt et nous pouvons découvrir les _commits_ successifs, notons au passage que les articles ne cessent pas d'être mis à jour après leur mise en ligne.
L'une des forces de Git est de pouvoir disposer de branches, des versions du projet qui vivent en parallèle avant de peut-être fusionner avec le projet initial.
C'est le cas avec _Distill_ puisque certains articles ont conservé une branche "report" qui doit correspondre à une révision.
Nous pouvons laisser à d'autres la possibilité de modifier notre texte via ces outils de versionnement.
Peut-être que cela nous enlève notre maîtrise sur le monde, nous la laissons à d'autres pendant un temps.
Pour étendre cet exemple&nbsp;: il est désormais possible de _brancher_ des programmes qui agissent de façon automatique – ou disons sans intervention humaine directe – sur un dépôt Git hébergé sur une plateforme de code comme GitHub ou GitLab.
Un texte peut évoluer grâce à des robots qui corrigent seuls les fautes d'orthographe et de grammaire, puis qui ajoutent des références bibliographiques grâce à des algorithmes.
Ce dernier point est encore de la science fiction, mais c'est tout-à-fait envisageable.

## Conclusion
L'écriture numérique et plus spécifiquement hypertextuelle – ou pour le dire de façon plus abrupte, le web – révèle des aspects inédits de l'_écriture_, prolongements de deux mille ans de développement de supports de lecture et d'outils d'écriture.
Les trois sujets ou concepts extraits de _Du papyrus à l'hypertexte_ et abordés ici nous ont permis de toucher du doigt des spécificités que nous allons rappeler brièvement.
La tabularité est avant tout un processus pour faciliter la lecture et l'appropriation d'un texte, ainsi qu'un enrichissement pour les humains et les robots.
Elle est aussi le creux de l'écriture.
La note nous aide à comprendre combien l'espace actuel, numérique, peut être façonné pour faciliter la compréhension d'un paratexte, voir d'un méta-texte.
Enfin la stabilité n'est plus un objectif à atteindre mais un déclaration d'un état dans une chaîne de modifications et de versions.
Pour croiser ces trois notions prenons l'exemple de la citation d'un texte numérique qui est amené à être modifié.
Comment permettre cette citation d'un contenu non stable&nbsp;?
La tabularité du texte peut contenir, sur sa totalité ou des passages conscrits, les indications des _commits_ concernés, et ainsi former un état du texte.

Pour clore cette courte réflexion, et ouvrir sur d'autres recherches complémentaires, ces trois notions participent à la constitution d'une éditorialisation {{< cite "vitali_rosati_quest-ce_2016" >}}.
La tabularité, la note et la stabilité sont des marqueurs de l'évolution du texte, et des outils pour penser et comprendre l'écriture ~~numérique~~.
Ces trois marqueurs forment une dynamique, qui elle-même forme un espace de sens et de compréhension du monde.

## Bibliographie

{{< bibliography >}}

---

_Texte rédigé dans le cadre du cours de Marcello Vitali-Rosati, [Littérature et culture numérique](http://vitalirosati.net/slides/2019/cours-2019-fra6730.html), à l'Université de Montréal en décembre 2019._
