---
title: "Vim : maîtriser son environnement"
date: 2022-04-10T10:10:31-04:00
categories:
- carnet
- phd
groupe:
- vim
tags:
accroche: "Mon expérience d'écriture avec Vim continue — et d'ailleurs je comprends que la dimension expérience ne va pas s'arrêter. Je m'interroge cette fois sur le fait qu'avec cet éditeur de texte j'apprends à maîtriser mon environnement, un environnement d'écriture, d'édition et de pensée."
published: true
bibfile: "data/analyses-vim.json"
---
Mon expérience d'écriture avec Vim continue — et d'ailleurs je comprends que la dimension _expérience_ ne va pas s'arrêter.
Je m'interroge cette fois sur le fait qu'avec cet éditeur de texte j'apprends à maîtriser mon environnement, un environnement d'écriture, d'édition et de pensée.

Maintenant que [j'écris en écrivant](/2022/02/12/vim-lecriture-dans-lecriture/) et que [j'édite en déclarant](/2022/02/20/vim-editer-par-declarations/), je suis en apprentissage constant.
Avec Vim vient tout un langage qu'il faut appréhender et faire sien : l'outil est exigeant si l'on souhaite réellement l'_utiliser_.
Je m'explique, jusqu'ici j'applique presque mes pratiques d'avant (avec des éditeurs comme Sublime Text, Atom, VSCode/Codium ou Nano), j'utilise encore peu les options multiples et parfois vertigineuses de Vim.
Et je personnalise principalement l'interface graphique, l'agencement des éléments sur mon écran.

En lisant le texte de Silvio Lorusso {{< cite lorusso_liquider_2021 >}}, et plus particulièrement les références à Alan Kay, je comprends que Vim offre une approche ouverte — comme d'autres éditeurs comparables.
Vim fait partie de cette famille d'outils qui sont à la fois utilisables facilement (à condition de connaître _quelques_ commandes basiques), et qui peuvent permettre une autonomie très importante (jusqu'à configurer soi-même un certain nombre d'options).

> Voici, encore une fois, la devise d'Alan Kay : "Les choses simples devraient être simples et les choses compliquées possibles".  
> {{< cite lorusso_liquider_2021 >}}

Avec Vim, écrire et modifier un fichier est (relativement) simple.
La courbe d'apprentissage pour ces actions (ouvrir un fichier, modifier, enregistrer, fermer) n'est pas plus importante que pour d'autres éditeurs de texte.
En revanche, si je souhaite naviguer rapidement entre plusieurs fichiers, aller à une ligne, remplacer une portion de texte ou répéter une action, cela nécessite un apprentissage supplémentaire — mais tout à fait accessible.
Aussi, le degré de personnalisation est très important : je peux créer tous les raccourcis clavier dont j'ai besoin, agencer mon éditeur, modifier l'aspect graphique.
La complexité est donc possible, je peux me créer un environnement d'écriture et d'édition qui correspond à mes besoins, sans être limité par une interface graphique qui ne me donnerait que quelques options.
Avec Vim, je modifie un fichier de configuration nommé `vimrc`, il contient toutes mes spécifications.
Je place au même endroit tous mes réglages personnels et les indications propres aux extensions — je n'évoque pas plus les extensions ici, j'en parlerai probablement plus tard.

La différence avec un éditeur de texte comme VSCode/Codium réside dans le fait que tout est paramétrable via du texte.
Les éditeurs de code plus modernes proposent aussi des fichiers de configuration (dans différents formats), mais l'accès n'est pas toujours simple, et surtout c'est l'interface graphique qui est mise en avant.
La cohérence d'un outil comme Vim est donc telle que _tout_ se déroule via du texte, et que chaque action ou modification entraîne un apprentissage — et non un comportement dicté par le logiciel.
Par ailleurs Vim est une application qui ne requiert que très peu de ressources.
C'est l'un des griefs formulé à l'encontre des éditeurs de texte populaires et relativement récents comme Atom ou VSCode, alors que Vim peut fonctionner sans problème sur des machines peu puissantes.
Ce qui semble plutôt pertinent pour un logiciel qui permet de saisir et de manipuler du texte.

Avec un éditeur de texte comme Vim, j'apprends à écrire, le texte sous les doigts.
