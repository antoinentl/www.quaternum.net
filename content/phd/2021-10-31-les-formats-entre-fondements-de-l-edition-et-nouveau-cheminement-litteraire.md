---
title: "Les formats, entre fondements de l'édition et nouveau cheminement littéraire"
date: 2021-10-31
description: "Essai rédigé en guise de réponse à la question de l'examen de synthèse."
categories:
- carnet
- phd
bibfile: data/formats.json
---
Cet essai a été rédigé dans le cadre de mon examen de synthèse en août 2021 en une semaine, une étape du cheminement doctoral qui correspond au fonctionnement de l'Université de Montréal et plus spécifiquement au Département de littératures et de langues du monde et à l'option Humanités numériques.

## Définir les formats informatiques

### Le format
Le terme _format_ est un terme technique, il délimite les caractéristiques d'un objet.
Cette délimitation se traduit par un certain nombres de données, d'instructions, ou de règles.
Le but est de constituer une série d'informations compréhensible, utilisable et communicable.
Pour prendre un exemple concret, l'impression d'un document nécessite de s'accorder sur un format de papier.
Les largeurs, longueurs et orientations sont normalisées, des standards sont établis, ils permettent alors de concevoir des imprimantes qui peuvent gérer des types définis de papier.
Sans des formats de papier il est difficile de créer des machines adéquates, nous pouvons imaginer la complexité d'un appareil capable de s'adapter à n'importe quelle dimension de papier.
L'usage du format dans l'imprimerie est sans doute la première apparition de ce terme technique {{< cite "volmar_formats_2018" "15-16" >}}, le _format_ est ainsi d'abord attaché au livre et à sa fabrication.
Nous pouvons noter que des outils ou des processus sont associés au format : les instructions sont définies pour qu'une action soit réalisée par un agent — humain, analogique, mécanique, numérique.

Dans le champ littéraire le format qualifie la façon dont les mots sont composés et dont le texte s'agence, par exemple de la poésie, du théâtre ou un essai.
Un format de texte donne une information sur ce que contient ou dit ce texte, à la fois pour les personnes qui l'éditent mais aussi pour celles qui le lisent.
D'ailleurs le format du texte influence la façon dont l'objet livre qui l'accueille peut être conçu : un petit format pour un texte sans appareil critique, un format plus large pour laisser de la place aux notes et aux références bibliographiques.
Que ce soient les formats de papier ou les formats littéraires, nous nous accordons sur des définitions communes, sur des éléments qui sont clairement énoncés et interprétables — par des machines ou des humains comme nous pouvons le constater dans les exemples cités.

> […] formats ­represent­ the­ necessary­ forms ­of­ structuring­ and ­delivering ­media ­that­ coordinate­ between­ infrastructures ­and ­users. {{< cite "jancovic_format_2019" >}}

La circulation des données est possible dans un système d'information — qu'il soit informatique ou non — grâce au format.
Nous prenons comme postulat que tout média est aujourd'hui un système d'information.
Il semble donc que, sans format, pas de média.

### Format et média : un enjeu littéraire
Les formats constituent les rouages des systèmes médiatiques qui nous entourent.
En observant ce qui constitue les médias nous définissons les formats et nous analysons leur articulation avec leur environnement.
Dans l'approche proposée par Marshall McLuhan, notre regard ne doit pas se focaliser sur l'information, le message, mais sur ce qui permet la constitution et la structuration de cette information.
Si l'auteur de _Pour comprendre les médias_ {{< cite "mcluhan_pour_1968" >}} ne mentionne pas directement le terme de _format_, c'est pourtant ce qui apparaît en creux à différentes reprises dans son ouvrage.
Nous pouvons prendre deux exemples qui illustrent le rapport entre format et média.
La création de la typographie puis de l'impression à caractères mobiles est, pour Marshall McLuhan, "la première expérience de mécanisation d’un métier complexe" {{< cite "mcluhan_pour_1968" "199" >}} : la reproductibilité des livres est permise par des formats, ceux des caractères, des feuilles et des machines.
Convenir d'une dimension pour les caractères de plomb ou celle d'une feuille de papier permet de mettre en place des processus de réplication et de reproduction, à l'échelle artisanale ou industrielle.
Les caractères peuvent être fabriqués plus facilement puis échangés entre des ateliers différents, même chose pour les feuilles de papier dont les tailles sont connues et que les fabricants peuvent produire et diffuser sans devoir prendre un compte un nombre trop important de formats.
Pourquoi parler de mécanisation ici ?
Elle précède l'automation, concept auquel Marshall McLuhan consacre un chapitre complet {{< cite "mcluhan_pour_1968" "391-404" >}}, et qui définit les prémises des nouveaux médias via l'utilisation de l'électronique et de l'informatique.
La technologie électrique permet d'abandonner la fragmentation propre à la mécanisation, l'enjeu est la formulation des directives, c'est-à-dire la façon dont les informations sont structurées pour pouvoir être interprétées par un programme informatique.
En passant de la mécanisation à l'automation, le processus prime par rapport à la transformation, la codification des instructions importe plus que l'application de ces instructions.
C'est l'ère de la programmation.
Avec l'automation peuvent apparaître les _nouveaux médias_, Lev Manovich en propose une analyse fine {{< cite "manovich_langage_2010" >}}, et démontre la nécessité d'étudier ce qui les composent : la formalisation des informations et des processus, les pratiques associées, la circulation des informations, la constitution d'interfaces, etc.
Le texte — en tant qu'objet littéraire — et le livre sont sujet à ces évolutions, de la mécanisation vers l'automation.
Définir les formats est un enjeu aussi littéraire.

### Le format informatique
Un _format informatique_ est une spécification technique, il structure des informations, il est le pivot entre une organisation logique et son implémentation dans un système informatique.
Un fichier doit avoir un format, sans quoi il ne pourra être produit, transmis ou lu.
Un format informatique est le lien entre l'infrastructure et la personne qui utilise cette infrastructure.
Le choix des formats informatiques détermine la manière dont les informations sont créés, stockées, envoyées, reçues, interprétées, affichées.
Autant dire qu'aujourd'hui ils occupent une place importante dans notre environnement, et leur incidence dépasse le domaine de l'informatique, leur étude a pourtant été longtemps délaissée dans le champ des médias.

Le format DOC ou .doc, un format très largement utilisé depuis les années 1980, mérite une attention particulière.
Il est le produit d'un logiciel, le traitement de texte Microsoft Word — que nous appellerons _Word_ — un outil d'écriture très répandu dans plusieurs domaines dont la littérature et l'édition {{< cite "kirschenbaum_track_2016" >}}.
Le format et le logiciel sont conçus pour inscrire et mettre en forme du texte sur le modèle de la page.
Word ne peut pas lire n'importe quel format informatique, il faut que les données soient structurées d'une façon précise pour que le logiciel puisse les comprendre, et ensuite les modifier, et enfin produire une nouvelle version du fichier.
En d'autres termes, le format DOC a été créé pour les besoins d'un logiciel spécifique.
Dans notre exemple c'est le format informatique qui est le fruit du logiciel, nous verrons plus loin que d'autres fonctionnements s'imposent désormais.
Par ailleurs, le format DOC est longtemps resté propriétaire, ses spécifications n'étaient pas publiques et des brevets empêchaient toute initiative de développement d'un logiciel autre que Word capable de lire ou de modifier des fichiers .doc.

Nous avons défini les termes de format et de format informatique, démontrant le lien entre ces notions et les médias, et plus particulièrement les domaines de la littérature, du livre et de l'édition.
Les formats informatiques occupent une place déterminante dans les processus d'édition et de publication.
Analyser les formats informatiques aujourd'hui est un moyen de comprendre l'évolution de l'édition et plus particulièrement de l'édition numérique.
Réaliser une étude systématique des formats permet d'envisager une phénoménologie de l'édition.
Dans un premier temps nous analysons l'influence des formats sur les pratiques d'édition, en montrant la filiation qui existe entre la _mécanisation_ et l'_automation_ — pour reprendre les concepts de Marshall McLuhan.
Cette partie se concentre sur les formats utilisés dans l'édition.
Sans être exhaustive notre démarche entend démontrer combien les choix de formats agissent sur les pratiques éditoriales.
Dans un deuxième temps nous analysons les technologies d'édition numérique, afin de comprendre la relation entre format et processus d'édition et de publication.
Cette recherche doit porter autant sur les manières de _fabriquer_ que sur l'usage des artefacts produits — ceux-ci ont eux-même des formats —, révélant autant des pratiques de recherche que des détournements.
La diversité des pratiques de production ou de traitement des textes est partie intégrante de l'approche pluridisciplinaire des humanités numériques.
Les formats informatiques sont autant une manifestation des changements dans l'édition que l'origine de ces changements.
Les _digital humanities_ sont à la fois un dispositif de recherche, une méthodologie et un champ d'expérimentation, elles constituent le cadre critique pour mener nos recherches.

## Pratiques d'édition
Formes, formats et langages sont des éléments constituants de l'édition, des conditions d'existence du livre pourrions-nous dire.
Afin de confirmer l'hérédité entre des processus d'édition à différentes époques, nous analysons ce que sont les formes du livre et leur lien avec les formats.
Cela nous permet de définir l'édition numérique comme une évolution de l'édition sans en être un champ parallèle.
Enfin, pour expliquer le fonctionnement de l'édition numérique nous présentons des types spécifiques de formats informatiques.

### Production des formes
Pour aborder les questions de formats informatiques et de pratiques d'édition, nous devons faire un détour par les _formes_ du livre.
L'apparition du codex, cet assemblage de feuilles pliées venu concurrencer le rouleau de papyrus remplaçant lui-même les tablettes de glaise, a profondément modifié la consultation, la circulation et le stockage du livre.
D'abord manuscrit très souvent imposant, le livre devient imprimé pour faciliter sa production et sa diffusion.
L'impression à caractères mobiles utilise d'abord les formats alors en vigueur, puis elle en crée de nouveaux avec la création d'ouvrages de plus petite taille facilement manipulables — nous pouvons évoquer ici l'entreprise pionnière d'Aldo Manuzio et ses livres au format in-octavo {{< cite "manguel_histoire_1998" "167-169" >}}.
Un phénomène semblable a lieu avec le livre numérique et les premiers formats qui reproduisent la pagination, suivi par des formes plus éloignées du codex.

Nous pouvons noter deux éléments contribuant à susciter un intérêt sur les formats dans le cas du livre.
Tout d'abord la filiation qui existe dans les avancées technologiques : l'impression à caractères mobiles utilise les acquis de l'édition manuscrite sans la détruire, il y a une progression commune et non un remplacement.
Ce propos est d'ailleurs également celui de Marshall McLuhan {{< cite "mcluhan_pour_1968" "203" >}}, ou plus récemment de Louise Merzeau {{< cite "merzeau_ceci_1998" >}} avec une profondeur de champ plus contemporaine.
Ensuite, le format de l'artefact est autant la marque de fabrication de l'éditeur que l'expression des usages des lecteurs et des lectrices.
Pour des raisons économiques le livre de poche émerge au début du XIX<sup>e</sup> siècle : sur une idée d'un éditeur (Allen Lane) la collection Penguin Books souhaite en effet faciliter l'acquisition de textes littéraires avec des objets accessibles {{< cite "manguel_histoire_1998" "177" >}} — accessibilité financière mais aussi matérielle avec un livre facilement manipulable.
Ici, le format est créé pour susciter de nouveaux usages, non permis par les formats alors en vigueur.
Cette hybridation des formes et des formats est aussi un phénomène observable dans les premières expérimentations numériques : la littérature électronique vient compléter des formes déjà existantes, les artefacts imprimés et numériques s'hybrident {{< cite "ludovico_post-digital_2016" >}}.
Le deuxième élément concerne le passage de l'édition au numérique dès les années 1980 : les premières initiatives, notamment l'usage d'un traitement de texte ou d'un logiciel de publication assistée par ordinateur, révèlent une volonté d'utiliser les outils informatiques pour dupliquer les processus jusqu'ici à l'œuvre.
Autrement dit, les mêmes approches sont mimées, sans pour autant embrasser les potentialités du numérique.
L'utilisation de Word en est un bon exemple : les débuts du traitement de texte sont d'abord accueillis avec beaucoup d'enthousiasme par les auteurs et les autrices de littérature, mais lorsque le traitement de texte de Microsoft se complexifie et devient un outil bureaucratique plus qu'un outil littéraire, le constat est sans appel pour certains :

> [Charles] Stross unsparingly lashed Word as "a tyrant of the imagination, a petty, unimaginative, inconsistent dictator that is ill-suited to any creative writer’s use." {{< cite "kirschenbaum_track_2016" "236" >}}

L'édition semble mimer la mécanisation avec les moyens numériques, il est alors difficile de parler d'_édition numérique_, et les formats sont encore très influencés par les pratiques issues de l'impression à caractères mobiles.
Les formats informatiques les plus utilisés par l'édition généraliste dans les deux dernières décennies du XX<sup>e</sup> siècle sont probablement les formats DOC et PDF : l'un est propriétaire, impossible à utiliser en dehors du logiciel Microsoft Word ; l'autre est un émulateur d'impression, fossilisant le texte pour le meilleur (un même fichier est lisible par toutes et tous de la même façon) et pour le pire (pourquoi ne pas profiter des possibilités du numérique pour afficher du texte liquide ?).
Pouvoir utiliser un même format dans plusieurs logiciels, voir dans plusieurs systèmes d'exploitation, est encore loin d'être possible, même si, comme nous le verrons par la suite, des pratiques marginales apparaissent déjà.
À la fin du XX<sup>e</sup> les pratiques semblent uniformes — LaTeX cohabite pourtant avec les premiers traitements de texte —, pourtant ces choix de logiciels ne sont pas pour autant neutres.
Comparé à LaTeX qui permet de gérer l'écriture et la composition d'un texte avec une instantanéité et une synergie des différentes phases d'édition, le duo traitement de texte et logiciel de publication assistée par ordinateur conduit à un processus linéaire, fractionnel et centralisateur.
Comme nous le verrons par la suite, le choix du format informatique pour concevoir et fabriquer des livres fait partie du geste éditorial {{< cite "ouvry-vial_acte_2007" >}}.

### Édition numérique
Nous le répétons : pour étudier l'évolution de l'édition numérique nous pouvons analyser les formats informatiques qui y sont utilisés.
L'édition numérique n'apparaît que lorsque les moyens informatiques ne sont plus utilisés à des fins de reproduction des processus mécaniques, mais en prenant en compte les changements — parfois radicaux — imposés par le numérique.
L'informatisation des métiers de l'édition a été quelque peu bousculée par l'arrivée du livre numérique au tournant des années 2006-2007 — date à laquelle des liseuses à encre électronique bon marché sont commercialisées, où des catalogues de titres numériques sont disponibles et où un format standard est mis en place.
Comme le soulignent Benoît Epron et Marcello Vitali-Rosati dans _L'édition à l'ère numérique_ {{< cite "epron_ledition_2018" >}}, le choix des formats informatiques est d'abord celui des environnements de lecture numérique : à la fois en terme de matériels de lecture (liseuses, tablettes, etc.) et de mode de lecture (contenus figés, reformatables ou protégés).
L'adoption d'un standard pour le livre numérique — le format EPUB — modifie en partie le paysage de ces écosystème éditoriaux et numériques, sans pour autant devenir une norme adoptée par tous les acteurs — Amazon reste par exemple l'un des seuls acteurs à préférer son format propriétaire AZW au standard EPUB.

Le format EPUB {{< cite "marcoux_livrel_2014" >}} est un standard créé pour pouvoir commercialiser le livre numérique.
Il est à la fois un format informatique interopérable, une spécification technique documentée, et un argument pour convaincre les éditeurs et les plateformes de vente de faire consensus.
En s'accordant sur un même format, les éditeurs, les distributeurs et les libraires facilitent la gestion et la circulation des fichiers.
Un fichier EPUB est un site web encapsulé, un dossier compressé qui embarque les textes structurés au format XHTML, la mise en forme gérée avec des feuilles de style CSS, des contenus multimédia, et des scripts pour certaines interactions.
La particularité de ce format est d'être liquide, c'est-à-dire que le texte peut être graphiquement reconfiguré selon de nombreux paramètres.
Taille du texte, police typographique, marges, interligne, couleurs du texte et du fond : autant de possibilités pour adapter l'environnement de lecture.
Si l'affichage privilégié sur les dispositifs de lecture numérique est la page, celle-ci est à tout moment recomposable.
C'est un codex en flux.
Il est également possible de choisir une interface déroulable, à la manière d'un volumen ou d'une page web.
Les spécifications techniques de l'EPUB étant accessibles à toutes et tous, il est théoriquement possible de créer une application de lecture qui comprend l'EPUB, mais aussi des logiciels de fabrication de fichiers informatiques dans ce format.
Pour des structures qui étaient dans le mimétisme de l'impression à caractères mobiles, qui appliquaient des processus centrés sur la page, le changement est vertigineux.
De l'édition de documents paginés à l'édition de flux textuels : ce sont des nouvelles compétences qu'il faut acquérir, des réflexes qu'il faut abandonner.
La reconfiguration d'un livre à chaque lecture, voilà ce que permet le format EPUB.
Pour des typographes cette perspective n'est pas forcément réjouissante, pas plus que pour un auteur ou une autrice qui pourrait regretter que leur texte soit lu dans des conditions incontrôlées.
Le résultat est un design en partie génératif, la composition est automatisée selon un certain nombre de règles et de contraintes, sans intervention manuelle pour, par exemple, éviter des erreurs typographiques (gouttière dans un texte justifié par exemple).
Sans être un format d'archivage pérenne, l'EPUB est un format standard qu'il est possible de conserver dans le temps.
Les fichiers qui le composent sont lisibles avec un simple éditeur de texte, il n'est pas nécessaire de disposer d'un logiciel spécifique.

Travailler avec le format EPUB c'est donc adopter une interopérabilité pour les méthodes de travail et pour les pratiques de lecture.
D'un côté les structures d'édition peuvent choisir le logiciel de travail pour créer, modifier et lire le format EPUB, ce qui est impossible pour des opérations de composition typographique habituellement réalisées avec des logiciels comme InDesign.
De l'autre côté les lectrices et les lecteurs disposent d'un choix de logiciels ou d'applications de lecture qui sont en capacité de lire ce format.
InDesign, Sigil ou des éditeurs de texte comme Atom ou Codium permettent d'interagir avec le format EPUB.
L'arrivée d'un nouveau format qui vient s'ajouter au PDF utilisé pour l'impression engendre des temps et des coûts de production plus importants.
Une solution est de considérer la création de plusieurs artefacts à partir d'une même source, pour réduire les temps de modification : une correction implique alors une seule modification.
Le _single source publishing_ est ce principe de publication qui consiste à générer plusieurs formats de sortie (ici le PDF et l'EPUB) à partir d'une même source.
Si ce fonctionnement est une voie prometteuse pour les structures d'édition, il nécessite plusieurs ajustements de la chaîne d'édition, et implique ainsi d'utiliser des formats particuliers.
Nous détaillons ce point dans la suite de notre exposé, avant cela nous devons expliquer quels types de formats sont utilisés en édition numérique.

### Des types de formats informatiques
En édition numérique les formats informatiques utilisés sont nombreux, il est difficile d'en faire un recensement exhaustif en quelques lignes, nous nous proposons plutôt d'étudier un type de format en particulier.
Pour résumer cette partie nous pourrions clamer que l'enjeu est de structurer le texte, comme nous avons déjà pu le voir avec le format EPUB.

L'édition s'est constituée autour des techniques, nous pouvons évoquer trois périodes qui permettent de prendre la mesure de cette relation : la mécanisation avec l'impression à caractères mobiles ; une phase de numérisation transitoire, ou l'édition numérique rime avec reproduction sans prise en compte des potentialités ; l'édition numérique qui embrasse les processus numérique et les formats disponibles, le format EPUB en étant un exemple.
Nous avons déjà mentionné des formats informatiques comme DOC ou INDD, chacun étant produit et associé à un logiciel spécifique.
L'édition numérique est basée sur des types de formats qui lui sont propres {{< cite "bouletreau_les_2017" >}}, pour la gestion du texte il s'agit des formats de balisage.
Les formats informatiques de balisage sont aussi appelés des _langages_ de balisage, cette distinction est importante et apporte une dimension nouvelle concernant le rapport entre humain et machine {{< cite "cramer_language_2008" >}}.
Les langages de balisage utilisent des balises pour qualifier des portions de texte.
Une série de caractères peut être délimitées comme une unité de sens grâce à un balisage spécifique, ainsi `<prenom>Antoine</prenom>` permet d'identifier que les lettres `Antoine` sont un prénom.
L'acte d'encoder le texte {{< cite "renear_text_2004" >}} fait alors partie intégrante du geste éditorial {{< cite "ouvry-vial_acte_2007" >}}.

Chaque langage de balisage a ses spécifications propres.
Le langage HTML dispose d'un certain nombre d'éléments, un élément a une valeur sémantique, par exemple `<h1>Titre</h1>` identifie un titre et plus spécifiquement un titre qui a le plus haut niveau dans un arbre sémantique.
Certains langages comme le XML sont extensibles via l'association d'un schéma qui précise quelles sont et comment fonctionnent les balises.
Structurer un texte avec le format HTML impose des contraintes fortes : il n'est possible d'utiliser que les balises disponibles.
Les différentes balises de l'actuel version 5 de HTML et le fonctionnement en arborescence sont amplement suffisants pour l'édition d'un texte qui est lu sur des dispositifs de lecture classique.
En revanche pour un texte qui nécessite une structuration fine, comme le fait de baliser chaque nom ou prénom, le langage XML se révèle plus adéquat, quoique plus complexe à utiliser.
Il ne faut pas limiter XML à une série de balises encadrant des segments de texte, l'arbre sémantique de XML peut être parcouru de façon dynamique.
Nous pouvons préciser qu'un texte balisé en XML permet de rendre le texte interrogeable, par exemple en proposant un moteur de recherche qui interroge tous les noms ou prénoms d'un corpus de textes pour reprendre notre exemple précédent.

En fonction des artefacts qui sont produits, c'est-à-dire des objets numériques qui seront consultés, un format informatique est plus pertinent qu'un autre.
À des pratiques éditoriales correspondent des formats informatiques.
Dans _A New Republic of Letters_ Jerome McGann explique que les humanités, et plus particulièrement la littérature, requiert de structurer les textes et pas seulement de les _numériser_.
Le format PDF est par exemple bien trop limité pour exprimer toutes les nuances d'un texte {{< cite "mcgann_new_2014" "135" >}}, pour révéler toutes les dimensions inscrites dans un fichier.
Faciliter les recherches dans des corpus littéraires impliquent de faire des choix de format comme des langages de balisage.
C'est aussi le propos de Geoffrey Rockwell et Stéfan Sinclair {{< cite "rockwell_hermeneutica_2016" >}}, pour pouvoir être utilisé dans certains cas, un texte doit être structuré, et cela implique des formats spécifiques.

Ce bref panorama confirme l'importance des formats informatiques dans les processus d'édition numérique.
Nous allons continuer cette recension de formats en analysant cette fois les technologies de l'édition numérique, afin d'observer le changement de paradigme à l'œuvre : les formats informatiques qui sont des standards permettent de construire un environnement riche.

## Technologies d'édition numérique
Nous nous proposons tout d'abord de présenter l'évolution des technologies d'édition et de publication, et de voir à quel point cette évolution est liée aux formats.
Nous détaillons ensuite le changement de paradigme, changement suscité par des formats de balisage et les pratiques d'encodage du texte.
Le domaine des humanités numériques est à la fois un terrain d'observation et un champ d'expérimentation de ces usages.
Enfin, des détournements apparaissent pour répondre aux défis de l'édition numérique, en s'inspirant de pratiques issues d'autres domaines.

### Panorama
Un imposant travail de recension des différentes technologies d'édition numérique a été réalisé par Julie Blanc et Lucile Haute sous forme d'une ligne du temps {{< cite "blanc_technologies_2018" >}}.
Cette recherche, qui ne se prétend pas exhaustive mais qui est pourtant une initiative inédite et un apport majeur, rassemble des systèmes d'exploitation, des matériels informatiques, des logiciels de lecture, des logiciels de publication, des applications pour le texte, et des formats et des langages.
Il est très intéressant de voir quelle évolution empruntent à la fois les machines, les programmes et les formats informatiques, mais aussi l'apparition de plateformes et de services en ligne qui viennent supplanter certains logiciels.

Prenons quelques exemples pour comprendre des éléments de cette évolution.
Le langage de balisage et logiciel de composition TeX apparaît à la fin des années 1970, presque dix ans après l'un des premiers langages de balisage, SMGL, mais vingt ans avant les premières spécification de XML.
TeX est complexe à utiliser, il est un puissant système de composition (typo)graphique de documents imprimés, et prend en compte des objets comme les formules mathématiques.
LaTeX fait son apparition dans l'objectif de rendre TeX plus accessible.
SGML est un langage de balisage qui est, lui aussi, difficile à manier.
XML est une réponse pratique, ambitieuse et modulaire pour rendre l'encodage de texte plus simple, le format XML pouvant ensuite être converti dans d'autres formats.
SGML, XML, TeX et LaTeX sont chacun, à leur manière, une tentative de distinguer le fond et la forme, la structuration du texte et sa mise en forme.
LaTeX, par sa philosophie même, embarque pourtant des informations concernant la composition du texte {{< cite "allington_latex_2016" >}}, contrairement à XML qui est un langage _purement_ sémantique.
Ces formats informatiques n'ont pas de logiciel associé, c'est-à-dire qu'ils ne sont pas le produit d'une application.
Interagir avec ces formats peut se faire avec des logiciels spécialisés — que nous ne listons pas ici —, ou avec un éditeur de texte qui lit n'importe quel texte brut.
L'enjeu autour de XML est particulier, puisque des outils de conversion ou d'aide à la saisie peuvent venir grandement faciliter sa manipulation.

D'autres formats de balisage font leur entrée au début des années 2000.
AsciiDoc est créé en 2002 pour faciliter un peu plus l'encodage du texte : à partir de quelques signes typographiques, sans balises comme celles utilisées avec XML ou LaTeX et qui alourdissent grandement l'écriture et la lecture, un texte peut être _sémantisé_ puis converti dans de multiples formats.
AsciiDoc est aussi un convertisseur, en plus d'être un langage de balisage c'est un programme chargé de convertir des fichiers aux formats HTML ou XML (nous pouvons également mentionner le convertisseur Asciidoctor développé à partir de 2013).
L'éditeur O'Reilly utilise AsciiDoc pour une partie de ses ouvrages techniques, et transforme ce format en XML avec un schéma nommé DocBook, les versions finales et distribuées étant l'EPUB, le HTML ou le PDF.
Ce qu'il est intéressant de noter ici, c'est l'adaptation à des pratiques d'édition numérique via l'adoption de langages de balisage plus faciles d'accès.
D'ailleurs AsciiDoc est un langage de balisage _léger_, en comparaison de la verbosité des balises d'un langage comme XML.

Qu'est-ce que peuvent permettre ces formats non figés et parfois extensibles ?
C'est l'objet de notre partie suivante.

### Changement de paradigme
L'application des principes de l'informatique à la structuration du texte est un événement majeur pour l'édition.
La question n'est pas de savoir si les pratiques ont déclenché l'invention d'innovations technologiques comme XML ou AsciiDoc, ou l'inverse — quoique nous y répondons en partie plus loin.
Nous constatons que des pratiques émergent avec l'irruption de certains formats, et que des formats sont créés à l'initiative de certaines pratiques.

Les humanités numériques fourmillent — n'aillons pas peur des mots — d'exemples où les formats informatiques ont permis de donner une nouvelle dimension aux textes.
Nouvelle dimension dans les possibilités de créations littéraires, d'éditions critiques, d'analyses des textes ou de productions de livres.
La littérature électronique ou numérique s'est en partie constituée autour des possibilités offertes par l'hypertexte, le format HTML et l'écosystème du web {{< cite "hayles_electronic_2008" >}}.
La démarche de création est aussi une démarche de recherche :

> A novelist working in hypertext is not only writing a fiction, but also experimenting with alternative models of textual navigation and user interaction. {{< cite "rettberg_electronic_2016" "129" >}}

La réalisation d'éditions critiques en contexte numérique — pour reprendre le nom du laboratoire GREN, Groupe de recherche sur les éditions critiques en contexte numérique —, a été, dès le début des années 2000, un travail conjoint d'encodage et de formalisation d'un standard : le schéma TEI (Text Encoding Initiative) pour le format XML {{< cite "burnard_quest-ce_2015" >}}.
Comme souvent dans la mise en place de spécifications techniques d'un format ouvert (donc non propriétaire), une communauté se constitue autour d'un standard.
La Text Encoding Initiative consiste en une série de documents permettant de délimiter un schéma XML, mais ce travail ne peut pas se faire sans les nombreux échanges qui constituent la matière pour mettre en place une série de règles.
Pourquoi encoder les textes ?
Les usages sont nombreux, que ce soit la production d'artefacts divers qui seront consultés, lus et explorés via différents canaux.
Les corpus littéraires peuvent être analysés grâce au balisage des noms de personnes, des noms de lieux ou des concepts — pour ne prendre que quelques exemples.
Des versions différentes peuvent être produites, comme une interface web affichant les données liées aux éléments balisés (une carte pour un nom de lieu) ou une version imprimable avec des index.
L'encodage est un moyen de rendre les textes plus exploitables, comme nous l'avons déjà souligné.
Mais sans un cadre pour réaliser cette tâche, l'entreprise serait d'une part extrêmement longue (la définition d'un schéma est une opération complexe qui demande du temps) et d'autre part vouée à l'échec à moyen ou long terme (maintenir un schéma cohérent est une tâche ardue).
L'enjeu n'est plus la reproduction d'un document de façon homothétique comme cela a été le cas avec les premières initiatives de numérisation — la conversion ne consistait qu'en la prise de vue des pages des livres, alors disponibles au format image, images encapsulées dans des PDF peu exploitables —, mais la structuration fine des informations qui composent un texte.

Nous venons d'évoquer l'édition multimodale, cette capacité de produire plusieurs versions d'un même texte — typiquement une version imprimée et une version numérique.
Cette notion est en lien avec le _single source publishing_, déjà abordé, qui ajoute à l'édition multimodale la capacité de produire ces différents formats de sortie à partir d'une même et seule source (source qui peut être composée de plusieurs fichiers).
En plus de pouvoir rendre un texte exploitable dans le cadre de recherches scientifiques, l'encodage avec XML permet d'envisager la production d'artefacts très divers tout en n'ayant à manipuler une seule source.
Plus qu'un gain en terme de coûts ou de temps, cette pratique change le rapport au texte : les indications de mise en forme doivent se faire sur la base d'une structuration, et non plus sur le rendu graphique avec la contrainte de la page.
La limite de ce type de démarche est liée aux compétences nécessaires pour travailler avec le format XML et un schéma associé, mais aussi à l'utilisation des logiciels permettant d'interagir avec ce format, ou encore à la complexité du fonctionnement de certaines chaînes d'édition.
Comment certains formats peuvent permettre de contourner ces difficultés, et comment des systèmes de publication basés sur ces formats facilitent l'édition ?
L'ambition de notre dernière partie est de donner des pistes de réponses pratiques à ces questions.

### Détournements
Il est donc possible d'établir des démarches d'édition numérique grâce à des formats ou langages de balisage.
Désormais des formats informatiques existent indépendamment des logiciels qui permettent de les lire et de les manipuler.
Des standards ouverts ont permis l'émergence de nouveaux processus de publication.
Nous nous proposons, pour cette dernière partie qui fait office de conclusion, d'explorer des détournements de formats.

En 2004 John Gruber et Aaron Swartz écrivent les spécifications d'un nouveau langage de balisage léger, Markdown.
À la suite d'AsciiDoc ou d'autres systèmes de "scripturation" {{< cite "perret_histoire_2020" >}}, Markdown s'impose par sa simplicité : grâce à quelques symboles typographiques il est possible de structurer un texte.
Niveaux de titres, listes ordonnées ou non ordonnées, emphases, images, liens hypertextes, citations, Markdown répond à un réel besoin, celui d'écrire en HTML sans avoir à écrire en HTML.
La verbosité des langages de balisage _non léger_ est un frein à l'adoption de textes structurés, cette structuration permettant une interopérabilité (lecture sur plusieurs supports) et un traitement du texte (encodage du texte).
Markdown a besoin d'un convertisseur, c'est également le cas pour AsciiDoc déjà cité, le format seul n'a d'intérêt que comme espace de travail.
Le plus souvent transformé en HTML, Markdown peut également être converti en d'autres formats balisés, voir en PDF via des étapes intermédiaires ou des processeurs dédiés.
Si l'adoption de ce langage de balisage léger est importante {{< cite "fauchie_markdown_2018" >}}, ces limites dans le domaine de l'édition numérique sont vite perçues.
Markdown est un moyen simple de baliser un texte, d'autant plus avec des applications d'écriture qui peuvent faciliter l'usage des balises — c'est le cas de logiciels comme iAwriter ou Typora, ou plus récemment Zettlr pour le domaine académique —, mais comment parvenir à atteindre la richesse d'un format comme XML ?

Markdown, comme AsciiDoc précédemment, est compréhensible par les humains et les machines, facilement manipulables en _texte brut_, et il dispose de nombreux logiciels qui permettent de l'afficher ou de le modifier.
À la suite de la création de ce format, un écosystème très riche, quoique peu standardisé, se constitue.
Ce sont de nombreuses initiatives de conception d'espaces d'écriture et d'édition qui deviennent accessibles à un plus grand nombre de personnes.
Faciliter l'accès à des formats informatiques complexes est une entreprise singulière, elle ouvre de nouvelles possibilités à celles et à ceux qui étaient jusqu'ici habituées à utiliser un logiciel plutôt qu'un format.

> The behavior of the writing space becomes a metaphor for the human mind as well as for human social interaction. {{< cite "bolter_writing_2001" "13" >}}

La question légitime qui se pose ici est la façon dont Markdown peut être utilisé dans des environnements qui requièrent une sémantique plus détaillée.
Il faut d'abord souligner que Markdown n'est pas un standard, il existe plusieurs _saveurs_ de Markdown qui permettent par exemple de gérer les notes de bas de page.
Certaines initiatives comme CommonMark vises à établir un standard, afin de clarifier les nombreuses adaptations du format et d'étendre certaines pratiques {{< cite "mpondo-dicka_markdown_2020" >}}
La question de la conversion offre une opportunité d'augmenter un balisage léger comme Markdown.
Pandoc, un programme informatique de conversion de langages de balisage, ou Hugo, un générateur de site statique, sont deux programmes informatiques qui font office de convertisseurs.
Des balisages supplémentaires, propres à ces deux programmes, offrent la possibilité d'intégrer des blocs sémantiques dans des documents au format Markdown.
Nous pouvons prendre deux exemples : avec Pandoc il est possible de baliser des portions de texte en attribuant une classe : `[ceci est un concept clé du texte]{.concept}` devient `<span class="concept">ceci est un concept</span>`.
Hugo a mis en place un système de _shortcodes_ qui consiste à définir des blocs sémantiques à partir d'indications {{< cite "fauchie_fabriques_2021" >}}, système plus puissant que ce que propose Pandoc : en plus d'ajouter un balisage au texte, les _shortcodes_ sont presque des documents dans le document, tant leurs paramètres peuvent être nombreux et leur construction complexe {{< cite "hakimian_parsiyahugo-shortcodes_2021" >}}.

L'association de Markdown à des formats de sérialisation permet de gérer des données _satellites_ comme les métadonnées.
Pourquoi présenter les formats de sérialisation si nous nous concentrons sur le texte en édition numérique ?
Les textes peuvent être accompagnés de données, comme les métadonnées qui définissent le document, un jeu de données ou encore des références bibliographiques structurées.
Les formats YAML, TOML ou JSON sont des formats de sérialisation, ils permettent de structurer des informations facilement et de les proposer à des logiciels ou des programmes informatiques sous forme de fichiers en plein texte plutôt que sous forme de base de données.
L'avantage ici est que ces formats sont plus simples à manier qu'une base de données relationnelles, et que l'infrastructure qui les portent est beaucoup plus légère — en ressources techniques comme en conception d'infrastructure.
Ils fonctionnement non pas avec un système de balises, mais avec des règles simples de séparation des informations selon une arborescence et un arbre hiérarchique de données.
Lisible par des humains, les formats de sérialisation reposent sur des spécifications limitées — cela ne veut pas dire pour autant que leur usage ne peut pas engendrer d'erreurs.
S'il est possible de structurer du texte avec un format de sérialisation comme le JSON, cette pratique est loin d'être recommandée.
Combiner des formats de balisage léger avec des formats de sérialisation de données et des interfaces accessibles permet de concevoir des chaînes d'édition ou de publication puissantes, c'est d'ailleurs ce qu'a fait le musée Getty avec l'outil Quire {{< cite "fauchie_les_2020" "6-8" >}} ou la Chaire de recherche du Canada sur les écritures numériques avec Stylo {{< cite "vitali-rosati_ecrire_2020" >}}.

Utiliser Markdown et les outils associés est un acte de détournement d'objets numériques issus d'autres domaines que celui de l'édition ou de la littérature.
C'est sur ce point que nous souhaiterions conclure notre texte.
Ce détournement a une dimension littéraire, en cela qu'il s'agit de privilégier le texte et ses balisages, plutôt que des logiciels opaques dont le fonctionnement est difficilement compréhensible.
Par ailleurs, les différents efforts qui visent à rendre une chaîne d'édition numérique plus accessible instaurent un environnement d'apprentissage.
D'une certaine façon le programme annoncé par Marshall McLuhan dans son chapitre sur l'automation {{< cite "mcluhan_pour_1968" "391-404" >}} semble se réaliser.
En favorisant le processus plutôt que la suite linéaire de tâches figées, en remplaçant l'application de fonctionnements incompréhensibles par la formulation d'instructions, nous transformons les pratiques d'édition en apprentissage continu.
Réaliser simultanément des phases de révision et de composition typographique est désormais possible, cela est permis par des formats qui sont agnostiques et ouverts, compréhensibles et interopérables, pérennes et sémantiques, nativement littéraires.
En guise d'ouverture nous souhaitons émettre une hypothèse à la suite de ces différents éléments : un nouveau geste d'édition, émergeant, ne consisterait-il pas en la conception de livres tout autant que l'élaboration de ses fabriques de publication ?

Ce court essai ne constitue pas une étude complète des formats informatiques utilisés dans l'édition numérique, mais il donne à voir l'évolution des pratiques et des processus qui les formalisent.
C'est une phénoménologie de l'édition numérique que nous avons pu parcourir, à travers les formats et les systèmes qui les utilisent ou les exploitent.
Les possibilités actuelles et à venir du texte {{< cite "hegland_future_2020" >}} reposent sur cette progression, sur ce cheminement technique et littéraire.

## Bibliographie

{{< bibliography cited >}}