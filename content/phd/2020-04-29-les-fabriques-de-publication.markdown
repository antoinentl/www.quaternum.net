---
layout: post
title: "Les fabriques de publication"
date: "2020-04-29T21:00:00"
comments: true
published: true
description: "J'entame une série de billets sur des fabriques de publication.
J'amorce des analyses rapides de divers systèmes de publication pour défricher et échanger sur ces questions."
categories:
- carnet
- phd
classement:
- phd
status:
aliases:
  - /fabriques
---
J'entame une série de billets sur des fabriques de publication.
J'amorce des analyses rapides de divers systèmes de publication pour défricher et échanger sur ces questions.

<!-- more -->

Ce billet introductif a pour simple but d'expliquer ma démarche, et de présenter quelques précautions nécessaires.
À travers une quinzaine de fabriques présentées en quelques paragraphes, je me propose de rassembler le matériel nécessaire à des analyses plus poussées.
Plutôt que de retarder le démarrage de ces analyses je préfère commencer vite, quitte à être imparfait et trop bref.

Au cœur de ces analyses réside la question de la dénomination de ces initiatives, du concept qu'il convient de convoquer.
Outil, instrument, dispositif, machine.
Technologie, technique, méthode.
Chaîne, système, fabrique, forge.
Le choix du terme _fabrique_ est fait par défaut, et aussi parce qu'il revêt une dimension collective qui correspond aux programmes et logiciels présentés.
L'enjeu de ces analyses est également de pouvoir m'arrêter sur une notion ou un concept qui convient à ma démarche, et de disposer d'éléments réutilisables pour la suite.

Si l'origine de ces analyses est une recherche scientifique, je ne prétends pas répondre à toutes les exigences requises par une démarche et une méthode scientifique.
C'est un travail en cours, fait avec le plus grand sérieux certes, mais qui évoluera au fur et à mesure.

Pour naviguer dans ces _technologies de l'édition numérique_, la consultation du recensement de Julie Blanc est particulièrement utile : [Technologies de l’édition numérique](https://recherche.julie-blanc.fr/timeline-publishing/)

Un autre travail de recensement, mené par John W Maxwell en 2019, permet de découvrir un ensemble d'outils et de plateformes de publication _open source_ : [Mind the Gap: A Landscape Analysis of Open Source Publishing Tools and Platforms](https://mindthegap.pubpub.org/)

## Liste des fabriques

1. [LaTeX](/2020/04/29/fabriques-de-publication-latex/)
2. [Pandoc](/2020/04/30/fabriques-de-publication-pandoc/)
3. [Asciidoctor](/2020/05/03/fabriques-de-publication-asciidoctor/)
4. [Jekyll](/2020/05/05/fabriques-de-publication-jekyll/)
5. [Org-Mode](/2020/05/10/fabriques-de-publication-org-mode/)
6. [Quire](/2020/05/13/fabriques-de-publication-quire/)
7. [Gabarit Abrupt](/2020/05/18/fabriques-de-publication-gabarit-abrupt/)
8. [Stylo](/2020/05/21/fabriques-de-publication-stylo/)
8. [Zettlr](/2020/08/28/fabriques-de-publication-zettlr/)

## Pour suivre cette série

- vous pouvez vous abonner [au flux RSS de ce site](/atom.xml) ;
- vous pouvez vous rendre sur cette page, dont l'URL raccourcie est la suivante : [www.quaternum.net/fabriques](https://www.quaternum.net/fabriques) ;
- vous pouvez suivre [#fabriquesdepublication sur Twitter](https://twitter.com/search?q=%23fabriquesdepublication&src=typed_query&f=live) ou [sur Mastodon](https://mamot.fr/tags/fabriquesdepublication).

## Pour réagir et échanger (ce site ne comporte pas d'espace de commentaires)

- écrire ailleurs (sur votre site personnel par exemple, c'est l'occasion d'en créer un) en m'indiquant votre publication (en attendant un système de webmention vous pouvez m'écrire à antoine[at]quaternum[dot]net) ;
- suggérer des lectures ou des modifications [via le dépôt de ce site](https://gitlab.com/antoinentl/www.quaternum.net/issues/new) (un compte GitLab sera nécessaire) ;
- interagir via les réseaux sociaux mentionnés plus haut.
