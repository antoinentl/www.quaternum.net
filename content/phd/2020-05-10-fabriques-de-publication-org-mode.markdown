---
layout: post
title: "Fabriques de publication : Org-Mode"
date: "2020-05-10T23:00:00"
comments: true
published: true
description: "Cinquième analyse d'une série sur les fabriques de publication : Org-Mode"
categories:
- carnet
- phd
classement:
- phd
status: brouillon
bibfile: data/analyses-org-mode.json
---
Cinquième analyse d'une série sur les fabriques de publication&nbsp;: Org-Mode.

<!-- more -->

[Introduction et liste des fabriques](/fabriques)

Org-Mode est un mode d'édition de l'éditeur Emacs, il s'agit d'un ensemble de composants qui permettent de produire des documents dans différents formats.
Org-Mode est un outil dont la puissance dépend d'un environnement de travail spécifique et de l'apprentissage d'une pratique d'écriture.

## Description
[Org-Mode](https://orgmode.org/) est assez peu connu, et c'est dommage car c'est un moyen de produire facilement – à condition de se placer dans un environnement particulier – des documents dans des formats très divers.
Org-Mode est intégré à l'éditeur de texte [Emacs](https://www.gnu.org/software/emacs/), c'est donc en ce sens que la production de documents PDF, EPUB ou HTML peut être faite _facilement_.
Emacs est déjà un ensemble de composants, c'est un éditeur de texte extensible, et extensible à tel point qu'il est possible de faire beaucoup d'autres choses dans Emacs que d'écrire du texte ou du code, comme naviguer sur le web ou lire des documents.
L'utilisation d'une fabrique de publication _dans_ Emacs est donc assez naturelle pour ses utilisateurs.

Org-Mode est un _mode_ de l'éditeur Emacs, sans Emacs pas de Org-Mode – il existe toutefois des applications _mobiles_ pour Android ou iOS.
Concrètement Org-Mode propose un ensemble de règles pour interagir avec des documents en plein texte, que ce soit des listes de tâches ou des documents structurés.
Pour construire un document, Org-Mode propose son propre format de balisage léger – avec des balises qui ressemblent à Markdown, rappelons que Markdown est plutôt un héritier d'initiatives telles que Org-Mode.
Voici un exemple de balisage d'un lien : `[[lien][description]]`.
Pour naviguer dans un document et modifier certains éléments il faut faire appel à des commandes spécifiques propres à Emacs, même s'il existe des alternatives graphiques.
Org-Mode intègre notamment des commandes LaTeX pour l'édition.
[La documentation](https://orgmode.org/org.html) est très complète {{< cite "dominik_org_2010" >}}.

Org-Mode s'intègre parfaitement dans l'éditeur Emacs, et c'est à la fois son avantage et son principal défaut comme nous le voyons plus loin.
Il existe ainsi beaucoup d'extensions à l'intérieur même de Org-Mode, ou via l'utilisation d'autres programmes – comme LaTeX ou même un générateur de site statique spécifique à Org-Mode : [org2web](https://github.com/tumashu/org2web).

Org-Mode est utilisé dans beaucoup de domaines, et notamment comme outil de gestion de projets scientifiques {{< cite "misshula_org-mode_2014" >}}, pour manipuler des données et rédiger des articles {{< cite "bennett_writing_2019" >}}.
Je découvre également que des auteurs ou des écrivains l'utilisent pour écrire et organiser leur travail d'écriture {{< cite "ballantyne_my_2018" >}}.

Org-Mode dispose de son propre format, `.org`, un format de balisage léger avec des informations d'entête, qui peut ressembler à AsciiDoc ou à l'association de Markdown et de YAML.

## Histoire
Org-Mode est d'abord pensé par son créateur Carsten Dominik comme outil d'organisation et de planification, il s'agit d'un éditeur de texte aux fonctions avancées imaginés pour interagir de façon dynamique avec des fichiers de listes de tâches notamment.
[Bastien Guerry](https://bzg.fr/) maintient désormais le projet Org-Mode aux côtés de nombreux contributeurs, [le programme](https://code.orgmode.org/bzg/org-mode) en tant que tel connaît des évolutions très régulières même si les fonctionnalités principales restent inchangées.
La philosophie du projet d'Emacs garantit une certaine robustesse à Org-Mode.

## Specimen
Un des meilleurs moyen de comprendre le fonctionnement de Org-Mode est probablement d'observer un document produit et sa source, ce qui est possible avec les tutoriels présents sur la page du projet, et notamment [Tutoriel Org (emacs org-mode)](https://orgmode.org/worg/org-tutorials/orgtutorial_dto-fr.html).
La source est visible via un lien en fin de page, qui mène [à cette version](https://orgmode.org/worg/org-tutorials/orgtutorial_dto-fr.org.html).
La communauté d'Org-Mode étant très active, les articles, vidéos ou billets de blog  sont nombreux et permettent de découvrir cet éditeur de texte ou apprendre des usages avancés.

## Critique
Org-Mode est une fabrique de publication intéressante pour plusieurs raisons.
La première réside dans le fait qu'il s'agit réellement d'une _fabrique_, en tant qu'ensemble de composants pensés et organisés pour permettre l'édition et la production de documents.
Il est fascinant de comparer par exemple un logiciel de traitement de texte classique et Org-Mode, ce dernier mettant en place toute une stratégie pour une utilisabilité et une efficience maximales – vous m'excuserez cette formulation faible d'un point de vue théorique.
Contrairement à certaines initiatives qui visent à dupliquer un modèle propriétaire dans une version libre, Org-Mode est une reconfiguration des modes d'édition.
Certes Org-Mode nécessite un apprentissage, un temps de maîtrise d'Emacs aussi, mais ensuite des actions complexes sont possibles très facilement.

La puissance de Org-Mode est inversement proportionnelle à son interopérabilité.
Ce qui se passe dans Org-Mode reste dans Org-Mode, ou plutôt dans Emacs.
Il n'est pas possible d'interagir facilement avec un document `.org` en dehors de cet environnement.
Je peux lire et éditer le format, mais il me sera presque impossible de produire ou d'exporter vers d'autres formats sauf à disposer d'une compatibilité dans d'autres logiciels.
Par exemple le générateur de site statique Hugo permet de gérer le format `.org`, avec l'aide d'un programme intermédiaire – voir [la documentation de Hugo](https://gohugo.io/content-management/formats/).
Certain·e·s pourraient m'objecter que nous nous retrouvons dans la même situation qu'avec Jekyll – pour prendre un exemple de fabrique déjà abordée ici.
Non car Jekyll repose sur plusieurs formats utilisés ailleurs : Markdown, YAML, Liquid, ainsi qu'une logique assez similaire que pour d'autres générateurs de site statique.
Passer de Jekyll à Hugo ou 11ty est relativement simple, ou en tout cas faisable.
Pour donner un autre exemple d'interopérabilité AsciiDoc peut être utilisé dans le générateur de site statique Hugo.
Cette limite est compréhensible et se justifie dans le fonctionnement même d'Org-Mode, modulaire à l'intérieur d'Emacs à défaut d'être interopérable avec d'autres logiciels ou fabriques.

Enfin les pratiques acquises avec Org-Mode, et Emacs, ne peuvent pas être réutilisées pour d'autres outils – je pense à certains balisages spécifiques, à l'usage intensif de raccourcis, et à la navigation dans un document.
Aujourd'hui nous pouvons constater que des environnements d'écriture basés sur des éditeurs de texte ou des IDE sont assez similaires : passer par exemple de Sublime Text à Atom ou d'Atom à VS Code n'implique pas de profonds changements de pratiques, les configuration peuvent être portées de l'un à l'autre de ces outils.
Il s'agit d'un point important : réduire la dépendance à un outil pour s'assurer de pouvoir changer facilement d'instrument sans perdre une _fonctionnalité_.

## Vers d'autres fabriques
Je n'ai pas d'autres fabriques à indiquer ici, tant Org-Mode est spécifique et, d'une certaine façon, refermé sur lui-même – attention il ne s'agit pas d'un jugement de valeur mais un constat sur son fonctionnement et sa dépendance à Emacs.

## Références
Je souhaiterais simplement souligner que si mon intérêt s'est à un moment donné porté sur Org-Mode, c'est grâce à sa formidable communauté (merci [Obrow](https://framapiaf.org/@obrow)).

{{< bibliography >}}
