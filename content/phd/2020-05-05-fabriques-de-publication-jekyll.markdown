---
layout: post
title: "Fabriques de publication : Jekyll"
date: "2020-05-05T22:00:00"
comments: true
published: true
description: "Quatrième analyse d'une série sur les fabriques de publication : Jekyll"
categories:
- carnet
- phd
classement:
- phd
status: brouillon
bibfile: data/analyses-jekyll.json
---
Quatrième analyse d'une série sur les fabriques de publication&nbsp;: Jekyll.

<!-- more -->

[Introduction et liste des fabriques](/fabriques)

Jekyll est un générateur de site statique, emblématique d'une façon de concevoir et de fabriquer des sites web.

## Description
Mais qu'est-ce donc qu'un générateur de site statique {{< cite camden_working_2017 >}} – ou _static site generator_ en anglais&nbsp;?
Sous ce terme quelque peu trompeur se cache une profonde reconfiguration des modes de conception et de fabrication des objets numériques que sont les sites web {{< cite taillandier_mouvance_2016 >}}.
[Jekyll](https://jekyllrb.com/) est un générateur de site statique parmi tant d'autres, mais emblématique d'une (nouvelle&nbsp;?) façon d'envisager la production de publications comme les sites web.

Jekyll est un programme, écrit en Ruby, qui génère un site web, donc des fichiers HTML et CSS, à partir de différents fichiers&nbsp;: un fichier de configuration général qui indique les instructions pour le site dans son ensemble&nbsp;; des fichiers de contenu dans l'un des formats de balisage que Jekyll est capable de comprendre, Markdown dans la majorité des usages, comprenant des informations d'entête comme le titre ou l'auteur&nbsp;; des gabarits aux formats HTML avec la syntaxe [Liquid](https://shopify.github.io/liquid/) pour définir les modèles des pages web.
En soit rien de bien exotique, cela correspond à des frameworks utilisés depuis longtemps dans le domaine du développement web, ou comme nous l'avons déjà vu à Pandoc qui fait à peu de choses près le même type de transformation.
La différence essentielle ici étant que Jekyll gère une publication complexe&nbsp;: un ensemble de documents organisés avec des éléments récurrents, des spécificités en fonction des types de pages, la possibilité de varier les gabarits, etc.

Jekyll est relativement simple à comprendre {{< cite phelan_comment_2017 >}} et à utiliser.
Deux éléments facilitent son usage, y compris pour des personnes éloignées du développement informatique&nbsp;: le fait que Markdown soit le langage de balisage léger utilisé par défaut, et le fait que le choix du langage de _templating_ soit Liquid.
Jekyll dispose [d'une documentation très complète](https://jekyllrb.com/docs/), et sa popularité a généré de nombreux forums dédiés et d’innombrables billets de blog sur le sujet.
Malgré tout, Jekyll est un programme qui s'utilise avec un terminal pour lancer des commandes – générer le site ou le prévisualiser, avec ou sans options.
Aujourd'hui des interfaces se branchent via des systèmes de gestion de version, permettant d'écrire, d'enregistrer et de publier des pages et des sites web sans ligne de commande.

## Histoire
Jekyll est créé en 2008 par Tom Preston-Werner {{< cite preston-werner_blogging_2008 >}}, qui est entre autre l'un des fondateurs de GitHub, la plateforme d'hébergement de code – inutile de préciser que Tom Preston-Werner est riche.
Jekyll est né d'un désir de simplicité à une époque où des systèmes de gestion de contenu (ou CMS pour _Content Management System_) comme WordPress dominaient largement les usages – et le marché.
Pour comprendre comment et pourquoi Jekyll a vu le jour, il faut comprendre quelle communauté a permis son arrivée&nbsp;: des développeurs.
Ce générateur de site statique gère les contenus comme du code, d'une certaine façon il simplifie la gestion d'un site web puisque tout est accessible à partir d'un éditeur de code/texte.
Il aplatît ce que les CMS classiques gèrent avec des langages dynamiques et des bases de données.
Parker Moore sera le développeur principal de Jekyll pendant cinq années {{< cite autrand_interview_2016 >}}, assurant l'évolution et la maintenance de ce programme, mais aussi et surtout réussissant à fédérer une communauté autour de ce générateur de site statique.

Jekyll a été rapidement intégré à GitHub comme l'outil de création de sites web, le fameux service [GitHub Pages](https://pages.github.com/) – nous pourrions d'ailleurs nous demander si Jekyll n'a pas été créé pour disposer d'un CMS versionnable.
Ce point est important dans l'histoire de Jekyll et des générateurs de site statique, puisqu'en proposant cette solution par défaut pour créer des pages web, avec un hébergement gratuit associé, GitHub a permis une adoption très large de Jekyll.
Au début des années 2010, la mode, chez les développeurs, était d'avoir son site web sur GitHub et généré et hébergé avec GitHub Pages – le service a récemment été [revu en terme de communication](https://github.dev/).
Et sans vouloir m'avancer cela a également permis l'émergence du développement continu {{< cite shahin_continuous_2017 >}} avec des plateformes comme GitHub ou GitLab&nbsp;: ce processus qui consiste à déployer automatiquement du code depuis un dépôt Git – je reviendrai sur cette pratique dans d'autres fabriques.

En terme de popularité il faut noter qu'en France, par exemple, la communauté [Jamstatic](https://jamstatic.fr/) s'est d'abord appelée Jekyll-fr.
Si le nom a été rapidement modifié pour englober le mouvement [JAMstack](https://jamstack.wtf/) dans son ensemble, ce premier essai est symptomatique de l'engouement qu'a suscité Jekyll.

Depuis quelques années et après un succès mérité, Jekyll est désormais détrôné par d'autres générateurs de site statique pour différentes raisons&nbsp;: performance (le fameux temps de _build_ dont il est question plus bas), langages de programmation plébiscités, mode.
Aujourd'hui [Hugo](https://gohugo.io/), [Eleventy (de son petit nom 11ty)](https://www.11ty.dev/) ou [Gatsby](https://www.gatsbyjs.org/) sont probablement plus représentatifs du paysage des générateurs de site statique ou de la JAMstack, paysage qui évolue vite.

## Specimen
Le nombre de sites produits avec Jekyll doit être tout simplement gigantesque.
Plutôt que de prendre des exemples parmi des sites web relativement classiques (comme [quaternum.net](https://www.quaternum.net), généré avec Jekyll), le plus simple est probablement de consulter la page [showcase](https://jekyllrb.com/showcase/) de Jekyll, ou de découvrir les près de deux cent thèmes Jekyll sur [JAMstack Themes](https://jamstackthemes.dev/#ssg=jekyll).

## Critique
Jekyll n'est pas le _premier_ générateur de site statique, ni celui qui est le plus utilisé.
Mais il est représentatif d'un mouvement, que certain·e·s pourraient qualifier de retour, et c'est vrai que les scripts maison pour générer quelques pages HTML existent depuis les débuts du web.
Ce qui m'intéresse c'est de comprendre quel changement de paradigme impose Jekyll, et notamment parce qu'il est simple à utiliser avec beaucoup de fonctionnalités existantes par défaut.

L'expression – ou concept – _générateur de site statique_ est trompeuse car des outils comme Jekyll ne font que _générer_ des fichiers HTML, ce qui n'induit pas qu'un site ainsi produit serait moins dynamique qu'un autre produit avec WordPress.
La différence réside dans la fabrication qui se fait en deux temps, et donc les nouvelles contraintes qu'impose ce fonctionnement.
Pour gérer les interactions il faut soit utiliser des services extérieurs, soit ajouter d'autres briques logicielles.
Il s'agit du A de JAMstack, pour API – je ne m'étends pas sur cette question mais interrogeons-nous sur l'ordre des couches de cette _stack_ {{< cite hoizey_jamstack_2020 >}}.

Jekyll introduit une dimension asynchrone que les CMS classiques comme WordPress ou Drupal (ou même Spip ou Dotclear) tendaient à faire disparaître, ou plutôt à invisibiliser.
En rendant la publication asynchrone, les étapes d'édition d'un site web réapparaissent.
Envisager à nouveau un découpage des phases de fabrication d'un site web est crucial, puisqu'il (re)devient possible de séparer des temps qui correspondent à la création et à la gestion d'une publication.
Jekyll permet par exemple de travailler sur sa machine, sans être connecté sur un serveur, et de disposer d'un environnement d'écriture et d'édition distinct d'un environnement de publication – beaucoup plus simple à installer et paramétrer qu'avec des CMS dynamiques comme WordPress.
Par ailleurs, Jekyll ne traite que des fichiers en plein texte – ou texte brut –, ce qui signifie que les sources du site peuvent être déplacées facilement, sans les contraintes d'une base de données.
D'ailleurs nous pourrions émettre l'hypothèse que Jekyll ou d'autres générateurs de site statique ont influencé des _flat CMS_ comme [Kirby](https://getkirby.com/) ou [Grav](https://getgrav.org/).
Et puisqu'il s'agit de fichiers en plein texte, ils peuvent être versionnés {{< cite fauchie_version_2020 >}}.

Le versionnement n'est pas un détail.
La vague des générateurs de site statique, puis celle de la JAMstack, a pour origine des pratiques de développement.
La gestion de versions, et Git en particulier, est de plus en plus utilisée dans les processus de conception, de production et de maintenance de la programmation.
Proposer des systèmes de gestion de contenu qui prennent en compte Git est quelque chose qui vient de la communauté des développeurs, mais qui a aussi du sens pour des personnes amenées à écrire, quelque soit le type de contenu {{< cite github_empowering_2018 >}}.

Jekyll peut être augmenté avec des extensions, écrites en Ruby.
J'utilise deux extensions très utiles qui illustrent ce fonctionnement&nbsp;: [jekyll-microtypo](https://github.com/borisschapira/jekyll-microtypo/), créée par [Boris Schapira](https://github.com/borisschapira/jekyll-microtypo/), permet d'affiner la typographie principalement pour les contraintes francophones&nbsp;; [jekyll-scholar](https://github.com/inukshuk/jekyll-scholar), créée par Sylvester Keil, permet de créer des citations bibliographiques et des bibliographies à partir de fichiers BibTeX.
Le fait d'ajouter des éléments à Jekyll peut devenir un problème, comme la question de la compatibilité entre Jekyll et l'extension, ou le ralentissement provoqué par l'extension.
Comparons rapidement cette méthode avec deux autres générateurs de site statique qui ont fait des choix très différents&nbsp;: 11ty est basé sur le langage de programmation JavaScript et l'environnement Node.js, le royaume des extensions et des dépendances (pour le meilleur et pour le pire)&nbsp;; Hugo ne comporte pas d'extension, si une fonctionnalité est jugée pertinente elle sera ajoutée dans le programme ou gérée autrement – les [shortcodes](https://gohugo.io/content-management/shortcodes/) sont par exemple un moyen d'augmenter les possibilités du générateur de site statique en accord avec le programme.

Avec ces éléments nous voyons poindre une autre dimension très intéressante&nbsp;: la modularité.
Si des CMS comme WordPress intègrent déjà le versionnement, il n'est pas possible de _sortir_ cette fonctionnalité du système.
Markdown pour la structuration du texte, YAML pour les métadonnées, Jekyll pour la génération des fichiers HTML/CSS, Git pour versionner le texte, il s'agit là d'un fonctionnement modulaire.

D'un point de vue technique Jekyll peut être jugé comme lent, ou plutôt comme étant moins rapide que d'autres.
C'est ce qui est appelé le temps de _build_ – pour temps de génération des fichiers HTML et CSS le cas échéant.
Certains générateurs de site statique ont été conçus en réaction à la relative lenteur de Jekyll, et c'est ainsi que Hugo a fait son apparition, permettant de générer des sites complexes et de taille importante en moins d'une seconde là où Jekyll met plusieurs secondes ou dizaines de secondes.
Cela ne me semble pas être l'enjeu déterminant mais il faut bien reconnaître que ce temps de _build_ a des conséquences importantes, comme le temps de calcul d'une machine ou la possibilité de pouvoir prévisualiser une page sans avoir à attendre.

D'autres langages de balisage pouvaient être utilisés dans Jekyll, comme Textile, désormais seul Markdown est _supporté_.
D'ailleurs c'est [Kramdown](https://kramdown.gettalong.org/), également écrit en Ruby, qui est le convertisseur utilisé.
J'ai découvert récemment qu'il était possible d'utiliser directement Pandoc comme convertisseur.
Markdown est un format de balisage léger bien pratique, compréhensible par les humains et les programmes, il reste un peu trop léger.
Un peu comme Pandoc, Jekyll permet d'étendre Markdown, notamment via des fonctions comme `include` ou via celles des extensions – par exemple pour citer avec jekyll-scholar il faut utiliser `{% cite diaz_using_2018 %}`.
Ce qui enrichit contraint également, cette syntaxe dans la syntaxe peut ajouter en complexité pour une maintenance sur le long terme.

Pour terminer sur cette critique trop longue, notons que Jekyll a été et est encore un pont entre des personnes habituées à manipuler du code et des personnes habituées à manipuler des textes.
Jekyll n'est pas une fin en soit, mais un moyen de faire converger des pratiques très différentes, notamment par le biais d'interfaces intermédiaires – j'en reparlerai.

## Vers d'autres fabriques
Certaines des prochaines fabriques utilisent des générateurs de site statique, j'y reviendrai donc plus longuement.
Avant cela citons tout de même [Ed](https://minicomp.github.io/ed/), une utilisation de Jekyll pour la réédition numérique de textes littéraires.
Même si Ed est présenté comme un _thème_, nous pouvons considérer que c'est un peu plus que cela&nbsp;: certaines extensions sont plébiscitées comme jekyll-scholar, et il y a une modélisation de l'information qui dépasse le simple thème.
Un exemple d'application de Ed. est [MARXdown](https://marxdown.github.io/), le nom est suffisamment explicite pour ne pas avoir besoin de préciser l'objet de ce site web – [Hypothesis](https://web.hypothes.is/) est également utilisé dans ce cas précis.
D'autres applications de Jekyll en milieu universitaire existent {{< cite diaz_using_2018 >}}.


## Références

{{< bibliography >}}
