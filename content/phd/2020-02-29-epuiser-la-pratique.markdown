---
layout: post
title: "Épuiser la pratique"
date: "2020-02-29T06:00:00"
published: true
description: "Un accident artistique."
categories:
- carnet
- phd
classement:
- phd
status:
image: "https://www.quaternum.net/images/2019-epuiser-01-mini.jpg"
---
Un accident artistique.

<!-- more -->

![](/images/2019-epuiser-01.jpg)

L'idée était venue presque par hasard.
Profiter de deux mois dans une autre ville pour produire quelque chose.
Avec une contrainte à s'imposer, et une certaine dimension de régularité.
Le carnet comme support.
Même si l'exploration de l'intime est souvent attirante l'écriture n'était pas une option viable à ce moment-là.
Apprendre à dessiner.
En deux mois
Chaque jour.
Projet saugrenu dont il était facile de se détacher, au besoin.

L'expérience passe vite.
Dans la douleur et dans la satisfaction.
Les progrès sont rapides, le style s'affirme vite, le résultat est palpable.
Montrable aussi.
Pourquoi ne pas continuer, comme une opportunité de créer, avec de maigres moyens mais un horizon flamboyant.
Transcrire un bâtiment, proposer un cadrage, rendre compte de la ville, capter les détails, garder une trace.
Dessiner dehors, s'appuyer sur le regard.
L'atelier est le carnet.
Dessiner sur une table est exclu.

Dessiner au moins une fois par jour, remplir le carnet, chercher le trait juste.
La répétition comme moyen d'aboutir à un moment de félicité : un geste graphique et artistique remarquable, sinon parfait.
Parfois quelques détours, comme ajouter de la couleur, modifier le rythme du trait, changer de format, trouver une autre technique, mais toujours créer dans un geste continu, sans casser l'intention.
Pas de crayonné, mais un trait noir indélébile.
Pas de feuille volante, mais un carnet dont les pages sont reliées.
Garder la rature si elle advient, conserver les ébauches, archiver les échecs.
Tout ça, mélangé.

Forcément ça implique d'investir sa personne.
Marcher longtemps avant de trouver le détail qui attire l'œil, ou le modèle qui sera reproductible.
Et le corps dans tout ça : rester assis presque une heure dans une position inconfortable, négocier avec le froid l'hiver, et parfois entrer en conflit.
Accroupi près d'une usine, ça surprend.
Pas d'intérêt pour le tracteur autre que sa représentation.
Quelques points de suture après avoir dessiné une carcasse de voiture derrière un camp de fortune.
Non il n'était pas question de voler des enfants, juste dessiner.
Et les nombreuses sollicitations qui se veulent bienveillantes.
C'est joli, c'est beau.
Pourtant le dessin pour le dessin, être son propre lecteur.
Le reste est autre chose, possible, mais autre chose.

Se satisfaire de feuilleter les pages noircies.
Grimacer souvent, prendre du plaisir à voir les dessins se suivre.
Les bons comme les moins bons.
Être le premier à constater que quelque chose se passe.
Être le créateur, le spectateur, le critique.
Fabriquer un livre à édition limitée, le seul exemplaire disponible.
Maîtriser cette chaîne de la création.
Et pourtant.
Parfois.
Dévoiler aux proches l'achèvement, quand tout est figé et seulement bon à montrer.
Alors qu'au creux de soi le geste n'en finit plus.
Il y a toujours un dessin à inscrire dans le carnet.
Toujours un immeuble à dessiner, une usine à représenter, un panneau tordu à conserver.

Les carnets se suivent, se multiplient.
Dessiner n'est pas vital, dessiner était une contrainte et devient une obligation.
Peut-être par nécessité de produire, quel que soit le résultat et sa qualité.
Un dessin raté c'est mieux que pas de dessin.
Avec tous ces carnets il est possible de constater l'ampleur de la tâche.
De mesurer l'effort, le temps passé, la réussite, l'évolution, la chute.
Le dessin est-il là pour autre chose ?
Passer des heures à traverser une ville, à se perdre dans ses recoins ?
Être le touriste de son propre quotidien atteint vite une limite.
La discipline devient pesante.

La création en voyage est un moyen, pas une fin.
Ne pas tomber dans les clichés des vues attendues.
Ça aurait pu être au coin de la rue, mais ce fut dans cette capitale européenne facile à dessiner.
Ne pas rendre reconnaissable ce bâtiment.
Proposer une autre interprétation, jouer avec les préjugés.
Les carnets deviennent des boussoles.
Là, à ce moment, il s'est passé autre chose que le dessin.
Les amis, les amours, les sentiments, les émotions.
À côté, autour, dedans.
Plus dedans que ce qui était prévu.

![](/images/2019-epuiser-03.jpg)

C'est un univers qui prend forme.
Plus seulement les pages remplies ou la tentation de la perfection.
Noter les signaux faibles.
De ceux qui sont d'habitude écartés.
Plutôt que de regarder les photos pour se souvenir, feuilleter les carnets.
La maîtrise est une supercherie.
C'est le stylo qui tient tout, qui dirige.
Chaque périple est anticipé dans l'objectif d'une nouvelle création.
À l'occasion d'un déplacement trouver quelques heures pour s'échapper.
Se plaindre des jours de pluie.
Attendre le printemps.
Guetter les prochains voyages.

L'anticipation de la création devient pollution, ça prend toute la place.
Partir à la chasse au dessin.
Le pas lourd, la main hésitante.
Les dessins se font plus rares.
C'était pourtant un moyen de se retrouver.
Une ascèse vers l'extérieur, une retraite à l'intérieur du monde, un _break_.
D'autres pratiques pointent.
Tester l'écriture, le journal.
La pire contrainte, abandonnée plus tard.
Le texte fait son chemin.
Aussi froid que le dessin d'observation.
Rien dire de soi, en apparence.

Le dessin brûlait les doigts d'excitation.
Il est devenu un poids, une charge mentale.
Quelque chose s'est usé en chemin.
La recette devenait trop prévisible, le résultat moins aléatoire.
Les ratures rares.
Celles qui subsistent ont comme origine l'hésitation, pas la tentative.
Est-ce que c'est triste ?
Regrettable ?
Dommage ?

Ça se passerait ailleurs.
Si tant est que la création soit une quantité d'énergie disponible.
Alors allons voir ailleurs plutôt que rien du tout.
Hésiter, c'est vraiment rater.
L'échec est un bel essai.
Il faut négocier avec l'univers créé.
Celui-ci n'est pas détaché du reste.
S'il y a un reste.
Tout se réagençait déjà.
Les carnets cachaient les métamorphoses à l'œuvre.
L'urgence de s'écouter.

Le dessin n'est pas un vieux copain.
Il n'y a pas eu d'amitié.
Une porte ouverte, d'autre couloirs empruntés.
Regretter c'est croire que le chemin est balisé.
Tout le monde est perdu mais personne ne le sait.
Les carnets étaient une boussole.
Il y a en d'autres, des boussoles.
Aussi des phares, des lumières dans la nuit.
Tout se superpose, s'entrechoque.
C'est beau.

![](/images/2019-epuiser-02.jpg)

_Texte de création rédigé dans le cadre du séminaire de Jean-Simon Desrochers, Département des littératures de langues française, Université de Montréal, novembre 2019._
