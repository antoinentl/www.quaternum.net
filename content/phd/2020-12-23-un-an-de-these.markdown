---
layout: post
title: "Un an de thèse"
date: "2020-12-23T08:00:00"
comments: true
published: true
description: "Après un an de thèse il est temps de faire un point sur ce parcours aussi passionnant que chargé."
categories:
- carnet
- phd
classement:
- phd
---
Après un an de thèse il est temps de faire un point sur ce parcours aussi passionnant que chargé.

<!-- more -->

J'aurais probablement dû intituler ce billet "Un an de doctorat", je triche un peu en centrant faussement ces quelques paragraphes sur la thèse.
Le texte qui suit est probablement rempli de banalités : ce carnet de thèse est surtout écrit _pour moi_, il s'agit donc d'une forme de point d'étape.

J'ai débuté un doctorant au Département des littératures de langue française de l'Université de Montréal en septembre 2019, ma thèse est dirigée par Marcello Vitali-Rosati, et elle porte sur la reconfiguration des processus de publication et sur les pratiques d'écriture dans le champ littéraire.
Je reviendrai en détail sur _le sujet_ dans quelques semaines.

![](/images/2020-un-an-01.jpg)

## Une thèse au Québec : quelques spécificités
Je ne fais pas de thèse en France, donc il m'est un peu difficile de comparer mon expérience québécoise avec le système français.
Le fait de connaître un certain nombre de doctorant·e·s en France me permet tout de même de relever quelques spécificités, désolé par avance si je dis des énormités.

La différence la plus importante est sans doute la "scolarité" : un doctorat comprend environ 5 séminaires obligatoires, composé chacun d'une quarantaine d'heures de cours, avec beaucoup de lectures et de rendus (certains billets de ce carnet sont des travaux de séminaires).
Pour donner un ordre d'idée, pendant la _session_ d'automne 2019 (donc de septembre à janvier), je suivais 2,5 séminaires, ce qui représente par semaine (en moyenne) : 7,5 heures de cours, 5 heures de préparation (essentiellement des lectures), et 4 heures pour les rendus, ce qui fait au total deux (grosses) journées de travail.
L'avantage c'est que j'ai toujours pu faire coïncider ces travaux avec mes sujets de recherche (ok c'était parfois tiré par les cheveux) : en plus d'apprendre plein de choses j'ai pu tout de même avancer en creux sur ma thèse.

L'autre différence consiste en des étapes définies et structurantes, notamment le dépôt du sujet qui se fait vers la fin de la première année, et un examen de synthèse dans le courant de la deuxième ou troisième année — cet examen de synthèse permet de valider des premiers éléments théoriques et de soumettre une petite partie rédigée pour passer à l'étape dite _de rédaction_.

![](/images/2020-un-an-02.jpg)

## Multi-tâches : savoir switcher
J'ai la chance de disposer de quelques financements pour ce doctorat, par financement je veux dire que je suis rémunéré pour travailler sur d'autres choses que sur ma thèse.
Cela signifie que la plupart de mon temps est — pour l'instant — occupé par autre chose que ma recherche _personnelle_ : gestion des projets du laboratoire (la [Chaire de recherche du Canada sur les écritures numériques](https://ecrituresnumeriques.ca)) et activités d'enseignement (en plus des cours que j'ai pu suivre, la fameuse scolarité).
Pour moi débuter un doctorat voulait surtout dire rejoindre [l'équipe](https://ecrituresnumeriques.ca/fr/Equipe) de la _Chaire_.
J'ai la chance de pouvoir travailler dans de très bonnes conditions, et sur des sujets qui sont directement ou indirectement liés à mes recherches.
Il y a beaucoup de travail, mais à un moment ou à un autre cela nourrit mes recherches, et surtout ça me plaît.

![](/images/2020-un-an-04.jpg)

## Publier vite, publier souvent
L'écriture est au centre de la thèse, et pas uniquement au moment de la rédaction.
En dehors des séminaires je n'ai pas écrit avant plusieurs mois, il m'a fallu un bon moment pour [mettre en place mon atelier de thèse](/2020/03/29/atelier-de-these/).
Si je me suis fixé un calendrier de lectures et de rédaction (concepts et analyses) que j'ai bien du mal à respecter, je parviens à prendre des notes plusieurs fois par semaine — j'ai en fait principalement déporté [ma veille](/flux).
Je publie donc très régulièrement, mais de façon cachée.
Déjà parce que la forme est mouvante, je bricole mon atelier et c'est parfois cassé.
Aussi certaines notes me sont uniquement destinées, et je ne crois pas que cela aurait un intérêt quelconque de rajouter au brouhaha du monde et des internets — en écrivant cela je ne me convaincs pas.
Depuis septembre 2019 j'ai réussi à tenir le rythme d'un billet minimum par mois (en moyenne, je triche), publié sur mon carnet de thèse (public) : [https://www.quaternum.net/phd/](https://www.quaternum.net/phd/).
Articuler atelier de thèse (privé) et carnet de recherche (public) est un chantier que je me réserve pour le printemps avec peut-être une refonte de quaternum.net et le lancement d'autres formes intermédiaires de publication pour la thèse.
Quoi qu'il en soit je prends conscience que pour moi écrire signifie bricoler continuellement l'outil d'écriture et de publication, les deux sont liés — et l'un motive l'autre, une forme de programmation éditoriale.

![](/images/2020-un-an-05.jpg)

## Entre solitude et collectif
L'avantage du système québécois est la proximité avec les autres doctorant·e·s, les séminaires sont autant de découvertes et d'occasions d'échanger — quand ils se déroulent en présentiel, autant dire que depuis mars 2020 les contacts sont plus rares et l'ambiance devient quelque peu morose.
Ces relations peuvent paraître anodines mais c'est précieux dans le cadre d'un travail long et besogneux comme la thèse.
Les activités d'un laboratoire sont aussi l'occasion d'échanger, surtout lorsqu'on est entouré de personnes brillantes (je suis entouré de personnes brillantes).
Dans les faits le temps manque pour parler de la recherche, il faut reconnaître que les activités et les cours prennent continuellement le dessus.
Il y a aussi les copains et les copines qui s'intéressent aux mêmes thématiques que moi, dans et hors du système académique, quel plaisir de pouvoir discuter, partager des références, tester des hypothèses, etc.
Sans ça tout ce bazar n'aurait plus vraiment de sens.

## Vie personnelle : jonglage
Je n'ai pas l'habitude de parler de ma vie personnelle, mais le fait d'avoir une fille de trois ans au moment de commencer un doctorat change pas mal de choses – en plus de mon âge avancé, 36 ans au moment de débuter.
Ce projet universitaire _et_ professionnel est aussi un projet personnel dans lequel j'ai embarqué des proches.
La gestion du temps n'est pas simple, mais je ne vois pas cette situation comme un frein, bien au contraire.
Si cela doit être précisé, je ne considère pas que c'est à l'autre de gérer le quotidien pendant que je fais ma thèse.
Le sacrifice au nom de la thèse je trouve ça déprimant, sans parler de la pression...
Et j'estime que la thèse est un travail salarié comme un autre, certes qui déborde malgré tout, mais j'essaye de m'imposer des horaires (fortement décalés).
La pandémie ajoute un niveau de complexité.
Au printemps j'ai pu profiter de plusieurs semaines de solitude pour [écrire](/fabriques), oscillant entre excitation (du temps disponible et des sujets de recherche exaltants) et angoisse (je ne détaille pas ici).

## Un marathon
Depuis que j'ai débuté ce doctorat je combats l'idée selon laquelle une thèse c'est avoir du temps pour chercher, pour lire, pour écrire.
C'est une façon d'envisager la thèse assez répandue en dehors du milieu universitaire, j'ai depuis longtemps cette image d'Épinal en tête, complètement erronée.
Pourtant ce n'est pas faute de côtoyer beaucoup de doctorants et de doctorantes, y compris bien _avant_ de débuter...
Une thèse c'est un marathon, il faut savoir planifier et se tenir à ce plan.
Je ne parviens pas encore à respecter les échéances que je me fixe, mais je me rassure en me disant que mes recherches avancent et que j'écris constamment.
Les prochains mois seront pour moi déterminants en terme d'organisation.
Il faut aussi écouter son rythme, par exemple écrire le soir ne me convient pas (cela me demande trop de concentration après une journée plus que chargée), alors que me réveiller 30 minutes plus tôt (quand j'y arrive) me permets d'écrire vite et bien et me donne de la motivation pour la journée.
Aussi j'ai tendance à repousser certaines recherches, notamment parce qu'elles me semblent très intéressantes.
Erreur !
Il faut tout de suite noter les idées, même des bribes, et épuiser chaque piste afin de ne pas les transformer en fantasmes (se complaire dans la projection d'un travail sans jamais le réaliser vraiment).

![](/images/2020-un-an-03.jpg)

## Quelques chiffres
_Mesurer_ l'avancement d'une thèse au bout d'un an (et quelque) avec des chiffes n'est pas une bonne idée.
Si je pose ici quelques chiffres c'est surtout pour moi, et parce que cette thèse est la continuité d'un travail de recherche entrepris depuis environ 4 ans (ce qui n'est pas forcément le cas de tout·e jeune doctorant·e) :

- 1 communication acceptée (mais colloque reporté) ;
- 2 projets d'articles (en cours de rédaction) ;
- 5 interventions réalisées (hors spectre académique strict) ;
- 9 analyses rédigées et publiées ([les fabriques](/fabriques)) ;
- 19 billets [dans mon carnet de thèse](/phd) ;
- 120 notes dans mon atelier (allant de quelques lignes à plusieurs dizaines de milliers de signes, ce qui donne en tout 300 pages) ;
- 202 commits dans le dépôt de mon atelier, les 150 premiers sont lisibles [ici](/2020/10/27/apercu-01/).

Je ne sais pas si je referai un bilan au bout de 2 ans.
Cette première année a été décisive pour mettre en place un certain nombre d'outils et de méthodes (mon [atelier de thèse](/2020/03/29/atelier-de-these/) est central), peut-être que ces quelques lignes seront utiles à d'autres.
