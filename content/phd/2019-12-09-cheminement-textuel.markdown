---
layout: post
title: "Cheminement textuel"
date: "2019-12-09T12:00:00"
comments: true
published: true
description: "Exploiter les aspérités de la pratique d'écriture pour révéler ce qui se passe entre les lignes du texte."
categories:
- carnet
- phd
classement:
- phd
aliases:
  - /chemin
---
Exploiter les aspérités de la pratique d'écriture pour révéler ce qui se passe entre les lignes du texte.

<!-- more -->

<iframe src="https://presentations.quaternum.net/cheminement-textuel/"></iframe>

<a href="https://gitlab.com/antoinentl/cheminement-textuel/commit/4141395" class="commit">4141395</a> Mon projet ici est de dévoiler la création, montrer ce qui se passe en creux.
<a href="https://gitlab.com/antoinentl/cheminement-textuel/commit/45c0452" class="commit">45c0452</a> Donner à voir ce qui se cache dans les plis du textes, entre les différents CTRL+S successifs.
<a href="https://gitlab.com/antoinentl/cheminement-textuel/commit/7335454" class="commit">7335454</a> Au contraire du papier qui ne peut pas facilement dissimuler les ratures et les pages froissées, le numérique parvient à lisser les étapes successives de création.
<a href="https://gitlab.com/antoinentl/cheminement-textuel/commit/6f74af3" class="commit">6f74af3</a> Attention, je ne souhaite pas sous-entendre que tout brouillon doit nécessairement être révélé.
<a href="https://gitlab.com/antoinentl/cheminement-textuel/commit/38974c8" class="commit">38974c8</a> Ce serait dangereux de présupposer qu'un texte édité ou publié ne se suffit pas à lui-même.
<a href="https://gitlab.com/antoinentl/cheminement-textuel/commit/f0fbd97" class="commit">f0fbd97</a> Mais il me faut revenir en arrière sur l'origine de cette entreprise et de ce questionnement.
<a href="https://gitlab.com/antoinentl/cheminement-textuel/commit/cf53696" class="commit">cf53696</a> Il y a plusieurs pratiques d'écriture, on a tendance à rester dans un certain fantasme d'une seule, et littéraire par ailleurs.
<a href="https://gitlab.com/antoinentl/cheminement-textuel/commit/05e8d4c" class="commit">05e8d4c</a> À côté du texte il y a le code, qui prend de plus en plus de place dans notre environnement.
<a href="https://gitlab.com/antoinentl/cheminement-textuel/commit/ba76577" class="commit">ba76577</a> Le code n'étant pas nouveau en soit, sa forme et sa place sont par contre assez inédites.
<a href="https://gitlab.com/antoinentl/cheminement-textuel/commit/ddfa6d7" class="commit">ddfa6d7</a> Observons tout d'abord comment est géré le texte, sans que cette observation n'ait aucune base scientifique ou même méthodologique.
<a href="https://gitlab.com/antoinentl/cheminement-textuel/commit/3cf05b4" class="commit">3cf05b4</a> Le premier point est la superposition : les enregistrements se succèdent de façon linéaire, une version en suivant une autre.
<a href="https://gitlab.com/antoinentl/cheminement-textuel/commit/dd12da6" class="commit">dd12da6</a> Le second est la substitution : chaque nouvelle version écrase la précédente, sans intention ou même moyen de revenir en arrière.
<a href="https://gitlab.com/antoinentl/cheminement-textuel/commit/beacc4f" class="commit">beacc4f</a> À moins qu'une duplication soit effectuée et c'est le troisième point : les copies créées seront progressivement abandonnées ou oubliées.
<a href="https://gitlab.com/antoinentl/cheminement-textuel/commit/47e4e5d" class="commit">47e4e5d</a> Le code bénéficie de processus de maniement plus élaborés.
<a href="https://gitlab.com/antoinentl/cheminement-textuel/commit/cc3882a" class="commit">cc3882a</a> Chaque intervention, ou plutôt chaque session de travail, dispose d'une déclaration : un message qui explique ce qui a été fait.
<a href="https://gitlab.com/antoinentl/cheminement-textuel/commit/a58dec4" class="commit">a58dec4</a> Il est possible de naviguer dans les versions successives, de voyager dans le temps et de revenir à un point précis, initialement déclaré.
<a href="https://gitlab.com/antoinentl/cheminement-textuel/commit/31dbbba" class="commit">31dbbba</a> Enfin, les branches sont le moyen de conserver des copies synchronisables.
<a href="https://gitlab.com/antoinentl/cheminement-textuel/commit/f0be158" class="commit">f0be158</a> Il s'agit de flux, et d'inscrire le flux autant que d'inscrire ce que produit le flux.
<a href="https://gitlab.com/antoinentl/cheminement-textuel/commit/b6e8f46" class="commit">b6e8f46</a> Découvrir quel est le cheminement textuel, par quels détours les mots, les phrases, les paragraphes et les chapitres se sont formés.
<a href="https://gitlab.com/antoinentl/cheminement-textuel/commit/c63318c" class="commit">c63318c</a> Ce dévoilement ne doit pas être celui de chaque mouvement d'écriture, de chaque touche appuyée, mais celui de chaque déclaration.
<a href="https://gitlab.com/antoinentl/cheminement-textuel/commit/3d3739b" class="commit">3d3739b</a> Espérons que ce soit le seul instant technique : Git est un système de gestion de versions décentralisé, utilisé principalement dans le domaine de l'informatique.
<a href="https://gitlab.com/antoinentl/cheminement-textuel/commit/3f27f6f" class="commit">3f27f6f</a> Pour faire court, Git est un outil de versionnement du code, d'abord destiné aux développeurs.
<a href="https://gitlab.com/antoinentl/cheminement-textuel/commit/b42e94d" class="commit">b42e94d</a> Mais le code c'est du texte, donc Git permet de gérer du texte.
<a href="https://gitlab.com/antoinentl/cheminement-textuel/commit/a2b6296" class="commit">a2b6296</a> Il faut envisager Git comme un logiciel qui n'est fait que pour manier les enregistrements.
<a href="https://gitlab.com/antoinentl/cheminement-textuel/commit/0aeb44a" class="commit">0aeb44a</a> Git est comme un journal, l'espace de description de l'expérience d'écriture.
<a href="https://gitlab.com/antoinentl/cheminement-textuel/commit/c513d3d" class="commit">c513d3d</a> Grâce à Git on peut inscrire nos doutes, pour apprendre à les aimer, pour les conserver, et peut-être pour les révéler.
<a href="https://gitlab.com/antoinentl/cheminement-textuel/commit/7e57cc0" class="commit">7e57cc0</a> Oublions le dévoilement de la création pour préférer le cheminement textuel.
<a href="https://gitlab.com/antoinentl/cheminement-textuel/commit/be10837" class="commit">be10837</a> Une nouvelle dimension apparaît : des micro-textes en creux de la création en cours, c'est l'écriture dans l'écriture.
<a href="https://gitlab.com/antoinentl/cheminement-textuel/commit/a55d8fd" class="commit">a55d8fd</a> Des commentaires jalonnent la pratique, accompagnent le processus, de façon éclatée mais documentée.
<a href="https://gitlab.com/antoinentl/cheminement-textuel/commit/fa9db20" class="commit">fa9db20</a> Il s'agit de décrire le geste, d'inscrire l'intention.
<a href="https://gitlab.com/antoinentl/cheminement-textuel/commit/bb90940" class="commit">bb90940</a> La façon de faire importe autant que le résultat.
<a href="https://gitlab.com/antoinentl/cheminement-textuel/commit/c736683" class="commit">c736683</a> Un même texte peut avoir plusieurs cheminements, un même cheminement peut amener à différents textes.
<a href="https://gitlab.com/antoinentl/cheminement-textuel/commit/f1c7828" class="commit">f1c7828</a> Encore une fois il ne faut pas tomber dans une vision idéalisée de l'écriture, dans un fantasme du brouillon.
<a href="https://gitlab.com/antoinentl/cheminement-textuel/commit/fb33161" class="commit">fb33161</a> Le méta-texte n'est pas forcément utile, et il est peut-être vain de le détacher du premier texte.
<a href="https://gitlab.com/antoinentl/cheminement-textuel/commit/fd45dd7" class="commit">fd45dd7</a> Pour comprendre un peu mieux ce qui est exposé ici, prenons l'exemple de cette présentation.
<a href="https://gitlab.com/antoinentl/cheminement-textuel/commit/cde0978" class="commit">cde0978</a> Le présent texte est l'ensemble des déclarations des modifications (appelés _commits_ dans Git) de la présentation ci-dessus.
<a href="https://gitlab.com/antoinentl/cheminement-textuel/commit/e8a240c" class="commit">e8a240c</a> Ainsi la situation est renversée : ce texte, qui est un méta-texte, se place au même niveau que le texte premier, la présentation.
<a href="https://gitlab.com/antoinentl/cheminement-textuel/commit/15a47f7" class="commit">15a47f7</a> La pratique de création est elle-même renversée, le méta-texte qui était en creux devient l'aspérité, ce sur quoi l'expérience prend prise.
<a href="https://gitlab.com/antoinentl/cheminement-textuel/commit/8b8371e" class="commit">8b8371e</a> Fin.
<a href="https://gitlab.com/antoinentl/cheminement-textuel/commit/db5c668" class="commit">db5c668</a> Ce projet a été inspiré par Brendan Dawes à la suite de son article Using a Git Repo to create a physical document of the work.

_Cette expérimentation a été réalisée dans le cadre du cours de Jean-Simon Desrochers, La pratique d'écriture, à l'Université de Montréal. Comme indiqué le texte est l'ensemble des messages des commits de la présentation, les sources sont disponibles sur le dépôt [https://gitlab.com/antoinentl/cheminement-textuel/](https://gitlab.com/antoinentl/cheminement-textuel/)._
