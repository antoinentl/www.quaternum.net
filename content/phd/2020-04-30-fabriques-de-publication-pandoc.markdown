---
layout: post
title: "Fabriques de publication : Pandoc"
date: "2020-04-30T22:00:00"
comments: true
published: true
description: "Deuxième analyse d'une série sur les fabriques de publication : LaTeX."
categories:
- carnet
- phd
classement:
- phd
status: brouillon
bibfile: data/analyses-pandoc.json
---
Deuxième analyse d'une série sur les fabriques de publication&nbsp;: Pandoc.

<!-- more -->

[Introduction et liste des fabriques](/fabriques)

Pandoc est un convertisseur de documents balisés.
Pandoc permet de convertir un fichier depuis un format de balisage vers un autre format de balisage, typiquement depuis le format de balisage léger Markdown vers le format de balisage moins léger HTML.
Créé en 2006 par John MacFarlane, professeur de philosophie, Pandoc est souvent qualifié de "couteau suisse de l'édition".

## Description
Pandoc est un logiciel libre de conversion, disponible sur les trois systèmes d'exploitation Linux, Microsoft Windows et macOS.
Écrit en Haskell, il gère une trentaine de formats différents, comme formats d'entrée, comme formats de sortie, ou les deux – certains formats ne peuvent être convertis qu'en entrée ou en sortie.
Voici quelques exemples de conversion&nbsp;:

- Markdown → HTML
- Markdown → AsciiDoc
- Microsoft Word docx → Markdown
- HTML → LaTeX

Pour utiliser Pandoc il faut passer par un terminal, et donc écrire des commandes plutôt que d'appuyer sur des boutons, ce qui peut sembler complexe pour des personnes habituées aux interfaces graphiques.
Son utilisation est malgré tout relativement simple, les commandes de conversion sont basées sur la recette type suivante&nbsp;:

`pandoc [options] [fichier-source]`

Cette commande va transformer le fichier source au format HTML – c'est le format d'export par défaut – et l'afficher dans le terminal.
Pour spécifier le format et le fichier de sortie, il suffit d'ajouter un élément à la commande, voici un exemple de conversion d'un fichier au format texte brut (.txt) vers le format HTML&nbsp;:

`pandoc -o export.html source.txt`

Pandoc a également son propre balisage, étendant les possibilités de Markdown.
Notons que, toujours dans le cas de Markdown, Pandoc offre des options pour utiliser une version plus ou moins enrichie du langage de balisage léger.

Pandoc est parfois méconnu alors qu'il est utilisé dans de nombreux logiciels d'édition.
Il est également un prérequis – ou une dépendance – à certains programmes.

## Histoire
[John MacFarlane](https://johnmacfarlane.net/) semble avoir créé ce programme d'abord pour ses besoins personnels, avant qu'il ne devienne le compagnon incontournable des curieux et curieuses de la publication numérique.
Il faut noter que John MacFairlane continue de maintenir ce programme, dont le code source est disponible et modifiable [sur GitHub](https://github.com/jgm/pandoc/).

## Spécimens
Difficile de prendre un exemple de publication produite avec ce logiciel, Pandoc pouvant être considéré comme un rouage d'une fabrique plutôt que comme une fabrique elle-même.
Les liens avec d'autres fabriques ci-dessous est plus éclairant.

## Critique
Pandoc est peu connu en comparaison de ses nombreuses applications, c'est en quelque sorte un outil de l'ombre – comme il en existe beaucoup.
Pour réaliser une tâche il est rare de disposer d'un seul programme en informatique, c'est pourtant le cas avec Pandoc, et probablement pour trois raisons.
La première est que malgré la relative simplicité des langages de balisage, convertir certains formats est parfois un défi de complexité.
Qui plus est Pandoc dispose de nombreuses options très puissantes, par exemple pour la gestion des notes ou des citations – avec l'aide incontournable du programme [pandoc-citeproc](https://github.com/jgm/pandoc-citeproc), également développé par John MacFarlane.
La seconde est que Pandoc fait bien ce pour quoi il est conçu, c'est un exemple de logiciel efficace qu'il semble difficile de détrôner.
Pour des conversions spécifiques – notamment Markdown vers HTML – les concurrents sont nombreux, conçus pour s'adapter à des langages de programmation particuliers (Python, Ruby, Go).
La troisième raison est que les langages de balisage, il faut bien l'admettre, n'attirent pas non plus les foules.
Et dans des cas comme XML, Pandoc devient dérisoire en comparaison de XSLT.

Pandoc est un convertisseur agnostique, il n'impose pas une pratique par rapport à un balisage.
Certaines conversions sont toutefois tout simplement impossibles&nbsp;: un format complexe est trop difficile à réduire au risque de perdre la majorité des informations, ou de ne pas savoir quoi en faire.

Pandoc est un révélateur.
Tout d'abord parce que son utilisation nous fait comprendre avec beaucoup d'acuité qu'un texte est forcément structuré.
Aussi son usage nous fait mieux comprendre les enjeux d'ouverture et d'interopérabilité liés aux formats.

En théorie Pandoc pourrait être utilisé pour produire facilement des pages web, disons qu'il est plutôt conçu pour fabriquer des fichiers HTML.
Une organisation spécifique ou un programme peuvent ensuite permettre d'envisager l'utilisation de Pandoc pour concevoir un site web, mais il serait faux de penser que Pandoc peut le faire seul {{< cite "bortzmeyer_les_2018" >}} – ce détail est important malgré mon apparente volonté de couper les cheveux en quatre.

Si les utilisateurs et les utilisatrices qui découvrent Pandoc regrettent dans un premier temps la nécessité de devoir recourir au terminal, sa puissance fait oublier la contrainte de la ligne de commande.

## Vers d'autres fabriques
Comme dit précédemment Pandoc est une brique constitutive de nombreux systèmes de publication.
Voici quelques exemples choisis totalement arbitrairement (et non représentatifs d'une certaine diversité), et dans lesquels la contrainte de la commande (et de l'usage du terminal) est sous-traitée à un logiciel&nbsp;:

- [Zettlr](https://zettlr.com/)&nbsp;: un logiciel d'écriture sémantique basé notamment sur le langage de balisage léger Markdown. Pandoc permet d'exporter depuis le format Markdown vers HTML ou PDF (via xelatex)&nbsp;;
- [Stylo](stylo.ecrituresnumeriques.ca/)&nbsp;: l'éditeur de texte sémantique basé lui aussi sur Markdown. Pandoc est l'élément qui permet d'exporter un document dans différents formats (HTML, PDF, docx, etc.).

Pandoc sera donc une des _fabriques de publication_ dont nous parlerons régulièrement [dans cette série](/fabriques).

## Références
La documentation de Pandoc est très riche ([https://pandoc.org](https://pandoc.org)), la version PDF fait près de 150 pages.
Le Web regorge de méthodes et de questions pratiques sur Pandoc, mais il y a encore trop peu d'articles théoriques.
[Arthur Perret](https://arthurperret.fr), doctorant français, a contribué ces dernières années à une réflexion théorique (et sémiologique) autour de Pandoc.

{{< bibliography >}}
