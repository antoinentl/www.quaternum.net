---
layout: post
title: "Publier les carnets, éloge du brouillon"
date: "2019-12-05T06:00:00"
comments: true
published: true
description: "Quel espace d'écriture et de publication pour un carnet de thèse&nbsp;? Ou comment trouver les outils, les moyens de diffusion et la légitimité adéquats."
categories:
- carnet
- phd
classement:
- phd
bibfile: data/publier-les-carnets.json
---
Quel espace d'écriture et de publication pour un carnet de thèse&nbsp;?
Ou comment trouver les outils, les moyens de diffusion et la légitimité adéquats.

<!-- more -->

Débutant un doctorat dans le domaine de l'écriture numérique, la question du carnet de recherche (numérique) est pour moi essentielle.
Triplement essentielle. Publier des textes hétérogènes, trouver des échos dans différentes communautés, et obtenir une forme de légitimité.
Avec une hypothèse qui traverse ces trois horizons&nbsp;: les processus de publication ont une influence sur les pratiques d'écriture, il convient de les décortiquer.

## À l'origine, le carnet de thèse
Les carnets de thèse et de recherche numériques se sont démocratisés dans l'espace francophone grâce à [Hypothèses.org](https://hypotheses.org).
Les doctorants et les chercheurs ont désormais les moyens de créer très facilement un site ou blog, la nécessité de justifier un espace de publication numérique semble loin {{< cite "dacos_les_2010" >}}.
Outre l'outil d'écriture et de mise en ligne – un WordPress customisé –, un carnet Hypothèses offre plusieurs avantages supplémentaires&nbsp;: l'attribution d'un identifiant unique pour les publications en série ou numéro ISSN, l'inscription dans une communauté de chercheurs via le portail Hypothèses.org, et l'indexation dans les moteurs de recherche d'[OpenEdition](https://search.openedition.org/results?s=&pf=HO) et d'[Isidore.science](https://isidore.science).
La reconnaissance est un effet de bord, désormais si vous indiquez dans la communauté francophone des sciences humaines que vous disposez d'un carnet Hypothèses, la plupart des personnes comprendront de quoi vous parlez.

Il y a des initiatives de publication plus personnelles, hors de l'écosystème institutionnalisé et sous forme de sites personnels.
Trop rares selon moi.
Il serait pertinent d'en dresser une liste partielle et de pointer les éventuelles distinctions avec les blogs Hypothèses, mais ce n'est pas l'objet de ce billet.

La situation étant rapidement exposée, dans mon cas j'hésite entre deux choix&nbsp;:

1. conserver ce seul espace personnel d'écriture et de publication ([quaternum.net](https://www.quaternum.net)), en dédiant une partie du site au carnet de thèse ([là](/phd))&nbsp;;
2. publier également (doublement) sur un carnet hébergé sur la plateforme Hypothèses.org.

Comment s'explique cette deuxième option&nbsp;?
Profiter de l'écosystème offert par OpenEdition, principalement l'attribution d'un numéro ISSN, le rattachement à une communauté et l'indexation dans le moteur de recherche Isidore.science.
Pour gagner en légitimité&nbsp;: mon profil n'étant pas, à l'origine, académique, j'ai ce besoin de disséminer quelques-uns de mes billets sur des espaces identifiés comme académiques.
Je ne pense pas qu'un moteur de recherche généraliste suffise.
Et puis aussi parce que parfois je me sens seul sur mes sujets de prédilection, et peut-être qu'il y a d'autres personnes qui seraient ravies d'apprendre que des sujets comme Markdown, Git ou le _web to print_ sont abordés quelque part.
Et pas uniquement dans une forme comme la communication ou l'article, c'est-à-dire dans les circuits scientifiques traditionnels.

## Un espace de publication autonome et maîtrisé
Je milite pour une décentralisation des espaces de publication numérique et pour que chacun·e puisse maîtriser ses outils d'écriture – même si _militer_ est un bien grand mot.
Cela implique une démarche éditoriale et une connaissance des processus technologiques de publication.
Il s'agit là de deux dimensions techniques, il serait naïf de penser que la démarche éditoriale n'est pas technique, elle l'est même si elle n'implique pas nécessairement des notions de langages de balisage ou de programmation.
Le format de blog proposé par Hypothèses n'est pas des plus satisfaisant pour moi&nbsp;:

- je ne souhaite pas utiliser WordPress, mais alors [vraiment pas](/2012/12/23/pourquoi-quitter-wordpress/). WordPress est un peu au blog ce que Word est au traitement de texte {{< cite "dehut_en_2018" >}}, un succès populaire bienvenu qui a engendré une confusion entre fond et forme&nbsp;;
- les thèmes disponibles semblent assez peu performants et résilients avec beaucoup trop de JavaScript, et ergonomiquement discutables. Je suis tout à fait conscient qu'OpenEdition n'a pas vraiment le choix à ce niveau, maintenir un nombre restreint de thèmes pour plus de 3000 blogs est une nécessité. J'ai des sueurs froides rien qu'à entrevoir la complexité de la chose&nbsp;;
- la gestion bibliographique n'est pas permise. Qu'est-ce qu'une "gestion bibliographique"&nbsp;? Utiliser un logiciel/service comme Zotero ou un format comme [BibTeX](https://fr.wikipedia.org/wiki/BibTeX). Écrire dans un espace numérique et devoir gérer une bibliographie à la main est profondément masochiste&nbsp;;
- qu'en est-il de la pérennité&nbsp;? WordPress a le défaut de dépendre d'une base de données et d'un langage dynamique, une configuration modulaire s'inspirant de la [JAMstack](https://jamstack.wtf/) consoliderait la durabilité, à mon humble avis. Et il est tout à fait possible de brancher un CMS sur un générateur de site statique.

Il faut préciser un point important ici&nbsp;: j'aime expérimenter avec mon espace de publication personnel (mon site) et j'ai quelques connaissances techniques me facilitant la tâche.
Mais ce n'est pas le cas de tout le monde, et cela ne me semble pas être sensé que de le souhaiter.
En revanche permettre à chacun·e de gagner en littératie numérique {{< cite "vitali-rosati_les_2018-2" >}}, d'autant plus dans le domaine académique, n'est sans doute pas possible avec un modèle fermé comme celui-ci.

## L'après carnet de thèse
Un carnet de thèse peut-il se transformer en site personnel et atelier numérique pour un chercheur ou une chercheuse&nbsp;?
Je ne suis pas certain que gérer son identité numérique de chercheur avec un carnet Hypothèses soit une bonne idée.
Déjà parce que WordPress impose des contorsions désagréables, d'autre part parce que le modèle Hypothèses s'est formé autour des thématiques de recherche plutôt que des personnes.
Favoriser les projets – d'écriture, de recherche – est une bonne chose, mais sur le long terme il faut s'interroger sur la pertinence de ne disposer que d'un carnet Hypothèses comme espace de publication numérique.
Par exemple si nos sujets de recherche évoluent, ou que le blog était pensé comme accompagnant la thèse.

De l'autre côté réduire un site web personnel à quelques lignes liminaires, une maigre bibliographie et un CV est dommage sinon à éviter.
L'idéal étant de conjuguer une vitrine biographique, et un carnet de recherche qui présente tout l'hétérogénéité et l'évolution d'une pratique académique, voir aussi des prises de position ou des choses qui n'ont rien à voir.
Écrire, _avant tout_ {{< cite "roberts_ten_2017" >}}, publier les brouillons.
Mais je m'écarte du sujet initial.

## Éloge du brouillon
Grâce aux échanges avec [Arthur](https://arthurperret.fr) et à ces quelques lignes, j'entrevoie une solution pour moi, et qui pourrait s'étendre à d'autres&nbsp;:

- légitimité&nbsp;: ne pas ouvrir de carnet Hypothèses si cela n'est pas pertinent en terme de communautés&nbsp;;
- ["reconnaissance documentaire"](https://arthurperret.fr/2019/11/18/pour-un-autre-carnet-de-recherche-numerique/#de-la-reconnaissance-du-blog-scientifique)&nbsp;:
  - se poser la question de l'attribution d'un numéro ISSN sans que cela ne soit une obligation, voir même de DOIs pour certains articles (soyons fous)&nbsp;;
  - chercher des solutions d'indexation dans des moteurs de recherche spécialisés comme Isidore.science.

Et, mais c'est ambitieux et peut-être vain, esquisser le schéma d'une infrastructure technique comme alternative à WordPress, mais ce sera sur du long terme et pas tout seul.
L'idée de fond est donc de pouvoir donner autant de légitimité à des carnets indépendants qu'à des blogs Hypothèses, et qu'ils bénéficient d'autant de visibilité qu'ils ne promeuvent une pluralité de tons, de formes et d'écritures.
Valoriser le brouillon – la recherche en train de se faire – mêlé aux publications des circuits conventionnels comme les revues et les monographies.
Les carnets font partie, pour certain·e·s, d'une même démarche d'écriture que les articles ou les communications, valorisons ces espaces de publication&nbsp;!

## Quelques références

{{< bibliography >}}
