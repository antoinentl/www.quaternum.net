---
title: Déployer
date: 2021-04-06
---
Je débute une performance scientifique dans le cadre du colloque [Études du livre au XXI<sup>e</sup> siècle](https://projets.ex-situ.info/etudesdulivre21/), mon texte progressivement mis en ligne s'intitule [Déployer le livre](https://deployer.quaternum.net/).
