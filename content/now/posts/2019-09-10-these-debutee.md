---
title: Thèse débutée
date: 2019-09-10
status: avant
---
Depuis septembre 2019 je réalise un doctorat à l'Université de Montréal au Département des littératures de langue française, sous la direction de [Marcello Vitali-Rosati](https://vitalirosati.com/) et [Michael Sinatra](https://www.michaelsinatra.org/), à Montréal.
