---
title: Option humanités numériques
date: 2021-06-20
---
J'ai changé de département au sein de l'Université de Montréal, je suis passé du Département des littératures de langue française au Département de littératures et de langues du monde, principalement pour rejoindre la première cohorte du doctorat en littérature [option Humanités numériques](https://admission.umontreal.ca/programmes/doctorat-en-litterature/structure-du-programme/#segment-5).
