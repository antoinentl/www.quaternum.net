---
title: "Réel-Virtuel, mise en ligne"
date: 2019-02-06
---
Mon article [Markdown comme condition d'une norme de l'écriture numérique](http://www.reel-virtuel.com/numeros/numero6/sentinelles/markdown-condition-ecriture-numerique) a été mis en ligne le 30 décembre 2018 sur le site de la revue Réel-Virtuel, il s'agit de mon premier article pour une revue.
