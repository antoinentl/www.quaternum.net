---
title: Premier billet
date: 2019-09-19
---
Je viens de publier mon premier billet _de doctorat_, [Mes outils](https://www.quaternum.net/2019/09/21/mes-outils/), qui explique très brièvement quelques outils que j'utilise dans mes pratiques d'apprenti chercher. Je vais tenter de rassembler ces articles de blog quelque part sur quaternum.net.
