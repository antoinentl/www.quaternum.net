---
title: Réel-Virtuel
date: 2018-10-26
---
Je viens de mettre en ligne un article écrit il y a un an, bientôt publié sur la revue [Réel-Virtuel](http://www.reel-virtuel.com/)&nbsp;: [Markdown comme condition d'une norme de l'écriture numérique](/2018/10/18/markdown-comme-condition-d-une-norme-de-l-ecriture-numerique/).
