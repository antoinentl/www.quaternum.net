---
title: Novendécaméron
date: 2022-11-30
status: avant
---
Le recueil [_Le Novendécaméron_](https://novendecameron.ramures.org/) a été imprimé en plusieurs exemplaires, c'est une forme d'aboutissement d'un projet qui a débuté au printemps 2021.
Mené avec [Louis-Olivier](https://www.loupbrun.ca/), avec Chantal Ringuet et Jean-François Vallée à l'édition, c'est un chouette projet avec des chouettes personnes.
