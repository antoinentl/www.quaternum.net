---
title: Écrire
date: 2023-01-29
---
Depuis plusieurs mois j'ai débuté la _rédaction_ de ma thèse, phase qui s'accélère depuis le début de l'année.
C'est réjouissant, épuisant, excitant et puissant (la démarche plus que ce que j'écris…).
