---
title: Corrections
date: 2019-05-07
---
J'ai enfin intégré des corrections à mon mémoire de Master, "Vers un système modulaire de publication&nbsp;: éditer avec le numérique", toujours disponible en ligne&nbsp;: [https://memoire.quaternum.net](https://memoire.quaternum.net).
