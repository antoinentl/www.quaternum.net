---
title: Fin
date: 2019-07-05
---
Je finis un projet de publication de catalogue de musée avec [Julie](http://julie-blanc.fr/) qui sera en ligne en septembre 2019, nous nous inspirons fortement de la chaîne de publication [Quire](https://gettypubs.github.io/quire/) pour réaliser ce travail.
