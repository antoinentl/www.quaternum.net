---
title: "Bibliographie"
date: 2024-01-01
status: "avant"
---

La page listant [mes publications](/publications) (académiques) est plus présentable, avec une mise à jour et une catégorisation — en attendant que je trouve le temps de fabriquer un mécanisme plus complet (notamment pour intégrer les dépôts sources, ou pour récupérer certaines publications et les afficher ailleurs).
