---
title: Catalogue
date: 2019-12-10
---
Après plus d'un an passé sur un projet de publication de catalogue de musée avec [Julie](http://julie-blanc.fr/), la version web est en ligne&nbsp;! Attention peinture fraîche&nbsp;: [https://villachiragan.saintraymond.toulouse.fr](https://villachiragan.saintraymond.toulouse.fr).
