---
title: Inscrit
date: 2019-07-19
---
Je suis inscrit en doctorat (Département des littératures de langue française, Université de Montréal), je débute une thèse sous la direction de [Marcello Vitali-Rosati](https://vitalirosati.com/) en septembre 2019 à Montréal, ce qui me remplit de fierté (et m'angoisse un peu aussi).
