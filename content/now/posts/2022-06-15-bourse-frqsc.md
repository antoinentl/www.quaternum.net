---
title: Une bourse FRQSC
date: 2022-06-15
status: avant
---
J'ai obtenu une bourse du Fond de recherche du Québec Société et culture pour mon doctorat.
Au delà de l'aspect financier substantiel (quoique, les vases sont parfois communicants…), c'est une reconnaissance de ma recherche en cours et de mon _dossier_ académique.
