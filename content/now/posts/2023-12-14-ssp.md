---
title: Single source publishing
date: 2023-12-14
status: avant
---
Après plusieurs années de recherche sur les principes du _single source publishing_, j'ai publié un article (co-signé avec Yann Audin), ["The Importance of Single Source Publishing in Scientific Publishing"](https://www.digitalstudies.org/article/id/9655/), dans la revue _Digital Studies/Le champ numérique_.
J'ai aussi donné quelques raisons de perdre du temps avec cette idée de publier plusieurs formats à partir d'une source unique dans [un épisode](https://skhole.ecrituresnumeriques.ca/episodes/ep2_fauchie/) du podcast Skholé, produit par la Chaire de recherche du Canada sur les écritures numériques (j'y dis quelques bêtises).
