---
title: Fin master
date: 2019-09-05
---
Je ne suis plus responsable de l'unité d'enseignement "Piloter un projet d'édition numérique" du Master Publication numérique de l'[enssib](https://www.enssib.fr/), après quatre années denses et quatre promotions d'étudiant·e·s incroyables.
