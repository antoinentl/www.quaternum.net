---
title: "Humanistica 2023"
date: 2023-07-04
status: avant
---
À l'occasion du colloque francophone sur les humanités numériques, Humanistica, j'ai pu présenter des travaux en lien avec ma thèse et autour de la notion de _fabrique_.
Un résumé allongé (parce que ce ne sont pas vraiment des actes…) est [disponible sur HAL](https://hal.science/HUMANISTICA-2023/hal-04128280v1).
