---
title: Communs éditoriaux
date: 2022-01-15
---
Premier article publié dans la Revue Française des Sciences de l'information et de la communication, écrit à quatre mains avec Valérie Larroche !
Article disponible en accès libre : [Quel régime de conception pour les communs éditoriaux ?](https://journals.openedition.org/rfsic/11994).
