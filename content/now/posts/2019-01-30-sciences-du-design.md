---
title: Sciences du design
date: 2019-01-30
---
L'article co-écrit avec Thomas Parisot, [Repenser les chaînes de publication par l’intégration des pratiques du développement logiciel](https://gitlab.com/antoinentl/readme.book/) a été publié dans le huitième numéro de la revue Sciences du Design, je suis très fier de voir ce texte disponible, un gros travail d'écriture avec Thomas et l'équipe de la revue. L'article est [disponible sur Cairn.info](https://www.cairn.info/revue-sciences-du-design-2018-2-page-45.htm) (et sur demande).
