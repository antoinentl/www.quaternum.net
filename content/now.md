---
layout: page
title: "Maintenant – Now"
type: pages
---
Cette page [Now](https://nownownow.com/about) présente mes différents projets et activités, _en ce moment_&nbsp;:

* je débute une performance scientifique dans le cadre du colloque [Études du livre au XXI<sup>e</sup> siècle](https://projets.ex-situ.info/etudesdulivre21/), mon texte progressivement mis en ligne s'intitule [Déployer le livre](https://deployer.quaternum.net/)&nbsp;;
* j'ai publié [mon premier recueil des commits de mon atelier de thèse](/2020/10/27/apercu-01/), c'est crypto-littéraire&nbsp;;
* [la série](/fabriques) sur les _fabriques de publication_ s'est ralentie mais je continue activement de travailler dessus&nbsp;;
* Julie a écrit [un billet complet](https://julie-blanc.fr/blog/2020-11-05_chiragan/), orienté design, sur notre travail pour le Musée Saint-Raymond&nbsp;;
* depuis ces derniers mois j'ai mis en place une nouvelle chaîne éditoriale pour produire des supports de cours, j'espère trouver le temps de la présenter, ce serait l'occasion de me remettre dans celle que j'avais bricolé avec AsciiDoc et Asciidoctor&nbsp;;
* depuis septembre 2019 je réalise un doctorat à l'Université de Montréal au Département des littératures de langue française, sous la direction de [Marcello Vitali-Rosati](https://vitalirosati.com/) et [Michael Sinatra](https://www.michaelsinatra.org/), à Montréal&nbsp;;
* mon mémoire de Master, "Vers un système modulaire de publication&nbsp;: éditer avec le numérique", est toujours disponible en ligne&nbsp;: [https://memoire.quaternum.net](https://memoire.quaternum.net).

_Cette page a été mise à jour le 10 mars 2021._
