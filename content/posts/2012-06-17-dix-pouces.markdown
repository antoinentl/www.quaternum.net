---
comments: true
date: 2012-06-17
layout: post
slug: dix-pouces
title: Dix pouces
categories:
- carnet
---
Depuis presque un an mon environnement numérique personnel se résume la plupart du temps à dix pouces, la taille de l'écran de mon netbook qui me suit (presque) partout.

Cet écran fait plus exactement 10,1 pouces, et 16/9e en proportion, soit 1024 par 600 pixels, c'est le seul dont je dispose, y compris en mode "bureau" (je compte tout de même ajouter un deuxième écran à un moment ou à un autre). Au départ je me suis gravement interrogé sur la façon de lire, écrire, interagir sur un si petit écran au format à l'italienne, Windows 7 Starter y était installé par défaut, impossible à manipuler, ergonomie ratée (au moins pour des écrans de cette taille), lenteur et inutilité des effets. Dès le passage à Ubuntu et à l'interface Unity, cet écran ne m'a plus paru ridicule. En écrivant ces lignes je me rends compte que c'est le même format, la même proportion (mais pas la même "taille") que mes carnets de dessin, peut-être y a-t-il un lien.

Loin de moi l'idée de lancer un débat sur l'interface Unity des versions "récentes" d'Ubuntu, c'est la seule que je connaisse vraiment, les essais sous d'autres interfaces se sont à chaque fois soldés par des échecs. L'avantage d'Unity est la place laissé aux contenus, pas de barre de tâches en bas de l'écran, juste une barre en haut qui fait presque tout. Un lanceur est à disposition sur le côté gauche, masqué la plupart du temps, il permet d'y placer les applications que l'on souhaite ouvrir facilement.

<img width="600" height="350" alt="" src="/images/quaternum_blog_dix-pouces01.jpg">

La découverte de alt+tab a changé ma perception de l'ordinateur, voire même de l'informatique, et plus encore avec Ubuntu (sans trop savoir pourquoi, puisque l'utilisation est la même sous Windows). Désormais j'utilise relativement peu le curseur, la combinaison alt+tab me permet de passer d'une application lancée à une autre (d'où l'absence de barre de tâches). De la même façon, la manipulation des onglets d'un navigateur se fait par la combinaison shift+tab (de gauche à droite) ou ctrl+shift+tab (de droite à gauche), et ça change tout (surtout vu le temps passé sur le Web et donc sur un navigateur). Je ne parle pas de rapidité d’exécution, mais plutôt de facilité d'appréhension, et du plaisir d'oublier l'effort que requiert la souris pour passer d'une "fenêtre" à une autre.

Le format à l'italienne (16/9e) permet de répartir les espaces de "travail", de diviser l'écran en deux bandes verticales, égales ou non, pour certaines applications. Je ne rentrerai pas dans le détail des différentes applications que j'utilise et de l'espace qu'elles prennent sur ces dix pouces, mais par exemple Firefox prend tout l'espace, alors que sur un même écran je vais placer Hotot (client Twitter) à moins de la moitié en largeur, et Sublime Text 2 (éditeur de texte) sur le reste de la largeur.

<img width="600" height="350" alt="" src="/images/quaternum_blog_dix-pouces02.jpg">

<img width="600" height="350" alt="" src="/images/quaternum_blog_dix-pouces03.jpg">

La seule application qui me pose problème sur dix pouces, c'est Gimp, c'est trop difficile de prendre un minimum de recul sur les images et de répartir les fenêtres consacrées aux différents outils sur cet espace.

Je me suis tant fait à cette disposition que le "grand" écran (dix neuf pouces) que j'utilise quotidiennement dans le cadre de mon travail me perturbe. J'ai désormais du mal à agencer cet espace presque carré, et je rêve d'un deuxième écran pour pouvoir à nouveau disposer d'un format 16/9e.