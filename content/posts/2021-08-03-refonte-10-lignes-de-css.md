---
title: "Refonte : 10 lignes de CSS"
date: 2021-08-03T08:00:00-04:00
categories:
- carnet
groupe: refonte
tags:
- refonte
---
Pour rendre lisible ce site web pendant les travaux, quelques lignes de CSS suffisent.

Plutôt que de mettre en ligne une version finalisée du site, je me suis inspiré de la feuille de style de Sylvain Durand ([voir cette version archivée](https://web.archive.org/web/20210305081103/https://sylvaindurand.org/)) qui tient en quelques lignes de CSS.
Les réglages sont minimalistes, et sembleraient acceptables pour une utilisation sur le long terme.
Sauf que :

- certains détails typographiques nécessitent un peu plus que quelques lignes de CSS ;
- je ne veux pas renoncer à embarquer une ou plusieurs fontes, même si je suis conscient des bienfaits des fontes systèmes ;
- je voudrais faire un peu de programmation avec CSS, notamment gérer des affichages semi-dynamiques ;
- je ne vais pas échapper au thème sombre...
- je voudrais bien faire du SASS et faire appel à des variables pour faciliter l'évolution de la mise en forme.

Actuellement le style de ce site repose sur ces 10 lignes :

```nohighlight
body { font: 17px/1.5em serif; margin: 0 auto 3em; padding: 2em 20px; max-width: 800px; color: #030D23; }
nav ul, nav li { margin: 0; padding: 0; list-style-type: none; }
header { margin-bottom: 3em; }
nav { margin: 0 0; max-width: 20em; }
a { color: inherit; }
a:hover { color: inherit; }
pre { color: white; background: #030D23; padding: 1em; white-space: pre-wrap; word-break: keep-all; word-break: break-all; }
pre, code { font: 14px/1.5em monospace; }
h1, h2, h3, h4 { font-size: 18px; font-weight: 600; margin: 1.5em 0 0; }
h1 { font-size: 21px; margin-bottom: 1.5em; }
```