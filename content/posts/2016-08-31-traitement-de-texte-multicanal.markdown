---
layout: post
title: "Traitement de texte multicanal"
date: "2016-08-31T09:00:00"
comments: true
published: true
description: "Quels sont les avantages et les inconvénients du texte formaté, du texte brut et de Markdown dans un environnement de pratiques d'écriture hétérogènes ?"
image: "https://www.quaternum.net/images/full-multichannel-text-processing-mini.png"
categories: 
- carnet
---
Dans la grande histoire des logiciels de traitement de texte, le texte naît de la rencontre entre Microsoft Word et une imprimante. Aujourd'hui, il est écrit et édité sur plusieurs dispositifs et applications, puis envoyé par mail, imprimé, copié, collé, annoté, publié, syndiqué et agrégé (RSS), partagé et re-partagé, en utilisant toutes sortes d'outils et de plateformes. Les formats propriétaires, par nature obstinés, s'insèrent difficilement dans ce nouvel environnement frénétique. Le texte brut fait mieux, mais il lui manque la mise en forme. Markdown pourrait être notre joker. Si seulement il était plus attirant !  
Quels sont les avantages et les inconvénients du texte formaté, du texte brut et de Markdown[^1] dans cet environnement de pratiques d'écriture hétérogènes ?

<!-- more -->

## Avant-propos
Ce texte est une traduction française de l'article de [iA](https://ia.net/), "Multichannel Text Processing", publié originellement sur le site de Information Architects Inc. le 10 juin 2016 :  
[https://ia.net/know-how/multichannel-text-processing](https://ia.net/know-how/multichannel-text-processing)

Cette traduction a été réalisée par [Antoine Fauchié](/) et [Valentin Famelart](http://ex-retis.com/), puis corrigée et modifiée par [Jiminy Panoz](http://jiminy.chapalpanoz.com/) et [Anthony Masure](http://www.anthonymasure.com/), puis relue par [Julien Taquet](https://twitter.com/John_Tax), [Nicolas Taffin](http://polylogue.org/) et [Frank Adebiaye](http://www.fadebiaye.com/). Merci à eux pour le travail fourni, et merci à iA d'avoir accepté que cette traduction soit publiée.  
**Hasard heureux, [Frank Taillandier](http://frank.taillandier.me/) a également traduit ce texte, il est consultable [ici](http://frank.taillandier.me/2016/08/28/traitement-de-texte-multicanal/), une fusion des deux traductions est prévue !**

©2016 iA Inc. pour le texte original, Août 2016 CC BY-NC-SA pour la traduction.

- [Texte formaté](#texte-format)
- [Texte brut](#texte-brut)
- [Markdown](#markdown)
- [Un processus de publication moderne](#un-processus-de-publication-moderne)
- [Conclusion](#conclusion)
- [Bonus : différents types de workflows](#bonus--diffrents-types-de-workflows)

![](/images/full-multichannel-text-processing.png)

## Texte formaté
Les textes formatés avec Microsoft Word ou avec le format .rtf sont connus pour leurs promesses liées au WYSIWYG (What You See Is What You Get, ce que vous voyez est ce que vous obtenez, en français). Le gras, les italiques, les différentes polices et options de formatage peuvent être utilisées, tout en voyant immédiatement le résultat à l'écran ! Ce fut une révolution, comparé au formatage maladroit "voir le code source", ou à aucun formatage du tout. Mais il y a des inconvénients.

![](/images/plain-text-vs-rich-text-plain-text.png)

Avec le texte brut, le texte *est* la source. Avec le texte formaté, nous voyons une simulation. Ce que nous voyons peut nous satisfaire, mais sous la surface notre traitement de texte produit un texte encodé plus complexe. Vous pouvez découvrir ce monde caché en créant un document Word, écrire "Hello World", et enregistrer, puis changer l'extension en `.zip` et décompresser le fichier. Bienvenue en 1979 ![^2]

![](/images/hello-world.png)

Si vous êtes assez courageux pour regarder dans le dossier obtenu, vous pouvez commencer à vous demander si vous avez écrit "Hello World" ou "Hello Hell" (bonjour l'enfer) :

![](/images/hello-hell.png)

Le texte mis en forme est plus lourd que le texte brut. Le principal problème avec les formats personnalisés est que la relation entre le code source et le texte -- entre ce que nous voyons et ce que nous ne voyons pas -- est fantasque. Voici ce que vous obtenez vraiment lorsque vous travaillez avec ces formats en 2016 :

### Bugs et problème d'UX
Comment sortir d'une liste ou enlever une indentation ? Comment annuler un lien sur un mot ? Comment se débarrasser de cette mise en forme en gras, de cette hauteur de ligne ou de la taille de ce titre ? Et comment diable pouvez-vous placer deux images côte à côte ? Parfois on ne sait pas si on a affaire à un bug ou à une mauvaise conception de l'interface.

### L'impasse mexicaine du copier-coller [^10]
Le principal défi des formats personnalisés, dans un environnement de publication multicanal, est qu'ils cassent le copié-collé. Nous copions un simple paragraphe depuis un PDF, en le collant dans notre email le texte français tourne au *Western spaghetti Italien* [^11], avec beaucoup d'espaces inquiétants et de sauts de ligne. Et il n'y a pas que le PDF. Avec du texte formaté nous ne savons jamais ce qui va se passer au moment où nous *collons*.

### Compatibilité
Alors que le `.rtf` est largement supporté et que de nombreux traitements de texte lisent le `.docx`, différents logiciels interprètent ces formats différemment. Vous ne pouvez pas, de manière fiable, copier du RTF ou du DOCX dans votre [CMS](https://fr.wikipedia.org/wiki/Syst%C3%A8me_de_gestion_de_contenu). Et oubliez tout de suite l'idée de faire des allers-retours entre votre CMS et votre document source sur Word.

### Exports multiples
Exporter paraît possible "d'un seul click", mais exporter votre texte en plusieurs versions complexifie votre processus de publication. Les retours et les corrections ne peuvent pas être facilement intégrés dans un unique fichier source. Gérer ces différentes versions peut vite devenir un cauchemar.

### Accessibilité
Le texte enrichi ne vous permet pas d'éditer la source du document. Peut-être que le texte est dans un dossier qui se prétend être un fichier, ou caché ailleurs, "à l'abri de l'utilisateur", enfoui bien profond sous un monceau de code abscon, ou encrypté dans le Fort Knox d'une base de donnée secrète.

Bien entendu, en tant que développeur d'affaires, vous adorez la cage dorée des formats propriétaires. En tant que personne lambda écrivant des textes en 2016, utilisant différentes applications, appareils, plateformes et formats, vous ne les aimez pas. Et qui sait comment nous pourrons gérer du `.docx` dans 10 ans ? Ou dans 30 ?

>Bien que les traitements de texte modernes peuvent faire des choses fantastiques -- ajouter des graphiques, des tableaux et des images, appliquer une mise en forme sophistiquée -- il y a une chose qu'ils ne peuvent pas faire : garantir que les mots que j'écris aujourd'hui seront lisibles dans dix ans. Ce n'est qu'une des raisons qui font que je préfère travailler en texte brut : c'est intemporel. Mes petits enfants seront capables de lire un fichier texte que j'aurais créé aujourd'hui, bien après que qui que ce soit pourra se rappeler ce qu'est un fichu fichier .docx.[^3]

Dans l'environnement de texte multicanal d'aujourd'hui, les formats de texte enrichi créent plus de problèmes qu'à une époque moins complexe. L'idée que vous ayez besoin d'installer une certaine version d'une application sur une certaine version d'un système d'exploitation pour ouvrir un fichier apparaît comme une blague. Afin d'être interopérable entre différentes applications et plateformes, le texte lui-même a besoin de se libérer des restrictions d'applications, de plate-formes ou de terminaux.

## Texte brut
Le seul format de fichier qui fonctionne comme prévu, partout, est le *No format*, en d'autres termes : du texte brut. Et c'est tout ce dont nous avons besoin pour écrire notre premier jet.

>Texte brut veut dire : des mots séparés par des espaces ; des phrases séparées par des points ; des paragraphes en général séparés par un saut de ligne. Si vous travaillez dans le domaine de l'écriture, dans celui de la publication ou de l'écriture de scénarios, c'est souvent tout ce dont vous avez besoin.[^4]

Le texte brut est simple. Il vous aide à vous concentrer sur ce que vous avez à dire.

Le texte brut est libre. TextPad, TextEdit, Vim, votre téléphone, la vieille application AOL de 1997 de votre oncle... Pas besoin de se mettre sur son 31.

Le texte brut est léger.

Le texte brut coule comme de l'eau de source. Mais contrairement à l'eau, il n'étanche pas toutes les soifs. Que ce soit une version imprimable, un article de blog, un PDF, un email ou même un fax, tôt ou tard ce texte devra prendre une forme appropriée au média pour être lu. Dès lors que vos mots s'inscrivent dans un média, ils requièrent une structure visuelle. Le matériau professionnel a besoin d'un en-tête, d'un pied de page et d'une page de couverture. Certains textes ne viennent à la vie qu'après avoir été illustrés et enrichis d'images, de vidéos ou de tableaux. Nous voulons pouvoir faire des liens hypertextes lorsque nous écrivons en étant connectés. Nous avons besoin de notes de bas de page pour la version imprimée.

La transition du texte brut au texte formaté est en général abrupte et irréversible. Vous écrivez dans TextEdit ou Notepad, mais une fois que vous migrez vers du RTF, Docx ou HTML, il n'y a plus de retour possible. Cela dit, le texte veut passer naturellement de l'état de mots simples à l'état de prose formatée. C'est ici que Markdown entre en scène.

## Markdown
Les langages de balisage, comme le Markdown, MediaWiki ou LaTeX, vous permettent de structurer vos mots sans avoir à construire une usine à gaz derrière le texte brut. Malheureusement...

### Markdown c'est nul !
Vous avez peut-être tenté d'écrire en Markdown ou d'éditer une entrée dans Wikipédia et *détesté* ça, parce que "Pourquoi voudrais-je apprendre une nouvelle 'syntaxe' pour formater du texte alors que j'ai un outil qui le fait en un clic, et qui me montre exactement ce qui sort ?"[^5] Et vous avez raison :

- écrire dans des balises peut résoudre certains problèmes de copier-coller, mais du pur Markdown a toujours l'air désordonné ;
- alors que le Markdown est plus simple que le HTML, vous avez toujours besoin de retenir la syntaxe, et vérifier comment ajouter un lien vous sort à chaque fois de votre enchaînement ;
- Markup, Markdown, MultiTruc... Ces formats ont leurs propres problèmes de compatibilité.

Markdown n'est pas la solution parfaite pour tous les types de rédacteurs, ou pour toutes les formes et étapes d'écriture. Mais si vous faites tout vous-même depuis la prise de note jusqu'à la publication, c'est la solution la plus efficace à ce jour. Si vous avez un éditeur de texte qui vous impose votre outil d'écriture, Markdown n'est généralement pas une option. Mais là encore, la possibilité d'être plus facilement partageable que n'importe quel format de fichier traditionnel peut rendre la collaboration bien plus aisée...

### Un plaidoyer pour Markdown
Mis à part l'aspect esthétique, Markdown est imbattable si les seules choses que vous faites sont **gras** et *italique*, et taper `#` ou `##` ou `###` pour différents niveaux de titre peut paraître maladroit. Mais, une fois la syntaxe apprise, taper des symboles croisillon est plus rapide et facile que de lâcher le clavier, trouver le curseur, sélectionner du texte, cliquer sur "Titre" dans l'onglet des Styles, et choisir le bon niveau de titre. Et contrairement à ces styles capricieux, les symboles croisillon se comportent toujours comme ils le doivent.

Si vous connaissez ces trois choses, `*`, `**` et `#`, vous en savez assez à propos de Markdown pour commencer. Et plus vous devenez meilleur en Markdown, moins vous rencontrez de résistance dans votre processus d'écriture. Voyons quelques défis plus complexes :

#### Les liens
Les liens en markdown peuvent complètement ruiner l'apparence de votre texte. C'est pratique de voir le lien dans le texte et ne pas avoir à gérer les cliques-droits et les pop-ups. Mais si vous utilisez trop de liens, le texte devient illisible. Un moyen de contourner ce problème est d'utiliser des références de liens -- qui apparaissent alors en bas de page. Un autre moyen pourrait être de réduire les liens dans l'éditeur, mais on retrouve là les problèmes du Texte Enrichi.

#### Multimedia
Avoir plus de libertés avec les images dans le texte est cool. Si vous voulez un programme qui gère la position des images correctement, utilisez InDesign ou faites-le dans votre CMS. N'allez pas vers Word et, Bon Dieu, n'attendez pas de votre Markdown qu'il fasse tout parfaitement alors qu'il vous aide à écrire votre message pour mieux le diffuser. Oui, vous pouvez utiliser du Markdown dans InDesign.[^6]

#### Tableaux
Les tableaux en texte brut ont une mauvaise réputation. S'ils sont complexes, ils paraissent en effet horribles. Mais pour quelques lignes et colonnes ils peuvent très bien fonctionner : à l'inverse de Word, vous voyez exactement ce qui se passe. (Si vous faites des tableaux évolués, Word est votre ennemi quoi qu'il arrive de toute manière -- utilisez Excel ou Numbers.) Ils sont énervants à créer, eux aussi ; mais avec [un peu de magie](https://vimeo.com/160875330)...

#### Automatisation
Avec MultiMarkdown[^7] (une version de Markdown dopée aux hormones), vous pouvez automatiser la création et la mise à jour de tables des matières, et avec des métadonnées vous pouvez même concevoir des index. Ok, ça paraît très compliqué. Et *c'est* compliqué. Mais vous devriez [essayer](https://vimeo.com/158324329). Vous n'avez pas besoin d'être un hacker ninja rockstar pour comprendre les bases. Et une fois que vous comprenez les bases, il devient plus simple de [générer une table des matières dans MultiMarkdown](https://vimeo.com/158311378) que dans MS Word.

#### Notes de bas de page
MultiMarkdown gère également les notes de bas de page. La syntaxe est un peu obtuse, à la manière des liens, mais de toute façon ils sont tout aussi tortueux dans les éditeurs de texte enrichi. Mais avec un [éditeur Markdown qui permet de prévisualiser](https://vimeo.com/158933545), vous pouvez apprendre la syntaxe en cliquant.

Plus vous maîtrisez Markdown, plus vite et plus simplement vous naviguez entre le texte brut et le texte formaté. C'est là que Markdown excelle : en *créant des ponts* entre le texte brut et le texte enrichi, Markdown vous permet de continuellement façonner votre texte -- depuis le premier brouillon jusqu'à la publication multicanale.

#### Rendu en direct
Il y a de nombreuses manières pour améliorer le rendu du Markdown, par exemple le rendre invisible, mais si vous remplacez du Markdown brut par un rendu WYSIWYG, vous réintroduisez tous les problèmes qui rendent le Texte Enrichi caduc, et vous en ajoutez de nouveaux. Si vous essayez de faire en Markdown tout ce qu'un logiciel WYSIWYG comme Word fait, vous finirez par reconstruire ce traitement de texte autour d'un langage qui n'est pas conçu pour le WYISWYG. C'est pourquoi l'application iA Writer affiche les caractères Markdown.

## Un processus de publication moderne

Nous pouvons imaginer les différentes étapes d'un processus d'écriture comme étant des phases séparées qui peuvent être contrôlées et établies dans un ordre chronologique. Dans la réalité, nous prenons des notes avant d'écrire, puis nous continuons d'accumuler du matériau jusqu'à la publication. L'édition commence avec le premier jet et -- pas seulement dans les médias numériques -- continue après la publication.[^8]

Des étapes séparées sont nécessaires dans un processus créatif, mais un processus d'écriture ne se comporte pas comme une cascade : les étapes se superposent, interagissent, s'entre-influencent, et nos outils d'écriture et de publication doivent nous permettre d'aller et venir à volonté.[^9] Écrire est une tâche chaotique par nature.

![](/images/workflow-note-draft-edit-publish.png)

"Se concentrer sur l'écriture" ne signifie pas "écrire avec des œillères", ça signifie plutôt que votre attention est concentrée sur un seul aspect du processus à la fois. Il peut même être utile d'aller et venir consciemment entre deux étapes voisines. Markdown vous permet de ne pas vous inquiéter de ces transitions.

### Écrire et prévisualiser
Aller et venir d'une étape à l'autre durant un processus est nécessaire et réjouissant. Si vous aimez travailler dans un éditeur WYSIWYG, ou imprimer de temps en temps, ou régulièrement prévisualiser sur votre blog avant de publier, vous le savez déjà : un formatage du texte bien défini nous aide à donner de la perspective au texte pour le lecteur. Voir notre texte imprimé change notre perception de celui-ci. Vous obtenez le même effet dès lors que vous convertissez votre Markdown et regardez votre rendu final en HTML.

![](/images/markdown-with-preview-plain-text.png)

L'effet n'est sans doute pas aussi impressionnant que le passage de l'écran au papier, mais il vous donne un aperçu de la structure du texte à venir.  
En vous mettant dans la peau du lecteur, vous pouvez reprendre votre texte avec un nouveau regard. C'est plus rapide et ça supprime la tâche longue et ennuyeuse de reporter dans votre document vos corrections manuscrites.

La transition consciente entre texte brut et texte formaté vous permet d'imaginer comment le texte se lira depuis l'extérieur.

La transition volontaire entre texte brut et texte formaté vous montre comment le texte est lu depuis l'extérieur.

### Éditer et Publier

Votre perspective sur le texte varie également lorsque que vous passez du front-end au back-end (et vice-versa) — lorsque que vous envoyez votre contenu dans votre CMS.

Si vous avez déjà écrit votre texte en Markdown, assurez-vous que votre outil de publication vous permette d'éditer le texte brut sans perdre votre formatage. Vous n'avez pas besoin de tout miser sur un CMS basé sur Markdown. Si vous utilisez Wordpress, vous pouvez installer un plugin Markdown pour l'éditeur de texte. C'est encore plus simple si votre application d'écriture supporte plusieurs plateformes dès le départ. Voilà pourquoi [iA Writer](https://ia.net/writer) vous permet de [publier directement sur Wordpress et Medium](https://vimeo.com/164238502).

Publier directement depuis votre application est cool. Néanmoins, la clé est de pouvoir copier-coller votre texte d'un support à l'autre sans perdre votre travail. Copier-coller est souvent plus rapide que d'appuyer sur des boutons. À moins que vous ne soyez un génie de la chaîne de production, vous ne vous en tirerez pas sans ajouter quelques touches finales sur InDesign, Wordpress ou Pages. À quoi bon utiliser un outil de publication si vous ne pouvez pas faire ce que vous faites sur votre traitement de texte ?


## Conclusion

Nous utilisons différents terminaux pour prendre des notes, différentes applis pour nos brouillons et nos corrections, nous envoyons du texte à d'autres gens, et nous utilisons plusieurs plateformes pour mettre en forme et publier nos propos. Le processus de production, et la forme finale de nos textes, sont devenus plus compliquées à appréhender. Nous avons besoin de quelque chose de mieux que la méthode de formatage traditionnelle qui nous enferme dans le cadre et la structure prédéfinis d'un format/application/plateforme/appareil, afin de faire face à la réalité complexe et actuelle du formatage et de la publication.

Parce qu'il est simple et universel, le texte brut peut nous mener plus loin que n'importe quel format de fichier. Cela dit, les éditeurs de texte brut ne sont pas faits pour structurer visuellement du texte, optimiser une disposition complexe, ajouter une typographie détaillée ou lier entre eux des blocs de texte. Ils sont parfaits pour trouver les bons mots, mais perdent de leur intérêt à mesure que le texte se développe. Un processus d'écriture moderne doit nous permettre de naviguer librement entre texte brut et texte formaté, que ce soit grâce à des outils automatisés ou par du copier-coller.

Pour le moment, les langages à balisage légers comme Markdown sont le seul moyen de faire cela. Markdown peut paraître un peu brouillon et, oui, toutes sortes d'améliorations sont possibles et nécessaires. Malgré ses limitations, il résout des problèmes méthodologiques complexes là où la séparation traditionnelle entre texte brut et texte formaté échoue. Il nous permet d'utiliser notre texte, et le fichier dans lequel il est contenu, partout, indépendamment des terminaux, plateformes ou applications. Le moment où vous déplacez vos textes, styles, dépendances et contenus, corps de texte et mise en forme, ne devrait plus être un point de non-retour, cela devrait être une phase sur laquelle vous pouvez aller et revenir. Markdown rend cela possible.

Si vous voulez des formatages plus complexes comme des liens, des images, des notes de bas de page ou une table des matières, utilisez les options ou les raccourcis. Pour améliorer votre maîtrise du Markdown avec des syntaxes plus avancées, aidez-vous d'un éditeur de texte qui permet la prévisualisation en direct. Cela vous donne aussi un aperçu de ce que votre formatage donnera, vous permettant d'utiliser un balisage plus complexe puisque vous pouvez immédiatement identifier n'importe quelle erreur.

L'automatisation entre votre service Cloud, votre application de prise de note, votre traitement de texte et votre environnement publication, c'est cool, mais pas indispensable. Ce qui *est* fondamental c'est la capacité de copier-coller votre texte de l'un à l'autre sans perdre votre formatage ou vos informations. Le texte brut est le seul moyen qui vous le garantit. Configurez votre plate-forme de publication pour interpréter le Markdown, et vous pourrez librement naviguer entre l'écriture, l'édition et la publication. Il existe de nombreuses manières et applications pour y parvenir. Et c'est justement là que nous voulons en venir. Le texte brut est libre et léger, et c'est bien ainsi qu'il doit rester. Évitez les applis qui veulent vous mettre dans de petites boîtes.

![](/images/appleiigsandimagewriterii.png)

## Bonus : différents types de workflows

Cet essai écrit par l'équipe de iA est un manifeste en faveur de l'utilisation du Markdown, il y a plusieurs mentions du logiciel [iA Writer](https://ia.net/writer), basé sur le langage sémantique Markdown. Si vous ne disposez pas d'un environnement OS X, iOS ou Android, il existe d'autres éditeurs de texte, comme [Sublime Text](http://www.sublimetext.com/) ou [Atom](https://atom.io/), ou d'autres, qui gèrent très bien Markdown. Voici quelques exemples de *processus de publication* ou *workflows* :

### Les applications et logiciels prêts à utiliser
iA Writer est simple d'utilisation, avec notamment des fonctionnalités de prévisualisation ou d'exports intégrées nativement, par exemple en PDF ou en .docx, ou même en connectant iA Writer à des plateformes comme Wordpress ou Medium. C'est donc un workflow très complet, prêt à utiliser. Il existe d'autres applications du même type, mais rares sont celles qui permettent de réaliser toutes les opérations courantes facilement, et sans devoir modifier des fichiers de configuration.

### Les CMS
Un CMS est un système de gestion de contenu, par exemple Wordpress ou Drupal sont des CMS. Jekyll est également un CMS -- mais sans base de données --, et on peut même considérer que Jekyll est un workflow gérant le Markdown, dont l'objectif est la publication en ligne -- j'en parlai [ici](/2014/04/26/latex-et-jekyll/). Il existe de nombreux autres systèmes similaires utilisant Markdown, dont certains sont des services en ligne clé en main -- comme [GitLab Pages](https://pages.gitlab.io/) ou [GitHub Pages](https://pages.github.com/).

### Les worklows faits maison
Si vous êtes curieux et que vous maîtrisez la programmation, vous pouvez créer votre propre worflow. Plusieurs méthodes sont possibles, l'une d'elle est de transformer un fichier Markdown en HTML+CSS, pour cela il vous faut gérer plusieurs étapes :

- la rédaction : un éditeur de texte banal fait bien l'affaire, mais si les quelques balisages sont graphiquement reconnus (comme sur iA, ou Sublime Text ou Atom avec un plugin) c'est plus confortable ;
- il faut transformer le Markdown en HTML, par exemple un mot en `*italique*` deviendra `<em>italique</em>` ;
- il faut ensuite attribuer une feuille de style à cette page HTML ;
- enfin, si vous souhaiter en faire un PDF -- pour envoi ou pour impression --, il faut penser à l'organisation du document : gestion des pages, liens internes, etc.

Vous pouvez créer un tel processus de publication en utilisant des plugins dans des éditeurs de texte comme Sublime Text ou Atom, et ensuite modifier -- relativement -- facilement la structuration des documents ou la mise en page. Certaines personnes créent même totalement leur workflow avec quelques lignes de code, afin de maîtriser et de personnaliser leur chaîne de publication.


[^1]: Le *texte brut Pur* est un fichier écrit en texte brut sans aucun formatage ou balisage. Le *texte brut* comprend tous les formats qui sauvegardent le texte directement dans le fichier (`.txt`, `.md`, `.markdown`, etc.). Par *Texte Enrichi* nous faisons référence à tous les formats qui sauvegardent les informations de formatage sur une couche invisible au sein d'un code source inaccessible (`.rtf`, `.docx`, `.pages`, etc.). *Texte Enrichi* fait référence à l'apparence visuelle d'un texte typographiquement amélioré -- indépendamment du format de fichier. *Markdown* est présenté comme un formatage Markdown dans un fichier de texte brut. Combiner Markdown avec un format de fichier fermé est contreproductif. [John Gruber](https://daringfireball.net/projects/markdown/), le créateur de Markdown, définit le Markdown comme une "syntaxe de formatage du texte brut". Cependant, dans les faits le Markdown peut être écrit dans du `.docx`, ou empaqueté et présenté dans n'importe quel format de fichier. Cela pose tout un tas de problèmes, mais n'oublions pas cette possibilité.

[^2]: Pourquoi 1979 ? “Le 29 Novembre 1979, c'est la première fois que le terme 'Microsoft' est utilisé par Bill Gates” (source : [Wikipedia, History of Microsoft](https://en.wikipedia.org/wiki/History_of_Microsoft)). La suite est mieux encore : "[1979] est clairement une date falsifiée — les spécialistes de la gestion de crise ne seront pas surpris d'apprendre que les mécréants peuvent modifier les métadonnées des fichiers, après leur création réelle sur le système, à l'aide de logiciels libres conçus pour cette tâche. Ce qui nous intéresse ici, c'est que cette date, 1979, se situe un jour avant la plus vieille date que vous pouvez spécifier dans la recherche Windows. Essayez de taper ‘F3’ ou d'aller dans Démarrer → Rechercher – vous ne pouvez pas spécifier une date antérieure au 1er janvier 1980. Vous devrez rechercher depuis la ligne de commande pour trouver des fichiers créés avant cette date.” (source : [Miscreant hiding techniques: Would the real explorer.exe please stand up? And the relevance of 1979 when doing searches…](https://blogs.technet.microsoft.com/robert_hensing/2005/01/10/miscreant-hiding-techniques-would-the-real-explorer-exe-please-stand-up-and-the-relevance-of-1979-when-doing-searches/))

[^3]: [Why Plain Text is the Best](http://www.macworld.com/article/1161549/forget_fancy_formatting_why_plain_text_is_best.html), par David Sparks.

[^4]: [Words, words, words](http://dooling.com/index.php/2012/12/20/plain-text-for-authors-writers/), par Richard Dooling.

[^5]: [Gradually falling in love with Markdown](http://prolost.com/blog/2012/7/16/gradually-falling-in-love-with-plain-text.html), par Stu Maschwitz.

[^6]: Voici une des nombreuses solutions : [markdownID: Markdown import plugin for InDesign](http://www.jongware.com/markdownid.html). Des dingues ont même réussi à trouver un moyen de construire un pont Markdown entre Word et InDesign : [From Word to Markdown to InDesign: Fully automated typesetting](http://rhythmus.be/md2indd/).

[^7]: "MultiMarkdown est une surcouche basée sur la syntaxe Markdown, créé par John Gruber. Il ajoute de nouvelles fonctionnalités syntaxiques (tableaux, notes de bas de page et citation, par exemple), en plus des nombreux formats de sortie listée précédemment (Markdown ne crée que du HTML). De plus, il adapte les règles typographiques en fonction de la langue (citations alignées à gauche ou à droite, par exemple). [MultiMarkdown by Fletcher Penney](http://fletcherpenney.net/multimarkdown/)

[^8]: Lorsque nous prenons des notes nous avons des éclairs du prochain titre, avant même de penser à ce que nous voulons dire. Nous repensons inévitablement ce que nous voulons dire tout en écrivant. Nous devons souvent faire des recherches supplémentaires lorsque nous éditons un texte, et parfois même lorsque nous écrivons. La mise en page peut révéler des erreurs invisibles dans le texte. Les limitations du medium vont nous obliger à couper, réécrire ou ajouter du texte pour adapter la forme : ce qui a un effet salutaire sur le style d'écriture, car il nous oblige à réfléchir sur ce que nous voulions vraiment dire.

[^9]: L'une des nombreuses erreurs que nous avons faites lors du développement de notre éditeur Markdown fut de nous éprendre de l'idée qu'un processus d'écriture pouvait ou devait être séparé en différentes étapes, et que ces étapes devaient être directement implémentées dans l'extension du fichier. Cette méthode fonctionne pour des auteurs très disciplinés mais les retours que nous avons reçus à propos de cette configuration ont clairement montré que nous nous étions trompés. Si iA Writer 3 est rétrocompatible avec ce système, la plupart de nos auteurs ainsi que nous-mêmes sommes contents de l'avoir abandonné.

[^10]: Note des traducteurs : le sous-titre anglais, *Copy Pastasciutta*, est un jeu de mot intraduisible en français. Il fait référence au “code spaghetti”, un code peu clair et qui fait un usage excessif de sauts inconditionnels (voir goto), d'exceptions en tous sens, de gestion des événements complexes et de *threads* divers. (source : [Wikipédia, Programmation spaghetti](https://fr.wikipedia.org/wiki/Programmation_spaghetti))

[^11]: Note des traducteurs : l’impasse mexicaine est une scène clé du western spaghetti. Elle désigne une situation défavorable où les acteurs ont tout intérêt à maintenir le statu quo plutôt que de tenter un mouvement qui risquerait d'aggraver la situation. Ici, tenter un copier-coller sans style ou appliquer un formatage par dessus celui existant.

*Ce texte est une traduction de l'article de [iA](https://ia.net/), [Multichannel Text Processing](https://ia.net/know-how/multichannel-text-processing). ©2016 iA Inc. pour le texte original, Août 2016 CC BY-NC-SA pour la traduction.*

Une remarque sur la traduction ?

<a href="mailto:antoine@quaternum.net" class="link-button">Écrivez-moi</a>