---
layout: post
title: "Un point sur Jekyll"
date: 2014-12-29
comments: true
categories: 
- carnet
---
Après avoir [quitté Wordpress](/2012/12/23/pourquoi-quitter-wordpress/) et testé Octopress pendant plus de deux ans — pour ce site et pour d'autres carnets — je passe à Jekyll, l'occasion de faire le point sur ce *générateur de site statique*.

<!-- more -->

## Petite définition de Jekyll
>Simple, blog-aware, static sites.  
>[jekyllrb.com](http://jekyllrb.com/)  

Jekyll est un générateur de site statique qui permet de se passer d'un système reposant sur une base de données : à partir de fichiers écrits en Markdown, Textile ou autres formats de structuration *simple*, on obtient un ensemble de fichiers HTML, CSS — et éventuellement JavaScript. La *production* du site est du côté de la machine de l'utilisateur-rédacteur, et non du côté du serveur web comme avec des CMS classiques comme Wordpress, Drupal, Dotclear ou PluXml.  
Pas besoin d'un serveur disposant de PHP et d'une base de données MySQL, en revanche dans le cas de Jekyll et pour la partie production, cela nécessite l'utilisation de Ruby, et est plutôt destiné à celles et ceux qui utilisent une machine sous Linux ou Mac OS X. La rédaction ne nécessite par contre qu'un éditeur de texte, on peut donc écrire sur n'importe quelle machine, et c'est là l'avantage de séparer la gestion du contenu de la production du site.  
Voir [par ici](/2014/04/26/latex-et-jekyll/) pour une définition plus complète.


## Pourquoi un générateur de site statique&nbsp;?
Les avantages sont nombreux :

- pouvoir [écrire en Markdown](/2012/12/04/markdown-pour-simplifier-et-maitriser/), sans être nécessairement connecté ;
- obtenir un site **très** léger — moins de 5Mo pour ce carnet même s'il ne comporte qu'une soixantaine d'articles, et moins de 9Mo pour l'ensemble du système, site compris ;
- qui dit site léger dit site très réactif ;
- ne pas avoir à gérer un serveur web avec des briques comme PHP et des bases de données, donc hébergeable à peu près partout ;
- être en mesure de pouvoir migrer très facilement puisque le contenu est dissocié du système, et que les articles et pages sont écrits en Markdown — très facilement transformable en ce que l'on veut, dont du HTML ;
- etc.

Utiliser un tel *workflow* n'est pas sans contrainte, la preuve en est que j'ai dû passer d'[Octopress](http://octopress.org/) — framework basé sur Jekyll — à Jekyll suite à un changement d'ordinateur, n'ayant pas réussi à paramétrer la bonne version de Ruby pour Octopress sur la nouvelle machine... Jekyll est désormais beaucoup plus simple à installer sur Linux et Mac OS X — quelques minutes pour un newbie comme moi, si si. Il faut aussi mentionner l'absence de gestion de commentaires — puisque tout est en statique — mais on peut utiliser un service tiers pour cela, tel que Diqus par exemple, personnellement je préfère l'idée [d'inciter les éventuels commentateurs à réagir en publiant à leur tour](http://www.la-grange.net/2013/01/03/commentaires).

## Attribuer un style différent pour chaque billet
C'était [l'une de mes préoccupations](https://twitter.com/antoinentl/status/294586922469298176) au début de mon utilisation d'Octopress, pouvoir utiliser un style ou thème différent pour chaque article ou page, notamment influencée par [le carnet de Karl](http://www.la-grange.net) qui évolue au fil des années sans perdre les designs successifs. L'une des applications du concept de [web qui rouille]() en quelque sorte.  
Avec Jekyll c'est relativement simple à faire, voici ci-dessous l'architecture des contenus de Jekyll.  

![Architecture de Jekyll](/../../../images/carnet-jekyll.jpg)  

1. À chaque type de contenu — article ou page — est attribué un *layout*, un modèle de structuration et de style — *post* ou *page* — qui ne concerne que le contenu du type d'item, et non les éventuels éléments de navigation comme le nom du site, le menu, le pied de page, etc.  
2. Les *modèles* d'article ou de page appellent un modèle plus général qui est celui de la page web qui s'affiche, dans Jekyll c'est *default*, ce modèle comprend donc les éléments de navigation cités plus haut.  
3. Ce modèle général *default* appelle lui-même un *header* qui comporte notamment les métadonnées comme le titre du site et la feuille de style.
4. La feuille de style concerne tout le site, attention avec certains thèmes il peut y avoir du [Sass](http://fr.wikipedia.org/wiki/Sass_%28langage%29), ce qui complique un peu le fonctionnement.

Il suffit de créer, pour chaque article, les modèles *post*, *default* et *header*, ainsi que la feuille de style qui correspond : on indique dans le fichier Markdown de l'article le *layout* utilisé, ce dernier appelle le *modèle* général, celui-ci appelle un *header*, et enfin le *header* appelle la feuille de style.  
On peut donc obtenir un site qui aura des articles ou des pages aux designs différents, assez facilement et sans créer de nouvelles règles dans Jekyll. Reste le cas de la page d'accueil puis des pages d'index, mais il s'agit cette fois plutôt d'une question de choix et de design général que d'une question technique. Enfin, ce *workflow* repose entièrement sur Jekyll, mais puisque l'on obtient des fichiers statiques on pourrait imaginer changer de système selon certaines parties du site, mais c'est une autre histoire.