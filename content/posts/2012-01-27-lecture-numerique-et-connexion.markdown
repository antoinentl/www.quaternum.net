---
comments: true
date: 2012-01-27
layout: post
slug: lecture-numerique-et-connexion
title: Lecture numerique et connexion
categories:
- carnet
---
Parmi l'une des formes du livre numérique, le streaming semble prendre une place particulière voire privilégiée, comme une solution à un certain nombre de contraintes. Il s'agit donc de lecture en ligne, souvent par le biais d'une "liseuse", avec toutes les possibilités qu'offre notamment le HTML5. Il ne s'agit pas d'un accès à un fichier, avec téléchargement de l'objet numérique dans tel ou tel format, avec ou sans DRM, avec ou sans chronodégrabilité, mais bien de lecture en ligne. Un accès à un contenu à un moment (certains diront une forme de DRM).

>La constitution pour chacun de sa bibliothèque numérique n’est pas neutre.  
[François Bon, Pour une définition du livre numérique](http://www.tierslivre.net/spip/spip.php?article2765)

L'accès en streaming règle beaucoup de problèmes : la compatibilité entre formats de fichiers et machines, le mode d'accès collectif notamment pour les bibliothèques, le choix de l'usage des DRM. C'est un bon compromis pour qui est équipé d'une tablette ou de tout autre outil permettant d'accéder à un site Web comprenant pas mal de Javascript. Mais aussi et surtout pour qui dispose d'une connexion continue avec son "dispositif de lecture". Avec cette consèquence que devrait résoudre en partie le HTML5 : pas de connexion pas d'accès aux textes. Il ne faut pas oublier que certaines plateformes mettent en place une "liseuse" en ligne qui affiche... des images, cela induit une consommation gourmande.  
Peut-on se constituer une bibliothèque, sa propre bibliothèque avec ce type d'accès ? Que se passe-t-il lorsque l'accès ou l'abonnement à une telle offre se termine ?