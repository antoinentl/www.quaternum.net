---
comments: true
date: 2013-02-18
layout: post
slug: mouvement-connexion-et-synchronisation
title: Mouvement, connexion et synchronisation
categories:
- carnet
---
>La quasynchronicité permet de se focaliser sur la future connectivité tout en l'oubliant parfois l'espace de cet instant. Futile mais indispensable.  
[David Larlet, Quasynchronicité](https://larlet.fr/david/blog/2013/quasynchronicite/)

Pour celles et ceux qui prennent le train régulièrement et qui ont besoin/envie d'être connectés, l'exercice est délicat à gérer, autant avec nos machines qu'avec nos habitudes.  
**Qu'est-ce que ces mouvements nous apportent dans nos exercices de connexion&nbsp;?**


## Une gestion différente de la connexion

Ne plus considérer la connexion comme une évidence et comme une nécessité mais comme quelque chose qui _se fera_ ou qui _devra_ se faire, à un moment ou à un autre, pendant le voyage mais peut-être aussi à l'arrivée. La connexion n'est pas une fin en soi, tout comme l'accès à une ressource web ou l'envoi d'un message.


## Accepter la déconnexion

L'accepter et même la choisir, pour ne pas que la recherche de réseau - et l'attente - ne devienne une quête sans fin. Il faut alors organiser cette phase&nbsp;: par exemple par l'ouverture d'un certain nombre d'onglets dans le navigateur, la synchronisation de fichiers/dossiers, ou le fait de couper les applications gourmandes en connexion...  
La synchronisation n'a besoin que de connexions intermittentes, et non d'une connexion permanente, il faut aussi accepter cela. Ce sera de plus en plus difficile à appréhender à l'heure notamment des tablettes ou du streaming.


## Vivre la connexion plus intensèment

Apprendre à travailler/vivre hors connexion c'est réserver les périodes de connexion à ce qui nécessite _réellement_ une connexion. On ne s'en rend plus compte mais beaucoup de choses peuvent être faites sans connexion sans que cela n'ait d'incidence particulière sur notre vie et/ou notre travail. Les périodes de connexion sont alors des moments d'intense connectivité&nbsp;: les recherches, consultations... y sont rassemblées.


## Mieux gérer la connexion après

Appréhender les connexions intermittentes permet aussi de mieux gérer la connexion en "mode normal"&nbsp;: faire l'économie des chargements inutiles sur le Web, réduire la réception ou l'envoi de données, la fréquence des synchronisation.

_Billet écrit, généré et mis en ligne depuis un train._