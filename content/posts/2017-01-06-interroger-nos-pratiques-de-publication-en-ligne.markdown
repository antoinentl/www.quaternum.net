---
layout: post
title: "Interroger nos pratiques de publication en ligne"
date: "2017-01-06T13:00:00"
comments: true
published: true
description: "Lorsqu'une plateforme de blog montre des signes de faiblesse, c'est le bon moment pour s'interroger sur nos pratiques de publication et de présence sur le web pour ne pas dépendre des services tiers et des silos&nbsp;: choisir ses outils, et suivre la formule magique POSSE, pour Publish (on your) Own Site, Syndicate Elsewhere, proposée par l'IndieWeb. En d'autres termes&nbsp;: réveillez les blogs&nbsp;!"
categories: 
- carnet
---
Lorsqu'une plateforme de blog montre des signes de faiblesse, c'est le bon moment pour s'interroger sur nos pratiques de publication et de présence sur le web, pour ne pas dépendre des services tiers et des silos&nbsp;: choisir ses outils, et suivre la formule magique POSSE, pour Publish (on your) Own Site, Syndicate Elsewhere, proposée par l'IndieWeb. En d'autres termes&nbsp;: réveillez les blogs&nbsp;!

<!-- more -->

## Publier sur le web
L'histoire du web est une histoire de publication&nbsp;: protocoles, standards, outils et services. [L'annonce récente](https://blog.medium.com/renewing-mediums-focus-98f374a960be#.bm8s0yt8p) de la *restructuration* de [Medium](https://medium.com), la dernière plateforme de blog à la mode depuis quelques années, laisse imaginer une fermeture, voir la perte de beaucoup beaucoup de billets et articles. Ce ne sera ni la première ni la dernière fois que le web connaîtra ce type d'épisode, c'est le jeu des services en ligne -- qu'il faut connaître sinon accepter *avant* d'aller y mettre tout ses contenus. C'est une question d'identité numérique et de [pérénnité](https://clochix.net/marges/161218-perennite). Mais alors, que faire&nbsp;? Partir vers une autre plateforme en espérant qu'elle tienne plus longtemps&nbsp;? Auto-héberger ses contenus, et donc devoir être à la fois administrateur système, développeur back-end et développeur front-end&nbsp;? D'autres solutions alternatives existent&nbsp;!

## Silos, services tiers et alternatives
Medium est un service tiers -- c'est-à-dire que lorsque vous créez un compte sur Medium vous acceptez son fonctionnement et ses règles. Mais c'est aussi un silo, le but est de *rester* sur Medium&nbsp;: si vous aviez un blog ou carnet ailleurs, vous allez vite avoir envie de rejoindre Medium, notamment pour les fonctionnalités qui y sont proposées, ou parce que votre/vos communautés y sont et que c'est donc plus simple pour interagir avec elles.

Vous souhaitez publier un article sur le web&nbsp;? Voici quelques solutions sélectionnées subjectivement&nbsp;:

### Services tiers, le tout en un
[Tumblr](https://www.tumblr.com/), [Medium](https://medium.com/), [Wordpress](https://fr.wordpress.com/), etc. Les services en ligne sont nombreux. Parfois *avec* publicité incluse -- comme Wordpress dans sa version service gratuit --, parfois sans possibilité de modifier le graphisme -- comme sur Medium&nbsp;: toutes ces plateformes ont leurs particularités. Payer quelques dizaines d'euros par an auprès de Wordpress peut être une solution pour avoir un bel outil de publication en ligne, sans publicité, avec certaines libertés et sans se soucier des contraintes techniques.

Depuis quelques semaines [Telegram](https://telegram.org/) propose un service très intéressant&nbsp;: [http://telegra.ph/](http://telegra.ph/). Vous pouvez publier anonymement et gratuitement un article, sans avoir besoin de créer un compte.

### GitHub Pages ou GitLab Pages, des solutions pas si complexes
[GitLab](https://gitlab.com/) et [GitHub](https://github.com/) proposent d'héberger des sites web via [des générateurs de site statique](https://www.quaternum.net/2016/01/09/generateur-de-site-statique-un-modele-alternatif/), gratuitement. Pour faire simple&nbsp;: vous écrivez vos pages, articles, billets sous la forme de fichiers Markdown, vous *envoyez* ces contenus sur ces plateformes de code via le logiciel de *versioning* Git, et GitLab et GitHub mettent à jour automatiquement votre site web, blog ou carnet. Pour mieux comprendre lisez la page d'explication de GitLab&nbsp;: [https://pages.gitlab.io/](https://pages.gitlab.io/).

C'est une solution qui peut paraître très technique, elle ne l'est pas tant que cela. Et apprendre à utiliser Git peut même être très pertinent si vous avez un lien avec les sciences de l'information, le web, ou si vous êtes un peu curieux.

Mais alors GitLab ou GitHub ne sont-ils pas eux-mêmes des services tiers ou des silos&nbsp;? Ce sont des services tiers qui hébergent du code, mais vous conservez localement, sur votre machine, vos contenus -- vos pages, vos articles, vos choix de mise en page, etc. -- que vous pourrez facilement héberger ailleurs si besoin. Et ce ne sont pas totalement des silos, puisque votre site ou blog peut être à *votre* adresse -- nom de domaine -- et donc faire partie d'une galaxie d'autres initiatives indépendantes et *relativement* libres -- GitLab et GitHub étant des hébergeurs, il y a des lois et des contraintes à respecter.

### Héberger son carnet, relever les manches
Pour les plus motivés, il est possible de prendre un espace ou un serveur chez un hébergeur, ou se lancer dans l'auto-hébergement&nbsp;: associer son nom de domaine, et y installer son outil de publication. De Wordpress à [Jekyll](http://jekyllrb.com/), en passant par [Dotclear](https://fr.dotclear.org/), [Ghost](https://ghost.org/), [Kirby](https://getkirby.com/), ou des solutions faites à la main, tout est possible&nbsp;! Par contre il faudra gérer beaucoup de choses&nbsp;: l'hébergement, l'outil de publication -- la partie *back* --, et le site web -- la partie *front*.

Faire de l'auto-hébergement est également possible sans compétences techniques, par exemple chez [Gandi](https://www.gandi.net/) qui propose gratuitement [un blog sous Dotclear avec chaque nom de domaine](https://www.gandi.net/domaine/blog) (merci à Clochix de m'avoir soufflé cette possibilité).

Avant de choisir un outil, il faut s'assurer qu'il permet *facilement* de récupérer les contenus si une migration vers un autre outil, hébergement, service, était envisagée.


## POSSE et stratégie

### Publish (on your) Own Site, Syndicate Elsewhere
"Publie sur ton propre site web, et diffuse le contenu ailleurs". La philosophie [du concept POSSE proposé par l'IndieWeb](https://indieweb.org/POSSE) -- [version française](https://indieweb.org/POSSE-fr) -- est donc de garder le contrôle sur nos publications en ligne et nos identités numériques, tout en distribuant les contenus sur d'autres espaces, que ce soit des sites web, des services tiers, des silos. Il s'agit donc de mettre en place un workflow dont le point de départ est un site/blog hébergé, pour ensuite distribuer et diffuser sur des réseaux sociaux, sur une plateforme de blog comme Medium -- pour profiter des options de partage et d'interaction --, ou sur d'autres services de diffusion -- lettres d'information, impression, etc.

### Stratégie globale
Il s'agit donc bien d'une stratégie globale&nbsp;: individuelle ou collective, professionnelle ou personnelle, d'entreprise, ou promotionnelle, etc. En terme de chaîne de publication, tout est à imaginer, et certains outils permettent par ailleurs d'automatiser la diffusion de contenus depuis un site web -- et un flux de syndication RSS ou Atom -- vers d'autres plateformes. Dans le cas d'une monétisation de contenus -- ce n'est pas vraiment l'objet de ce billet -- [vous pouvez aller découvrir des stratégies et des outils associés](/2016/11/04/blend-web-mix-2016-jour-2/#et-si-les-sites-web-taient-sur-le-point-de-disparatrenbsp-virginie-clve).

Il faut éviter une vision binaire&nbsp;: publier uniquement sur son blog auto-hébergé ou n'utiliser que des services tiers. Il y a différentes approches, différentes stratégies, et différents niveaux de diffusion.

La question du référencement peut être posée&nbsp;:

- il faut éviter la [duplication de contenu](https://fr.wikipedia.org/wiki/Contenu_dupliqu%C3%A9#Cons.C3.A9quences_des_contenus_en_double) -- ou *duplicate content* -- qui peut générer un déréférencement s'il est mal géré&nbsp;;
- à l'inverse, si vous ne souhaitez pas être référencé par les moteurs de recherche, il vaut mieux éviter de diffuser du contenu ailleurs que sur son propre site web -- sur lequel vous pouvez maîtriser cette question du référencement, notamment via [robots.txt](http://robots-txt.com/).


## Un carnet, des carnets, un réseau
Quelques ressources pour clôre ce billet&nbsp;:

- [POSSE](https://indieweb.org/POSSE) sur IndieWeb ([version française](https://indieweb.org/POSSE-fr))&nbsp;;
- [Jailbreaking the Internet](https://willnorris.com/2013/08/jailbreaking-the-internet) par Will Norris, à propos du concept POSSE ([version française](https://t37.net/jailbreaker-internet.html))&nbsp;;
- [Le fanzinat des gens du web](/2016/06/02/le-fanzinat-des-gens-du-web/) sur quaternum.net, à propos de blogs, de carnets et des enjeux qui y sont liés&nbsp;;
- [Ces liens qui nous (ré)unissent](https://nrkn.fr/blog/2016/11/20/ces-liens-qui-nous-unissent/) par Pep, à propos des réseaux de carnets web et de comment les lier hors silos&nbsp;;
- [Pérénnité](https://clochix.net/marges/161218-perennite) par Clochix, à propos du contrôle de nos vies numériques&nbsp;;
- [Facebook et les gens biens](http://nota-bene.org/Facebook-et-les-gens-biens) par Stéphane, à propos des silos&nbsp;;
- [Un Web sous surveillance, conf au Capitole du Libre 2016](https://www.miximum.fr/blog/conf-cdl-2016/) par Thibault, à propos de l'état du web d'aujourd'hui&nbsp;;
- [Militantisme Festif](https://larlet.fr/david/blog/2016/militantisme-festif/) par David, à propos de pistes concrètes pour un *autre* web que celui des silos.


## Discussions (ajouts du 15 janvier 2017)
Quelques compléments en lien ou en parallèle de ce billet :

- [Espace personnel](http://nota-bene.org/Espace-personnel) par Stéphane, qui se demande comment simplifier les processus de prise d'indépendance numérique ;
- [L'auto-hébergement, une préoccupation de barbu ?](http://marges.clairezuliani.com/binary_transition/2017/01/14/interet-auto-hebergement-ess-systeme-ferme.html) par Claire, qui résume une discussion engagée sur Twitter par Clochix, sur la question de l'auto-hébergement de contenus ;
- [Medium’s pivot and the inherent instability of new businesses](Medium’s pivot and the inherent instability of new businesses) par Jason Kottke, via [Karl](http://la-grange.net/).