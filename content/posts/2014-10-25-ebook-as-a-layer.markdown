---
layout: post
title: "Ebook as a layer"
date: "2014-09-30T08:25:00"
comments: true
categories: 
- carnet
---
Le livre numérique — au sens d'un fichier numérique structuré représentant le contenu d'un livre avec toutes ses métadonnées — n'est peut-être qu'un *calque*. L'*ebook* est actuellement utilisé seul et pour lui-même, mais peut-être devons-nous imaginer plus d'usages qui font du livre numérique une dimension supplémentaire et complémentaire du livre papier.

<!-- more -->

>Dans tous les cas, que le canal de communication soit perçu comme un tracé ou comme un ensemble de repères dépend de la manière dont nous le percevons&nbsp;: comme communication pour "aller de A à B", ou comme canal pour se repérer sur une surface.  
[Tim Ingold, Une brève histoire des lignes](http://www.zones-sensibles.org/livres/tim-ingold-une-breve-histoire-des-lignes/)


## UmiX expérience
J'ai participé à [UmiX](http://umix.fr/), un hackathon organisé par le master Architecture de l'information (ENS Lyon, Université Lyon 1, enssib) les 25, 26 et 27 septembre.  
J'ai eu la chance de rencontrer beaucoup de personnes très motivées et passionnantes, et de travailler avec Emeline, Areg et Konstantin. Pendant deux jours nous avons esquissé puis dessiné un concept d'annotation de livre papier avec une interface *numérique* dans une bibliothèque, nous l'avons appelé [annothèque](http://www.annotheque.fr)&nbsp;:

![Concept](/images/ebook-as-a-layer.png)

>De nombreux dispositifs d’annotation existent et se développent dans des écosystèmes numériques&nbsp;: applications sur smartphones et tablettes, services en ligne, etc. Mais aujourd’hui, l’expérience d’annotation numérique connectée au livre papier n’existe pas. La bibliothèque, lieu d’accès au savoir, espace de lecture mais aussi de travail, peut offrir une expérience d’annotation qui mêle un livre papier et des outils numériques. Proposer une expérience d’annotation aussi simple que des notes aux crayons sur un livre papier et aussi intuitive qu’écrire avec des outils numériques, voilà le projet de l’annothèque&nbsp;!
  
Cette expérience fut enrichissante pour bien des raisons, la première étant de conceptualiser une idée en quelques heures avec un stylo et du papier, en tentant de ne pas se focaliser sur la faisabilité technique du projet.


## L'expérience de lecture
La lecture est une pratique active, plus encore lorsqu'il s'agit de *non fiction* — essais, documents, formes longues du journalisme, etc. Les représentations sociales et culturelles de la lecture limitent parfois cette pratique à un simple exercice solitaire fait d'allers retours entre l'œil et le papier. Pourtant toute lecture est *active*, le fait de relire un passage ou de corner une page en sont des manifestations. Surligner des passages ou prendre des notes — sur ou hors du livre — sont des actes plus visibles.


## Nous nous sommes trompés sur le livre numérique
La majorité des efforts de promotion du livre numérique se concentre sur l'expérience de lecture numérique, d'autant plus en France avec l'impossibilité actuelle de vendre des *bundles* livre papier + livre numérique. L'industrie du livre s'efforce de conserver une dichotomie entre le papier et le numérique, cela se voit jusque dans les organisations des grands éditeurs, groupes d'édition et distributeurs&nbsp;: les départements numériques sont distincts des départements *classiques*, les workflows de production commencent tout juste à être en capacité à produire des fichiers pour l'impression et le numérique.  
Le fichier EPUB peut être utilisé non pas uniquement comme un support de lecture numérique, mais il peut aussi ajouter des fonctionnalités au livre papier, comme une surface supplémentaire. À partir de la reconnaissance d'un passage d'un livre papier, imaginons des fonctionnalités issues de la structuration d'un contenu&nbsp;:

+ repérage dans un texte avec des informations sur la localisation et le *niveau*&nbsp;: chapitre, sous-chapitre, paragraphe, etc&nbsp;;
+ copie d'un passage&nbsp;;
+ inscription de notes *dans* le fichier EPUB&nbsp;;
+ outils de visualisation de la lecture à partir de l'extraction des informations concernant l'activité de lecture&nbsp;;
+ etc.

Les usages complémentaires entre le livre papier et le numérique — l'EPUB et le web — sont encore timides, il y a encore beaucoup de choses à inventer.