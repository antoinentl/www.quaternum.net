---
comments: true
date: 2012-06-09
layout: post
slug: des-outils-pour-prendre-des-notes
title: Des outils pour prendre des notes
categories:
- carnet
---

Suite à [quelques billets](https://www.quaternum.net/2012/05/09/quel-moteur-pour-ce-carnet/) sur le choix d'un outil pour publier sur ce carnet, je me demande comment prendre des notes tout en respectant quelques paramètres.

PluXml me servira bientôt à publier des billets sur [mon carnet Web](https://www.quaternum.net/)&nbsp;: pour rédiger ces articles je compte passer par un éditeur de texte (actuellement Sublime Text 2), pour ensuite les envoyer via FTP sur PluXml (en mode brouillon ou en les publiant directement) - c'est pourquoi [la découverte](https://www.quaternum.net/2012/05/17/pluxml-un-moteur-presque-en-dur) que PluXml génère un fichier XML pour chaque billet fut une grande nouvelle pour moi. La création d'un script/programme capable de transférer les fichiers est à prévoir pour faciliter la mise en ligne.

Mais pour prendre quelques notes, et notamment sur autre chose qu'un laptop, j'utilise Diigo à la fois comme outil de marque page mais aussi pour prendre des notes sur smartphone. D'un autre côté je me sers de l'application Android de Wordpress pour écrire certaines esquisses de billets (je dois avouer que l'ergonomie très réussie de cette application semble inégalée). Si je récapitule, j'ai besoin de prendre des notes :

+   depuis mon netbook sous Ubuntu ;
+   depuis un smartphone sous Android ;
+   et via une application Web, pour un accès depuis (presque) n'importe où.

La solution que j'envisageais était l'utilisation d'éditeurs de texte sur tous les supports, Sublime Text 2 ou Geany pour Ubuntu, et [Jota pour Android](http://sites.google.com/site/aquamarinepandora/home/jota-text-editor), mais cela me semble désormais trop fastidieux et trop complexe à gérer en terme d'usage et de synchronisation.

Voici une solution qui paraît plus réaliste : remplacement de Diigo par Shaarli et suppression de Wordpress pour Android. Voici la répartition des usages par support et interface&nbsp;:

+   netbook&nbsp;: si connexion utilisation de Shaarli, autrement passage par un éditeur de texte (l'idéal serait une application capable de synchroniser des fichiers .txt avec Shaarli, mais je ne rêve pas)&nbsp;;
+   smartphone&nbsp;: utilisation d'une application Shaarli permettant de prendre des notes (à la Diigo), si celle-ci est développée un jour ;
+   Web&nbsp;: passage directement par Shaarli (je pense d'ailleurs à installer une autre instance uniquement pour la prise de notes, autre que [celle-ci](http://liens.quaternum.net/)).

L'obstacle le plus important est donc le développement d'une application Android Shaarli pour la prise de notes, mais j'ai de bons espoirs. En attendant je vais encore me contenter de Diigo et me détacher peu à peu de l'application Wordpress.