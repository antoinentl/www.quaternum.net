---
comments: true
date: 2013-01-05
layout: post
slug: une-certaine-diversite-du-web
title: Une certaine diversité du Web
categories:
- carnet
---
La diversité des sites web dans leur forme et leur rendu est nécessaire, parce qu'il n'y a pas *un* Web.

## Quelques notes

Une liste rapide des éléments qui font la diversité des sites web, et qui concernent plutôt les carnets, les blogs ou les *petits* sites&nbsp;:

+   l'architecture des sites&nbsp;;
+   la construction des URL&nbsp;: par exemple des URL compréhensibles (mentions du titre, de la catégorie, de la date de publication...)&nbsp;;
+   la structuration de l'information&nbsp;: la hiérarchisation de l'information, les niveaux de titres, les métadonnées visibles...
+   l'ergonomie&nbsp;: comment naviguer de billets en pages, entre les rubriques et les catégories, comment rechercher dans le site, comment savoir où l'on est&nbsp;;
+   le graphisme&nbsp;: les choix typographiques, la "charte graphique", la lisibilité du texte et du rapport texte/image&nbsp;;
+   l'adaptabilité&nbsp;: comment le site va s'adapter aux différentes tailles d'écran, de la non-adaptation au responsive design en passant par la version mobile&nbsp;;
+   comment est pensée l'entrée dans le site&nbsp;;
+   ...

J'ai toujours été admiratif des sites faits à la main, ou des personnalisations très poussées de CMS connus, ce sont de telles initiatives qui permettent d'avoir une diversité de formes sur le Web.

## Wordpress&nbsp;: des dégâts collatéraux&nbsp;?

On ne peut pas dire que Wordpress contribue à la diversité du Web, tant dans sa structure que dans sa forme - mais plutôt dans sa forme. Peut-être que Wordpress permet une facilité de publication sur le Web au détriment d'une certaine diversité. Combien de fois je reconnais un carnet, blog ou site réalisé avec ce CMS&nbsp;?  
C'est un peu le même constat avec Octopress lorsqu'on visite [la liste des sites réalisés avec ce générateur](https://github.com/imathis/octopress/wiki/Octopress-Sites "Lien vers github"). L'ergonomie et les choix typographiques de ce carnet devraient bientôt évoluer.