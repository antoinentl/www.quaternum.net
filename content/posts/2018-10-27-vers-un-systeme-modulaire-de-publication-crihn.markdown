---
layout: post
title: "Vers un système modulaire de publication"
date: "2018-10-27T10:00:00"
comments: true
published: true
description: "Le colloque Repenser les humanités numériques - Thinking the Digital Humanities Anew du Centre de recherche interuniversitaire sur les humanités numériques a été l'occasion de présenter mon mémoire de recherche Vers un système modulaire de publication : éditer avec le numérique. Voici le texte et les ressources de cette présentation."
categories:
- carnet
aliases:
  - /crihn/
slug: vers-un-systeme-modulaire-de-publication-crihn
bibfile: data/crihn.json
---
Le colloque "Repenser les humanités numériques / Thinking the Digital Humanities Anew" du Centre de recherche interuniversitaire sur les humanités numériques a été l'occasion de présenter mon mémoire de recherche [Vers un système modulaire de publication : éditer avec le numérique](https://memoire.quaternum.net).
Voici le texte et les ressources de cette présentation.

<!-- more -->
## Sommaire de cet article

- [Vers un système modulaire de publication](#vers-un-système-modulaire-de-publication)
- [Support de présentation](#support-de-présentation)
- [Bibliographie](#bibliographie)

## Vers un système modulaire de publication
Voici le texte de ma communication, incluant quelques liens et des références bibliographiques.

### 0. Préambule
L'exercice auquel je vais m'atteler est singulier, pour moi.

Mon mémoire s'attache à proposer un système modulaire de publication comme pouvant se substituer à des chaînes linéaires, repositionnant ainsi l’humain au cœur des machines ou des programmes.
J'ai mené pendant un an et demi ce travail de recherche sous la direction d'Anthony Masure, maître de conférences en design à l’université Toulouse – Jean Jaurès, et de Marcello Vitali-Rosati, professeur au département des littératures de langue française de l’Université de Montréal et titulaire de la Chaire de recherche du Canada sur les écritures numériques.
Ce mémoire s'inscrit plus globalement dans un travail de recherche qui a pris différentes formes.

Mon mémoire, intitulée "Vers un système modulaire de publication : éditer avec le numérique", est disponible [en ligne](https://memoire.quaternum.net), je vous invite à aller le découvrir à la suite de ce colloque, sa lecture n'étant pas forcément linéaire vous pourrez trouver des parties qui vous intéressent peut-être plus que d'autres.
J'ai notamment intégré, dans le mémoire, les travaux préparatoires comme les commentaires de texte et les analyses de cas.

Ce mémoire a également une version imprimée, que vous pouvez découvrir avec cette photo :

![](/images/2018-crihn-memoire-print.png)

Par travail de recherche plus global, j'entends d'autres incarnations que le mémoire seul : des interventions et des communications, notamment lors du colloque ÉCOLAB en novembre 2017 avec Julie Blanc – il s'agissait d'une sorte d'introduction à mon travail et à celui de Julie –, ou à Montréal en avril 2018 lors du colloque ÉCRIDIL – avec une intervention sur un point particulier de mon mémoire –, ou encore l'intervention d'aujourd'hui qui vient clore ce travail, au moins sous sa forme actuelle.

![](/images/2018-crihn-intervention-ecolab.jpg)

![](/images/2018-crihn-intervention-ecridil.jpg)

![](/images/2018-crihn-quentin.jpg)

À travers cette présentation je vais m'atteler à vous présenter le travail de mon mémoire, et cela en quatre parties :

1. La première partie concerne les raisons de s'interroger sur les modes de publication, qu'est-ce que veut dire éditer "avec le numérique" ?
2. La seconde partie expose l'étude de plusieurs expérimentations de "système de publication", originaux et non conventionnels. Cela m'amène à poser le principe de _modularité_, principe central lié à l'interopérabilité et à la multiformité.
3. Dans un troisième temps je définis précisément la notion de système, afin de comprendre quel est le changement de paradigme lors du passage de la _chaîne_ au _système_.
4. Enfin je souhaite présenter la façon dont j'ai travaillé avec mes directeurs de mémoire, ainsi que ma démarche de pré-publication du mémoire. Cela me permet également d'évoquer les prolongements nécessaires à ce travail, comme la réalisation d'une quatrième partie, ou la dimension reproductible de cette approche.

### 1. Avec le numérique
Deux événements majeurs modifient profondément le livre dans ses modes de conception, de fabrication et de diffusion :

1. l'informatisation de l'édition et plus globalement des métiers du livre, avec l'usage quasi généralisé du traitement de texte – le plus souvent Microsoft Word – et du logiciel de publication assistée par ordinateur – majoritairement Adobe InDesign ;
2. l'arrivée du livre numérique, non pas en 1971 avec l'initiative de Mickael Hart, mais à partir de 2006-2007 avec le format EPUB, la commercialisation d'un catalogue et la disponibilité d'un dispositif de lecture numérique.

Dans les faits les différents acteurs de la chaîne du livre utilisent des logiciels qui posent un certain nombre de questions.
L'_ebook_ introduit par ailleurs la notion de flux à la fois dans la conception des objets numériques, dans la perception des textes et dans la circulation – marchande ou non marchande – de ceux-ci.

![](/images/2018-crihn-traitement-de-texte-rouge.png)
_Figure 1 : décompression d'un fichier .odt, comportant uniquement le mot "bonjour"_

La figure 1 ci-contre représente les fichiers qui composent un simple .odt, le format du traitement de texte LibreOffice Writer.
Le seul mot "bonjour" inscrit avec un traitement de texte nécessite un ensemble de fichiers – XML pour la plupart –, alors qu'il s'agit d'une simple inscription.

> En utilisant un traitement de texte, nous admettons un certain nombre de leurs prémisses.
>
> {{< cite "dehut_en_2018" >}}

Cette citation est extraite d'un article de Julien Dehut, "En finir avec Word ! Pour une analyse des enjeux relatifs aux traitements de texte et à leur utilisation".
Je ne mène pas une croisade contre les traitements de texte, je dois vous confesser que j'en utilise encore moi-même parfois – lorsqu'on m'y oblige.
Par ailleurs je vous invite à lire le formidable ouvrage de Matthew Kirschenbaum, _Track Changes_, si vous vous intéressez aux pratiques d'écriture sur les cinquante dernières années ce texte est incroyable.
Et de toute façon ce texte est incroyable.
Tout cela pour suggérer que nous gagnerions toutes et tous à faire appel à d'autres outils, ou plutôt à modifier nos modes d'édition actuellement conduits par ces boîtes noires.
Le traitement de texte n'est qu'un exemple.
Nous devons faire le constat d'une certaine forme d'uniformisation liée à des logiciels comme Microsoft Word (et son équivalent livre LibreOffice Writer) et Adobe InDesign.

Nous devons nous interroger sur notre utilisation du numérique pour concevoir et fabriquer des livres.
Encore une fois, le cas du traitement de texte est un exemple parmi d'autres.
Partant du constat d'un certain monopole dans ce domaine mais également dans celui de la publication assistée par ordinateur, nous devons distinguer l'utilisation du numérique comme simple solutionnisme technologique et penser des modes d'édition que nous pouvons bâtir, afin qu'ils soient plus inclusifs.
La double dimension d'apprentissage et transmission est également essentielle, j'y reviendrai.
Lorsque nous parlons de _concevoir_ et de _fabriquer_ des livres, il s'agit en fait des chaînes de publication : l'ensemble des méthodes et des outils ordonnés qui remplissent des objectifs déterminés pour un projet éditorial.
C'est précisément sur la question des _chaînes de publication_ que je me concentre ici.


### 2. Modularité
La _modularité_ est un principe, sinon un concept, central dans la perspective d'une intégration plus profonde du numérique dans les chaînes de publication.
Plusieurs structures d'édition ont mis en place des chaînes modulaires, et ont ainsi développé leur propre outil de fabrication, plutôt que de faire reposer leur processus d'édition uniquement sur des outils relativement fermés, opaques et peu pérennes comme les traitements de texte ou les logiciels de publication assistée par ordinateur.
O'Reilly, Getty Publications, Electric Book Works : parmi ces trois exemples, je vais m'attarder sur Getty Publications et leur outil Quire.
Pour en savoir plus du O'Reilly Media je vous invite à lire [la partie 2 de mon mémoire](https://memoire.quaternum.net/2-experimentations/2-1-o-reilly/).

![](/images/2018-crihn-getty-terracottas.jpg)
_Figure 2 : capture d'écran du livre web [Ancient Terracottas](https://www.getty.edu/publications/terracottas/)_

La figure 2 représente la page de couverture de la version web d'un des catalogues de Getty Publications.
Nous pouvons découvrir un site web relativement classique, avec tout de même quelques fonctionnalités comme des cartes localisant les œuvres, ou encore un zoom permettant de _rentrer_ dans les prises de vue – impossible avec une forme imprimée classique.
Autre point déterminant, les différentes formes du catalogue sont présentées : la version web, les formats de livre numérique (EPUB et Kindle), un fichier PDF de basse qualité imprimable sur une imprimante de bureau, le lien pour acheter une version imprimée, et enfin le téléchargement de jeux de données.
Il y a donc plusieurs niveaux d'accès au catalogue, pour diverses utilisations.
L'accès web est le plus ouvert, mais il est également possible de lire le catalogue avec un dispositif de lecture plus confortable, d'imprimer les pages jugées intéressantes ou encore de travailler avec les données (images, métadonnées).
En plus de réaliser une forme d'hybridation entre numérique et imprimé, comme l'explique Alessandro Ludovico sans _Post-Digital Print_, l'originalité de ces livres produits par Getty Publications réside dans la manière dont ils ont été conçus et fabriqués.

![](/images/2018-crihn-getty-equipe.jpg)
_Figure 3 : photographie de l'équipe du département numérique de Getty Publications_

Parce qu'il y a des humains derrière ces projets numériques, la figure 3 est une photo de l'audacieuse équipe qui a mis en place Quire, et qui travaille avec cette chaîne de publication.
Je vous présente Eric Gardner, Ruth Evans Lane et Greg Albers.
Ils ont mis en place une chaîne modulaire : un ensemble de programmes, basés sur des standards, tout cela en appliquant le principe de _single source publishing_, c'est-à-dire que la même source est utilisée pour la version web, la version EPUB, les versions imprimées, et les jeux de données.

Ce principe de _modularité_ est central car il dépend du principe d'_interopérabilité_ – qui permet une communication entre des humains et entre différents programmes informatiques – et il engendre le principe de _multiformité_ – qui correspond aux possibilités de production de formes très diverses (EPUB, web, PDF, etc., comme nous avons pu le voir avec Getty Publications).
Pour vous figurer ce concept, vous pouvez vous imaginer un ensemble de briques, comme des Lego, chacune d'entre elles devant répondre à une ou plusieurs fonctions.
Une chaîne de publication correspond ainsi à l'agencement de composants qui ont la capacité de communiquer entre eux grâce au principe d'interopérabilité – par exemple via l'utilisation de standards – et produisent des formes diverses.
L'ajout d'un composant peut par exemple permettre de générer une nouvelle forme.

> [L’homme] est parmi les machines qui opèrent avec lui.
>
> {{< cite "simondon_du_2012" "13" >}}

La question de la modularité permet de repositionner l'homme par rapport aux programmes ou aux applications.
Nous invoquons ici Gilbert Simondon, philosophe de la technique.
Sa thèse de 1958, _Du mode d'existence des objets techniques_, peut être réinterprétée à l'horizon d'un environnement profondément numérique.
Car, aujourd'hui encore, notre civilisation est "mal technicienne" pour reprendre les mots de Gilbert Simondon lors d'un entretien avec Jean Le Moyne et disponible en ligne.
Une chaîne de publication peut être considérée comme un objet technique contemporain, et, en l'interrogeant, nous pouvons replacer l'humain au cœur de processus relativement désincarnés dans le cas de logiciels fonctionnant comme des boîtes noires.
Une chaîne de publication modulaire représenterait une forme de progrès technique, comme une opportunité de mieux nous positionner par rapport à la technique, et de ne plus seulement appuyer sur des boutons ou activer des fonctions, mais intégrer les outils, voir les construire.

Cette notion de modularité nous amène presque naturellement au concept de _système_.


### 3. Système
Notre sujet ici est la chaîne de publication.
En abandonnant la notion de _chaîne_, nous abandonnons la dimension de linéarité : les étapes de la publication ne se suivent plus d'une manière irréversible dans les exemples que je vous ai présentés.
La notion de système recouvre une dimension de synergie : synergie des composants qui interagissent de façon dynamique.
Même si Gilbert Simondon n'utilise pas ce concept nous sommes en plein dans ce qu'il décrit comme un objet technique _concret_, mais je vous laisserai découvrir la pensée complexe mais passionnante de Gilbert Simondon.

> Là où [la chaîne] a presque toujours une forme et des frontières fixes, la cohérence d'un système peut être altérée dans le temps et dans l'espace, et son comportement déterminé à la fois par des conditions externes et par ses mécanismes de contrôle.
>
> {{< cite "burnham_esthetique_2015" "63" >}}

Je fais appel au texte de Jack Burnham, et je le détourne quelque peu pour expliquer quel changement de paradigme implique l'abandon du terme _chaîne_ pour celui de _système_.

Voilà ce que recouvre le concept de système :

- des étapes réversibles : pouvoir revenir en arrière sans générer des difficultés, comme intervenir sur un fichier que seul un designer graphique peut ouvrir et modifier ;
- une synergie des composants : chaque module est lié à d'autres modules, et pas uniquement au précédent et au suivant ;
- un ensemble modulaire : il y a une organisation complexe faite de dépendance et d'échanges d'information.

Ce système nécessite d'être incarné, pour reprendre une formulation de Marcello Vitali-Rosati lors de la soutenance de mon mémoire, mais aussi d'aborder une posture d'apprentissage.

### 4. Apprentissage
Pour la rédaction du mémoire j'ai appliqué les principes exposés dans mon texte.
Il est intéressant de noter comment j'ai travaillé avec mes directeurs de mémoire, et de s'interroger sur l'utilité de ces dispositions.
S'agissait-il d'_esbroufe_ ?
Est-ce que j'ai moi-même contribué à une certaine forme de solutionnisme technologique ?

Pour écrire et structurer mon texte je me suis donc passé de traitement de texte et j'ai utilisé un langage de balisage léger, Markdown.

Pour partager, collaborer d'une certaine façon, et valider mon texte, j'ai utilisé le système de versionnement Git.

Pour mettre en forme je me suis appuyé sur la structure sémantique réalisée avec Markdown et j'ai appliqué des feuilles de style CSS.

Pour générer les formes et les formats, puis rendre public ce mémoire, j'ai utilisé les technologies du Web, et plus particulièrement un générateur de site statique qui transforme des fichiers Markdown en HTML et les organise.
Ainsi que paged.js pour générer un fichier PDF paginé à partir des fichiers HTML.

J'ai tenté de mettre en place des moyens de communication avec mes deux directeurs de mémoire, notamment via des commentaires sur une plateforme me permettant également de versionner mon mémoire.
Cette expérimentation a été riche en apports, même si elle n'a pas été un succès total.
Les limites de ces nouvelles méthodes sont du même ordre que les nouvelles dépendances que j'évoque dans la conclusion de mon travail : les contraintes ne sont pas supprimées, elles sont quelque peu déplacées, permettant aux humains qui interagissent avec des programmes informatiques de ne plus être asservis, désarmés ou aveuglés.

![](/images/2018-crihn-memoire-issue.png)
_Figure 4 : capture d'écran d'un ticket sur la plateforme GitLab_

Voici le premier mode de discussion avec mes directeurs.
Il s'agit d'une _issue_, ou ticket en français, dans l'environnement de GitLab, une plateforme collaborative de versionnement de code.
Mes deux directeurs de mémoire me font ici plusieurs retours sur une partie du mémoire.

![](/images/2018-crihn-memoire-merge.png)
_Figure 5 : capture d'écran d'une discussion dans une merge request_

La Figure 5 représente cette fois une discussion _dans_ le texte.
Il est ainsi possible d'annoter chaque ligne, et d'entamer une conversation.

En amont j'ai produit des formes variées de pré-publication.
Il s'agit d'une pratique de plus en plus répandue : donner à voir un travail encore en brouillon, alors qu'il n'y a pas de validation ou d'édition – et c'est peut-être là une différence essentielle entre _publication_ et _édition_.
Il n'est pas pertinent d'appliquer ce principe à tous les écrits académiques, mais il est intéressant, surtout dans le champ des humanités numériques, d'interroger la façon dont le numérique modifie aussi nos pratiques d'écriture et de publication.

![](/images/2018-crihn-brouillon.png)
_Figure 6 : capture d'écran de la plateforme GitLab_

Les pré-publications, versions print ou articles en cours de publication, autant de textes achevés par les auteurs, qui méritent d'exister à défaut d'être édités.
La figure 6 représente les différences entre deux versions d'un article bientôt publié dans la revue _Sciences du design_.

En aval j'ai produit différentes formes du mémoire pour la lecture par le jury, mais aussi pour toute personne intéressée.
Il serait trop long de détailler les différents composants à l'œuvre pour rédiger et publier ce mémoire, je ne souhaite pas faire une présentation de l'architecture technique du mémoire.
J'ai donc mis en place un système capable de produire, à partir de la même source, un site web et un fichier PDF imprimable.
La version EPUB est prévue, ainsi que la transposition du site en progressive web application – une application avec les technologies du Web, lisible hors connexion et installable.

La dimension modulaire d'un système de publication tel que je l'ai décrit ici offre la possibilité d'une reprise d'un ou plusieurs éléments, et pas forcément de la totalité des briques.
Je me suis moi-même largement inspiré de plusieurs initiatives, reprenant les éléments dont j'avais besoin.
Par exemple paged.js est le composant permettant de générer un fichier PDF d'impression, développé par Paged Media dont Julie Blanc fait activement partie, elle en parlera après moi.
Son travail est décisif par rapport à ce que je vous ai exposé.
Il s'agit aussi de documenter, pour que chacun puisse comprendre puis s'approprier ces briques.
Cette proposition n'est qu'un système, il s'agit d'une dynamique et non d'une solution toute faite.
Cette dynamique doit faire l'objet d'une réutilisation dans le cadre de projets éditoriaux, en l'adaptant et la modifiant.

> Nous avons trop peu d'occasions de vraiment _faire_ quoi que ce soit parce que notre environnement est trop souvent prédéterminé à distance.
>
> {{< cite "crawford_eloge_2016" "84" >}}

Je vous invite à lire l'ouvrage de Matthew Crawford, _Éloge du carburateur, essai sur le sens et la valeur du travail_.
Pendant ce riche colloque, nous avons pu aborder à la fois l'idée de rétroaction dans les humanités numériques, les concepts d'éditorialisation et de rupture, mais aussi la question des outils libres, ou les stratégies de médiation numérique inclusives : l'acte de publication réunit ces différentes intentions.
Le numérique, si nous décidons d'en faire une utilisation réfléchie, nous avons l'occasion de "vraiment faire" quelque chose.

Et, pour conclure, l'exemple de Stylo développé par la Chaire de Recherche du Canada sur les écritures numériques est remarquable, ce qui me permet de faire le lien avec les interventions de [cette séance](https://www.crihn.org/colloque-2018/programme/).

## Support de présentation
Le support de présentation de cette communication est disponible en ligne :  
[https://presentations.quaternum.net/systeme-modulaire-de-publication-crihn/](https://presentations.quaternum.net/systeme-modulaire-de-publication-crihn/)

Les contenus de cette présentation sont versionnées sur GitLab :  
[https://gitlab.com/antoinentl/systeme-modulaire-de-publication-crihn](https://gitlab.com/antoinentl/systeme-modulaire-de-publication-crihn)

## Bibliographie
Cette bibliographie correspond à cette communication, le mémoire contient une bibliographie beaucoup plus étoffée.
Les références de cette communication sont également disponibles [sur Zotero](https://www.zotero.org/antoinentl/items/collectionKey/6IILIZW4).

{{< bibliography >}}
