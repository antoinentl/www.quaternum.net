---
layout: post
title: "Générateur de site statique, un modèle alternatif"
date: "2016-01-09T13:00:00"
comments: true
published: true
categories: 
- carnet
---
Depuis quelques années l'intérêt pour les générateurs de sites statiques grandit&nbsp;: pourquoi les *static site generators* suscitent tant d'attention à l'heure où des <abbr title="Content Management System">CMS</abbr> comme Wordpress ou Drupal semblent être les outils les plus utilisés pour la création de site et la gestion de contenus sur le web&nbsp;? Simple gadget pour *geek*&nbsp;? Ou *workflow* qui permet d'aborder la publication et la gestion de contenus autrement&nbsp;? Petit panorama du fonctionnement des générateurs de sites statiques, des modèles alternatifs et disruptifs.

<!-- more -->

1. [Définition](#1)
2. [Fonctionnement](#2)
3. [Quels intérêts&nbsp;?](#3)
4. [Quels usages&nbsp;?](#4)
5. [Quelles limites&nbsp;?](#5)
6. [Tentative de conclusion](#6)
7. [Quelques ressources](#7)

<h2 id="1">1. Définition</h2>
Qu'est-ce qu'un générateur de site statique -- *static site generator* en anglais&nbsp;?

Les systèmes de gestion de contenu -- content management system ou CMS en anglais -- classiques comme Wordpress, Spip ou Drupal, permettent de gérer de façon dynamique les contenus&nbsp;: les pages et articles d'un site n'existe pas, ils sont générés dynamiquement lorsqu'un utilisateur tente de les afficher dans son navigateur. L'ensemble des contenus est donc stocké dans des bases de données. Si je souhaite voir telle page dans mon navigateur, le titre, le contenu, le menu, le pied de page, etc. seront appelés et ma page sera dynamiquement générée. L'avantage est du côté de l'administrateur du site plus que du côté de l'utilisateur, car il est très simple de modifier n'importe quel élément du site, les modifications seront visibles immédiatement *en ligne*.  
Sans parler d'inconvénients, les *CMS* classiques et dynamiques présentent quelques contraintes, dont celle de devoir disposer d'un serveur web qui gère un ou des langages dynamiques -- le plus souvent PHP -- ainsi que des bases de données -- SQL.

Un générateur de site statique, comme son nom l'indique, génère des fichiers statiques pour un site web. Plutôt que de disposer de fichiers dynamiques qui permettent d'aller chercher l'ensemble des contenus d'une page web -- titre, corps du texte, date d'édition, auteur, menu, pied de page, etc. --, avec un générateur de site statique on obtient un ensemble de fichiers HTML et CSS, *à l'ancienne*.  
Un générateur de site statique distingue donc les parties *édition de contenus*, *gestion des fichiers du site web*, et *consultation par les utilisateurs*. On peut dire qu'il s'agit d'un workflow asynchrone. À chaque modification -- d'une page ou d'un article, du menu ou du pied de page -- l'ensemble du site web doit être généré à nouveau.  
Comme les CMS classiques, la gestion de la mise en forme est également distincte, avec un fonctionnement par *thèmes* ou *templates*.  
Il suffit de disposer d'un serveur web pour y déposer les fichiers du site web ainsi généré, sans besoin de langages dynamiques ou de bases de données. Le serveur web n'aura comme rôle que de servir les pages demandées.

**Attention** à ne pas confondre *static site generator* et *flat CMS* -- ou *file-based CMS*. Les *flat CMS* sont des systèmes qui fonctionnent sans base de données mais avec des langages dynamiques qui permettent une interaction *synchrone*. L'intéret des *flat CMS*&nbsp;? Se passer de base de données sans perdre l'avantage d'interagir en direct sur son site, et de disposer d'un système beaucoup plus léger. [Kirby](http://getkirby.com/) est un exemple de *flat CMS*, il est d'ailleurs utilisé pour le -- nouveau -- site web des [Rencontres internationales de Lure](http://delure.org/).

<h2 id="2">2. Fonctionnement</h2>
Voici les différentes étapes dans l'utilisation d'un générateur de site statique&nbsp;:

1. édition des contenus&nbsp;: bien souvent via un simple éditeur de texte et avec le langage de balisage [Markdown](https://fr.wikipedia.org/wiki/Markdown)&nbsp;;
2. génération des fichiers du site web -- HTML et CSS&nbsp;;
3. *déploiement*&nbsp;: envoi des fichiers sur un serveur web.

Habituellement ces opérations sont effectuées *en local*, sur la machine de la personne qui administre le site, et non directement sur son serveur web.

Un générateur de site statique se charge donc de *générer* l'ensemble des fichiers (étape 2), et éventuellement de *déployer* le site (étape 3). Si l'on prend l'exemple de [Jekyll](http://jekyllrb.com/), voici les différents types de fichiers à l'origine du site web généré&nbsp;:

- des fichiers de contenus, en Markdown&nbsp;: pages et articles&nbsp;;
- des fichiers de structuration et de mise en forme, en HTML et CSS&nbsp;: gestion du *header*, des menus, du pied de page, les feuilles de style, etc.&nbsp;;
- des fichiers de paramétrage, en [YAML](https://fr.wikipedia.org/wiki/YAML)&nbsp;: les métadonnées du site, le nombre d'articles par défaut sur la page, la façon dont le site web doit être déployé, etc.

Il existe de nombreux systèmes, [StaticGen](https://www.staticgen.com/) en recense plus d'une centaine, basés sur différents langages, et avec des options et des usages parfois très différents. Idem pour les *thèmes*, nombreux ou relativement simples à créer.  
Certains, comme [Karl](http://www.la-grange.net/) ou [David](https://larlet.fr/david/) -- ou bien d'autres -- développent leur propre système, souvent minimaliste.


<h2 id="3">3. Quels intérêts&nbsp;?</h2>
Les intérêts sont nombreux, en voici quelques-uns&nbsp;:

- l'édition des contenus se fait très souvent *en local*, et donc sans avoir besoin d'une connexion. Celle-ci sera nécessaire lorsque le site sera *déployé*, c'est-à-dire envoyé sur le serveur web&nbsp;;
- le site web n'est fait que de fichier HTML et CSS -- et éventuellement JavaScript --, ce qui le rend très très léger, donc très performant, et très rapide à afficher pour l'utilisateur qui le consulte&nbsp;;
- la maintenance est simple côté serveur web, puisqu'il n'y a pas de langage dynamique ou de base de données&nbsp;; idem pour les aspects de robustesse et de résilience&nbsp;;
- pas de problème de sécurité lié aux langages dynamiques ou aux bases de données, puisque le serveur web n'accueille que des fichiers HTML et CSS&nbsp;;
- si vous changez de système de gestion de contenus, tous vos articles et pages sont des fichiers Markdown, lisibles sur n'importe quel système d'exploitation avec n'importe quel logiciel. Il y a donc une réelle interopérabilité, et un certain degré d'archivage des contenus&nbsp;;
- les générateurs de sites statiques sont des systèmes relativement simples, beaucoup plus que des CMS comme Wordpress ou Drupal qui sont souvent bien trop puissants pour l'usage qui en est fait -- site ou blog de petite taille.


<h2 id="4">4. Quels usages&nbsp;?</h2>
Il faut reconnaître que ce type de système fonctionne bien pour des carnets, des blogs, des *petits sites web* qui ne nécessitent pas une mise à jour à la seconde. Même si aujourd'hui le web semble de plus en plus dynamique, cela concerne de nombreux sites web&nbsp;:

- blogs et carnets personnels&nbsp;;
- blogs de structures privées ou publiques&nbsp;;
- sites web statiques -- je veux dire par là qu'il n'y a pas de nécessité à faire une mise à jour toutes les heures -- comme les *portfolios*, certains sites institutionnels, etc.&nbsp;;
- livres et manuels en ligne -- avec l'exemple de [couscous.io](http://couscous.io/) qui est un générateur de site statique&nbsp;;
- etc.

En terme d'usage pour le gestionnaire du site web, il faut surtout être prêt à abandonner l'usage des [WYSIWYG](https://fr.wikipedia.org/wiki/What_you_see_is_what_you_get) pour [Markdown](https://fr.wikipedia.org/wiki/Markdown). Une libération plus qu'une contrainte&nbsp;!


<h2 id="5">5. Quelles limites&nbsp;?</h2>

Le frein le plus important est l'installation, le paramétrage et l'utilisation d'un générateur de site statique&nbsp;: il faut bien souvent passer par un terminal -- avec des commandes très simples, mais tout de même --, et la configuration peut être rebutante.  
Mais il existe de nombreuses ressources pour installer et configurer un générateur de site statique, et c'est un modèle désormais accessible à des profils non développeurs, comme moi.

Le fait d'écrire *en local* peut être vu comme un avantage -- ne pas dépendre d'une connexion, disposer d'un environnement plus propice à l'écriture que les WYSIWYG des habituels CMS, limiter le transfert de données parfois inutile --, ou comme un inconvénient&nbsp;: il faut nécessairement utiliser la machine configurée avec le générateur de site statique pour générer le site web et déployer ou envoyer les fichiers vers le serveur web. Il est toutefois possible d'envisager d'installer un tel système sur un serveur web, certaines plateformes proposent d'ailleurs ce type de service.

Le frein principal est la gestion des commentaires, avec l'obligation d'utiliser des langages dynamiques et des bases de données, ou un service tiers comme [Disqus](https://disqus.com/).

Enfin, point déterminant, les générateurs de sites statiques ne sont pas adaptés pour des sites web complexes, ou alors en les combinant avec d'autres systèmes -- notamment pour la gestion de très nombreuses données ou pour les sites de commerce en ligne.


<h2 id="6">6. Tentative de conclusion</h2>
Si cela semble complexe à appréhender, gérer son site web avec un générateur de site statique est désormais accessible à toute personne curieuse. Cela semble même devenir une option sérieuse ou un choix pour certains *produits* web de petites ou grandes entreprises.

Je ne rentre pas dans les détails techniques du fonctionnement d'un générateur de site statique -- et notamment le fait que désormais il est possible de ne *générer* qu'une partie des fichiers en fonction de modifications apportées, point important --, mais selon moi le principal changement de paradigme semble être sur la distinction écriture-publication.  
Par ailleurs, la légèreté -- du système de gestion et du site web obtenu -- est un point essentiel, et le fait que l'on peut utiliser un système plus en adéquation avec les besoins réels -- dans les cas de blogs, carnets et sites web *vitrines* -- plutôt que des <strike>usines à gaz</strike> des systèmes trop complexes.

Les générateurs de sites statiques ont par ailleurs impulsé un mouvement de simplicité, de légèreté, de robustesse et de résilience qui semble pris en compte dans les évolutions des CMS.

Enfin, utiliser un générateur de site statique oblige et permet de maîtriser le système de gestion de contenus que l'on utilise, point essentiel.


<h2 id="7">7. Quelques ressources</h2>

- [Brian Rinaldi, Static site generators: the latest tools for building static websites](http://conferences.oreilly.com/fluent/javascript-html-us/public/content/static-site-generators?download=true)&nbsp;;
- [StaticGen, Top Open-Source Static Site Generators](https://www.staticgen.com/)&nbsp;;
- [What is an Static site generator?](https://www.quora.com/What-is-an-Static-site-generator), sur Quora&nbsp;;
- [Eduardo Bouças, An Introduction to Static Site Generators](https://davidwalsh.name/introduction-static-site-generators)&nbsp;;
- [Mathias Biilmann Christensen, Why Static Website Generators Are The Next Big Thing](https://www.smashingmagazine.com/2015/11/modern-static-website-generators-next-big-thing/)&nbsp;;
- sur quaternum.net&nbsp;: [Pourquoi quitter Wordpress&nbsp;?](/2012/12/23/pourquoi-quitter-wordpress/), [LaTeX et Jekyll : deux workflows de publication](/2014/04/26/latex-et-jekyll/) et [Un point sur Jekyll](/2014/12/29/un-point-sur-jekyll/).