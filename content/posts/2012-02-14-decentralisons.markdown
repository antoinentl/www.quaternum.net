---
comments: true
date: 2012-02-14
layout: post
slug: decentralisons
title: Décentralisons
categories:
- carnet
---
Plusieurs événements plus ou moins récents ne peuvent que nous inciter à revoir notre utilisation de services proposés sur le Web, et à réfléchir à comment et où nous plaçons nos données&nbsp;; parmi ces événements&nbsp;: notamment la nouvelle politique de confidentialité de Google, ou le rachat de Netvibes par Dassault Systèmes. Et puis plus généralement revenir (ou moins dans mon cas) à la base du fonctionnement et de la philosophie d'Internet&nbsp;: la décentralisation des données, des services et des systèmes (depuis quelques années, et encore plus depuis un an, mon usage intensif des multiples services de Google allait à l'encontre de ces principes).  
Utiliser massivement les services et les infrastructures (souvent gratuites en apparence) d'entreprises comme Google soulève plusieurs questions&nbsp;: la confidentialité des données (point le plus souvent évoqué), mais aussi la centralisation de l'infrastructure (systèmes et logociels). Il faudrait donc définir et construire nos propres écosystèmes de services (mails, agrégateurs de flux RSS, calendriers, carnets d'adresse, sauvegarde et partage de documents...). Reste la question de l'hébergement, mais même si l'on met en place un tel écosystème complet chez un hébergeur comme OVH, cela ne change-t-il pas quelque chose au moins sur la définition et la maîtrise de ces services ? Décentralisons !