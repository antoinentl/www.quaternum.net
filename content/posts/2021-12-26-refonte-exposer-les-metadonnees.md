---
title: "Refonte : exposer les métadonnées"
date: 2021-12-26T08:00:00-04:00
categories:
- carnet
groupe: refonte
tags:
- refonte
accroche: "Sur le Web, les métadonnées sont la condition d'une lisibilité, d'une visibilité, d'une indexabilité, et d'un signalement."
published: true
---
Sur le Web, les métadonnées sont la condition d'une lisibilité, d'une visibilité, d'une indexabilité, et d'un signalement.
Trop souvent laissées de côté, les métadonnées de la plupart des sites personnels et autres blogs se limitent à une courte description et quelques mots-clés.
Très utiles dans des usages que l'on peut qualifier de généralistes, ces métadonnées sont incontournables dans les domaines de la recherche académique ou de la publication.

J'ai choisi de réécrire les métadonnées de ce site web avant d'attaquer le rendu graphique, mais pourquoi ?
Parce que dans mes pratiques les métadonnées des sites que je consulte et que j'enregistre me font gagner un temps précieux.
Si vous avez la possibilité d'exposer des métadonnées, faites-le !

## Pourquoi exposer ses (méta)données ?
J'en avais déjà parlé [ici](/2018/04/04/une-reappropriation-des-donnees-par-leur-structuration/) (désolé je radote), d'une façon quelque peu maladroite.
Exposer des métadonnées remplit plusieurs objectifs :

- **lisibilité** : une page web sans titre c'est une absence d'information lors de l'affichage de cette page (par exemple dans la fenêtre de votre navigateur web), sans parler des sites web qui sont des applications et dont certaines informations sont déterminantes. Nécessaires aussi pour un affichage adéquat sur les réseaux sociaux, indiquer le nom du site, l'auteur ou l'autrice donne des indications précises. Les métadonnées sont donc importantes pour la simple _lisibilité_ d'une page web ;
- **visibilité et indexabilité** : les _robots_ des moteurs de recherche et autres outils qui recensent le Web se servent de ces métadonnées pour enregistrer la description sommaire des pages web, mais aussi pour les classer ou les _indexer_. Même si les règles changent souvent, il faut noter qu'un titre cohérent, une description et une série de mots-clés permettent d'être référencé convenablement ;
- **signalement** : les liens hypertextes n'utilisent normalement que le… lien (ou URL) ! Mais certaines applications vont enregistrer d'autres informations, comme le créateur ou la créatrice de la page web, ou la date de publication. Typiquement un logiciel/service de gestion de références bibliographiques comme Zotero fait ce travail de récupération — de bien plus de données que les seuls titre, auteur ou autrice et date de publication.

Si vous prenez soin des contenus de votre site web, faites de même avec les métadonnées.

## Quelques bonnes pratiques
Il y a de multiples façons d'exposer des métadonnées, je ne parle ici que des balises `meta` dans l'entête des fichiers HTML.
Pour en savoir plus sur l'implémentation de, par exemple, du RDFa, ce billet de blog est très complet : [Introduction to RDFa](https://alistapart.com/article/introduction-to-rdfa/).

Autre précision importante : il ne s'agit ici que des prémisses ce que l'on peut appeler le [web sémantique](https://fr.wikipedia.org/wiki/Web_s%C3%A9mantique).
D'autres méthodes, parfois plus complexes, sont nécessaires pour exposer des données qui peuvent être manipulables (et pas seulement récupérables).
L'intérêt du web sémantique pour un carnet comme celui-ci est très limité, cette pratique étant plutôt destinée à des sites web comme des entrepôts de données quels qu'ils soient.

Concernant les métadonnées que j'expose ici, j'en distingue deux types :

- les informations utiles à l'indexation : pour les moteurs de recherche, et cela se résume à quelques balises comme le titre, la description, les mots-clés, l'auteur et le lien canonique ;
- les informations de _partage_ : pour les outils comme les logiciels de gestion de références bibliographiques (typiquement Zotero), et pour quelques moteurs de recherche spécialisés, et cette fois il s'agit d'un nombre plus conséquent de données : la date de publication ou de mise en ligne, la langue, la licence attribuée aux contenus, le type de contenus, l'éditeur, l'identifiant ORCID, le résumé du billet, etc.

Plutôt que de préciser en détail les métadonnées (vous pouvez aussi regarder le code source de cette page), voici plusieurs catégories que je privilégie :

- format Dublin Core : ce qui me semble être le minimum pour les humains et les machines ;
- format citation (Google Scholar) : pour une indexation par le moteur de recherche académique de Google ;
- format Twitter Card : pour un affichage quand une page web est partagée sur Twitter ;
- Open Graph Protocol : pour un affichage quand une page web est partagée sur certains réseaux sociaux comme Facebook ;
- format de micro-données Schema.org : exposition des données compatible avec RDF.

## Le cas de Zotero
Pour que les données exposées puissent être récupérées par Zotero, les formats cités ci-dessus suffisent amplement, et notamment le format citation pour préciser qu'il s'agit d'une monographie, d'un chapitre de livre ou d'un article par exemple.
Ça se complique pour un contenu de type _billet de blog_, pour que Zotero reconnaisse convenablement ce format il faut utiliser autre chose que des métadonnées dans l'entête de la page HTML : [la méthode COinS](https://en.wikipedia.org/wiki/COinS).

Afin de structurer les métadonnées d'un billet de blog pour que Zotero reconnaisse ces informations, il s'agit plus spécifiquement de [la norme Z39.88 (attention le lien mène vers 120 pages de documentation au format PDF)](https://groups.niso.org/apps/group_public/download.php/14833/z39_88_2004_r2010.pdf).
J'avoue ne pas avoir lu cette documentation, donc si vous souhaitez utiliser cette méthode, voici les métadonnées pour cette page web — je me suis inspiré [de ce billet de blog](https://www.anton.angelo.nz/2012/05/08/adding-bilbiographic-metadata-to-blog-post/) :

```
<span class="Z3988" title="ctx_ver=Z39.88-2004&amp;rft_val_fmt=info%3Aofi%2Ffmt%3Akev%3Amtx%3Adc&amp;rft.title=Refonte : exposer les métadonnées&amp;rft.aulast=Fauchié&amp;rft.aufirst=Antoine&amp;rft.source=quaternum.net&amp;rft.date=2021-12-26&amp;rft.type=blogPost&amp;rft.format=text&amp;rft.identifier=https://www.quaternum.net/2021/12/26/refonte-exposer-les-metadonnees/&amp;rft.language=French"></span>
```

L'intérêt pour moi d'exposer ces données de façon organisée et méthodique est un moyen d'appliquer des bonnes pratiques pour un _simple_ site web comme ce carnet, tout en explorant les possibilités pour d'autres projets éditoriaux.