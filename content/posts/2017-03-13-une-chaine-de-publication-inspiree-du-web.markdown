---
layout: post
title: "Une chaîne de publication inspirée du web"
date: "2017-03-13T10:00:00"
comments: true
published: true
description: "Le web influence aujourd'hui le domaine du livre jusque dans ses outils de production : une chaîne de publication peut être élaborée en s'éloignant des modèles existants. Des initiatives le démontrent, basées sur l'interopérabilité, la modularité et une approche multiforme du livre."
image: "https://www.quaternum.net/images/2017-chaine-de-publication-mini.png"
categories: 
- carnet
---
Le web influence aujourd'hui le domaine du livre jusque dans ses outils de production&nbsp;: une chaîne de publication peut être élaborée en s'éloignant des modèles existants. Des initiatives le démontrent, basées sur l'interopérabilité, la modularité et une approche multiforme du livre.

<!-- more -->

![Tentative de représentation dessinée de la granularité d'un texte, de la page au livre](/images/2017-chaine-de-publication-01.png)

1. [Définition d'une chaîne de publication](#1-définition-dune-chaîne-de-publication)
2. [Les chaînes traditionnelles : leurs contraintes, leurs limites](#2-les-chaînes-traditionnelles-leurs-contraintes-leurs-limites)
3. [Les étapes d’une chaîne de publication](#3-les-étapes-dune-chaîne-de-publication)
4. [Une nouvelle approche](#4-une-nouvelle-approche)
5. [Ressources](#5-ressources)


## 1. Définition d'une chaîne de publication
Une chaîne de publication ou d'édition a pour objectif de gérer des contenus depuis le manuscrit d'un auteur jusqu'à la publication imprimée ou numérique d'un ouvrage, en passant par les phases de relecture, de structuration et de mise en forme. Cette chaîne, ce *workflow*, est le noyau technique d'une maison d'édition lui permettant de manipuler des textes et de leur donner vie sous forme de livre. Un certain nombre de chaînes de publication de maisons d'édition sont dépendantes de logiciels, elles sont peu flexibles et peu performantes, peu ou pas adaptées au (livre) numérique, ou tout simplement inexistantes. Basées sur des logiciels comme Microsoft Word et Adobe InDesign, ou sur des chaînes XML puissantes mais difficiles à utiliser, elles sont le nœud stratégique d'une maison d'édition, le reflet de l'organisation d'une équipe autour de textes.

La philosophie et les technologies du développement web peuvent apporter de nouvelles approches au milieu du livre et de l'édition, c'est ce que l'équipe de [Getty Publications](http://www.getty.edu/publications/) a expliqué dans un billet daté de mai 2016, [An Editor’s View of Digital Publishing](http://blogs.getty.edu/iris/an-editors-view-of-digital-publishing/), et c'est ce que je me propose de détailler ici.



## 2. Les chaînes traditionnelles&nbsp;: leurs contraintes, leurs limites
Le monde de l'édition utilise depuis longtemps le *numérique*, mais souvent par le biais de l'outil logiciel plus que par la mise en place de *worklows* -- des méthodes et des organisations pour la production de livres. Voici deux exemples de chaînes de publication traditionnelles, dans deux secteurs d'édition différents&nbsp;:

### 2.1. Le couple traitement de texte et PAO
Les outils les plus utilisés dans le monde de l'édition sont sans aucun doute Microsoft Word et Adobe InDesign, et constituent bien souvent à eux seuls une chaîne éditoriale -- d'autant plus dans le domaine de la littérature ou de la fiction où la structuration de contenus est relativement simple. Word présente l'avantage d'être l'un des logiciels les plus utilisés pour le texte, principalement pour communiquer entre auteur, éditeur, directeur de collection, correcteur, graphiste, etc. InDesign est un outil de <abbr title="Publication assistée par ordinateur">PAO</abbr> facile à manipuler pour mettre en forme -- plus que pour structurer -- des contenus, dans l'objectif final d'*imprimer* un livre -- un assemblage de feuilles de papier formant un objet physique.

### 2.2. Chaîne(s) XML
Certaines maisons d'édition ont développé des chaînes de publication XML, en complément ou à la place du duo traitement de texte et logiciel de PAO, afin de&nbsp;:

- structurer les contenus, avec un niveau sémantique très élevé, au-delà des simples niveaux de titres et listes à puces&nbsp;;
- pouvoir diffuser les différentes formes des textes publiés, notamment sur des plateformes comme [OpenEdition Books](https://books.openedition.org/) ou [Cairn.info](http://www.cairn.info/)&nbsp;;
- assurer une pérennité à ces contenus riches avec une <abbr title="Document type definition">DTD</abbr> qui documente l'utilisation de XML.

L'exemple emblématique de l'utilisation de chaînes XML est celui de nombreuses [presses universitaires](https://fr.wikipedia.org/wiki/Maison_d%27%C3%A9dition_universitaire), et notamment [la chaîne d’édition structurée XML-TEI](http://www.unicaen.fr/recherche/mrsh/document_numerique/projets/chaine_editoriale).


### 2.3. Des besoins très différents, des limites
Ces deux exemples concernent des secteurs, des démarches et des besoins très différents. Pourtant, que ce soit le duo traitement de texte et logiciel de PAO, ou les chaînes XML, il y a un certain nombre de limites en terme de facilité d'usage et de qualité de production, qui dépendent des outils utilisés.

Un traitement de texte comme Microsoft Word présente des contraintes importantes. Pour mieux comprendre les limites d'un <abbr title="What You See Is What You Get">WYSIWYG</abbr>, voici un article très complet de iA&nbsp;: [Traitement de texte multicanal](https://www.quaternum.net/2016/08/31/traitement-de-texte-multicanal/). L'autre écueil du duo traitement de texte et logiciel de PAO est la perte de maîtrise des éditeurs de leurs outils de conception, de fabrication et de production.

Si les chaînes XML-TEI sont justement un exemple de reprise en main par les éditeurs de leur outil de publication, elles ont plusieurs inconvénients&nbsp;:

- complexité du modèle XML-TEI -- qui répond à un besoin de structuration très fin dans le domaine des sciences humaines et sociales&nbsp;;
- difficultés de maintenir une structure XML trop personnalisée, contrairement à HTML5 qui est balisé -- et HTML5 est un langage XML. Les débats sur cette question sont souvent [enflammés](http://www.infogridpacific.com/blog/igp-blog-20130210-avoid-xml-first-like-the-plague.html)&nbsp;;
- dépendance à certains logiciels pour écrire -- des traitements de texte comme Word ou LibreOffice -- ou pour éditer le XML&nbsp;;
- irréversibilité du processus d'édition&nbsp;: difficultés voire impossibilité de modifier les contenus à certains moments de la chaîne -- logiques d'export et d'import difficilement réversibles&nbsp;;
- forte dépendance à des développements spécifiques dans le cas d'approche de plateforme complète basée sur XML comme [Zord](https://github.com/DrozNumerique/Zord/wiki).

Les *workflows* de fabrication de livres pourraient s'appuyer sur des approches alternatives et modulaires, leur donnant une plus grande indépendance, et facilitant les liens entre papier et numérique, entre auteurs et éditeurs, et entre livre et web.


## 3. Les étapes d'une chaîne de publication
Afin de bien comprendre le fonctionnement d'une chaîne de publication et les besoins qui y sont liés, voici les différentes étapes de conception d'un livre, et des propositions pour mettre en place un *workflow* modulaire&nbsp;:

### 3.1. Écrire et structurer&nbsp;: utiliser un langage sémantique simple
L'écriture d'un texte, et plus particulièrement dans le cas de la non-fiction, consiste en deux processus&nbsp;: l'inscription et la structuration. Si un logiciel comme Microsoft Word est conçu pour inscrire un texte, la structuration est parfois moins évidente&nbsp;: la confusion est fréquente entre structure -- qualification d'un contenu, un niveau de titre par exemple -- et mise en forme -- taille ou graisse d'une police qui signale formellement un niveau de titre.

Il y a des méthodes, des langages et des logiciels qui permettent de réaliser une inscription et une structuration avec plus de cohérence et d'efficacité. Se passer d'application <abbr title="What You See Is What You Get">WYSIWYG</abbr> est possible, et un langage sémantique simple comme [Markdown](https://fr.wikipedia.org/wiki/Markdown) dissocie la structure et la mise en forme -- ce que des <abbr title="What You See Is What You Get">WYSIWYG</abbr> ont tendance à mélanger --, et supprime la dépendance à un logiciel particulier&nbsp;: Markdown est un format interopérable, compréhensible par les humains, et utilisé par -- et utilisable avec -- [de nombreux logiciels et services](https://markdownlinks.com/). Facilement déclinable en HTML ou en XML, Markdown devient le remplaçant idéal d'un traitement de texte.

Markdown vous semble trop limité en terme de structuration&nbsp;? Ce langage sémantique simple peut intégrer des éléments HTML, et il existe d'autres langages sémantiques compréhensibles par des humains mais extensibles, comme [AsciiDoc](http://asciidoctor.org/docs/what-is-asciidoc/). En plus d'un format pour le texte, l'utilisation d'un langage de description des métadonnées peut donner plus de puissance à ces langages sémantiques, [YAML](http://www.yaml.org/) en est un exemple.


### 3.2. Partager, collaborer et valider&nbsp;: versionner
Les solutions de partage de fichiers et de documents sont nombreuses, du *pad* au service de bureautique en ligne -- comme Dropbox ou Google Drive. Cependant ces outils présentent plusieurs limites&nbsp;: le manque de distinction entre la structure et la mise en forme, la nécessité d'être continuellement connecté dans certains cas, ou la dépendance à une plateforme. Par exemple il faut reconnaître que Google Drive offre beaucoup de possibilités pour travailler à plusieurs sur un texte, tant que vous restez sur la plateforme. Si vous décidez de finaliser un travail avec un autre outil ou logiciel, cela nécessitera des opérations complexes et douloureuses -- par exemple pour styler correctement un document texte.

Plutôt que de travailler sur une même plateforme fermée et limitée, ou plutôt que d'*échanger* des fichiers avec des numéros de version rapidement incompréhensibles, un fonctionnement plus logique est de *versionner*&nbsp;: construire une arborescence logique qui intègre différentes versions des textes en fonction des différents intervenants d'un même projet éditorial. Pour cela il existe des logiciels de gestion de versions, comme [git](https://fr.wikipedia.org/wiki/Git), très utilisé pour collaborer sur du code, mais qui peut aussi tout à fait convenir à du texte, et qui s'avère même plus pertinent que le système de révisions. Si git demande un apprentissage non négligeable -- mais pourtant aujourd'hui incontournable dans les domaines du numérique --, de plus en plus d'outils et d'interfaces facilitent son utilisation -- comme la plateforme [GitHub](https://github.com/). Bonus&nbsp;: dans certains cas, publier *publiquement* le code source du livre sur une plateforme comme GitHub ou [GitLab](https://gitlab.com) ouvre en plus des perspectives de collaboration, comme je l'ai présenté [récemment](/2017/03/07/ecrire-un-livre-en-2017/).

Travailler avec un format identique pendant toutes les étapes d'une chaîne de publication assure une interopérabilité&nbsp;: une fluidité des échanges, des opérations simples sur les textes, et une pérennité des fichiers. Ajouter à cela un logiciel de gestion de versions, et vous disposez déjà d'un *workflow* pour le textes.


### 3.3. Mettre en forme&nbsp;: HTML + CSS
Dans le monde du livre et de l'édition, mettre en forme un texte semble totalement dépendant d'InDesign, ou d'autres outils similaires, non interopérables et bien souvent fermés. Ces solutions logicielles posent un problème important&nbsp;: une fois arrivé à cette étape de mise en forme, le retour à l'étape précédente est difficile, les nouvelles interventions sur le texte deviennent donc complexes pour certains acteurs de la chaîne.

Le problème ne vient pas de l'outil lui-même, mais de l'usage qui en est fait&nbsp;: InDesign devrait plutôt être utilisé pour sa capacité à créer une mise en forme plutôt que pour la fabrication d'un livre -- un fichier PDF, dans le cas du livre papier. La logique du web est séduisante&nbsp;: la structure et la mise en forme sont distinctes, et le format de mise en forme n'est pas dépendant d'un logiciel. Vous pouvez donc créer, modifier, partager, améliorer votre feuille de style sans dépendre d'un outil particulier. Créer des livres papier avec des fichiers HTML et une feuille de style CSS&nbsp;? Cela est possible, comme le démontre [l'expérience d'Hachette récemment décrite par Dave Cramer](https://www.xml.com/articles/2017/02/20/beyond-xml-making-books-html/). Des solutions comme [Prince](http://www.princexml.com) peuvent par ailleurs grandement faciliter la transformation de HTML+CSS en PDF. Il y a encore beaucoup de travail pour atteindre un niveau de qualité comparable à celui permis par un outil comme InDesign, mais il est aujourd'hui possible de concevoir et de fabriquer un livre en CSS.

Utiliser un format compréhensible et interopérable, gérer les différentes parties du texte avec un logiciel de version, et séparer la mise en forme pour conserver la puissance de la chaîne de publication. Il reste donc à générer le livre, quelle que soit sa forme.


### 3.4. Générer les différentes versions du livre
Aujourd'hui le format PDF n'est plus le seul format nécessaire pour fabriquer un livre et le diffuser. Parmi d'autres formats désormais indispensables, on peut citer l'EPUB pour les livres numériques -- qui peut connaître une déclinaison en KF8 pour Amazon --, et le format web -- un ensemble de pages HTML et de fichiers CSS. Les logiciels de PAO ne génèrent pas convenablement ces deux formes, ils sont pensés pour des mises en forme figés, idéal donc pour obtenir un format de sortie PDF -- imprimable --, et par ailleurs leur logique de <abbr title="What You See Is What You Get">WYSIWYG</abbr> entraîne des quiproquos et des confusions entre structure et mise en forme.

Aujourd'hui une chaîne de publication doit générer simultanément plusieurs formes et formats, du PDF à l'EPUB en passant par de l'HTML/CSS. Et pour faire correctement ces opérations vous ne pouvez pas faire confiance à un seul logiciel&nbsp;: la qualité sera médiocre, ou la dépendance à l'outil trop importante, ou [la dette technique](/2016/02/24/dette-technique/) énorme, ou les trois à la fois. Un générateur de site statique va se charger de produire des pages web, et de les organiser, les fichiers originaux des textes restent donc tels qu'ils sont. Les pages web pourront ensuite être transformées au format PDF ou adaptées pour un livre numérique au format EPUB. On peut même envisager d'ajouter une brique supplémentaire, comme le *framework* [Blitz](https://friendsofepub.github.io/Blitz/), pour la production d'EPUBs de qualité.

Un langage sémantique simple, un logiciel de versions, une mise en forme en CSS, une génération des fichiers et formats de sortie grâce à un générateur de site statique&nbsp;: voici une chaîne de publication modulaire, inspirée des outils que le web a construit au fil des années.


### 3.5. Publier
Cette cinquième étape ne concerne que la version web d'un livre. Pour en savoir plus sur le concept de livre web ou *web book*&nbsp;: [Le livre web, une autre forme du livre numérique](/2016/10/24/le-livre-web-une-autre-forme-du-livre-numerique/).

Une chaîne de publication peut également produire des pages web organisées, donc des fichiers HTML et CSS, pour créer un site web. Les générateurs de site statique se chargent de cette opération, en se basant sur des fichiers au format Markdown par exemple, et à partir de données de configuration et de métadonnées souvent au format [YAML](http://yaml.org/). Sans venir bouleverser le reste de la chaîne de publication, il est possible de déployer le livre en version web, que ce soit pour un accès libre ou sur identification, ou un extrait diffusable et lisible.


## 4. Une nouvelle approche
Si des chaînes de publication inspirées du web existent depuis longtemps -- voir quelques exemples ci-dessous --, c'est l'approche modulaire qui me semble originale et inédite&nbsp;: définir chaque étape et choisir des outils adéquats.

### 4.1. Des chaînes et des expérimentations
Explorons quelques exemples de chaînes de publication originales&nbsp;:

[LaTeX](https://fr.wikipedia.org/wiki/LaTeX) est un langage et un workflow, d'abord pensé pour la publication de documents imprimés, mais qui peut être utilisé pour la fabrication de documents numériques. C'est une chaîne de publication qui dépend entièrement du langage TeX.

O'Reilly Media a mis en place une chaîne de publication basée sur DocBook -- un langage sémantique dont est issu AsciiDoc -- et git dès le début des années 2000. Aujourd'hui ce modèle a même été industrialisé, commercialisé et ouvert à d'autres usages, avec la plateforme [Atlas](https://atlas.oreilly.com/).

Plus expérimentales, les démarches d'<abbr title="Open Source Publishing">OSP</abbr> autour de [HTML 2 PRINT](http://osp.kitchen/tools/html2print/)&nbsp;: il s'agit de fabriquer des livres physiques, en papier, à partir d'HTML et de CSS -- et un peu de JavaScript. OSP utilise également des *pads* et git dans ses différents projets.

[GitBook](https://www.gitbook.com/) est une plateforme ou forge de fabrication de livres basée sur git, avec un ensemble d'outils&nbsp;: de l'éditeur pour faciliter la gestion de la sémantique et de git, au *template* pour la mise en forme, en passant par des fonctionnalités collaboratives en ligne.

Arthur Attwell a mis en place une chaîne de publication basée sur un générateur de site statique, et elle fonctionne en production pour des livres web et des livres imprimés. Il a décrit cette démarche dans son livre *[The Electric Book workflow](http://electricbook.works/)*.

[Asciidoctor](http://asciidoctor.org/) est une chaîne de publication basée sur AsciiDoc, un langage sémantique plus puissant que Markdown, et plus facile à utiliser et plus adapté à la conversion en HTML5. C'est sans doute l'initiative la plus intéressante en terme de chaîne de publication inspirée du web.

### 4.2. Getty Publications
L'approche du département *numérique* de [Getty Publications](http://www.getty.edu/publications/) est quelque peu différente des exemples présentés ci-dessus&nbsp;: plutôt que de remplacer des outils par d'autres outils -- Word et InDesign par un éditeur de texte et l'utilisation d'HTML/CSS par exemple --, l'équipe d'édition numérique de Getty Publications a mis en place une chaîne de publication alternative basée sur une approche modulaire&nbsp;:

- choix d'un langage sémantique utilisée tout du long de la chaîne de publication&nbsp;: Markdown&nbsp;;
- méthode de partage et de collaboration&nbsp;: git&nbsp;;
- structuration et mise en forme&nbsp;: HTML et CSS&nbsp;;
- génération des différentes versions du livre&nbsp;: générateur de site statique pour les versions web et EPUB, avec l’ajout de Prince pour produire le PDF à partir des pages web, et des formats de données comme le CSV ou le JSON pour certains contenus des livres.

La nouveauté ne réside pas dans les alternatives à un traitement de texte et un outil de publication assistée par ordinateur, mais dans la constitution d'[une nouvelle chaîne de publication modulaire](https://jamstatic.fr/2017/01/23/produire-des-livres-avec-le-statique/). Les différentes briques de cette chaîne peuvent être remplacées facilement, et les modifications du contenu peuvent se faire à n'importe quel moment du processus éditorial. Pour découvrir des exemples de publication numérique de Getty Publications&nbsp;: [http://www.getty.edu/publications/digital/digitalpubs.html](http://www.getty.edu/publications/digital/digitalpubs.html).

Cette chaîne est interopérable, modulaire et multiforme.


### 4.3. Un nouveau modèle de chaîne de publication
![Représentation graphique et dessinée des concepts d'interopérabilité, de modularité et de multiforme](/images/2017-chaine-de-publication-02.png)

Les principes d'un nouveau modèle de chaîne de publication sont les suivants&nbsp;:

1. **interopérabilité**&nbsp;: utilisation de formats compatibles avec différents logiciels et modes de fonctionnement. L'interopérabilité permet d'avoir une approche modulaire, puisque les formats utilisés ne dépendent pas d'un seul logiciel&nbsp;;
2. **modularité**&nbsp;: possibilité de modifier une partie de la chaîne de publication, sans incidence majeure sur ces trois principes et sur les autres parties ou étapes de la chaîne. La modularité ouvre des perspectives multiformes, puisque des *briques* peuvent être ajoutées à la chaîne de publication en fonction des besoins&nbsp;;
3. **multiforme**&nbsp;: production et génération de plusieurs formes et formats du livre, sans remettre en cause l'ensemble de la chaîne.

À partir de ces trois principes il devient possible de créer une chaîne de publication évolutive, indépendante, réversible et résiliente. Une chaîne de publication moderne.


## 5. Ressources
Voici une liste de sources qui ont inspirées cet article, et que je vous recommande de lire&nbsp;:

- Rachel Andrew, "Designing For Print With CSS", *Smashing Magazine*, [https://www.smashingmagazine.com/2015/01/designing-for-print-with-css/](https://www.smashingmagazine.com/2015/01/designing-for-print-with-css/)
- Arthur Attwell, *The Electric Book workflow*, [http://electricbook.works/]()
- Dave Cramer, "Beyond XML: Making Books with HTML", *XML.com*, [https://www.xml.com/articles/2017/02/20/beyond-xml-making-books-html/](https://www.xml.com/articles/2017/02/20/beyond-xml-making-books-html/)
- David Demarre, *Git for Humans*, A Book Apart, [https://abookapart.com/products/git-for-humans](https://abookapart.com/products/git-for-humans)
- Antoine Fauchié, "Le livre web, une autre forme du livre numérique", *quaternum*, [https://www.quaternum.net/2016/10/24/le-livre-web-une-autre-forme-du-livre-numerique/](https://www.quaternum.net/2016/10/24/le-livre-web-une-autre-forme-du-livre-numerique/)
- Antoine Fauchié et Eric Gardner, "Publier des livres avec un générateur de site statique", *JAMStatic*, [https://jamstatic.fr/2017/01/23/produire-des-livres-avec-le-statique/](https://jamstatic.fr/2017/01/23/produire-des-livres-avec-le-statique/)
- Antoine Fauchié, "Écrire un livre en 2017", *quaternum*, [https://www.quaternum.net/2017/03/07/ecrire-un-livre-en-2017/](https://www.quaternum.net/2017/03/07/ecrire-un-livre-en-2017/)
- Sophie Fétro, "Œuvrer avec les machines numériques", *Back Office*, [http://www.revue-backoffice.com/numeros/01-faire-avec/sophie-fetro-oeuvrer-machines-numeriques](http://www.revue-backoffice.com/numeros/01-faire-avec/sophie-fetro-oeuvrer-machines-numeriques)
- Eric Gardner, "Digital Publishing Needs New Tools", *The Getty Iris*, [http://blogs.getty.edu/iris/digital-publishing-needs-new-tools/](http://blogs.getty.edu/iris/digital-publishing-needs-new-tools/)
- Eric Gardner, "Building Books with Middleman Extensions", [http://egardner.github.io/posts/2015/building-books-with-middleman/](http://egardner.github.io/posts/2015/building-books-with-middleman/)
- Lara Hogan, "Coding a book", *larahogan.me/blog/*, [http://larahogan.me/blog/coding-a-book/](http://larahogan.me/blog/coding-a-book/)
- iA, "Multichannel Text Processing", [https://ia.net/topics/multichannel-text-processing/](https://ia.net/topics/multichannel-text-processing/) ([traduction française](https://www.quaternum.net/2016/08/31/traitement-de-texte-multicanal/))
- Ruth Evans Lane, "An Editor’s View of Digital Publishing", *The Getty Iris*, [http://blogs.getty.edu/iris/an-editors-view-of-digital-publishing/](http://blogs.getty.edu/iris/an-editors-view-of-digital-publishing/)
- Catherine Muller, "Retour sur la Biennale du numérique 2015. Métiers du livre (2/4)&nbsp;: édition XML-TEI, écriture et document numérique", *Les billets d'enssibLab*, [http://www.enssib.fr/recherche/enssiblab/les-billets-denssiblab/biennale-du-numerique-edition-numerique-encodage-xml-tei](http://www.enssib.fr/recherche/enssiblab/les-billets-denssiblab/biennale-du-numerique-edition-numerique-encodage-xml-tei)
- OSP, "HTML sauce cocktail, sauce à part", *OSP-BLOG*, [http://ospublish.constantvzw.org/blog/news/html-sauce-cocktail-sauce-a-part](http://ospublish.constantvzw.org/blog/news/html-sauce-cocktail-sauce-a-part)
- Jiminy Panoz, "The Blitz’ introductory tutorial", *Medium*, [https://medium.com/@jiminypan/blitzintrotutorial-270da9f853c0#.z93c2zcw0](https://medium.com/@jiminypan/blitzintrotutorial-270da9f853c0#.z93c2zcw0)
- Richard Pipe, "Avoid XML First Like the Plague", *Infogrid Pacific*, [http://www.infogridpacific.com/blog/igp-blog-20130210-avoid-xml-first-like-the-plague.html](http://www.infogridpacific.com/blog/igp-blog-20130210-avoid-xml-first-like-the-plague.html)
- Dudley Storey, "5 Powerful Tips And Tricks For Print Style Sheets", *Smashing Magazine*, [https://www.smashingmagazine.com/2013/03/tips-and-tricks-for-print-style-sheets/](https://www.smashingmagazine.com/2013/03/tips-and-tricks-for-print-style-sheets/)
- Frank Taillandier, "La mouvance statique", [https://frank.taillandier.me/2016/03/08/les-gestionnaires-de-contenu-statique/](http://frank.taillandier.me/2016/03/08/les-gestionnaires-de-contenu-statique/)
- Julien Taquet, "Making books with HTML + CSS — a look at Vivliostyle (Part 1 – page layouts)", *Paged Media*, [https://www.pagedmedia.org/making-book-with-html-css-a-look-at-vivliostyle-part-1-page-layouts/](https://www.pagedmedia.org/making-book-with-html-css-a-look-at-vivliostyle-part-1-page-layouts/)
- Norman Walsh, *DocBook 5.2: The Definitive Guide*, O'Reilly, [http://tdg.docbook.org/tdg/5.2/docbook.html](http://tdg.docbook.org/tdg/5.2/docbook.html)