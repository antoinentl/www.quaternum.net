---
layout: post
title: "Max Bill, Jan Tschichold : une chronologie"
date: 2015-10-06
published: true
comments: true
categories: 
- carnet
---
Lors de ma lecture de *Max Bill / Jan Tschichold. La querelle typographique des modernes*, de Hans-Rudolf Bosshard aux éditions B42, j'ai parfois manqué d'une vision claire de la chronologie des événements de cette *querelle typographique*. Voici quelques dates clés, accompagnées de citations extraites subjectivement, permettant une lecture *assistée*.

<!-- more -->

## Max Bill / Jan Tschichold. La querelle typographique des modernes

![Couverture](/images/max-bill-jan-tschichold.png)

>Dans *Qu’est-ce qu’un designer*, Norman Potter regrettait que si peu d’architectes ne connaissent le conflit qui opposa, dans les années 1940, l’artiste Max Bill et le typographe Jan Tschichold à propos du design graphique et de la typographie. On pourrait sûrement, dans l’Hexagone, élargir son regret aux designers, cette discorde n’ayant été que très rarement évoquée dans les ouvrages sur l’histoire des arts appliqués en langue française.
En publiant *Max Bill / Jan Tschichold. La querelle typographique des modernes*, les éditions B42 vont certainement contribuer à rendre cette affaire moins confidentielle.  
[Charles Gautier, Max Bill / Jan Tschichold, La querelle typographique des modernes](http://strabic.fr/Max-Bill-Jan-Tschichold)

<h2 id="chronologie">Chronologie</h2>

[1925](#1925) -- [1928](#1928) -- [1930](#1930) -- [1933](#1933) -- [1935](#1935) -- [1938](#1938) -- [1945](#1945) -- [1946](#1946) -- [1948](#1948) -- [1967](#1967) -- [1987](#1987)


<h3 id="1925">1925, elementare typographie <a href="#chronologie">↑</a></h3>
Hors-série *elementare typographie* du périodique Typographische Mitteilungen de Jan Tschichold.

<h3 id="1928">1928, Die neue Typographie <a href="#chronologie">↑</a></h3>
Publication du livre de Jan Tschichold, *Die neue Typographie*.

<h3 id="1930">1930, Qu’est ce que la Nouvelle Typographie et que veut-elle&nbsp;?&nbsp;<a href="#chronologie">↑</a></h3>
Publication de l'article *Qu’est ce que la Nouvelle Typographie et que veut-elle&nbsp;?* dans la revue AMG, également disponible sur le site [signes](http://signes.org/page.php?id=16-jan-tschichold-em-qu-est-ce-que-la-nouvelle-typographie-et-que-veut-elle-em-amg-n-19-septembre-1930).  

>Le mode d’action du nouveau typographe repose sur une claire perception du but à atteindre et de la meilleure voie pour y parvenir.

<h3 id="1933">1933, incarcération de Jan Tschichold par les nazis <a href="#chronologie">↑</a></h3>
Dix jours après que les nazis aient pris le pouvoir, [Tschichold et sa femme sont arrêtés](https://en.wikipedia.org/wiki/Jan_Tschichold#Life).

<h3 id="1935">1935, Typographische Gestaltung <a href="#chronologie">↑</a></h3>
Jan Tschichold semble faire un retour à la typographie traditionnelle dans *Typographische Gestaltung*.

<h3 id="1938">1938, conversion de Jan Tschichold <a href="#chronologie">↑</a></h3>
Conversion de Jan Tschichold pour la typographie axiale, symétrie axiale, typographie traditionnelle.

<h3 id="1945">1945, Konstanten der Typographie <a href="#chronologie">↑</a></h3>
Conférence de Jan Tschichold, "Konstanten der Typographie" — "Constantes de la typographie".

<h3 id="1946">1946, über typografie <a href="#chronologie">↑</a></h3>
Publication de l'article "über typografie" — de la typographie — de Max Bill dans la revue *SGM* — *Schweizer graphische Mitteilungen*.

>dans nulle autre corporation, liée aux arts appliqués, que la typographie, il n'y a autant de règles antérieures à la conception. ce substrat fondamental définit la nature de la typographie.

### 1946, Mythe et réalité <a href="#chronologie">↑</a>
Publication de l'article "Mythe et réalité" de Jan Tschichold dans la revue *SGM* — *Schweizer graphische Mitteilungen*.  

>De nombreux problèmes (en particulier la conception de livres) sont bien trop compliqués pour être résolus par les procédés réducteurs de la Nouvelle typographie. Dès que le créateur qui dirige l'ensemble de la composition ne peut plus disposer en permanence de chaque page sous les yeux et régler lui-même chacun des plus petits problèmes formels, le caractère extrêmement subjectif de la Nouvelle typographie menace grandement l'unité du travail qu'il recherche.

>L'innovation et la conception d'une forme surprenante sont possibles dans peu de livres&nbsp;; pour les autres, elles sont ressenties comme une gêne et importunent les autres.

<h3 id="1948">1948, Über moderne Typographie <a href="#chronologie">↑</a></h3>
Publication de l'article ["Über moderne Typographie" — "De la typographie moderne" — de Paul Renner](http://from.esad-gv.fr/to/#37:De%20la%20typographie%20moderne) dans la revue *SGM* — *Schweizer graphische Mitteilungen*.  

>Des controverses telles que celle qui oppose Bill and Tschichold (dans les numéros 5 et 6 de 1946 de cette revue) sont donc instructives; leurs opinions contradictoires illustrent les tensions qui sont celles de la typographie de notre époque, et par conséquent il ne serait pas avisé de prendre parti précipitamment.

<h3 id="1967">1967~1974, rencontre entre Max Bill et Jan Tschichold <a href="#chronologie">↑</a></h3>
Jan Tschichold et Max Bill [se rencontrent à Berzona, en Suisse](http://typographica.org/typography-books/max-bill-kontra-jan-tschichold-der-typographiestreit-der-moderne/).

<h3 id="1987">1987, réimpression de "La nouvelle typographie" <a href="#chronologie">↑</a></h3>
*La nouvelle typographie* est réimprimée, Jan Tschichold y écrit la préface&nbsp;:

>Parfois, les auteurs doivent être protégés de leur autodestruction.


## Sources

- *Max Bill / Jan Tschichold. La querelle typographique des modernes* aux [éditions B42](http://www.editions-b42.com/books/max-bill-jan-tschichold/)&nbsp;;
- une chronique de *Max Bill / Jan Tschichold. La querelle typographique des modernes* par Charles Gautier sur [Strabic](http://strabic.fr/Max-Bill-Jan-Tschichold)&nbsp;;
- une chronique de *Max Bill kontra Jan Tschichold: Der Typografiestreit der Moderne* par Maurice Meilleur sur [Typographica](http://typographica.org/typography-books/max-bill-kontra-jan-tschichold-der-typographiestreit-der-moderne/)&nbsp;;
- ["Über moderne Typographie" — "De la typographie moderne" — de Paul Renner](http://from.esad-gv.fr/to/#37:De%20la%20typographie%20moderne), une traduction inédite disponible sur le site From—to&nbsp;;
- ["La typographie asymétrique de Jan Tschichold"](http://typographisme.net/post/La-typographie-asym%C3%A9trique-de-Jan-Tschichold) par Anne-Sophie Fradier sur le site Typographisme.