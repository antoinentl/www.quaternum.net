---
comments: true
date: 2013-03-22
layout: post
slug: le-web-et-les-flux
title: Le Web et les flux
categories:
- carnet
---
>Les formes informent le sens qui informe les formes.  
[Annick Lantenois, Le vertige du Funambule](http://www.editions-b42.com/books/le-vertige-du-funambule/)

Ces temps-ci beaucoup de questions sur [la lecture du Web](https://larlet.fr/david/blog/2013/navigateur-contenus/ "Lien vers le blog de David Larlet"), [de l'évolution de nos pratiques, des formats et des outils](http://esquisses.clochix.net/2013/03/17/navigateur-contenus-personnels/ "Lien vers Clochix"). Des interrogations aussi sur [la forme](http://emmanuel.clement.free.fr/blog/index.php/post/2013/03/05/epub-ou-html "Lien vers le blog d'Emmanuel") des livres *numériques*, tout cela est lié et ce n'est pas une coïncidence... Moi-même je redécouvre le Web avec [un lecteur de flux RSS](http://lzone.de/liferea/ "Lien vers Liferea") sous forme de logiciel et non d'application web, pour faire le choix du confort de la lecture hors connexion. Mais [où sont d'ailleurs les données](http://www.christian-faure.net/2013/03/21/lhypercriticite-numerique-de-la-france/ "Lien vers le carnet de Christian Fauré")&nbsp;?

Qu'est-ce que le flux sans capacité de le lire&nbsp;?