---
comments: true
date: 2012-10-25
layout: post
slug: dynamiques
title: Dynamiques
categories:
- carnet
---
>La forme d'un A ne saurait être changée selon l'impératif d'une machine déjà existante ou susceptible d'être construite.  
[Eric Gill, Un essai sur la typographie, Ypsilon éditeur, page 16](http://www.ypsilonediteur.com/fiche.php?id=104)

Avant l'arrivée de @font-face et des webfonts, l'usage des images pour la typographie sur le Web était fréquent : pour des lettres ou des groupes de mots. En somme des alternatives aux limites posées par les fonts installées par défaut sur les ordinateurs. Il ne s'agissait alors pas de typographie, la typographie est par essence dynamique, elle est une composition d'éléments modifiables, non figés. La typographie est vivante, ce que les évolutions numériques ne font que confirmer. Malheureusement certains sites web, notamment des sites de belles revues papier, utilisent encore ce procédé, que ce soit pour des titres, ou pire pour du texte "de labeur". Signe d'un clivage encore trop fort entre papier et numérique - mais pas forcement l'inverse.
Enfin, si le numérique - dans les modes de diffusion et non dans les modes de production - souligne le caractère dynamique de la typographie, qu'elles pourront être les prochaines évolutions, au-delà de @font-face ou des webfonts, au-delà d'une propriété de CSS ou d'un service ?