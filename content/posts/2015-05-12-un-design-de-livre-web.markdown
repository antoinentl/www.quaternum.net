---
layout: post
title: "Un design de livre web"
date: 2015-05-12
comments: true
published: true
desc: "Quel pourrait être le design d'un livre web - un livre sous la forme d'un site web ? Le récent book site de Donny Truong, Professional Web Typography, semble être un très bon exemple de la structuration et du design d'un livre numérique, en version web."
keywords: "livre, web, livre numérique, typographie, Donny Truong, Professional Web Typography"
categories: 
- carnet
---
Quel pourrait être le design d'un *livre web* -- un livre sous la forme d'un site web&nbsp;? Le récent *book site* de Donny Truong, *Professional Web Typography*, semble être un très bon exemple de la structuration et du design d'un livre numérique, en version web.

<!-- more -->

## Professional Web Typography
C'est par le biais de [la veille de David](https://larlet.fr/david/stream/2015/) que j'ai découvert il y a quelques jours le livre de Donny Truong, [Professional Web Typography](https://prowebtype.com/). Je ne m'attarderais pas sur le contenu, ne l'ayant pas encore lu, mais c'est un exemple de *livre web* ou de *livre site* -- *book site* en anglais&nbsp;: tout le contenu est en ligne, et le design offre une lecture sur écran très confortable. Il n'y a ni version EPUB, ni version imprimée, pour l'instant le site se suffit à lui-même.

## Structuration
Le site *Professional Web Typography* se structure en quatre zones principales&nbsp;:

- une zone de titre, avec le titre du livre&nbsp;;
- une zone de contenu, avec le titre du chapitre, et les contenus correspondant -- ici du texte, du code et des images&nbsp;;
- une zone de navigation, avec des liens vers les chapitres suivant ou précédent&nbsp;;
- une zone de sommaire, avec l'ensemble de la table des matières -- chapitres et sous-chapitres.

![Capture d'écran de Professional Web Typography](/images/prowebtype-introduction.png)  

Le site est donc d'une grande simplicité en terme de structure, ce qui lui permet notamment de s'adapter naturellement aux différentes tailles d'écran. Un seul point de rupture est prévu pour que le site ait deux versions en fonction des largeurs des dispositifs de lecture, l'adaptation se faisant sur la taille des polices -- réduite sur les écrans plus petits -- et la table des matières en pied de page -- de deux à une colonne. Le texte est *liquide*, la lecture se fait donc parfaitement sur des grands comme des petits écrans. On pourrait donc qualifier ce site d'[adaptatif *et* de responsif](http://blog.uxpin.com/6439/responsive-vs-adaptive-design-whats-best-choice-designers/).  

Chaque élément de la structure est clairement identifié et identifiable, le temps d'appropriation de cette application de lecture est -- selon moi -- de l'ordre de l'immédiateté.

## Navigation
Les principaux éléments de navigation&nbsp;:

- le titre du livre web -- la première partie de la structure du site -- qui est un lien vers la page d'accueil, cette dernière faisant office de couverture et de quatrième de couverture avec trois critiques. Un article complet pourrait d'ailleurs être consacré à [cette couverture](https://prowebtype.com/), très intéressante en terme de démarche éditoriale et de design graphique&nbsp;;
- des liens à la fin du contenu et avant le pied de page -- la troisième partie de la structure du site -- permettant de passer aux chapitres précédent ou suivant. L'équivalent des pages précédentes et pages suivantes d'une application de lecture numérique, contextualisé&nbsp;;
- le sommaire ou table des matières -- la troisième partie de la structure du site.

L'une des grandes originalités de ce *livre web*, c'est justement cette table des matières en pied de page, et non dans un menu dédié comme c'est souvent le cas. Les menus habituels sont encore majoritairement placés dans le `header` -- dans la partie supérieure du site, avant le contenu -- avec une adaptation en *menu hamburger* -- les [fameuses](http://www.simpleweb.fr/2014/07/29/faut-il-abandonner-les-menus-facon-hamburger/) trois barres horizontales -- pour des écrans plus petits. L'accès aux différents chapitres et sous-chapitres se fait très facilement, et quoi qu'il arrive l'arborescence est toujours visible.  

En terme de design et d'ergonomie, ce choix de la simplicité est à mon avis le bon. Limiter les subterfuges dynamiques -- menu sandwich, éléments de navigation qui n'apparaissent qu'au survol, introduction d'interactions issues du tactile -- permet une grande accessibilité de l'outil, proche de celle d'un livre physique, papier.

## Le design de book site, un work in progress
Cet exemple n'est pas le premier, les tentatives de design de livres sous la forme de sites web sont nombreuses. Mais celle-là semble se démarquer par sa simplicité, et par cet accès à la table des matières en pied de page.  

Parmi quelques exemples, on peut citer&nbsp;:

- d'autres livres sur la typographie et le web&nbsp;:
  + *Practical Typography* de Matthew Butterick&nbsp;: [practicaltypography.com](http://practicaltypography.com)&nbsp;;
  + *The Elements of Typographic Style Applied to the Web* de Richard Rutter&nbsp;: [webtypography.net](http://webtypography.net)&nbsp;;
- la thèse d'Anthony Masure, [Le design des programmes, Des façons de faire du numérique](http://www.softphd.com/these/introduction/desordre), avec notamment des options de lecture intégrées dans l'application web&nbsp;;
- les livres numériques de [Pelican Books](https://www.pelicanbooks.com/), en *full web* et en accès payant&nbsp;;
- *Design et Architecture de l'ebook* de Thibault Mahé, avec une structure très semblable à celle du livre numérique au format EPUB, malheureusement [le projet web](http://thibault.mahe.io/journal/article20/introduction-au-design-et-a-l-architecture-de-l-ebook) n'est plus visible en ligne&nbsp;;
- interfaçage du site de [Thierry Crouzet](http://blog.tcrouzet.com/), sur la base du rouleau&nbsp;: une fois sur un article, la page peut se dérouler indéfiniment&nbsp;;
- [Radius](http://www.radius-experience.com/), une expérimentation de la maison d'édition Walrus&nbsp;;
- [Jiminy Panoz](http://jiminy.chapalpanoz.com/) signale régulièrement des intiatives de *livre-web* -- "books in browsers".

## Éloge de la simplicité
Les expérimentations les plus récentes, comme *Web Professional Typography*, sont, à mon sens, celles qui fonctionnent le mieux, car elles sont les plus simples en terme d'accès aux contenus et d'usage en terme de lecture. Aller vers la simplification, avec en tête cet adage "Less is more", n'est pas une chose facile en matière d'architecture de l'information et de design graphique -- ["Less is more (difficult)"](https://twitter.com/jcolman/status/577937669679763456).  

La cohérence, le graphisme et le dispositif de navigation de *Web Professional Typography* sont fascinants de simplicité et d'ingéniosité, et ne peut qu'inviter à la lecture.