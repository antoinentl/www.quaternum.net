---
layout: post
title: "Book as a service"
date: 2015-03-10
comments: true
published: true
categories: 
- carnet
---
Le livre numérique n'est-il pas condamné à n'être qu'un service&nbsp;? Le livre numérique n'est-il pas amené à être plus proche du web que du livre papier&nbsp;?

<!-- more -->

## SaaS et CJUE
"Book as a service" est une référence à l'expression [SaaS](https://fr.wikipedia.org/wiki/Saas), *Software as a Service*, et cela concerne tous les services et logiciels en ligne que nous utilisons au quotidien. Ce ne sont plus nos machines qui *contiennent* le service mais des serveurs auxquels nous accèdons à distance, le plus souvent via un navigateur web. Pour l'utilisateur cela ne change pas grand chose, c'est même plus simple en terme de mises à jour et de fonctionnement.  
L'expression "Book as a service" a été utilisée dès 2011 pour définir les services de *streaming* ou d'abonnement de livre numérique, [sur Tools of Change for Publishing](http://toc.oreilly.com/2011/07/books-as-a-service.html).  
"Book as a service" est également une référence au récent avis de la CJUE (Cour de justice de l'Union européenne) concernant le taux de TVA du livre numérique, rendu jeudi 5 mars 2015. Ce ne sera donc pas 5,5% de TVA pour le livre numérique comme c'est le cas actuellement en France, mais 20%, car le livre numérique est considéré "comme un service" par l'Union européenne, alors qu'en France le livre numérique est considéré comme un livre, et donc comme un bien — je fais court.

## Ebook as a service ?
"Book as a service" et non "Ebook as a service", car je ne pense pas que l'enjeu se passe réellement du côté de l'*ebook* — *livre numérique* en français — mais bien du côté du livre en général. Je prends pour exemple mes lectures du moment :

- *To be continued* : une bande dessinée en ligne, peut-être est-ce une "bande dessinée numérique", en tout cas c'est la première fois que je vois des auteurs prendrent réellement en compte le médium web, dans sa forme. S'il y a un jour une édition papier ce sera forcément déceptif ou illisible. Ça se passe par ici pour la lecture : [tobecontinuedcomic.com](http://tobecontinuedcomic.com/) ;
- *Le petit bossu* de Roberto Arlt : une série de nouvelles, éditée au format papier par Cent pages, comme d'habitude avec Cent pages c'est surprenant et superbe, et je ne parle pas du texte incroyable ;
- *À quoi tient le design* de Damien Huygue : il s'agit d'un coffret de six fascicules au format papier, c'est un essai publié chez [De L'Incidence Éditeur](http://www.delincidenceediteur.fr/#!a-quoi-tient-le-design/c2ct). Je commence timidement la lecture, préférant attendre la version numérique qui devrait me permettre une lecture active (avec [Addr](http://getaddr.co/), j'en reparlerai) comprenant des notes.

## L'intention et le social
Il n'y a lecture que s'il y a *intention de lecture*. Ces trois livres me sont arrivés et restés dans les mains pour des raisons très diverses, mais au final c'est bien parce que j'ai une intention particulière pour chacun de ces *livres*. Et ces lectures me procurent du plaisir, autrement je ne les lirais pas. Dans l'ordre, j'ai trouvé ces livres :

- en ligne via [Radio GRAND PAPIER](http://radio.grandpapier.org/6-To-Be-Continued-Comic-Book) ;
- en librairie sur un conseil, même si je connais déjà le travail des éditions Cent pages ;
- en discutant avec [Anthony Masure](http://www.anthonymasure.com/) et l'éditeur (un peu par hasard).

Mon activité de lecture est profondement sociale, déjà dans l'acquisition du livre, mais aussi et surtout sur ce qui se passe pendant et après mes lectures. J'ai offert une fois un livre numérique, un *ebook*. J'avais soigneusement supprimé les mentions de mon nom et de mon adresse électronique, dûes au *watermarking*, du fichier EPUB. En revanche j'ai déjà partagé, ou je vais partager, les trois livres ci-dessus. Sans difficulté.

## As a service
Je m'inquiète de la scission entre l'industrie du livre et des pratiques de lecture qui dépasse largement le livre et concerne plutôt le web. Consulter un site web n'est peut-être pas une pratique similaire à la lecture d'un livre, mais pourtant il y a aussi ces notions d'intention et de pratique sociale.  
Je n'achète presque plus de fichiers EPUB, et ce n'est pas parce que je ne souhaite pas payer, bien au contraire comme le souligne par exemple [David à propos de la musique](https://larlet.fr/david/stream/2015/02/23/). Peut-être plutôt pour des raisons d'usage, d'accès aux contenus. Le web est un service, quel que soit le contenu.  
Cet avis de la CJUE n'est peut-être pas une mauvaise chose en soi, justement parce que le livre peut être autre chose qu'un format papier ou un format EPUB.