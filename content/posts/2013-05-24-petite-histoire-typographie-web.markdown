---
layout: post
title: "Petite histoire de la typographie sur le Web"
slug: petite-histoire-typographie-web
date: 2013-05-24
comments: true
categories:
- carnet
---
>La forme typographique permet certes de rendre le langage et la pensée concrète, mais curieusement elle est le résultat d’une abstraction visuelle.  
[Jean-Baptiste Levée, Livraison 13, Typographie et Langage, page 152](http://www.revuelivraison.org/Livraison13/Accueil-liv13.html)

Une question me revient [régulièrement](https://www.quaternum.net/2012/08/23/lecture-numerique-et-integrale/ "Lien vers quaternum.net"), notamment en *ouvrant* des fichiers EPUB&nbsp;: est-ce qu'il est pertinent d'embarquer les caractères/fontes utilisées dans un livre numérique&nbsp;? Car si une fonte n'est pas *embarquée*, alors si le support de lecture ne la possède pas, la police affichée ne sera pas celle prévue - si tant est qu'une police était prévue par l'éditeur.  
Une fonte embarquée peut par ailleurs poser le problème de la récupération de celle-ci par l'utilisateur&nbsp;: un fichier EPUB s'ouvre comme un dossier *zippé*, et donc les fichiers correspondant à une fonte peuvent être très facilement copiés - c'est un autre sujet à appronfondir, sur le terrain des licences.  

Avant d'aller plus loin dans cette interrogation, prenons le Web, le grand frère du livre numérique en terme de format et d'interopérabilité, et relevons trois temps pour la typographie sur le Web&nbsp;:

## Un choix limité
Jusqu'à la moitié de la première décennie du vingt-et-unième siècle&nbsp;: limitation aux fontes installées sur le support de lecture - ordinateur - utilisé&nbsp;: Times New Roman, Arial, Georgia, Comic Sans... Frustration typographique pour tous le monde&nbsp;: à la fois ceux qui publient et ceux qui consultent et lisent&nbsp;; et tendance à utiliser autre chose que du texte&nbsp;: titres sous forme d'images par exemple, voir abandon du HTML et utilisation de Flash ou carrément du PDF pour des choix typographiques originaux.

## @font-face
Arrivée de [@font-face](https://developer.mozilla.org/fr/docs/CSS/@font-face "lien vers Mozilla") un peu avant 2010&nbsp;: possibilité d'utiliser - via des indications dans la feuille de style - n'importe quelle fonte à condition quelle soit hébergée quelque part, sous des formats divers comme TTF, WOFF, SVG, EOT... Facilité d'utilisation, démocratisation typographique du Web, HTML et Web décomplexé face à d'autres formats - propriétaires et non-interopérable - face au papier - au moins au niveau typographique. Si on regarde concrètement comment cela s'est passé il faut bien reconnaître que - comme souvent avec les évolutions technologiques du Web - cela a pris du temps pour que tous les navigateurs implémantent cela correctement. Aujourd'hui encore certains navigateurs mobiles ne prennent pas vraiment en compte @font-face.  
Je me souviens encore de mon étonnement il y a quelques années en découvrant certains titrages aux polices audacieuses et qui m'étaient inconnues. Un clic droit sur ces titres à la typographie inédite, il ne s'agissait pas d'une image, incroyable&nbsp;!  
@font-face - pourtant imaginé il y a bien longtemps - peine à être utilisé face aux web fonts, et c'est fort dommage.

## Web fonts
Les web fonts sont une utilisation *masquée* de @font-face. La fonte est hébergée ailleurs, par un service tiers, et l'intégration dans un site est relativement simple&nbsp;: quelques lignes de code, une pincée de JavaScript, et le tour est joué. Pas la peine de s'inquiéter de savoir si tous les formats - pour tous les navigateurs - sont là, le service se charge de tout. Les services les plus connus sont [Google Fonts](http://www.google.com/fonts/ "Lien vers Google Fonts") ou [Typekit](https://typekit.com/ "Lien vers Typekit") d'Adobe.
Démocratisation encore plus forte du fait de la simplicité d'utilisation, mais questions de la pérennité et du contrôle&nbsp;: problème si Google décide de ne pas inclure le format compatible avec tel navigateur, re-problème si Typekit change le prix du service...

L'évolution de l'intégration de la typographie sur le Web ne serait-elle pas à rapprocher [des pratiques amateurs](http://delure.org/Rencontres-de-Lure-2013-Avis-aux.html "Lien vers Delure.org") en la matière&nbsp;?

Cette présentation synthétique mérite surement des précisions et des ajouts&nbsp;!