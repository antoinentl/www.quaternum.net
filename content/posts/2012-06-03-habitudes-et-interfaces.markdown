---
comments: true
date: 2012-06-03
layout: post
slug: habitudes-et-interfaces
title: Habitudes et interfaces
categories:
- carnet
---
Je découvre le lecteur de flux RSS [Leed](http://blog.idleman.fr/?p=1233 "Lien vers le blog d'idleman") (application Web à installer sur son serveur), et j'en profite pour changer des habitudes récemment installées.

Le choix des outils que l'on utilise est complexe et dépend de nombreux paramètres, dans mon cas certains de ceux-ci sont listés [ici](https://www.quaternum.net/2012/03/26/des-exigences-dusage/ "Lien vers quaternum"). Il y en a plusieurs que je n'ai pas encore exposés et qui sont pourtant déterminants&nbsp;: ergonomie, adéquation entre les fonctionnalités requises et l'interface, façon dont l'outil a été pensé. [KISS](http://fr.wikipedia.org/wiki/Principe_KISS "Lien vers Wikipédia") est une bonne façon d'aborder les choses à ce sujet. En terme d'interface, celles qui sont les plus réussies sont celles que l'on oublie assez vite.

Lorsque je suis passé de Google Reader à [rsslounge](http://rsslounge.aditu.de/), j'ai tout d'abord abandonné l'interface proposée par Google, mais j'ai aussi redécouvert l'utilisation intensive d'un lecteur de flux RSS. Pour ces différentes raisons ma lecture du Web a donc été fortement modifiée (tant en terme d'approche que de lecture de fond).

En testant Leed, le lecteur de flux RSS créé et développé par [Idleman](http://blog.idleman.fr), je bouleverse une fois de plus ces habitudes récemment acquises. Je sens que ce n'est pas si simple de modifier les réflexes et les pratiques, pourtant vieilles que de quelques mois.

Il est bon de remettre en cause nos habitudes et de garder constamment un esprit d'ouverture à d'autres environnements et fonctionnements.