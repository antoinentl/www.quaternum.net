---
layout: post
title: "Dette technique"
date: "2016-02-24T22:00:00"
comments: true
published: true
categories: 
- carnet
---
La *dette technique* est un concept incontournable dans l'environnement dans lequel nous vivons, technologique et numérique. Le livre de Bastien Jaillot, *La dette technique*, aborde cette question appliquée aux projets web, et permet d'entrevoir un ensemble de bonnes pratiques liées à la gestion de tout projet.

<!-- more -->

## Le concept de "dette technique"
Le concept de "dette technique" n'est pas neuf, il est apparu au début des années 2000 grâce à [Ward Cunningham](https://fr.wikipedia.org/wiki/Ward_Cunningham). La définition de Bastien Jaillot est claire et synthétique :

>En construisant un projet Web, de nombreux choix sont effectués, et ceux-ci, combinés à leurs implémentations, ont un impact sur le cycle de vie de votre projet.  
Cela s’appelle **la dette technique** : l’accumulation des risques pris lors des différentes phases techniques tout au long de la vie d’un projet.  
Bastien Jaillot, La dette technique

Pour prendre un exemple concret, un projet web développé il y a quelques années avec une technologie qui n'est plus maintenue aujourd'hui risque de poser un certain nombre de problèmes : compatibilité (côtés client et serveur), sécurité, évolution, etc. Tout projet contracte une dette technique plus ou moins importante et plus ou moins rapidement, c'est inévitable.

Prendre conscience de la *dette technique*, c'est envisager les choses sur le moyen et le long terme, et pas uniquement sur le court terme. La dette technique, c'est également prévoir le cas où un projet sera repris par une personne ou une équipe qui n'en était pas à l'origine : comment les composants seront compris et interprétés ? Est-ce qu'il y a une documentation ? Est-ce que tous les problèmes ont été résolus au moment de la livraison ? Etc.


## Le livre de Bastien Jaillot
*La dette technique* est un livre de Bastien Jaillot publié par Les Contrôleurs du train de 13h37 en décembre 2015, [et disponible aux formats papier et numérique (EPUB)](http://boutique.letrainde13h37.fr/products/la-dette-technique-bastien-jaillot). Il est focalisé sur les projets web mais peut s'appliquer à beaucoup d'autres domaines techniques.  
Précision importante : le livre n'est pas réservé aux techniciens, il peut être lu par toute personne curieuse.

Le livre s'articule autour de trois parties, Bastien Jaillot les résument très bien :

>Un surnom m’a un jour été donné : "Pompier du code". J’aime cette notion. Un pompier c’est 50 % d’entraînement, 40 % de prévention et 10 % d’opération de secours. Ça correspond bien à ce livre, qui a pour but de sensibiliser à la dette technique et de donner des pistes pour la prévenir.

Les trois parties de ce livre sont donc les suivantes :

1. Identifier : définition de la *dette technique*, historique, identification de la dette technique et impacts
2. Prévenir : pourquoi, comment et la question de la durée
3. Résoudre : évaluation et moyens d'action

Vous travaillez dans un domaine technique ? Vous produisez des objets ou du code ? **Lisez ce livre !**

Quelques citations :

>Les décideurs ne comprennent que trop peu les enjeux techniques de la qualité (quand ils s’y intéressent). Sachant que la dette technique est inévitable, où placer le curseur de la qualité minimale acceptable ?

>Nous avons vu que personne n’est indispensable et qu’il ne faut pas essayer de le devenir, c’est aussi mauvais pour vous que pour le projet.  
Autant partir de ce principe dès le début et planifier en conséquence.

>Le point essentiel est donc de savoir gérer l’échec dans un projet. Ne pas le préparer revient à le garantir, car – [Loi de Murphy](http://fr.wikipedia.org/wiki/Loi_de_Murphy) aidant – vous pouvez être certain que le pire scénario finira par se produire. Évitez de ne compter que sur votre bonne étoile.

>Le système est opérationnel et on ne sait pas pourquoi ?  
Parfait, il est temps de tout documenter. Voici une liste non-exhaustive des questions à se poser :  
[je vous laisse découvrir cette liste en vous procurant le livre !]

À lire également, [le billet de Gaël Poupard sur le livre de Bastien Jaillot](http://www.ffoodd.fr/la-dette-technique/).

## Il ne s'agit pas que de web
Le concept de *dette technique* semble incontournable pour le développement web, aujourd'hui. Mais il ne concerne pas que le web. Un peu à la manière de [6/5](https://www.quaternum.net/2016/01/18/6-5/) aux éditions Zones sensibles, qui abordent la question du trading à haute fréquence, *La dette technique* de Bastien Jaillot devrait être lu par toute personne curieuse du monde dans lequel nous vivons aujourd'hui.

Dans le domaine du livre numérique, deux *développeurs de livres numériques* se sont penchés sur cette question de *dette technique* : Joël Faucilhon pour Lekti et Jiminy Panoz pour Chapal&Panoz.

>En observant les ePubs produits de manière industrielle dans les pays à bas coût, il apparaît clairement que la qualité du code produit – un ePub n'est rien d'autre que du code HTML et des CSS, à l'égal d'un site Internet – va empêcher une maintenance aisée au fur et à mesure des années, même pour effectuer des modifications mineures.  
[Lekti, Production de livres numériques (ePub) et dette technique](http://www.lekti-ecriture.com/bloc-notes/index.php/post/2016/La-production-de-livres-num%C3%A9riques-est-elle-homog%C3%A8ne)

>[...] la dette technique grossit de manière exponentielle depuis des années et on peut craindre qu’elle ne finisse par faire imploser le système.  
[Jiminy Panoz, Aperçu de Kindle Previewer 3](http://jiminy.chapalpanoz.com/kindle-previewer-3/)

Le livre numérique est un domaine relativement jeune, qui se développe dans un écosystème complexe -- en partie celui du livre --, mais qui pose également la question de la *dette technique*.

## Questionner la gestion de projet
Dans le *monde du web*, on constate que certains projets sont des suites de délégations : les intermédiaires s'accumulent pour tenter d'optimiser des coûts de développement. Qui porte la responsabilité lorsque les intervenants sont trop nombreux ? Quels sont les gains réels sur le moyen ou le long terme si les choix techniques ne sont pas pertinents et la qualité du développement absente ?  
Ce sont des décisions stratégiques décisives, qui peuvent représenter des enjeux humains, financiers et d'image importants.

>La dette technique est une notion impérative à connaître pour quiconque participe, de près ou de loin, à un projet Web.

Pour vous procurer le livre *La dette technique* de Bastien Jaillot, rendez-vous sur Le train de 13h37 : [http://boutique.letrainde13h37.fr/products/la-dette-technique-bastien-jaillot](http://boutique.letrainde13h37.fr/products/la-dette-technique-bastien-jaillot).