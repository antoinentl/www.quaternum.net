---
layout: post
title: "Le fanzinat des gens du web"
date: "2016-06-02T11:00:00"
comments: true
published: true
description: "Le scénario semble inéluctable&nbsp;: les réseaux sociaux ont presque remplacé les blogs et Medium semble être une place privilégiée pour partager ses expériences autour du web. Et pourtant certains acteurs du web – développeurs, graphistes, designers, sysadmin, etc. – continuent d'alimenter des carnets web, en privilégiant le fait maison et un modèle distribué, un peu à la manière des fanzines des années 1990."
categories: 
- carnet
---
Voici le texte augmenté de mon *lightning talk* de cinq minutes à l'occasion de [Sud Web 2016](https://sudweb.fr/2016/), sur le thème des blogs et carnets web des acteurs du web, un manifeste pour des espaces de publication indépendants, artisanaux, créatifs et ouverts&nbsp;!

<!-- more -->

- [0. Introduction](#0-introduction)
- [1. L’histoire du web est une histoire de publication](#1-lhistoire-du-web-est-une-histoire-de-publication)
- [2. Medium, un cas à part](#2-medium-un-cas-à-part)
- [3. Des alternatives qui perdurent](#3-des-alternatives-qui-perdurent)
- [4. Les enjeux des carnets web fabriqués maison](#4-les-enjeux-des-carnets-web-fabriqués-maison)
- [5. Restons créatifs, expérimentons dans nos carnets&nbsp;!](#5-restons-créatifs-exprimentons-dans-nos-carnets-)
- [6. Support de présentation et vidéo](#6-support-de-présentation-et-vidéo)


## 0. Introduction

### Constat : effet de centralisation et d'homogénéisation
Le scénario est inéluctable&nbsp;! irréversible&nbsp;? consternant peut-être&nbsp;: en dix ans les réseaux sociaux ont presque remplacé les blogs, et Medium (la plateforme créée par des fondateurs de Twitter) semble être une place privilégiée pour partager ses expériences autour du web. Nous assistons à une centralisation et à une homogénéisation de la publication sur le web.

### Mais : des alternatives subsistent
Malgré ces constats sombres pour le web décentralisé, certains *acteurs du web* -- développeurs, graphistes, designers, *sysadmin*, etc. -- continuent d'alimenter des blogs ou carnets ou de créer des livres web, en privilégiant le *fait maison* et un modèle *distribué*. Un peu à la manière des fanzines des années 1990 -- libres et indépendants.

### Les carnets web, le fanzinat du web ?

![6 cases de bande dessinée représentant les étapes de fabrication d'un fanzine : photocopie, pliage](/images/2016-fabrication-fanzines.png)
*Pierre Maurel, Blackbird, [L'employé du Moi](http://employe-du-moi.org/Blackbird)*

Pour celles et ceux qui ont moins de <strike>20</strike> 30 ans, les années 80 et 90 ont été bouleversées par un processus d'impression accessible&nbsp;: **la photocopieuse**. D'abord plébiscité par le mouvement punk, puis par la scène musicale underground des années 80, le fanzine est aujourd'hui encore utilisé pour faire circuler des idées, ou pour s'exprimer -- notamment par la scène indépendante de la bande dessinée. Le fanzine est un objet imprimé, souvent en noir et blanc, et agrafé et/ou plié. **Économique**, **léger** et **reproductible**, il est surtout un potentiel de **créativité**.

>Le fanzine n'est qu'un moyen/outil accessible.

### Quelles pratiques chez les acteurs du web ?
Ce qui m'intéresse c'est d'observer les pratiques d'écriture et de publication des gens qui font le web&nbsp;: graphistes, développeurs, chefs de projet, consultants, administrateurs système, etc. Cet écosystème est problablement représentatif, dans une certaine mesure, des pratiques de toutes et tous.


## 1. L'histoire du web est une histoire de publication

### Un web de documents
Même s'il est aujourd'hui de plus en plus applicatif, le web est, à l'origine, un web de documents, avec l'association d'un langage sémantique (le HTML), et d'un protocole de communication (le HTTP).

Par ailleurs une fusion est en cours entre l'IDPF (équivalent du W3C pour le livre numérique et son format l'EPUB) et le W3C, un enjeu pour les [Portable Web Publications](https://www.w3.org/TR/pwp/).


### Les plateformes et les CMS, une évolution constante
Les CMS, ou gestionnaires de contenus, sont assez rapidement apparus. Ces outils (SPIP, Dotclear, Wordpress, Ghost) se sont rapidement mués en plateformes ou services (Blogger, Typepad, Wordpress, Wattpad, Medium). Wordpress est le CMS et la plateforme la plus emblématique, conçue pour les blogs mais aussi utilisés pour des sites plus ou moins complexes.

>Wordpress est la photocopieuse du web.  
Frank

![Dessin d'une photocopieuse](/images/2016-photocopieuse.png)
*Pierre Maurel, Blackbird, [L'employé du Moi](http://employe-du-moi.org/Blackbird)*

### CMS autohébergés vs plateformes : des contraintes
Installer un CMS c'est cool, mais ça demande un certain effort, y compris pour maintenir et faire évoluer l'outil -- sans parler de certains problèmes de sécurité.

Utiliser une plateforme *as a service* c'est confortable mais limité en terme de configuration et de personnalisation, et ça pose des questions de coût (vos données payent la plateforme&nbsp;?), de pérennité (que se passe-t-il si la plateforme ferme&nbsp;?), de censure, de présence de publicité... Bref de liberté et d'indépendance.

![Deux cases de bande dessinée représentant des étapes de fabrication d'un fanzine](/images/2016-fabrication-fanzines-02.png)
*Pierre Maurel, Blackbird, [L'employé du Moi](http://employe-du-moi.org/Blackbird)*


## 2. Medium, un cas à part

### Qu'est-ce que Medium&nbsp;?
Medium est une plateforme de publication en ligne qui offre une interface d'écriture très simple d'utilisation, et un design épuré qui invite à la lecture.

![Capture d'écran d'un article sur la plateforme Medium](/images/2016-medium.png)
*Capture d'écran d'un article sur la plateforme Medium, ["Making Medium More Powerful for Publishers"](https://medium.com/the-story/making-medium-more-powerful-for-publishers-39663413a904#.vyu0kgi9f), (sic)*


### Une certaine approche du design
Autant pour le rédacteur que pour le lecteur, Medium a apporté un vrai vent de fraîcheur&nbsp;: interface de lecture épurée, outil d'écriture minimaliste, vrai souci pour la micro-typographie, design élégant, on ne peut pas nier qu'il est agréable de lire sur Medium.


### Un outil de diffusion
Medium est aussi un réseau social, qui semble réussir là où Blogspot ou Wordpress avait en partie échoué. Créée par des fondateurs de Twitter, la plateforme est directement liée à ce réseau social bien connu, et propose des fonctionnalités de partage, de recommandations et d'échanges (via les commentaires et les surlignements notamment).

Là où les autres plateformes peinaient à dépasser le stade *partage*, Medium semble parvenir à être un outil de diffusion en se basant sur des principes de recommandation, de reconnaissance par les pairs, etc.

![Deux cases de bande dessinée représentant des étapes du processus de diffusion d'un fanzine](/images/2016-diffusion.png)
*Pierre Maurel, Blackbird, [L'employé du Moi](http://employe-du-moi.org/Blackbird)*

Medium n'est finalement qu'une plateforme de publication parmi d'autres, qui a l'avantage de pointer les principaux objectifs d'un outil de publication&nbsp;: l'outil d'écriture, la forme, les fonctionnalités de partage et la diffusion.


## 3. Des alternatives qui perdurent

### Quel est ce fanzinat des gens du web ?
Ce serait trop long de présenter et d'explorer les nombreux carnets ou blogs d'intégrateurs, de graphistes, d'experts, d'étudiants, de développeurs, d'hacktivistes, etc. Voici un fichier <abbr title="Outline Processor Markup Language">OPML</abbr> regroupant une soixantaine de carnets web et de blogs, vous pouvez modifier/augmenter cette liste&nbsp;:

<a href="https://github.com/antoinentl/les-carnets-des-gens-du-web" class="link-button">https://github.com/antoinentl/les-carnets-des-gens-du-web</a>


### Quelles approches éditoriales et formelles ?
Le point commun de ces carnets est de parler, plus ou moins fréquemment, de web&nbsp;! Certains parlent également de leur vie, de politique, publient des photos ou des dessins, partagent du code, des livres, présentent des projets, lancent des débats, évoquent des expériences. L'autre point commun est la connexion entre ces carnets, la plupart de ces espaces de publication individuels n'existeraient sans doute pas s'ils étaient seuls.

En terme de forme, c'est souvent minimaliste, parfois entièrement fait à la main, toujours lisible **et** surprenant&nbsp;!

### Avec quels outils ?
De l'outil développé à la main au CMS bien connu qu'est Wordpress en passant par les générateurs de site statique, il y a finalement presque autant d'outils que d'initiatives&nbsp;! Les carnets ou blogs des acteurs du web sont représentatifs de ce qu'il est possible de faire, de la diversité que peut offrir le web&nbsp;: protéiforme, ingénieux, parfois bancale, souvent *forké* et toujours en mouvement.

Une tendance tout de même&nbsp;: les *flat* CMS (Kirby) sans bases de données ([voir le retour de Julien](http://mariejulien.com/post/2016/05/05/Refonte-de-judbd.com-%3A-%C3%89tude-de-cas-%282e-partie-%3A-le-CMS%29)), et les générateurs de site statique (Jekyll) ([voir un article à ce sujet](/2016/01/09/generateur-de-site-statique-un-modele-alternatif)).

Le générateur de site statique est peut-être cette photocopieuse des années 2010&nbsp;!


## 4. Les enjeux des carnets web fabriqués maison

### L'indépendance
Ne pas dépendre d'un tiers -- sauf peut-être pour l'hébergement ou la connexion&nbsp;: liberté de forme, de temporalité, et de ton (dans une certaine mesure).

![Deux cases de bande dessinée décrivant un futur où les publications auto-éditées seraient interdites](/images/2016-independance.png)
*Pierre Maurel, Blackbird, [L'employé du Moi](http://employe-du-moi.org/Blackbird)*

### Autres logiques éditoriales
Ne pas être *que* dans l'instantanéité, il y a les temps court (instantanéité), moyen et long&nbsp;!

Croisons les sujets, varions les approches, privilégions le temps long&nbsp;!

Ne nous limitons pas à un fond blanc et du surlignage vert comme sur Medium&nbsp;! Sachons sortir (au moins en partie) des jardins fermés&nbsp;!


### Bac à sable
Un certain nombre d'acteurs du web pratique leurs carnets ou blogs à la façon d'un bac à sable&nbsp;

>I need the source code to my blogging system. That’s part of my blogging experience, being able to evolve the software.  
[Dave Winer, Blogging like it’s 1999](http://scripting.com/2015/10/07/iNeedABetterBloggingSystem.html)

>Consultant en Web Performance depuis quelques années, j’ai décidé un jour de ne pas être le cordonnier mal chaussé.  
[Boris Schapira, Un blog avec Jekyll et Codeship](https://borisschapira.com/2016/02/jekyll-codeship/)

>Starting a blog in 2004 was cool. Starting one in 2015 is anachronistic. But I found no other place to write more than 140 characters, use a versioning system and retain full ownership and control over form and content.  
[Nicolas Kayser-Bril, Why start a blog in 2015](http://blog.nkb.fr/why-start-a-blog-in-2015)

## 5. Restons créatifs, expérimentons dans nos carnets !
Il y a au moins 3 raisons pour lesquelles des développeurs, graphistes, designers, chefs de projet, intégrateurs ou consultants doivent continuer (ou peut-être commencer) à écrire dans leur propre carnet&nbsp;:

- expérimenter et tester&nbsp;;
- maîtriser et partager&nbsp;;
- s'amuser&nbsp;!

![Dessin représentant un groupe dans une pièce autour d'une photocopieuse, souriant et déclamant : "yes !"](/images/2016-final-fanzinat.png)
*Pierre Maurel, Blackbird, [L'employé du Moi](http://employe-du-moi.org/Blackbird)*

## 6. Support de présentation et vidéo
Le support utilisé lors ma présentation est disponible en ligne&nbsp;:

<a href="http://presentations.quaternum.net/le-fanzinat-des-gens-du-web/" class="link-button">Support de la présentation "Le fanzinat des gens du web"</a>

Ainsi que la vidéo&nbsp;:

<a href="https://vimeo.com/172225269" class="link-button">Vidéo du lightning talk "Le fanzinat des gens du web"</a>

J'en profite pour remercier chaleureusement la thym (l'équipe) de Sud Web, et bravo&nbsp;!