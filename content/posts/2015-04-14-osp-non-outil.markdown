---
layout: post
title: "OSP : non-outil"
date: 2015-04-14
comments: true
published: true
categories: 
- carnet
---
OSP — Open Source Publishing — a été invité à la première édition du colloque de la biennale [Exemplaires : formes et pratiques de l'édition](http://exemplaires2015.fr) à Lyon en avril 2015. L'occasion de regarder de plus près les processus mis en place par ce collectif de designers qui a choisi de n'utiliser que des outils *libres*.

<!-- more -->

## Origines d'OSP
C'est lors d'un partenariat avec Constant — une "Association pour l'Art et les Médias" dont les orientations sont "le logiciel libre, les alternatives au copyright et le (cyber)féminisme" — que des designers chargés de réaliser un travail graphique s'interrogent sur l'adéquation entre, d'un côté, leur processus de travail et leurs outils, et de l'autre les valeurs de ce partenaire et commanditaire. Travaillant avec des outils propriétaires, ces graphistes décident de tenter d'utiliser uniquement des logiciels libres -- ce que prône entre autre Constant -- dans le cadre de ce projet. OSP était né, et l'aventure continue depuis 2006.  
Le concept de [logiciel libre](https://fr.wikipedia.org/wiki/Logiciel_libre) -- free en anglais -- se réfère à quatre règles (source : Wikipédia) :

0. la liberté d'exécuter le programme, pour tous les usages ;
1.  la liberté d'étudier le fonctionnement du programme et de l'adapter à ses besoins ;
2.  la liberté de redistribuer des copies du programme (ce qui implique la possibilité aussi bien de donner que de vendre des copies) ;
3. la liberté d'améliorer le programme et de distribuer ces améliorations au public, pour en faire profiter toute la communauté.


*Open Source* est un concept qui comprend uniquement les deux premières règles. Pourquoi Open Source Publishing ne s'appelle pas "Free Publishing" ? Peut-être pour deux raisons : la première est l'ambiguité du terme *free*, et d'autant plus pour des francophones ; la seconde est que OSP crée des process -- composés d'applications --, des créations graphiques, mais peu de systèmes ou de logiciels qui pourraient être repris et modifiés.

## Les outils conditionnent nos façons de travailler
Sarah Magnan et Alexandre Leray -- membres d'OSP présents à Lyon dans le cadre du colloque Exemplaires -- interrogent les pratiques des graphistes, et constatent qu'il n'y pas vraiment d'alternative à la suite Adobe dans le cas de la mise en page. Même si les logiciels ne limitent pas la démarche et le processus de création, ils conditionnent la façon de travailler et de créer.  
En passant de logiciels propriétaires à des logiciels libres, d'autres pratiques se mettent en place. La notion d'intuitivité et les acquis sont totalement remis en cause.  

![Le pad ethertoff](/images/osp-ethertoff.png "Le pad ethertoff")  


## Un nouveau projet = de nouveaux outils
OSP adopte de nouveaux outils pour chaque nouveau projet, ou fait évoluer les précédents :

- logiciels libres comme alternatives à des logiciels propriétaires -- Gimp, Scribus, etc ;
- langages du web -- HTML et CSS ;
- applications *web* comme les *pads* -- sorte de blocs notes collectifs -- ou le wiki.
  
Pendant son intervention dans le cadre d'Exemplaires, Alexandre Leray prend l'exemple d'un projet qui a pris la forme d'un wiki -- le projet [Oral Site](http://osp.kitchen/work/oralsite.www/). Les commanditaires ont appris à utiliser le wiki en même temps que l'équipe d'OSP. Lors de la mise à jour de l'outil, OSP a proposé de simplifier certains processus : ajout de *boutons* pour remplacer des étapes de *code*. Les commanditaires ont préféré conserver ce passage par le code, pour conserver leur liberté lié à ce processus.  

![Oral Site](/images/osp-oralsite.png "Oral site")  


## Horinzontaliser les échanges
Les processus de création d'OSP, dans le cadre de commandes ou de partenariats, reposent à chaque fois sur plusieurs principes :

- implication du client ou du partenaire dans la création, notamment via des outils de suivi des versions des travaux, outils également publics -- sur le principe de *versionning* lié à [Git](https://fr.wikipedia.org/wiki/Git) ;
- apprentissage conjoint d'OSP et du client/partenaire au système mis en place pour la création. Le fait de ne pas savoir quel outil sera utilisé, et donc de ne pas maîtriser totalement celui-ci, fait partie de la démarche.

La *charte* d'OSP est disponible [en ligne](http://osp.kitchen/about).
Dans les différentes démarches d'Open Source Publishing, il y a toujours cette volonté de mettre les différents acteurs d'un projet au même niveau. Ce n'est pas la maîtrise d'un logiciel qui définit un designer, mais plutôt son approche et le dialogue qu'il peut mettre en place, et alimenter.

## Le code sans le code
Ce qui intéresse le plus les membres d'Open Source Publishing, ce sont les pratiques et les discussions autour de ces logiciels libres, et pas tant le fait que le code de ces outils soit ouvert. Il ne s'agit pas simplement d'outils, mais d'une culture et donc d'une communauté de personnes qui partagent ensemble des apprentissages, des pratiques, des découvertes.  
Les outils sont au coeur de la démarche des membres d'OSP, et pourtant ils ne sont que secondaires par rapport aux processus et aux échanges qu'ils provoquent.  
Depuis 2006 OSP a fait le choix de privilégier des processus relativement simples qui reposent de plus en plus sur le web, et de moins en moins sur des outils "rugueux" comme Gimp ou Scribus. Cela se traduit par exemple par le fait d'*imprimer du HTML* dans le cas d'affiches ou de livres. Plus récemment, OSP s'implique dans [Médor](https://medor.coop/fr/), "un magazine trimestriel belge et coopératif d'enquêtes et de récits" dont le *workflow* repose sur une plateforme développée sous licence libre.  

![Exemple d'une affiche en HTML](/images/osp-balsamine.png "Exemple d'une affiche en HTML")  


## Une démarche, pas une esthétique
Si le graphisme de certains travaux d'OSP semble porter une esthétique "libre" ou "open source", le collectif s'en défend, et ce n'est effectivement pas le cas en regardant l'ensemble des projets. En revanche le choix d'utiliser d'autres processus a plusieurs conséquences, qui font partie de la démarche de ces designers. Notamment le fait de ne pas avoir comme objectif d'obtenir des supports de communication lissés. Les commandes réalisées sont autant les résultats *tangibles* -- affiches, sites web vitrines, livres, etc --, que les outils.  
L'alternative n'est pas de remplacer des outils -- propriétaires -- par d'autres outils -- libres -- mais plutôt de modifier le *workflow*, les processus de création et de partage.

## Quelques références

- le site d'Open Source Publishing : [http://osp.kitchen](http://osp.kitchen/) ;
- [Visual Culture : Open Source Publishing, Git et le design graphique](http://strabic.fr/OSP-Visual-Culture), un article d'Anthony Masure sur Strabic ;
- informations sur la biennale Exemplaires : [http://exemplaires2015.fr](http://exemplaires2015.fr/) ;