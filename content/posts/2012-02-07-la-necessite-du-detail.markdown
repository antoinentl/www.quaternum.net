---
comments: true
date: 2012-02-07
layout: post
slug: la-necessite-du-detail
title: La nécessité du détail
categories:
- carnet
---
>Le principe général qui guide de manière concrète l'espacement entre les mots est le suivant : "Aussi grand que nécessaire, aussi petit que possible."  
[Jost Hochuli, Le détail en typographie, Éditions B42](http://www.editions-b42.com/books/le-detail-en-typographie/)

Je continue la lecture de ce petit livre avec un plaisir et un étonnement proches de l'ahurissement. Je me demande naïvement si le Web et les livres numériques (hors formats figés type PDF), en d'autres termes le HTML et l'ePub, prennent en compte ces "unités microtypographiques". Et dans tous les cas cela a-t-il une incidence sur nos lectures numériques ?

>Cependant, ici encore, c'est l'œil qui est dernier juge.  
[Jost Hochuli, Le détail en typographie, Éditions B42](http://www.editions-b42.com/books/le-detail-en-typographie/)