---
comments: true
date: 2011-12-06
layout: post
slug: classer-le-web-des-outils
title: Classer le Web, des outils
categories:
- carnet
---
Il y a encore un an l'utilisation de services de marque-pages ne m'apparaissait pas évidente, mon usage du web se devait d'être efficace, et donc je laissais de côté toutes les ressources qui ne m'étaient d'aucune utilité immédiate. Pourtant, parfois, je ne retrouvais pas un billet aperçu quelques jours plus tôt, et l'usage intensif d'un moteur de recherche me semblait insensé (notamment par ce que cela induit : une fâcheuse impression d'être assisté).

Les marque-pages des navigateurs m'ont toujours laissé sans grande satisfaction : les possibilités d'accès étant trop limitées (difficulté de les avoir sur plusieurs support) ; et le fait d'alimenter des listes sans fin sans grande précision d'organisation... Le seul service web que je me permettais dans cette catégorie : Netvibes, qui me servait à la fois d'agrégateur et de liste de liens. Outil de marque-pages finalement encore plus limité que les marque-pages "traditionnels", le seul avantage étant l'accès à distance depuis n'importe quel machine. J'ai essayé puis abandonné très vite Delicious, une des raisons essentielles : angoisse d'avoir la possibilité de tout enregistrer...

Mes besoins sont les suivants :

+   marquer un lien, lui attribuer des tags, et un commentaire ;
+   créer des notes ;
+   donner une visibilité à ces liens et notes, et gérer cette visibilité (publique/privée) ;
+   créer et gérer des listes pour classer ces liens et notes ;
+   créer et gérer des groupes, et pouvoir les alimenter à plusieurs ;
+   disposer de flux RSS : global, par tag, par liste... ;
+   avoir la possibilité de rechercher facilement dans mes liens/notes (par tag, texte intégral, date d'enregistrement) ;
+   disposer d'extensions pour les navigateurs ;
+   disposer d'une application pour smartphone avec accès offline.

J'ai donc tenté [Diigo](http://www.diigo.com/user/antoinef/), service similaire à Delicious, en plus moche et en moins ergonomique, mais avec certaines fonctionnalités supplémentaires. Les principaux atouts de ce services, qui répondent à mes attentes ci-dessus, sont :

+   service de marque-page social relativement classique qui permet donc d'enregistrer des liens avec attribution de tags ;
+   possibilité de commenter chaque lien (je reviendrai sur ce point) ;
+   gestion des liens par le biais de listes ;
+   création de "groupes" : listes que l'on peut alimenter à plusieurs ;
+   création de notes (sans limitation du nombre de caractères) ;
+   moteur de recherche interne (dans l'ensemble des éléments enregistrés par moi et par d'autres personnes inscrites sur Diigo) ;
+   extensions pour navigateurs, et en particulier celle pour Firefox est bien faite ;
+   application Android qui permet un usage offline ;
+   d'autres fonctionnalités existent, mais je ne m'en sers pas.

Le problème avec Diigo, comme avec bon nombre de services web (gratuits ou payants), c'est le fait que l'on n'est pas maître de ses propres données. Je rêve de solutions alternatives mais je n'en trouve pas de satisfaisantes, parce qu'aucune ne propose la possibilité d'alimenter des listes à plusieurs (fonctionnalité "groups" sur Diigo). Je ne sais pas si, justement, [SemanticScuttle répond à mes attentes](http://geek.bafouillages.net/2010/12/social-bookmarking-avec-semanticscuttle-alternative-libre-a-delicious/).

Il reste la dimension sociale d'un outil comme Diigo, que j'aborderai plus loin.