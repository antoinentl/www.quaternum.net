---
layout: post
title: "Pourquoi quitter Wordpress ?"
date: 2012-12-23
comments: true
categories:
- carnet
---
## Wordpress n'est pas un outil d'écriture naturel
En tout cas pas pour moi. Pour écrire un article il faut :

+   disposer d'une connexion Internet&nbsp;;
+   ouvrir un navigateur web&nbsp;;
+   se connecter à l'interface de Wordpress&nbsp;;
+   créer un article ou afficher celui sur lequel on souhaite intervenir&nbsp;;
+   être contraint d'écrire dans le [wysiwyg](http://fr.wikipedia.org/wiki/What_you_see_is_what_you_get "Lien vers Wikipédia") de Wordpress.

On peut trouver plusieurs moyens de contourner la contrainte du wysiwyg de Wordpress&nbsp;:

+   utiliser un client pour Wordpress, mais aucun n'est satisfaisant&nbsp;: Blogilo bugg, l'application pour Android ou iOS est une application avec tout ce qu'elle comporte comme limites (mise en forme, gestion des versions hasardeuse, pas d'équivalent pour Linux...)&nbsp;;
+   pendant un temps j'ai écris mes articles en HTML, en effectuant un copier-coller dans le wysiwyg&nbsp;;
+   pour le moment j'écris mes billets en [Markdown](https://www.quaternum.net/2012/12/03/ "Lien vers quaternum"), avec également un copier-coller dans le wysiwyg, en utilisant le plugin [Markdown on Save Improved](http://wordpress.org/extend/plugins/markdown-on-save-improved/) pour Wordpress.

Ces deux dernières solutions ne sont pas satisfaisantes&nbsp;: les articles écrits en HTML ou en Markdown sont bons à jeter puisque les ultimes modifications sont faites dans le wysiwyg.  
Cette façon d'écrire les billets n'est pas vraiment tenable sur la durée - plus pour des raisons de principe que techniques, car ce n'est pas pour le nombre de billets que j'écris...  
Wordpress correspond à une utilisation d'Internet qui n'est plus la mienne&nbsp;: être connecté pour écrire.


## "Obsolescence injectée dans l'écrit"

Pour un carnet de ce type, l'utilisation d'un CMS de la puissance et de l'envergure de Wordpress [semble disproportionnée](https://www.quaternum.net/2012/12/04/markdown-pour-simplifier-et-maitriser/ "Lien vers quaternum"). Et Wordpress est un système qui nécessite d'être mis à jour et maintenu régulièrement.

Nicolas Taffin le dit diablement bien&nbsp;:

>@antoinentl oui, je ne crois pas aux CMS. Obsolescence injectée dans l'écrit. Retour au Gopher ? J'adorais #1990's  
[@NT_polylogue](https://fr.twitter.com/NT_polylogue/status/280989793633058816)


## Dissocier l'écriture du moteur du carnet

C'est le point qui me semble le plus important. L'écriture se fait ailleurs que dans le moteur du carnet. Je peux décider d'utiliser un autre générateur ou un CMS, puisque les articles existent en dur, avec leurs métadonnées propres.


## Migration

Tout ça pour dire que je vais (enfin) migrer sans tarder vers un générateur de sites statiques (j'en parlais déjà [là](https://www.quaternum.net/2012/05/09/quel-moteur-pour-ce-carnet/ "Lien vers quaternum")). Après quelques tests [Octopress](http://octopress.org "Lien vers Octopress") (framework basé sur Jekyll) est le plus simple à prendre en main. Sans rentrer dans les détails cela me permettra&nbsp;:

+   de me passer de wysiwyg&nbsp;: on peut écrire en Markdown dans n'importe quel éditeur de texte, l'écriture est dissociée du moteur du carnet&nbsp;;
+   de conserver tout ce que j'écris sans me soucier de la façon dont est généré ce carnet&nbsp;;
+   de gérer le site en local et d'y effectuer les relectures nécessaire *avant* la mise en ligne (séparation de l'écriture et de la publication)&nbsp;;
+   de disposer d'un site portable en HTML&nbsp;;
+   ...