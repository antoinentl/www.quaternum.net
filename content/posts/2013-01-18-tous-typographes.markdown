---
layout: post
title: Tous typographes
slug: tous-typographes
date: 2013-01-18
comments: true
categories:
- carnet
---
>Avec le texte au format numérique, à la mise en page liquide, chacun peut changer les paramètres élémentaires. Il faudrait être typographe soi-même pour lire son livre savamment et ne pas jeter plus de mille ans de connaissance de mise en forme du texte. Pour un professeur, la perspective d'un retour à l'école de tous ces lecteurs en quête de connaissances typographiques est assez joyeuse&nbsp;!  
[Philippe Millot, Les polices ont du caractère, un article de Corinne Renou-Nativel sur La-Croix.com](http://liens.quaternum.net/?4dLhmg)

Philippe Millot - qui est _dessinateur de livres_ - abordait il y a quelques semaines une excellente question. Les applications qui *lisent* les livres numériques sont nombreuses (intégrées dans les liseuses, proposées sur les tablettes ou les ordinateurs, disponibles en ligne via un navigateur), et proposent un nombre plus ou moins important de choix de réglages (de la police aux marges, en passant par les couleurs). Potentiellement n'importe qui peut modifier les paramètres originaux d'un livre numérique, la mise en page n'est plus forcement celle qui a été pensée pour le livre, mais un choix - potentiel - du lecteur. Choix qui peut faciliter la lecture (police plus lisible), ou qui peut être désastreux à la fois pour le lecteur et le livre. Choix qui peut [remettre en cause](https://www.quaternum.net/2012/12/27/derriere-les-livres/ "Lien vers quaternum") le rôle du graphiste - ou graphic designer.  
Il y a même des cas où le "style de l'éditeur" est remplacé - par défaut - par le style proposé par l'application (c'est [la mauvaise surprise que j'avais eu avec la Bookeen](https://www.quaternum.net/2012/08/23/lecture-numerique-et-integrale/ "Lien vers quaternum")).

Il y a quelque chose de beau dans cette possibilité de choix typographique des lecteurs. Mais c'est aussi particulièrement effrayant. N'y a-t-il pas un risque à ce que nous nous perdions dans ces possibilités&nbsp;? Peut-être aussi que cette "ouverture" n'est que temporaire...