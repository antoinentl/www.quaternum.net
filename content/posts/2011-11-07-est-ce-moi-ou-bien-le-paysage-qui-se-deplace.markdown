---
comments: true
date: 2011-11-07
layout: post
slug: est-ce-moi-ou-bien-le-paysage-qui-se-deplace
title: Est-ce moi ou bien le paysage qui se déplace ?
categories:
- carnet
---
>Ça y est nous partons. Les premiers tours de roue sont si lents que la perception peine à trancher : est-ce moi ou bien le paysage qui se déplace.  
[Marie Saur et Nylso, Jérôme et la route](http://www.flblb.com/Jerome-et-la-route.html)

Tout à l'heure brève discussion avec un éditeur de la région Rhône-Alpes à propos de livres numériques. A ma question de savoir où est-ce qu'il en est de sa réflexion, il semble principalement bloqué par l'apparente absence de demande et par la peur d'investir dans le vide. Je tente de lui expliquer que cet investissement peut être aussi un moyen de comprendre comment sa chaîne peut (et va) se transformer (et pas uniquement à cause de la fabrication d'un nouveau fichier), et d'entrevoir les modifications plus générales à travers ses bouleversements hypothétiques et contemporains/futurs.

En lisant cette phrase extraite de [Jérôme et la route](http://www.flblb.com/Jerome-et-la-route.html), je perçois son questionnement, quand est-ce que nous partons réellement, quand est-ce que nous démarrons réellement une nouvelle expérience ? Il n'y a justement pas de moment précis (pur fantasme), le questionnement de cet éditeur est peut-être justement ce point de départ diffus, espérons alors qu'il y ait continuité, prolongement.