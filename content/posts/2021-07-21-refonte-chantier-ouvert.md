---
title: "Refonte : chantier ouvert"
date: "2021-07-21T08:00:00"
description: "La refonte d'un site web est toujours un exercice stimulant, cela fait plusieurs mois que je voulais reconstruire de zéro mon carnet quaternum.net, je débute cette entreprise en rendant public ce processus."
categories:
- carnet
---
La refonte d'un site web est toujours un exercice stimulant, cela fait plusieurs mois que je voulais reconstruire mon carnet quaternum.net en partant de zéro, je débute cette entreprise en rendant public ce processus.

Ce carnet a cette forme depuis au moins 7 ans, j'avais alors adapté [un thème Jekyll](https://demo.getpoole.com/), tordant la structure et la feuille de styles.
J'ai besoin, et surtout envie, de construire _à la main_ et sans modèle ce qui est un peu ma maison numérique.
Cela signifie définir des gabarits de pages et dessiner la composition, tout cela dans un souci de simplicité.

## À ciel ouvert
D'autres [s'y sont](https://v7.robweychert.com/blog/2020/01/v7-introduction/) [essayés](https://www.sarasoueidan.com/blog/redesign/) ou [s'y essayent actuellement](https://www.robinrendle.com/notes/designing-in-public), réaliser une refonte ou un _redesign_ de façons progressive et publique a quelque chose de grisant, et aussi d'un peu risqué.
Je m'impose cette contrainte pour que l'opération ne soit pas trop longue, le ~~marathon~~ doctorat ne me laissant que peu de temps.
Tout est versionné et disponible sur [ce dépôt public](https://gitlab.com/antoinentl/www.quaternum.net/-/milestones/2), les remarques sont les bienvenues.
Il est aussi possible de [m'écrire](/a-propos).

## Articuler les espaces de publication
Le premier objectif de cette refonte est de relier plusieurs espaces de publication, et cela implique un changement de générateur de site statique.
J'en profite pour jouer avec [Hugo](https://gohugo.io/) que je pratique intensément depuis un an, et ainsi de faire plus facilement le lien avec deux autres espaces de publication :

- [mon atelier de thèse](/2020/03/29/atelier-de-these/). Il n'est pas public mais je publie certains des contenus [ici](/phd) ;
- une forge de textes qui me permet de diffuser des textes de façon temporaire (intervention, projet d'article), un espace lui aussi non public.

En utilisant la même _fabrique_ ou presque, je peux faire circuler des contenus avec un balisage commun — par exemple via l'usage des [_shortcodes_ de Hugo](https://gohugo.io/content-management/shortcodes/).

Pourquoi Hugo et pas un générateur plus récent ou plus à la mode ?
Hugo n'a pas de dépendance (un seul fichier binaire suffit), il est rapide (pratique pour prévisualiser en parallèle de l'éditeur de texte), multiformat (plusieurs formats peuvent être générés à partir d'une même source) et modulaire (mais sans la contrainte des extensions habituelles).
Le seule problème, propre à tous les générateurs de site statique, c'est le fait que tous les contenus sont re-générés, sans possibilité de faire de l'incrémental (ne re-générer que les pages modifiées).
Si je choisis Hugo aujourd'hui, c'est aussi pour pouvoir changer plus tard, cette décision n'étant pas, et ne pouvant pas, être définitive.

## Quelques contraintes pour démarrer
Cette première étape est sommaire, puisqu'il n'y a même pas de feuille de style et que la structure HTML est très basique — y compris les métadonnées d'entête — voir même un peu bancale.
Les seules contraintes que je me suis fixé pour démarrer sont les suivantes :

- tenter de prolonger le suivi des fichiers : conserver le versionnement que j'effectue avec Git depuis quelques années, donc sans supprimer l'historique sur la plupart des fichiers. Certaines commandes Git étaient un peu acrobatiques, je n'ai pas pris la peine d'écrire des scripts ;
- obtenir les mêmes adresses : normalement les 1157 pages (environ) ont toutes la même URL, ce qui m'a demandé une reprise quasi-manuelle pour 150 pages et entièrement manuelle pour une centaine de plus. C'est ce qui a été le plus long (plusieurs heures) ;
- proposer le même flux RSS : malgré mes efforts vous risquez de voir l'ensemble de mes billets _réapparaître_ dans votre lecteur de flux, désolé par avance. J'ai toutefois gardé la même structure ;
- conserver un système de citation/bibliographie : avec [Hugo Cite](https://labs.loupbrun.ca/hugo-cite/) de [Louis-Olivier](https://www.loupbrun.ca/), ce qui m'a pris à peine une heure.

Les prochaines étapes seront la structure HTML, les métadonnées, les pages d'index, la composition typographique, la gestion de la rouille, la production de formats alternatifs, etc.