---
comments: true
date: 2012-05-09
layout: post
slug: quel-moteur-pour-ce-carnet
title: Quel moteur pour ce carnet ?
categories:
- carnet
---
J'utilise Wordpress depuis un certain temps, principalement parce qu'il est facile à installer, à manipuler et à administrer, et qu'il existe un nombre important de thèmes et d'extensions à y associer. Mais voilà Wordpress est potentiellement fait pour répondre à des besoins importants, il est donc à la fois puissant et très lourd (en temps de chargement et ressources sur le serveur). Dans le cas de ce carnet c'est beaucoup trop pour ce que j'en fais.

J'avais déjà imaginer adopter la démarche inverse en découvrant [le site de Frank Adebiaye](http://www.fadebiaye.com/ "Lien vers le site de Frank Adebiaye") : tout faire à la main, éditer des fichiers HTML pour avoir quelque chose de très simple. C'était bien avant de savoir que des solutions comme [Jekyll](http://jekyllrb.com/ "Lien vers le site de Jekyll" ) existent. Jekyll est un générateur de pages statiques (ou "blog-aware, static site generator") écrit en Ruby, il permet de créer des pages statiques tout en adoptant et en respectant facilement une architecture définie. Il n'y a pas d'interface d'administration, il faut passer par un éditeur de texte pour écrire les articles en HTML ou en [Markdown](http://fr.wikipedia.org/wiki/Markdown "lien vers Wikipédia").
Les avantages sont nombreux :

+   pas de base de données ni de PHP, il faut juste avoir un serveur Web ;
+   les pages étant créées en statiques, on peut très facilement conserver une archive du site "à plat" ;
+   les temps de chargement sont très rapides ;
+   les ressources du serveur nécessaires sont minimes (c'est du HTML pur et simple) ;
+   on peut très facilement créer des feuilles style différentes et les associer aux pages que l'on veut (dans le cas de CMS comme Wordpress c'est beaucoup plus compliqué) ;
+   l'écriture des articles ne passe plus par une interface d'administration mais par l'éditeur de texte de notre choix (oh joie).

Les carnets de [Clochix](http://esquisses.clochix.net/) ou de [Karl (La Grange)](http://la-grange.net/) utilisent le même type de système, [Jérôme Mahuet](http://phollow.fr/2012/05/de-wordpress-a-jekyll/) (que je découvre) est aussi récemment passé à Jekyll (c'est beau). Mes connaissances techniques étant un peu limitées, je ne vais pas tout de suite adopter cette solution, mais je commence à y réfléchir sérieusement. D'ailleurs [Porneia Delights y est déjà passé](http://porneia.free.fr/pub/archives/2012.html#0410) et prépare apparemment un billet sur ce sujet. Le problème (pour moi) est la mise en place d'un tel dispositif, c'est faisable mais cela me demandera un peu de temps&nbsp;: pas la peine d'apprendre le Ruby ou le Python, mais il faut un certain temps pour bien comprendre le fonctionnement, paramétrer tout ça et créer à la fois la structure des pages et les feuilles de style qui vont avec.

D'autres alternatives, à la fois plus légères par rapport à Wordpress, et plus faciles à mettre en place que des moteurs de pages statiques, existent. Des CMS n'utilisant pas de base de données par exemple, [Idleman](http://blog.idleman.fr/?p=1055) en a fait tout un billet, son choix s'est porté sur [Blogotext](http://lehollandaisvolant.net/blogotext/), développé par le Hollandais Volant. De mon côté je lorgne plutôt sur [PluXml](http://www.pluxml.org/), solution très légère, sans base de données, et basée sur du PHP et du XML. Le résultat se rapproche beaucoup de Wordpress (ou de Dotclear), la légèreté en plus et quelques fonctionnalités en moins (que je n'utilise en fait pas sous Wordpress pour la plupart).  
J'hésite encore à choisir cette solution pour ce carnet, principalement à cause de la forme et de la structuration des URL. Avec PluXml actuellement les URL ressemblent à cela : www.quaternum.net/article-XX/titre-de-larticle. Impossible de reproduire la structure que j'ai choisie avec Wordpress : www.quaternum.net/AAAA/MM/JJ/titre-de-l-article. Pour moi la possibilité de choisir la forme des URL est totalement incontournable, c'est le seul élément qui me permet un minimum de pérennité : par exemple dans le cas d'un changement de "plateforme".  
Je ne sais pas si l'URL rewriting peut aller jusque là, et je suis d'ailleurs preneur d'informations. Pendant ce temps je teste et je cherche une solution. Peut-être que je vais passer directement de Wordpress à Jekyll sans l'étape PluXml.