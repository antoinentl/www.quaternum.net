---
layout: post
title: "Croisements et à la main : jour 5 des Rencontres de Lure"
date: "2015-09-04T22:00:00"
comments: true
published: true
categories: 
- carnet
slug: lure-2015-jour-5
---
Cherchant depuis deux ans un moyen de restituer mon expérience des [Rencontres internationales de Lure](http://delure.org/), je profite de la thématique de cette année -- Portraits/paysages, un monde de formats -- pour expérimenter un nouveau moyen de rendre compte de cette semaine de rencontres très dense : je décide cette fois de rédiger un article pour chaque journée, reprenant quelques idées notées et issues des conférences ou des discussions. L'objectif n'est pas uniquement de partager des discours entendus, mais de voir également qu'est-ce que j'ai *appris* et qu'est-ce que je pourrais réutiliser *réellement* dans mes pratiques professionnelles ou personnelles.  
Voici ce que j'ai noté pour la *cinquième* journée. Pour la première journée, c'est [par ici](/2015/08/31/lure-2015-jour-1/), pour la seconde journée c'est [par là](/2015/09/01/lure-2015-jour-2/), et pour la quatrième journée c'est [par ici](/2015/09/03/lure-2015-jour-4/).

<!-- more -->

## Croisements et ouverture
De nombreuses discussions animent les temps hors des conférences des Rencontres de Lure, l'occasion de partager, de découvrir, de débattre, de discuter, de se rassembler, de comprendre, d'apprendre.  

Je veux rendre compte d'une discussion parmi d'autres, avec [Martin Violette](http://cargocollective.com/MartinViolette) (enfin je crois que c'est lui), un étudiant qui rentre prochainement au [Post-diplôme "typographie et langage" de l'ésad d'Amiens](http://postdiplome.esad-amiens.fr/).  
En évoquant [la présentation de Mike Sabbagh](/2015/09/03/lure-2015-jour-4/#recherche), Martin m'a expliqué que, dans sa démarche de typographe -- étudiant ou professionnel --, il y a pour lui une nécessité de porter un discours lisible. Mike avait en effet apporté un soin particulier à expliquer tous les termes techniques utilisés par les dessinateurs de caractères, répondant avec patience aux questions les plus basiques.  

Les travaux typographiques doivent être compris par tous afin de permettre un dialogue avec d'autres domaines ou secteurs de la typographie. Permettre le dialogue pour ouvrir les pratiques et les perspectives. Par exemple la musique peut amener des concepts de rythme différents dans une recherche de dessin de caractère, et cela n'est possible que si la discussion est compréhensible entre deux personnes de ces deux *domaines*.  

L'effort d'ouverture, d'explication, doit être continu, les Rencontres de Lure sont à ce titre un bon terrain pour expérimenter cette lisibilité des pratiques.

## À la main
[Émilie Rigaud](http://www.emilie-rigaud.com/) est venue présenter son travail de dessinatrice de caractères, produits sous l'égide de sa fonderie ["A is for"](http://www.aisforapple.fr/). Émilie a pris le temps de décrire les processus de création de huit caractères.  

![Quelques images de la famille de caractères Coline](/images/delure-2015-emilie-rigaud-coline.png)  

Le processus de création d'Émilie Rigaud est intéressant et semble dénoter d'une apparente culture actuelle : elle ne travaille qu'*à la main* pour le dessin de caractères. L'ordinateur n'est qu'un outil, qui "accélère le processus", qui vient en "fin de course du processus". Y compris pour les versions *bold* ou italique de ses fontes.

>La tension créée par la main me permet de dessiner un caractère.

Si aujourd'hui tous les outils pour dessiner *numériquement* des caractères existent -- des dispositifs comme les tablettes graphiques aux logiciels plus ou moins complexes --, et si les fontes sont de plus en plus utilisées pour les écrans, le workflow des typographes peut être à rebours de cela. Émilie Rigaud utilise le dessin, mais aussi le découpage pour trouver des *patterns* de forme afin de construire des caractères.  
Utiliser des outils qui sont, en apparence, dans d'autres paradigmes que les résultats visés -- par exemple dessiner à la main des caractères pour l'impression numérique ou l'écran -- est une perspective en partie déstabilisante mais réjouissante.

>Je ne veux pas de caractères qui crient plus fort que le texte.  
Émilie Rigaud