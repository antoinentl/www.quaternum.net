---
layout: post
title: "Un serveur à la maison"
date: "2014-01-05T21:32:00"
comments: true
categories: 
- carnet
---
>Je ne comprends pas qu’une sauvegarde distante doive traverser l’atlantique. [...]  
Je ne comprends pas que l’on fasse l’apologie de l’illimité dans un monde fini. [...]  
Je ne comprends pas. Mais je m’interroge. Ce n’est pas une ère d’abondance mais de gaspillage numérique dont nous faisons partie. Et cela ne fait que commencer…  
[David Larlet, Gaspillage numérique](https://larlet.fr/david/blog/2014/gaspillage-numerique/)

L'apparition du [Raspberry Pi](http://fr.wikipedia.org/wiki/Raspberry_Pi "Lien vers Wikipédia") depuis quelques années permet à des — presque — néophytes d'expérimenter un peu ce que veut dire le concept d'Internet&nbsp;: participer à un réseau distribué, et donc installer relativement facilement une petite machine qui fait office de serveur web, voir plus. Il ne s'agit pas de tout héberger, de tout déplacer sur une telle machine à la disponibilité limitée. L'idée est plutôt de réduire la distance entre moi et certaines données ou applications, lorsque je veux multiplier les points d'accès à des services, éviter qu'une ["sauvegarde distante doive traverser l’atlantique"](https://larlet.fr/david/blog/2014/gaspillage-numerique/ "Lien vers larlet.fr").  
On pourrait aller plus loin dans cette idée de [relocaliser](/2012/02/14/decentralisons/ "Lien vers quaternum.net") nos données, en multipliant les points d'accès à certains services ou données, créer un réseau *maîtrisé* avec ce type de machine.  

Installer une machine du type Raspberry Pi permet aussi d'expérimenter et de comprendre&nbsp;:

+ ce qu'est Internet&nbsp;;
+ qu'il est possible de se passer de tiers à moindre coût mais avec de nombreuses contraintes&nbsp;;
+ les limites, en se trompant et en recommançant&nbsp;;
+ comment gérer ses propres [carnets](/2014/01/02/construire-du-sens/ "Lien vers quaternum.net")&nbsp;;
+ etc...