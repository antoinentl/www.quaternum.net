---
comments: true
date: 2011-12-15
layout: post
slug: classer-le-web-la-dimension-sociale
title: Classer le Web, la dimension sociale
categories:
- carnet
---
Comme je l'expliquais [ici](https://www.quaternum.net/2011/12/06/classer-le-web-des-outils/ "Lien vers quaternum.net"), j'utilise Diigo comme un outil de classement des contenus du Web, c'est ma "bibliothèque de liens". Par définition Diigo est un réseau social : création d'un compte ; gestion d'une identité numérique (présentation, lien vers d'autres services...) ; possibilité de suivre d'autres comptes ; partage/récupération de liens ou notes ; création et gestion de groupes (listes de liens) ; possibilité de commenter des liens ou des notes. La dimension sociale de cet outil est-elle si prédominante ? En effet la seule fonctionnalité sociale que j'utilise est celle des groupes (alimentation d'une liste à plusieurs, avec commentaires). C'est d'ailleurs principalement cette fonctionnalité qui m'a fait préférer cet outil à d'autres services (Delicious) ou systèmes (SemanticScuttle).

La dimension sociale des outils de marque-pages est à mon avis très limitée, encore plus pour les outils qui ne proposent pas cette option de partage à plusieurs. J'avoue que je ne vois pas réellement l'intérêt de "suivre" un autre compte Diigo, ou alors peut-être est-ce une piste que je devrais explorer notamment avec l'utilisation des flux RSS, mais alors hors Diigo.

Mon usage de Diigo me donne l'impression que cet outil est certes très puissant, mais qu'il pourrait finalement n'être utilisé que comme un back office. La visibilité de ce classement, ainsi que sa diffusion pouvant se faire ailleurs, par le biais des flux RSS (intégration dans des pages, par le biais de widgets...).