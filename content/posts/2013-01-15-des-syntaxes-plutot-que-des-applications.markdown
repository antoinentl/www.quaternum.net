---
comments: true
date: 2013-01-15
layout: post
slug: des-syntaxes-plutot-que-des-applications
title: Des syntaxes plutôt que des applications
categories:
- carnet
---
La découverte de [todo.txt](https://github.com/ginatrapani/todo.txt-cli/wiki/The-Todo.txt-Format "Lien vers la description du fonctionnement de todo.txt sur github") me fait rendre compte d'une chose&nbsp;: il faut abandonner les applications pour des systèmes simples qui reposent sur des syntaxes. [En cherchant](https://www.quaternum.net/2012/03/26/des-exigences-dusage/ "Lien vers quaternum") des outils *numériques*, j'ai trop souvent fait l'erreur de partir de l'interface - du point de vue de l'ergonomie - plutôt que des réponses précises à des besoins.  
De la même façon que [Markdown](https://www.quaternum.net/2012/12/04/markdown-pour-simplifier-et-maitriser/ "Lien vers quaternum") simplifie l'écriture et la gestion de celle-ci - parce que le texte et les métadonnées sont dans un simple fichier lisible partout - todo.txt simplifie la gestion d'une liste de tâches grâce à un seul fichier texte. L'interface n'est plus au centre de l'utilisation.  
Il est toujours possible d'imaginer des accès et des interactions - simples mais reposant sur des systèmes complexes - qui interviennent sur ces fichiers. [Jekyll](https://github.com/mojombo/jekyll/ "Lien vers le projet sur github"), le générateur de site statique, part des fichiers écrits en Makdown pour la partie contenus. Todo.txt peut être synchronisé avec des services tels que Ubuntu One, et une application permet une gestion sur des supports mobiles.

Abandonner les applications pour des systèmes simples qui reposent sur des syntaxes, cela permet :

+ une simplicité d'usage après un - court - temps d'apprentissage&nbsp;;
+ une interopérabilité&nbsp;: lisibilité de l'information dans la majorité des contextes (par exemple dans les cas de Todo.txt et Jekyll on peut revenir aux fichiers lisibles par n'importe quel éditeur de texte)&nbsp;;
+ la pérennité (valeur qui varie selon l'utilisation, pour une todo liste c'est tout de même très relatif sur la durée)&nbsp;;
+ de ne pas alourdir les serveurs&nbsp;: limiter les requêtes&nbsp;;
+ de favoriser les usages hors ligne, plutôt que la connexion permanente pour accéder à des interfaces web par exemple.

## Le cas d'une liste de tâches

La todo liste peut être considérée comme un gadget, ou comme un outil de gestion des tâches, et donc d'organisation du travail, tout dépend du point de vue.  
J'ai utilisé [myTinyTodo](http://www.mytinytodo.net/ "Lien vers le projet") pendant presqu'un an, mais cette application est trop lourde&nbsp;: nécessité d'une base de données et accès via une interface web.  
Je lorgne sur [Nitro](http://nitroapp.com "Lien vers le projet")&nbsp;: fonctionnement similaire avec un système plus simple mais qui nécessite l'utilisation d'un client ou d'une interface web pour interpréter et modifier le fichier .json&nbsp;; possibilités d'utilisation hors connexion.  
[Trello](https://trello.com/ "Lien vers Trello") est attrayant, mais cet outil n'est utilisable qu'avec une connexion, et on a pu constater [la fragilité](http://blog.trello.com/card-filter-updates-cards-page-sorting-checklist-copy-and-more/) de son infrastructure lors de la tempête Sandy.  
Todo.txt est intéressant parce que c'est l'occasion d'[apprendre](https://www.quaternum.net/2012/06/03/habitudes-et-interfaces/ "Lien vers quaternum") une nouvelle syntaxe - certes simple - et de modifier ma façon de travailler avec les outils numériques.