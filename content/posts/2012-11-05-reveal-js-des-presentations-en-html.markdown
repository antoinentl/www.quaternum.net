---
comments: true
date: 2012-11-05
layout: post
slug: reveal-js-des-presentations-en-html
title: 'reveal.js : des présentations en HTML'
categories:
- carnet
---
reveal.js est un script JS permettant de créer des présentations du type powerpoint ou keynote en HTML5 et CSS3, affichable dans un navigateur et avec des effets simples et beaux. [reveal.js](http://hakim.se/projects/reveal-js "Lien vers le site de Hakim El Hattab") est un ensemble de fichiers HTML, CSS et JavaScript facilement modifiables, c'est Hakim El Hattab qui est à l'origine de ce petit projet. [Sur Github](http://github.com/hakimel/reveal.js "Lien vers Github") on retrouve les fichiers, avec plusieurs thèmes en plus de celui par défaut. Pour celles et ceux qui maîtrisent le Web, reveal.js est une bonne base pour créer des présentations originales. Il suffit pour cela de modifier un fichier HTML, le contenu est structuré en "section", une page ou "diapositive" ressemble à cela :

<pre><code>&lt;section&gt;
&lt;h1&gt;Titre&lt;/h1&gt;
&lt;h2&gt;Sous-titre&lt;/h2&gt;
&lt;p&gt;texte&lt;/p&gt;
&lt;/section&gt;</code></pre>

Pour la création et les modifications reveal.js propose aussi une version en ligne pour créer, éditer et publier les présentations, elle s'appelle [rvl.io](http://www.rvl.io/ "Lien vers rvl.io"). L'interface colle tout à fait au projet&nbsp;: c'est simple et efficace. On peut même accéder au code source de chaque "diapositive" pour préciser le code. La fonction export se résume à la récupération du code HTML correspondant au contenu, que l'on peut ensuite réutiliser à partir des fichiers proposés sur Github.

reveal.js est une bonne alternative aux présentations "classiques" pour plusieurs raisons :

+   la mise en forme tient dans un fichier HTML et dans une feuille CSS, donc tout est possible, dans la limite de ce que permettent les standards du Web&nbsp;;
+   on peut notamment jouer relativement facilement sur la typographie avec @font-face ou les webfonts&nbsp;;
+   le contenu est structuré&nbsp;;
+   la navigation ne se limite pas à un défilement horizontal ou vertical, mais à la fois horizontal _et_ vertical&nbsp;;
+   on peut utiliser la présentation en local ou sur une clé USB, pour la modifier un éditeur de texte suffit, pour afficher la présentation un navigateur récent suffit&nbsp;;
+   on peut aussi l'héberger facilement (il s'agit de fichiers HTML, CSS et JavaScript.

Pas besoin d'un outil lourd comme LibreOffice Impress, pour peu que l'on comprenne le HTML et le CSS et que l'on veuille passer un peu de temps sur la structuration et la mise en forme (less is more).

En plus de reveal.js, on peut aussi utiliser [remote.io](http://remot.io/ "Lien vers remote.io"), système lui aussi très simple qui permet de commander la présentation depuis un smartphone :

+   il faut d'abord se rendre sur cette page : [http://remot.io](http://remot.io/)&nbsp;;
+   on récupère simultanément l'URL d'accès pour la commande via le smartphone, et le bookmarklet que l'on ajoute sur son navigateur qui est utilisé pour la présentation&nbsp;;
+   on se connecte via son smartphone à l'URL indiquée&nbsp;;
+   une fois la présentation affichée dans la navigateur, on active la connexion en cliquant sur le bookmarklet&nbsp;;
+   on fait glisser les "slides" avec ses doigts depuis le smartphone.