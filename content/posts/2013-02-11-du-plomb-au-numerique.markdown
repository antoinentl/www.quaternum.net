---
comments: true
date: 2013-02-11
layout: post
slug: du-plomb-au-numerique
title: Du plomb au numérique, quelques questionnements
categories:
- carnet
---
>Les fontes sont un objet technique et juridique complexe, car s'y mêle à la fois une dimension artistique, une dimension logicielle, ainsi que des attentes et des scénarios d'usage divers.  
[Fontes libres, Chapitre 5](http://fr.flossmanuals.net/fontes-libres/ch005_licences)

Ce lundi 11 février 2013, une conférence d'[Alice Savoie](http://www.frenchtype.org/ "Lien vers le site d'Alice Savoie") a eu lieu aux Archives municipales de Lyon, organisée par le Musée de l'imprimerie de Lyon, dans le cadre de [Type Display](http://www.facebook.com/pages/Type-display/575153225844200 "Lien vers la page Facebook de l'événement")&nbsp;:  
**Du plomb au numérique&nbsp;: Gill, Times, Univers et autres anecdotes typographiques**

L'objectif de cette conférence était de prendre du recul sur les évolutions de la typographie&nbsp;: de la composition mécanique à la composition numérique en passant par la photocomposition. Chaque mutation connaît des phases successives d'adaptation des caractères précédemment créés, puis des stades de création avec les différentes avancées et contraintes techniques, soulevant des questions de dessin des caractères, de fabrication des fontes, de production et d'industrialisation.  
Alice Savoie hésite sur le fait que le numérique est tout à la fois une évolution et une révolution, sans être définitive sur le qualificatif - à mon sens à juste titre.  
Sur la fin de la conférence Alan Marshall - le directeur du Musée de l'imprimerie - et Alice Savoie s'accordent sur le fait que le numérique a été un moyen, pour beaucoup de fonderies, de gagner leur indépendance. Depuis plusieurs années on assiste à un mouvement similaire à celui du début du 20e siècle&nbsp;: un retour à la créativité et au dynamisme.

L'objet de cette conférence était bien l'évolution _jusqu'au_ numérique, c'est pourquoi les questions soulevées par la création numérique, l'écriture numérique ou la lecture numérique n'ont pas - ou assez peu - été abordées. Malgré tout plusieurs questions - parmi d'autres - me viennent après coup&nbsp;:

+ le choix des licences, lié à la dématérialisation des polices, est déterminant quant aux possibilités de collaborations créatives, et notamment l'enrichissement de fontes pour plusieurs alphabets. Sujet abordé à travers le manuel [Fontes libres](http://fr.flossmanuals.net/fontes-libres/ "Lien vers le manuel Fontes libres") , et notamment par [Frank Adebiaye](http://www.fadebiaye.com/read.html "Lien vers le site de Frank Adebiaye")&nbsp;;
+ aujourd'hui un nombre incalculable de fontes existent, pourtant - comme l'ont très bien fait ressortir les échanges avec le public lors de cette conférence - les textes de labeur _semblent_ très souvent mis en forme avec la même poignée de fontes. Peut-être est-ce là autant un manque de connaissances typographiques [des lecteurs](https://www.quaternum.net/2013/01/18/tous-typographes/ "Lien vers quaternum") et des éditeurs - oui j'ai bien dit éditeurs&nbsp;;
+ le Web et le livre numérique interrogent nos pratiques de lecture, aussi à travers des questions typographiques&nbsp;;
+ les formats des typographies évoluent [toujours](https://www.quaternum.net/2013/01/30/une-typographie-en-css/ "Lien vers quaternum"), notamment via les Web fonts et @type-face, qu'en est-il de la pérennité de ces fichiers et de ce patrimoine _numérique_&nbsp;?
+ ...