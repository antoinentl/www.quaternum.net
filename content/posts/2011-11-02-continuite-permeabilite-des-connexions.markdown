---
comments: true
date: 2011-11-02
layout: post
slug: continuite-permeabilite-des-connexions
title: Continuité/perméabilité des connexions
categories:
- carnet
---
Billet en réaction à [Envoyé spécial dans mon ordi](http://www.le-tigre.net/Envoye-special-dans-mon-ordi,26306.html "Lien vers le site le-tigre.net") de Xavier de la Porte, publié dans Le Tigre en juillet/août 2011, et mis en ligne le 25 octobre 2011.

Xavier de la Porte parle de l'usage de son smartphone, ou plutôt de son non-usage, puisqu'il ne l'utilise presque que comme un téléphone "classique". Il en vient à décrire l'intérêt, voire même l'aubaine pour lui de ne pas utiliser son smartphone pour ses qualités de connexion (Internet, Web, mails, Facebook, Twitter), et donc de profiter d'un certain plaisir de la déconnexion : "Ce qui me plaît dans la déconnexion, c’est l’idée de la reconnexion."

J'arrive moi aussi à une année d'utilisation d'un smarphone, et il est intéressant de constater les différences d'usage.

## Le smartphone comme ordinateur
Dans les premiers mois je me suis servi de mon smartphone comme d'un ordinateur : lecture de textes et de documents divers (articles, compte-rendus, mails), possibilité d'écrire aussi (surtout via l'application de messagerie, parfois des longs mails, ou via [Diigo](http://www.diigo.com/user/antoinef/ "Lien vers ma bibliothèque Diigo") pour prendre des courtes notes), avantages de la connexion (presque) constante, et de l'outil d'écriture à portée de main. La continuité de la connexion comme quelque chose de naturel (d'un lieu à un autre, d'une machine à une autre). Usages professionnel et personnel mêlés. L'écran de 4 pouces semblait me suffire, au point où je n'allumais que peu mon ordinateur personnel (sauf pour des tâches complexes comme la retouche d'images).  
Pendant près de dix mois j'étais souvent connecté (hormis la nuit et quelques heures par jour en coupant la 3G), et donc potentiellement très sollicitable. La continuité de l'usage d'une machine, et qui plus est de la connectivité, étaient acquises.

## Le smartphone comme bipeur
Il y a deux mois j'ai fait l'acquisition d'un "netbook", ces ordinateurs portables de petite taille, en l'occurrence 10 pouces et moins de 1,4 kilogramme. L'usage que j'avais alors de mon smartphone a mué. D'ordinateur multitâche ce "téléphone intelligent" est devenu à peu de chose près un bipeur. Principalement parce que j'ai très nettement diminué mon temps de lecture sur écran de 4 pouces, préférant repéré les billets, récits, articles, messages, les marquer et les lire plus tard sur mon écran de 10 pouces très mobile, permettant une lisibilité plus importante.

## Le vrai choix de la connexion
Xavier de la Porte défend très bien (et avec beaucoup d'humour), le fait que la reconnexion peut être appréciable, voire même délicieuse. "Le smartphone nous prive de l’intense plaisir de la reconnexion, avec ce qu’elle charrie d’espoir."  
Pourtant le smartphone peut permettre de dépasser cela, c'est-à-dire de sortir de cette dépendance aux outils numériques de communication, et de choisir les moments de connexion, déconnexion et reconnexion. L'usage des outils numériques connectés peut être maîtrisé. On peut sortir de cet état d'attente (agréable ou non, cela dépend), pouvoir être disponible ou ne pas l'être, définir son degré de perméabilité aux connexions. Ne plus être dépendant des machines et d'une connectivité.