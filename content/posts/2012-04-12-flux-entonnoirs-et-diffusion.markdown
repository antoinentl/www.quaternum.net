---
comments: true
date: 2012-04-12
layout: post
slug: flux-entonnoirs-et-diffusion
title: Flux, entonnoirs et diffusion
categories:
- carnet
---

[Cécile](http://liber-libri.blogspot.fr/2012/03/veille-demeler-lecheveau.html "Lien vers le blog Liber libri") et [Pierre (aka Reup)](http://akareup.hypotheses.org/445 "Lien vers le carnet de aka Reup") ont réfléchi le mois dernier à leurs dispositifs de veille du point de vue technique&nbsp;: du récolement d'informations à la diffusion&nbsp;; de la gestion des flux à l'enregistrement, au tri et à la diffusion d'informations provenant de ces flux. Il est toujours très enrichissant de découvrir les pratiques numériques des autres, et d'autant plus concernant la veille. Je donne moi aussi ma recette, maigre participation au sujet. Dans mon cas voici plusieurs données à prendre en compte dès le départ (et qui correspondent en fait à mes [exigences d'usage](https://www.quaternum.net/2012/03/26/des-exigences-dusage/))&nbsp;:

+   disposer d'un système techniquement stable au moins à moyen terme&nbsp;;
+   m'extraire le plus possible des systèmes fermés, et me diriger vers des solutions que je peux héberger, et donc contrôler et faire évoluer&nbsp;;
+   effectuer une lecture de ce que je conserve, et commenter chaque information/page, commentaire qui peut se limiter à une ou plusieurs citations provenant de la source : à mon sens cela ne sert à rien de diffuser des informations sans les avoir lu et commenté. On ne peut pas tout lire et ce n'est pas grave, le travail de veille est fondamentalement imparfait et incomplet, l'importance est la pertinence.

**Les flux**

+   un agrégateur de flux RSS&nbsp;: je tente depuis plusieurs semaines de me débarrasser de Google Reader, la solution [rsslounge](http://rsslounge.aditu.de/) commence à être satisfaisante&nbsp;: mais il n'y a pas de version mobile, l'application Android est assez limitée, et il y a un problème de gestion des caractères accentués (ce qui est gênant pour les exports OPML, obligé de modifier le fichier XML à la main)... Mais je continue tout de même l'expérimentation&nbsp;;
+   Twitter&nbsp;: je dois avouer que c'est aujourd'hui ma principale source d'informations, grâce au travail de veilleurs aguerris (difficile de les citer tous ici). L'idée est de me dégager de Twitter et de privilégier mon agrégateur (et donc rsslounge installé sur un serveur que je maîtrise)&nbsp;;
+   l'éternelle sérendipité&nbsp;;
+   les recommandations de toutes natures, mais qui doivent se matérialiser par une page Web.

**L'entonnoir**&nbsp;: pour moi ici tout se passe avec des gestionnaires de marque-pages, donc principalement [Diigo](https://www.quaternum.net/2011/12/06/classer-le-web-des-outils/ "Lien vers quaternum") mais aussi ["mon" Shaarli](http://liens.quaternum.net/ "Lien vers ma bibliothèque de liens sous Shaarli"). Ces outils me permettent de marquer tout ce que je trouve pour ensuite faire le travail de tri nécessaire :

+   suppression ou déplacement si j'estime que l'information est hors d'un de mes champs de veille (pour le moment ma répartition se fait naturellement de cette façon : veille professionnelle avec Diigo et le reste avec Shaarli)&nbsp;;
+   mise à l'écart des textes pour une lecture approfondie&nbsp;;
+   renommage des titres selon cette nomenclature : [Titre de l'article/billet] - [Nom du site]&nbsp;;
+   indexation, rédaction d'un résumé ou commentaire (neutre pour le professionnel, volontairement subjectif pour le reste), puis classement dans différents groupes et listes de veille.


**La diffusion**&nbsp;: tout repart de Diigo selon [les groupes dans lesquels je classe les liens](http://groups.diigo.com/user/antoinef "Lien vers les groupes liés à mon compte Diigo"). Grâce aux flus RSS de chaque groupe je peux donc facilement automatiser une diffusion sur des réseaux sociaux ou des plateformes (exemple des billets automatisés sur Wordpress). Shaarli propose aussi [un flux RSS](http://liens.quaternum.net/?do=rss "Lien vers le flux RSS de "mon" Shaarli").  
Il est important de garder à l'esprit ce que l'on veut faire de cette veille, et pour moi cela se traduit notamment par une mini lettre d'information diffusée dans un cadre professionnel et pour l'instant volontairement restreint&nbsp;; hors de ce cadre tout ce que je "récolte" est utile pour la préparation de cours, pour alimenter mes réflexions personnelles partagées en partie ici, ou tout simplement pour disposer d'un espace de mémoire numérique.