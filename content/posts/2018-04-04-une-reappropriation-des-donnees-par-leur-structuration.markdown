---
layout: post
title: "Une réappropriation des données par leur structuration"
date: "2018-04-04T10:00:00"
comments: true
published: true
description: "Et si nous envisagions la réappropriation des données par la question de leur structuration ? Un document, un article ou un livre peut être mieux compris, maîtrisé et manipulé si sa structuration est visible et compréhensible. À partir de trois exemples voici un billet qui prolonge ma participation à une table ronde lors des journées d'étude du DDAME à Toulouse les jeudi 15 et vendredi 16 mars 2018."
image: "https://www.quaternum.net/images/2018-reappropriation-donnees-mini.png"
categories: 
- carnet
---
Et si nous envisagions la réappropriation des données par la question de leur structuration&nbsp;? Un document, un article ou un livre peut être appréhendé, modifié, maîtrisé et manipulé si sa structuration est visible et compréhensible. À partir de trois exemples voici un billet qui prolonge ma participation à une table ronde lors des journées d'étude du DDAME à Toulouse les jeudi 15 et vendredi 16 mars 2018.

<!-- more -->

## Les journées d'étude du DDAME
Les journées d'étude 2018 du [Département documentation, archives, médiathèque et édition (DDAME)](http://ddame.univ-tlse2.fr/) de l'Université Toulouse Jean Jaurès avaient pour sujet "La diffusion du patrimoine et de la culture à l'heure du web des connaissances. Structuration, partage et interconnexion des données". La table ronde à laquelle j'ai participé aux côtés de Florence Clavaud (qui travaille aux Archives nationales) et de Nicholas Crofts (qui est consultant en informatique) s'intitulait "Universalité des modèles conceptuels de données, mutualisation des pratiques et prise en compte des usagers". Je n'étais pas tout à fait dans le sujet, et je n'ai pas pu évoquer tout ce que j'avais prévu (c'est le jeu des tables rondes), voici donc trois idées développées.

À noter également que cette participation et ce billet accompagnent des recherches en cours sur les chaînes de publication&nbsp;: depuis [ce billet](/2017/03/13/une-chaine-de-publication-inspiree-du-web/) jusqu'à un mémoire en cours de rédaction, ainsi que des articles et des communications, seuls ou aux côtés de [Julie Blanc](http://julie-blanc.fr/) et de [Thomas Parisot](https://oncletom.io/) notamment.

## Visibilité des documents patrimoniaux sur le Web
Le premier exemple de réutilisation de _données_ est celui d'utilisateurs de documents patrimoniaux. Que ce soit pour sauvegarder des métadonnées ou partager une page de présentation d'un document, les usages peuvent être nombreux. Un des exemples désormais classiques est celui du partage d'une page web présentant un document patrimonial, sur les réseaux sociaux. Il y a encore quelques années ce partage se limitait à un simple lien, puis Facebook et Twitter ont mis en place des standards bien plus riches pour extraire et afficher les métadonnées d'une page web.

Facebook a créé le protocole [Open Graph](http://ogp.me/) inspiré notamment par le [Dublin Core](http://dublincore.org/). Twitter a créé le concept de [Cards](https://developer.twitter.com/en/docs/tweets/optimize-with-cards/guides/getting-started) basé sur un fonctionnement similaire.

Prenons l'exemple de Twitter et de [Gallica](http://gallica.bnf.fr)&nbsp;: lorsque vous partagez sur Twitter une page de présentation d'un document, comme [cette photo des Archives nationales](http://gallica.bnf.fr/ark:/12148/btv1b10517106t/f1.item.r=archives%20nationales), Twitter va extraire automatiquement des informations indiquées dans les métadonnées de la page. Gallica utilise les données de la notice du document pour&nbsp;:

- afficher les informations sur la page web de présentation du document&nbsp;;
- renseigner les métadonnées de cette page web pour le partage sur des réseaux sociaux comme Twitter ou Facebook.

Voici un schéma pour résumer ce fonctionnement assez simple&nbsp;:

![Schéma représentant la distribution des métadonnées d'un document](/images/2018-metadonnees.svg)

Le code source de la page web présentant cette photo des Archives nationales comporte donc un certain nombre de métadonnées, voici un aperçu en image&nbsp;:

![Capture d'écran d'une page d'un document sur Gallica, avec l'affichage du code source](/images/2018-page-gallica.png)

Le partage sur Twitter (ou sur Facebook) n'est pas le seul cas d'usage, il est aussi possible d'utiliser ces métadonnées pour enregistrer les références d'un document dans un système de gestion de références bibliographiques. Ainsi cette (simple) page web peut être un moyen d'enregistrer plusieurs informations sur ce document dans [Zotero](https://www.zotero.org/). Voici le schéma du dessus modifié en conséquence&nbsp;:

![Schéma précédent modifié pour un partage avec l'outil de gestion bibliographique Zotero](/images/2018-zotero.svg)

**Les données d'un document, aussi riche soient-elles, peuvent donc être facilement partagées avec des outils accessibles comme les réseaux sociaux, ou des outils plus professionnels comme des outils de gestion de références, à condition d'adapter l'affichage visible et en partie invisible de ces données – ici une page web.**

## Structuration des données&nbsp;: l'exemple de l'édition de livres
Déplaçons-nous dans le domaine de l'édition. J'utilise un exemple que je connais désormais bien, il s'agit de la chaîne de publication [Quire](https://github.com/gettypubs/quire) développée par le département numérique de [Getty Publications](http://www.getty.edu/publications/digital/). Nous pouvons noter que d'autres exemples existent, notamment [The Electric Book workflow](http://electricbook.works/) développé par [Fire and Lion](https://fireandlion.com/).

### Des chaînes de publication monolithiques
La situation des chaînes de publication dans l'édition est la suivante&nbsp;:

- édition généraliste&nbsp;: utilisation des deux outils Word (traitement de texte) et InDesign (logiciel de publication assistée par ordinateur)&nbsp;;
- édition universitaire&nbsp;: chaînes de publication puissantes mais complexes (avec intégration d'XML TEI notamment).

Plusieurs constats peuvent être faits&nbsp;: une confusion entre structure et mise en forme, des étapes d'édition définitives (le retour en arrière est soit impossible, soit très compliqué), une dépendance à des outils fermés et monolithiques, une pérennité limitée, une difficulté de compréhension de la structuration des données, des formats illisibles sans les interfaces complexes qui vont avec, etc.

Nous pourrions résumer (très grossièrement, j'en conviens) la situation actuelle avec le schéma suivant – il faut noter ici le sens unilatérale des flèches&nbsp;:

![Schéma d'une chaîne de publication classique](/images/2018-livre-situation-actuelle.svg)

### Des chaînes de publication modulaires
Ce que Getty Publications a voulu mettre en place avec Quire, c'est répondre à un certain nombre de critères, dont les suivants&nbsp;:

- distinguer structure et mise en forme pour les rédacteurs/auteurs, avec un langage lisible par les humains&nbsp;;
- conserver le même format/fichier dans toutes les étapes&nbsp;;
- prévoir la réversibilité des étapes&nbsp;;
- générer différents formats&nbsp;: PDFs, EPUB, web, jeux de données, etc.

Le résultat schématisé donnerait ceci&nbsp;:

![](/images/2018-quire.svg)

Nous pouvons prendre l'exemple du livre _[Ancient Terracottas](https://www.getty.edu/publications/terracottas/)_ publié par Getty Publications&nbsp;:

- le livre est disponible sous différents formats ou versions – visibles depuis le menu du site dans la partie "Downloads"&nbsp;: site web, EPUB, PDF, version imprimée&nbsp;;
- certaines données peuvent également être exportée au format de tableur CSV – visibles depuis le menu du site dans la partie "Object Data"&nbsp;;
- les contenus de ce catalogue sont gérés dans des fichiers au format YML – un format de description de données très simples – ou au format Markdown dans le cas d'autres livres de Getty Publications&nbsp;;
- les sources sont gérées avec Git et la plateforme GitHub.

**Là où une chaîne _classique_ impose une certaine perméabilité des étapes d'édition, cette chaîne de publication modulaire permet aux différents intervenants d'être tous au même niveau. Un correcteur peut encore intervenir sur le texte après la mise en forme, un graphiste peut déjà préparer la maquette et la mise en forme avant la fin de l'écriture ou de la correction. Chacun peut se réapproprier le livre pendant sa conception et sa fabrication.**

## Permettre l’interaction sur un texte
Le troisième exemple est celui de [Distill](https://distill.pub/), une revue numérique sur le _machine learning_. L'originalité de cette revue de recherche universitaire est double&nbsp;:

- elle intègre des diagrammes interactifs – ou _reactive diagrams_ – qui permettent une meilleure compréhension des concepts présentés&nbsp;;
- les contenus des articles sont gérés comme du code, c'est-à-dire qu'ils sont versionnés.

Pour versionner les fichiers qui constituent les articles, Distill utilise la plateforme [GitHub](https://github.com/) – GitHub est un service d'hébergement et de gestion de développement de code, utilisant le système de contrôle de versions [Git](https://git-scm.com/). Chaque article a un dépôt dédié, chaque modification ou série de modifications est donc visible, comme on peut le voir dans le schéma ci-dessous.

![](/images/2018-distill-schema.svg)

Voici un exemple d'historique de version pour un article sur GitHub&nbsp;:

![Capture d'écran de l'historique de modifications d'un dépôt de Distill sur GitHub](/images/2018-distill-modifications.png)

![Capture d'écran de l'affichage de différences de deux modifications d'un fichier de la revue Distill sur GitHub](/images/2018-distill-modifications-02.png)

Cette gestion des contenus comme du code est possible justement parce que les données des articles sont structurées avec un langage de balisage, ici le format utilisé est HTML avec un schéma spécifique – précis mais relativement simple à manipuler –, Distill propose [un guide](https://distill.pub/guide/) pour comprendre son fonctionnement.

**Dans cet exemple tous les contributeurs peuvent être au même niveau&nbsp;: il n'y a pas de changement d'outil pendant les différentes phases de gestion des articles, les modifications sont visibles par toutes et tous, chaque personne peut s'approprier les contenus avec la contrainte de la compréhension d'un langage de balisage comme HTML et d'un système de gestion de versions comme Git – ce qui est tout de même un effort et un temps d'apprentissage non négligeable, mais cette possibilité existe.**
