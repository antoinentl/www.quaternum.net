---
layout: post
title: "Le livre web, une autre forme du livre numérique"
date: "2016-10-24T12:00:00"
comments: true
published: true
description: "Les livres web sont des livres sous la forme de sites web. Quelle est l'origine du micro-phénomène des web books, quels impacts sur la forme du livre et sur les pratiques de lecture, quelles perspectives pour les livres web face à l'écosystème du livre numérique ?"
image: "https://www.quaternum.net/images/2016-livre-web-livre-mini.png"
categories: 
- carnet
---
Les *livres web* sont des livres sous la forme de sites web. Quelle est l'origine du micro-phénomène des *web books*&nbsp;? Quels sont les impacts sur la forme du livre et sur les pratiques de lecture&nbsp;? Quelles perspectives pour les livres web face à l'écosystème du livre numérique ?

<!-- more -->

![Dessin d'un livre avec une URL inscrite sur la couverture, représentant maladroitement le concept de livre web.](/images/2016-livre-web-livre.png)

Depuis plusieurs années des auteurs de non-fiction utilisent le web comme forme et moyen de diffusion de leurs livres. En parallèle des livres numériques au format EPUB, des projets de livres web ont vu le jour en ligne et sont désormais des sources largement consultées ou citées dans leurs domaines. Mis à jour régulièrement, ce sont des livres en mouvement.

Ces initiatives indépendantes répondent à des logiques liées au web plus qu’à l’édition classique : rapidité de mise en ligne, diversité des mises en forme, souci du design numérique, facilité de diffusion notamment par le référencement, ouverture des contenus, possibilité de mises à jour et d’évolutions, accessibilité et détails typographiques.

Cet article fait suite à une intervention lors du colloque [Écrire, éditer, lire à l’ère numérique&nbsp;: design et innovation dans la chaîne du livre](https://ecridil.hypotheses.org/), qui s'est déroulé en avril 2016 à Nîmes. Ma proposition initiale est disponible [ici](/2016/03/07/le-livre-web-comme-objet-d-edition/), et le support de ma présentation lors de ce colloque, [là](http://presentations.quaternum.net/le-livre-web-comme-objet-d-edition/)).  
Le texte ci-dessous sera publié -- à quelques différences près -- dans les actes de ce colloque.

1. [Un web de documents](#1-un-web-de-documents)
2. [Une évolution de la publication numérique](#2-une-évolution-de-la-publication-numérique)
3. [Le livre web, une alternative formelle&nbsp;?](#3-le-livre-web-une-alternative-formelle)
4. [Les limites et perspectives de ce micro-phénomène](#4-les-limites-et-perspectives-de-ce-micro-phénomène)


## 1. Un web de documents

Originellement conçu comme un ensemble de documents liés entre eux, le web a singulièrement bouleversé nos pratiques d'écriture, de publication et de lecture.

### 1.1. L'origine du web
Le web est, à l'origine, pensé comme un écosystème de documents. Il est l'association&nbsp;:

- d'un langage sémantique (HTML) qui donne une structure logique aux contenus et qui utilise le lien hypertexte ;
- et d'un protocole de communication (HTTP) permettant l'accès à ces documents par le biais du réseau internet.

Encore aujourd'hui, le web conserve une logique liée aux contenus, dans sa forme, ses usages, mais également dans la manière dont il est conçu et produit. Devenant de plus en plus applicatif, des outils et services de publication apparaissent dans le paysage du web dès les années 2000, ouvrant les perspectives d'écriture, d'accès et d'appropriation de ces contenus[^1].


### 1.2. Une histoire de publication
L'histoire du web est donc une histoire de publication : sites universitaires, sites personnels, blogs, carnets en ligne, plateformes de partage de documents, etc.

Les outils et les services en ligne ont permis des pratiques d'écriture variées pour des publics divers grâce aux CMS[^2] -- *outils* -- puis aux plateformes de publication -- *services* : Spip, Wordpress, Typepad, Blogger, Medium, etc.


## 2. Une évolution de la publication numérique

Le web est une histoire de la publication débutée à la fin du vingtième siècle, l'économie du document numérique est donc récente, celle-ci repose sur des questions techniques et d'usages, empruntées au web.

### 2.1. L'arrivée du format EPUB
La mise en place d'un standard pour le format du livre numérique coïncide avec la volonté de disposer d'un écosystème structuré et d'un marché économique pour les publications numériques. Les bases de données payantes, la plupart scientifiques, sont -- presque -- aussi vieilles que le web, mais l'arrivée d'un marché numérique pour le format *livre* -- et non plus l'*article* -- a forcé les différents acteurs de l'écosystème du livre numérique à travailler de pairs pour permettre des expériences de lecture qui dépasse la simple consultation.

Le format EPUB[^3] a vu le jour au milieu des années 2000, et peut être résumé en quelques mots : il s'agit d'un ensemble de pages HTML, d'une feuille de style, de quelques fichiers -- images, sons, vidéos, polices typographiques -- et de métadonnées descriptives. Un mini site web, en somme, mais qui ne nécessite pas de connexion pour être consulté sur différents dispositifs : liseuses[^4], téléphones portables, tablettes, ordinateurs. L'EPUB est une solution pour gérer une collection de documents, ce que le web ne parvient pas à faire de façon aussi *arrêtée*.

L'enjeu le plus délicat autour du format EPUB est la compatibilité : comment réussir à obtenir un rendu équivalent sur différents dispositifs de lecture numérique ? En 2016, la simple combinaison de textes et d'images peut encore présenter des points d'achoppement tant en terme d'affichage que de qualité de rendu, alors que le web y parvient désormais avec beaucoup d'agilité. Par ailleurs, les applications de lecture numérique ne respectent pas toujours la mise en forme intégrée dans le fichier -- c'est ce que l'on appelle des *overrides*.

![Image de trois rendus du même livre numérique via trois applications différentes.)](/images/2016-trois-rendus.png)
*Figure 1. Trois rendus du même livre numérique via trois applications différentes. Note&nbsp;: [ce livre numérique est pourtant bien fait](http://jiminy.chapalpanoz.com/livre-gratuit-le-b-ba-du-livre-numerique/).*


### 2.2. Lier l'EPUB et le web
Le format EPUB et le web partagent, depuis la version 3 de l'EPUB, des fondations techniques communes : le HTML 5 pour le langage sémantique, et le CSS 3 pour la mise en forme des contenus. Le livre numérique bénéficie donc désormais des mêmes atouts que le web contemporain. Par ailleurs, les deux instances en charge des standards du livre numérique et du web se rapprochent et ont prévu de fusionner : l'IDPF et le W3C ont donc comme objectif de travailler étroitement pour le développement des standards de la publication numérique[^5].

Le livre numérique a effectivement besoin que le web vienne lui prêter main forte, à la façon d'un "grand frère"[^6]. D'une certaine façon, l'EPUB connaît aujourd'hui ce que le web a subit il y a une dizaine d'années : compatibilités incertaines, mauvaises pratiques des développeurs et/ou des applications/navigations, manque de standards et limitations formelles (principalement au niveau typographique, macro et micro)[^7].

### 2.3. Quelques limites
Le format EPUB présente beaucoup d'avantages, dont la portabilité, et de grands espoirs, dont l'accessibilité et l'interopérabilité, mais il faut bien constater plusieurs limites, qui résident dans le statut et les usages actuels de ce format :

- compatibilité avec les différents supports de lecture ou les interfaces de lecture : certaines fonctionnalités de la version 3 de l'EPUB ne sont par exemple supportées que par une minorité d'applications de lecture ;
- le format est encore trop dépendant de certaines grandes entreprises, notamment du fait de l'utilisation de DRM (mesures techniques de protection) ;
- les mauvaises pratiques sont courantes, et cela risque d'engendrer une dette technique dans les prochaines années[^8] ;
- les interactions à l'intérieur du livre numérique sont encore très limitées – l'usage de ces interactions posent par ailleurs des questions sur les expériences et les pratiques de lecture ;
- enfin, les innovations en terme de design restent très rares, et cela n'est pas tant dû au format en lui-même qu'à son implémentation dans les différentes applications de lecture.

L'utilisation massive du format EPUB était décisive pour voir émerger un marché du livre numérique, c'est désormais chose faite. Mais le format encore jeune doit continuer d'évoluer, et aussi et surtout son usage et son implémentation.


## 3. Le livre web, une alternative formelle&nbsp;?

Si le format EPUB est la réponse la plus cohérente et la plus prometteuse quand à la mise en portabilité des publications numériques, des alternatives peuvent être imaginées en terme d'accès et de forme (design graphique) : le livre web ou *web book* présente plusieurs atouts.

### 3.1. La question de l'accès
Le livre web est tout simplement un livre sous la forme d'un site web :

- un ensemble de pages structurées, organisées et rendues accessibles par un menu ou une table des matières ;
- des contenus qui s'adaptent formellement à la taille de l'écran, responsifs ou adaptatifs ;
- des liens hypertextes qui permettent de naviguer dans et en-dehors du livre.

Accessible en ligne, le livre web supprime l'un des grands problèmes du livre numérique, puisqu'il nécessite le logiciel le plus utilisé aujourd'hui tant sur ordinateurs que sur supports mobiles : un navigateur web. En dehors des liseuses, la grande majorité des dispositifs mobiles ne proposent pas toujours d'application de lecture numérique par défaut -- et je ne parle pas des problèmes de compatibilité entre ces différentes applications --, alors que le livre web peut être lu partout.

![Capture d'écran du livre web de Donny Truong, Professionnal Web Typography, https://prowebtype.com/](/images/2016-prowebtype-intro.png)
*Figure 2. Capture d'écran du livre web de Donny Truong, Professionnal Web Typography, [https://prowebtype.com](https://prowebtype.com/)*

Pour découvrir de nombreux exemples de *livres web* ou *web books*, voici une liste publiée sur GitHub, à augmenter&nbsp;: [https://github.com/antoinentl/web-books-initiatives](https://github.com/antoinentl/web-books-initiatives)

### 3.2. Maîtrise de la forme et de ses comportements
Si la maîtrise du rendu d'un site web était complexe il y a dix ans, aujourd'hui les technologies et les bonnes pratiques sont disponibles, et un même site web aura une forme presque équivalente entre différents supports et navigateurs. Par ailleurs, le rendu typographique sera fidèle aux intentions du concepteur, contrairement à une grande majorité de situations dans le cas du livre numérique.

![ Captures d'écran du livre web de Frank Chimero sur ordinateur, The Shape of Design](/images/2016-shape-of-design-laptop.png)  
*Figure 3. Captures d'écran du livre web de Frank Chimero sur ordinateur, The Shape of Design, [http://read.shapeofdesignbook.com](http://read.shapeofdesignbook.com/)*

![Captures d'écran du livre web de Frank Chimero sur support mobile, The Shape of Design](/images/2016-shape-of-design-mobile.png)  
*Figure 4. Captures d'écran du livre web de Frank Chimero sur support mobile, The Shape of Design, [http://read.shapeofdesignbook.com](http://read.shapeofdesignbook.com/)*


### 3.3. Possibilités d'interaction
Si le livre web présente un avantage indéniable en terme d'expérimentation et de rendu  graphiques, il faut ajouter à cela la possibilité d'ouvrir le livre à des contributions, et notamment par l'utilisation de Git via des plateformes comme GitHub ou GitLab, ou plus simplement via les commentaires des blogs ou carnets en ligne.

Brad Frost a intégré cette méthode lors de la rédaction de son livre *Atomic Design* : ce livre web est lisible en ligne, et il était possible de soumettre des corrections et modifications via la plateforme GitHub.

![Capture d'écran du livre web de Brad Frost, Atomic Design, et des commentaires concernant les corrections et modifications proposées par des internautes](/images/2016-atomic-design.png)  
*Figure 5. Capture d'écran du livre web de Brad Frost, Atomic Design, et des commentaires concernant les corrections et modifications proposées par des internautes, [http://edits.atomicdesign.bradfrost.com/chapter-1/](http://edits.atomicdesign.bradfrost.com/chapter-1/)*


## 4. Les limites et perspectives de ce micro-phénomène

Au-delà des innovations permises par le livre web, quelles sont les limites et les perspectives de ce micro-phénomène ?

![Dessin représentant maladroitement le croisement du web et du format EPUB.](/images/2016-livre-web-web-epub.png)

### 4.1. La question de la portabilité des livres web
Le principal défi du livre web est celui de la consultation hors connexion. Cette question est très bien résumée par l'initiative Offline First :

>Nous vivons dans un monde déconnecté et alimenté par des batteries, mais notre technologie et nos meilleures pratiques sont un vestige du passé continuellement connecté et rechargé. Le hors connexion est une caractéristique clé des applications web modernes. La conception d'objets numériques hors ligne doit puiser dans ce que nous avons appris du développement mobile et responsive[^9].

Des solutions techniques pour des consultations hors ligne de sites web plus ou moins complexes sont en cours de développement et d'implémentation[^10], notamment via la spécification technique Service Workers[^11]. Désormais, certains sites web, une fois une seule page consultée, sont disponibles en partie ou totalement hors connexion. Un système de mise en cache, à la façon du format EPUB, mais directement dans le navigateur, permet de gérer des contenus hors connexion. Encore en cours d'implémentation [sur certains navigateurs web](http://caniuse.com/#search=service%20worker), Service Worker sera bientôt utilisable partout. Vous pouvez par exemple couper votre connexion et consulter [cette page](/flux/) ou [celle-ci](/carnet/) de ce site web.

[Jeremy Keith](https://adactio.com/) a rendu disponible hors ligne son livre *[HTML5 For Web Designers](https://html5forwebdesigners.com/)*[^12]. Je vous invite à tester ce livre web, notamment si vous avez un *smartphone* sous Android&nbsp;:

- visiter la page d'accueil du site avec le navigateur web Chrome&nbsp;: [https://html5forwebdesigners.com](https://html5forwebdesigners.com/)&nbsp;;
- dans les paramètres du navigateur, sélectionner "Ajouter à l'écran d'accueil"&nbsp;;
- une icône vient s'ajouter à votre écran, comme une application&nbsp;;
- vous pouvez ensuite visiter ce site web comme une application, et sans connexion.


### 4.2. Des évolutions parallèles et éloignées de l'EPUB ?
Le concept de livre web ne se veut pas une solution parallèle au format EPUB, mais une alternative en terme d'accès et de design. Des initiatives existent déjà pour déployer conjointement des contenus sous la forme d'un site web, d'un livre numérique au format EPUB, et d'autres formats comme le PDF. [GitBook](https://www.gitbook.com/) propose ce type de service : cette plateforme met à disposition un certain nombre d'outils produisant ces formes diverses.

Nous pouvons donc assister à un changement de paradigme : les efforts de standardisation du format EPUB pourraient donc nourrir également le livre web par des services intermédiaires, centralisés (comme GitBook) ou distribuées.

Le web, dans le cas des *web books*, présente plusieurs limites, [soulignées par Hadrien Gardeur](https://twitter.com/Hadrien/status/790834369896349696) :

1. il n'existe aucun document qui lie les différentes ressources d'un même *livre web* pour indiquer qu'il s'agit d'une publication -- et plus précisèment d'un livre&nbsp;;
2. les métadonnées du web ne correspondent pas exactement à celle du livre&nbsp;;
3. la navigation est entièrement déclarative et liée aux pages sur lesquelles on navigue, contrairement au format EPUB qui définit très clairement cela&nbsp;;
4. l'expérience de lecture peut être déceptive, puisqu'on ne revient pas automatiquement au passage lu -- cela peut être détourné avec des accès par URLs et des ancres, et des pages courtes&nbsp;;
5. les réglages utilisateurs peuvent être limités, en partie en fonction de la conception du site web&nbsp;;
6. le livre web est potentiellement très instable, puisqu'il peut être mis à jour à tout moment.

Par ailleurs, un autre objectif serait de transformer facilement un EPUB en *web application*&nbsp;: un navigateur pourrait lire un EPUB, avec par exemple une gestion des contenus hors ligne. C'est l'objet des recherches d'[Hadrien Gardeur](https://twitter.com/Hadrien), avec cet exemple&nbsp;: [https://hadriengardeur.github.io/webpub-manifest/examples/MobyDick/index.html](https://hadriengardeur.github.io/webpub-manifest/examples/MobyDick/index.html). Pour plus d'informations sur ces tentatives "d'industrialisation et de standardisation du livre web", il *faut* lire le [Web Publication Manifest et ses projets liés](https://github.com/HadrienGardeur/webpub-manifest).

### 4.3. Le modèle économique
Le livre web est par définition ouvert, il n'est pas à lui seul une proposition de modèle économique, mais il est complémentaire d'autres formes du même contenu : livre numérique au format EPUB, livre papier, financements autres, etc. La plupart des livres web proposent une option de don, en prix libre, et avec ou sans contrepartie, ou reposent sur la vente des versions imprimées. Si les consultations et relais de ces publications web sont souvent nombreuses et positives, les sommes engrangées restent minimes[^13].

Le livre web est une forme complémentaire d'autres formes d'un même contenu, peut-être la première en terme d'accès, mais probablement pas la seule en terme de modèle économique. Nous pourrions imaginer le livre web comme une nouvelle étape d'un modèle hybride, pour le livre, sous cette forme -- en s'inspirant fortement de la proposition de Marin Dacos en 2009 [dans cette émission de Place de la Toile](https://archive.org/details/PlaceDeLaToilespcialeditionNumrique) (à écouter à partir de la 21e minute)&nbsp;:

- livre web pour faire connaître et pour découvrir un ouvrage, avec tout ou partie du contenu&nbsp;;
- version imprimée -- numérique ou à la demande en POD -- pour une lecture *déconnectée* et confortable, de tout ou partie de l'ouvrage&nbsp;;
- version EPUB pour une lecture *active* -- avec moteur de recherche interne, navigation aisée entre les chapitres, etc.

### 4.4. Des avant-gardes artisanales
Le concept de livre web représente pour le moment des initiatives majoritairement individuelles, artisanales – au sens noble du terme –, et peut-être isolées au regard du marché du livre numérique au format EPUB. Mais il faut noter que depuis plusieurs années des professionnels se réunissent autour de questions liées au livre web via l'événement [Books in Browsers](http://booksinbrowsers.org/), et un projet comme [Rebus](https://rebus.foundation/) s'inscrit également dans cette démarche[^14].


Le concept de web book ouvre des perspectives immenses en termes de design graphique, d'accès à l'information, de diffusion et d'accessibilité numérique.
Ces initiatives innovantes nourriront les évolutions du livre numérique au format EPUB, et la convergence de l'EPUB et du web[^15].


[^1]: Salaün, J.M., *Vu, lu, su*. Les architectes de l'information face à l'oligopole du Web, 2012, Paris, La Découverte.
[^2]: Pour Content Management System, ou Système de gestion de contenus.
[^3]: EPUB : acronyme de "electronic publication".
[^4]: La liseuse, par rapport aux autres dispositifs cités, présente la particularité d'avoir un écran à encre électronique qui augmente le confort de lecture et diminue la consommation d'énergie.
[^5]: DPF, "World Wide Web Consortium (W3C) and International Digital Publishing Forum (IDPF) Explore Plans to Combine", [http://idpf.org/news/world-wide-web-consortium-w3c-and-international-digital-publishing-forum-0](http://idpf.org/news/world-wide-web-consortium-w3c-and-international-digital-publishing-forum-0)
[^6]: Conférence sur le livre numérique par Jiminy Panoz lors de Paris Web, "Pourquoi le web devrait s'intéresser au livre numérique", [https://www.paris-web.fr/2016/conferences/pourquoi-le-web-devrait-sinteresser-au-livre-numerique.php](https://www.paris-web.fr/2016/conferences/pourquoi-le-web-devrait-sinteresser-au-livre-numerique.php)
[^7]: Butterick, M., "The Bomb in the Garden", 11 avril 2013, [http://unitscale.com/mb/bomb-in-the-garden/](http://unitscale.com/mb/bomb-in-the-garden/)
[^8]: Faucilhon, J., "Production de livres numériques (ePub) et dette technique", *Le Bloc-notes de Lekti*, 27 janvier 2016, [http://www.lekti-ecriture.com/bloc-notes/index.php/post/2016/La-production-de-livres-num%C3%A9riques-est-elle-homog%C3%A8ne](http://www.lekti-ecriture.com/bloc-notes/index.php/post/2016/La-production-de-livres-num%C3%A9riques-est-elle-homog%C3%A8ne)
[^9]: Proposition de traduction du manifeste Offline First, [http://offlinefirst.org](http://offlinefirst.org/)
[^10]: Fauchié, A., "Quatre bonnes raisons de faire du web hors connexion", *quaternum*, [https://quaternum.net/2016/06/23/quatre-bonnes-raisons-de-faire-du-web-hors-connexion/](https://www.quaternum.net/2016/06/23/quatre-bonnes-raisons-de-faire-du-web-hors-connexion/)
[^11]: W3C, Service Workers, [https://www.w3.org/TR/service-workers/](https://www.w3.org/TR/service-workers/)
[^12]: Keith, J., "Taking an online book offline", *Adactio*, 3 juin 2016, [https://adactio.com/journal/10754](https://adactio.com/journal/10754)
[^13]: Truong, D., "60 Days of ProWebType", *VISUALGUI*, 4 juillet 2015, [https://www.visualgui.com/2015/07/04/60-days-of-prowebtype/](https://www.visualgui.com/2015/07/04/60-days-of-prowebtype/)
[^14]: McGuire, H., "What books can learn from the Web / What the Web can learn from books", [https://medium.com/@hughmcguire/what-books-can-learn-from-the-web-what-the-web-can-learn-from-books-64670171908f#.cfq8wfa9j](https://medium.com/@hughmcguire/what-books-can-learn-from-the-web-what-the-web-can-learn-from-books-64670171908f#.cfq8wfa9j)
[^15]: idpf, "World Wide Web Consortium (W3C) and International Digital Publishing Forum (IDPF) Explore Plans to Combine", [http://idpf.org/news/world-wide-web-consortium-w3c-and-international-digital-publishing-forum-0](http://idpf.org/news/world-wide-web-consortium-w3c-and-international-digital-publishing-forum-0)