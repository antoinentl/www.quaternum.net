---
layout: post
title: "Workflows, formats"
date: "2016-01-04T21:00:00"
comments: true
published: true
categories: 
- carnet
---
Les formats sont au centre de nos flux de travail et de communication&nbsp;: interroger la notion de *workflow*, c’est interroger nos façons de travailler et de transmettre.  
Ce texte a été initialement publié dans le numéro 3 de [la revue *Après\Avant* des Rencontres internationales de Lure](http://www.r-diffusion.org/index.php?ouvrage=RDL-03), publié en mai 2015. Il a été remanié pour la publication sur quaternum.net.

<!-- more -->

## Courte définition du concept de “workflow”
Un *workflow* est un processus comprenant différentes étapes pour réaliser une tâche. Dans le *workflow* ce qui importe ce sont les articulations qui lient ces étapes. Prenons un exemple concret&nbsp;: quel est le *workflow* pour un magazine papier entre la première réunion de rédaction et la fabrication du support papier&nbsp;? Nous pouvons déjà imaginer des étapes telles que la discussion et l’attribution de tâches, la rédaction ou les notes prises sur papier, la dactylographie des textes, la circulation des fichiers numériques pour relectures, corrections et validation, la mise en page à partir de ces fichiers, la réalisation d’un PDF pour l’imprimeur, l’impression sur
support papier, la distribution dans les kiosques, etc. Voilà quelques étapes du *workflow* d’un magazine ou d’une revue, et c’est par ailleurs un peu celui de la revue [*Après\Avant*](http://delure.org/les-a-cotes/revue-apres-avant).

## Pourquoi parler de formats&nbsp;?
Dans les domaines de l’écrit et de l’image, lorsqu’on aborde la question des processus de création, de production et de diffusion – donc de *workflows* – on en vient assez vite à parler de formats. Le format est la manifestation des protocoles qui rendent lisible ­– pour un humain et/ou une machine – une information inscrite.

La liste des formats est longue, on peut en revanche délimiter assez facilement les *types* de format&nbsp;: physiques, analogiques, numériques, etc. Si l’on reprend l’exemple de la revue, on peut faire une liste des formats utilisés pour inscrire des informations, et des outils associés. Je m'appuie ici sur les pratiques, observées, des membres des Rencontres de Lure&nbsp;: graphistes, chercheurs, designers, éditeurs, etc.

### Textes écrits sur supports physiques
Les feuilles volantes, cahiers, carnets, tableaux, post-its, etc.

Les **carnets personnels** sont des outils intimes où les idées prennent forme, avant de devenir un élément d’une réflexion collective. À la fois brouillons, dispositifs de notes, espaces de création et possibles archives.

Les **post-its** sont des espaces réduits où s’inscrivent des idées courtes et souvent fortes. Outil de visualisation, ils s’articulent avec le paperboard et permettent aussi d’inscrire des contenus issus des carnets, dans le collectif. Le post-it est un dispositif temporaire, voué à disparaître au terme d’une réflexion en cours.

Le **paperboard** permet une inscription pour une visualisation plus globale, c’est par exemple un bon cadre pour réunir des post-its. L’inscription
s’y fait à un ou plusieurs, la grande taille des feuilles de papier ouvre des perspectives d’expression et de rassemblement – des post-its par exemple – mais le contenu qui y est inscrit sera ensuite synthétisé ailleurs, le paperboard étant lui aussi un dispositif temporaire.

### Textes inscrits dans des fichiers
Les habituels fichiers .txt, .doc, .odt, etc. Mais également les textes communiqués et éditables à plusieurs&nbsp;: pads en ligne, Google Docs, fichiers transmis par Dropbox ou autres, etc.

Un **pad** est un traitement de texte simplifié en ligne, où plusieurs personnes peuvent intervenir en même temps&nbsp;: les modifications sont identifiées selon leurs auteurs, par une couleur -- l'association Framasoft propose [Framapad](http://framapad.org/), outil libre. C’est un outil pour prendre des notes à plusieurs, le texte pouvant évoluer, être modifié et corrigé, en temps réel. Les options de mise en forme se limitent à ce que l’on peut faire avec du HTML&nbsp;: niveaux de titre, gras, italique, taille du texte. Son usage est temporaire, l’outil étant limité par les options de structuration et de mise en forme. Le contenu d’un pad est rarement exporté – ce qui permettrait pourtant de conserver les propriétés de structuration –, son contenu est tout simplement copié puis collé dans un autre dispositif.

**Google Docs** est à la fois une suite d’outils d’édition (traitement de texte, tableur, etc.) en ligne et collaboratifs, mais c’est aussi un outil de
synchronisation (sauvegarde de fichiers sur les serveurs de Google, et via plusieurs dispositifs). Pour la partie édition en ligne, Google Docs
propose les mêmes fonctions qu’un pad, mais avec des options de mises en forme presque aussi puissantes que les outils des suites bureautiques classiques, ce qui lui donne à la fois une fonction de prise de note, mais aussi potentiellement d’édition, de mise en forme et d’archivage.

### Textes mis en forme dans des logiciels de PAO
**Adobe InDesign** – l’un des logiciels les plus utilisés dans la Publication assistée par ordinateur – est rarement utilisé dans des étapes de prise de notes ou d'élaboration, certains professionnels *de l'image* utilisent un outil comme TextEdit avec le format [RTF](https://fr.wikipedia.org/wiki/Rich_Text_Format). Ce format permet de mettre en forme des documents sans risque de problèmes de compatibilité. Il s’agit d’une prise de notes qui peut intégrer un premier niveau de mise en forme, mais celle-ci relève en général d’indications visuelles et non de structuration – les mises en forme n’étant pas reprises telles quelles.

**Markdown** est utilisé à la marge, et plutôt de façon individuelle – alors que ce format se révèle utile dans le cadre d’un partage de documents, puisque cette sémantique ne dépend pas des outils d’édition.

Adobe InDesign est utilisé assez vite dans l’étape de production d’un document, et principalement pour des questions de visualisation de l’information. C’est finalement un outil par défaut pour les graphistes, plus habitués à manipuler un dispositif comme celui-ci que des outils de structuration des contenus à part entière.

## La communication au centre des workflows
On l’aura compris, le *workflow* est la description et la formalisation des moyens de communication entre humains, entre machines, et entre humain et machine. L’objectif est d’arriver à trouver des formats qui permettent la communication, et donc la réalisation d’un processus qui comporte
différents noeuds de communication. L’ensemble de ces protocoles d’inscription et de communication définit le concept de *workflow*. Ce qui est énoncé jusqu’ici semble évident. Nous évoluons dans un monde de formats où nous avons acquis des automatismes qui nous font oublier que partager une page
web n’est pas forcément une opération simple, ou imprimer un document au format A4 aux États-Unis n’est pas si facile. Si je dispose d’une information sur papier, comment la communiquer rapidement à un interlocuteur physiquement éloigné de moi&nbsp;? Si le fax était un moyen répandu pour diffuser cette information, aujourd’hui nous aurions plutôt tendance à prendre une photo du texte ­– avec un appareil photo, un téléphone ou un scanner – et à envoyer le fichier image par courrier électronique. Si le document papier comporte des éléments importants dans sa mise en forme, nous aurions peut-être le réflexe de l’envoyer par voie postale, ne gardant comme copie qu’une image du document original. Toute la question est de savoir quelle sera la façon la plus simple pour partager une information, un document, des contenus, d’intégrer de nouveaux éléments dans ce flux pour ensuite communiquer la nouvelle information augmentée. Aborder la question du *workflow*, c’est parler de transmission.

## Qu’est-ce que le numérique pose comme question(s)&nbsp;?
Internet, le Web et les terminaux qui permettent leur fonctionnement facilitent nos processus de création et de production de documents, ainsi
que les moyens de les faire circuler. Logiciels de traitement de texte, de mise en forme — PAO —, applications de partage de documents ou de travail collaboratif, le *numérique* offre des outils dont nous aurions du mal à nous séparer aujourd’hui. Lorsque nous regardons les *workflows* que nous mettons en place dans nos vies professionnelles, nous constatons que nous sommes désormais dans des “jardins fermés”, aussi simples à utiliser qu’aliénant&nbsp;: utiliser la suite d’Adobe, de Illustrator à Photoshop et InDesign, en passant par le réseau social Behance, désormais la propriété d’Adobe, a forcément une incidence sur nos pratiques. La suite Adobe étant désormais disponible en abonnement, que se passe-t-il si je ne peux pas payer ce mois-ci&nbsp;? Lorsque les serveurs d’Adobe se font pirater, dois-je craindre pour ma carte bancaire ou les travaux en cours destinés à mes clients&nbsp;? Si la majorité des graphistes occidentaux utilisent la suite Adobe, cela n’a-t-il pas une incidence sur leurs productions et leurs identités graphiques ([voir l'article de Kévine Donnot sur ces questions](http://www.cnap.fr/code-design))&nbsp;? Le fait que des formats utilisés très majoritairement par une profession ne soient pas standardisés ne pose-t-il pas question en terme de partage, de passage d’une application métier à une autre, et de pérennité&nbsp;? Les outils ne sont pas neutres, ils influencent nos usages et nos méthodes.

Remettre en cause nos processus de travail, c’est imaginer des façons créatives de communiquer&nbsp;: changer d’outil selon le projet, revenir à des
méthodes plus traditionnelles comme l’envoi postal, être attentif aux contraintes d’un projet et non à celles des outils installés sur nos machines.  
Remettre en cause nos processus de travail, c’est penser la question de la transmission en tant que designers, c’est placer une certaine forme d’indépendance au centre de nos démarches de communication, de nos *workflows*.

*Retrouvez cet article dans le numéro trois de la superbe revue des Rencontres internationales de Lure, Après\Avant&nbsp;: plus d'informations sur le site des Rencontres de Lure : [http://delure.org/les-a-cotes/revue-apres-avant](http://delure.org/les-a-cotes/revue-apres-avant)&nbsp;; acheter la revue chez votre libraire&nbsp;! Informations sur la disponibilité [par ici](http://www.placedeslibraires.fr/livre/9791092707038-apres-avant-n-03-revue-annuelle-de-culture-graphique-apres-avant/)&nbsp;; commander la revue sur le site du diffuseur, r-diffusion : [http://www.r-diffusion.org/index.php?ouvrage=RDL-03](http://www.r-diffusion.org/index.php?ouvrage=RDL-03).*