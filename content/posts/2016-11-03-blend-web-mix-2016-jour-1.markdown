---
layout: post
title: "Blend Web Mix 2016 : jour 1"
date: "2016-11-03T14:00:00"
comments: true
published: true
description: "Quelques notes sur la première journée de conférences de Blend Web Mix 2016 : bots, stratégie globale, blockchain, PWA, accessibilité et dette technique."
categories: 
- carnet
---
Quelques notes sur la première journée de [Blend Web Mix 2016](http://www.blendwebmix.com/), les conférences dédiées au web qui se sont déroulées à Lyon les 2 et 3 novembre 2016&nbsp;: bots, stratégie, blockchain, PWA, accessibilité et dette technique.

<!-- more -->

## À la rencontre du bot. Laurent Guitton
[Laurent Guitton](https://twitter.com/webDevLint) a exploré l'évolution des *bots* et des *chatbots*, ces robots utilisant l'intelligence artificielle et avec lesquels les humains interagissent à travers des logiciels ou services web. L'intervention de Laurent Guitton, très claire et très pédagogique, s'est déclinée en plusieurs parties&nbsp;: histoire des bots, arrivée des *chatbots*, usages concrets, stratégie, outils, etc.

J'ai noté deux idées intéressantes&nbsp;:

- la stratégie de création d'un *bot* repose sur deux étapes principales&nbsp;: déterminer un certain nombre de scénarios, et gérer les conditions ;
- "un usage est fait pour être perverti ou détourné"&nbsp;: si les chatbots sont souvent utilisés pour accompagner les utilisateurs, on peut imaginer d'autres usages, et à ce titre [uxchat.me](http://uxchat.me/) en est un bon exemple.

La vidéo de la conférence est disponible en ligne : [https://www.youtube.com/watch?v=9yZImYHCJ-g](https://www.youtube.com/watch?v=9yZImYHCJ-g).


## De Start-up à Scale-up&nbsp;: les leçons d’Evernote. Xavier Delplanque
[Xavier Delplanque](https://twitter.com/xavierdelplanq) a réalisé un panorama très complet de l'histoire d'Evernote, cette application pour enregistrer et gérer ses notes et ses projets&nbsp;: stratégie de développement, méthodes pour faire évoluer l'application, remises en cause, bilan, etc.

Deux idées m'ont marqué&nbsp;:

- à un moment donné, pour *forcer* les utilisateurs à utiliser toutes les potentialités de l'application, les équipes d'Evernote ont supprimé l'option de *pièce jointe*, obligeant la création et la gestion de documents *dans* Evernote. Plutôt que d'être un simple support de stockage et de partage de documents, l'application est donc (re)devenue une plateforme d'édition et de gestion de documents ;
- Xavier Delplanque a évoqué le concept d'*expérience frustrante* pour forcer les utilisateurs à passer d'un abonnement *gratuit* à un abonnement *payant*. Si ce concept est encore très flou, comme l'a souligné Xavier lui-même, il mérite qu'on s'y attarde plus longuement. Peut-être peut-on parler de *bad ux*.

Xavier Delplanque a retenu trois leçons de son expérience chez Evernote&nbsp;:

1. collectez de la donnée&nbsp;: même si l'on n'a pas d'usage immédiat, cela peut servir plus tard ;
2. expérimentez&nbsp;: il ne faut pas avoir peur de tester. Complément&nbsp;: tester sur un échantillon, et fonctionner par petites itérations ;
3. Keep it Simple (Stupid)&nbsp;: il faut concevoir et maintenir un produit simple.

La vidéo de la présentation de Xavier Delplanque est disponible en ligne : [https://www.youtube.com/watch?v=IkwLGCCuYjI](https://www.youtube.com/watch?v=IkwLGCCuYjI).


## La blockchain, quand l'individu sert au collectif... malgré lui. François Zaninotto
Ce concept à la mode méritait bien une conférence de cinquante minutes, et [François Zaninotto](http://www.redotheweb.com/) a réussi ce pari avec pédagogie et humour. Quelques points notés&nbsp;:

- la blockchain est une association de concepts/technologies&nbsp;:
  - peer to peer&nbsp;: modifications répliquées de proche en proche sur tous les noeuds, *circulation de l'information* ;
  - asymetric cryptographic&nbsp;: *authentifier la transaction* ;
  - proof-of-work consensus&nbsp;: *légitimer la transaction* ;
- la technologie blockchain est immature, déshumanisée, et peu sécurisée -- ce qui est un vrai problème, puisque la philosophie de la blockchain est de supprimer les intermédiaires de confiance.

>La blockchain c'est un peu des ordinateurs qui jouent au diams.  
François Zaninotto

La conférence de François Zaninotto est disponible en ligne : [https://www.youtube.com/watch?v=JaAHbWpl9Ro](https://www.youtube.com/watch?v=JaAHbWpl9Ro).

## Les Progressive Web Apps expliquées aux décideurs. Antoine Contal
Les Progressive Web Apps, ou PWA, commencent à être connues, il y a déjà eu de [nombreux billets](https://frank.taillandier.me/2016/08/09/argumentaire-commercial-pour-les-progressive-web-apps/) et des conférences sur le sujet -- [dont celle de Hubert Sablonnière lors de Paris Web 2016](https://www.paris-web.fr/2016/conferences/progressive-web-apps-le-futur-du-web-arrive.php). [Antoine Contal](https://twitter.com/antoine_contal) a abordé les PWA sous l'angle de la stratégie.

Voici l'approche utilisée par Antoine Contal, qui permet d'avoir une bonne vision de ce que sont les PWA&nbsp;:

1. avant&nbsp;: des logiciels pour réaliser des manipulations *complexes*, et des *web apps* comme le webmail facile à maintenir mais déceptives en matière d'expérience utilisateur ;
2. puis&nbsp;: arrivée d'Ajax avec des interactions plus importantes sur le web, mais limitation par rapport aux applications mobiles ;
3. aujourd'hui&nbsp;: combat sur mobile entre le web (mobile) et les applications, avec le fameux "The Web is Dead" de The Wired en 2010 ;
4. le web peut être amené au niveau des applications, avec des fonctionnalités similaires&nbsp;: rapidité, fonctionnement hors ligne, accès sur la *home*, etc. mais cela reste un site web (mobile) ;
5. l'une des clés est le concept de *progressive enhancement*&nbsp;: une amélioration progressive en fonction du contexte d'utilisation (appareil, navigateur, qualité de la connexion, etc.).

Dommage que deux idées n'aient pas été plus développées ou abordées&nbsp;:

- les PWA permettent de palier à des contextes d'utilisation *imparfaits*&nbsp;: dispositifs -- *smartphone*, tablettes, etc. -- peu performants, connectivité imparfaite, etc. Antoine Contal a évoqué cette idée sans s'y attarder très longtemps. Les PWA peuvent être une solution à l'approche *progressive enhancement*, et non l'inverse ;
- les PWA peuvent être un moyen de concevoir des sites et services web accessibles.

La conférence d'Antoine Contal est disponible en ligne : [https://www.youtube.com/watch?v=AvWBJeb0v9A](https://www.youtube.com/watch?v=AvWBJeb0v9A).


## Accessibilité et utilisabilité&nbsp;: astuces d’intégrateur pour webdesigners. Véronique Lapierre
15 minutes de vulgarisation sur l'accessibilité et la qualité web par [Véronique Lapierre](http://webetcaetera.net/)&nbsp;: claires, synthétiques, efficaces.

Un concept soulevé par Véronique Lapierre&nbsp;:

>On ne corrige pas en matière d'accessibilité, on conçoit.  
Laurent Denis

L'accessibilité est donc du ressort de toute une équipe, et pas uniquement de l'intégrateur ou du développeur. Il faut donc prendre en compte cette contrainte dès la conception d'un projet, voici quelques exemples&nbsp;:

- dimensions des zones réactives (liens, boutons, cases à cocher, etc.) ;
- bouton pause sur les *slideshows* ;
- fil d'Ariane.

Le support de présentation de Véronique Lapierre est disponible en ligne&nbsp;: [http://webetcaetera.net/a11y/](http://webetcaetera.net/a11y/)

L'intervention de Véronique Lapierre est disponible en ligne : [https://www.youtube.com/watch?v=HyTSjg6u2GQ](https://www.youtube.com/watch?v=HyTSjg6u2GQ).


## To patch or not to patch. Bastien Jaillot
Si l'intitulé de cette conférence pouvait laisser penser que [Bastien Jaillot](https://twitter.com/bastnic) parlerait de technique, ce n'était pas le cas. C'est le point essentiel -- d'après moi -- de cette présentation, l'humain dans les projets&nbsp;:

- privilégier les valeurs partagées à l'excellence technique&nbsp;: il est préférable de travailler avec des personnes qui partagent les mêmes valeurs, ou qui *vont* partager les mêmes valeurs, plutôt que chercher des profils techniques. la technique s'apprend ;
- "sur-coder ça vous rend indispensable (et ce n'est cool pour personne)", ce n'est pas une bonne stratégie que de se rendre indispensable, à court, moyen ou long terme&nbsp;:
  - pendant le projet cela prend plus de temps ;
  - lors de la mise en production ou de la maintenance les sollicitations seront nombreuses -- et probablement douloureuses ;
  - après le projet l'équipe ne retiendra que la dépendance et la lourdeur...

Bastien Jaillot est par ailleurs l'auteur du livre [La dette technique](http://boutique.letrainde13h37.fr/products/la-dette-technique-bastien-jaillot), dont j'avais parlé [ici](/2016/02/24/dette-technique/).

La conférence de Bastien Jaillot est visible en ligne : [https://www.youtube.com/watch?v=s3d_wE_nG30](https://www.youtube.com/watch?v=s3d_wE_nG30).

**Les notes de la deuxième journée de Blend Web Mix sont à retrouver [en suivant ce lien](/2016/11/04/blend-web-mix-2016-jour-2/).**