---
comments: true
date: 2011-10-10
layout: post
slug: quelle-valeur-ajoutee-a-la-veille
title: Quelle valeur ajoutée à la veille
categories:
- carnet
---
Le 27 septembre dernier Lionel Dujol [s'est posé la question](http://twitter.com/#!/lioneldujol/status/118661666719277056) de l'intérêt de continuer à alimenter (la plupart du temps automatiquement), son fil twitter. Quelques réactions rapides s'en suivent sur Twitter, puis sur Facebook, et enfin [un billet sur son blog](http://labibapprivoisee.wordpress.com/2011/10/05/etats-dame-sur-la-diffusion-de-ma-veille/), le 5 octobre.  
Lionel Dujol parle de surinformation, il ne voit pas l'intérêt d'en rajouter une couche avec de multiples liens diffusés sur Twitter, en plus de façon automatique. Pourtant, certains (comme moi) suivent ces dépêches avec intérêt, même s'il s'agit en effet d'information froide, sans valeur ajoutée. C'est tout l'avantage et l'inconvénient de Twitter, une information diffusée facilement (voire automatiquement grâce à des outils comme [dlvr.it](http://dlvr.it/) ou [ifttt](http://ifttt.com/)), rapidement et massivement. Et l'outil semble surtout pertinent pour ceux qui reçoivent l'information, même si le seul titre d'un article/billet est parfois trop court, on trie et on sélectionne rapidement, mais ailleurs, sur un autre espace que Twitter. Car, comme l'explique très bien Lionel, impossible d'en dire plus sur Twitter qu'un titre et un lien.  
Lionel Dujol a trouvé un bon moyen de rendre compte de sa veille : [un billet hebdomadaire sur son blog](http://labibapprivoisee.wordpress.com/category/la-veille-apprivoisee/) avec une poignée de liens commentés, parmi les centaines diffusés via Twitter. Marie D. Martel, comme d'autres, [avait montré la voix quelques semaines plus tôt](http://bibliomancienne.wordpress.com/2011/09/16/la-nanoveille-no-1/).

Donc Twitter permet de diffuser des liens (en fait les [favoris de Lionel sur Google Reader](https://www.google.com/reader/shared/lionel.dujol), renvoyés automatiquement grâce à dlvr.it), mais quel outil pour trier et pour commenter, avant de passer aux phases de sélection puis de diffusion ? [Diigo](http://www.diigo.com/) joue justement très bien ce rôle, avec la possibilité de commenter un lien. [Le compte de Lionel](http://www.diigo.com/user/Lioneldujol) se ré-anime il y a quelques jours, Silvère Mercier parle même [d'une autre solution avec Evernote](http://twitter.com/#!/Silvae/status/122321275493416960).
L'assemblage paraît presque parfait :
	
+   un agrégateur pour surveiller (avec ou non l'utilisation additionnel d'un compte Twitter comme dans mon cas) ;
+   un gestionnaire de marques-pages pour la première étape de tri, avec commentaires pour justifier les choix (donc uniquement les articles lus, même synthétiquement) ;
+   une deuxième étape de tri par le biais de listes ou de groupes (Diigo permet justement une très bonne gestion collaborative par groupes) ;
+   et une diffusion sur un billet de blog. L'intérêt du billet étant notamment la clarté de l'interface de lecture (comparée à une liste sur Diigo, certes pratique mais imbuvable), la possibilité de commenter pour n'importe qui, et le référencement accru.

Je n'aborderai pas Facebook ou Google plus, puisqu'il s'agit, encore plus que pour des services comme Diigo ou Twitter, d'espaces fermés et clos. On comprend l'intérêt de recueillir des commentaires (précisions, critiques, échanges) via ces deux réseaux sociaux, mais cela ne peut être qu'un outil intermédiaire et non un réel espace de publication, à mon sens en tout cas, mais cela est une autre question.