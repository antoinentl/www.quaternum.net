---
comments: true
date: 2012-01-09
layout: post
slug: themes-wordpress-et-interfaces-de-lecture
title: Thèmes Wordpress et interfaces de lecture
categories:
- carnet
---
Depuis quelques jours je cherche un thème Wordpress pour ce carnet, c'est l'occasion de déterminer un peu ce que je veux faire en termes formels et d'accès au contenu (rien de bien exotique toutefois). Voici quelques contraintes et exigences, à la fois pour le choix du thème et sa personnalisation :

Contenus de ce blog :
	
+   principalement du texte ;
+   donc peu d'images a priori ;
+   des liens hypertextes ;
+   des citations.

Accès au contenu :

+   privilégier un accès direct aux billets sans utiliser d'introduction ou de chapeau ;
+   et donc mettre en avant le plein texte directement ;
+   retrouver les articles d'une même catégorie facilement ;
+   un menu latéral ou en pied de page (pour l'accès aux catégories, aux articles, à quelques pages statiques...).

Paramètres de style :

+   largeur flexible, permettant une lecture sur différentes tailles d'écran ;
+   modification possible de la feuille de style ;
+   accent sur la typographie ou tout du moins le choix de la police ;
+   choisir une taille de police qui permet une lecture agréable sur tous supports ;
+   pas d'utilisation de pictogrammes ou d'images dans le thème par défaut ;
+   utilisation limitée de Java.

Enfin, le respect des standards W3C (XHTML ou HTML5) et de plusieurs normes d'accessibilité élémentaires est une contrainte incontournable.

Après quelques jours, j'ai trouvé [ce thème](http://leonpaternoster.com/wp-themes/) de Leon Paternoster. Quelques éléments ne me conviennent pas : quelques erreurs dans le code [apparemment](http://validator.w3.org/check?uri=http%3A%2F%2Fwww.quaternum.net%2F&charset=%28detect+automatically%29&doctype=Inline&group=0), et des paramètres encore difficiles à modifier (des détails).  
Je suis encore étonné de ne pas trouver de thème très simple qui réponde à ces critères... La solution serait évidemment de créer moi-même ce thème au combien simple et épuré, ou au moins d'adapter à mes besoins un thème déjà très simple/basique. Le problème étant que mes connaissances en PHP et en CSS sont un peu limitées, cela simplifierait pourtant cette recherche...