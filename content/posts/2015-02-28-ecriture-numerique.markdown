---
layout: post
title: "Écriture numérique ?"
date: 2015-02-28
published: false
comments: true
categories: 
- carnet
---
Je rebondis sur la note de David [sur le livre et l'email](https://larlet.fr/david/blog/2015/livre-email/), en écho à mon sens [au billet d'Emmanuel](http://emmanuel.clement.free.fr/blog/index.php/post/2014/12/31/D%C3%A9charge-cognitive), mais aussi à deux réflexions personnelles — [ici](/2014/01/02/construire-du-sens/) et [là](/2015/02/14/pratiques-de-lecture/).

>Là où j’avais l’impatience de publier un tweet, je retrouve la sérénité de ne publier qu’une seule note, choisie avec soin, parfois/souvent rédigée à l’avance. Ne pas céder à l’émotion, à la réponse facile, à la blague foireuse. *Imaginez si vous vous contraigniez à ne publier qu’une seule chose par jour.* Quelle serait-elle ?  
[David Larlet](https://larlet.fr/david/blog/2015/livre-email/)

## Le web : lecture/écriture
La nécessité d'écrire et l'envie d'ouvrir ce site se sont faites à la lecture de la poignée de carnets que je suis depuis plus de trois ans. Peut-être de la même façon que ma pratique du dessin s'est établie lors de la découverte de certains auteurs et de leurs traits.  
Le fait est que plus je lis et plus j'écris — la plupart du temps des bribes pour moi-même — et plus je lis les carnets web et plus je souhaite publier, *en ligne*.

## Un journal
Fin 2013 je fais un constat similaire à celui de David, celui de la nécessité de sortir des silos pour créer mon espace de publication — en dehors de réseaux comme Twitter ou [Readmill](/2013/10/14/readmill-et-medium-le-livre-et-le-web/). Depuis une quinzaine de mois j'écris un court *billet* par jour. Souvent une citation, une phrase, ou un texte plus développé. D'abord dans un carnet papier avant d'être publié sur ma machine *en local*. J'hésite encore *à mettre en ligne* ces bribes, principalement parce que ça ne présente pas beaucoup d'intérêt, ensuite pour des raisons de *dévoilement*. L'expérience est par ailleurs absolument passionnante, je la conseille fortement — avec cette seule contrainte d'écrire tous les jours.  
Ma démarche est beaucoup plus personnelle que celle de David, ma première nécessité n'est pas de partager, mais d'inscrire des fragments, de conserver une trace d'une lecture, d'un échange, d'une idée. D'une certaine façon ce carnet est la version aboutie de ces *miettes*.

## Publier aujourd'hui
Ces questions de publication, de journaux plus ou moins personnels, doivent être approchées de celles liées aux évolutions du livre. Ce que je lis le plus aujourd'hui, ce sont les carnets de [David](https://larlet.fr/david/), [Karl](http://www.la-grange.net/), [Bert](http://www.myowncottage.org/), [Emmanuel](http://emmanuel.clement.free.fr/blog/), [Éric](https://n.survol.fr/) ou [Sacha](http://www.sachagoerg.com/). Mes lectures de livres sont totalement mêlées à celles de ces carnets, tout comme mes notes sont autant dans un carnet papier, dans des fichiers textes ou en ligne.  
Publier aujourd'hui, c'est par exemple s'appuyer sur le web, avec des moyens simples comme des sites web *adaptés*, des mails envoyés
