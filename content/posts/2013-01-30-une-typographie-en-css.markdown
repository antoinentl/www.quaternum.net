---
comments: true
date: 2013-01-30
layout: post
slug: format-de-typographie
title: Une typographie en CSS ?
categories:
- carnet
---
>Créer des scripts ou des applications est un effort pour aller au-delà des limites du logiciel et pour établir des structures permettant de créer des polices pouvant réagir plus souplement aux diverses demandes de chaque nouveau projet.  
[Peter Bilak, Le caractère typographique comme programme, page 130](http://www.ecal.ch/publications.php?id=1300)

[Geoffrey Dorne signale](http://graphism.fr/des-icnes-libres-en-css-full-patate "Lien vers graphism.fr") une expérimentation intéressante&nbsp;: des icônes générées par quelques lignes de codes CSS, en alternative à l'utilisation d'images. Ça s'appelle [One div](http://one-div.com/ "Lien vers One div"). La vectorisation des images permet déjà l'adaptation de celles-ci aux différents formats et résolutions d'écrans. Des icônes en full CSS règlent le problème du poids des images et des requêtes aux serveurs, tout en posant une contrainte&nbsp;: le risque que l'affichage ne soit pas similaire sur tous les navigateurs, voir même qu'il n'y ait pas du tout d'affichage sur certains navigateurs - One div indique d'ailleurs la compatibilité avec les cinq principaux _browsers_ pour chaque icône.

Pourquoi pas ne pas imaginer le même type de fonctionnement pour la typographie sur le Web&nbsp;: une génération dynamique avec un format/langage compréhensible par les humains. Tout comme c'est déjà le cas pour [certains détails typographiques sur le Web](https://www.quaternum.net/2012/02/07/la-necessite-du-detail/ "Lien vers quaternum") (gestion des marges, césures, styles des paragraphes...).  
En terme de format de typographie il y a une initiative intéressante qui existe déjà&nbsp;: [UFO (Unified Font Object)](http://unifiedfontobject.org/ "Lien vers le site d'UFO") est un format typographique ouvert, à base d'XML, lisible par les humains, permettant d'encapsuler des métadonnées, et utilisé principalement dans des applications telles que KalliCulator ou Font Constructor.  
Une typographie en CSS serait surement extrèmement lourde, certains glyphes nécessiteraient peut-être des dizaines de lignes de code. Mais l'idée est séduisante d'avoir un format "web" pour une utilisation de la typographie sur le Web.