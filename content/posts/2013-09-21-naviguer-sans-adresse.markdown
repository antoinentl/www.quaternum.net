---
layout: post
title: "Naviguer sans adresse"
date: "2013-09-21T10:55:00"
comments: true
categories:
- carnet
---
## Un navigateur pour le tactile
Il y a peu de temps Opera a sorti un nouveau navigateur pour iPad&nbsp;: Opera Coast. Les alternatives à Safari n'étant pas si nombreuses, cette arrivée était en soi une bonne nouvelle.  
Pour faire court, la particularité de ce *browser* est de simplifier la manipulation, et de faire un *vrai* navigateur adapté au tactile. On ne peut pas dire que Safari soit transcendant en terme d'interface, Chrome non plus d'ailleurs. Le mimétisme entre écrans *classiques* - ordinateurs pour faire court - et tablettes était jusqu'ici assez affligeant. Et donc Opera Coast est, à ma connaissance, la première expérimentation intéressante&nbsp;: faire un navigateur pour le *tactile*.  
La démarche ainsi que les fonctionnalités sont expliquées [ici](http://my.opera.com/ODIN/blog/introducing-coast-by-opera).  

## Une expérience étonnante
Je passerai rapidement sur ce qu'on appelle l'expérience utilisateur&nbsp;: après quelques minutes d'adaptation c'est fluide et agréable, l'interface épurée révèle le web, la lecture est immersive.  
Je ne tenterai pas de comparaison hasardeuse, mais passer de Safari à Coast c'est un peu comme découvrir [UberWriter](http://uberwriter.wolfvollprecht.de/ "Lien vers le site d'UberWriter") après LibreOffice&nbsp;: pour faire des tâches simples c'est un vrai plaisir, le superflu disparaît au profit de la lecture.  

## Naviguer sans adresse
Ce qui me perturbe le plus avec Coast c'est de naviguer *sans adresse*&nbsp;: l'URL des pages n'est affichée qu'au départ - un petit peu comme si on indiquait une adresse physique où l'on désire se rendre - ensuite le navigateur n'affiche que la page, sans bouton de navigation ni *barre d'adresse*. Si l'on passe d'une page à une autre d'un même site on ne voit donc pas l'adresse qui change, même chose si l'on sort d'un site pour se rendre sur un nouveau domaine.  
Je me doute que la plupart des utilisateurs se moquent de ce genre de détail, et dans mon entourage rares sont ceux qui tapent une URL dans la barre d'adresse d'un navigateur. Le passage par un moteur de recherche semble être la pratique la plus répandue, aussi connu soit le site sur lequel on se rend, la confusion entre navigateur et moteur de recherche étant parfois totale...  
Pourtant la lecture des URL me semble nécessaire, elle fait parti de l'environnement du web, elle permet de se repérer, d'appréhender une certaine géographie du web. Je ne sais quoi penser de cette évolution.  