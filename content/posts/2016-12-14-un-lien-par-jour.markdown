---
layout: post
title: "Un lien par jour : méthode de veille"
date: "2016-12-14T08:00:00"
comments: true
published: true
description: "Faire de la veille est un exercice aussi vital que délicat, un lien par jour est une méthode pour que cela reste une activité agréable et efficace !"
image: "https://www.quaternum.net/images/2016-un-lien-par-jour-mini.png"
categories: 
- carnet
slug: un-lien-par-jour
---
Faire de la veille est un exercice aussi vital que délicat, *un lien par jour* est une méthode pour que cela reste une activité agréable et efficace&nbsp;!

<!-- more -->

Cet article est écrit dans le cadre de [#nowwwel](https://twitter.com/search?f=tweets&vertical=default&q=%23nowwwel), les [24 jours de web](http://www.24joursdeweb.fr/) 2016 décentralisés -- voir l'[article de Rémi à ce sujet](http://www.hteumeuleu.fr/en-decembre-ecrivez-partagez-hashtag-nowwwel/).

![Illustration représentant la chaîne de veille d'Antoine (flux RSS, navigateur, site web)](/images/2016-un-lien-par-jour.png)

## La veille, pourquoi faire&nbsp;?
Les domaines du numérique, et notamment le web, nécessitent de se tenir informé très régulièrement&nbsp;: évolutions technologiques, pratiques, usages, standards, méthodes, outils, débats, évolutions légales, etc. Faire de la veille, c'est s'assurer de suivre ce mouvement, de se tenir informé. Au-delà de pouvoir dire "ah oui je connais ce nouveau framework", il s'agit d'être en mesure de comprendre les enjeux qui y sont liés&nbsp;: est-ce que ce *nouveau framework* est réellement nouveau par rapport à d'autres outils similaires&nbsp;? quelle incidence va avoir cette nouveauté sur mes pratiques professionnelles&nbsp;?

Faire de la veille et la partager, c'est participer à une réflexion collective dans un domaine&nbsp;: signaler, réagir, tester, et permettre également à d'autres de faire de la veille -- je me base énormément sur la communauté pour réaliser [la mienne](/flux/).

Pour bien comprendre l'intérêt de la veille, il faut lire l'excellent article de Thibault Jouannic -- ou plutôt de la transcription de sa conférence à Paris Web 2015&nbsp;: **[La veille techno pour les vieux croûtons](https://www.miximum.fr/blog/veille-techno-vieux-croutons-paris-web-2015/)**

>Une veille techno intéressante, c'est une veille techno qui reste fun.

## Les trois étapes de la veille
Je sépare mon activité de veille en trois étapes (+ une étape bonus) qui se suivent chronologiquement&nbsp;:

1. **récolter**
2. **trier**
3. **enrichir et publier**
4. **diffuser**

### 1. Récolter

![Illustration représentant un lecteur de flux RSS/Atom, Twitter, des newsletters et des fenêtres de navigateurs](/images/2016-un-lien-par-jour-02.png)

Il s'agit du flux *entrant*, dans mon cas cela passe par trois voies&nbsp;: un lecteur de flux RSS, Twitter et des lettres d'information. La régularité est très variable&nbsp;: plusieurs fois par jour -- pour Twitter --, ou quelques fois par semaine -- pour mon lecteur de flux RSS ou les lettres d'information dans mes mails.

La méthode est simple&nbsp;: j'ouvre un nouvel onglet pour chaque lien qui m'intéresse, que ce soit sur mon laptop ou mon smartphone. Je ne lis pas l'article ou le billet immédiatement, je reviens dessus plus tard dans la journée ou la semaine. Mon navigateur -- sur mon ordinateur et mon téléphone -- est donc l'outil qui me permet de sauvegarder temporairement un lien. Il est déterminant d'adopter une certaine discipline par rapport aux onglets, j'évite désormais d'en conserver plus d'une trentaine d'ouverts&nbsp;: si je dépasse cette limite, je vérifie si chaque onglet va m'intéresser à court ou moyen terme, et si nécessaire je le conserve ailleurs.

Quelques ressources pour constituer une base de veille sur le domaine du web&nbsp;:

- l'excellente Web Development Reading List ou WDRL d'[Anselm Hanneman](https://helloanselm.com/)&nbsp;: [https://wdrl.info/](https://wdrl.info/)&nbsp;;
- une liste de blogs d'entreprises compilée par Éric&nbsp;: [https://docs.google.com/spreadsheets/d/1TvK18ZrFWSVe3ZnxywTcHmUZsVU4cggRTAUmC9S3Opw/](https://docs.google.com/spreadsheets/d/1TvK18ZrFWSVe3ZnxywTcHmUZsVU4cggRTAUmC9S3Opw/) ([David](https://larlet.fr/david/) a sauvegardé une version [ici](https://larlet.fr/static/david/blog/blogs-equipes-tech.csv) si vous n'arrivez pas à accéder au document Google)&nbsp;;
- une liste de carnets des gens du web, sous forme d'<abbr title="Outline Processor Markup Language">OPML</abbr>, le format d'import/export des lecteurs de flux RSS/Atom, est disponible sur GitHub (n'hésitez pas à contribuer&nbsp;!)&nbsp;: [https://github.com/antoinentl/les-carnets-des-gens-du-web](https://github.com/antoinentl/les-carnets-des-gens-du-web).

### 2. Trier

![Illustration représentant des fenêtres de navigateurs sur smartphone et ordinateur et la synchronisation](/images/2016-un-lien-par-jour-03.png)

C'est l'étape de tri des liens récoltés, elle est la plupart du temps partielle et rapide&nbsp;: lecture du chapeau, lecture en diagonale. C'est à ce moment que je détermine si un lien peut rentrer dans ma veille, et plus particulièrement dans le partage de celle-ci.

Je pars du principe que la veille peut être une activité (très) chronophage, mais je ne dispose pas de ce temps et faire trop de veille c'est s'assurer de ne rien *faire*... Il faut donc trouver un moyen d'aller vite. **Je ne retiens qu'un lien par jour**, ou plutôt 5 ou 6 liens par semaine, c'est pour moi l'assurance de lire en profondeur un article par jour, de le placer aux côtés des autres liens conservés, et d'y réagir au moins partiellement. En terme de temps passé, dans mon cas cela représente entre deux heures et quatre heures par semaine.

"Un lien par jour" est fortement inspiré de [l'expérience de David Larlet durant l'année 2015](https://larlet.fr/david/blog/2015/oiseau-bleu/).

### 3. Enrichir et publier

![Illustration représentant une chaîne de publication&nbsp;: d'un navigateur vers un éditeur de texte vers une page web](/images/2016-un-lien-par-jour-04.png)

La meilleure façon de faire de la veille efficace c'est de la publier et de la partager. Pendant plus de quatre ans [j'ai effectué une veille sur le numérique dans le domaine du livre](/2016/03/09/decompte/#veilles-numriques-envoyes), il s'agissait principalement d'un travail de signalement d'informations et de ressources à destination d'environ 800 professionnels du livre, sous forme de newsletter.

Aujourd'hui j'effectue une veille personnelle, par nécessité professionnelle et par plaisir -- plaisir de découvrir, d'inscrire et de partager. La forme de la publication de ma veille est assez basique&nbsp;: un billet/article par item/lien dans la partie `/flux` de [mon carnet en ligne](https://www.quaternum.net/), avec une mention particulière dans [mon flux RSS/Atom](/atom.xml). La structure de chaque billet est la suivante&nbsp;:

- une citation emblématique du lien que je veux partager&nbsp;;
- la mention de l'auteur et du titre de l'article, et le lien (URL)&nbsp;;
- un commentaire plus ou moins étoffé et original&nbsp;;
- éventuellement la source par laquelle j'ai découvert ce lien.

Pour découvrir des exemples originaux de partage de veille, vous pouvez (re)lire mon billet&nbsp;: [Quatre partages de veille&nbsp;: des liens et des ressources distribués](/2015/09/22/veille-et-partage/). Voici d'autres initiatives personnelles :

- Jeremy Keith : [https://adactio.com/links/](https://adactio.com/links/) ;
- Claire : [https://clairezed.github.io/marges/](https://clairezed.github.io/marges/) ;
-  iGor milhit : [https://id-libre.org/shaarli/](https://id-libre.org/shaarli/).


### 4. Bonus&nbsp;: diffuser

![Illustration représentant la diffusion façon POSSE, depuis une page web vers Twitter, une newsletter et vers une lettre imprimée](/images/2016-un-lien-par-jour-05.png)

Publier est l'acte de rendre public, diffuser est l'étape suivante qui permet de faire connaître une publication. Dans mon cas la première diffusion est celle via [mon flux RSS/Atom](/atom.xml), mais on peut imaginer beaucoup d'autres moyens&nbsp;: les réseaux sociaux, une newsletter, des mails directs, une impression papier (pourquoi pas), etc.

J'effectue une veille avant tout pour moi-même, le fait de la publier m'oblige à une certaine discipline. Je suis mon premier lecteur et cela me suffit. Mais si ma veille peut être utile à d'autres, alors tant mieux. Limiter ma veille à un lien par jour est aussi un moyen de réduire le bruit et la surcharge informationnelle, et publier et diffuser cette veille sur mon propre site web vise à sortir des silos, façon POSSE&nbsp;: [Publish (on your) Own Site, Syndicate Elsewhere](https://indieweb.org/POSSE).


## Quels outils&nbsp;?
Je souhaite dépendre le moins possible de services extérieurs, voici mes outils pour effectuer une bonne veille&nbsp;:

- [un navigateur](https://www.mozilla.org/fr/firefox/)&nbsp;;
- [un outil de synchronisation des navigateurs](https://www.mozilla.org/fr/firefox/sync/) -- entre le laptop et le smartphone&nbsp;;
- [un lecteur de flux RSS/Atom](http://www.lzone.de/liferea/)&nbsp;;
- [un éditeur de texte](https://www.sublimetext.com/)&nbsp;;
- [un générateur de site statique](http://jekyllrb.com/).

Il y a beaucoup d'outils, des lecteurs de flux RSS aux gestionnaires de marques pages, mais aucun n'est parfait, la veille est une activité essentiellement manuelle, malgré ce que pourraient laisser penser certains services en ligne.


## Et vous, comment veillez-vous&nbsp;?
Quelles sont vos pratiques de veille&nbsp;? Quelles méthodes et outils utilisez-vous&nbsp;? [#nowwwel](http://www.hteumeuleu.fr/en-decembre-ecrivez-partagez-hashtag-nowwwel/) est l'occasion de partager vos recettes et vos pratiques, ["plus on est de fous, plus on lit"](https://twitter.com/HTeuMeuLeu/status/806576666499809280)&nbsp;!