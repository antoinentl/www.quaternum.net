---
layout: post
title: "Quatre singularités du livre numérique"
date: "2016-05-19T22:00:00"
comments: true
published: true
description: "Voici un focus sur certaines spécificités du livre numérique, à prendre en compte lors des phases de conception d'un projet de publication numérique. Cet article se veut un rappel des contraintes ou opportunités qui sont liées aux standards du format du livre numérique et aux dispositifs de lecture : des évidences qui méritent d'être rappelées."
categories: 
- carnet
---
À la suite d'une session d'une journée autour du livre numérique avec des étudiants de l'[École supérieure d'art et design de Saint-Étienne](http://esadse.fr/), voici un focus sur certaines spécificités du livre numérique, à prendre en compte lors des phases de conception d'un projet de publication numérique. Cet article se veut un rappel des contraintes ou opportunités qui sont liées aux standards du format du livre numérique et aux dispositifs de lecture&nbsp;: des évidences qui méritent d'être rappelées.

<!-- more -->

## Back to the future
Avant d'aborder ces quatres singularités -- qui se retrouvent dans d'autres technologies, le web en premier ordre --, voici quelques bases concernant le format du livre numérique, l'EPUB&nbsp;:

- l'EPUB est un format ouvert et standardisé, il est proposé et maintenu par l'<abbr title="International Digital Publishing Forum">IDPF</abbr>&nbsp;;
- l'EPUB partage beaucoup de points communs avec le web, et notamment l'utilisation d'HTML 5 et de CSS 3 pour l'EPUB 3.1&nbsp;;
- le format EPUB se veut un format qui peut permettre la mise en accessibilité de ses contenus, tout comme le web&nbsp;;
- la convergence entre l'EPUB et le web [est lancée](https://medium.com/@naypinya/books-in-a-browser-375df76207ce#.ku3j5tklg).


## 1. Distinguer structure et mise en forme
Cela peut paraître évident mais il est déterminant de rappeler que le design d'un livre numérique ne peut être envisagé qu'en distinguant&nbsp;:

- la structure -- sémantique -- des contenus, il s'agit par exemple&nbsp;:
  + des niveaux de titres&nbsp;;
  + des blocs de citation&nbsp;;
  + des mises en valeur de portions de textes&nbsp;: *strong* ou emphase&nbsp;;
  + des listes ordonnées ou non ordonnées&nbsp;;
  + etc.&nbsp;;
- la mise en forme -- esthétique --, et donc les valeurs attribuées aux éléments structurés, il s'agit de la feuille de style. Quelques exemples&nbsp;:
  + les listes ont une marge plus importante que le corps de texte&nbsp;;
  + les titres de niveau 1 sont en capitales&nbsp;;
  + les citations sont en italiques&nbsp;;
  + etc.

Distinguer la structure et la mise en forme permet de développer un livre numérique sans arriver à des aberrations verbeuses -- comme des dizaines de `span` et de `div` qui se baladent partout --, et de permettre une lecture accessible de ces contenus -- lisibles par des synthèses vocales par exemple, [j'y reviendrai](#laccessibilit).


## 2. Des contenus liquides
C'est une notion finalement assez récente pour les *webdesigners*, puisque le *responsive web design* est un concept qui a été massivement utilisé à partir de 2009-2010. Depuis 2007 et la version 2.0 de l'EPUB, le livre numérique est liquide&nbsp;: les contenus -- texte, images -- s'affichent et s'organisent entre eux en fonction de la largeur et de la hauteur de l'écran. Un même texte pourra donc apparaître avec 20, 60 ou 150 signes sur une même ligne.

Par ailleurs, et j'y reviendrai plus loin, l'utilisateur a plus ou moins de libertés selon les dispositifs de lecture qu'il utilise&nbsp;: choix de la typographie, taille des caractères, marges, interligne, etc.

Lors de la conception d'un livre numérique, il faut donc accepter que la mise en forme ne pourra être totalement maîtrisée, puisque tout dépendra&nbsp;:

- du dispositif de lecture utilisé -- le matériel&nbsp;;
- du système d'exploitation du dispositif&nbsp;;
- de l'application de lecture&nbsp;;
- des choix de l'utilisateur.

Contrairement au design web désormais grandement dompté -- en comparaison du début des années 2000, et parfois en dépit des questions de standards, de performance ou d'accessibilité --, le design de livre numérique repose plus sur une intention que sur une maîtrise absolue.


## 3. Le non-choix typographique
Même s'il est possible d'*embarquer* des polices typographiques -- en utilisant [@font-face](https://en.wikipedia.org/wiki/Web_typography#History) -- le choix des caractères typographiques dépend de plusieurs éléments&nbsp;:

- le système d'exploitation du dispositif&nbsp;;
- l'application de lecture&nbsp;;
- le choix de l'application&nbsp;;
- les choix de l'utilisateur.

L'application de lecture qui interprète et donne le rendu de l'EPUB ne laisse pas forcément le choix au lecteur&nbsp;: soit d'afficher le livre numérique comme son créateur l'a conçu, soit de laisser au lecteur des réglages plus ou moins avancés.

Une grande partie des choix typographiques du designer peuvent parfois disparaître&nbsp;: la police indiquée ne sera pas celle retenue par l'application, les choix micro-typographiques ne seront pas respectés, etc. On peut donc parler de non-typographie.

>Avec le texte au format numérique, à la mise en page liquide, chacun peut changer les paramètres élémentaires. Il faudrait être typographe soi-même pour lire son livre savamment et ne pas jeter plus de mille ans de connaissance de mise en forme du texte. Pour un professeur, la perspective d’un retour à l’école de tous ces lecteurs en quête de connaissances typographiques est assez joyeuse !  
SP Millot (Les polices ont du caractère, un article de Corinne Renou-Nativel sur La-Croix.com)


## 4. L'accessibilité
Le format EPUB porte la même ambition que le web&nbsp;: permettre un accès et une lecture par toutes et tous, quelque soit notre degré d'empêchement. Et nous avons tous un certain degré d'empêchement face à une interface (par exemple il m'est très difficile de lire un texte qui n'est pas structuré).

>The power of the Web is in its universality. Access by everyone regardless of disability is an essential aspect.  
[Tim Berners-Lee](https://www.w3.org/standards/webdesign/accessibility)

Distinguer la structure et la mise en forme permet plusieurs usages&nbsp;:

- ne pas prendre en compte la feuille de style intégrée dans le livre numérique, pour des usages spécifiques&nbsp;: affichage en grands caractères par défaut, chargement d'une police pour dysléxiques, etc.&nbsp;;
- lecture du contenu par une synthèse vocale, qui ne restranscrira que la sémantique -- la structure -- du document, et non sa mise en forme.

Vous souhaitez comprendre de quoi je parle ? Rendez-vous sur un site web de votre choix, et désactivez la feuille de style. Surprise !

Le format *accessible* pour les publications numériques a longtemps été le format [DAISY](https://en.wikipedia.org/wiki/DAISY_Digital_Talking_Book), mais l'objectif est que le format EPUB 3.1 le remplace à terme.


## Bonus
Le support de mon intervention lors de cette session à l'[École supérieure d'art et design de Saint-Étienne](http://esadse.fr/) est disponible en ligne&nbsp;:

<a href="http://presentations.quaternum.net/le-livre-numerique-theorie-enjeux-decouverte/" class="link-button">Découvrir le support de présentation</a>

Quelques points qui pourraient être abordées plus longuement&nbsp;:

- la performance du livre numérique&nbsp;: poids, dépendances, rapidité d'affichage, etc.&nbsp;;
- les métadonnées&nbsp;: quelles bonnes pratiques pour une *réelle* utilisation des métadonnées intégrées dans un fichier EPUB ?
- l'évolution du format&nbsp;: quel avenir pour le format EPUB ?
- EPUB + web&nbsp;: quelles perspectives dans la convergence -- future -- de ces deux formats ?
- EPUB et dette technique&nbsp;: question déjà abordée dans [ce billet de Lekti](http://www.lekti-ecriture.com/bloc-notes/index.php/post/2016/La-production-de-livres-num%C3%A9riques-est-elle-homog%C3%A8ne).

Merci à [Jiminy](http://jiminy.chapalpanoz.com/) pour ses relectures attentives.