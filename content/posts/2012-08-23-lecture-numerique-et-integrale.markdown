---
comments: true
date: 2012-08-23
layout: post
slug: lecture-numerique-et-integrale
title: Lecture numérique et intégrale
categories:
- carnet
---
>Cette approche, qualifiée désormais d'"intégrale" par Gerstner, était ainsi résumée : "Dans la typographie intégrale, la relation entre la langue et l'écriture se construit à travers une nouvelle unité, une nouvelle globalité. Conceptions typographiques et textuelles ne sont plus vraiment deux processus de travail qui se succèdent sur deux plans différents, mais se mêlent l'une à l'autre dans un rapport de réciprocité."  
[Robin Kinross, La typographie moderne, éditions B42, page 149](http://www.editions-b42.com/books/La-Typographie-moderne/)

Il y a presque un mois Publie.net a édité une version numérique de [Zonzon Pépette, fille de Londres, d'André Baillon](http://www.publie.net/fr/ebook/9782814596511/zonzon-pepette-fille-de-londres "Lien vers publie.net"), un classique. La Dame au Chapal s'est chargée de la création graphique, et s'en explique dans deux billets très complets, [ici](http://ladameauchapal.com/2012/06/06/zonzon-pepette-andre-baillon/ "Lien vers le blog de La Dame au Chapal") et [là](http://ladameauchapal.com/2012/07/27/zonzon-pepette-2/ "Lien vers le blog de La Dame au Chapal"). C'est tout ce qui fait l'intérêt de la réédition de ce texte du domaine public - d'ailleurs les éditions Cent pages ont aussi proposé [une version papier de ce texte](http://atheles.org/centpages/horscollection/zonzonpepettefilledelondres/index.html) - la possibilité de lire un fichier numérique qui a fait l'objet d'un vrai travail graphique&nbsp;: illustrations de couverture et des chapitres, choix des typographies, agencement du texte... Chose encore trop rare pour les livres - numériques - à majorité textuelle et qui reproduisent trop souvent une vision du livre page à page, transposition du livre papier, sans conception ou création.

Mais au moment d'ouvrir le livre préalablement chargé dans la Bookeen Cybook Odyssey, frisson d'angoisse mêlé de déception : la mise en page n'apparaissait pas. La police par défaut - relativement froide et impersonnelle sur l'Odyssey - avait remplacé la Bell. De même, disparition des superbes lettrines en Lemondrop. Et enfin apparition d'espaces entre les paragraphes, cassant le rythme effréné de ce texte.
Après avoir ajouter les typographies Bell - pour le texte - et Lemondrop - pour les lettrines - directement dans le dossier "fonts" de l'Odyssey, en les ayant préalablement extraites de l'ePub, j'ai tenté de modifier la police par défaut, échec (mais espoir naïf de ma part, surtout pour les lettrines). J'ai ensuite trouvé la fonction "Styles de l'éditeur" dans les paramètres d'affichage - ce que j'aurais dû faire en premier -, le travail graphique de la Dame au Chapal est alors apparu, oh joie !
Ce qui étonnant c'est le fait que l'Odyssey m'ait joué ce tour, sans que je n'arrive ni à le reproduire ni à l'expliquer. J'ai quelques craintes sur le fait que certaines liseuses ou interfaces imposent leur mise en page au détriment d'un vrai travail de création. Le fait de pouvoir extraire - relativement facilement - les polices utilisées pose aussi un certain nombre de questions, même si certaines réponses résident dans les licences d'utilisation des fonts plus que dans leur accès.

Petite expérience qui me permet de saluer le beau travail graphique de [la Dame au Chapal](http://ladameauchapal.com/ "Lien vers le blog de La Dame au Chapal"), permit par [Publie.net](http://www.publie.net "Lien vers publie.net").