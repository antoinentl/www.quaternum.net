---
layout: post
title: "Hébergement, service, choix"
date: "2014-03-01T14:57:00"
comments: true
categories: 
- carnet
slug: hebergement
---
>La beauté de l'Internet c'est que tous le monde a la possibilité d'essayer et de faire différentes choses.  
[Fadi Chehadé, président de l'ICANN sur Place de la toile](http://www.franceculture.fr/emission-place-de-la-toile-rencontre-avec-le-chef-d-internet-2014-02-22)

Loin de moi l'idée de faire un billet d'humeur, mais je tente de rebondir sur un fait de cette semaine&nbsp;: OVH rencontre [un problème](http://travaux.ovh.net/?do=details&id=10325 "Lien vers OVH Travaux") avec un "filer" depuis la semaine dernière, ce qui impacte de nombreux sites hébergés sur des serveurs mutualisés — y compris des sites que j'administre. Rien de bien dramatique ou surprenant, des problèmes techniques de ce type peuvent advenir chez de nombreux hébergeurs. Je me questionne toutefois sur la rapidité de résolution du problème, mais n'ayant pas les connaissances techniques suffisantes je ne peux pas vraiment évaluer cela. Quoi qu'il en soit, le problème s'éternisant, je bascule certains sites et services sur mon Raspberry Pi pour continuer à travailler/écrire/lire.  

Je me pose les questions suivantes&nbsp;:

+ suis-je en mesure de me plaindre&nbsp;? J'ai souscris à un service qui ne me garantit pas vraiment une continuité de service maximale — la notion de ["Haute disponibilité" utilisée par OVH](http://www.ovh.com/fr/hebergement-web/hebergement-pro.xml "Lien vers OVH") étant assez vague&nbsp;;
+ envisager de me diriger vers [un autre hébergeur](https://www.gandi.net/hebergement/ "Lien vers Gandi") m'assurera-t-il un meilleur service, et notamment une meilleure communication en cas de problème&nbsp;? Si c'est le cas alors le coût plus important se justifie tout à fait&nbsp;;
+ rejoindre [des associations qualifiées et ouvertes](http://www.gresille.org/ "Lien vers Grésille") qui permettent aussi d'héberger, et à moindre coût&nbsp;;
+ n'ai-je pas aussi intérêt à [auto-héberger](/2014/01/05/un-serveur-a-la-maison/ "Lien vers quaternum.net") certains sites et services&nbsp;? Cela m'obligerait à gérer mes sauvegardes, prendre en compte le débit limité, régler certaines questions de sécurité, accepter les pannes. Je ne pourrais m'en vouloir qu'à moi-même en cas de problème, ce qui est déjà beaucoup.

L'hébergement de sites et services web est une question de choix&nbsp;: choisir ses contraintes sans oublier que [nous ne sommes pas tous en mesure de mettre en place certaines solutions](http://esquisses.clochix.net/2013/12/15/gloubiboulga/ "Lien vers Esquisses").