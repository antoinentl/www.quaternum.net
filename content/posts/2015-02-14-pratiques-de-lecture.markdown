---
layout: post
title: "Pratiques de lecture (questions et réponses)"
date: 2015-02-14
comments: true
categories: 
- carnet
slug: pratiques-de-lecture
---
Sylvie, une étudiant en Master Design graphique, m'a posé quelques questions sur mes pratiques de partages de lecture, l'occasion de faire un point sur mes usages. Je reprends ici ses questions et mes réponses — parfois rallongées pour plus de compréhension.

<!-- more -->

## Quels sont les supports de lecture que vous utilisez&nbsp;? 
Livres papier, smartphone, tablette, écrans d’ordinateur (le plus souvent un ordinateur portable), liseuse, dans le désordre.

## Quels sont vos différents types de lectures&nbsp;?
Billets de carnets et sites web, essais, bandes dessinées, revues, peu de romans.

## Est-ce que ces supports varient selon les types de lectures que vous effectuez&nbsp;?
Oui :

- les carnets web sont en ligne&nbsp;;
- les essais sont autant papier que numériques&nbsp;;
- les bandes dessinées en papier pour les livres, en ligne pour les histoires courtes (principalement sur [GRANDPAPIER](http://grandpapier.org)) ou les carnets de dessin&nbsp;;
- les revues surtout en papier, parce que c'est encore physiquement que ça se diffuse le mieux&nbsp;;
- les romans autant en papier que sur liseuse.

## Dans quelles conditions lisez-vous le plus souvent&nbsp;?
Deux types de réponses dans mon cas :

- lecture *égoïste* : lectures personnelles en lien avec mes intérêts du moment&nbsp;;
- lecture *de survie* : prendre une revue ou un livre papier pour m'éloigner de l'écran ou d'autres choses&nbsp;; découvrir un récit, une histoire, une écriture&nbsp;;
- lecture *professionnelle* : lectures pour le travail, mais il s'agit de lectures rapides ou survolées, presque toujours sur écran.

## Partagez-vous vos lectures, et si oui, comment&nbsp;?
Principalement en discutant, mais aussi :

- via [ce carnet en ligne](https://www.quaternum.net), mais moins que je ne le voudrais&nbsp;;
- dans des échanges, en direct et de façon orale&nbsp;;
- avec moi-même dans un carnet.

## Quelles manipulations effectuez-vous durant vos lectures&nbsp;?
Je recopie des passages comme des traces pour reconstruire les propos des auteurs. Je fais quelques schémas aussi. Parfois je *livetweet* un livre sur Twitter (mais j'avoue que cette pratique me fatigue désormais un peu). J'étais un grand fan de l'application [Readmill](/2013/10/14/readmill-et-medium-le-livre-et-le-web/), c'est un peu dans ce sens que l'on a imaginé l'[Annothèque](http://www.annotheque.fr) avec trois autres personnes.

## Quel serait le support de lecture de vos rêves&nbsp;?
J'ai longtemps pensé qu'une tablette 8 pouces rétro éclairée **et** à encre électronique serait le support idéal. Je comprends désormais qu'il n'y a pas *un* mais *des* supports de lecture. De la même façon il y a *des* supports d'inscription : livre papier, ordinateur portable, carnet papier, smartphone et tablette.