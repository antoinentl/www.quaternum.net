---
layout: post
title: "Zeitgeist : le fond, la forme, et le reste"
slug: zeitgeist
date: 2013-03-29
comments: true
categories:
- carnet
---
>Satisfaire les métamorphoses de la forme pour mieux les dépasser, rétablir une intimité plus précise avec notre fonds, mesurer plus finement encore la densité d’un texte, apprécier au plus juste la fécondité d’un corpus, qui ainsi pourra encore véritablement _peser_ au-dedans d’une interface, mettant en appétit le lecteur, sans pour autant épuiser, à la première consultation, sa curiosité naissante.  
[Frank Adebiaye, hinterland](http://www.forthcome.fr/hinterland/hinterland_editorial.html)

Frank Adebiaye [vient de présenter](http://fadebiaye.tumblr.com/post/46573481457/zeitgeist "Lien vers le tumblr de Frank Adebiaye") un projet fort intéressant. _Zeitgeist_ est "une entreprise littéraire", quelque chose se rapprochant du livre, un texte qui, dans ses formes et dans son cœur même, mêle et entrelace&nbsp;:

+ la lecture numérique&nbsp;: le fichier est structuré, avec une numérotation des paragraphes&nbsp;; le texte peut être lu de façon aléatoire et en ligne [ici](http://www.forthcome.fr/hoplites/zeitgeist/index.html "Lien vers Forthcome")&nbsp;; et l'arrivée prochaine d'une version EPUB est prévue - qui ne sera surement pas très différente du fichier HTML5&nbsp;;
+ le Web, parce que la base du code de ce livre est du HTML5 (ainsi qu'un peu de JavaScript), et que la première diffusion est celle de la version _lecture aléatoire_ en ligne, [ici](http://www.forthcome.fr/hoplites/zeitgeist/index.html "Lien vers Forthcome")&nbsp;;
+ la diffusion numérique, le fichier source à la base de toutes ces déclinaisons étant bientôt déposé sur Github - le site de partage de code permettant de gérer les versions des logiciels&nbsp;;
+ le papier&nbsp;: parce qu'il y a une version papier qui découle directement du fichier initial (HTML5), par le biais d'une impression directement dans le navigateur, chose possible notamment du fait de la structuration du texte.

Ce qui est fascinant dans ce projet c'est que la chaîne de publication est **la même** pour les différents supports - lecture aléatoire en ligne, version papier, fichier EPUB. Un seul fichier en HTML5, convenablement structuré, permet ensuite d'envisager différentes déclinaisons&nbsp;: une version en ligne qui propose une lecture numérique originale, un livre papier, un livre numérique, et un fichier HTML _versionné_ - le fichier _initial_ étant déposé sur Github.

Je n'aborde pas la partie "typographique" du projet, ce que l'on peut voir [ici](http://www.forthcome.fr/hoplites/zeitgeist/index.html "Lien vers la lecture aléatoire de Zeitgeist") est déjà beau pour les yeux et le cerveau.

La simplicité, la cohérence, la complémentarité fond → forme, le _workflow_, et l'originalité de ce projet me laisse béat d'admiration. Il y a finalement assez peu de projet _numérique_ excitant et novateur, [celui-ci](http://fadebiaye.tumblr.com/post/46573481457/zeitgeist "Lien vers le tumblr de Frank Adebiaye") en fait partie.