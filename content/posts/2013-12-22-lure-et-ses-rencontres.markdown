---
layout: post
title: "Lure et ses rencontres"
date: "2013-12-22T21:52:00"
comments: true
categories:
- carnet
---
>Il y aura bien un avant et un après #Lure2013. #CoupDeBlues  
[@AnthonyMasure](http://twitter.com/AnthonyMasure/statuses/373941930310107137)

[Les Rencontres de Lure](http://delure.org/ "Lien vers le site des Rencontres de Lure") se sont terminées depuis déjà presque quatre mois, l'occasion de poster ici le petit texte que j'ai proposé à la lettre des Rencontres, et qui a été publié en version papier aux côtés de ceux de Sandra Chamaret et d'Anthony Masure, double plaisir et double honneur. Difficile d'ajouter autre chose, si ce n'est d'inviter tous les curieux à participer à l'édition 2014. Seul moyen de comprendre de quoi il en retourne&nbsp;: vivre les choses et rencontrer les participants.

## Avant

Il y a plus d'un an que les Rencontres de Lure sont pour moi un doux rêve. Passer quatre ou cinq jours assis dans une salle à écouter des gens parler de typographie de 9h30 à 19h me semblait être une bonne façon de finir l'été. J'allais oublié deux paramètres&nbsp;:

+ 85 personnes par jour participent à ces rencontres — ce qui fait potentiellement beaucoup de rencontres et de discussions&nbsp;;
+ les Rencontres se passent à Lurs, superbe petit village perché dans les Alpes-de-Haute-Provence. Autant dire une sorte de paradis loin de toute agitation citadine et routinière, bientôt animée par des passionnés, aux côtés des touristes étonnés.

Je fus studieux jusque dans la préparation de mon sac&nbsp;:  

+ *Une théorie de l'écriture* de Gerrit Noordzij chez Ypsilon éditeur&nbsp;;
+ le fameux numéro 13 de la revue Livraison — *spécial typographie* — en version PDF&nbsp;;
+ des bouchons — pour le dortoir&nbsp;;
+ la fameux *Jeu de 9 familles typographiques* des éditions 205 — enfin une occasion de trouver des partenaires de jeu&nbsp;;
+ plusieurs paquets de biscuits&nbsp;;
+ un carnet papier.


## Pendant

Une fois arrivé à la gare et le moment de doute — façon "mais qu'est-ce que je fous là&nbsp;?" — écarté, les Rencontres de Lure peuvent se résumer très subjectivement à quelques faits aussi marquants que personnels&nbsp;:

+ appliquer le principe de la grille typographique pour installer une salle de réception, et rire beaucoup&nbsp;;
+ tenter de retrouver des bribes d'allemand, sans y parvenir&nbsp;;
+ regarder des graphistes dessiner des poneys colorés sur une nappe&nbsp;;
+ reprendre des notes sur du papier, et regarder comment font les autres&nbsp;;
+ discuter pendant presque cinq jours avec Eric Muller, de presque tout sauf de sa conférence — découverte le dernier jour et par ailleurs passionnante&nbsp;;
+ comprendre au bout d'une semaine que "graphiste", "typographe", "éditeur", "dessinateur", "designer"... ne veut pas dire grand chose à Lure, par contre "Luriens" c'est la classe.


## Après

Depuis quelques mois je tente d'expliquer à mon entourage ce que j'ai vécu, là-bas, pendant cinq jours. Mes amis comprennent bien que c'est une expérience plaisante — les photos de Lurs aident aussi. Je peux réussir à évoquer les nombreuses et passionnantes conférences, mais difficile de décrire l'ambiance et l'énergie, et je ne peux que les inviter à me rejoindre l'année prochaine, de la même façon que j'avais découvert les Rencontres de Lure.
