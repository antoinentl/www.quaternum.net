---
layout: post
title: "Construire du sens"
date: "2014-01-02T23:41:00"
comments: true
categories: 
- carnet
---
>Dans l'histoire de l'informatique, le matériel (Hardware) a permis l'éclosion de logiciels (Software) générateurs de données (Dataware) qui ont ensuite migré dans le nuage (Cloudware), et qui se retrouvent aujourd'hui possiblement remises à disposition des usagers. Sauf que nous avons de moins en moins la place de stocker localement ces données. Sauf que nous n'avons plus les outils pour "traiter" ces données. Pour leur donner du sens. Pour les interpréter.  
[Olivier Ertzscheid, Gimme my Data Back, Achille et la Data](http://affordance.typepad.com//mon_weblog/2013/12/gimme-my-data-back-achille-et-la-data.html)

## Produire du sens avec nos données

Nous nous retrouvons avec des quantités d'*objets numériques* qui nous échappent par leur quantité et leur décontextualisation, avec la fause conviction de documenter nos vies, au quotidien&nbsp;: dans l'intimité, dans les situations professionnelles, dans nos échanges avec les autres, etc.  
Recontextualiser ce que nous produisons — photos, messages, flux — devient nécessaire pour "leur donner du sens", dans des environnements maîtrisés, contrôlés, extérieurs aux silos centralisés.  

## Un journal

Produire du sens à partir de cette matière peut passer par exemple par un journal&nbsp;: faire de la contrainte de la régularité un moyen pour tisser du lien entre ces bribes *dématérialisées*, pour marquer des événements, ne pas perdre de mémoire certains *fragments* sans pour autant vouloir *tout* conserver, utiliser cette contrainte du temps pour faire une sorte de tri, et pour confronter. Lieu et temps de convergence.  
Il est désormais tellement simple de créer un site ou un blog, hébergé sur des plateformes ou choisir de le faire *soi-même* pour quelques euros par mois&nbsp;; ou encore envisager de construire un véritable carnet personnel, via [un Raspberry Pi hebergé chez soi ou chez des proches](/2014/01/05/un-serveur-a-la-maison/ "Lien vers quaternum.net"). Et pourquoi pas [en local](http://loic.mathaud.fr/notes/2013/web-local/ "Lien vers le blog de Loïc")&nbsp;?  

## Construire nos propres outils

Je suis conscient que seule une poignée de curieux auront les moyens et prendront le temps de penser et de construire ces outils, ces journaux web visibles ou invisibles.  

>Dire que pour échapper aux silos il "suffit" d’avoir son propre serveur, est aussi irréaliste que de conseiller de faire son potager pour échapper aux grandes chaines agro-alimentaires. Juste sur le papier, très difficile à grande échelle dans la réalité.  
[Clochix, Gloubiboulga et serveur](http://esquisses.clochix.net/2013/12/15/gloubiboulga/)

Mais faut-il attendre des entreprises qu'elles nous proposent des applications dédiées pour traiter nos données qu'elles détiennent&nbsp;? Surement pas. [Offrir des noms de domaine](http://nota-bene.org/Offrir-un-nom-de-domaine "Lien vers nota-bene.org"), louer des espaces sur des serveurs, proposer à des proches d'héberger certains services sur nos propres serveurs maison, utiliser des applications — simples — sur nos propres machines&nbsp;; voilà des pistes — alternatives — à explorer, pour nous permettre de redonner du sens et de raconter des histoires.