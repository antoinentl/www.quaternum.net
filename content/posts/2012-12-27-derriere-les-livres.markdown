---
layout: post
title: Derrière les livres
slug: derriere-les-livres
date: 2012-12-27
comments: true
categories:
- carnet
---
>Je ne mets plus les noms des alphabets à la fin des livres, car je me suis rendu compte que certains d'entre nous [designers graphiques] prennent des raccourcis avec l'expérience, et c'est jamais bon. [...] Qu'ils fassent pareil ne me dérange pas dans le fond, quoique, mais il faut qu'ils le trouvent. Il faut travailler un peu quand même. Parce que ce qui est là ce n'est pas parce qu'on en a l'idée cinq minutes avant ou dix minutes avant, mais parce que ça fait vingt ans qu'on travaille. Et ça ça n'est comme ça que parce que ça fait vingt ans que je travaille. Le livre je ne le faisais pas comme ça il y a quinze ans. [...] Je ne mets pas le nom, mais il y est l'alphabet. Tous le monde peut le regarder et se dire "ah c'est cet alphabet là, d'accord...".  
[Philippe Millot dans un entretien effectué par les étudiants de l'École européenne supérieure d'art de Bretagne, Rennes, avec Philippe Millot et Olivier Gadet](http://rouges-gorges-et-cosaques.net/)

Cette citation pose la question de la réutilisation et de la copie du travail des graphistes ou "designers graphiques"&nbsp;: mises en page, associations typographiques, choix de papiers...  
Philippe Millot est "dessinateur de livres", il a dessiné (entre autres) des livres aux éditions Cent Pages, ces objets sont des merveilles de création typographique. Difficile de lire d'autres livres après avoir découvert ceux de [Cent Pages](http://atheles.org/centpages "Site du diffuseur des éditions Cente Pages")&nbsp;; même chose pour les livres des [éditions B42](http://www.editions-b42.com/ "Site des éditions B42") par exemple.

Le livre numérique, au format ePub et sans DRM, est ouvert. On peut très facilement voir comment le livre est conçu en décompressant le fichier (après l'avoir renommé en .zip) et en observant les différents fichiers&nbsp;: XML, feuilles de style CSS, métadonnées, fontes. On découvre alors les choix typographiques&nbsp;: polices utilisées, marges, espacements, structuration du contenu (chapitres, pages, niveaux de titres...), couleurs des textes et des fonds... Ce qui pouvait être caché dans un livre papier se dévoile en quelques clics.

Le travail des graphistes est-il désacralisé, parce que visible&nbsp;? D'un côté il semble que ceux qui viennent du Web n'y voient aucun problème, habitués par cette ouverture. Les faiseurs de livres papier acceptent-ils ce postulat inscrit - pour le moment - dans les livres numériques&nbsp;?  
Est-ce que cela changera quelque chose dans le processus de création et de mise en page&nbsp;?  
Quand bien même cela est *potentiellement* visible, combien de lecteurs iront voir le "code source" d'un livre numérique&nbsp;?