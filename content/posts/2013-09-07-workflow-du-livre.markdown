---
layout: post
title: "Workflow du livre"
slug: workflow-du-livre
date: 2013-09-07
comments: true
categories:
- carnet
---
## Tentative de définition du "workflow" dans le cas de l'écrit et du livre

Difficile de trouver un terme court en français pour restituer le sens de "[workflow](http://fr.wikipedia.org/wiki/workflow)"&nbsp;:

+ flux de travail&nbsp;;
+ flux opérationnel&nbsp;;
+ chaîne de production&nbsp;;
+ chaîne de publication&nbsp;;
+ ...

Dans le cas du livre - et de l'édition ou publication - le workflow peut être défini comme étant toutes les étapes de la publication depuis la réception d'un texte - par un éditeur par exemple - à sa mise à disposition aux lecteurs.  

Le format EPUB n'est surement qu'une transition, une étape du passage du papier au "numérique" - ce terme ne voulant plus dire grand chose - ou à la dématérialisation. Le livre ou l'écrit se dirigeant inexorablement vers le web.  
À l'heure actuelle le livre numérique ne fait que reproduire le schéma - et le worflow - de l'écosystème du livre papier, au moins en grande partie. Diffusion et distribution comprises, le seul point positif et quelque peu innovant est la prise en compte de l'archivage par certains e-diffuseurs - mais pour combien de temps&nbsp;?  

## Esquisse

Les expérimentations des [Floss Manuals](http://fr.flossmanuals.net/) ou du [Monde diplomatique](http://www.monde-diplomatique.fr/2013/05/A/49059) semblent les plus intéressantes.  
Prenons le Monde diplomatique, chaque article est désormais simultanément disponible sous différentes formes&nbsp;:

+ page ou billet en ligne pour chaque article&nbsp;;
+ version papier du journal&nbsp;;
+ version EPUB du journal, qui est en fait un export du site depuis SPIP.

On pourrait imaginer quelque chose de similaire pour le livre&nbsp;:

+ textes - chapitres, morceaux de livre - mis en ligne comme c'est déjà le cas sur les blogs et sites d'auteurs. Les expérimentations de François Bon vont dans ce sens, que ce soit [tiers livre](http://www.tierslivre.net/) ou [nerval](http://nerval.fr/)&nbsp;;
+ livre numérique au format EPUB, et/ou sélection de textes initialement publiés en ligne&nbsp;;
+ POD ou impression à la demande directement issue du fichier EPUB.

Cela rejoint aussi la - très belle - expérimentation de Frank Adebiaye avec [Zeitgeist](/2013/03/29/zeitgeist/).  

## Enjeux et contraintes

Les deux points fondamentaux sont, à mon avis, la fluidité du "worklow", du flux, et la pérennité à la fois des versions web et des versions EPUB.  
Réflexion en cours.  