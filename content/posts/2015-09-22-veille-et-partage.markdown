---
layout: post
title: "Quatre partages de veille : des liens et des ressources distribués"
date: "2015-09-22T21:00:00"
comments: true
published: true
categories: 
- carnet
slug: veille-et-partage
---
Voici une analyse de quelques formes de partage de veille, à travers des exemples majoritairement décentralisés. Un rapide panorama de pratiques, bien souvent personnelles, qui consistent à noter, enregistrer, recontextualiser et partager des liens, des ressources et des idées.  
(Le titre est emprunté à une citation de David Larlet.)

<!-- more -->

## David Larlet&nbsp;: veille et réflexion
[David Larlet](https://larlet.fr/david/) a déplacé le partage de sa veille depuis Twitter vers son site, depuis début 2015&nbsp;:

>C’est un acte militant car je souhaite encourager les personnes comme Richard à réacquérir des outils décentralisés. Le confort et la popularité d’outils centralisés à la Twitter et consorts tuent le Web à petit feu. Je ne souhaite plus être un contributeur actif de sa destruction. Ce site est à l’image du Web que je veux voir grandir&nbsp;: des liens et des ressources distribuées. Des expériences et de l’acquisition de savoirs.  
[L’oiseau bleu](https://larlet.fr/david/blog/2015/oiseau-bleu/)

Le principe de [son partage de veille](https://larlet.fr/david/stream/) est le suivant&nbsp;:

- un article par jour -- et David tient cette contrainte depuis plus de huit mois&nbsp;;
- pour chaque article&nbsp;:
  + une citation la plupart du temps, issue d'un article sur le Web. Parfois une phrase sur son travail ou son rythme de vie&nbsp;;
  + la source de la citation&nbsp;;
  + le *cache* de la source de la citation&nbsp;: David effectue son propre archivage du Web, afin de conserver les contenus qu'il a découverts et lus&nbsp;;
  + une réaction qui peut être plus ou moins courte, et enrichie par ses réflexions, ou parfois remise en contexte avec d'autres items de veille ou ses propres articles&nbsp;;
- techniquement il s'agit d'un CMS maison, conçu avec du Python, et probablement Django, un peu sur le même fonctionnement que des *noDB CMS* comme Jekyll.

Le fonctionnement opté par David consiste donc à noter, enregistrer et partager dans la régularité et la durée. Après plus de huit mois cela semble très bien fonctionner, c'est en tout cas l'une des veilles que je lis régulièrement, et ce format me convient très bien. (Je me suis par ailleurs beaucoup inspiré de ce système pour mettre en place [ma propre veille personnelle](/flux/).)

## jocundity&nbsp;: "bulletin d’idées motrices"
[Frank Adebiaye](http://www.fadebiaye.com/) a mis en place un "bulletin d’idées motrices", [jocundity](http://jocundity.fr/), depuis mars 2015&nbsp;:

>jocundity est une initiative de Forthcome et de Frank Adebiaye visant à mettre sur pied un bulletin de veille sélectif s’appuyant sur la notion d'idée motrice au sens de Maurice Ponthière et de Delphine Gardey, c’est-à-dire, ainsi que nous l’interprétons ici, d’un concept, d’un principe, d’une intuition pouvant donner lieu à des appropriations et des incarnations positives et constructives.  
[À propos, jocundity](http://www.jocundity.fr/about.html)

Irrégulier, ce bulletin daté rassemble à chaque publication quelques items qui comprennent chacun quatre informations, structurées de la façon suivante&nbsp;:

- date&nbsp;: il s'agit de celle de la découverte de l'article ou de l'information&nbsp;;
- un commentaire, souvent d'une seule ligne&nbsp;;
- le titre de la source originale&nbsp;;
- le lien vers la source&nbsp;;
- un ou plusieurs mots-clés correspondant au contenu des ressources partagées et aux intérêts de Frank&nbsp;: littérature, typographie, musique, édition, gastronomie, science, document, enseignement, technologie, et bien d'autres.

Le fonctionnement de [jocundity](http://www.jocundity.fr/) est très intéressant, puisque le back-office repose sur JSON, mais peut-être que ce point sera l'occasion d'un plus long billet.  

La démarche de Frank est généreuse, elle se veut un déclencheur pour celui qui lit, au croisement de la typographie, de la littérature, de l'économie, de la création, du design et du document.

## Karl Dubost&nbsp;: "Vide Grenier"
[Karl Dubost](http://www.la-grange.net/karl) tient un carnet quotidien depuis 2000, sur le modèle suivant&nbsp;: image (personnelle) + citation (d'une lecture en cours) + pensée/réflexion. Véritable journal d'une vie qu'on ne peut que deviner, les *Carnets de La Grange* sont passionnés et passionnants.  

Depuis quelques mois Karl propose une partie "Vide Grenier" à chaque fin de billet, regroupant ainsi ses trouvailles du jour, sous la forme d'une citation, d'une phrase ou d'un lien.  

Le croisement des écrits personnels et de cette *veille* est étonnant, les deux n'ayant parfois rien à voir. Et pourtant, de la même façon que les textes de Karl croisent ses photos et les citations de ses lectures en cours, les quelques liens donnent une nouvelle dimension à ce journal -- qui n'en est pas un -- et d'autant plus depuis que la publication des billets se fait avec environ un mois de décalage. Quelle valeur alors pour cette veille parfois technologique&nbsp;? (Réponse&nbsp;: peut-être encore plus de pertinence avec le recul.)  

Il faut préciser que le site de Karl ne dispose pas de gestion des commentaires&nbsp;: c'est volontaire, l'objectif est d'inciter ses lecteurs à publier sur leurs propres sites pour réagir.  

Comme David, Karl utilise un générateur de site statique, fait maison, [en Python](https://github.com/karlcow/ymir).


## À lire ailleurs&nbsp;: analyse et partage
>InternetActu.net et la Fing veillent pour vous  
[alireailleurs.tumblr.com](http://alireailleurs.tumblr.com/)

À lire ailleurs est la veille conjointe d'[InternetActu.net](www.internetactu.net) et de la [Fing](www.fing.org), réalisée par [Hubert Guillaud](http://www.internetactu.net/author/hubert/). Cette veille était sur Diigo il y a encore un an et demi, elle prend désormais la forme d'un blog, hébergé sur Tumblr.

>“A lire ailleurs” est le nom de la revue de web d’InternetActu.net, le média de la Fing. C'est une revue de veille synthétisant des articles et des informations dans le champ des technologies et de leurs impacts, disponible dans de multiples formats (sur Diigo, sur Seenthis, sur le bouillon des bibliobsédés… sous forme de fil RSS, de fil Twitter) publiée sous licence Creative Commons Attribution 4.0, stipulant que vous reprendre ce travail, le republier même pour une utilisation commerciale.  
L'objectif de cette revue de web est de donner à lire des synthèses d'articles dans le champ des transformations technologiques, politiques, économiques et sociétales.  
[À lire ailleurs, la revue de web par InternetActu.net,](http://alireailleurs.tumblr.com/post/81384827645/a-lire-ailleurs-la-revue-de-web-par)

Le point de départ de chaque *post* est un article, synthétisé, contextualisé, analysé et commenté. Un travail de fond, un travail de journaliste.  
Ici l'utilisation de Tumblr est presque un détail, le choix s'est surement fait pour la simplicité d'utilisation en *back*, et pour la lisibilité en *front* -- le thème est minimaliste, juste ce qu'il faut.  

[À lire ailleurs](http://alireailleurs.tumblr.com/) est une source incontournable pour qui s'intéresse au monde dans lequel on vit.

## Vers une décentralisation et une recontextualisation ?
Ceci n'a rien d'une analyse scientifique, mais je remarque depuis deux ans un retour à des démarches de veille profonde, qui s'éloignent de l'usage des réseaux sociaux -- Twitter, Facebook, mais aussi Delicious, Diigo ou d'autres outils similaires. Le partage d'un simple lien à peine accompagné d'un titre est remplacé par des dispositifs décentralisés, souvent en parallèle de carnets en ligne. Des partages augmentés d'une recontextualisation, d'un commentaire et de rebonds. Des veilles distribuées.  

Au-delà de ces démarches *distribuées*, on peut apprécier les formes diverses&nbsp;: des interfaces de lecture souvent minimalistes mais personnelles. S'éloigner des silos, également formels, devient d'autant plus captivant.