---
layout: post
title: "La machine"
date: "2013-11-11T22:07:00"
comments: true
categories: 
- carnet
---
Depuis quelques mois ma dépendance à mon ordinateur portable me pose problème, j'y centralise beaucoup d'usages — plus que de données que je répartie un peu partout avec un souci d'interopérabilité. Cette petite machine, plus petite qu'un format A4 et plus épaisse qu'un livre de poche, m'accompagne partout&nbsp;: presque quotidiennement au travail, pour tous mes déplacements professionnels comme personnels, dans un café ou chez des amis, sur mon bureau ou sur la table de la cuisine, dans mon lit... Et pour tant d'usages&nbsp;: messageries personnelle et professionnelle, lecture et écriture, web, pour regarder des images et des films, écouter de la musique, bidouiller... Je tente d'entrevoir les dispositifs alternatifs pour remplacer cette machine — *la machine* — et certains ne sont pas forcement numériques. Il me semble que cette démarche est tout autant une question de bon sens que la prérogative à une survie en milieu numérique. Et pourtant j'espère pouvoir faire vivre cet ordinateur portable longtemps. Pendant ce temps-là j'apprécie cette *centralisation* des usages, cet environnement confortable, ce lieu de lecture, d'écriture et d'échanges parmi tant d'autres finalement.  
Ne pas oublier que nos usages ne sont jamais définitifs, et donc pas plus les dispositifs qui les entourent.