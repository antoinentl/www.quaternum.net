---
layout: post
title: "Pourquoi gérer du contenu avec Git ?"
date: "2018-06-11T10:00:00"
comments: true
published: true
description: "Pourquoi utiliser Git pour administrer du contenu, plutôt qu'un CMS ?
Voilà, en quelques points, une esquisse de réponse à la question posée par Nicolas.
Il est question de versionnement, de déploiement, de fichiers texte et de hub.
Ce billet, très imparfait, ne mérite que d'être précisé et complété !"
categories:
- carnet
---
Pourquoi utiliser Git pour administrer du contenu, plutôt qu'un CMS&nbsp;?
Voilà, en quelques points, une esquisse de réponse à la question posée par Nicolas.
Il est question de versionnement, de déploiement, de fichiers texte et de hub.
Ce billet, très imparfait, ne mérite que d'être précisé et complété&nbsp;!

<!-- more -->

À l'occasion de [l'annonce](/2018/06/04/un-memoire-en-depot/) de l'ouverture du dépôt Git de mon mémoire en cours d'écriture, [Nicolas s'interroge](https://twitter.com/nicolastilly/status/1005429681305346048) sur l'usage de Git pour du texte.
Qu'est-ce que Git peut apporter de plus par rapport à un système de gestion de contenu (CMS) web classique&nbsp;?
Réponse en quelques points&nbsp;:

1. Plutôt qu'opposer CMS et Git, il faut plutôt comparer la complexité de certains CMS qui utilisent des bases de données et la simplicité de la gestion de fichiers texte – par exemple au format Markdown – par le biais de Git, pour reprendre les termes de [Frank](https://frank.taillandier.me/).

2. Un système de gestion de contenu – ou CMS – classique basé sur des bases de données comme WordPress oblige à utiliser une interface web, ce qui signifie qu'un outil est imposé.
Rares sont les CMS dits _dynamiques_ à proposer des APIs permettant de connecter d'autres interfaces ou applications – ou alors de façon unilatérale.
Personnellement j'aime beaucoup écrire dans un éditeur de texte&nbsp;: je n'ai pas besoin de me soucier de la connexion internet, je peux changer d'éditeur et modifier son aspect sans que cela n'ait d'incidence sur le processus.
C'est ce que j'avais tenter d'expliquer [ici](/2012/12/23/pourquoi-quitter-wordpress/).

3. Premier bonus&nbsp;: écrire avec un langage de balisage léger comme Markdown est alors la meilleure approche, la structure du document n'est pas dépendante d'un éditeur en mode WYSIWYG (What You See Is What You Get).

4. Deuxième bonus&nbsp;: Git vous permet de conserver une version locale des fichiers, c'est le cas également pour les éventuels autres intervenants du projet.
Il n'y a pas de dépendance à une plate-forme – quand bien même une instance de WordPress serait par exemple installée sur un serveur personnel –, et les fichiers source ne sont ni dans des formats incompréhensibles par nous les humains, ni dans des bases de données.

5. Git est conçu pour versionner des fichiers&nbsp;: les évolutions d'un projet peuvent être suivies en sachant qui (personne/rôle) à fait quoi (fichiers modifiés) et où (version/branche du projet) et à quel moment.
À cela on peut opposer le fait que beaucoup de gestionnaire de contenus ont adopté ce fonctionnement – comme [Google Docs](https://github.com/google/diff-match-patch) par exemple –, mais c'est un procédé très limité et enfermé dans l'application.

6. Git est une machine à voyager dans le temps&nbsp;: on peut revenir en arrière sans tout casser.
Certes ce n'est pas forcément toujours simple – surtout en ligne de commande – mais c'est _possible_&nbsp;!

7. Un dépôt Git peut être hébergé par des plate-formes, l'ensemble des fichiers et des interactions qui le composent peuvent être visualisés avec des interfaces web.
GitLab, GitHub ou Gitbucket sont les plus populaires, elles permettent de manier des projets Git, et apportent la possibilité de faire toutes les interventions habituelles via une interface web – donc en se passant d'un terminal par exemple –, plus _d'autres choses_.

8. Ce "d'autres choses" est très intéressant&nbsp;: par exemple la gestion d'issues (tickets pour signaler des bugs ou des erreurs) en lien avec Git (issue liée à une branche), ou des interfaces qui rendent certaines actions complexes plus compréhensibles.

9. Toujours à partir de Git et d'une plate-forme en ligne, il est possible d'automatiser des actions.
Exemple&nbsp;: à chaque fois que je fais un commit (un enregistrement) dans la branche master (la version principale du projet), des fichiers sont envoyés sur un serveur web.

10. En terme d'organisation cela veut dire que l'on peut tester des choses dans une branche parallèle, lorsque c'est validé on pousse ces modifications sur master et les fichiers seront automatiquement envoyés sur un serveur.
Pratique pour administrer et tester un site web 😉
C'est ce qu'on appelle le [déploiement continu](https://about.gitlab.com/2016/08/05/continuous-integration-delivery-and-deployment-with-gitlab/).

11. Ce principe peut aller plus loin&nbsp;: à chaque commit sur une branche les fichiers Markdown vont être transformés en fichiers HTML grâce à un générateur de site statique, les différents programmes nécessaires au générateur de site statique fonctionnant sur une machine virtuelle.

12. On peut imaginer beaucoup plus d'usages&nbsp;: par exemple avec [Thomas](https://oncletom.io) nous nous sommes amusés à générer un fichier .docx à partir d'une source au format AsciiDoc associée à une bibliographie en bibtex et une feuille de style Word 😎

13. Ces actions automatisées peuvent également être des tests, différents selon des branches définies (beta, preprod, etc.).
C'est ce qu'on appelle l'[intégration continue](https://rpadovani.com/introduction-gitlab-ci).

14. Dans le cas des générateurs de site statique, des services viennent se connecter à un dépôt Git pour faciliter la gestion des contenus directement en ligne, avec des interfaces dignes d'un CMS.
C'est le cas de [Forestry.io](https://forestry.io/)&nbsp;: "Your editing team won’t even realize they’re editing Markdown and committing to your repo."

15. Toujours dans le cas des générateurs de site statique, d'autres services peuvent se charger de déployer le site web, c'est le cas de [Netlify](https://www.netlify.com/).

16. Est-ce qu'il est possible d'associer Git et un CMS plus classique qu'un générateur de site statique&nbsp;?
Oui, si celui-ci fonctionne sans base de données.
Cela peut être le cas avec [Kirby](https://getkirby.com/) par exemple, Kirby est un _flat_ CMS&nbsp;: il fonctionne sans base de données mais avec un langage de programmation dynamique (PHP).

17. Il faut voir Git comme un _hub_ auquel viennent se connecter différents services.

Pour compléter ces points je vous conseille la vidéo de l'intervention de William Chia lors de [Git Merge](https://git-merge.com/) 2018, qui formule d'une autre façon ce que je viens d'expliquer ici – ou pourquoi Git est un puissant outil pour gérer des contenus&nbsp;: [Empowering non-developers to use Git](https://www.youtube.com/watch?v=pY5i0Io86UQ).

Les propositions de modification de cette liste sont bienvenues&nbsp;!
