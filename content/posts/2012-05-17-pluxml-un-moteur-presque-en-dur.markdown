---
comments: true
date: 2012-05-17
layout: post
slug: pluxml-un-moteur-presque-en-dur
title: PluXml, un moteur (presque) en dur
categories:
- carnet
---
Ce que j'avais préssenti se révèle donc vrai : le moteur de blog sans base de données [PluXml](http://www.pluxml.org/) crée des fichiers en dur pour chaque article/page.

Pourquoi cela présente-t-il un intérêt ? Contrairement à Wordpress qui "range" tous les contenus dans une base de données, ou à [Blogotext](http://lehollandaisvolant.net/blogotext/ "Lien vers Blogotext sur le site du hollandais volant") qui stocke les billets et pages dans des fichiers PHP, PluXml génère des fichiers XML pour chaque article et page. J'ai vraiment la sensation que PluXml se situe entre un CMS "classique" utilisant une base de données (Wordpress, Drupal...) et un moteur de pages statiques (type [Jekyll](http://phollow.fr/2012/05/de-wordpress-a-jekyll/ "Lien vers phollow.fr")). On peut donc préparer les articles hors ligne et les déposer ensuite simplement sur le serveur&nbsp;: le passage par l'interface d'administration n'est pas obligatoire. Un simple transfert par FTP suffit. On peut donc utiliser l'éditeur de texte de son choix pour écrire (certes "dans le code", mais il suffit de connaître quelques bases du HTML), aucune connexion n'est nécessaire pendant la rédaction, c'est tout de même plus simple en terme de gestion que les traditionnels copier/coller à partir de fichiers HTML, ou l'utilisation de clients dans le cas des protocoles XML-RPC.  

Pour que cela soit parfait il suffirait de créer un script capable de générer le fichier XML à partir d'un simple fichier HTML : le défaut que je rencontre actuellement étant que les fichiers à éditer sont du XML qui encapsulent du HTML, un éditeur de texte (Notepad, Geany, Sublime Text 2...) ne peut pas comprendre deux formats à la fois, il faut donc gérer le HTML un peu à l'aveugle...  

L'autre avantage non négligeable c'est le fait de conserver (très) facilement une archive des billets et pages créés, et de gérer cela tout aussi facilement (on pourrait même pousser le système vers une configuration synchronisée via ownCloud ou les classiques Dropbox...).

**Contruction des noms de fichiers**  
La structure du nommage ressemble à cela :  

000a.00b.00C.AAAAMMJJhhmm.titre-article.xml  

avec :

+   a : numéro ou ID de l'article, c'est de cette façon qu'est reconnu le fichier, il est donc très important de faire attention à cette valeur et de ne pas nommer deux articles avec le même ID ;
+   b : ID de la catégorie de l'article. S'il y a plusieurs catégories associées à l'article, alors les ID suivants viennent s'ajouter ainsi avec des virgules : 000a.001,002,003.00c...
+   c : ID de l'auteur de l'article ;
+   AAAAMMJJhhmm : date de publication ;
+   si l'article est en brouillon alors l'élément suivant sera ajouté : 000a.draft,00b.00c...
+   titre-article : titre de l'article qui apparaîtra dans l'URL, attention l'utilisation des majuscules dans cette partie rend la page invisible.


**Paramètre dans le fichier XML**  

Apparemment il n'y a qu'un seul paramètre qui ne soit pas dans le nom du fichier : l'autorisation des commentaires sur l'article. Il faut aller voir la balise "allow_com" et modifier le "1" par un "0" ou l'inverse.

Le changement de l'un de ces paramètres sera visible immédiatement en ligne : sur le front office partout sur le site (page d'accueil, archives, flux RSS...) mais aussi sur le back office, c'est là que l'on se rend compte que PluXml est rudement bien pensé.  

Lors de la création d'un article via l'interface d'administration de PluXml, le fichier .htaccess n'est pas modifié dans le dossier /data/articles/.

Je m'entraîne donc un peu [ici](http://beta.quaternum.net) avec ce moteur, le temps de résoudre [quelques problèmes](http://forum.pluxml.org/viewtopic.php?id=3364 "Lien vers le forum de PluXml") et d'adapter le template et la feuille de style (autant dire que c'est en chantier).


**Petite précision concernant Blogotext**  

Blogotext propose exactement le même type de fonctionnement, sauf qu'il s'agit de fichiers PHP comme expliqué au début de ce billet. La différence joue sur les paramètres et le nommage des fichiers. J'ai tout de même une préférence pour PluXml, que je tenterai d'expliquer plus tard.