---
comments: true
date: 2011-12-24
layout: post
slug: faire-venir-le-web
title: Faire venir le Web
categories:
- carnet
---
Lire le Web, l'information en général et les carnets en particulier. Pour trouver quelque chose, pour s'informer, pour suivre, par sérendipité. Par le biais d'abonnements mail, ou avec les flux RSS surtout, pour maîtriser, pour être discret (mais pas forcement anonyme). Donc avec un lecteur de flux RSS, Google Reader et autres - Netvibes tombe en désuétude et je cherche toujours un lecteur version client simple et efficace. Aussi par mail, depuis des flux RSS, via des services comme [Ifttt](http://ifttt.com/). Le mail reste la meilleure façon d'être alerté.

Propulser ailleurs par soi-même ou grâce aux autres, et pour soi mais aussi pour les autres. Twitter ou Identi.ca sont des très bons moyens de se tenir informer, de recueillir des informations et des liens, de rebondir, voir même de réagir, de commenter. Encore faut-il être capable de suivre des flux denses presque en permanence : par principe et en pratique pas de possibilités de regarder ce qui s'est passé il y a plus de 24h sur sa timeline.

Faire venir le Web, trouver les moyens adéquats.