---
layout: post
title: "Readmill et Medium, le livre et le web"
date: "2013-10-14T20:30:00"
comments: true
categories:
- carnet
---
>Bien que le livre numérique soit philosophiquement plus proche du Web (dont il emprunte les langages et technologies) que du livre, on ne cherche que très peu à remettre en cause la forme et les mécanismes connus des lecteurs : ils tournent les pages et écrivent dans les marges.  
[Jiminy Panoz, Design du livre numérique, 10 %](http://jiminy.chapalpanoz.com/editorial-design-hack-les-livres/)

Plutôt que de prolonger le débat sur la forme du livre numérique, je voudrais simplement analyser les points de rencontres de plus en plus forts entre le web — dynamique et parfois éphémère — et le livre numérique au format EPUB — clos et dans une certaine mesure, pérenne — à travers deux expériences de lecture.

## Readmill
[Readmill](https://readmill.com/ "Lien vers Readmill") est une application de lecture, pour smartphones et tablettes, sous iOS et Android. En plus d'une interface épurée qui évite tout [skeuomorphisme](https://fr.wikipedia.org/wiki/Skeuomorphisme "Lien vers Wikipédia") à la iBook — finie l'étagère en bois et les pages qui se tournent — Readmill propose plusieurs choses&nbsp;:

+ avant toute chose, pour utiliser Readmill il faut créer un compte&nbsp;;
+ chaque livre numérique — fichier EPUB ou PDF — Readmill propose de rendre la lecture privée ou publique. Dans le deuxième cas n'importe qui peut voir le niveau d'avancement dans le livre ainsi que les passages surlignés et les commentaires&nbsp;;  
+ surligner&nbsp;: si la lecture est en version publique alors les passages surlignés seront aussi publics&nbsp;;
+ commenter&nbsp;: à partir des passages surlignés. Une discussion peut se faire via plusieurs *comptes* Readmill, à condition que la lecture soit publique.

![Exemple de surlignagne sur Readmill](../../../../images/readmill-surlignage.jpg)

Je fais l'expérience de la lecture sur Readmill depuis plusieurs mois, et il y a quelques semaines j'ai commencé à surligner et à commenter [quelques livres](https://readmill.com/antoinentl "Lien vers mon profil Readmill"). J'ai ainsi pu échanger avec Jiminy Panoz sur [un de ses livres](https://readmill.com/books/design-du-livre-numerique "Lien vers Readmill"), je suis assez surpris des possibilités d'interaction&nbsp;: travailler sur un texte seul ou à plusieurs.

![Exemple d'annotation et de commentaires sur Readmill](../../../../images/readmill-annotations.jpg)

Il y a ici un lien direct entre le livre numérique — le fichier EPUB clos — et le web&nbsp;: visibilité de la lecture, partage de passages surlignés, annotations et discussions. Les possibilités d'interactions sont empruntées du web, que se passerait-il si les fichiers EPUB étaient remplacés par des pages ou des sites web&nbsp;? Pas sûr que l'interface et les services proposés par Readmill conserveraient leur intérêt.

## Medium
Créée par des co-fondateurs de Twitter Evan Williams et Biz Stone, [Medium](https://medium.com/ "Lien vers Medium") est une plateforme de publication qui repose sur des principes pour le moins originaux&nbsp;:

+ création d'un compte à partir d'un profil Twitter existant, le concept est donc basé sur la gestion de l'identité numérique - notion d'auteur liée à un profil du réseau social&nbsp;;  
+ dans un premier temps on ne peut pas publier, mais uniquement marquer des pages, suivre des blogs et commenter&nbsp;;  
+ une fois l'autorisation permise, on découvre que la publication se fait via des *groupes* — voir par exemple [le *groupe* The Future of Publishing](https://medium.com/the-future-of-publishing "Lien vers Medium")&nbsp;;  
+ l'interface d'écriture est épurée, avec des possibilités de mises en forme très limitées, le dépouillement est clairement au service de l'écriture.

Du côté de la lecture l'interface est toute aussi simple, le texte est central, à tel point qu'on peine même à trouver la date de publication... Écrire sur Medium se fait aussi *dans la marge*, puisque, avec un compte, on peut laisser des commentaires au fil du texte, et même surligner des passages et les commenter.

![Exemple de commentaires sur Medium](../../../../images/medium-commentaires.jpg)

![Exemple de surlignage sur Medium](../../../../images/medium-surlignage.jpg)

>Find something interesting to read.  
[Medium, Page 404](https://medium.com/404)

Medium est fondamentalement une plateforme de **publication**, où tout est pensé pour lire **et** écrire dans les meilleures conditions. L'intégration des commentaires au fil du texte permet des discussions qui ne se limitent pas aux commentaires d'approbation ou de critique. Le webdesign est presque trop [à la mode](http://www.la-grange.net/2013/08/29/designer "Lien vers La Grange") pour être absolument réussi...  

## Vers le web ?
Readmill et Medium ont beaucoup de points communs, autant dans les concepts/principes de lecture et de commentaires/annotations, que dans le design d'interface. Il manquerait presque une version hors ligne de Medium, et Readmill [propose déjà de publier](https://readmill.com/authors/claim "Lien vers Readmill"). N'oublions pas non plus que Readmill et Medium centralisent, chacun à leur manière.  
Le format EPUB est-il destiné à n'être qu'une archive figée face à un web réactif, comme le suppose [Julien](http://blog.walrus-books.com/2013/10/10/le-livre-numerique-comme-une-porte-et-une-cle/ "Lien vers le blog de Walrus")&nbsp;? Ce qui est certain c'est que l'évolution du livre numérique est aussi une question d'interactions, et donc d'interface.  