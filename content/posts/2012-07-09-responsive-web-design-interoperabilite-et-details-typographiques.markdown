---
comments: true
date: 2012-07-09
layout: post
slug: responsive-web-design-interoperabilite-et-details-typographiques
title: Responsive web design, interopérabilité et détails typographiques
categories:
- carnet
---
Les sites Web qui s'adaptent aux différentes tailles d'écrans se multiplient, signe profond qu'une interface Web ne doit plus exister sous différentes versions, mais qu'elle doit simplement s'adapter.

Le "responsive layout" ou ["responsive web design"](http://fr.wikipedia.org/wiki/Responsive_Web_Design "Lien vers Wikipédia") est apparu il y a un peu plus de deux ans&nbsp;: des feuilles CSS qui permettent d'adapter le site en fonction de la largeur de l'écran (ou de la fenêtre), avec l'utilisation plus ou moins intensive du Javascript. Un bon exemple est le blog de [Jérôme](http://phollow.fr/ "Lien vers phollow.fr")&nbsp;: le site/blog s'adapte réellement au support et/ou à l'interface de lecture. Pour une largeur de 1000 pixels ou plus, la page d'accueil se sépare en plusieurs colonnes ; à partir de 700 ou 800 pixels, le site ne se compose plus que d'une seule colonne : idéal pour une lecture sur smartphone. Entre ces deux type d'interface de lecture, la mise en page n'est pas la seule à avoir évoluée, la taille de la police et l'interlignage ne sont pas non plus les mêmes. Ce carnet repose aussi (actuellement) sur un thème "extensible", s'appuyant sur le [Scherzo theme](http://leonpaternoster.com/wp-themes "Lien vers le site de Leon Paternoster").

Les paramètres de tailles de police ou d'interlignage évoluent en fonction de seuils (largeur de l'écran et/ou de la fenêtre). Ces seuils peuvent être plus ou moins nombreux, on peut imaginer que plus ils seront nombreux et plus l'interface de lecture s'adaptera rééllement aux écrans, assurant une interopérabilité maximale. Avec ce type de Web design, la règle typographique suivante pourrait presque être respectée :

>Pour un corps et un caractère donnés, plus une ligne est longue, plus l'interlignage devra être important.  
[Jost Hochuli, Le détail en typographie, Éditions B42](http://www.editions-b42.com/books/le-detail-en-typographie/)

L'arrivée des touts petits écrans nous a, à mon sens, sauvé d'une tendance qui commençait à prendre beaucoup trop d'ampleur : créer des sites Web pour des écrans très larges (plus de 1280 pixels). Et iOS nous a sauvé d'un deuxième fléau : Flash, au risque qu'aujourd'hui certains sites Web ne deviennent [trop dépendants de Javascript](http://www.w3.org/Translations/WCAG20-fr/#ensure-compat "Lien vers les spécifications WCAG2.0").

Le responsive Web design nous sauve aujourd'hui de la multiplication des interfaces données pour un même site Web&nbsp;: "version bureau" et "versions mobiles", ces dernières étant plus ou moins réussies, et s'activant avec des critères plus ou moins pertinents (parfois en fonction du système d'exploitation...). Le Web n'est plus qu'un&nbsp;: les sites doivent être pensés en fonction des utilisations qui en seront faites, et non pour appliquer une charte graphique qui ne correspond parfois à rien en terme d'usage.