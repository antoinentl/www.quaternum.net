---
layout: post
title: "Le livre web comme objet d'édition"
date: "2016-03-07T23:00:00"
comments: true
published: true
description: "Observation d'un micro-phénomène : les livres web, comme réponse à l'appel à communication du colloque Écrire, éditer, lire à l’ère numérique : design et innovation dans la chaîne du livre (12 et 13 avril 2016, Nîmes)."
categories: 
- carnet
slug: le-livre-web-comme-objet-d-edition
---
Réponse à l'appel à communication du colloque [Écrire, éditer, lire à l’ère numérique : design et innovation dans la chaîne du livre](http://ecridil.hypotheses.org/), qui se déroulera les 12 et 13 avril 2016 à Nîmes.

<!-- more -->

Depuis plusieurs années des auteurs de *non fiction* utilisent le web comme forme et moyen de diffusion de leurs livres. *Practical Typography* de Matthew Butterick[^1]&nbsp;; *Le design des programmes*, thèse d'Anthony Masure[^2]&nbsp;; *Atomic Design* de Brad Frost[^3], encore en cours d'écriture&nbsp;; *Startup Playbook* de Sam Altman[^4]&nbsp;; etc. Autant de projets de *livres web* qui ont vu le jour en ligne et sont désormais des sources largement consultées ou citées dans leurs domaines. Mis à jour régulièrement, ce sont des livres en mouvement.

Loin des plateformes d'éditeurs, ces initiatives indépendantes répondent à des logiques liées au web plus qu'à l'édition *classique*&nbsp;: rapidité de mise en ligne, formes hétérogènes et souci du design *numérique*, facilité de diffusion notamment par le référencement, ouverture des contenus, possibilité de mises à jour et d'évolutions, accessibilité et détails typographiques.

Rappelant un mouvement comme le fanzinat dans les années 1990 -- le coût des photocopies devenant accessible, les publications indépendantes et alternatives ont connu un essort incroyable pendant une vingtaine d'années[^7] --, les *web books* ravivent ce désir de partage et de liberté à travers des objets numériques souvent originaux et très lisibles. Parfois jugés pauvres technologiquement -- comme le fanzine à son époque --, et notamment face au format de livre numérique qu'est l'EPUB, les *livres web* restent pourtant bien plus accessibles que la plupart des livres numériques, ces derniers étant dépendants de logiques économiques et des plateformes -- souvent fermées -- associées.

Même s'il existe de nombreux services en ligne qui permettent d'écrire, de publier et de diffuser un livre au format web[^5], les *web books* sont souvent fabriqués par leurs auteurs. Cette volonté de maîtrise de la forme et de la diffusion est une particularité liée à ce phénomène[^8]. Le principal frein des *livres web* est la contrainte de la connexion, problème technique bientôt dépassé[^6]. Formellement plus originaux que les livres au format EPUB, les *web books* doivent encore trouver leur *modèle économique*&nbsp;: prix libre, souscription, liens avec la vente des livres papier, etc. En terme d'usages, ils ont déjà leur place dans un écosystème encore ouvert&nbsp;: le Web.

[^1]: Butterick, Matthew. *Practical Typography* [consulté le 03 février 2016]. Disponible sur le web&nbsp;: [http://practicaltypography.com/](http://practicaltypography.com/)
[^2]: Masure, Anthony. *Le design des programmes, Des façons de faire du numérique* [consulté le 03 février 2016]. Disponible sur le web&nbsp;: [http://www.softphd.com/](http://www.softphd.com/)
[^3]: Frost, Brad. *Atomic Design* [consulté le 03 février 2016]. Disponible sur le web&nbsp;: [http://atomicdesign.bradfrost.com/](http://atomicdesign.bradfrost.com/)
[^4]: À titre d'exemple, voir la plateforme GitBook, basée sur le protocole Git&nbsp;: [https://www.gitbook.com/](https://www.gitbook.com/)
[^5]: Altman, Sam. *Startup Playbook* [consulté le 03 février 2016]. Disponible sur le web&nbsp;: [http://playbook.samaltman.com/](http://playbook.samaltman.com/)
[^6]: La technologie *Service Workers* permet de gérer le cache et d'accéder à des sites web hors connexion&nbsp;: Service Workers, W3C Working Draft 25 June 2015 [https://www.w3.org/TR/service-workers/](https://www.w3.org/TR/service-workers/)
[^7]: Mouquet, Émilie. F comme fanzines…. *Bulletin des bibliothèques de France* [en ligne], n° 6, 2015 [consulté le 03 février 2016]. Disponible sur le Web&nbsp;: [http://bbf.enssib.fr/consulter/bbf-2015-06-0038-005](http://bbf.enssib.fr/consulter/bbf-2015-06-0038-005). ISSN 1292-8399.
[^8]: Gregory, Owen. In Their Own Write: Web Books and their Authors. *24 ways* [consulté le 03 février 2016]. Disponible sur le Web&nbsp;: [https://24ways.org/2013/web-books/](https://24ways.org/2013/web-books/)