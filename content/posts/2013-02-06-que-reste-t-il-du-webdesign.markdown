---
comments: true
date: 2013-02-06
layout: post
slug: que-reste-t-il-du-webdesign
title: Que reste-t-il du webdesign ?
categories:
- carnet
slug: que-reste-t-il-du-webdesign
---
A List Apart vient de changer de version [(de la 4.0 à la 5.0)](http://alistapart.com/article/a-list-apart-relaunches-new-features-new-design "Lien vers A List Apart")&nbsp;: ergonomie, webdesign, choix typographiques, mais aussi du contenu avec des nouvelles rubriques. **Tout** le contenu du site est réintégré dans cette nouvelle interface. Lissage de l'antériorité, effacement d'une certaine mémoire graphique.  
Pour quelles raisons ?

+ technique : maintenir une ancienne version peut être délicat&nbsp;;
+ esthétique : un nouveau webdesign doit - pourquoi pas - aussi servir les contenus _anciens_&nbsp;;
+ idéologique : donner de la cohérence, unifier, uniformiser, lisser ou effacer - c'est selon.

Cela peut être compréhensible pour beaucoup de sites web, mais cela me semble tellement dommageable pour la lecture web de ne pas tenir compte du contexte graphique, des choix esthétiques qui accompagnait l'écriture du moment. D'une certaine façon le Web s'oublie lui-même "graphiquement", comme si cela n'avait pas d'importance, comme si le webdesign d'origine pouvait être vite oublié au profit de la nouveauté.