---
comments: true
date: 2012-09-29
layout: post
slug: surface-de-lecture-numerique
title: Surface de lecture numérique
categories:
- carnet
---
>Il n'est pas explicable, mais avéré, que l'être humain trouve des surfaces aux proportions vraiment géométriques, intentionnelles, plus agréables ou plus belles que celles définies par des proportions de hasard. Un format laid donne un livre laid.  
[Jan Tschichold, Livre et typographie, Éditions Allia, page 43](http://www.editions-allia.com/fr/livre/299/livre-et-typographie)

En 1953 Jan Tschichold a fait un important travail de recherche sur les tailles, les formats et les proportions des "blocs de texte", des pages, des livres. Difficile et presque inutile de faire une transposition du papier au numérique, pourtant cette question m'intéresse, parce que le texte dématérialisée revêt une dimension profondément dynamique, au moins - pour le moment - dans sa forme.
Le livre numérique est-il par définition "dynamique" ? Il me semble que oui&nbsp;: le texte est recomposable selon le format, les proportions de l'écran et la taille de la police ; la page peut être simple ou double ; les applications de lecture offrent souvent des options de mise en page. S'il existe des livres numériques en "fixed layout", ils doivent être à mon avis considérés comme des exceptions - ce terme ne se voulant pas péjoratif.
Les dispositifs de lecture numérique proposent des formats et des proportions très divers, principalement entre les liseuses et les tablettes - je n'aborde volontairement pas les ordinateurs et les smartphones, dispositifs de lecture numérique moins dédiés à une lecture immersive, même si cela mériterait débat.


## Proportions des écrans

Les liseuses ne permettent d'afficher qu'une seule page, ce qui change considérablement les choses puisque les proportions/constructions des pages des livres papier sont pensées en double page. La page de la liseuse est "orpheline", ce qui n'est pas forcement le cas pour les tablettes, souvent en double page pour un format paysage, ou en simple pour un format portrait.
Les écrans de liseuses ont majoritairement un format 3:4, c'en est d'ailleurs perturbant de similitude, du Kindle au Sony PRS T1 et T2, en passant par la Kobo, la PocketBook et la Odyssey de Bookeen. Pour les tablettes les proportions des écrans sont plus exotiques, l'iPad avec ses 9,7 pouces en 3:4 oblige peut-être à l'originalité, et certaines tablettes, vendues comme des "liseuses", ont un format d'écran très "long" proche du 5:9 ou du 3:5 - comme la Nexus 7 (10:16) par exemple.
Si la liseuse est un objet pensé pour la lecture de textes, ce n'est que très partiellement le cas pour les tablettes, les formats "longs" invitant aussi aux films (16:9).


## La gestion des marges dans un livre numérique

L'ePub est composé de pages HTML, accompagnées d'une ou plusieurs feuilles CSS. La gestion des marges se fait donc notamment avec la propriété "margin".  
La plupart des livres numériques sont proposés dans un seul format ePub, ce qui veut dire que la mise en page devra convenir pour les écrans au format paysage aussi bien qu'au format portrait - autant pour la double page que pour la page orpheline. Lorsque l'on observe des feuilles de style de fichiers ePub, la même valeur est toujours - à travers les différents tests que j'ai pu effectuer - attribuée aux marges intérieures et extérieures, de quelques pourcents. Même chose pour les marges supérieures et inférieures. Loin d'être la preuve d'une créativité frileuse, il s'agit tout simplement d'un pragmatisme de circonstance, une structuration "originale" serait trop risquée. Est-ce qu'il serait intéressant et possible de créer des feuilles CSS différentes selon la taille de l'écran, ses proportions et la lecture en page simple ou double ? En somme faire du "responsive design" dans des fichiers ePub. Attention, même si techniquement certains points techniques du livre numérique rejoignent ceux du Web (couple HTML/CSS), les questions d'affichage diffèrent encore pour le moment, puisque le livre numérique reste majoritairement un objet numérique qui se feuillette, et non qui se "scroll". On peut donc considéré qu'il y a une contrainte supplémentaire dans le cas du livre numérique, avec le découpage des pages.

Pourquoi toutes ces questions autour du format du livre numérique ? Il y a aujourd'hui un double enjeu contradictoire : le premier est la lisibilité - des marges neutres qui s'adaptent à tous les dispositifs de lecture numérique ; le deuxième est celui de la créativité, sortir d'une certaine homogénéité et découvrir d'autres parcours de lecture... Actuellement pratiquement tous les livres numériques proposent la même disposition du bloc de texte dans la page, dommage, surtout s'il ne s'agit que d'un frein technique.