---
layout: post
title: "Un mémoire en dépôt"
date: "2018-06-04T23:00:00"
comments: true
published: true
description: "Depuis quelques jours j'ai ouvert le dépôt d'un mémoire en cours de rédaction, l'occasion de montrer un travail d'écriture en train de se faire. Dépôt ? Mémoire ? Écriture ? Voici quelques explications !"
categories:
- carnet
---
Depuis quelques jours j'ai ouvert le dépôt d'un mémoire en cours de rédaction, l'occasion de montrer un travail d'écriture en train de se faire.
Dépôt&nbsp;?
Mémoire&nbsp;?
Écriture&nbsp;?
Voici quelques explications&nbsp;!

<!-- more -->

## Un billet comme point de départ
En mars 2017 je publie un article après plusieurs semaines de recherche&nbsp;: [Une chaîne de publication inspirée du web](/2017/03/13/une-chaine-de-publication-inspiree-du-web/).
Article qui fait suite à [un entretien](https://jamstatic.fr/2017/01/23/produire-des-livres-avec-le-statique/) que j'ai mené en janvier 2017 avec Eric Gardner, alors designer et développeur chez Getty Publications.
Entretien qui fait lui-même suite à [un article](http://blogs.getty.edu/iris/an-editors-view-of-digital-publishing/) de l'équipe numérique de Getty Publications en mai 2016, que me recommande Frank fin 2016 – merci Frank pour cette incroyable étincelle.

Tout cela pour dire qu'un billet sur mon carnet personnel ne suffit pas à faire le tour de ce qui me semble être une formidable révolution&nbsp;: fabriquer des livres avec les méthodes et les technologies du Web.

Alors en réflexion pour accéder à une équivalence Master 2 par le biais d'une VAE (Validation des acquis de l'expérience), ce sujet m'apparaît comme l'occasion parfaite pour démarrer [un mémoire](https://fr.wikipedia.org/wiki/M%C3%A9moire_(%C3%A9crit)#Le_m%C3%A9moire_universitaire).

## Organiser le travail
Très rapidement, pour écrire ce mémoire, je souhaite utiliser les mêmes méthodes et techniques que celles présentées dans mes _recherches_.
L'un des principes exposé dans l'article initiateur est le versionnement, et Git est un système de gestion de versions.
Git devient donc mon outil de travail – autant dire que j'ai beaucoup appris à l'utiliser grâce à cette démarche.

Pour celles et ceux qui connaissent Git, utiliser un tel dispositif pour travailler seul peut paraître assez peu productif.
Git est pensé pour permettre à plusieurs humains de collaborer sur du code.
Mais seul, Git ne perd pas tout son intérêt, il reste une puissante méthode pour s'organiser, obligeant à une discipline tout en étant un formidable moyen de voyager dans le temps – j'arrête là les superlatifs.
Il faut aussi ajouter à cela quelques fonctionnalités bien pratiques de plate-formes comme GitHub ou GitLab – notamment la gestion d'_issues_ ou tickets.

## Pourquoi ouvrir un dépôt&nbsp;?
En plus d'être un outil d'organisation et un moyen de travailler à plusieurs, Git peut permettre de _visualiser_ un projet, de comprendre la façon dont il est pensé, comment il est agencé, de suivre son évolution, etc.
Tout cela par le biais de plate-formes qui hébergent des projets Git.

L'ouverture publique du dépôt est donc pour moi l'occasion de montrer un travail d'écriture en cours.
Je ne suis pas naïf, l'idée ici n'est pas de recueillir des contributions sur un travail qui reste très majoritairement personnel – l'accompagnement de mes deux directeurs de mémoire étant par ailleurs nécessaire, et dans mon cas incontournable.

**Le dépôt est consultable ici&nbsp;:  
[https://gitlab.com/antoinentl/systeme-modulaire-de-publication/](https://gitlab.com/antoinentl/systeme-modulaire-de-publication/)**

Il me semble que ce type d'expérimentation est encore rare, notamment parce que Git nécessite quelques connaissances – même si les interfaces web peuvent faciliter sa compréhension et gommer certaines aspérités inhérentes à ce système de gestion de versions.
Je ne pense pas que l'usage de Git dans l'exercice du mémoire – ou de la thèse – soit nécessaire, ce que je tente de faire ici n'est donc pas tant une démonstration qu'une tentative.
Après tout, versionner publiquement un travail d'écriture universitaire n'est peut-être pas une bonne idée, mais je tente l'expérience&nbsp;!

Mon choix s'est porté sur la plate-forme GitLab pour deux raisons principales&nbsp;:

- la prédominance de GitHub me fatigue un peu, privilégier des alternatives me semble nécessaire, surtout [en ce moment](https://www.theverge.com/2018/6/3/17422752/microsoft-github-acquisition-rumors) – même si [GitLab](https://about.gitlab.com/) est loin d'être un petit projet indépendant, et GitHub présente des avantages indéniables&nbsp;;
- GitLab propose des dépôts privés – c'est-à-dire qui ne sont pas visibles par des utilisateurs non invités – sans compte payant, idéal comme bacs à sable.

Ce choix est imparfait, j'aurais tout aussi bien pu créer un dépôt chez [Framagit](https://framagit.org/), [Bitbucket](https://bitbucket.org/), ou même héberger ma propre instance de GitLab.
Ou ne pas faire de dépôt public.

## Quelques indications
Il est donc possible de découvrir des morceaux du mémoire en cours de rédaction, pour cela voici quelques recommandations&nbsp;:

- [le README – la description – du dépôt](https://gitlab.com/antoinentl/systeme-modulaire-de-publication/blob/master/README.md) est à lire avant toute chose, il évoluera avec le mémoire&nbsp;;
- les premiers fragments sont lisibles dans le dossier `materiaux`&nbsp;;
- attention il y a des branches de travail qui ne sont pas encore fusionnées avec la branche principale `master`, il faut donc regarder ce qui se passe sur [les autres branches](https://gitlab.com/antoinentl/systeme-modulaire-de-publication/branches)&nbsp;;
- si vous souhaitez suivre l'avancement du projet, la page [Activity](https://gitlab.com/antoinentl/systeme-modulaire-de-publication/activity) est un bon moyen, et il y a même [un flux RSS](https://gitlab.com/antoinentl/systeme-modulaire-de-publication.atom?rss_token=sqtUL4n56hJ7vqcBRE9m).

Pour intervenir sur le projet, plusieurs possibilités&nbsp;:

- [m'écrire](mailto:antoine@quaternum.net), ce qui est peut-être le plus simple&nbsp;;
- ouvrir une _issue_, c'est-à-dire un ticket permettant d'indiquer une erreur, de poser une question, de suggérer un ajout ou un problème&nbsp;: [https://gitlab.com/antoinentl/systeme-modulaire-de-publication/issues](https://gitlab.com/antoinentl/systeme-modulaire-de-publication/issues)&nbsp;;
- modifier un fichier, pour cela vous devrez d'abord _copier_ le projet – faire un _fork_ –, puis proposer une fusion ou _merge request_ avec mon dépôt&nbsp;;
- enfin une bibliographie est disponible sur Zotero, sous forme de groupe, il est donc possible d'ajouter des références&nbsp;: [https://www.zotero.org/groups/2191614/](https://www.zotero.org/groups/2191614/).


## Un mémoire modulaire
Les premières recherches ont débuté il y a plus d'un an, ce mémoire est devenu lui-même modulaire, dérivant d'articles en communications et interventions, avec notamment&nbsp;:

- **un entretien** avec Eric Gardner de Getty Publications&nbsp;: [Publier des livres avec un générateur de site statique](https://jamstatic.fr/2017/01/23/produire-des-livres-avec-le-statique/)&nbsp;;
- **un article** sur Strabic à propos de [PrePostPrint](https://prepostprint.org/)&nbsp;: [Workshop PrePostPrint&nbsp;: Chercher, manipuler, partager, imprimer](http://strabic.fr/Workshop-PrePostPrint)&nbsp;;
- **une communication** en duo avec Julie Blanc à l'occasion du colloque Écolab (Les écologies du numérique) en novembre 2017 à Orléans&nbsp;: (Re)Penser les chaînes de publication&nbsp;: soutenabilité et émancipation&nbsp;;
- **une communication** en duo avec Thomas Parisot à Codeurs en Seine en novembre 2017 à Rouen&nbsp;: [README.book](https://www.youtube.com/watch?v=25wCiZVLNBg)&nbsp;;
- **une participation** à une table ronde à la journée d'étude du Département documentation, archives, médiathèques et édition (DDAME) de l’Université Toulouse - Jean Jaurès en mars 2018 : [Une réappropriation des données par leur structuration](https://www.quaternum.net/2018/04/04/une-reappropriation-des-donnees-par-leur-structuration/)&nbsp;;
- **deux articles** en cours de révision&nbsp;;
- **une communication** dans le cadre du colloque [ÉCRIDIL](http://ecridil.ex-situ.info/) au printemps 2018 à Montréal&nbsp;: [Git comme nouvel ingrédient des chaînes de publication](http://presentations.quaternum.net/git-comme-nouvel-ingredient-des-chaines-de-publication/)&nbsp;;
- et probablement d'autres articles et interventions qui suivront dans les prochains mois.

Ce mémoire croise donc les travaux de [Julie Blanc](http://julie-blanc.fr/) et les réflexions de [Thomas Parisot](https://oncletom.io/) – merci à eux.
La rédaction du mémoire est dans sa phase finale, phase qui devrait durer encore plusieurs semaines.

[Bonne lecture&nbsp;!](https://gitlab.com/antoinentl/systeme-modulaire-de-publication/)
