---
comments: true
date: 2012-01-18
layout: post
slug: mes-lectures-de-la-grange
title: Mes lectures de La Grange
categories:
- carnet
---
>Des notes dans le carnet, je crée, j’invente, des idées en appellent d’autres. Publier ? J’hypertextualise le papier, je le sculpte. Depuis des années, nous faisions passer toutes les brochures du papier vers le Web. Ce cycle s’achève. Nous inventons maintenant les processus pour créer le papier à partir du Web. Le processus de reconstruction est infini. Il ne faut jamais renoncer à se réinventer.  
[Karl Dubost, Self-Design, Carnets de La Grange](http://www.la-grange.net/2009/05/02/self-design)

Découverte du Carnet Web de La Grange de [Karl Dubost](http://www.la-grange.net/karl/) il y a presque un an. D'abord lecture Web, fragmentée, sans linéarité, sans retenue, au fil de mes préférences sur la page d'accueil du Carnet Web, et quand finie dans les archives, par bribes. Puis suivi au fil des publications, souvent en attente, via le flux RSS avec une alerte mail. Lecture du matin, ne pas savoir ce que je vais lire, avec parfois des accumulations, selon le niveau technique des billets et l'humeur du moment. Parfois des pauses de quelques semaines, puis reprises frénétiques, plaisir de me balader dans les [archives](http://www.la-grange.net/map), de retrouver des billets déjà lus, de découvrir d'autres textes que j'avais, avant, évités.  
Lecture Web principalement sur smartphone, mais aussi sur netbook de 10 pouces ou sur écrans plus grands.

J'avais écrit un billet maladroit [ici](https://www.quaternum.net/2011/11/29/faire-dun-site-un-livre/), juste avant la sortie simultanée des deux livres numériques, des deux éditorialisations de [La Grange](http://www.la-grange.net/). Jean-François Guayrard pour [Numériklivres](http://comprendrelelivrenumerique.com/2011/11/30/ebook-avez-vous-connu-lamour-karl-dubost-defi/) et François Bon pour [publie.net](http://www.tierslivre.net/spip/spip.php?article2674) ont donc publié le 30 novembre deux versions, deux approches, deux "sélections" très différentes de ces 10 années d'écriture quasi quotidienne.

Regard sur ma propre pratique après la lecture de _L'ange comme extension de soi_ ([publie.net](http://www.publie.net/fr/ebook/9782814505506/l-ange-comme-extension-de-soi)) : je lis d'un trait, parce qu'embarqué dans ces séries de billets, qui donnent une toute autre dimension aux textes. Voir la répétition de certains sujets, à plus ou moins long terme, certains billets qui se ressemblent beaucoup, voir les ajouts parfois minimes et décisifs, et le style qui évolue d'année en années... Oublier parfois que ce ne sont pas des paragraphes d'un même texte mais des billets rassemblés. Le livre permet cette accumulation, ces rencontres entre des textes qui étaient éloignés dans le temps, et dans un certain espace (la "[map](https://www.quaternum.net/2011/11/02/continuite-permeabilite-des-connexions/)" du site).

La lecture de _Avez-vous connu l'amour ?_ ([Numériklivres](http://comprendrelelivrenumerique.com/2011/11/30/ebook-avez-vous-connu-lamour-karl-dubost-defi/)) a été très différente, il s'agit d'un autre livre, qui a presque peu de rapports avec _L'ange comme extension de soi_ (il n'y a d'ailleurs qu'un ou deux billets en commun entre ces deux sélections).

Il n'est pas question ici de préférences entre ces deux textes, mais plutôt de ce que publie.net et Numériklivres ont réussi à faire avec la même matière, mais aussi avec des choix significativement différents. La Grange est tellement riche que la réalisation de ces deux projets a été possible. Le livre numérique permet de faire cela à partir du Web.

Si ma lecture de ces deux livres numériques a été sans précédent par rapport aux expériences de lecture que j'avais eu jusque là du Carnet Web de La Grange, tant en découverte, en densité, quelques remarques tout de même sur mon expérience. La lecture Web me permet de ne pas tout lire d'une traite (même si ces deux livres ne reprennent pas l'ensemble du site Web de La Grange), la publication au fil du temps, sorte de feuilletons, me permet de comprendre, d'intégrer et de "digérer" d'une autre manière. Ces livres je les ai lus dans un même mouvement, même si en plusieurs fois. L'inscription et les réactions n'ont pas été les mêmes en moi.  
J'ai utilisé d'autres "dispositifs de lecture", j'ai lu ces livres numériques sur netbook (10 pouces), sur smartphone, sur liseuses, sur tablette. _Avez-vous connu l'amour ?_ ne comporte que peu de liens, mais avec _L'ange comme extension de soi_ la lecture sur liseuse (Odyssey puis Cybook Opus de Bookeen) fut souvent frustrante, à cause de l'impossibilité de lire le Web qu'offre La Grange à travers de multiples liens, j'étais presque obligé d'avoir le smartphone ou le netbook à portée de main pour explorer ces rebonds. La lecture sur liseuses m'a justement permis cette rencontre plus forte avec les textes de Karl Dubost. Mais je reviendrai une autre fois sur ces questions de "dispositifs de lecture".

J'aurai du mal à parler du contenu tant les textes de Karl sont beaux et justes, de ceux dont l'on a envie de citer une ligne sur deux, au moins. Je me rends compte que son carnet Web me donne envie d'y retourner parce qu'il ne s'arrête pas, alors que j'ai refermé les deux livres numériques sans les rouvrir ensuite (si ce n'est pour montrer et partager). Mais ces deux livres m'ont permis de découvrir autre chose, qui était pourtant là sous mes yeux.

[L'ange comme extension de soi](http://www.publie.net/fr/ebook/9782814505506/l-ange-comme-extension-de-soi)
[Avez-vous connu l'amour ?](http://librairie.immateriel.fr/fr/ebook/9782924060124/avez-vous-connu-l-amour)