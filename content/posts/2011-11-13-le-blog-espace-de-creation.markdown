---
comments: true
date: 2011-11-13
layout: post
slug: le-blog-espace-de-creation
title: Le blog, espace de création
categories:
- carnet
---
J'avais assisté il y a plusieurs années à [une table-ronde sur les blogs](http://www.periscopages.org/page.php?id=125), organisée par (feu) le [festival Périscopages](http://www.periscopages.org/) à Rennes, avec Nylso, Lisa Mandel et Bartolomé Sanson (Kaugummi). [Nylso](http://nylso.free.fr/Blog/index.php), dessinateur et auteur de bandes dessinées (aux côtés de Marie Saur), parlait déjà à cette époque du blog comme d'un espace de création à part : pas comme d'un objet de publication similaire au livre, ou comme un outil de communication.

Récemment ([le 26 octobre 2011](http://radio.grandpapier.org/Emission-du-26-octobre)), Nylso et Marie Saur étaient invités sur Radio Grandpapier, Nylso a reparlé de son blog comme d'un "endroit de liberté en noir et blanc". Il s'explique, lorsqu'il a lancé son blog il y a 5 ans, il voulait "vraiment militer pour des blogs qui puissent ressembler à des fanzines" ; il ajoute&nbsp;: "l'avènement d'une nouvelle technologie fait un nouveau mouvement", de la même façon que les photocopieurs ont révolutionné la micro édition et le fanzinat. "Avec [Éric](http://maher.free.fr/dotclear/index.php) [éditions La Chose], on s'est dit qu'on allait utiliser les blogs dans le bon sens", entre le carnet et l'espace de liberté. Pas un carnet totalement "public" puisque le but n'est pas non plus de tout montrer.

"On peut aller autant dans l'underground avec un blog, qu'avec d'autres médiums", rien de nouveau dans ce discours, juste l'affirmation d'un positionnement face à des pratiques qui reprennent finalement des schémas éculés (prépublications sur le Web plutôt que dans des revues papier, livres papier qui reprennent le contenu d'un site/blog).

La notion de "underground" pourrait aussi être interrogée.