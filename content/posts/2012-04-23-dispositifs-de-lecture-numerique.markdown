---
comments: true
date: 2012-04-23
layout: post
slug: dispositifs-de-lecture-numerique
title: Dispositifs de lecture numérique
categories:
- carnet
---
J'ai participé pendant deux mois et demi à l'expérimentation [Calliopê de l'enssib](http://www.enssib.fr/chantier-mine/calliope-usages-des-dispositifs-numeriques-de-lecture-en-bibliotheque "Lien vers le site de l'enssib"). Calliopê c'était&nbsp;: une quinzaine de personnes, principalement des étudiants (anciens et actuels) et des personnels de l'enssib&nbsp;; des dispositifs de lecture numérique (ordinateurs portables, netbooks, smartphones, tablettes et liseuses) dont certains mis à disposition (tablettes et liseuses)&nbsp;; [une étagère numérique](http://www.enssib.fr/etagere-numerique)&nbsp;; un processus d'évaluation&nbsp;; des gens qui se sont rencontrés et qui ont beaucoup discuté.

Durant les différents "focus groups" les échanges ont été nombreux, et très riches. Les discussions ont permis d'aborder à plusieurs reprises les différents dispositifs de lecture numérique. À l'intérieur d'un petit groupe nous avons réussi à déterminer les principales qualités que nous attendions d'un dispositif de lecture numérique, et ses caractéristiques&nbsp;:

1. l'ouverture : comment accède-t-on, à travers une machine et des logiciels/applications, à du contenu ? A-t-on la possibilité de lire des fichiers ePub, des PDF, d'autres types de fichiers ? Les modes de lecture en ligne et hors ligne sont-ils satisfaisants ? Le dispositif est-il (physiquement) facilement maniable ? Son autonomie est-elle suffisante pour l'usage que l'on en fait&nbsp;?
2. l'écran : l'interface de lecture, la "dalle". Dans le cas d'une liseuse l'attention va se porter sur la qualité des contrastes, et le rendu général&nbsp;: rapprochement avec l'aspect d'un poche ou d'un papier journal par exemple. Dans le cas d'une tablette il va plutôt s'agir de la qualité de la vitre (reflets), et des réglages de la luminosité et des contrastes&nbsp;;
3. l'interface d'écriture et d'interaction : quels sont les outils d'écriture mis à disposition sur le dispositif de lecture numérique, à l'intérieur (par exemple pour les notes) ou à l'extérieur du logiciel de lecture&nbsp;?

On peut développer la liste en ajoutant encore plusieurs points (précisions sur la connectivité, autres applications, fiabilités de l'outil à moyen et long terme, degré d'ouverture du système...).