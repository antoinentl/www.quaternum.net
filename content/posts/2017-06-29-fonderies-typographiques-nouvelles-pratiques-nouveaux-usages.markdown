---
layout: post
title: "Fonderies typographiques : nouvelles pratiques, nouveaux usages"
date: "2017-06-29T12:00:00"
comments: true
published: true
description: "Compte rendu de la rencontre Fonderies typographiques : nouvelles pratiques, nouveaux usages, organisée par les Rencontres internationales de Lure en partenariat avec le Labo de l'édition mardi 29 avril 2014. Originellement publié sur le site web du Labo de l'édition, je dépose ici ce compte rendu pour archive."
categories: 
- carnet
---
Compte rendu de la rencontre "Fonderies typographiques&nbsp;: nouvelles pratiques, nouveaux usages", organisée par les Rencontres internationales de Lure en partenariat avec le Labo de l'édition mardi 29 avril 2014. Originellement publié sur le site web du Labo de l'édition, je dépose ici ce compte rendu pour archive.

<!-- more -->

## Introduction par Frank Adebiaye
Rencontre organisée conjointement par les Rencontres internationales de Lure et le Labo de l'édition, dans les locaux du Labo de l'édition, lieu emblématique de l'édition numérique&nbsp;: l'occasion d'aborder la question de la typographie numérique, qui en est elle-même au moins à sa seconde révolution. Après l'essort de la PAO dans les années 80, nous sommes maintenant à l'époque d'Internet, du web, et de la typographie *embarquée*. Si le web a permis aux fonderies typographiques la diffusion de leurs travaux, aujourd'hui quel est l'impact du *numérique* sur les pratiques et les usages de ces fonderies&nbsp;?  
Jean François Porchez — dessinateur de caractères internationalement reconnu et fondateur de Typofonderie — et Raphaël Bastide — designer graphique et dessinateur de caratères, défenseur inlassable de la culture libre — vont répondre à ces questions.


## Raphaël Bastide

Raphaël Bastide aborde les nouveaux usages de la typographie, sur le web, (libre ou pas libre) sur le papier et sa position par rapport à ces différents accès.

### Pratiques professionnelles et amateures
Raphaël Bastide nous présente tout d'abord le travail de studios qui ont une pratique intéressante et atypique, notamment sur des approches amateures, autour de la typographie libre, avec une démarche qui dépasse les choix graphiques&nbsp;:

+ [Vier5](http://vier5.de/)&nbsp;: approche côté amateur, punk, cyberpunk, travail autour du caractère brut&nbsp;;
+ [Open source publishing](http://osp.constantvzw.org/work/)&nbsp;: collectif belge qui travaille avec la typographie libre.

Et nous donne son point de vue sur Google Fonts&nbsp;: 632 caractères, on a tout ce qu'on veut sauf des choses utiles. Un entre-deux qui n'est pas confortable&nbsp;:

+ les sources sont très difficilement trouvables et Google Fonts ne souhaite pas particulièrement que les caractères soient modifiés&nbsp;;
+ parmi ces 632 caractères il y a beaucoup de choses sans personnalité&nbsp;: donc peu utiles dans le cadre d'un usage professionnel.


### Nouveaux faiseurs de caractères
Raphaël Bastide parle de *faiseurs* plutôt que de *dessinateurs de caractères* car aujourd'hui les faiseurs ne sont plus seulement des designers mais ils peuvent être des amateurs. Mouvement proche de celui qu'a connu la photographie. Quelques exemples&nbsp;:

#### Oyvind Kolas
Oyvind Kolas a fait [un programme](http://pippin.gimp.org/) qui permet de générer des fontes, qui donne des résultats étranges, utilisable dans peut-être 10% des cas. Certains typographes comme Oyvind Kolas ne sont plus des designers mais des *hackers*&nbsp;: des personnes qui créent des outils.

#### Typographie générative
Depuis plusieurs années on assiste à une explosion des outils permettant la génération de typographies, avec aujourd'hui certains d'entre eux qui sont réellement utilisables par le plus grand nombre&nbsp;:

+ [Metapolator](http://metapolator.com/)&nbsp;: projet — subventionné par Google — de logiciel libre qui permet de générer facilement des masters, des squelettes, à partir desquels on peut générer énormement de familles de caractères. Tout devient très facile à faire&nbsp;;
+ [Prototypo](http://www.prototypo.io/)&nbsp;: projet français de création rapide de caractères à partir d'une application en ligne, sur le web.

La typographie générative est, pour Raphaël Bastide, l'occasion de remettre en cause la question de la classification typographique.


### Renouveau de la classification typographique
Il ne s'agit plus de classer le caractère mais le processus de création&nbsp;: quelle licence est attribuée au caractère ou à la famille de caractères&nbsp;? Pour Raphaël Bastide, impliqué dans une pratique basée sur le libre, il est plus intéressant de savoir quelle est la licence d'un caractère que sa classification. Ainsi chaque fonderie parle de sa propre classification à travers ses choix concernant les usages offerts aux utilisateurs&nbsp;: une classification décentralisée où chaque fonderie parle son langage.


### Nouveaux modèles économiques
Caractères libres, générés, webfonts&nbsp;: il s'agit de distribuer plus qu'un fichier binaire, il faut aussi donner une explication, livrer de la documentation, des exemples de mises en œuvre, un logiciel qui va avec dans le cas de la typographie générative. Nous ne sommes plus dans le même contexte qu'il y a 10 ou 20 ans, désormais "on veut une histoire", on attend un savoir-faire. Par exemple la révolution de la typographique générative va entraîner une mise en avant de l'artisanat.

### Présentation de deux projets

#### Use & Modify
[Use & Modify](http://usemodify.com/) est un CMS — développé par Raphaël Bastide — qui permet de faire un site de fonderie et de présenter des typographies, avec des sections, des tags, bref de faire de la médiation, de la *curation*, autour de la typographie. Cet outil répond à la question&nbsp;: comment diffuser rapidement une production typographique — libre ou pas libre&nbsp;?

#### Steps Mono
Travail d'explication d'un processus de création à quatre mains sur un caractère typographique, visible dans le numéro 120 de la revue *Étapes*.


## Jean François Porchez

La présentation de Jean François Porchez s'intitule *Sublimer le design typographique*&nbsp;: le métier de typographe et son savoir-faire. Il retrace l'histoire récente de la typographie depuis l'arrivée de la PAO dans les années 1980.

### Petits rappels historiques autour des pratiques des fonderies
Travail *à l'ancienne*&nbsp;: l'important pour les fonderies était de bien présenter les caractères, de montrer leur savoir-faire. Si les évolutions de la typographie ont été nombreuses, notamment avec la photocomposition, la révolution typographique n'a pas attendu le web&nbsp;: l'ITC — International Typeface Corporation — a par exemple permis la diffusion de caractères et la valorisation du travail de création typographique dès les années 60, notamment en transformant des fonderies comme Linotype ou Monotype en distributeurs. La typographie est alors déjà accessible à tous le monde.

### Culture graphique
Dans les années 70 lancement se lancent de nouvelles revues diffusées gratuitement&nbsp;: le meilleur moyen de se faire connaître est de parler de *culture graphique*, et la typographie va s'y insérer tout naturellement.

### Fonderies indépendantes et individuelles
Les premières fonderies indépendantes apparaissent dans les années 80, comme Fontshop en 1989. Les moyens de rencontres et de diffusion ne reposent pas encore sur Internet&nbsp;:

+ exemple d'une présentation d'un caractère de Jean François Porchez à Fontshop lors des Rencontres internationales de Lure&nbsp;;
+ l'ATypI est un autre exemple de ces espaces de rencontres et de diffusion.

L'apparition des fonderies individuelles — comme Bitstream de Matthew Carter en 1981 — est le résultat d'un syndrome&nbsp;: la volonté pour des designers graphiques de maîtriser leurs créations.

### Notre passage au numérique
En France le passage au numérique ne concerne que quelques designers au début des années 1990&nbsp;: il faut surtout citer François Boltana dans l'avénement des *nouvelles* fonderies. Au début des années 1990 apparaissent aussi des distributeurs de caractères numériques&nbsp;: Fontshop notamment.

### Création de Typofonderie et espaces d'échanges
Création de [Typofonderie](http://typofonderie.com/) en 1994&nbsp;: Internet n'existe pas encore et se faire connaître n'est pas simple. Jean François Porchez édite un guide typographique à compte d'auteur&nbsp;: une bonne manière de faire la promotion de son savoir-faire à cette époque. À cette période les plateformes d'échanges "se font en vrai", par exemple via les Rencontres de Lure. Nombreux débats et crispations autour du passage d'un savoir-faire d'un petit nombre à un beaucoup plus grand nombre, comme le montre Raphaël Bastide. Par exemple aujourd'hui la typographie générative est peut-être l'apprentissage de l'écriture numérique.

### Ce que le web a permis
La vente des fontes en 1996 consistait à l'envoi de fax et de disquettes, et des licences par courrier. Le web a permis "de toucher une niche, mais à l'international", via des outils comme un catalogue disponible 24h/24 et 7J/7, un formulaire de commande en ligne, et surtout la Gazette lancée au même moment, tout cela est une vraie révolution&nbsp;! Puis ensuite l'apparition des contenus dynamiques avec la possibilité de commander en ligne.

### Tisser un réseau
Les Rencontres internationales de Lure, ATypI à Lyon en 1998&nbsp;: autant d'occasions d'animer la scène typographique, de croiser les talents, en France et à l'étranger, avec des jeunes et moins jeunes.

### Valoriser et promouvoir
Naissance du Typographe.com — site, blog, forum — parce que la presse graphique comprend mal le renouveau de la typographique&nbsp;: il y a une nécessité de promouvoir ce travail, les graphistes vont prendre en main cette démarche de valorisation. Le Typographe deviendra par la suite [la Gazette](http://typofonderie.com/gazette/).  
Création de ZeCraft consacrée aux identités de marque et aux caractères de commande, par opposition à Typofonderie, dévolue à la vente de caractères de catalogue. On retrouve dans cette démarche cette volonté de diffuser et valoriser le travail de typographe.


### Qualité. Longévité. Intégrité. 20 ans.

>On est tout petit mais on touche un public à l'internationale.

La ligne éditoriale et la rigueur associée nécessaire est déterminante pour Typofonderie. Les produits doivent être soignés, tout comme la démarche&nbsp;: il faut conserver la confiance acquise avec les clients. Le lancement d'un caractère typographique ne doit pas être uniquement la mise en ligne, la vente sur un site, un PDF et un tweet&nbsp;: c'est ici qu'intervient le travail effectué sur [la Gazette](http://typofonderie.com/gazette/), parler du travail des typographes, interviewer les graphistes.



## Débat animé par Frank Adebiaye

### Retour sur Google Fonts
**Raphaël Bastide**&nbsp;: le point commun avec Jean François Porchez est le fait que derrière les caractères il y a de l'humain. Google Fonts est l'exemple concret où l'utilisateur devient le produit et n'est plus un simple utilisateur, Google Fonts n'est pas assez humain.  
**Jean François Porchez**&nbsp;: il faut d'abord préciser ce qu'est une fonte gratuite, exemple avec les fontes installées par défaut sur les systèmes d'exploitation. "La typographie a un prix", et c'est le cas dans Windows où de nombreux typographes ont travaillé pour mettre des fontes de qualité à disposition. Même chose avec les outils d'Adobe et les typographies proposées par défaut. Gratuit&nbsp;? Accessible librement&nbsp;? La problématique du gratuit est donc là depuis longtemps, et Google Fonts n'est qu'une nouvelle étape. Mais quel est l'avantage pour le designer qui a mis à disposition une fonte sur Google Fonts&nbsp;? Sinon d'avoir des pages vues et parfois une rémunération&nbsp;?  
**Raphaël Bastide**&nbsp;: on ne peut pas ignorer la présence de Google Fonts. L'avantage c'est que l'on peut modifier les caractères puisqu'ils sont libres, et il y a parfois des caractères intéressants. Google est en train de poser les jalons de la typographie itérative, et investie dans la recherche typographique. Il est important pour les designers, pour les artisans, d'utiliser Google Fonts pour comprendre comment cela fonctionne, de voir ce qu'il est intéressant de prendre dans ce type de projet.

### Défi de l'objet livre numérique
**Raphaël Bastide**&nbsp;: la qualité c'est la licence pour pouvoir modifier le logiciel, ou le caractère, ce point est essentiel dans la démarche de designer de Raphaël Bastide qui se définit comme un *hacker*, avec la dimension politique que cela comporte. Chaque caractère trouvera son utilisateur&nbsp;: par le biais des licences qui permettent de modifier les fontes.  
**Jean François Porchez**&nbsp;: différence ou antagonisme avec Raphaël Bastide qui tient sur le fait de pouvoir modifier une création. Pour certains artistes la copie *est* du piratage, quand bien même la copie est la base d'une nouvelle création&nbsp;: le cœur du débat c'est de savoir comment la création est perçue, quelle est notre attitude par rapport à la création des autres&nbsp;? Mettre en place une création avec une communauté, dans un processus *open source*, ne fonctionne pas, car les designers souhaitent promouvoir *leur* création. Concernant le livre numérique&nbsp;: l'objet esthétique est — pour le moment — très similaire à celui du livre papier. On assiste au passage d'un objet statique à un objet dynamique, notamment avec l'intégration de la connexion et du paiement au chapitre, et donc des modèles économiques différents.

>La seule chose qui reste dans le livre *numérique* c'est le caractère typographique.

**Raphaël Bastide**&nbsp;: travailler uniquement avec des logiciels libres, en tant que designer graphique, est pour Raphaël Bastide une façon de se mettre en difficulté, de créer des outils et des programmes, de participer à d'autres programmes, bref de mettre en place des créations *ouvertes* où les personnes qui les perçoivent ou qui y participent sont *actives*. Il ne faut peut-être pas critiquer trop vite les modèles de création par la communauté&nbsp;: contribuer à un caractère typographique n'est pas simple, et les outils de génération typographique vont peut-être permettre de dépasser ces difficultés.  
**Jean François Porchez**&nbsp;: l'open source est peut-être *bien* pour la technique et la technologie, en revanche la problématique de la création typographique touche à la création plastique. La collaboration a peut-être uniquement du sens dans une démarche de pédagogie. La valorisation est nécessaire, et ça passe par un vrai choix éditorial, et pas une pluralité de possibilités. La collaboration horizontale ne peut pas fonctionner dans le cas de la création typographique&nbsp;: il doit y avoir un choix pris par un *boss*, et c'est la valorisation du travail typographique.

### Question de la création
**Olivier Nineuil**&nbsp;: ce sujet des outils et des savoir-faire sera justement abordé pendant la session 2014 des [Rencontres internationales de Lure](http://delure.org/). Aujourd'hui il y a beaucoup de fonderies qui produisent beaucoup, mais quelle est leur positionnement en tant qu'éditeur&nbsp;? Il y a une surenchère continue&nbsp;: performances, nombre de caractères disponibles, etc. Mais quelle est la ligne éditoriale de ces fonderies&nbsp;?  
**Raphaël Bastide**&nbsp;: démarche de designer qui est de partir d'un caractère existant et de le modifier pour que cela devienne *sa* création, en respectant les licences. Raphaël Bastide rejoind la position de Jean François Porchez car il veut arriver à *sa* création.  
**Grégori Vincens (fondateur de Font You)**&nbsp;: Raphaël Bastide parle d'une démarche issue du web libre, là où Jean François Porchez se positionne comme auteur. Ce qui est intéressant c'est l'usage&nbsp;:

>Comment ce deuxième geste typographique peut générer de la créativité&nbsp;?

L'objectif de [Font You](http://fontyou.com/) c'est de convoquer cette créativité là, avec un rôle de *curator* pour exacerber la "créativité libérée". La problématique est très différente entre Font You et Typofonderie par exemple.

[Enregistrement de la rencontre effectuée par Peter Gabor](https://soundcloud.com/labodeledition/fonderies-typographiques-nouvelles-pratiques-nouveaux-usages)