---
layout: post
title: "Écrire un livre en 2017"
date: "2017-03-07T10:00:00"
comments: true
published: true
description: "Thomas Parisot a récemment présenté son expérience d'écriture de son livre sur Node.js : basé sur l'interopérabilité et le versionnement, via l'utilisation d'AsciiDoc et de GitHub, ce processus ouvre de nombreuses perspectives pour les chaînes d'édition. Retour sur sur cette rencontre accueillie par La Péniche à La Coop à Grenoble."
image: "https://www.quaternum.net/images/2016-livre-web-livre-mini.png"
categories: 
- carnet
---
[Thomas Parisot](https://oncletom.io/) a récemment présenté son expérience d'écriture de son livre sur [Node.js](https://oncletom.io/node.js)&nbsp;: basé sur l'interopérabilité et le versionnement, via l'utilisation d'AsciiDoc et de GitHub, ce processus ouvre de nombreuses perspectives pour les chaînes d'édition. Retour sur sur cette rencontre accueillie par La Péniche à La Coop à Grenoble.

<!-- more -->

Thomas est en train d'écrire un livre sur [Node.js](https://fr.wikipedia.org/wiki/Node.js), et il a entamé une résidence itinérante pour finaliser ce travail, il s'en explique sur son blog&nbsp;: [Résidence d'écriture Node.js](https://oncletom.io/2017/residence-nodejs/). Je lui ai proposé de venir échanger autour de cette démarche d'écriture et il a accepté -- **merci Thomas&nbsp;!** La Coop -- infolab et espace de coworking -- nous a accueilli [jeudi 23 février 2017](http://www.la-coop.net/evenement/ecrire-avec-github/) pour cette rencontre -- **merci à eux&nbsp;!**

## Les chaînes de publication sont rouillées
Le constat de départ, en tant qu'auteur, est le suivant&nbsp;: travailler sur un texte avec l'équipe d'une maison d'édition est complexe. Thomas avait déjà eu l'occasion de publier [un autre livre](http://www.eyrolles.com/Informatique/Livre/reussir-son-blog-professionnel-9782212127683) avec Eyrolles, voici une petite liste des problèmes rencontrés en lien avec la chaîne d'écriture et de publication utilisée à l'époque&nbsp;:

- le format .doc et les logiciels de traitement de texte -- LibreOffice Writer ou Microsoft Word -- ne sont pas conçus pour travailler à plusieurs sur un texte. Les phases de relecture, de commentaires et de corrections sont pénibles et douloureuses&nbsp;;
- les nombreux échanges par mails arrivent en phase finale d'écriture pour informer des relectures et interventions sur le texte après une longue traversée en solitaire de l'auteur. Ces échanges ne sont pas très performants et, dans ce cas cet outil asynchrone qu'est la messagerie électronique devient vite ingérable&nbsp;;
- les versions numérotées des fichiers -- `chapitre-01-relecture-TP-ok-v-03.doc` par exemple -- ne permettent pas une gestion très fine dans le temps.

 
## Faciliter la collaboration autour d'un texte
Depuis plusieurs années des auteurs et des éditeurs ont expérimenté d'autres méthodes d'écriture. J'y reviendrai bientôt dans un long billet, mais regardons comment Thomas s'est organisé pour écrire son livre et modifier la collaboration avec son éditeur&nbsp;:

- utilisation d'un langage sémantique simple pour écrire -- *inscrire et structurer le texte*&nbsp;: choix d'[AsciiDoc](http://www.methods.co.nz/asciidoc/), plus puissant que [Markdown](https://fr.wikipedia.org/wiki/Markdown) dans le cas de l'écriture d'un livre&nbsp;;
- export aux formats `.doc` et PDF à partir du format AsciiDoc et d'[AsciiDoctor](http://asciidoctor.org/)&nbsp;;
- publication du code [sur GitHub](https://github.com/oncletom/nodebook), plateforme d'hébergement de code basée sur git. Cela permet à n'importe qui de lire le code source du livre -- [en version brute](https://raw.githubusercontent.com/oncletom/nodebook/master/chapter-01/index.adoc) ou [interprétée par GitHub](https://github.com/oncletom/nodebook/blob/master/chapter-01/index.adoc) --, et de contribuer au texte&nbsp;: soit en proposant des modifications, soit en ouvrant des *issues* ou tickets pour signaler des corrections&nbsp;;
- génération d'[un site web](https://oncletom.io/node.js) à partir des fichiers AsciiDoc -- sur le modèle des générateurs de site statique -- pour offrir une interface de lecture numérique.

Ce *workflow* change la façon de travailler autour du texte&nbsp;:

- les corrections peuvent être intégrées facilement grâce à un format commun, sans dépendre d'un format non interopérable comme le `.doc` ou de logiciels complexes comme les traitements de texte&nbsp;;
- les versions du texte sont bien mieux gérées via l'outil de versionnement qu'est git, et via la plateforme GitHub qui facilite les interventions -- notamment pour celles et ceux qui ne sont pas habitués à git.

## Quelques bouleversements
À partir de cette chaîne d'écriture, voici quelques bouleversements constatés&nbsp;:

- la structure et la mise en forme sont plus clairement distinguées avec un langage sémantique, là où un traitement de texte à tendance à les brouiller&nbsp;;
- le travail avec l'éditeur peut commencer dès le début de l'écriture, et non pas lors de la finalisation d'un chapitre ou du livre complet&nbsp;;
- les corrections sont plus faciles à proposer, à valider, à intégrer et à suivre&nbsp;;
- pour l'instant Thomas note qu'il y a beaucoup de temps de gagné sur le long terme -- puisqu'en réalité la gestion des contributions en demande beaucoup, de temps --, et moins de souffrance dans la pratique d'écriture.

Voici quelques bouleversements *potentiels* et quelques questionnements induits par ce fonctionnement&nbsp;:

- puisque des contributeurs peuvent intervenir sur le texte, notamment pour le corriger, le rôle de correcteur de l'éditeur est en partie remis en cause -- j'ai bien dit *en partie*&nbsp;;
- idem en terme de structuration du texte, et peut-être de gestion éditoriale&nbsp;;
- la gestion des contributions n'est pas forcément simple, notamment si une personne propose *beaucoup* de modifications, dans ce cas il vaut mieux demander au contributeur de faire plusieurs propositions -- il faut aller regarder [les issues du livre de Thomas](https://github.com/oncletom/nodebook/issues) pour mieux comprendre ce point&nbsp;;
- comment doivent être considérés les contributeurs&nbsp;? Est-ce qu'ils ont un statut de co-auteur&nbsp;?
- est-ce que l'éditeur ne devrait pas s'inspirer de cette chaîne d'*écriture* pour modifier sa chaîne de *publication*, pour fluidifier les échanges avec les auteurs mais également pour simplifier la production des livres&nbsp;? (Ce dernier point est un *teaser* puisque j'en parlerai longuement très bientôt)&nbsp;;
- etc.


## Au-delà de la technique
Si le point de départ de la rencontre était essentiellement axé autour des outils et des modes d'organisation dans un exercice d'écriture, les questions soulevées avec la dizaine de participants avaient pour sujet le rôle et la fonction de l'éditeur, les évolutions de la chaîne du livre, et la façon dont une production collaborative peut fonctionner.

Thomas est par ailleurs en *résidence itinérante*, et cette expérience physique rentre en résonance avec sa démarche d'écriture&nbsp;: ouvrir les possibilité d'échange, interroger les processus, multiplier les questions autour des modes de collaboration, et, surtout, rencontrer des gens.

Enfin, Thomas fait [du pain au levain](https://twitter.com/oncletom/status/833340662686896129), et je trouve que cette pratique n'est pas anodine, c'est peut-être même une fabuleuse analogie pour comprendre cette expérimentation&nbsp;: le levain est une matière qui, si elle est nourrie, ne se tarit jamais, et peut être facilement partagée. Les outils et les modes d'organisation mis en place par Thomas pour écrire son livre lui permettent une multiplicité d'échanges autour d'un texte, inépuisables.