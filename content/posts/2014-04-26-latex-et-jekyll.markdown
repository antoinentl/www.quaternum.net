---
layout: post
title: "LaTeX et Jekyll : deux workflows de publication"
date: "2014-04-26T13:07:00"
comments: true
categories:
- carnet
slug: latex-et-jekyll
---
LaTeX et Jekyll sont deux *workflows* de publication, l'un dédié à l'impression, l'autre au web. Quels sont les points communs entre ces deux processus de mise en forme et de publication&nbsp;?

<!-- more -->

## LaTeX

Créé par Leslie Lamport en 1983 à partir de TeX de Donald Knuth — lui-même créé en 1977 — LaTeX est un langage et un workflow pensé pour l'impression de documents — article, thèse, livre — et donc principalement pour générer du PDF. Sans rentrer dans les détails, LaTeX est un *système* qui repose sur les éléments suivants&nbsp;:

+ un langage de balisage relativement simple permettant de gérer la structuration, la mise en forme et les aspects *classiques* d'un document&nbsp;: niveaux de titres, liste, notes de bas de page, pagination, italique, tableau, légendes, etc&nbsp;;
+ la rédaction et l'édition de fichiers .tex se font dans un éditeur de texte, l'idéal étant d'avoir une coloration syntaxique pour faire apparaître le balisage de LaTeX. La syntaxe est compréhensible et plus légère que du HTML, en revanche la lecture d'un document n'est pas non plus *confortable*&nbsp;;
+ la génération du format PDF se fait depuis un terminal, sur le principe il n'y a donc aucune interface graphique si ce n'est celle de l'éditeur de texte. Des interfaces graphiques basées sur LaTeX ont pourtant fait leur apparition&nbsp;;
+ dans la mesure où les fichiers .tex et associés — notamment pour gérer les bibliographies ou les feuilles de style — sont en fait des fichiers texte, LaTeX est interopérable&nbsp;: vous pouvez ouvrir un fichier .tex sur n'importe quel dispositif disposant d'un éditeur de texte sans risque de compromettre le fichier. Le plus complexe est de disposer de l'infrastructure permettant de générer les formats de sortie. Cette infrastructure peut comprendre, en plus d’une collection de programmes pour le fonctionnement général, des extensions parfois nécessaires pour la gestion de bibliographies ou de mises en forme particulières. On désigne cet ensemble sous le nom de distribution LaTeX, tant il s’agit d’un mini-système d’exploitation typographique en soi — à la fois comme une gestion générale de la mise en forme mais aussi comme la gestion des caractères typographiques avec Metafont.

Si LaTeX est un système relativement stable et puissant, offrant la possibilité d'une structuration et d'une mise en forme très maîtrisée, son utilisation demande un apprentissage, certaines fonctionnalités sont mêmes difficiles à manipuler.  

## Jekyll

On retrouve la même logique pour [Jekyll](http://jekyllrb.com/ "Lien vers jekyllrb.com") — un workflow de publication reposant sur un langage de composition — si ce n'est que les formats de rédaction des documents peuvent être&nbsp;: Markdown, Textile ou Liquid.  
Le fonctionnement de Jekyll en quelques points&nbsp;:

+ le langage de publication le plus utilisé avec Jekyll est le Markdown (voir [ce billet](/2012/12/04/markdown-pour-simplifier-et-maitriser/) pour en savoir plus sur Markdown). Markdown permet la structuration d'un document — niveaux de titre, liste, notes, gras, italique — à partir d'une syntaxe très simple et très lisible&nbsp;;
+ Jekyll est un générateur de site statique, un *CMS* sans base de données, qui repose sur les fonctionnalités suivantes&nbsp;: transformation de chaque fichier Markdown en page ou en article au format HTML&nbsp;; mise en forme avec des feuilles de style CSS&nbsp;; organisation du contenu, principalement sous la forme d'un blog — date de publication, auteur, catégorie&nbsp;;
+ la création et l'édition des fichiers Markdown passent par un éditeur de texte, pour le reste Jekyll fonctionne à partir d'un terminal et quelques commandes assey simples et compréhensibles&nbsp;;
+ des *frameworks* pour Jekyll existe, [Octopress](http://octopress.org/) est surement le plus stable et le plus simple à mettre en place, avec [de nombreux thèmes existants](https://github.com/imathis/octopress/wiki/3rd-Party-Octopress-Themes)&nbsp;;


## Deux workflows très proches

Jekyll est-il le *petit frère* de LaTeX&nbsp;? Difficile d'imaginer que Tom Preston-Werner n'ai pas inventé Jekyll avec en arrière pensée LaTeX. Et finalement c'est peut-être aussi le cas pour Markdown et ses créateurs John Gruber et Aaron Swartz. On le constate depuis quelques années, Markdown est de plus en plus utilisé, et nombreuses sont les interfaces et les applications qui proposent ce langage par défaut — il était temps depuis sa création en 2004.  
Il est essentiel de bien comprendre les limites de ces deux *workflows*&nbsp;: ils ont été créés dans des buts bien précis et donc ils correspondent d'abord à ces usages. Markdown+Jekyll est clairement un workflow pour faire de l'HTML et du web, et non pour éditer des documents complexes avec des mises en forme figées — non liquides. Mais on pourrait tout à fait imaginer une adaptation de Jekyll pour la production de fichiers EPUB ou de PDF destinés à l'impression, avec les limites de mise en forme de Markdown.  

Si LaTeX s'apprend, Markdown se pratique, et Jekyll — une fois l'étape parfois délicate du paramétrage — s'utilise tout simplement.