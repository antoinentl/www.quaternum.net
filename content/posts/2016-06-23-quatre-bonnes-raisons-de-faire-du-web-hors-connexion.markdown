---
layout: post
title: "Quatre bonnes raisons de faire du web hors connexion"
date: "2016-06-23T11:00:00"
comments: true
published: true
description: "Les avantages du web hors connexion : des performances améliorées, une consommation réduite, une meilleure accessibilité et un respect des données privées."
image: "https://www.quaternum.net/images/2016-online-offline-mini.png"
categories: 
- carnet
---
Le web hors connexion&nbsp;? C'est possible avec Application Cache depuis un certain temps, et désormais avec les Service Workers. Au-delà de la simple consultation hors ligne, le web *offline* c'est aussi&nbsp;: des performances améliorées, une consommation réduite, une meilleure accessibilité et un respect des données privées.

<!-- more -->

![Emoji représentant un interrupteur ouvert et un interrupteur fermé](/images/2016-online-offline.png)

## Le web hors ligne et les Service Workers
Depuis HTML5, le web peut être utilisé hors connexion avec différentes technologies&nbsp;: une fois une page *chargée* dans un navigateur, elle peut être consultée à nouveau sans connexion. Le web hors ligne, c'est à la fois&nbsp;: offrir la possibilité aux utilisateurs de consulter ou d'utiliser des sites/pages/applications sans connexion&nbsp;; et faciliter ces consultations ou usages du fait que, même si l'utilisateur est toujours connecté à Internet, certains fichiers n'ont plus besoin d'être chargés depuis le serveur web, puisqu'ils sont stockés localement.

![Emoji signifiant une page ou un document et l'action de stocker](/images/2016-service-workers.png)

C'était possible avec Application Cache, avec [un certain nombre de problèmes](http://alistapart.com/article/application-cache-is-a-douchebag), heureusement [les Service Workers sont arrivés](https://www.w3.org/TR/service-workers/) il y a un an, offrant plus de facilité d'utilisation, et plus d'options. Voici une définition des Service Workers&nbsp;:

>Les service workers jouent essentiellement le rôle de serveurs proxy placés entre une application web et le navigateur ou le réseau (lorsque disponible). Ils sont destinés (entre autres choses) à permettre la création d'expériences de navigation déconnectée efficaces, en interceptant les requêtes réseau et en effectuant des actions appropriées selon que le réseau est disponible et que des ressources mises à jour sont à disposition sur le serveur. Ils permettront aussi d'accéder aux APIs de notifications du serveur (push) et de synchronisation en arrière-plan.  
[Service Worker API, MDN](https://developer.mozilla.org/fr/docs/Web/API/Service_Worker_API)

L'objet de cet article n'est pas la façon dont fonctionnent ou on utilise un Service Worker, il y a [une littérature abondante sur le sujet](https://serviceworke.rs/) -- dont [le très bon billet de Makina Corpus](http://makina-corpus.com/blog/metier/2016/decouvrir-le-service-worker).

**Le web hors ligne présente d'autres avantages que le simple fait de ne plus avoir besoin d'une connexion pour consulter des contenus ou pour utiliser une application !**


## De meilleures performances
Si une ou plusieurs pages web d'un même site ou d'une même application peuvent être stockées localement, cela veut dire que les chargements de certains contenus ne nécessiteront plus de connexion, et cela peut avoir un impact important sur les performances.

![Emoji signifiant l'échange de données et une évolution positive](/images/2016-performance.png)

Et au-delà des contenus -- des pages HTML par exemple --, c'est également les fichiers CSS, JS, et les fontes qui peuvent être stockées dès la visite d'une page du site web utilisant les Service Workers.

[Jake Archibald](https://developers.google.com/web/updates/2015/07/measuring-performance-in-a-service-worker) ou [Patrick Meenan](http://fr.slideshare.net/patrickmeenan/service-workers-for-performance) en parlent en détails.


## Une consommation réduite
Si certains contenus ou fichiers sont stockés localement -- sur le dispositif de l'utilisateur&nbsp;: un smartphone, une tablette ou un ordinateur --, alors&nbsp;:

- le dispositif consommera potentiellement moins d'énergie&nbsp;: les antennes 3/4G ou Wifi et les ressources associées seront moins sollicitées&nbsp;;
- le serveur qui héberge le site web sera lui aussi moins sollicité et consommera lui aussi moins d'énergie.

![Emoji représentant l'action de réutiliser et la terre](/images/2016-consommation.png)

Ansel Hannemann [a constaté](https://twitter.com/helloanselm/status/740438960057487360), en observant les statistiques d'utilisation de son site <a href="https://wdrl.info/"><abbr title="Web Development Reading List">WDRL</abbr></a>, 30% de transmission de données en moins grâce aux Service Workers. Ce constat mériterait d'être appuyé par des données plus précises, mais le web hors ligne présente donc des avantages en terme de consommation et d'impacts écologiques -- et cela même si l'utilisateur est toujours connecté, c'est important de le préciser.


## Rendre les sites et applications plus accessibles

![Emoji représentant des ciseaux prêts à couper et une prise électrique](/images/2016-cut-electric.png)

J'aborde la question de l'accessibilité au sens très large&nbsp;: si vous avez une coupure de réseau, vous pourrez avoir accès à certains contenus. [Jeremy Keith]() a par exemple rendu un site web complet disponible hors ligne -- à condition de charger au moins une page en étant connecté&nbsp;: [HTML5 For Web Designers](https://html5forwebdesigners.com/). Il y a quelques semaines j'ai consulté la page d'accueil de ce livre web écrit par Jeremy Keith, depuis je continue de lire certains passages sans aucune connexion internet.


## Respect des données privées
Si je consulte plusieurs fois la même page et qu'elle est disponible hors ligne, mon navigateur fera moins de requêtes vers le serveur qui l'héberge. En d'autres termes, favoriser la consultation hors ligne permet de réduire les risques d'interception de données puisque tout ou partie se passe en local.

![Emoji représentant un cadenas fermé et une personne neutre](/images/2016-closed-someone.png)

Cela pose un problème pour suivre les usages d'un site web&nbsp;? Rien ne vous empêche d'utiliser un *tracker* pour suivre les usages connectés. En revanche pour les usages hors ligne, cela semble encore complexe.


## Bonnes pratiques et réflexions
L'utilisation des Service Workers est disponible à travers beaucoup de billets et tutoriels, mais il y a quelques points d'attention très importants&nbsp;:

- le script utilisant les fonctions des Service Workers définit ce qui doit être stocké localement, beaucoup de données peuvent être potentiellement *chargées*, sans que l'utilisateur ne s'en rende compte -- visuellement le navigateur n'indique que le chargement de la page, pas ce qui est mis en cache/local. Si cela dépasse plusieurs Mo, la consommation de données via une connexion 3G peut être une mauvaise surprise&nbsp;;
- les Service Workers ne fonctionnent qu'avec HTTPs, il faut donc bien veiller à ce que toutes les ressources du site web concerné soient disponibles en HTTPs -- comme dans toute implémentation de HTTPs&nbsp;;
- pour le moment les Service Workers [ne sont pas compatibles avec tous les navigateurs](http://caniuse.com/#search=service%20workers), mais le mouvement semble lancé. Et il y a toujours la possibilité d'utiliser en même temps AppCache et les Service Workers&nbsp;: les navigateurs qui supportent les Service Workers privilégient ces derniers&nbsp;;
- comment gérer les sites dynamiques utilisant des bases de données&nbsp;? [Les sites statiques peuvent être une solution](/2016/01/09/generateur-de-site-statique-un-modele-alternatif), ou l'utilisation de fichiers JSON plutôt que des bases SQL, même si cela ne sera pas toujours possible.


## Quelques ressources

### Offline First
Le manifeste d'[Offline First](http://offlinefirst.org/) est assez clair&nbsp;:

>We live in a disconnected & battery powered world, but our technology and best practices are a leftover from the always connected & steadily powered past.

La lettre d'information d'Offline First est une mine d'informations, pour vous abonner c'est [par ici](http://offlinefirst.org/news/).


### Progressive Web Applications
Les *Progressive Web Applications* sont des applications web qui devraient fournir une expérience utilisateur proche voir meilleure que les applications *natives* (Android ou iOS par exemple), basées sur les standards du web ouvert. La mise en place de la consultation hors ligne est un point important des *Progressive Web Apps*&nbsp;: une première étape essentielle.

Frank et Enguerran on traduit l’article de Max Lynch sur les Progressive Web Applications, publié initialement sur le site de Ionic, et c'est [une très bonne introduction](http://frank.taillandier.me/2016/06/28/que-sont-les-progressive-web-apps/) !

Alex Russell [a listé les points essentiels d'une *Progressive Web Application*](https://infrequently.org/2015/06/progressive-apps-escaping-tabs-without-losing-our-soul/)&nbsp;:

>- Responsive: to fit any form factor
- Connectivity independent: Progressively-enhanced with Service Workers to let them work offline
- App-like-interactions: Adopt a Shell + Content application model to create appy navigations & interactions
- Fresh: Transparently always up-to-date thanks to the Service Worker update process
- Safe: Served via TLS (a Service Worker requirement) to prevent snooping
- Discoverable: Are identifiable as “applications” thanks to W3C Manifests and Service Worker registration scope allowing search engines to find them
- Re-engageable: Can access the re-engagement UIs of the OS; e.g. Push Notifications
- Installable: to the home screen through browser-provided prompts, allowing users to “keep” apps they find most useful without the hassle of an app store
- Linkable: meaning they’re zero-friction, zero-install, and easy to share. The social power of URLs matters.

*Les emoji utilisés dans cet article sont générés grâce à ["Roger That" créé par iA](https://ia.net/know-how/roger-that-emoji-overdrive/).*