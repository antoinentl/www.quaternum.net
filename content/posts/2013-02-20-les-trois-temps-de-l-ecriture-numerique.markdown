---
comments: true
date: 2013-02-20
layout: post
slug: les-trois-temps-de-l-ecriture-numerique
title: Les trois temps de l'écriture (numérique)
categories:
- carnet
slug: les-trois-temps-de-l-ecriture-numerique
---
>Les biens d’un moine sont réduits en principe au strict minimum, de manière à l’affranchir de tout désir de posséder, ce despote absolu du cœur de l’homme, et à préserver intacte la liberté de pérégrination, celle "des nuages qui passent et de l’eau qui coule".  
[Giei Sato, Journal d'un apprenti moine zen](http://www.editions-picquier.fr/catalogue/fiche.donut?id=677)

Quelques notes en dehors de toute considération de contenus, plutôt du côté tuyaux.


## L'écriture

Comment écrire "en numérique", quels langages, quelles interfaces et quels outils utiliser ? Je pense qu'on ne peut plus parler de [traitement de texte](http://www.tierslivre.net/spip/spip.php?article2917 "Lien vers le tiers livre").  
[La solution offerte par le Markdown](https://www.quaternum.net/2012/12/04/markdown-pour-simplifier-et-maitriser/ "Lien vers quaternum") me semble évidente&nbsp;: écrire dans un format simple, interopérable, pérenne et facilement exportable (en HTML, EPUB, .odt...). Et permettant de s'extraire des bases de données.  
Le nombre d'applications est trop important pour les lister, mais elles doivent remplir quelques conditions&nbsp;:

+ possibilité d'écrire en Markdown&nbsp;;
+ options de synchronisation&nbsp;;
+ options d'export (au moins en HTML), en laissant à d'autres systèmes/applications le soin de la génération/publication (il est bon de savoir séparer les fonctions).


## La synchronisation

Parce qu'écrire en numérique c'est écrire sur plusieurs supports, les _devices_ doivent se synchroniser entre eux pour que l'expression "écriture numérique" ait un sens, pour que les textes puissent être augmentés facilement sans se poser la question de quels fichiers. Et je pense que l'on est désormais tous d'accord pour admettre que ce ne sont plus là des considérations d'une minorité d'ultra-connectés.  
Dropbox n'est pas une solution convenable, tout comme l'écriture en "streaming" (par exemple avec des interfaces web comme [Wordpress](https://www.quaternum.net/2012/12/23/pourquoi-quitter-wordpress/ "Lien vers quaternum")).


## La publication

Parce qu'écrire "en numérique" est avant tout rendre disponible les textes. Blog, site, livres numériques, tweets... les possibilités offertes sont surement aussi nombreuses que les applications d'écriture. On peut aussi parler de _génération_ lorsque l'on part de fichiers - et justement en Markdown. Maîtriser ces interfaces est une prérogative incontournable&nbsp;:

+ portabilité des solutions&nbsp;;
+ ouverture des formats et des systèmes&nbsp;;
+ respect des standards (HTML, EPUB...)&nbsp;;
+ et interopérabilité.

Partant  du principe que _publier_ est _rendre disponible_, je laisse volontairement de côté l'aspect diffusion/propulsion.