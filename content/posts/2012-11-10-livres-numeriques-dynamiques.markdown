---
comments: true
date: 2012-11-10
layout: post
slug: livres-numeriques-dynamiques
title: Livres numériques dynamiques ?
categories:
- carnet
---
>Un bon caractère peut convenir pour n'importe quel livre, et le format d'un livre dépend non de son contenu, mais du fait qu'on le lit en le tenant en main (par exemple un roman), ou à une table (des livres d'histoires ou de référence avec des plans ou avec d'autres illustrations de grand format), ou à un bureau, ou sur un lutrin (un missel ou un livre de choeur), ou encore qu'on le met dans sa poche (un livre de prières ou un dictionnaire de voyage).  
[Eric Gill, Un essai sur la typographie, Ypsilon éditeur, page 124](http://www.ypsilonediteur.com/fiche.php?id=104)

Je rêve de livres numériques dont la mise en page s'adapterait réellement au support de lecture : selon la taille de l'écran, son orientation, sa résolution et d'autres paramètres comme la couleur ou le rétroéclairage. Peut-on imaginer des livres numériques dont la typographie serait à ce point dynamique ? Est-ce que ce serait le type d'évolution que j'évoquais [dans ce billet](https://www.quaternum.net/2012/10/25/dynamiques/ "Lien vers quaternum") et que le responsive web design semble effleuré ? Cela semble possible en partie faisable avec les media-queries (voir [l'article de Julien](http://book-to-the-future.tumblr.com/post/34627070850/comment-les-media-queries-vont-revolutionner-lebook "Lien vers Book to the future") à ce sujet).

>Malgré tout, le producteur de livres, s'il est raisonnable, partira du principe que c'est la lecture, et non le contenu de la lecture, qui détermine le format du livre et le style de ses caractères ; les autres considérations interviennent comme facteurs d'adpation. Lorsque l'on conçoit un livre, il faut d'abord se demander : qui va le lire, et dans quelles conditions ?  
[Eric Gill, Un essai sur la typographie, Ypsilon éditeur, page 126](http://www.ypsilonediteur.com/fiche.php?id=104)