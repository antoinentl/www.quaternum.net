---
comments: true
date: 2012-02-18
layout: post
slug: aucune-trace
title: Aucune trace
categories:
- carnet
---
>Il est plus facile de ne laisser aucune trace  
Que de marcher sans toucher le sol  
*Tchouang Tseu Aphorismes et paraboles*

Hier, vendredi 17 février 2012, goût amer dans la bouche, Gallimard demande à publie.net et à ses diffuseurs le retrait de la traduction de François Bon du livre d'Ernest Hemingway, _Le vieil homme et la mer_. [Le billet de François Bon](http://tierslivre.net/spip/spip.php?article2788) évolue au fil de la situation, récit et réactions.

Gallimard en est détenteur des droits, et même si tout cela est bien complexe (comme l'explique très bien [Hubert Guillaud](http://lafeuille.blog.lemonde.fr/2012/02/17/nous-nechapperons-pas-a-reposer-la-question-du-droit/)) François Bon à tort sur le terrain juridique, Gallimard a tort sur tout le reste. En clamant la nécessité de faire respecter le droit, la vieille maison d'édition se met à dos toute une communauté ([le billet d’Éric D.](http://n.survol.fr/n/le-vieil-homme-et-ledition) remet d'ailleurs les choses à plat). Gallimard superflic.

C'est cette question de forme qui me pose problème, tout cela aurait pu être réglé autrement, Gallimard ne semble pas comprendre ce qui se passe actuellement ([Clément Monjou](http://www.ebouquin.fr/2012/02/17/gallimard-vs-publie-net-le-choc-de-deux-visions-de-ledition/) le résume très bien), d'autant plus qu'il y a une question de monopole intolérable, voir [les commentaires de Marc Jahjah](http://www.ebouquin.fr/2012/02/17/gallimard-vs-publie-net-le-choc-de-deux-visions-de-ledition/#comments) dans le billet de Clément Monjou :

>Ce dont est coupable Gallimard n’est donc pas de faire respecter le droit d’auteur (dont personne ne conteste la nécessité); ce dont Gallimard est coupable c’est d’être à la tête d’un catalogue dont la valeur sociale est inestimable et dont ils refusent aujourd’hui d’assurer non seulement la circulation mais la rénovation, c’est-à-dire l’assurance que l’oeuvre circulera bien compte tenu de l’époque dans laquelle elle s’inscrit alors. C’est une faute morale très grave : la maison Gallimard n’est plus à la hauteur des oeuvres dont elle a la charge.  
[commentaire de Marc JahJah,  Gallimard vs. Publie.net : le choc de deux visions de l'édition, eBouquin](http://www.ebouquin.fr/2012/02/17/gallimard-vs-publie-net-le-choc-de-deux-visions-de-ledition/#comments)

C'est pourquoi je mets à disposition ce texte, _Le vieil homme et la mer_ aux éditions publie.net, traduction de François Bon (acheté sur la Fnac.com puis déréméisé, double effort qui vaut bien ce partage) :

[Le vieil homme et la mer, traduction de François Bon, éditions publie.net](https://www.quaternum.net/documents/publienet_Le_vieil_homme_et_la_mer.epub)

Comme le dit très bien [Isabelle Aveline](https://twitter.com/#!/_zazieweb/status/170551255117201408), il faut acheter des livres sur publie.net, et surtout s'abonner, massivement, pour défendre cet espace de création, et pour éviter de tomber dans un univers encore plus fermé. A ce sujet [l'anticipation de Karl Dubost](http://la-grange.net/2012/02/17/droit-rever) fait froid dans le dos...