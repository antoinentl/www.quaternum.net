---
layout: post
title: "La rouille des fabriques"
date: "2020-09-01T22:00:00"
comments: true
published: true
description: "Comment faire pour qu'une page web dure dans le temps ? Cette question qui semble si simple soulève pourtant de nombreuses interrogations."
categories:
- carnet
---
Comment faire pour qu'une page web dure dans le temps ?
Cette question qui semble si simple soulève pourtant de nombreuses interrogations.

<!-- more -->

En réaction au billet de Jeff Huang, [This Page is Designed to Last](https://jeffhuang.com/designed_to_last/), [récemment traduit](https://framablog.org/2020/08/24/pour-une-page-web-qui-dure-10-ans/) par l'équipe de Framalang, et qui parle de comment _faire durer_ une page web, voici quelques réflexions dispersées.
Merci à Arthur d'[avoir partagé son retour d'expérience](https://twitter.com/arthurperret/status/1300095746847449088) sur le sujet.

## Distinguer les contraintes
Je passe rapidement sur l'aspect tout feu, tout flamme du billet de Jeff Huang, qui mélange choix de format et compression des fichiers, qui omet d'aborder la question des noms de domaine (soulevée [dans les commentaires](https://framablog.org/2020/08/24/pour-une-page-web-qui-dure-10-ans/#comment-82379) de la traduction française) ou celle des URIs, et qui parle de _durée_ sans replacer cette notion floue sur une échelle commune (le Web ? l'informatique ? l'écriture ? l'humanité ?).
Même si je semble très négatif, le texte de Jeff Huang fait vraiment du bien à lire, quelques précautions ou précisions sont néanmoins nécessaires.

## Durer = adapter
Un des problèmes de fond, à mon humble avis, est la volonté de Jeff Huang de vouloir faire un choix arrêté, choix qui ne sera pas modifié pendant les dix ou vingt prochaines années.
Envisager la pérennité de l'accès à un contenu doit se faire en acceptant que des décisions prises aujourd'hui seront probablement remises en cause après-demain.

Par ailleurs dans le cas de la mise en ligne de contenu la maladresse est préférable à l'inaction.
À force de chercher la formule idéale pour rendre accessible une page ou un site web pour _longtemps_, il est fort probable qu'aucune décision ne soit prise.
Tout simplement parce qu'il n'y a pas de formule parfaite.

Plutôt que de parler de _page qui dure_, le qualificatif _pérenne_ serait plus juste.
La notion de _pérennité_ ([voir la définition du CNRTL](https://www.cnrtl.fr/definition/p%C3%A9rennit%C3%A9)), contient une dimension d'évolution, en plus de celle d'éternité.
Le temps long n'est possible qu'à condition d'une _succession_ d'états.

## Plein texte et données
Une question est sous-entendue dans [This Page is Designed to Last](https://jeffhuang.com/designed_to_last/), celle de la complexité de la maintenance d'une base de données sur le temps long : Jeff Huang souligne à juste titre que des fichiers en plein texte sont bien plus simples à conserver.
Il part de l'exemple du générateur de site statique [Jekyll](/2020/05/05/fabriques-de-publication-jekyll/) et de ses dépendances, pour finalement décider d'écrire son site web entièrement en HTML.
Je vois trois limites dans ce choix.

La première limite est celle de la lisibilité, écrire des textes en HTML peut devenir rapidement pénible, mais après tout c'est une histoire de goût personnel.
(J'ai tenté la chose il y a plusieurs années, j'ai arrêté au bout de quelques mois.)

La seconde limite est celle du maintien d'une page HTML, qui est en fait une page web.
Un fichier HTML n'est pas forcément une page web.
À partir du moment où le fichier est exposé sur les réseaux il devient une page web, et certaines évolutions seront peut-être nécessaires.
Les métadonnées de la page [This Page is Designed to Last](https://jeffhuang.com/designed_to_last/) sont quasi-inexistantes, peut-être que c'est une volonté de l'auteur, mais peut-être que ce dernier souhaiterait la mettre à jour ainsi que les 9 autres pages de la catégorie "writing" de son site.
Faire cela à la main n'est pas forcément très _fun_, même si 9 pages cela reste faisable (mon site personnel a environ 1000 pages, c'est tout de suite moins rigolo).
Outre les questions de référencement, enrichir une page web de métadonnées peut être utile pour des raisons d'accessibilité ou d'enregistrement – par exemple avec Zotero qui parse les métadonnées d'une page, quand elles existent.

La troisième limite est celle de la distinction entre la page et le site : le site web est un ensemble de pages organisées.
Écrire une page en HTML est envisageable, organiser un site web (de plusieurs dizaines de pages) en HTML est nettement moins facile.
Quand bien même il n'y a qu'une page d'index à mettre à jour (la page qui permet d'accéder à toutes les pages), l'idée de devoir la faire évoluer manuellement me donne des frissons.
Par exemple si sur la page d'index, en plus du titre et de la date de chaque page (mois et année) vous ajoutez le jour de publication, puis peut-être aussi un résumé de quelques lignes (les premiers mots de chaque page), ces modifications vont demander beaucoup de temps.
Sans parler du moment où vous voudrez créer un plan du site (un _sitemap_) pour automatiser le référencement ou des sauvegardes, celles que Jeff Huang suggère justement.

## Baliser et publier
Je partage les craintes de Jeff Huang concernant la fragilité de générateurs de site statique comme Jekyll, mais je pense qu'il y a des solutions intermédiaires avant de vouloir écrire un site entier en HTML.
Par exemple choisir un système plus simple, reposant sur moins de dépendances, que ce soit des mastodons comme [Hugo](https://gohugo.io/) – un système complexe qui a l'avantage de fonctionner avec un simple fichier binaire –, des solutions maison [en quelques lignes de code](https://git.larlet.fr/davidbgk/larlet-fr-david/src/branch/master/site.py), ou le détournement de convertisseurs comme [Pandoc](https://ordecon.com/2020/07/pandoc-as-a-site-generator.html).

Baliser un contenu dans un langage de rédaction et non de diffusion, c'est distinguer l'écriture de la publication.
Cela peut sembler une forme d'exigence superflue, mais c'est pourtant la condition de la longévité : une source pourra toujours être transformée dans d'autres formats, à condition qu'elle reste lisible par un humain _et_ par une machine.
Et c'est exactement la définition des langages de balisage léger comme [Markdown](http://www.reel-virtuel.com/numeros/numero6/sentinelles/markdown-condition-ecriture-numerique) ou [AsciiDoc](/2020/05/03/fabriques-de-publication-asciidoctor/).
Cela ne veut pas dire que rédiger en HTML est une mauvaise idée, mais il faut séparer la page du reste du site web, comme précisé plus haut.

## Les fabriques qui rouillent
Olivier Thereaux et Karl Dubost avaient abordé l'épineuse question du _web qui dure_ d'une bien belle façon [lors de Paris Web en 2013](https://www.paris-web.fr/2013/conferences/esthetique-et-pratique-du-web-qui-rouille.php), à la suite de [réflexions](https://www.24joursdeweb.fr/2012/un-site-web-de-1000-ans/) [communes](https://olivier.thereaux.net/2013/01-Digital-Decay/) (il était alors question de _site web_ de _1000 ans_), en parlant de "web qui rouille".
Je pense qu'il faudrait étendre cette réflexion aux systèmes ou aux [fabriques](https://www.quaternum.net/fabriques), et les considérer comme étant partie prenante du web, et de nos publications.
Ainsi, plutôt que se concentrer sur la longévité d'une page web, il faudrait plutôt se concentrer sur les procédés qui permettent à cette page d'exister, et sur l'évolutivité de ces procédés.
J'utilise Jekyll depuis peut-être six ans, et je suis dépendant d'au moins deux extensions qui doivent être maintenues pour que je puisse continuer de générer mon site.
Je sais que je vais avoir des problèmes dans un an ou deux, peut-être plus tard.
Mais, d'une part, le HTML généré sera lisible pendant longtemps, et d'autre part les sources de mon site web peuvent être facilement converties pour être intégrées dans un autre générateur de site statique, même avec plus de 1000 pages.
J'apprécie ce déséquilibre, cette imperfection, parce qu'elle m'oblige à un moment donné à me reposer la question du fonctionnement et de la longévité, _bientôt_.
