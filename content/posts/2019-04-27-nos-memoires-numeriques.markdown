---
layout: post
title: "Nos mémoires numériques"
date: "2019-04-27T10:00:00"
comments: true
published: true
description: "À l'occasion d'une perte d'une partie de ma mémoire vive numérique stockée dans mon navigateur web, je prends conscience de la masse d'informations générée et de la dimension d'oubli liée à mon usage individuel du web."
categories:
- carnet
---
À l'occasion d'une perte d'une partie de ma mémoire vive numérique stockée dans mon navigateur web je prends conscience de la masse d'informations générée et de la dimension d'oubli nécessaire et liée à mon usage individuel du web.

<!-- more -->

## Lost in tabs
Début janvier 2019 mon navigateur web Firefox plante, c'est-à-dire qu'il a un comportement inattendu et qu'il s'arrête inopinément.
Cela arrive rarement et en soit ce n'est pas gênant, il suffit de le redémarrer et tout repart _comme avant_.
Le problème c'est l'usage que je fais de ce navigateur depuis plusieurs années&nbsp;: je laisse ouvert les onglets utiles à [ma veille](/flux), et j'ai ainsi plus d'une centaine de liens en attente, certains visités la première fois depuis plusieurs années.
Je fais ainsi de mon navigateur ma mémoire vive numérique, ce qui n'est pas sans risque puisque cette fois les onglets en question ne sont pas réouverts.
Ma session précédente n'a pas été restaurée et j'ai perdu près de 200 liens, la majorité mis de côté pour une lecture ultérieure.

Passé le constat – déjà fait depuis que j'ai cette fâcheuse pratique – que je me suis mis dans une situation pour le moins périlleuse et un peu surréaliste – si je n'ai pas lu certaines pages web depuis plus de six mois c'est que je ne les lirai jamais –, il faut que je trouve un moyen de retrouver ces plus de 150 liens.
Je sais qu'il y a tout de même quelques pépites parmi eux, je dois identifier ceux que je voulais intégrer dans ma veille ou qui sont nécessaires à mes différentes activités.

Intérieurement je me dis que l'on ne m'y reprendra plus, que je ne dois pas faire reposer ma mémoire vive – qui était devenue ma mémoire numérique sur le long terme vu le temps de conservation – sur un système qui peut faillir si facilement.
La question n'est pas la résilience d'un logiciel, mais plutôt la mienne et les moyens que je convoque pour cela.
Ce qui ne m'a pas empêché de pester contre Firefox et ce plantage...
Je ne vais pas parler ici de comment faire pour contourner un tel problème, mais de ce que j'ai découvert à ce moment-là – et c'est plutôt vertigineux.

## Plongée dans les données
Pour retrouver tout ou partie de ces fameux onglets _ouverts_, je commence une plongée dans mes données.
Je trouve une façon de récupérer mon historique, il y a pléthore d'extensions our Firefox qui font cela plus ou moins bien, personnellement j'ai utilisé [History Master](https://addons.mozilla.org/fr/firefox/addon/history-master/) qui comporte une fonction d'export (j'ai assez vite écarté l'idée d'explorer mes logs).
Je sélectionne quatre ans, puis deux ans, puis six mois, puis un mois, histoire d'avoir à ma disposition un _set_ exploitable et un minimum lisible&nbsp;: un fichier [CSV](https://fr.wikipedia.org/wiki/Comma-separated_values) de plusieurs milliers de lignes.

Une fois le fichier sous la main, je découvre plus de 3000 pages web différentes visitées en une trentaine de jours, dont certaines plusieurs fois – à la fois via mon ordinateur et mon téléphone portable, partageant l'historique via la fonction [Sync](https://fr.wikipedia.org/wiki/Firefox_Sync) de Firefox.
L'ensemble représente 30 000 visites.
C'est là que débute le vertige, je n'avais pas conscience de visiter autant de pages web différentes, et autant de pages web différentes en si peu de temps&nbsp;: 3 000 pages et 30 000 visites, 100 pages différentes par jour et 1 000 visites par jour.
Ces chiffres concernent plusieurs activités professionnelles différentes, mais aussi des usages plus _personnels_ et parfois divertissants.
Ma consommation du web me paraît considérable.

Je commence un nettoyage de mon historique, je trie ces données en supprimant les liens inutiles&nbsp;: recherches (moteurs de recherche, dictionnaires, encyclopédies), plateformes diverses (allant des revues scientifiques aux réseaux sociaux en passant par des sites de vidéo), sites institutionnels utilisés quotidiennement, etc.
J'utilise quelques expressions régulières pour rendre le fichier CSV utilisable.
L'objectif n'est pas de tout conserver, cela n'aurait pas de sens, mais de retrouver les pages qui ont un réel intérêt pour moi.
Je parviens au bout de quelques heures à ne sélectionner qu'une cinquantaine de pages que je souhaite lire, partager ou intégrer à ma veille.

## La fragilité du numérique
À la suite de ce petit épisode totalement anecdotique, reflet d'un usage démesuré du web d'une personne trop connectée, j'ai tout de même fait quelques constats.
À la fois en raison de cet historique imposant, mais aussi parce que j'ai effectué un nettoyage de mon navigateur.
J'ai pu noter quelques chiffres représentatifs de cette surutilisation du web, chiffres que je pose ici sans les analyser réellement&nbsp;:

- **110 000** et **190 000**&nbsp;: le nombre de pages web _différentes_ visitées et le nombre de visites total en deux ans&nbsp;;
- **3 000** et **30 000**&nbsp;: le nombre de pages web _différentes_ visitées et le nombre de visites total en trente jours&nbsp;;
- 45 300 fichiers et 3,6Go&nbsp;: nombre de fichiers et leur poids en deux ans d'utilisation d'un navigateur web, sans _nettoyage_ de Firefox&nbsp;;
- 305 fichiers et 193Mo&nbsp;: nombre de fichiers et leur poids après nettoyage de Firefox – sans suppression de l'historique.

Au-delà de la masse d'information que je crée seul à mon échelle, je note la difficulté de naviguer dans cette quantité.
Je ne suis pas bien outillé, et je ne sais pas trop si je voudrais l'être.
J'ai par exemple remarqué certaines _pages web_ visitées un nombre très important de fois (plus de 100 000 fois...), il s'agit en fait de _trackers_ – il y en a bien d'autres qui n'apparaissent pas dans l'historique.

Il y a une réelle nécessité d'oublier pour mieux se souvenir, Michel Lagües en a très bien parlé dans son livre _L'invention de la mémoire&nbsp;: écrire, enregistrer, numériser_ et pendant l'émission [Des livres aux ordinateurs, où stocker nos souvenirs ? sur France Culture](https://www.franceculture.fr/emissions/les-chemins-de-la-philosophie/loubli-24-linvention-de-la-memoire) il y a quelques semaines.

Je ne suis pas retourné voir cet historique trié, preuve que je pourrais probablement fermer mon navigateur après chaque journée d'utilisation de mon ordinateur et oublier mes navigations, et ainsi réfléchir un peu mieux à mon usage du web.
