---
comments: true
date: 2012-08-15
layout: post
slug: relire-jan-tschichold
title: Relire Jan Tschichold
categories:
- carnet
---
>La conception élémentaire en typographie n'est jamais absolue ou définitive, étant donné que le concept de composition élémentaire se modifie de façon nécessaire, et en permanence, suivant la mutation des éléments.  
[Jan Tschichold dans Robin Kinross, La typographie moderne, éditions B42, page 105](http://www.editions-b42.com/books/La-Typographie-moderne/)

Cette phrase de Jan Tschichold est le dixième point de son énoncé de la nouvelle typographie, publié initialement dans _Typographische Mitteilungen_, n°10, 1925, p. 198, 200 - et repris dans son intégralité dans l'ouvrage de Robin Kinross. Ce dixième et dernier point a une portée historique que l'on ne peut ignorer, que ce soit dans son contexte d'origine - à une époque où beaucoup de questions se posaient sur les formats de papier et les unités de mesure typographique et d'impression - ou dans une période plus contemporaine et numérique - je pense surtout au [responsive web design](https://www.quaternum.net/2012/07/09/responsive-web-design-interoperabilite-et-details-typographiques/ "Lien vers quaternum"). Ces quelques lignes de Jan Tschichold pourraient être reprises comme la devise des designers/graphistes/typographes - numériques - d'aujourd'hui. Non plus dans le processus de conception - mise en page avant impression - mais comme contrainte de départ&nbsp;: tout est susceptible de bouger en permanence, pour le Web comme pour le livre numérique - ou plus globalement pour toutes les interfaces de lecture.  

Et je ne pense pas aux designers/graphistes/typographes qui travaillent avec des outils numériques pour des productions papier, il ne s'agit pas ici uniquement de dématérialisation mais de création numérique&nbsp;: mise en page flexible, possibilité de restructuration du contenu - principalement textuel.

Les neuf premiers points sont aussi intéressants mais difficilement "transposables".