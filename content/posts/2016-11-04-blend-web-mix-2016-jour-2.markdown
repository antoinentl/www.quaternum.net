---
layout: post
title: "Blend Web Mix 2016 : jour 2"
date: "2016-11-04T08:00:00"
comments: true
published: true
description: "Quelques notes sur la deuxième journée de conférences de Blend Web Mix 2016 : performance web, fin des sites web, site web pour liseuse."
categories: 
- carnet
---
Quelques notes sur la deuxième journée de conférences de [Blend Web Mix 2016](http://www.blendwebmix.com/)&nbsp;: performance web, fin des sites web, site web pour liseuse.

<!-- more -->

Les notes de la première journée sont à retrouver [par ici](/2016/11/03/blend-web-mix-2016-jour-1/)

## Design de la performance web. Damien Senger
La performance est souvent une question traîtée en fin de projet, c'est une erreur et c'est ce qu'a expliqué [Damien Senger](https://blog.hiwelo.co/) pendant cette conférence.

Deux idées m'ont particulièrement marqué&nbsp;:

- des artifices peuvent être créés pour donner une autre expérience de l'attente à l'utilisateur, par exemple en lui donnant l'impression d'être actif&nbsp;;
- un concept qui est revenu dans plusieurs autres conférences&nbsp;: le *progressive enhancement*, ou amélioration progressive. Par exemple prévoir du [lazy loading](https://en.wikipedia.org/wiki/Lazy_loading) pour les styles typographiques ou les images. Pour les images également, l'affichage progressif utilisé par [Medium](https://medium.com/) -- d'abord très floues puis nettes -- est aussi un artifice.

Damien Senger a conclut sa conférence en souligant que "ce qui est important ce n'est pas forcément la vitesse mais le ressenti", il faut donc sans cesse tester&nbsp;!

La conférence de Damien Senger est à retrouver en ligne : [https://www.youtube.com/watch?v=nZVKbTGsh_U](https://www.youtube.com/watch?v=nZVKbTGsh_U).


## Et si les sites web étaient sur le point de disparaître&nbsp;? Virginie Clève
[Virginie Clève](http://www.cafe-numerique.com/) a pris le temps d'explorer toutes les possibilités offertes aujourd'hui -- et demain -- pour distribuer du contenu et être plus visibles sur le web. De <abbr title="Accelerated Mobile Pages">AMP</abbr> à Linkedin Pulse en passant par Facebook Instant Articles, Snapchat Discover, Medium ou Google Podium, ce panorama des silos et autres jardins fermés est aussi nécessaire qu'angoissant. Nécessaire parce que ces outils et plateformes de distribution sont une réalité et présentent des avantages. Angoissant car les inconvénients sont nombreux -- surtout à moyen et long terme --, et l'avenir peu réjouissant sous cet angle.

Pour moi cette conférence soulevait une question déterminante&nbsp;: fabriquer des pages web en utilisant le *protocole* de Google, ou publier sur une plateforme fermée comme Facebook Instant Articles, tient-il de l'opportunité ou du piège&nbsp;? Virginie Clève a insisté sur la stratégie à adopter&nbsp;: elle doit être multiple -- c'est-à-dire qu'il faut être présent sur le plus de plateformes possibles -- au risque d'être suicidaire. Et pourtant, selon moi, accepter le fonctionnement de ces silos est peut-être, déjà, <strike>vendre un peu son âme</strike> accepter un web centralisé, uniforme, ennuyeux -- mais monétisable.

Ce qui apparaissait toutefois en filigrane de cette conférence&nbsp;: le web est l'outil qui permet d'envisager de ne pas dépendre d'une seule plateforme. Mais le web, à mon sens, ne doit pas se réduire à cela.

Une dernière chose&nbsp;: certaines technologies permettent désormais d'être au même niveau de performance que des plateformes qui vantent ce point. les <abbr title="Progressive Web Applications">PWA</abbr> sont à regarder de près comme alternative à, notamment, <abbr title="Accelerated Mobile Pages">AMP</abbr>.

Le support de présentation de la conférence de Virginie Clève est [disponible en ligne](http://fr.slideshare.net/ovronaz/le-site-internet-estil-bientt-mort), ainsi que [l'enregistrement vidéo](https://www.youtube.com/watch?v=YPwlA3iFw9I).


## Un site web pour liseuse. Agnès Haasser
Sous-titrée "Voyage au pays des brouteurs bizarres", cette conférence était particulièrement réjouissante, même si le sujet choisi par [Agnès Haasser](http://tut-tuuut.github.io/) l'était beaucoup moins&nbsp;: quelles contraintes et limites pour le développement de sites web sur liseuse&nbsp;? Réponse&nbsp;: elles sont nombreuses, et c'est un vrai défi que de réussir à en produire un.

Une liseuse est un dispositif de lecture numérique à encre électronique, la technologie de l'encre électronique consiste en de petites billes qui changent de contrastes -- pas de couleur mais que du niveau de gris -- en fonction d'un champ magnétique. Très confortable pour la lecture -- c'est un support fait pour lire des livres numériques au format EPUB -- et peu gourmand en énergie -- celle-ci est nécessaire que pour changer d'état --, cet écran présente beaucoup d'avantages par rapport aux LCD classiques. Mais il y a de nombreuses contraintes à prendre en compte&nbsp;:

- temps de réponse très long&nbsp;: pas de *scroll* possible, donc il faut tout paginer&nbsp;;
- rafraîchissements réguliers&nbsp;: pour se re-caler l'écran repasse parfois au noir, ce qui est un peu perturbant, et ce qui rend le scroll encore plus pénible&nbsp;;
- nuances de gris très limitées et parfois hasardeuses&nbsp;;
- images fantôme&nbsp;: les billes conservent parfois l'image précédente.

"Développeuse de l'extrême", Agnès Haasser a présenté un contexte à fortes contraintes, qui permet de concevoir autrement un service ou produit. Cette conférence était réjouissante car, au-delà de l'humour d'Agnès Haasser, on comprend que cela est possible de développer un site web dans les pires conditions qui soient. Cela permet d'envisager la conception web dans d'autres situations complexes&nbsp;: compatibilité avec les navigateurs, accessibilité, mauvaise connectivité, etc.

La vidéo de la conférence d'Agnès Haasser est disponible en ligne : [https://www.youtube.com/watch?v=eAGJ2_Lf3mA](https://www.youtube.com/watch?v=eAGJ2_Lf3mA).