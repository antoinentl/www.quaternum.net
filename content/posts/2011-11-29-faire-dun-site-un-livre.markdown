---
comments: true
date: 2011-11-29
layout: post
slug: faire-dun-site-un-livre
title: Faire d'un site un livre
categories:
- carnet
---

[François Bon](http://www.tierslivre.net/spip/spip.php?article2674) (pour les éditions [Publie.net](http://www.publie.net/)) et François Gayrard (pour les éditions [NumerikLivres](http://comprendrelelivrenumerique.com/)) ont pour projet de faire du site [La Grange](http://www.la-grange.net/) de Karl Dubost un livre numérique. Le résultat sera visible dans moins de 2 jours, jeudi 1er décembre 2011 à 00h00.

Ce site est épais : plus de 4 000 pages et plus de 10 ans d'écriture, à travers des sujets oscillant entre les lectures de Karl Dubost, les techniques et fonctionnement du web, des notes diverses sur sa vie quotidienne... C'est "un espace personnel d'écriture, de réflexions, de collections de moments vécus". La structure de chaque article est régulière&nbsp;: une image et une citation viennent (presque à chaque fois) accompagner les textes de [Karl Dubost](http://www.la-grange.net/karl/). Le site est fermé aux commentaires, il change d'habillage environ une fois par an, et est non-indexable par les moteurs de recherche : "J'ai augmenté l'opacité afin de rendre plus naturel le lien humain", nous dit Karl.

Cette initiative d'édition d'un site web, louable puisqu'elle favorisera sûrement la visibilité de l'écriture, des propos et de la prose de Karl Dubost, pose néanmoins un certain nombre de questions, déterminantes et passionnantes, et auxquelles il n'y a peut-être pas vraiment de réponse :

+   Que va-t-il se passer à partir du 1er décembre à 00h01, au moment où Karl mettra en ligne un nouveau billet ? Y aura-t-il des versions successives du livre numérique de la-grange.net ? Le livre serait-il mis à jour au fur et à mesure ? Dans ce dernier cas quelle différence y aura-t-il alors avec le site actuel de Karl (déjà très lisible par ailleurs) ?...
+   Quelle valeur ajoutée vont apporter ces deux éditions simultanées ? Quelle va être la "plus value-éditoriale" ? On devine quelques réponses : un ou plusieurs index pour accéder aux billets, une organisation différente (nouvelle linéarité qui ne serait pas chronologique), des accès thématiques... Sans parler de la forme, du template, de la typographie.
+   Justement par rapport à la forme, les "Carnets Web de La Grange" en changent régulièrement (est-ce la raison du pluriel de "Carnets Web" ?). Les articles/billets de Karl Dubost conservent le contexte graphique et structurel de leur publication initiale (contrairement à la majorité des sites/blogs qui, lorsqu'il y a changement de forme, appliquent rétrospectivement la nouvelle structure et le nouveau graphisme). Le livre numérique qui sera fait de ce carnet Web ne va-t-il pas à l'encontre de cette intention et de cette démarche en lissant les templates ?
+   Karl Dubost se définit parfois comme un "open web evangelist", est-il un fervent défenseur du web en tant que ressource propre, ressource qui se suffit à elle-même ?
+   Le site web de Karl, et plus généralement ses propos et récits, ont-ils besoin de la légitimité d'une version éditorialisée (façon "tampon" de l'éditeur, voir [le récent billet de Marie D. Martel](http://bibliomancienne.wordpress.com/2011/11/19/la-verite-a-propos-des-auteurs-et-des-editeurs-un-long-preambule-pour-le-salon-du-peuple/)) ? Ou bien est-ce moi qui réfléchit à l'envers (je ne pense pas qu'il y ait la moindre recherche de légitimité), et il s'agit bien d'autres choses, une autre forme ?

Cette liste de questions pourrait en comporter bien d'autres. Peut-être que ce questionnement est vain et hors sujet, et peut-être que ce qui m'apparaît comme des contradictions (le mot est un peu fort) n'en sont pas.

Cette expérience est en tout cas passionnante à suivre, surtout à cette période, où, lorsque l'on parle de livre numérique, on en vient très vite à évoquer le Web.