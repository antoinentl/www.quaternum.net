---
comments: true
date: 2012-12-04
layout: post
slug: markdown-pour-simplifier-et-maitriser
title: 'Markdown : pour simplifier et maîtriser'
categories:
- carnet
---
J'avoue qu'au début je comptais faire un billet sur les bienfaits d'écrire en HTML, et puis la lecture d'un énième article sur les qualités de Markdown ([celui de David Bosman](http://davidbosman.fr/blog/2012/11/23/pourquoi-en-markdown/ "Lien vers le carnet de David Bosman")) a fini de me convaincre que c'était une erreur, je dois bien l'avouer. Et pourtant ce n'était pas faute d'avoir lu de nombreux billets de [Karl Dubost](http://www.la-grange.net/ "Lien vers La Grange") à ce sujet.

## Pourquoi Markdown ?

Je n'ai jamais pu supporter les "wysiwyg", certains sont meilleurs que d'autres mais aucun n'est parfait ou même satisfaisant. Écrire sur le Web implique forcement de prendre un minimum en compte le HTML, au moins ses balises les plus simples, au moins pour comprendre comment un document HTML est structuré - je ne dis pas non plus qu'il faut forcement écrire directement en HTML. Même le wysiwyg de Wordpress fait parfois mal les choses&nbsp;:

+   insertion de codes supplémentaires et inutiles&nbsp;;
+   utilisation de balises invisibles même dans la version "code"&nbsp;;
+   génération ou interprétation de balises au moment de l'affichage.

Depuis plusieurs mois je rédige mes billets dans Sublime Text 2 en HTML, et je fais un copier-coller dans l'onglet de code de Wordpress, je n'ai pas encore trouvé d'outil pour exporter les contenus des billets autrement, et c'est pourquoi [j'avais envisagé de passer à PluXml](https://www.quaternum.net/2012/05/17/pluxml-un-moteur-presque-en-dur/ "Lien vers quaternum"). De cette façon je sais exactement ce que j'écris, à la fois sur le fond (le contenu) et sur la forme (le code HTML).  
Markdown permet d'écrire dans n'importe quel éditeur de texte, par exemple avec une extension .txt ou .md.

## Quelques outils à utiliser avec Markdown

Avant tout il faut jeter un oeil à la syntaxe Markdown, [Michel Fortin explique tout cela très bien](http://michelf.ca/projets/php-markdown/syntaxe/), et en français.  
Pour écrire les billets j'utilise donc [Sublime Text 2](http://www.sublimetext.com/).  
Un plugin permet d'interpréter le Markdown directement depuis l'éditeur de l'interface web&nbsp;: [Markdown on Save Improved](http://wordpress.org/extend/plugins/markdown-on-save-improved/).


## Quelques questions en suspens

Concernant Markdown&nbsp;:

+   comment faire pour indiquer l'ouverture de liens dans une nouvelle fenêtre&nbsp;?
+   quel outil de "rendu" utiliser en parallèle de Sublime Text 2 pour voir sur deux écrans le Markdown et le résultat, et le tout sous Ubuntu&nbsp;?
+   quel outil utiliser sous Android pour écrire&nbsp;?

Plusieurs choses me tracassent sur l'écriture numérique, Markdown vient renforcer ces interrogations (tout en y répondant un peu) :

+   comment "envoyer" des fichiers Markdown directement vers Wordpress sans passer par l'interface web et faire du copier/coller&nbsp;?
+   comment récupérer tous les billets en Markdown qui seront désormais créés sous cette forme&nbsp;?


## Donc

Avec Markdown c'est tout comme écrire en HTML, sans la contrainte d'un langage certes simple mais lourd. Markdown me permet de maîtriser l'écriture web. Une demie-heure de rédaction de ce billet m'aura suffit à m'adapter à cette syntaxe.