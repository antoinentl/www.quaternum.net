---
layout: post
title: "La création d'un livre web : Le livre 010101"
date: 2015-10-26
published: true
comments: true
categories: 
- carnet
slug: creation-d-un-livre-web-le-livre-010101
---
Après avoir découvert le livre web [Professionnal Web Typography](/2015/05/12/un-design-de-livre-web/) de Donny Truong, voici une mise en pratique avec *Le livre 010101 (1971-2015)* de Marie Lebert&nbsp;: création d'un site web dédié et d'un fichier EPUB, afin de palier au seul format PDF disponible. Les principes&nbsp;: permettre une lecture *en ligne* ou *hors ligne*. Les contraintes&nbsp;: lisibilité, simplicité et légèreté. Explications du projet.

<!-- more -->

## Le projet de Marie Lebert
Depuis plusieurs années [Marie Lebert](https://marielebert.wordpress.com/) recense les événements majeurs de l'évolution du livre numérique à travers un ouvrage qui a connu plusieurs *éditions*. En avril 2015, elle publie la dernière version à jour&nbsp;: 46 chapitres, plus de 700&nbsp;000 signes, et une chronologie comprenant plus de 200 dates entre 1971 et 2015.

Une précision importante&nbsp;: depuis avril 2015 et le dépôt du livre de Marie Lebert au format PDF [sur le site de l'enssib](http://www.enssib.fr/bibliotheque-numerique/notices/65334-le-livre-010101-1971-2015), la publication est placée sous licence Creative Commons "Attribution - Pas d'Utilisation Commerciale - Pas de Modification 4.0 International". Le contenu du livre peut donc être librement diffusé, hors usage commercial.

## Une démarche personnelle
Seule une version PDF du livre de Marie Lebert est proposée, dommage pour un livre sur le livre numérique. Je décide mettre en forme les contenus déjà disponibles dans un *livre web*, accompagné d'un fichier au format EPUB.  

Ne répondant à aucune demande ou commande, je décide de mener ce projet en mon nom propre, et propose à Marie Lebert des premiers essais.

## www.010101book.net
Le livre est lisible en ligne à cette adresse&nbsp;:

<a class="link-button" href="http://www.010101book.net">www.010101book.net</a>

Le code source du projet est disponible sur GitHub&nbsp;: [https://github.com/antoinentl/010101book.net](https://github.com/antoinentl/010101book.net)


## Les étapes du projet
Pour celles et ceux qui seraient curieux des étapes d'un tel projet, voici quelques explications. Faites-vous un thé ou un café, c'est un peu long&nbsp;!

Je sais par avance que cette entreprise ne sera pas de tout repos, je découpe le projet en plusieurs parties&nbsp;:

1. [Conversion du PDF en HTML](#1)
2. [Scénarios pour une mise en ligne](#2)
3. [Structuration des contenus](#3)
4. [Production du site web](#4)
5. [Design](#5)
6. [Fabrication de l'EPUB](#6)
7. [Quelques briques technologiques](#7)
8. [Bilan (rapide)](#8)

<h3 id="1">1. Conversion du PDF en HTML</h3>
Marie Lebert n'ayant plus accès au fichier .doc originel, je n'ai que le format PDF comme point de départ. Afin de pouvoir envisager de faire quelque chose avec les contenus, je commence par convertir le fichier PDF en HTML, avec [Pandoc](http://pandoc.org/). Heureusement la mise en forme est minimaliste et à défaut d'une réelle feuille de style il y a une structure&nbsp;: des niveaux de titres. Quelques expressions régulières et une relecture en diagonale -- et quelques heures de travail -- et je dispose d'un contenu utilisable.

<h3 id="2">2. Scénarios pour une mise en ligne</h3>
L'envie est forte de vouloir produire un site *à la main*, mais je commence tout de même par faire le tour des quelques solutions qui existent déjà en matière de *livre web*. Je suis le projet [GitBook](https://www.gitbook.com/) depuis le lancement en mai 2014, mais je ne suis pas totalement en accord avec certains choix de design, et si je veux utiliser GitBook sérieusement cela demande un budget. Deux autres arguments m'éloignent de cette solution&nbsp;:

- je veux profiter de ce projet pour construire une interface de lecture *décentralisée*&nbsp;;
- le projet de Marie Lebert n'a pas de dimension collective, donc ici GitBook n'est pas totalement pertinent.

J'écarte assez vite l'utilisation d'un CMS avec base(s) de données, je ne veux pas maintenir un outil dont je ne saurai plus rien dans 3 ans.

Je m'oriente donc vers la production d'un site en HTML+CSS+JavaScript, facile à mettre à jour par moi ou par Marie Lebert, et qui ne demandera pas d'hébergement complexe. [Jekyll](/2014/12/29/un-point-sur-jekyll/) -- *noDB CMS* -- semble tout indiqué.

<h3 id="3">3. Structuration des contenus</h3>
Pour pouvoir intégrer les contenus dans une chaîne de publication basée sur Jekyll, il me faut découper chaque chapitres et sous-chapitres en autant de fichiers Markdown, un exercice de copiés-collés depuis un seul fichier HTML, puis de corrections. Ce sera la partie la plus longue et la plus fastidieuse du projet -- au total il y a 250 fichiers Markdown, autant de pages web qui seront générées via Jekyll.

<h3 id="4">4. Production du site web</h3>
Si vous ne connaissez pas Jekyll vous trouverez une littérature abondante sur le web, mais vous pouvez également lire ces trois billets sur quaternum.net&nbsp;:

- [Pourquoi quitter Wordpress&nbsp;?](/2012/12/23/pourquoi-quitter-wordpress/)
- [LaTeX et Jekyll&nbsp;: deux workflows de publication](/2014/04/26/latex-et-jekyll/)
- [Un point sur Jekyll](/2014/12/29/un-point-sur-jekyll/)

Jekyll est pensé par défaut pour les carnets web ou les *blogs*, mais pour construire un site avec une arborescence à deux niveaux c'est légèrement plus compliqué, Jekyll s'adapte aussi à cet usage. Je passe un certain temps à chercher les solutions les plus efficaces -- en même temps que l'étape 3 -- avec trois contraintes&nbsp;:

- ne pas perdre de temps en recherche et ensuite en intégration&nbsp;;
- trouver des solutions que je peux implémenter -- je ne développe pas en Ruby&nbsp;;
- ne pas faire une usine à gaz.

L'arborescence finale est la suivante&nbsp;:  

- niveau 1&nbsp;: home
- niveau 2&nbsp;: chapitre (niveau d'arborescence sans page HTML) ou *pages statiques* comme l'À propos
- niveau 3&nbsp;: sous-chapitre (s'il n'y a pas de sous-chapitre&nbsp;: création d'une page "Résumé")

Les principales difficultés arriverons lors des choix du design, et notamment du *menu*.


<h3 id="5">5. Design</h3>
Première contrainte&nbsp;: concevoir un design *liquide*. Je ne veux pas gérer des adaptations en fonction des *n* tailles d'écran, mais construire un même design lisible partout. Le site web étant très simple, je peux me permettre de n'envisager qu'un point de rupture&nbsp;: une diminution de la taille -- ou proportion -- des caractères en-deçà d'une certaine largeur.  

Très inspiré par [Professionnal Web Typography](/2015/05/12/un-design-de-livre-web/), et cherchant la simplicité maximale, le choix de la table des matières placée en pied de page me séduit beaucoup. Aussi parce que le site est de fait *single colonn*. Il n'y aura donc pas de menu, mais un accès aux différents chapitres et sous-chapitres sur chaque page et sans *activation* d'un menu. Ce sera l'un des points les plus complexes -- pour moi -- mais je trouve assez rapidement [de l'aide et une solution](http://stackoverflow.com/questions/31194588/generate-a-menu-in-jekyll). Cette table des matières très longues pourrait être amplement critiquée, j'assume cette décision. Dans le cadre de ce projet -- ce qui n'est pas forcément vrai ailleurs -- je défends le fait que la simplicité est un atout&nbsp;: il vaut mieux dérouler une page très longue que mal concevoir un menu qui sera peu visible ou accessible.

La navigation sera elle aussi très simple&nbsp;: deux liens "Partie précédente" et "Partie suivante" en bas de chaque sous-chapitre. La difficulté est de générer convenablement ces liens sans perdre dans l'arborescence, là encore je trouve [des réponses](http://stackoverflow.com/questions/31013878/navigation-for-pages-not-for-posts-in-jekyll) -- Jekyll a cet avantage de disposer d'une communauté active.

Après quelques recherches de couleurs via l'outil [Coloors](https://coolors.co/), je pars tout d'abord sur deux teintes de bleu et du orange flashy, puis j'abandonne le orange pour plus de simplicité. La charte graphique repose sur quatre couleurs (dont du blanc et du faux blanc)&nbsp;:  

![Couleurs utilisées pour 010101book.net](/images/010101-colors.png)  

Le choix de la fonte se fait selon deux critères&nbsp;: une sans serif très lisible et complète. Après avoir tenté la [Lato](http://www.latofonts.com/), je choisis l'[Open Sans](https://fr.wikipedia.org/wiki/Open_Sans), pour plusieurs raisons&nbsp;:

- c'est une fonte très complète&nbsp;;
- elle est très lisible en bas casse *et* en majuscule&nbsp;;
- elle est placée sous licence [Apache version 2.0](http://www.apache.org/licenses/LICENSE-2.0.html), je peux donc l'utiliser librement.


<h3 id="6">6. Fabrication de l'EPUB</h3>
Vous ne savez pas ce qu'est le format EPUB&nbsp;? C'est un format standardisé de livre numérique, un peu comme un site web encapsulé.

Je cherche tout d'abord une solution automatisée de production de l'EPUB&nbsp;: partir des mêmes fichiers du site web -- en Markdown -- pour générer un fichier EPUB, en détournant Jekyll. D'autres y avaient pensé avant moi, mais je ne trouve pas résultats concluants, et conformes aux standards actuels de l'EPUB. Je finis par reprendre le fichier HTML complet pour l'intégrer *à la main* dans des fichiers XHTML via [Sigil](http://sigil-ebook.com/). En quelques heures à peine le fichier EPUB est généré.


<h3 id="7">7. Quelques briques et outils</h3>
En dehors de Jekyll, j'ai utilisé plusieurs *briques* pour la production du site web&nbsp;:

- KNACSS pour la base de la feuille de style, un projet développé et maintenu par Raphael Goetter&nbsp;: [www.knacss.com](http://www.knacss.com)&nbsp;;
- Min+&nbsp;: "un *boilerplate* EPUB minimaliste, léger et modulaire", créé et maintenu par [Jiminy Panoz](http://jiminy.chapalpanoz.com/outils-ebook/).

Toujours pour la production du site web, voici les outils dont je me suis servis&nbsp;:

- un éditeur de texte&nbsp;: [Sublime Text 2](http://www.sublimetext.com/)&nbsp;;
- un navigateur web&nbsp;: [Mozilla Firefox](https://www.mozilla.org/fr/firefox/) et ses outils pour développeurs&nbsp;;
- la liste des bonnes pratiques d'Opquast, [un incontournable](https://checklists.opquast.com/fr/)&nbsp;;
- etc.


<h3 id="8">8. Bilan (rapide)</h3>
Si j'ai l'habitude de travailler en équipe sur des sites web beaucoup plus complexes, ce projet était l'occasion de me confronter à une gestion de projet *complète* en empruntant les différents rôles&nbsp;: chargé de projet, designer, intégrateur, etc.


## Remerciements
[Marie Lebert](https://marielebert.wordpress.com/) pour sa confiance.  
[Willy](http://insitu-collective.com/) pour ses nombreuses aides sur la réalisation de la mise en forme.  
[Anthony](https://twitter.com/AnthonyMasure), [Julien](https://twitter.com/John_Tax), [Frank](http://www.fadebiaye.com/) et [Priscille](https://twitter.com/Priscille_Lgrs) pour les relectures et les retours sur le site web.  
[Jiminy](http://jiminy.chapalpanoz.com/) pour les derniers réglages techniques sur l'EPUB.


## Des questions ou des remarques
Si vous avez des questions ou des remarques sur www.010101book.net, n'hésitez pas à m'écrire&nbsp;:

<a href="mailto:antoine@quaternum.net" class="link-button">Contactez-moi</a>