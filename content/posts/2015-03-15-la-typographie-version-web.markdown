---
layout: post
title: "La typographie version web"
date: 2015-03-15
comments: true
published: true
categories: 
- carnet
---
Voici quelques jours que [le projet CSS SANS est visible et médiatisé](http://yusugomori.com/projects/css-sans/). L'idée ? Créer une fonte uniquement avec des propriétés CSS3, le résultat est disponible [aux formats TTF et OTF](http://yusugomori.com/projects/css-sans/download#tf). Cette ingénieuse création pose une autre question : n'est-il pas possible d'écrire et de dessiner des caractères uniquement avec les outils du web -- HTML et CSS ?

<!-- more -->

CSS SANS permet en effet également d'afficher du texte sur une page web uniquement avec le couple HTML+CSS, sans *appeler* le fichier d'une police. Habituellement, tout texte qui s'affiche sur un écran est une police intégrée dans le système d'exploitation de la machine, ou une police *appelée* via la fonction `font-face` dans le cas du web. Il y a donc toujours un ou plusieurs fichiers *extérieurs* auxquels la page web fait référence. [CSS SANS](http://yusugomori.com/projects/css-sans/) permet de se passer de tels fichiers, et de décrire une police uniquement avec des propriétés CSS3 : chaque lettre est dessinée à partir de courbes et de traits.

## Exemple
Ci-dessous un "A" majuscule dessiné entièrement en CSS, si vous ne voyez rien c'est que votre navigateur n'interprète pas ou mal certaines fonctions CSS3 :  

<style type="text/css">
.A {
  position: relative;
  left: 30px;
  width: 60px;
  height: 91px;
  border-bottom: solid 14px #000000;
}
.A:before {
  -webkit-transform: skew(-19deg, 0);
  -moz-transform: skew(-19deg, 0);
  -ms-transform: skew(-19deg, 0);
  -o-transform: skew(-19deg, 0);
  transform: skew(-19deg, 0);
  position: absolute;
  content: '';
  top: 12.5px;
  left: 0;
  width: 16px;
  height: 125px;
  background-color: #000000;
}
.A:after {
  -webkit-transform: skew(19deg, 0);
  -moz-transform: skew(19deg, 0);
  -ms-transform: skew(19deg, 0);
  -o-transform: skew(19deg, 0);
  transform: skew(19deg, 0);
  position: absolute;
  content: '';
  top: 12.5px;
  left: 45px;
  width: 16px;
  height: 125px;
  background-color: #000000;
}
</style>

<div class="A"></div><br><br>  

Le code source qui va avec :

- feuille de style :

`.A {
  position: relative;
  left: 30px;
  width: 60px;
  height: 91px;
  border-bottom: solid 14px #000000;
}
.A:before {
  -webkit-transform: skew(-19deg, 0);
  -moz-transform: skew(-19deg, 0);
  -ms-transform: skew(-19deg, 0);
  -o-transform: skew(-19deg, 0);
  transform: skew(-19deg, 0);
  position: absolute;
  content: '';
  top: 12.5px;
  left: 0;
  width: 16px;
  height: 125px;
  background-color: #000000;
}
.A:after {
  -webkit-transform: skew(19deg, 0);
  -moz-transform: skew(19deg, 0);
  -ms-transform: skew(19deg, 0);
  -o-transform: skew(19deg, 0);
  transform: skew(19deg, 0);
  position: absolute;
  content: '';
  top: 12.5px;
  left: 45px;
  width: 16px;
  height: 125px;
  background-color: #000000;
}`

- code HTML :

`<div class="A"></div>`

## Full web
Cette initiative semble la première du genre à être aussi aboutie, permettant d'envisager un web uniquement fait d'HTML et de CSS. Du *full web* d'une certaine façon. Quelles différences avec des fichiers Open Type (OTF), WOFF ou True Type (TTF) ? Ces formats sont des descriptions plus complexes que quelques lignes CSS pour dessiner quelques caractères. Mais un système comme *CSS SANS* permet de dire : "ici il n'y a que du web".  
CSS SANS semble lisible et interprétable par les versions les plus récentes des navigateurs web les plus utilisés. Mais il ne s'agit que d'une police en majuscule latine de 26 glyphes, sans caractères accentués, sans bas de casse, sans ligatures, sans gestion de l'Unicode, etc.  
J'avais parlé d'une [Typographie en CSS il y a plus de deux ans](/2013/01/30/une-typographie-en-css/), suite à la création d'une bibliothèque d'icônes utilisée comme une fonte — technique très utilisée pour ne pas avoir à afficher des images. C'est exactement ce dont il s'agit ici, avec les mêmes limites pressenties.

## Les limites
CSS SANS est avant tout un projet créatif. la possibilité d'afficher du texte en *full web* est au stade du concept, et c'est presque un gadget. Cela permet d'entrevoir des possibilités, avec la question de la compatibilité entre navigateurs, et de la longueur et de la complexité du code pour des fontes plus complexes.  
Il ne s'agit pas non plus de mettre à la poubelle des années de travail autour de formats de polices, de tentatives de standardisation et de finesse d'affichage sur le web.  
Il reste également la question de l'accessibilité. Après tout lorsqu'on lit une page web avec une plage braille ou une synthèse vocale, cette notion de fontes disparaît totalement. Mais ici le texte est écrit *en code*, il ne s'agit pas de texte brut mais de balises HTML, un "A" s'écrit `<div class="A"></div>`. Comment peut-il être interprété comme un simple "A" ? Comment agrandir le texte avec les fonctions traditionnelles des navigateurs ? Etc.  

Cette idée de faire reposer l'affichage du texte et de son style uniquement sur du code, des standards et les potentialités des navigateurs est très séduisante, peut-être ne s'agit-il que d'un fantasme.