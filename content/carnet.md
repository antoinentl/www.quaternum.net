---
layout: carnet
title: Carnet
type: pages
---
Des articles sur les enjeux de la publication&nbsp;: web, publication numérique, questionnements technologiques, design, etc. Depuis 2019 en lien avec [mon doctorat](/phd).