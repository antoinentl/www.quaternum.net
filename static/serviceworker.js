'use strict';

const version = 'v0.59::';
const staticCacheName = version + 'static';
const pagesCacheName = version + 'pages';
const imagesCacheName = version + 'images';
const offlinePages = [
    '/',
    '/flux/',
    '/carnet/',
    '/projets/',
    '/a-propos/',
    '/now/',
    '/phd/'
];

function updateStaticCache() {
    return caches.open(staticCacheName)
        .then( cache => {
            // These items won't block the installation of the Service Worker
            cache.addAll([
                '/fonts/bitter-regular-webfont.woff',
                '/fonts/bitter-italic-webfont.woff',
                '/fonts/bitter-bold-webfont.woff',
                '/fonts/bitter-bolditalic-webfont.woff'
            ].concat(offlinePages));
            // These items must be cached for the Service Worker to complete installation
            return cache.addAll([
                '/2020/04/29/les-fabriques-de-publication/',
                '/2020/03/29/atelier-de-these/',
                '/2020/02/29/epuiser-la-pratique/',
                '/2020/01/09/version-concept/',
                '/2019/12/12/l-ecriture-du-papyrus-a-l-hypertexte/',
                '/2019/12/09/cheminement-textuel',
                '/2019/12/05/publier-les-carnets-eloge-du-brouillon/',
                '/2019/11/13/le-trouble-de-l-ecriture-numerique/',
                '/2019/09/21/mes-outils/',
                '/2019/04/27/nos-memoires-numeriques/',
                '/2018/10/27/vers-un-systeme-modulaire-de-publication-crihn/',
                '2018/10/18/markdown-comme-condition-d-une-norme-de-l-ecriture-numerique/',
                '/2018/06/11/pourquoi-gerer-du-contenu-avec-git/',
                '/2018/06/04/un-memoire-en-depot/',
                '/2018/04/04/une-reappropriation-des-donnees-par-leur-structuration/',
                '/2017/03/13/une-chaine-de-publication-inspiree-du-web/',
                '/2017/03/07/ecrire-un-livre-en-2017/',
                '/2017/01/15/2016/',
                '/2017/01/06/interroger-nos-pratiques-de-publication-en-ligne/',
                '/2016/12/14/un-lien-par-jour/',
                '/2016/11/04/blend-web-mix-2016-jour-2/',
                '/2016/11/03/blend-web-mix-2016-jour-1/',
                '/2016/10/24/le-livre-web-une-autre-forme-du-livre-numerique/',
                '/2016/08/31/traitement-de-texte-multicanal/',
                '/2016/06/23/quatre-bonnes-raisons-de-faire-du-web-hors-connexion/',
                '/2016/06/02/le-fanzinat-des-gens-du-web/',
                '/2016/05/19/quatre-singularites-du-livre-numerique/',
                '/2016/03/09/decompte/',
                '/2016/03/07/le-livre-web-comme-objet-d-edition/',
                '/public/css/style.css',
                '/public/css/style-neutral.css',
                '/hors-ligne/'
            ]);
        });
}

function stashInCache(cacheName, request, response) {
    caches.open(cacheName)
        .then( cache => cache.put(request, response) );
}

// Limit the number of items in a specified cache.
function trimCache(cacheName, maxItems) {
    caches.open(cacheName)
        .then( cache => {
            cache.keys()
                .then(keys => {
                    if (keys.length > maxItems) {
                        cache.delete(keys[0])
                            .then(trimCache(cacheName, maxItems));
                    }
                });
        });
}

// Remove caches whose name is no longer valid
function clearOldCaches() {
    return caches.keys()
        .then( keys => {
            return Promise.all(keys
                .filter(key => key.indexOf(version) !== 0)
                .map(key => caches.delete(key))
            );
        });
}

self.addEventListener('install', event => {
    event.waitUntil(updateStaticCache()
        .then( () => self.skipWaiting() )
    );
});

self.addEventListener('activate', event => {
    event.waitUntil(clearOldCaches()
        .then( () => self.clients.claim() )
    );
});

self.addEventListener('message', event => {
    if (event.data.command == 'trimCaches') {
        trimCache(pagesCacheName, 35);
        trimCache(imagesCacheName, 20);
    }
});

self.addEventListener('fetch', event => {
    let request = event.request;
    let url = new URL(request.url);

    // Ignore requests to some directories
    if (request.url.indexOf('/mint') !== -1 || request.url.indexOf('/cms') !== -1) {
        return;
    }

    // For HTML requests, try the network first, fall back to the cache, finally the offline page
    if (request.headers.get('Accept').indexOf('text/html') !== -1) {

        // For non-GET requests, try the network first, fall back to the offline page
        if (request.method !== 'GET') {
            event.respondWith(
                fetch(request)
                    .catch( () => caches.match('/hors-ligne/') )
            );
            return;
        }

        event.respondWith(
            fetch(request)
                .then( response => {
                    // NETWORK
                    // Stash a copy of this page in the pages cache
                    let copy = response.clone();
                    if (offlinePages.includes(url.pathname) || offlinePages.includes(url.pathname + '/')) {
                        stashInCache(staticCacheName, request, copy);
                    } else {
                        stashInCache(pagesCacheName, request, copy);
                    }
                    return response;
                })
                .catch( () => {
                    // CACHE or FALLBACK
                    return caches.match(request)
                        .then( response => response || caches.match('/hors-ligne/') );
                })
        );
        return;
    }

    // For non-HTML requests, look in the cache first, fall back to the network
    event.respondWith(
        caches.match(request)
            .then(response => {
                // CACHE
                return response || fetch(request)
                    .then( response => {
                        // NETWORK
                        // If the request is for an image, stash a copy of this image in the images cache
                        if (request.headers.get('Accept').indexOf('image') !== -1) {
                            let copy = response.clone();
                            stashInCache(imagesCacheName, request, copy);
                        }
                        return response;
                    })
                    .catch( () => {
                        // OFFLINE
                        // If the request is for an image, show an offline placeholder
                        if (request.headers.get('Accept').indexOf('image') !== -1) {
                            return new Response('<svg role="img" aria-labelledby="offline-title" viewBox="0 0 400 300" xmlns="http://www.w3.org/2000/svg"><title id="offline-title">Hors ligne</title><g fill="none" fill-rule="evenodd"><path fill="#D8D8D8" d="M0 0h400v300H0z"/><text fill="#9B9B9B" font-family="Helvetica Neue,Arial,Helvetica,sans-serif" font-size="72" font-weight="bold"><tspan x="93" y="172">hors ligne</tspan></text></g></svg>', {headers: {'Content-Type': 'image/svg+xml'}});
                        }
                    });
            })
    );
});
